package constants;

public class ErrorConstants {

	public static Integer visitorAllSuccessCode = 1200;
	public static Integer ForgetIdAllCheckInSuccessCode = 1300;
	public static Integer ForgetIdAllCheckOutSuccessCode = 1400;

	public static Integer visitorSearchVisitorURLError = 1101;
	public static Integer visitorSearchVisitorJSONError = 1102;
	public static Integer visitorSearchVisitorTimeOutError = 1103;
	public static Integer visitorSearchVisitorNetworkError = 1104;

	public static Integer visitorUpdateVisitorURLError = 1111;
	public static Integer visitorUpdateVisitorJSONError = 1112;
	public static Integer visitorUpdateVisitorTimeOutError = 1113;
	public static Integer visitorUpdateVisitorNetworkError = 1114;
	public static Integer visitorUpdateVisitorIOError = 1115;

	public static Integer visitorUploadVisitorURLError = 1121;
	public static Integer visitorUploadVisitorJSONError = 1122;
	public static Integer visitorUploadVisitorTimeOutError = 1123;
	public static Integer visitorUploadVisitorNetworkError = 1124;
	public static Integer visitorUploadVisitorIOError = 1125;

	public static Integer visitorUploadVisitURLError = 1131;
	public static Integer visitorUploadVisitJSONError = 1132;
	public static Integer visitorUploadVisitTimeOutError = 1133;
	public static Integer visitorUploadVisitNetworkError = 1134;
	public static Integer visitorUploadVisitIOError = 1135;

	public static Integer visitorUploadPhotoURLError = 1141;
	public static Integer visitorUploadPhotoTimeOutError = 1142;
	public static Integer visitorUploadPhotoNetworkError = 1143;
	public static Integer visitorUploadPhotoIOError = 1144;
	public static Integer visitorUploadPhotoNotFoundError = 1145;
	public static Integer visitorUploadPhotoProtocalError = 1146;
	public static Integer visitorUploadPhotoResponseError = 1147;

	public static Integer visitorUploadSignatureURLError = 1151;
	public static Integer visitorUploadSignatureJSONError = 1152;
	public static Integer visitorUploadSignatureTimeOutError = 1153;
	public static Integer visitorUploadSignatureNetworkError = 1154;
	public static Integer visitorUploadSignatureIOError = 1155;

	public static Integer visitorUploadCompletedTrueURLError = 1161;
	public static Integer visitorUploadCompletedTrueJSONError = 1162;
	public static Integer visitorUploadCompletedTrueTimeOutError = 1163;
	public static Integer visitorUploadCompletedTrueNetworkError = 1164;
	public static Integer visitorUploadCompletedTrueIOError = 1165;
	// public static Integer visitorUploadVisitError = 1104;
	// public static Integer visitorUploadPhotoError = 1105;
	public static Integer visitorUploadSignatureError = 1106;
	// public static Integer visitorUploadCompletedTrueError = 1107;
	public static Integer visitorUploadBusinesscardError = 1108;

	// device details error code
	public static Integer visitorDeviceURLError = 1181;
	public static Integer visitorDeviceJSONError = 1182;
	public static Integer visitorDeviceTimeOutError = 1183;
	public static Integer visitorDeviceNetworkError = 1184;
	public static Integer visitorDeviceIOError = 1185;
	public static Integer visitorDeviceCreatorError = 1186;

	public static Integer locationOffCode = 1190;
	public static Integer wifiOffCode = 1191;
	public static Integer wifiBadgePrint = 3101;
	public static Integer wifiAppHomeLaunch1 = 3102;
	public static Integer wifiAppVisitorDetailsLaunch1 = 3103;

	public static Integer SqliteError = 1171;
	public static Integer SendStationeryRequestVolleyException = 1172;

	public static Integer visitorUploadThumbPhotoURLError = 1201;
	public static Integer visitorUploadThumbPhotoTimeOutError = 1202;
	public static Integer visitorUploadThumbPhotoNetworkError = 1203;
	public static Integer visitorUploadThumbPhotoIOError = 1204;
	public static Integer visitorUploadThumbPhotoNotFoundError = 1205;
	public static Integer visitorUploadThumbPhotoProtocalError = 1206;
	public static Integer visitorUploadThumbPhotoResponseError = 1207;

	// Json data upload error code
	public static Integer visitorJsonDataURLError = 1211;
	public static Integer visitorJsonDataJSONError = 1212;
	public static Integer visitorJsonDataIOError = 1213;
	public static Integer visitorJsonDataOtherError = 1214;

	public static Integer visitorUpdateJsonDataURLError = 1221;
	public static Integer visitorUpdateJsonDataJSONError = 1222;
	public static Integer visitorUpdateJsonDataIOError = 1223;
	public static Integer visitorUpdateJsonDataOtherError = 1224;
	public static Integer visitorUpdateJsonDataCreatorError = 1225;
	
	public static Integer visitorUpdateWithOutMailURLError = 1241;
	public static Integer visitorUpdateWithOutMailJSONError = 1242;
	public static Integer visitorUpdateWithOutMailIOError = 1243;
	public static Integer visitorUpdateWithOutMailOtherError = 1244;
	public static Integer visitorUpdateWithOutMailCreatorError = 1245;
	
	public static Integer stationeryRequestSuccess = 1230;
	public static Integer stationeryRequestURLError = 1231;
	public static Integer stationeryRequestJSONError = 1232;
	public static Integer stationeryRequestIOError = 1233;
	public static Integer stationeryRequestJsonDataError = 1234;

	public static Integer checkInCheckOutRequestSuccess = 1270;
	public static Integer checkInCheckOutRequestURLError = 1271;
	public static Integer checkInCheckOutRequestJSONError = 1272;
	public static Integer checkInCheckOutyRequestIOError = 1273;
	public static Integer checkInCheckOutRequestJsonDataError = 1274;

	public static String errorCodeCSV = "ErrorCodeCSV.csv";
	public static String errorCodeCSVTemp = "ErrorCodeCSVTemp.csv";
	public static String dateFieldCSV = "Date";
	public static String ErrorMsgFieldCSV = "Error Message";
	public static String errorCodeFieldCSV = "Error Code";

	// backgroundProcess
	public static Integer backgroundProcessTimeOutException = 4001;
	public static Integer backgroundProcessJsonException = 4002;
	public static Integer backgroundProcessNoNetworkException = 4003;
	public static Integer backgroundProcessIOException = 4004;
	public static Integer backgroundProcessMalformedException = 4005;
	public static Integer backgroundProcessVolleyError = 4006;
	public static Integer backgroundProcessDownloadImageVolleyError = 4007;

	// People Record Count
	public static Integer peopleRecordCountWithSuccess = 5001;
	public static Integer peopleRecordCountWithFailure = 5002;
	public static Integer RecordCountNotGetException = 5003;

	public static String locationOff = "Location off";
	public static String wifiOff = "Wifi off";
	public static String printWifiOff = "";

	public static Integer dataCenterEmployeeFetchedListNull = 6001;

	public static Integer preApprovalRequestSuccess = 7001;
	public static Integer preApprovalRequestURLError = 7002;
	public static Integer preApprovalRequestJSONError = 7003;
	public static Integer preApprovalRequestIOError = 7004;
	public static Integer preApprovalRequestJsonDataError = 7005;

	public static Integer badgePrint = 3001;
	public static Integer badgeNoPrinter = 3002;
	public static Integer badgeViewError = 3003;
	public static Integer testPrint = 3004;

	public static Integer dataBaseLockedException = 8001;
	public static Integer dataBaseReadOnlyException = 8002;

	public static Integer appDCPasswordRequestSuccess = 9001;
	public static Integer appDCPasswordRequestURLError = 9002;
	public static Integer appDCPasswordRequestJSONError = 9003;
	public static Integer appDCPasswordRequestIOError = 9004;
	public static Integer appDCPasswordRequestJsonDataError = 9005;
	public static Integer appDCPasswordRequestResponseJsonDataError = 9006;

	public static Integer dataCentreOTPRequestSuccess = 9001;
	public static Integer dataCentreOTPRequestURLError = 9002;
	public static Integer dataCentreOTPRequestJSONError = 9003;
	public static Integer dataCentreOTPRequestIOError = 9004;
	public static Integer dataCentreOTPRequestJsonDataError = 9005;
	public static Integer dataCentreOTPRequestResponseJsonDataError = 9006;
}
