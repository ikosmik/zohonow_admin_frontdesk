package constants;

public class StringConstants {
    // dialog messages
    public static final String kConstantErrorMsgMandatory = "Enter Phone / Email";
    public static final String kConstantErrorMsgPhoneNumberMandatory = "Enter Phone Number";
    public static final String kConstantErrorMsgToCheckMailOrPhoneNoEmpty = "Enter either your mail ID\n or phone number so we\n could fetch your's details";
    public static final String kConstantErrorMsgToCheckPhoneNo = "Check your phone number";
    public static final String kConstantErrorMsgToValidMailId = "Doesn't seem to be a valid email ID";
    public static final String kConstantErrorMsgToValidDetail = "Some details seem\n to be missing or incorrect\n(marked in red)";
    // public static final String kConstantErrorMsgOfValidQrCode="InValidQrCode";
    public static final String kConstantErrorMsgValidMailText = "Only '.' and '@' allowed";
    public static final String kConstantErrorMsgValidPhoneNo = "Only numbers, '+', '.', '-', '(' and ')' allowed";
    public static final String kConstantErrorMsgValidIndianPhoneNo = "Only numbers, '+' allowed";

    public static final String kConstantSecuritySign = "SecuritySign";
    public static final String kConstantVisitorSign = "VisitorSign";

    public static final String kStringConstantsDeviceModeDataCenter = "DataCenter";
    public static final String kStringConstantsDeviceModeNormal = "Normal Mode";

    // Download image url
    public static final String kConstantErrorMsgValidQrCode = "Sorry!\n The date in the QR code\n is not today";
    public static final String peopleEmployeeDeptName = "departmentname";
    public static final String peopleEmployeeRecordId = "erecno";
    public static final String peopleEmployeeMobileNo = "mobile";
    public static final String peopleEmployeeID = "id";
    public static final String peopleEmployeeFirstName = "first_name";
    public static final String peopleEmployeeLastName = "last_name";
    public static final String peopleEmployeeEmailId = "emailid";
    public static final String peopleEmployeeStatus = "status";
    public static final String peopleEmployeeSeatingLocation = "seatingLocation";

    // employeeRecords in people
    public static final String peopleEmployeeExtension = "extension";
    public static final String peopleEmployeePhoto = "photo";
    public static final String peopleEmployeeZuid = "zuid";
    public static final String peopleEmployeeStatusMsg = "status_msg";
    public static final String peopleEmployeeWorkLocation = "work_location";
    public static final String kConstantQrCodeVisitorName = "Visitor's Name : ";
    public static final String kConstantQrCodeVisitorEmailID = "Visitor's Email ID : ";
    public static final String kConstantQrCodeVisitorPhone = "Visitor's Phone : ";
    public static final String kConstantQrCodeCheckInDate = "Check-In Date : ";
    public static final String kConstantQrCodePurposeOfVisit = "Visit Purpose : ";
    public static final String kConstantQrCodeOfficeLocation = "Visit Office Location : ";
    public static final String kConstantQrCodeVisitId = "Visit ID : ";
    public static final String kConstantQrCodeVisitorId = "Visitor ID : ";
    public static final String parseExitVisitorForm = "Visit";

    // QRCode Constants
    public static final String parseExitVisitId = "ID";
    public static final String parseExitVisitorName = "VisitorID.Name";
    public static final String parseExitVisitPurpose = "purpose.value";
    public static final String parseExitVisitorPhone = "VisitorID.Phone";
    public static final String parseExitVisitorEmail = "VisitorID.EmailID";
    public static final String parseExitEmployeeEmail = "EmployeeEmailID";
    public static final String parseExitVisitAddedTime = "Added_Time";
    public static final String parseExitVisitorCompany = "VisitorID.Company";

    public static final String clearLogMsg = "Clear Log Data?";
    public static final String kConstantLostData = "All data will be lost.\nAre you sure? ";
    public static final String kConstantSaveData = "Data will be Lost?\nYou want to Save ?";
    public static final String kConstantLostSelectionData = "Are you sure? ";

    public static final String okay = "OKAY";

    public static final String kConstantInvalidPasswordFormat = "Invalid Characters Found";
    public static final String kConstantInvalidPassword = "Invalid Password";
    public static final String kConstantEnterPassword = "Enter Password";
    public static final String kConstantAppPasswordSaved = "App Password Saved";

//    DataCentre Level
    public static final String kConstantappPasswordAdminLevel = "QWERTY";
    public static final String kConstantappPasswordOTPAdminLevel = "922892";


    public static final String kConstantMessageOTPExpired = "OTP is Expired Login Again";
    public static final String kConstantMessageOTPNotValid = "OTP is Not Valid";
    public static final String kConstantMessageOTPNotAbletoProcess = "Not able to Process Please Login Again";
    public static final String kConstantMessageOTPWrong = "Please enter Valid OTP";
    public static final String kConstantMessageOTPSessionExpired = "Your Session Expired Login Again";
    public static final String kConstantMessageNoNetworkAvailable =  "Network Not Available, Please connect to the Network";
    public static final String kConstantMessageCollectingData = "Collecting Data...";
    public static final String kConstantMessageBackgroundRunning ="Background Process Running";
    public static final String kConstantMessageBackgroundFinished ="Background process finished";

    // store log details
    // app version
    public static final String AppVersionNameFile = "AppVersionName.txt";
    public static final String AppVersionName = "Version Name : ";
    public static final String AppVersionDescTitle = "\nDescription : ";
    public static final String AppVersionDescValue = "Authtoken Updated Successfully";
    public static final String AppVersionStart = "1.0";
    // Application Logs
    public static final String peopleRecordCountSuccess = "Success: People Record Count ";
    public static final String peopleRecordCountFailure = "Failure: People Record Count ";
    public static final String fetchRecordCount = "Fetch Record Count ";
    public static final String EmployeeCountInLocalMessage = "Employee Record Count From Local after Insertion ";
    public static final String RecordCountNotGetExceptionMessage = "People Record Count Could Not get: ";

    public static final String creatorResponseSuccessFirst = "success";
    public static final String creatorResponseSuccessSecond = "Success";
    public static final String creatorExpectedVisitedDate = "visited";

    public static String kConstantSelectLocation = "Please select location";
    public static String kConstantSelectOfficeLocation = "Please select office location";

    public static String kConstantErrorMsgBadgePrint = "Sorry! could not print your badge!";
    public static String kConstantNoPrinter = "Printer not connected";
    public static String kConstantErrorPrinter = "Printer label empty or printing error";
    public static String kConstantBadgeViewError = "Badge error";
    public static String kConstantErrorMsgTestPrint = "Sorry! could not print your badge!";

    public static String errorCodeText = "Response Code:";

    public static String kConstantPrinterNotConnected = "Printer Not Connected!\nNot able to Print Right Now";
    public static String kConstantPrinterPrintingIDCard = "Printing ID Card...";

    public static String kConstantEmployeeExtensionAll = "Select";
    public static String kConstantEmployeeExtensionError = "Enter your ID without Extension like 0001,92,2892/19,089";
    public static String kConstantVisitorErrorPhoneNumber = "Enter 10 digit Mobile Number";
    public static String kConstantVisitorPhoneNumberText = "Your Phone Number";

    public static final String kConstantEnterEmployeeID = " Enter Employee ID ";
    public static final String kConstantEmployeeIDNotFound = "Seems like the employee ID isn't valid!";
    public static final String kConstantCheckInAgainAlert = "Check In Again...";
    public static final String kConstantCheckOutAgainAlert = "Check Out Again...";
    public static final String kConstantCheckInAgain = "Check In Again !";
    public static final String kConstantCheckOutAgain = "Check Out Again!";
    public static final String kConstantCheckIn = "Check-In";
    public static final String kConstantCheckOut = "Check-Out";

    // Constants for employee count URL
    public static final String employeeCountUrlKeyResponse = "response";
    public static final String employeeCountUrlKeyResult = "result";
    public static final String employeeCountUrlKeyRecordCount = "RecordCount";
    public static final String creatorSuccessMsg = "Success";

    // toast messages
    public static final String toastInvalidEmployeeIdInSecondTime = "Uh Oh! Seems like the employee ID isn't valid!";
    public static final String toastInvalidEmployeeIdInFirstTime = "Uh Oh! Seems like the employee ID isn't valid!";
    public static final String toastEmployeeIdIsEmpty = "Gosh! Looks like you forgot to enter your employee ID!";
    public static final String kConstantDialogConfirm = "CONFIRM";
    public static final String kConstantDialogSave = "SAVE";
    public static final String kConstantDialogCancel = "CANCEL";
    public static final String kConstantDialogBack = "BACK";
    public static final String kConstantDialogHome = "HOME";

    public static final String kConstantHi = "Hi ";
    public static final String kConstantGreetingMorning = "Happy Morning, ";
    public static final String kConstantGreetingNoon = "Happy Noon, ";
    public static final String kConstantGreetingEvening = "Happy Evening, ";
    public static final String kConstantGreetingNight = "Happy Night, ";
    public static final String kConstantSelectStationeryMsg = "!\n What stationery do you need?";
    public static final String kConstantSelectStationeryMsgDragDrop = "!\n Drag and Drop the Stationery You Need";
    public static final String kConstantSendStationeryRequestError = "Oops!\nCouldn't take in your\nStationery request right now!\nTry submitting after a while!";

    public static final String kConstantFileStationeryImageFolder = "StationeryItems/";

    public static final String processingTemporaryIdMsg = "Processing your\ntemporary ID request ...";
    public static final String forgotIdErrorMsg = "Oops!\nCouldn't register your\nrequest right now!\nPlease inform the security\n& make a manual entry.";
    public static final String visitorUploadProcessingMsg = "Checking in...";
    public static final String checkOutProcessingMsg = "Checking Out...";
    public static final String visitorPrintBadgeMsg = "Printing your badge...";
    public static final String enterLaptopSerialNumber = "Enter Laptop Serial Number";
    public static final String enterLaptopMake = "Enter Laptop Model";
    public static final String visitorUploadCompletedMsg = "Thank you\nfor registering!";
    public static final String forgetIDCheckInCompletedMsg = "Successfully Checked In!";
    public static final String forgetIDCheckOutCompletedMsg = "Successfully Checked Out!";
    public static final String welcomeToZohoMsg = "WELCOME TO ZOHO";
    public static final String visitorUploadErrorFirstMsg = "Uh-Oh!";
    public static final String visitorUploadErrorSecondMsg = "Sorry!\nLooks like you need\nto make a manual entry\nthis time!";
    public static final String visitorUploadErrorCodeText = "Error code : ";
    public static final String tellUs = "TELL US";

    public static final String peopleResponse = "response";
    public static final String peoplePunchIn = "punchIn";
    public static final String peopleResponseSuccess = "success";
    public static final String peopleAuthTockenInvalid = "Provided authtoken is invalid";

    public static final String forgotToastMsg = "So now you got your temporary ID!\nPlease return it when you leave today.";
    public static final String forgotIDReturnedNo = "no";

    // Constants to read business card and QrCode

    public static final String kConstantSplitName = "Name";
    public static final String kConstantSplitJob = "Job";
    public static final String kConstantSplitAddress = "Address";
    public static final String kConstantSplitPhone = "Phone";
    public static final String kConstantSplitMobile = "Mobile";
    public static final String kConstantSplitWeb = "Web";
    public static final String kConstantSplitEmail = "Email";
    public static final String kConstantSplitCompany = "Company";
    public static final String kConstantSplitFax = "Fax";
    public static final String kConstantSplitText = "Text";
    public static final String kConstantSplitDate = "Check-In Date :";
    public static final String kConstantSplitVisitorID = "Visitor ID";
    public static final String kConstantSplitVisitID = "Visit ID";
    public static final String kConstantSplitLocation = "Location";
    public static final String kConstantSplitPurpose = "Purpose";

    public static final String kConstantHomePageErrorMessage = "The details you entered\nwill be discarded.\nAre you sure?";
    public static final String kConstantSkipEmployeeMessage = "\nAre you sure?";
    public static final String kConstantHomePageMessageInVisitorBadge = "All details will be lost.\nAre you sure?";
    public static final String kConstantDialogMessageAmSure = "AM SURE";
    public static final String kConstantDialogMessageNotyet = "NOT YET";
    public static final String kConstantBusinessCardErrorMessage = "Some text is invalid, Marked as Red";
    public static final String kConstantBusinessCardOutsideClickMessage = "Click outside to close dialog";
    public static final String kConstantBusinessCardValidMessage = "Couldn\'t read\nyour business card";
    public static final String kConstantBackgroundStarted = "Background process started";
    public static final String kConstantAtleastOneItemSelected = "Forgot to choose\nthe stationery you want?";
    public static final String kConstantBackgroundPermissionError = "Can't start background process";
    public static final String kConstantExportPermissionError = "Can't export database";

    public static final String kConstantErrorMsgInPhotoShoot = "Take Photo";
    public static final String kConstantErrorMsgInName = "Enter your Name";
    public static final String kConstantErrorMsgInContact = "Doesn't seem to be a valid Mobile No or EmailId";
    public static final String kConstantErrorMsgInInQrCodeInValidDate = "Sorry! The date in the QR Code is not today";
    public static final String kConstantErrorMsgInQrCodeLocation = "Sorry!\nYou seem to be expected\nin a different office location";
    public static final String kConstantErrorMsgInValidEmployeeDetail = "Sorry!\nWe couldn\'t trace an employee\nwith the detail you gave.";
    public static final String kConstantErrorMsgInNetworkError = "Oops!\nLooks like there\'s no internet connectivity";
    public static final String kConstantErrorMsgInPrinterError = "Oops!\nLooks like there\'s no printer connectivity";
    public static final String kConstantMsgInStationeryItemUploadSuccess = "Hooray! Your stationery is now ready for pickup from the reception!";
    public static final String kConstantMsgInStationeryItemSendRequest = "Sending Request ...";

    public static final String kConstantErrorMsgInFillDetail = "Fill in all the details please";
    public static final String kConstantDialogTitleGetEmployeeId = "Enter employee ID for request stationery item";
    public static final String kConstantDialogHintMsgGetEmployeeId = "Enter your Employee ID?";
    public static final String kConstantDialogHintMsgAllItemsAdded = "ALL ITEMS ADDED TO THE LIST";

    public static final String kConstantValidSerialNumber =  "Enter Valid Serial Number";
    public static final String kConstantSerialNumber = "Enter Serial Number";
    public static final String kConstantDeviceModel = "Choose Device Model";
    public static final String kConstantOtherDeviceModel = "Enter Device Model";

    public static final String kConstantRequestSentToAdmin = "Request Sent to Frontdesk Admin";
    public static final String kConstantRequestSent = "Request Already Sent";
    public static final String kConstantRequestNotSentCreatorIssue = "Request Not Sent";

    public static final String kConstantMessageGreatDay = "Have a great day!";
}
