package constants;

public class DataBaseConstants {

	public static String dbName = "visitor.db";
	// public static String tableNameVisitor = "Visitor";
	public static String name = "Name";
	public static String country = "Country";
	public static String frequency = "Frequency";
	public static String position = "Position";
	public static String name_country_position = "Name_Country_Position";
	public static String tableIsCreate = "DROP TABLE IF EXISTS ";
	public static String tableNotCreate = "CREATE TABLE IF NOT EXISTS ";
	public static String visitorImage = "VisitorImage";
	public static String visitorImagePath = "VisitorImagePath";
	public static String dropTable = "DROP TABLE IF EXISTS ";
	public static String insertInto = "INSERT INTO ";
	public static String valuesForExistVisitor = " VALUES (?,?,?,?,?,?)";

	public static String tableNameEmployee = "Employee";
	public static String tableNameEmployeeForgetID = "employee_forget_id";
	public static String tableNameEmployeeNewTable = "NewEmployee";
	public static String dbEmployeeName = "Name";
	public static String dbEmployeeDept = "Department";
	public static String dbEmployeeId = "EmployeeID";
	public static String dbEmployeeRecordId = "RecordId";
	public static String dbTableRowId = "ID";
	public static String dbEmployeeFirstName = "FirstName";
	public static String dbEmployeeLastName = "LastName";
	public static String dbEmployeeEmailId = "EmailId";
	public static String dbEmployeePhoneNumber = "PhoneNo";
	public static String dbEmployeeStatus = "Status";
	public static String dbEmployeeSeatingLocation = "SeatingLocation";
	public static String dbEmployeeExtension = "Extension";
	public static String dbEmployeePhoto = "Photo";
	public static String dbEmployeeZuid = "Zuid";
	public static String dbEmployeeVisitFrequency = "VisitFrequency";
	public static String dbEmployeeCheckInTime = "CheckInTime";
	public static String dbEmployeeCheckOutTime = "CheckOutTime";
	public static String dbEmployeeCheckInTimeDate = "CheckInTimeDate";
	public static String dbEmployeeCheckOutTimeDate = "CheckOutTimeDate";
	public static String dbEmployeeOfficeLocation = "OfficeLocation";
	public static String dbEmployeeTemporaryID = "TemporaryID";
	public static String dbIsSyncWithCreator = "SyncedWithCreator";
	public static String dbIsDataCenterEmployee="IsDataCenterEmployee";

	//key management system, key_list
	public static String tableNameKeyList = "keylist";
	public static String dbColumnKeyID = "key_id";
	public static String dbColumnKeyPhoto = "key_photo";
	public static String dbColumnKeyType = "key_type";
	public static String dbColumnSecurityDetails = "security_details";
	public static String dbColumnFurnitureType = "furniture_type";
	public static String dbColumnVehicleNumber = "vehicle_number";
	public static String dbColumnRoomDetails = "room_details";
	public static String dbColumnOfficeLocation = "office_location";
	public static String dbColumnLocationInsideOffice = "location_inside_office";
	public static String dbColumnStatus = "status";
	public static String dbColumnSpareKeyCount = "spare_key_count";
	public static String dbColumnIsActive = "is_active";
	public static String dbColumnDescription = "description";
	//Key_allocation
	public static String tableNameKeyAllocation = "key_allocation";
	public static String dbColumnAllKeyID = "key_id";
	public static String dbColumnAllOfficeLocation = "office_location";
	public static String dbColumnAllLocationInsideOffice = "location_inside_office";
	public static String dbColumnAllAllocatedTime = "allocated_time";
	public static String dbColumnAllDeAllocatedTime = "de_allocated_time";
	public static String dbColumnAllReturnedBy = "returned_by";
	public static String dbColumnAllPersonThumbPhoto = "person_thumb_photo";
	public static String dbColumnAllPersonName = "person_name";
	public static String dbColumnAllPersonPhoto = "person_Photo";
	public static String dbColumnAllEmail = "person_email";
	public static String dbColumnAllMobileNo = "mobile_no";
	public static String dbColumnAllPersonSignature = "person_signature";
	public static String dbColumnAllReturnedPersonName = "returned_visitor_name";
	public static String dbColumnAllReturnedPersonContact = "returned_person_contact";

	public static String ConstantsDeleteQuery = "DELETE FROM ";
	// Database kConstants of purpose of visit

	public static String tableNamePurposeOfVisit = "PurposeOfVisit";
	public static String tableNameNewStationeryList = "NewStationeryList";
	public static String tableNameStationeryDetails = "StationeryDetails";

	//Existing visitor details table
	public static String tableExistingVisitor = "existing_visitor_table";
	public static String colnExistVisitorName = "exist_visitor_name";
	public static String colnExistVisitorEmployee = "exist_emp_email";
	public static String colnExistVisitorPurpose = "exist_purpose";
	public static String colnExistVisitorContact = "exist_contact";
	public static String colnExistLastVisitDate = "exist_last_visit_date";
	public static String colnExistCreatorVisitID = "exist_creator";

//  for Datacenter Company Name Has Been Added
	public static String colnExistCreatorCompanyName = "company_name";

//	DataCenter Pre-Approval Table
	public static String dbtableNamePreApproval = "PreApprovalData";
	public static String dbtableNamePreApprovalMultiPersonDetail = "MultipleVisitorDetails";

	public static String dbColumnNamePreApprovalVisitorName = "VisitorName";
	public static String dbColumnNamePreApprovalVisitorCompanyName = "CompanyName";
	public static String dbColumnNamePreApprovalDateofVisitLong = "DateOfVisitlong";
	public static String dbColumnNamePreApprovalDateofVisit ="DateofVisitDate";
	public static String dbColumnNamePreApprovalVisitorPhoneNumber = "PhoneNumber";
	public static String dbColumnNamePreApprovalEmailId="DataCenterEmployeeEmailID";
	public static String dbColumnNamePreApprovalisVisitorActive = "isVisitorActive";
	public static String dbColumnNamePreApprovalDetailsCreatorId ="CreatorId";

	public static String purpose = "VisitPurpose";
	public static String visitFrequency = "Frequency";
	public static String location = "Location";
	public static String kConstantsQuerySelect = " SELECT ";
	public static String kConstantsQueryFrom = " FROM ";
	public static String kConstantsSelectQuery = "SELECT * FROM ";
	public static String kConstantsSelectQueryForEmployeeFilter = "SELECT Name,PhoneNo,Photo FROM ";
	public static String kConstantsSelectQueryForVisitPurpose = "SELECT VisitPurpose FROM ";
	public static String kConstantsOpenBrace = " (";
	public static String kConstantsCloseBrace = ") ";
	public static String kConstantsCamma = ",";
	public static String kConstantsPrimaryKeyLong = " LONG not null, ";
	public static String kConstantsLongwithComma = " LONG,";
	public static String kConstantsLong=" Long ";
	public static String kConstantsForeignKey = " FOREIGN KEY ";
	public static String kConstantsNotNull = " NOT NULL ";
	public static String kConstantsDefault = "DEFAULT ";
	public static String kConstantsText = " TEXT ,";
	public static String kConstantsBlob = " BLOB ,";
	public static String kConstantsInteger = "  INTEGER,";
    public static String kConstantsIntegerWithoutComma="  INTEGER ";
	public static String kConstantsTextWithCloseBrace = " TEXT);";
	public static String kConstantsIntegerWithCloseBrace = " INTEGER);";
	public static String kConstantsWhere = " WHERE ";
	public static String kConstantsUpdate = "UPDATE ";
	public static String kConstantsSet = " SET ";
	public static String kConstantsEqualWithSingleQuote = "='";
	public static String kConstantsSingleQuote = "'";
	public static String kConstantsLikeStatement = " LIKE'%";
	public static String kConstantsOnDeleteCascade = " ON DELETE CASCADE";
	public static String kConstantsReferences= " REFERENCES ";

	public static String kConstantsLikeStatementWithSingleQuote = " LIKE '";
	public static String kConstantsPercentageWithSingleQuote = "%'";
	public static String kConstantsWhereConditionInVisitPurpose = " WHERE VisitPurpose LIKE '";
	public static String kConstantsOrderBy = " ORDER BY ";
	public static String kConstantsDescWithLimit = " DESC LIMIT ";
	public static String kConstantsLimit = " LIMIT ";
	public static String kConstantEqual = " = ";
	public static String kConstantLessthan = " < ";
	public static String kConstantTextPrimaryKey = " TEXT PRIMARY KEY,";
	public static String kConstantIntegerPrimaryKey = " INTEGER PRIMARY KEY,";
	public static String kConstantPrimaryKey = " PRIMARY KEY NOT NULL,";
    public static String kConstantPrimaryKeyAutoIncrement = " INTEGER NOT NULL ";

	public static String kConstantsDeleteLogQuery = "DELETE FROM logs WHERE date NOT IN (SELECT date FROM logs ORDER BY date DESC LIMIT 500)";
	public static String kConstantsDeleteVisitorQuery = "DELETE FROM visitor_details WHERE date NOT IN (SELECT date FROM visitor_details ORDER BY date DESC LIMIT 500)";
	
	// database Constants of visitorDetail

	// public static String kConstantVisitorDetail = "VisitorDetail";
	public static String tableNameVisitorDetail = "VisitorDetail";
	public static String visitorId = "visitorID";
	public static String visitorName = "Name";
	public static String visitorRecordId = "ID";
	public static String visitorMobileNo = "Phone";
	public static String visitorMailId = "EmailID";
	public static String visitorFax = "Fax";
	public static String visitorAddress = "Location";
	public static String visitorCompanyWebsite = "CompanyWebsite";
	public static String visitorDesignation = "Designation";
	public static String visitorBusinessCard = "BusinessCard";
	public static String visitorCompany = "Company";

	// database Constants of visitorDetailInOffMode
	public static String tableNameVisitorRecordInOffMode = "VisitorRecordOffMode";
	public static String kConstantIdAutoIncrement = " INTEGER PRIMARY KEY AUTOINCREMENT,";

	public static String kConstantIdAutoIncre = " INTEGER PRIMARY KEY AUTOINCREMENT,";

	// database Constants Of VisitRecordInOffMode

	public static String tableNameVisitRecordInOffMode = "VisitRecordOffMode";
	public static String visitId = "visitID";
	public static String visitIdInCreator = "creatorVisitID";
	public static String visitPurpose = "purpose";
	public static String dateOfVisit = "dateOfVisit";
	public static String officeLocation = "officeLocation";
	public static String visitEmployeeEmailId = "employeeEmailId";
	public static String codeNo = "codeNumber";
	public static String Approval = "Approval";
	public static String visitorPhoto = "visitorPhoto";
	public static String visitorQrCode = "visitorQRcode";
	public static String visitorSign = "visitorSign";
	public static String isItUpload = "isItUpload";
	public static String visitEmployeeName = "employeeName";
	public static String visitEmployeeId = "employeeID";
	public static String expectedDateOfVisit = "expectedDateOfVisit";
	public static String officeLocationNumber = "officeLocationNumber";
	public static String GMTDiffMinutes = "GMTDiffMinutes";
	public static String visitEmployeePhoneNo = "employeePhoneNumber";

	// database Constants of visitorVisitLink

	public static String contact = "contact";
	public static String tableVisitorVisitLink = "VisitorVisitLink";

	// database Constants of logs

	public static String tableNameLogs = "logs";
	public static String dbLogDate = "date";
	public static String dbLogErrorMessage = "error_message";
	public static String dbLogErrorCode = "error_code";
	public static String dbLogWifiSingnalStrength = "wifi_signal_strength";
	public static String kConstantOrderBy = " ORDER BY ";
	public static String kConstantGroupBy = " GROUP BY ";

	public static String kConstantDesc = " DESC ";

	// database Constants of un completed data
	public static String tableNameUnComplete = "uncomplete";
	public static String dbJsonDataDate = "date";
	public static String dbJsonDataId = "json_data_id";
	public static String dbJsonDataVisitPurpose = "purpose";
	
	// database Constants of visitor data
	public static String tableNameVisitor = "visitor_details";
	public static String dbVisitorVisitDate = "date";
	public static String dbVisitorJsonId = "json_data_id";
	public static String dbVisitorJsonData = "json_data";
	public static String dbVisitorPhotoUrl = "photo_url";
	public static String dbVisitorSignatureUrl = "signature_url";
	public static String dbVisitorPhotoUploaded = "photo_uploaded_code";
	public static String dbVisitorSignatureUploaded = "signature_uploaded_code";
	public static String dbVisitorPurpose = "purpose";

	// stationery table

	public static String tableStationeryList = "StationeryList";
	public static String dbStationeryItemName = "Name";
	public static String dbStationeryItemCode = "Code";
	public static String dbStationeryItemLimitPerMonth = "LimitPerMonth";
	// public static String dbStationeryItemImage="Image";
	public static String dbStationeryCreatorId = "CreatorId";
	public static String dbStationeryLocationCode = "LocationCode";
	public static String dbDirtyBit = "DirtyBit";
	public static String dbStationeryOfficeDeviceLocation = "DeviceLocation";
	public static String dbStationeryOfficeParentLocation = "ParentLocation";

	public static String tableStationeryRequest = "StationeryRequest";
	public static String dbStationeryRequestId = "RequestId";
	public static String dbStationeryRequestTime = "RequestTime";
	public static String tableStationeryItemEntry = "StationeryItemEntry";
	public static String dbStationeryEntryTime = "StationeryItemEntryTime";
	public static String dbStationeryItemQuantity = "Quantity";
	// public static

	public static String stationeryItemName = "name";
	public static String stationeryItemCode = "code";
	public static String stationeryItemLimitPerMonth = "limit_per_month";
	public static String stationeryItemImage = "image";

	public static String stationeryRequestId = "request_id";
	public static String stationeryRequestTime = "request_time";
	public static String stationeryItemQuantity = "quantity";
	public static String stationeryEntryTime = "entry_time";

	public static String addStationeryItemName = "&name=";
	public static String addStationeryItemCode = "&code=";
	public static String addStationeryItemLimitPerMonth = "&limit_per_month=";
	public static String addStationeryItemImage = "&image=";
	public static String addStationeryEmployeeId = "&employee_id=";
	public static String addStationeryEmployeeName = "&employee_name=";
	public static String addStationeryEmployeeEmailId = "&employee_email_id=";
	public static String addStationeryEmployeePhoneNo = "&employee_mobile_no=";
	public static String addStationeryEmployeeExtention = "&employee_extension=";
	public static String addStationeryEmployeeLocation = "&employee_location=";
	public static String addStationeryEmployeeZuid = "&employee_zuid=";

	public static String addLogCode = "&log_code=";
	public static String addLogMsg = "&log_message=";

	public static String addStationeryRequestId = "&request_id=";
	public static String addStationeryRequestTime = "&request_time=";
	public static String addStationeryItemQuantity = "&quantity=";
	public static String addStationeryEntryTime = "&entry_time=";
	public static String addStationeryEntry = "&stationery_entry=";

}
