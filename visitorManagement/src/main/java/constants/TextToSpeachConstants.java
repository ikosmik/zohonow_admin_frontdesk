package constants;

public class TextToSpeachConstants {
	
	public static final String speachName = "Enter your name";
	public static final String speachContact = "Enter your phone number or email ID";
	public static final String speachContactDatacenter = "Enter your phone number";
	public static final String speachEmployeeName = "Enter employee name";
	public static final String speachPurpose = "What is the purpose of the visit?";
	public static final String speachTempId = "Enter your temporary ID";
	public static final String speachEmpId = "Enter your employee ID";
	public static final String speachSignature = "Put your signature with your hand";
	public static final String speachHaveLaptop = "Have laptop or any electronic devices?";
	public static final String speachWelcome = "Welcome to ZOHO";
	public static final String speachAreYouSure = "Data will be lost\n\nAre you sure?";
	public static final String speachSmilePlease = "Smile please";
	public static final String speachNoface = "Your face is not visible. \nStand in front of the camera.\nCome closer to the camera.";
	public static final String speachCheckInNow = "Tab the button to check-in";
	public static final String speachDragDropItem = "Drag and Drop the Stationery";
	public static final String speachCompanyName = "Enter Your Company Name";
	public static final String speachOTP = "Enter O T P";
	public static final String speachResendOTP = "Re send O T P";
}
