package constants;

public class MainConstants {

    public static final int second = 1000;
    public static final int noFaceTime = 30000;
    public static final int noFaceDragDropTime = 60000;
    public static final int noActionTime = 30000;
    public static final int noActionValue = 3000;
    public static final int dragViewLoadTime = 100;
    public static final int signValidateTime = 500;
    public static final int afterTypingNoFaceTime = 60000;
    public static final int purposeDialogShowTime = 300;
    public static final int afterTypingNoFaceTimeInVisitorDetailActivity = 200000;
    public static final int autoCheckInTime = 15000;
    public static final int autoCheckInTimeDevice = 180000;
    public static final int autoCheckInLaptopTime = 40000;
    public static final int backgroundImgChangeTime = 10000;
    public static final int BRIGHTNESS_DIM_TIMEOUT = 120000;
    public static final int backgroundCreatorSync = 1000 * 60 * 1;
    public static final String yourname = "Please Enter Your Name";
    public static final String USER_AGENT = "Mozilla/5.0";

    public static final String kConstantRaw = "&raw=true";
    public static String creatorOwnerName = "frontdesk10/";
    public static String creatorFrontdeskOwnerName = "frontdesk10";


    public static String creatorAppLinkName = "&applinkname=";
    public static String creatorFieldLinkName = "&fieldname=";
    public static String creatorFieldOwnerName = "&zc_ownername=";
    public static String creatorJsonFormat = "json/";
    public static String backSlash = "/";
    public static String creatorViewText = "/view/";
    public static String creatorFormText = "/form/";
    public static String creatorRecord = "/record/";
    public static String creatorUpdate = "update/";
    public static String creatorAdd = "add/";
    public static String creatorAuthtokenText = "authtoken=";
    public static String creatorAuthtokenValue = "a405e2f5b1479bb38f971a99f8a2c599";
    public static String creatorScope = "&scope=";
    public static String creatorScopeValue = "creatorapi";
    public static String creatorRawTrue = "&raw=true";
    public static String creatorCriteria = "&criteria=";

    public static final String rawAndOwnerName = kConstantRaw+creatorFieldOwnerName+creatorFrontdeskOwnerName;

    public static final String peopleCheckInAuthToken = "authtoken=";
    public static final String peopleCheckInDateFormatString = "dateFormat";
    public static final String peopleCheckInCheckIn = "checkIn";
    public static final String peopleCheckInEmailId = "emailId";
    public static final String peopleCheckInAuthTokenValue = "b2abba0d72c97328b3b8031aff79d5f4";

    public static String creatorVisitorAppName = "visitormanagement";
    public static String creatorEmployeeCommonAppName = "zoho-common";
    public static String creatorTemporaryIDAppName="frontdesk-temporary-id-management";
    public static final String creatorStationeryAppName = "zohonow-stationery-request/";

    public static String creatorLink = "https://creator.zoho.com/api/";

    public static final String kConstantCreatorAuthTokenAndScope = creatorAuthtokenText+creatorAuthtokenValue+creatorScope+creatorScopeValue;
    public static final String kConstantPeopleAuthToken = "&authtoken="+peopleCheckInAuthTokenValue;

    //StringEncoder
    public static final String kConstantUTFEight="UTF-8";

    public static final String kConstantAddVisitorOTP = "/form/visitor_otp_details/record/add/";

    // public static Context kContext;
    public static final String kConstantPurposeCriteria = "&criteria=VisitPurpose==\"";
    public static final String kConstantUploadPurposeRawWithId = "&raw=true&VisitPurpose=";

    public static final String kConstantAddPurposeRecord = "/form/VisitPurpose/record/add/";
    public static final String kConstantViewPurposeRecord = "VisitPurpose_Report?"+kConstantCreatorAuthTokenAndScope+rawAndOwnerName;
    public static final String kConstantViewPurposeRecordCriteria = "&criteria=LocationCode==";
    public static final String kConstantUpdatePurposeDetail = "/form/VisitPurpose/record/update/";
    public static final String kConstantEmployeeFrequencyReport = "employee_visit_frequency_Report?";
    public static final String kConstantVisitReport = "Visit_Report?";
    public static final String kConstantOTPRequestReport = "visitor_otp_details_report?";
    public static final String kConstantEmpFreqReport = "emp_frequency_Report?";
    public static final String kConstantAddEmployeeFrequency = "/form/employee_visit_frequency/record/add/";
    public static final String kConstantFormName = "formname";
    public static final String kConstantStatus = "status";
    public static final String kConstantOperation = "operation";
    public static final String kConstantValues = "values";
    public static final String kConstantNewValues = "newvalues";
    public static final String kConstantUpdate = "update";
    public static final String approval = "no";
    public static final String kConstantZohoMail = "@zohocorp.com";
    public static final String supportMail = "frontdesk@zohocorp.com";
    public static final String toastMsgForMaximumCount = "Available count 10 only.";
//     Constants of UploadFile

    public static final String kConstantLineEnd = "\r\n";
    public static final String kConstantEncrype = "ENCTYPE";
    public static final String kConstantContentType = "Content-Type";
    public static final String kConstantMultiPart = "multipart/form-data";
    public static final String kConstantBoundary = ";boundary=";
    public static final String kConstantUploadFile = "uploaded_file";
    public static final String kConstantreplaceUrl = "%20";
    public static final String kConstantComma = ",";
    public static final String kConstantAuth = "Auth=";
    public static final String kConstantContentDisposition = "Content-Disposition: form-data; name=\"file\";filename=\"";
    public static final String kConstantFileUploadUrl = creatorLink+"xml/fileupload/";
    public static final String kConstantFileUploadAppNameAndFormNameInVisitDetail =creatorAppLinkName+creatorVisitorAppName+"&formname=Visit";
    public static final String kConstantFileUploadAppNameAndFormNameAppJsonDetail = creatorAppLinkName+creatorVisitorAppName+"&formname=app_upload_data";
    public static final String kConstantFileUploadInterviewAppNameAppJsonDetail = creatorAppLinkName+"zohonow-interview&formname=app_upload_data";
    public static final String kConstantFileUploadAppNameAndFormNameInVisitorDetail = creatorAppLinkName+creatorVisitorAppName+"&formname=Visitor";
    public static final String kConstantFileUploadFieldNameInVisitorImage = creatorFieldLinkName+"VisitorPhoto&recordId=";
    public static final String kConstantFileUploadFieldName = creatorFieldLinkName+"visitor_photo&recordId=";
    public static final String kConstantVisitorBadgeFileUploadFieldName = creatorFieldLinkName+"visitor_badge&recordId=";
    public static final String kConstantFileUploadFieldJSONInVisitorSign = creatorFieldLinkName+"visitor_signature&recordId=";
    public static final String kConstantFileUploadFieldNameInVisitorSign = creatorFieldLinkName+"VisitorSignature&recordId=";
    public static final String kConstantFileUploadFieldNameInBussinessCard = creatorFieldLinkName+"BusinessCard&recordId=";
    public static final String kConstantFileAccessTypeAndName = "&formAccessType=1&filename=";
    public static final String kConstantFileSharedBy = "&sharedBy=frontdesk10";
    public static final String kConstantZohoPeopleApi = "https://people.zoho.com/people/fetchPeopleContacts.do?authtoken=c369a17d7cab82a61a0f82748228e406&sIndex=0";
    public static final String kConstantZohoPeoplecountApi = "https://people.zoho.com/people/api/forms/employee/getRecordCount?";
    public static final String kConstantPeopleAuthTokenForGetCount =peopleCheckInAuthToken+peopleCheckInAuthTokenValue;
    public static final String kConstantNewLine = "\n";
    public static final String kConstantGetMethod = "GET";
    public static final String kConstantPostMethod = "POST";
    public static final String kConstantOk = "Ok";
    public static final String kConstantCancel = "Cancel";
    public static final String kConstantPurposeRecord = "VisitPurpose";
    public static final String kConstantEmployeeVisitFrequency = "employee_visit_frequency";
    public static final String kConstantEmployeeIDFrequency = "employee_id";
    public static final String kConstantEmployeeFrequency = "visit_frequency";
    public static final String kConstantFrequency = "Frequency";
    public static final String kConstantPurposeLocation = "LocationCode";
    public static final String kConstantDoubleHyphen = "--";
    public static final String kConstantOfficeInIndia = "India";
    public static final int kConstantBufferSize = 1024;

    // VisitorDetail Constant values
    public static final String kConstantVisitorDetail = "Visitor";
    public static final String kConstantVisitorName = "Name";
    public static final String kConstantVisitorId = "ID";
    public static final String kConstantMobileNo = "Phone";
    public static final String kConstantMailId = "EmailID";
    public static final String kConstantFax = "Fax";
    public static final String kConstantLocation = "Location";
    public static final String kConstantCompanyWebsite = "CompanyWebsite";
    public static final String kConstantDesignation = "Designation";
    public static final String kConstantBusinessCard = "BusinessCard";
    public static final String kConstantBusinessCardFileName = "BusinessCard.jpg";
    public static final String kConstantCompany = "Company";
    public static final String kConstantFileDirectory = "/sdcard/FrontDesk/";
    public static final String kConstantDBExportedPath = "/sdcard/FrontDesk/frontdesk.db";
    public static final String kConstantUploadExportedPath = "/sdcard/FrontDesk/image.jpg";
    public static final String kConstantDBCurrentPath = "/data/com.zoho.app.frontdesk.android/databases/visitor.db";
    public static final String kConstantFileTemporaryFolder = "Temp/";
    public static final String kConstantFileEmployeeImageFolder = "Employees/";
    public static final String kConstantFileStorageFolder = "Storage/";
    public static final String kConstantFileBadge = "/Temp/Badge.jpg";
    public static final String kConstantFileVisitorBadge = "/Temp/";

    // StationeryDetail Constant

    public static final String kConstantStationeryFormName = "StationeryList";
    public static final String kConstantLocationStationeryMappingFormName = "location_stationery_mapping";

    public static final String kConstantStationeryEntryFormName = "StationeryItemEntry";
    public static final String kConstantStationeryRequestFormName = "StationeryRequest";
    public static final String kConstantEmployeeForgetID = "employee_forget_id";
    public static final String kConstantDataCenterEmployeeFormName = "datacentre_employee_details";
    public static final String kConstantDataCenterEmployeePreApprovalFormName = "data_centre_pre_approved_visitor_details";
    public static final String kConstantEmployeeCommonExtension = "employee_extension";

    public static final String kConstantCreatorVisitorName = "&Name=";
    public static final String kConstantCreatorLocation = "&Location=";
    public static final String kConstantCreatorMobileNo = "&Phone=";
    public static final String kConstantCreatorEmail = "&EmailID=";
    public static final String kConstantCreatorDesignation = "&Designation=";
    public static final String kConstantCreatorCompany = "&Company=";
    public static final String kConstantCreatorCompanyWebsite = "&CompanyWebsite=";
    public static final String kConstantCreatorFax = "&Fax=";
    public static final String apiKey = "AIzaSyBmnjCjCGjGoP9tALFkLz-_dzdDujVLzlY";
    public static final String kConstantKey = "&key=";
    public static final String kConstantAutoCompleteLocationUrl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=";
    public static final String kConstantReplaceLocation = "[^a-zA-Z0-9/.]";
    public static final String kConstantEmpty = "";
    public static final String kConstantSingleQuotes = "'";
    public static final String kConstantOpenBraces = "{";
    public static final String kConstantCloseBraces = "}";
    public static final String kConstantAmpersand = "&";
    public static final String kConstantWhiteSpace = " ";
    public static final String kConstantMinus = "-";
    public static final String kConstantOpen = "(";
    public static final String kConstantClose = ")";
    public static final String kConstantRemoveNonNumeric = "[^\\d]";

    public static final String kConstantColon = ":";
    public static final String kConstantCurrentLocation = " Current location";
    public static final int kConstantLocationMaxLength = 2;
    public static final String kConstantVisitorImageFileFormat = "%d.jpg";
    public static final String kConstantImageFileFormat = ".jpg";
    public static final String kConstantImageThumbFile = "/thumb.jpg";
    public static final String kConstantImageUploadFile = "/fileUpload.jpg";
    public static final int kConstantSharedPreferenceControllerAppTheme = 0;

    public static final String TAG_RESULT = "predictions";

    public static final String kConstantvalidMailOrMobileNo = "[a-zA-Z]";
    public static final String kConstantMailTo = "mailto:";
    public static final String kConstantValidPlaceUrl = "[^a-zA-Z0-9/.]";
    public static final String kConstantEnterName = "Please enter Your Name";
    public static final String kConstantAcceptAlphabetMessage = "Accept Alphabets Only.";
    public static final Integer kConstantSuccessCode = 200;
    public static final Integer kConstantErrorTextSize = 20;

    // VisitDetail Constant Values
    public static final String kConstantCreatorVisitorId = "&VisitorID=";
    public static final String kConstantCreatorPurpose = "&Purpose=";
    public static final String kConstantCreatorPurpose1 = "&purpose=";
    public static final String kConstantCreatorDateOfVisit = "&DateOfVisit=";
    public static final String kConstantCreatorDateOfVisitLocal = "&CheckInTimeLocal=";
    public static final String kConstantCreatorExpectedDate = "&ExpectedDateOfVisit=";
    public static final String kConstantCreatorGMT = "&GMTDiffMinutes=";
    public static final String kConstantCreatorOfficeLocation = "&OfficeLocation=";
    public static final String kConstantCreatorLocationNo = "&OfficeLocationCode=";
    public static final String kConstantCreatorEmployeeName = "&EmployeeName=";
    public static final String kConstantCreatorEmployeeId = "&EmployeeID=";
    public static final String kConstantCreatorEmployeeEmailId = "&EmployeeEmailID=";
    public static final String kConstantCreatorEmployeePhoneNo = "&EmployeePhone=";
    public static final String kConstantCreatorEmployeeExtention = "&EmployeeExtension=";
    public static final String kConstantCreatorCheckInTimeLocal = "&CheckInTimeLocal=";
    public static final String kConstantCreatorSeatingLocation = "&EmployeeSeatingLocation=";
    public static final String kConstantCreatorCodeNo = "&AppRegistrationChoiceCode=";
    public static final String kConstantCreatorApproval = "&ApprovedByEmployee=";
    public static final String kConstantCreatorTempID = "&TempIDNumber=";

    public static final String kConstantCreatorGetVisitorId = "VisitorID";
    public static final String kConstantCreatorGetPurpose = "Purpose";
    public static final String kConstantCreatorGetDateOfVisit = "DateOfVisit";
    public static final String kConstantCreatorGetDateOfVisitLocal = "CheckInTimeLocal";
    public static final String kConstantCreatorGetExpectedDate = "ExpectedDateOfVisit";
    public static final String kConstantCreatorGetGMT = "GMTDiffMinutes";
    public static final String kConstantCreatorGetOfficeLocation = "OfficeLocation";
    public static final String kConstantCreatorGetLocationNo = "OfficeLocationCode";
    public static final String kConstantCreatorGetEmployeeName = "EmployeeName";
    public static final String kConstantCreatorGetEmployeeId = "EmployeeID";
    public static final String kConstantCreatorGetEmployeeEmailId = "EmployeeEmailID";
    public static final String kConstantCreatorGetEmployeePhoneNo = "EmployeePhone";
    public static final String kConstantCreatorGetEmployeeExtention = "EmployeeExtension";
    public static final String kConstantCreatorGetCheckInTimeLocal = "CheckInTimeLocal";
    public static final String kConstantCreatorGetSeatingLocation = "EmployeeSeatingLocation";
    public static final String kConstantCreatorGetCodeNo = "AppRegistrationChoiceCode";
    public static final String kConstantCreatorGetApproval = "ApprovedByEmployee";

    public static final String kConstantEmployeeIDAdd = "&employee_id=";
    // PurposeRecordUpload

    public static final String kConstantCreatorPurposeOfVisit = "&VisitPurpose=";
    public static final String kConstantCreatorFrequency = "&Frequency=";
    public static final String kConstantCreatorLocationCode = "&LocationCode=";

    // device Detail Constant Values
    public static final String kConstantCreatorDeviceMake = "&make=";
    public static final String kConstantCreatorDeviceSerialNo = "&serial_number=";
    public static final String kConstantCreatorDeviceVisitorId = "&visitor_id=";
    public static final String kConstantCreatorDeviceVisitId = "&visit_id=";

    public static final String kConstantCreatorInterviewDeviceMake = "&make=";
    public static final String kConstantCreatorInterviewDeviceSerialNo = "&serial_number=";
    public static final String kConstantCreatorInterviewDeviceVisitorId = "&candidate_id=";
    public static final String kConstantCreatorInterviewDeviceVisitId = "&interview_id=";

    // BussinessCard read activity

    public static final String kConstantInvalidQrcode = "Invalid QR Code";
    public static final Integer kConstantBusinessCardImageWidth = 400;
    public static final Integer kConstantBusinessCardImageHeight = 233;
    public static final Integer kConstantCreatorImageWidth = 800;
    public static final Integer kConstantVisitorBadgeWidth = 675;
    public static final String kConstantLicenseFile = "license";
    public static final String kConstantApplicationID = "com.zoho.leads";
    public static final Integer RECOGNITION_CALLBACK_COUNTER_STEP = 15;
    public static final Integer RECOGNITION_CALLBACK_PROGRESS_STEP = 10;
    public static final String licenseFile = "license";
    public static final String applicationID = "Android_ID";
    public static final Integer kConstantTotalTextPerPage = 6;
    public static final Integer kConstantHundred = 100;

    public static final String kConstantPatternsFileExtension = ".mp3";
    public static final String kConstantDictionariesFileExtension = ".mp3";
    public static final String kConstantKeywordsFileExtension = ".mp3";
    public static final Integer kConstantLengthOfMobileNo = 10;
    public static final int kConstantQrcodeRead = 1;
    public static final int kConstantQrcodeUnRead = 0;

    public static final int kConstantPhotoNotUploaded = 0;
    public static final int kConstantPhotoUploaded = 1;
    public static final int kConstantSignatureNotUploaded = 0;
    public static final int kConstantSignatureUploaded = 1;

    public static final int kConstantInterview = 2;
    public static final int kConstantVisitor = 1;

    public static final int invalidNumber = -1;
    public static final Integer kConstantZero = 0;
    public static final Integer kConstantMinusOne = -1;
    public static final Integer kConstantMinusTwo = -2;
    public static final Integer kConstantOne = 1;
    public static final Integer kConstantTwo = 2;
    public static final Integer kConstantThree = 3;
    public static final Integer kConstantFour = 4;
    public static final Integer kConstantFive = 5;
    public static final Integer kConstantSix = 6;
    public static final Integer kConstantSeven = 7;
    public static final Integer kConstantEight = 8;
    public static final Integer kConstantNine = 9;
    public static final Integer kConstantTen = 10;
    public static final Integer kConstantEleven = 11;
    public static final Integer kConstantTwelve = 12;
    public static final Integer kConstantThirteen = 13;
    public static final Integer kConstantFourteen = 14;
    public static final double kConstantthirty = 15.0;

    public static final Integer speachWelcomeMsgCode = 1001;
    public static final Integer speachSmilePleaseMsgCode = 1002;
    public static final Integer speachFaceNotMsgCode = 1003;
    public static final Integer speachAreYouSureCode = 1004;
    public static final Integer speachAreYouSureSaveCode = 1005;

    // TODO
    public static final String kConstantMinusOneString = "-1";

    public static final String RECOGNITION_LANGUAGES_SEPARATOR = ";";
    public static final String ACTION_RECOGNITION_PROGRESS = "com.abbyy.mobile.ocr4.sample.action.RECOGNITION_PROGRESS";
    public static final String EXTRA_RECOGNITION_PROGRESS = "com.abbyy.mobile.ocr4.sample.RECOGNITION_PROGRESS";

    public static final String kConstantFrameBackgroundColor = "#6b6b6b";
    public static final Double kConstantBusinessCardPageLimit = 6.0;
    public static final String kConstantGetLocation = "description";

    // Purpose Of Visit

    public static final Integer kConstantMinimumFontSize = 15;
    public static final Integer kConstantMaximumFontSize = 25;
    public static final Integer kConstantMaximumTextHeight = 200;
    public static final Integer kConstantMaximumTextWidth = 400;

    public static final String kConstantPurposeId = "ID";
    public static final String kConstantVisitFrequency = "Frequency";
    public static final String kConstantLocationCode = "LocationCode";

    public static final Integer kConstantFontSplitCount = 10;

    // Get the codeNo

    public static final Integer kConstantManualEntry = 1;
    public static final Integer kConstantBusinessCardEntry = 2;
    public static final Integer kConstantQrCodeEntry = 3;

    // Get the officeLocation lat and lang

    public static final Double kConstantZohoOfficeIndiaChennaiDLFLatitude = 13.022945;
    public static final Double kConstantZohoOfficeIndiaChennaiDLFLongitude = 80.173468;
    public static final Double kConstantZohoOfficeIndiaChennaiEstanciaLatitude = 12.8275248;
    public static final Double kConstantZohoOfficeIndiaChennaiEstanciaLongitude = 80.0489509;
    public static final Double kConstantZohoOfficeIndiaTenkasiLatitude = 8.902654;
    public static final Double kConstantZohoOfficeIndiaTenkasiLongitude = 77.334899;
    public static final Double kConstantZohoOfficeUSPleasantonLatitude = 37.686197;
    public static final Double kConstantZohoOfficeUSPleasantonLongitude = -121.893848;
    public static final Double kConstantZohoOfficeUSAustinLatitude = 30.2900633;
    public static final Double kConstantZohoOfficeUSAustinLongitude = -97.829593;
    public static final Double kConstantZohoOfficeJapanYokohamaLatitude = 33.5879044;
    public static final Double kConstantZohoOfficeJapanYokohamaLongitude = 130.2624538;
    public static final Double kConstantZohoOfficeChinaBeijingLatitude = 39.975957;
    public static final Double kConstantZohoOfficeChinaBeijingLongitude = 116.335871;
    public static final Double kConstantZohoOfficeIndiaReniguntaLatitude = 13.642954;
    public static final Double kConstantZohoOfficeIndiaReniguntaLongitude = 79.506778;

//    public static final Double kConstantDeveloperTestLatitude = 8.7115714;
//    public static final Double kConstantDeveloperTestLongitude = 77.4185052;

    // Constants of OfficeLocationCode
    public static final int kConstantOfficeLocationCodeIndiaChennaiDLF = 1;
    public static final int kConstantOfficeLocationCodeIndiaChennaiEstancia = 2;
    public static final int kConstantOfficeLocationCodeIndiaTenkasi = 3;
    public static final int kConstantOfficeLocationCodeUSPleasanton = 4;
    public static final int kConstantOfficeLocationCodeUSAustin = 5;
    public static final int kConstantOfficeLocationCodeJapanYokohama = 6;
    public static final int kConstantOfficeLocationCodeChinaBeijing = 7;
    public static final int kConstantOfficeLocationCodeIndiaRenigunta = 8;

    public static final int kConstantDeveloperOfficeLocationCode = 28;

    // Constants of OfficeLocation

    public static final String kConstantOfficeLocationIndiaChennaiEstancia = "Chennai-Estancia, India";
    public static final String kConstantOfficeLocationIndiaChennaiDLF = "Chennai-DLF, India";
    public static final String kConstantOfficeLocationIndiaTenkasi = "Tenkasi, India";
    public static final String kConstantOfficeLocationUSPleasanton = "Pleasanton, USA";
    public static final String kConstantOfficeLocationUSAustin = "Austin, USA";
    public static final String kConstantOfficeLocationJapanYokohama = "Yokohama, Japan";
    public static final String kConstantOfficeLocationChinaBeijing = "Beijing, China";
    public static final String kConstantOfficeLocationIndiaAndhraRenigunta = "Renigunta";
    public static final String kConstantDeveloperOfficeLocation = "Developer Office Location";

    // Constants of visitRecord

    public static final String kConstantVisitId = "ID";
    public static final String kConstantNo = "no";
    public static final String kConstantVisit = "Visit";
    public static final String kConstantempFrequency = "emp_frequency";
    public static final String kConstantEmp_id = "emp_id";
    public static final String kConstantFreqCount = "freq_count";

    // Count secs in Long
    public static final String kConstantCountZero = "0";
    public static final int kConstantDelayFirstPhoto = 500;
    public static final int kConstantDelaySecondPhoto = 1000;
    public static final int kConstantDelayThirdPhoto = 1500;
    public static final String kConstantGetImage = "images";
    public static final int compressQuality = 100;
    public static final int resizedImageWidth = 400;
    public static final int kConstantResizedImageSize = 200;
    public static final int resizedImageHeight = 400;
    public static final int visitorImageHeight = 450;
    public static final int visitorImageWidth = 800;
    public static final int employeeImageWidth = 120;
    public static final int employeeImageHeight = 120;
    public static final int visitorThumbImageHeight = 113;
    public static final int visitorThumbImageWidth = 200;
    // public static final int visitorBadgeImageWidth = 720;
    // public static final int visitorBadgeImageHeight = 1280;
    public static final int jsonCreateUploadPhoto = 1;
    public static final int jsonUploadJsonData = 2;
    public static final int fromHomeButton = 1;
    public static final int fromAutoHandler = 2;
    public static final int fromBackpress = 3;
    public static final int fromDialogConfirm = 4;
    public static final int fromSuccess = 5;

    public static final String kConstantDownloadEmployeeImage = "https://contacts.zoho.com/file/download?ID=";
    public static final String kConstantImageSource = "&authtoken=10eba051409724c34298638b2c86c3a9&fs=thumb";
    public static final String kConstantImageSourceOriginal = "&authtoken=10eba051409724c34298638b2c86c3a9";

    public static final int connectionTimeout = 1000 * 20;
    public static final int readTimeout = 1000 * 25;
    public static final int connectionPhotoTimeout = 1000 * 30;
    public static final int readPhotoTimeout = 1000 * 35;
    public static final String user_Agent = "Mozilla/5.0";
    public static final String kConstantUserAgent = "User-Agent";
    public static final String kConstantAcceptLanguage = "Accept-Language";
    public static final String acceptLanguage = "en-US,en;q=0.5";
    public static final String application = creatorLink +creatorFrontdeskOwnerName+"/json/"+creatorVisitorAppName;

    public static final String kConstantViewRecord = creatorLink+"json/"+creatorVisitorAppName+"/view/";

    //    Don't change while Using Copy if DataCenter Employees Not present in Copy
    public static final String applicationOwnerFrontdeskDC = "frontdeskin1dczohocorpcom";
    public static final String applicationNameFrontdeskDC = "visitor-management-dc";

    public static final String applicationFrontdeskDC = creatorLink+applicationOwnerFrontdeskDC+"/json/"+applicationNameFrontdeskDC;
    public static final String kConstantDataCenterViewRecord = creatorLink+"json/"+applicationNameFrontdeskDC+"/view/";
    public static final String kConstantFrontdeskDCAuthTokenAndScope = "authtoken=2faf8e1fc1ac11b9e7d37717ab9559a9&scope=creatorapi";
    public static final String kConstantFrontdeskDCRawAndOwnerName = kConstantRaw+"&zc_ownername="+applicationOwnerFrontdeskDC;
    public static final String kConstantFileUploadAppNameAndFormNameAppJsonDetailFrontdeskDC = creatorAppLinkName+applicationNameFrontdeskDC+"&formname=app_upload_data";
    public static final String kConstantFileSharedByFrontdeskDC = "&sharedBy="+applicationOwnerFrontdeskDC;
    public static final String kConstantUpdateFrontdeskDCVisitDetail = "/form/data_centre_visit_details/record/update/";
    public static final String kConstantAddFrontdeskDCVisitRecord = "/form/data_centre_visit_details/record/add/";
    public static final String kConstantAddFrontdeskDCDevicePasswordRequest = "/form/device_password_request_form/record/add/";
    public static final String kConstantFileUploadAppNameAndFormNameInVisitDetailFrontDeskDc = creatorAppLinkName+applicationNameFrontdeskDC+"&formname=data_centre_visit_details";
    public static final String kConstantUpdateFrontdeskDCVisitorDetail = "/form/visitor/record/update/";
    public static final String kConstantExitingVisitorReport = "/existing_visitor_report_for_android?";

    //    Don't change while Using Copy if Stationery Details  Not present in Copy
    public static final String kConstantViewStationeryApp = creatorLink+"json/"+creatorStationeryAppName+"/view/";

    public static final String kConstantViewCreatorEmployeeCommon = creatorLink+"json/"+creatorEmployeeCommonAppName+"/view/";
    public static final String kConstantViewCreatorForgetID = creatorLink+"json/"+creatorTemporaryIDAppName+"/view/";

    public static final String applicationStationeryApp = creatorLink+creatorFrontdeskOwnerName+"/json/"+creatorStationeryAppName+"/";
    public static final String kConstantAddDeviceInterviewRecord = "/form/interview_laptop_details/record/add/";
    public static final String kConstantViewInterviewRecord = creatorLink+"json/zohonow-interview/view/";
    public static final String interviewApplication = creatorLink+ creatorFrontdeskOwnerName+"/json/zohonow-interview";
    public static final String kConstantUtilityViewRecord = creatorLink+"json/utility/view/";
    public static final String kConstantUtilityApplication = creatorLink+creatorFrontdeskOwnerName+"/json/utility";


    public static final String kConstantInterviewCandidateReport = "candidate_report?";
    public static final String kConstantRawAndOwnerName = kConstantRaw+"&zc_ownername="+creatorFrontdeskOwnerName;

    public static final String kConstantViewInterviewRecordCriteria = "&criteria=LocationCode==";
    public static final String kConstantCriteria = "&criteria=ID==";
    public static final String kConstantUpdateVisitCriteria = "&criteria=VisitorID==";
    public static final String kConstantUpdateStationeryRequestCriteria = "&criteria=request_id==";
    public static final String kConstantCriteriaJsonData = "&criteria=ID==";
    public static final String kConstantVisitorWithoutMail = "&is_execute_without_mail=";
    public static final String kConstantVisitorJsonData = "&visitor_json_data=";
    public static final String kConstantInterviewOrVisitor = "&interview_or_visitor=";
    public static final String kConstantIsPhotoUpload = "&visitor_photo_uploaded_done=";
    public static final String kConstantIsSignUpload = "&visitor_signature_uploaded_done=";
    public static final String kConstantEncode = "UTF-8";
    public static final String kConstantIsUpload = "&UploadCompleted=";
    public static final String kConstantTrue = "true";
    public static final String kConstantVisitorDetailCriteriaInMobileNo = "&criteria=Phone==\"";
    public static final String kConstantVisitorDetailCriteriaInMailId = "&criteria=EmailID==\"";
    public static final String kConstantDoubleQuotes = "\"";
    public static final String kConstantAddVisitorDetails = "/form/Visitor/record/add/";
    public static final String kConstantAddAppJsonDetails = "/form/app_upload_data/record/add/";
    public static final String kConstantUpdateAppJsonDetails = "/form/app_upload_data/record/update/";
    public static final String kConstantAddDevideRecord = "/form/visitor_visit_laptop_details/record/add/";

    public static final String kConstantAddVisitRecord = "/form/Visit/record/add/";
    public static final String kConstantAddStationeryListRecord = "/form/StationeryList/record/add/";
    public static final String kConstantAddStationeryEntryRecord = "/form/StationeryItemEntry/record/add/";
    public static final String kConstantAddStationeryRequestRecord = "/form/StationeryRequest/record/add/";
    public static final String kConstantAddStationeryRequestFailureDetail = "/form/LogRecords/record/add/";
    public static final String kConstantUpdateVisitorDetail = "/form/Visitor/record/update/";
    public static final String kConstantUpdateVisitDetail = "/form/Visit/record/update/";

    public static final String kConstantViewDataCenterEmployeeDetails = "production_datacenter_employee_reports?"+kConstantFrontdeskDCAuthTokenAndScope+kConstantRaw+"&zc_ownername=frontdeskin1dczohocorpcom";
    public static final String kConstantViewDataCenterEmployeePreApprovalDetails = "production_data_center_visitor_pre_approval_report?"+kConstantFrontdeskDCAuthTokenAndScope+kConstantRaw+"&zc_ownername=frontdeskin1dczohocorpcom&visitor_pre_approval_id.date_of_visit=";
    public static final String kConstantViewVisitorDetail = "Visitor_Report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName;
    public static final String kConstantViewVisitDetail = "Visit_Report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName;
    public static final String kConstantViewStationeryListReport = "StationeryList_Report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName+"&is_item_active=true";
    public static final String kConstantViewNewStationeryListReport = "stationery_list_report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName+"&stationery_name.is_item_active=true";
    public static final String kConstantViewStationeryEntryReport = "StationeryItemEntry_Report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName;
    public static final String kConstantViewStationeryRequestReport = "StationeryRequest_Report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName;

    public static final String kConstantViewEmployeeForgetIDNotCheckOut = "employee_forget_id_not_checked_out?"+kConstantCreatorAuthTokenAndScope+rawAndOwnerName;
    public static final String kConstantViewEmployeeExtensionReport = "employee_extension_report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName+"&is_active=true";

    public static final String kConstantImgPatternFormat = "<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";

    public static final String kConstantUpdateStationeryRequestDetail = "/form/StationeryRequest/record/update/";
    // Interview
    public static final String kConstantViewInterviewDetail = "candidate_report?"+ kConstantCreatorAuthTokenAndScope +rawAndOwnerName;

    // Stationery
    public static final String kConstantAddCandidateRecord = "/form/Candidate/record/add/";
    public static final String kConstantUpdateCandidateDetail = "/form/Candidate/record/update/";
    public static final String kConstantAddInterviewRecord = "/form/Interview/record/add/";
    public static final String kConstantUpdateInterviewDetail = "/form/Interview/record/update/";
    public static final String kConstantInterviewDetailCriteriaInMobileNo = "&criteria=phone==\"";
    public static final String kConstantInterviewDetailCriteriaInMailId = "&criteria=email_id==\"";
    public static final String kConstantInterviewIsUpload = "&upload_completed=";
    public static final String kConstantCreatorCandidateName = "&name=";

    // public
    public static final String kConstantCreatorCandidateLocation = "&location=";
    public static final String kConstantCreatorCandidateMobileNo = "&phone=";
    public static final String kConstantCreatorCandidateEmail = "&email_id=";
    public static final String kConstantCreatorCandidateLaptopMacID = "&Laptop_Mac_ID=";
    public static final String kConstantCreatorCandidate = "Candidate";
    // Interview Constant Values
    public static final String creatorInterviewId = "&candidate_id=";
    public static final String creatorInterviewcheckInTimeGmt = "&checkin_time_gmt=";
    public static final String creatorInterviewcheckInTimeLocal = "&checkin_time_local=";
    public static final String creatorInterviewExpectedDateGmt = "&expected_date_of_visit_gmt=";
    public static final String creatorInterviewExpectedDateLocal = "&expected_date_of_visit_local=";
    public static final String creatorInterviewLocationCode = "&office_location_code=";
    public static final String creatorInterviewEmployeeName = "&employee_name=";
    public static final String creatorInterviewEmployeeId = "&employee_id=";
    public static final String creatorInterviewEmployeeZuid = "&employee_zuid=";
    public static final String creatorInterviewEmployeeEmailId = "&employee_email_id=";
    public static final String creatorInterviewEmployeePhoneNo = "&employee_phone=";
    public static final String creatorInterviewEmployeeExtention = "&employee_extension=";
    public static final String creatorInterviewSeatingLocation = "&employee_seating_location=";
    public static final String creatorInterviewCodeNo = "&app_registration_choice_code=";
    public static final String creatorInterviewApproval = "&approved_by_employee=";
    public static final String creatorInterviewTempID = "&temporary_id=";

    public static final String creatorOTPUploadJsonFormat = "&otp_json_details=";
    public static final String mConstantOTPRequestDate="otp_request_date";
    public static final String kconstantVisitorOTPDetails="visitor_otp_details";
    public static final String creatorAppPasswordUploadJsonFormat = "&password_json_data=";

    public static final String fileUploadAppNameAndFormNameInInterviewDetail = creatorAppLinkName+"zohonow-interview&formname=Interview";
    public static final String kConstantFileUploadFieldNameInInterviewImage = creatorFieldLinkName+"candidate_photo&recordId=";
    public static final String kConstantFileUploadFieldNameInInterviewSign =creatorFieldLinkName+"candidate_signature&recordId=";
    // validation
    public static final String kConstantValidText = "[A-Za-z. ]+";
    public static final String kConstantValidEmail = "^[A-Za-z0-9._%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}$";
    public static final String kConstantValidEmailText = "[a-zA-Z0-9@._-]+";
    public static final String kConstantValidMailOrMobileNo = "[a-zA-Z]";
    public static final String kConstantValidCompanyName = "[a-zA-Z0-9. ]+";
    public static final String kConstantValidAllCharacterswithoutSmiley = "[a-zA-Z0-9. |?*<\":>+\\[\\]/' \\n]+";
    public static final String kConstantRemoveSpecialCharacter = "[|?*<\":>+\\[\\]/' ]";
    public static final String kConstantValidPhoneNo = "[0-9+. ()-]{6,15}";
    public static final String kConstant_Valid_phone_no = "[7-9][0-9]{9}";
    public static final String kConstantValidIndianPhoneNo = "[0-9+.]{9}";
    public static final String kConstantValidPhoneNoText = "[0-9+. ()-]+";
    public static final String kConstantValidIndianPhoneNoText = "[0-9+.]{9}";
    public static final String kConstantValidEmployeeNoText = "[\\d /]+";
    public static final String kConstantDateTimeHalfFormat = "yyyy-MM-dd";
    public static final String kConstantDateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    public static final String peopleCheckInDateFormat = "dd/MM/yyyy HH:mm:ss";
    public static final String creatorDateFormat = "dd-MMM-yyyy HH:mm:ss";
    public static final String creatorDateFormatForgetID = "yyyy-MM-dd HH:mm:ss";
    public static final String creatorUTCDateFormat = "Etc/UTC";
    public static final String kConstantDate = "Select Date";
    public static final String kConstantTime = "Select Time";
    public static final String creatorPhotoName = "_photo";
    public static final String creatorSignatureName = "_signature";

    // Constants for phoneNo
    public static final String kConstantTenkasiStdCode = "914633";
    public static final String kConstantIndiaCode = "91";
    public static final String kConstantChennaiStdCode = "9144";

    public static final String ForgotIDApplication = creatorLink+"frontdesk10/json/"+creatorTemporaryIDAppName;
    public static final String kConstantFileUploadAppNameAndFormNameInForgotID = creatorAppLinkName+creatorTemporaryIDAppName+"&formname=TemporaryID";
    public static final String kConstantFileUploadFieldNameInEmployeePhoto = creatorFieldLinkName+"EmployeePhoto&recordId=";
    public static final String kConstantAddForgotID = "/form/TemporaryID/record/add/";
    public static final String kConstantUpdateForgotID = "/form/TemporaryID/record/update/";
    public static final String downloadImageURL = "https://creator.zoho.com/DownloadFileFromMig.do?";
    public static final String filePath = "&filepath=/";

    // Constants for forgotID Table fields
    public static final String viewLinkName = "&viewLinkName=";
    public static final String reportName = "StationeryList_Report";
    public static final String sharedBy = "&sharedBy=";
    public static final String sharedByValue = "frontdesk10";

    // creator image download
    public static final String criteria = "&criteria=";
    public static final String forgotIDEmployeeID = "&EmployeeID=";
    public static final String forgotIDEmployeeName = "&EmployeeName=";
    public static final String forgotIDEmployeePhone = "&EmployeePhone=";
    public static final String forgotIDEmployeeEmailID = "&EmployeeEmailID=";
    public static final String forgotIDEmployeeExtension = "&Extension=";
    public static final String forgotIDEmployeeDateOfRequest = "&DateOfRequest=";
    public static final String forgotIDEmployeeDateOfRequestGMT = "&DateOfRequestGMT=";
    public static final String forgotIDEmployeeDateOfIssued = "&IssueDate=";
    public static final String forgotIDEmployeePhoto = "&Photo=";
    public static final String forgotIDEmployeeTemporaryIDCardNumber = "&TemporaryID=";
    public static final String forgotIDEmployeeLocationCode = "&OfficeLocation=";
    public static final String forgotIDEmployeeSeatingLocation = "&EmployeeLocation=";
    public static final String forgotIDEmployeeLocation = "&EmployeeLocation=";
    public static final String forgotIDEmployeeReturned = "&Returned=";
    public static final String forgotIDEmployeeUploadCompleted = "&UploadCompleted=";
    public static final String forgotIDId = "ID";

    public static final String creatorID = "&ID=";

    public static final String forgotIDCheckInErrorCode = "&error_code=";
    public static final String kConstantCriteriaIncidentNumber = "&criteria=ID==";

    // Mail format
    public static final String mailType = "message/rfc822";
    public static final String mailSend = "Send Email";
    public static final String mailID = "frontdesk@zohocorp.com";
    public static final String errorCode = "Error Code : ";
    public static final String mailSubject = "ZohoNow App : Error in visitor registration";
    public static final String sentFrom = "Sent From Zoho One App";
    public static final String peopleCheckInURL = "https://people.zoho.com/people/api/attendance";
    public static final String peopleRecordsDownloadURL = "https://people.zoho.com/people/fetchPeopleContacts.do?";
    public static final String peopleStartingIndex = "&sIndex=";
    public static final String peopleRecordsLimit = "&limit=";
    public static final String creatorStartingIndex = "&startindex=";
    public static final String creatorRecordsLimit = "&limit=";

    // Singleton constants for menu selection
    public static final int visitorSingletonCode = 333;
    public static final int employeeSingletonCode = 111;
    public static final int employeeSingletonCodeTenkasi = 444;
    public static final int interviewSingletonCode = 222;

    public static final int peopleResponseSuccessCode = 1000;
    public static final int peopleResponseDateFormatError = 1001;
    public static final int peopleResponseAuthTockenError = 1002;
    public static final int peopleResponseOtherError = 1003;
    public static final int peopleResponseNetworkError = 1004;
    public static final int noForgotRecordIDError = 1005;
    public static final int forgotIDUploadCode = 1006;
    public static final int forgotIdNetworkError = 1007;
    public static final int printBadgeCode = 1008;
    public static final int reportStatusPrinterFailure = 1009;
    public static final int reportNoItemSelection = 1011;
    public static final int reportStatusWifiPrinterFailure = 1010;
    public static final Integer employeeIDDialog = 7;
    public static final Integer reportStatusStationeryItemUploadFailureError = 1012;
    public static final Integer reportCheckInCheckOutFailureError = 1022;
    public static final int forgotIDDialog = 11;
    public static final int printBadgeDialog = 21;
    public static final int badgeDialog = 31;
    public static final int printerIssue = 41;
    public static final int checkOutProcessing = 91;
    public static final int checkInProcessing = 92;
    public static final int selectAtleastOneItem = 51;
    public static final int uploadStationeryFailure = 61;
    public static final int uploadCheckInOutFailure = 71;
    public static final int dialogPasswordBadge = 81;

    public static final int tellUsCode = 999;
    public static final String socketTimeoutException = "SocketTimeoutException";

    public static final int employeeFreqReqLimit = 100;
    public static final int peopleApiRecordLimit = 2000;
    public static final int creatorRecordLimit = 2000;
    public static final int kConstantTabDuration = 3000;
    public static final int oldVisitDataDeleteDay = -2;

    public static final String interViewPurposeOnly = "INTERVIEW";
    public static final String interViewPurpose = "INTERVIEW CANDIDATE";
    public static final String visitorPurpose = "VISITOR";
    public static final String HRDepartment = "HR - ";
    public static final String HRDepartmentAnotherNamePlural = "Human Resources";
    public static final String HRDepartmentAnotherNameSingular = "Human Resource";

    public static final int stationeryItemMaximumCount = 10;

    // the visitor visiting purpose is in the following cases
    // public static final int visitorPurposeInterviewCode = 1;
    // public static final int visitorPurposeOfficialCode = 2;
    // public static final int visitorPurposePersonalCode = 3;
    public static final String kConstantPhotoFileFormat = "%s.jpg";

    public static final String sharedPreferences = "sharedPreferenceStationeryDetails";

    public static final String kConstantLogPageDateFormat = "yyyy/MM/dd HH:mm:ss Z";
    //public static int responseCount = 0;
    public static final String kConstantLogPageErrorCode = "Error Code : ";
    public static final String kConstantLogPageCode = "Code : ";

    //	public static String backgroundProcessVolleyError = "";
    public static final String kConstantLogPageWifiSignalStrength = "WiFi Signal Strength : ";
    public static final String kConstantLogPageWifidBm = " dBm";
    public static final String kConstantFileName = "screenshot_logs_page.jpg";

    // Boolean values
    public static final Boolean boolTrue = true;
    public static final Boolean boolFalse = false;
    // color values
    //public static final String color_code_333 = "#333333";
    public static final String color_code_efefef = "#efefef";

    // Json formation for store data to creator
    public static final String jsonVisitorName = "'visitor_name'";
    public static final String jsonVisitorPhone = "'visitor_phone'";
    public static final String jsonVisitorOTPRequestPhone = "visitor_phone";
    public static final String jsonVisitorEmail = "'visitor_email'";
    public static final String jsonVisitorCompany = "'visitor_company'";
    public static final String jsonVisitorisPreApproved = "is_visitor_pre_approved";
    public static final String jsonVisitorOldPurpose = "'visitor_old_purpose'";
    public static final String jsonVisitorVisitPurpose = "'visitor_purpose'";
    public static final String jsonVisitorCheckInTime = "'visitor_check_in_time'";
    public static final String jsonVisitorOfficeLocationCode = "'visitor_office_location_code'";
    public static final String jsonVisitorOfficeLocation = "'visitor_office_location'";
    public static final String jsonVisitorPhoto = "'visitor_photo'";
    public static final String jsonVisitorSignature = "'visitor_signature'";
    public static final String jsonVisitorTempId = "'visitor_temp_id'";
    public static final String jsonVisitorDeviceSerial = "'visitor_laptop_serial'";
    public static final String jsonVisitorDeviceMake = "'visitor_laptop_make'";
    public static final String jsonVisitorOtherDeviceDetails = "'other_device_details'";
    public static final String jsonVisitorApptoval = "'visitor_approval'";
    public static final String jsonVisitorAppRegistration = "'visitor_app_registration'";
    public static final String jsonVisitorExpectedVisitDate = "'visitor_expected_visit_date'";
    public static final String jsonVisitorExtDevice = "'visitor_ext_device'";
    public static final String jsonAppVersion = "'app_version'";
    public static final String jsonDeviceLocation = "'device_location'";
    public static final String jsonEmployeeName = "'employee_name'";
    public static final String jsonEmployeePhone = "'employee_phone'";
    public static final String jsonEmployeeEmail = "'employee_email'";
    public static final String jsonEmployeeId = "'employee_id'";
    public static final String jsonEmployeeSeatingLocation = "'employee_seating_location'";
    public static final String jsonEmployeeExtension = "'employee_extension'";
    public static final String jsonEmployeeZuid = "'employee_zuid'";

    public static final int retryMaxCount = 1;
    public static final int barcodeScanRequestCode = 100;

    public static final String barcodeScanFormat = "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A";
    public static final String barcodeScanIntent = "eu.michalu.SCAN";
    public static final String barcodeScanClassName = "com.google.zxing.client.android.CaptureActivity";
    public static final String barcodeScanFormatKey = "SCAN_FORMATS";
    public static final String barcodeScanErrorMsg = "Can't scan Laptop Serial number\nright now!\nError Code:6001";
    public static final int dialogConfirmHome = 101;
    public static final int dialogConfirmSkipEmployee = 102;

    public static final String purposeDataCenter ="DataCenter";
    public static final String purposeInterview = "Interview";
    public static final String purposeOfficial = "Visitor / Official";
    public static final String purposePersonal = "Guest / Personal";
    public static final String purposeGadgetSpecialist = "Gadget Specialist";
    public static final String purposeBankingExecutive = "Banking Executive";
    public static final String purposeAuditor = "Auditor";
    public static final String purposeHealthCareExpert = "Health Care Expert";
    public static final String purposeDoctor = "Doctor";
    public static final String purposePress = "Press";
    public static final String purposePartner = "Partner";
    public static final String purposeGovernment = "Government Official";

    public static final String putExtraFromPage = "from_page";

    public static final int fromSummary = 201;
    public static final int fromContact = 202;
    public static final int maxLengthUnTracedEmployee = 20;
    public static final int maxLengthTracedEmployee = 100;

    public static final String selectLocation = "Select Location";
    public static final String spKeyLocation = "location";
    public static final String spKeyDeviceLocation = "device_location";
    public static final String spKeyOfficeLocation = "office_location";
    public static final String spKeyOfficeLocationCode = "office_location_code";
    public static final String spKeyApplicationMode = "application_mode";
    public static final String spKeyNormalMode = "normal_mode";
    public static final String appColorThemeCode = "app_theme_code";
    public static final String appSettingsPassword = "app_settings_password";
    public static final String spKeyEmployeeExtension = "employee_extension";

    //	public static final int fromHaveDevice = 203;
//	public static final int fromDeviceDetails = 204;

    //	public static final String extDeviceOne = "CD/DVD";
//	public static final String extDeviceTwo = "Pen Drive";
//	public static final String extDeviceThree = "Memory Card";
//	public static final String extDeviceFour = "Hard Disk";
//	public static final String extDeviceFive = "Tablet";

    public static final String kConstantDeviceLocationOne = "Main Building / Tower";
    public static final String kConstantDeviceLocationTwo = "Plaza - North";
    public static final String kConstantDeviceLocationThree = "Woodlands";

    public static final String kConstantDeviceLocationFour = "Interview 1";
    public static final String kConstantDeviceLocationFive = "Interview 2";
    public static final String kConstantDeviceLocationSix = "Tenkasi";
    public static final String kConstantDeviceLocationRenigunta = "Renigunta";
    public static final String kConstantDeviceLocationSeven = "Plaza - South";
    public static final String kConstantDeviceLocationEight = "Lakeview";
    public static final String kConstantDeviceLocationNine = "MLCP";

    public static final String creatorEmployeeForgetIdJsonDetails = "/form/employee_forget_id_cloud_sync/record/add/";

    public static final String kConstantEmployeeIdkey = "employee_id";
    public static final String kConstantEmployeeEmailIdkey = "employee_email_id";
    public static final String kConstantEmployeeNamekey = "employee_name";
    public static final String kConstantEmployeeCheckInTimekey = "check_in_time";
    public static final String kConstantEmployeeCheckOutTimekey = "check_out_time";
    public static final String kConstantEmployeeCheckInTimekeyLong = "check_in_time_long";
    public static final String kConstantEmployeeCheckOutTimekeyLong = "check_out_time_long";
    public static final String kConstantEmployeeOfficeLocationkey = "office_location";
    public static final String kConstantEmployeeTempIdkey = "temp_id";
    public static final String kConstantAppVersion = "app_version";
    public static final String kConstantDeviceID = "device_id";
    public static final String kConstantDeviceName = "device_name";

    public static Long countSecZero = 0L;
    public static Long countSecOne = 1L;
    public static Long countSecTwo = 2L;
    public static Long countSecThree = 3L;
    public static Long countSecFour = 4L;
    public static Long countSecFive = 5L;
    public static Long kConstantMillisInFuture = 2500L;
    public static Double distanceInMiles = 0.5;
    public static int alreadyStartedBackgroundProcess = -1;
    public static int existVisitor = 301;
    public static int newVisitor = 302;
    public static int editVisitor = 303;
    public static int dialogDecision = 11;
    public static int dialogInfo = 12;
    public static int dialogConfirm = 13;
    public static int homePress = 21;
    public static int backPress = 22;
    public static int skipPress = 23;
    public static String locationSplit = "{,}";

    public static String employeeForgetIdJson = "&employee_forget_id_json_data=";
    public static String employeeForgetIdFormName = "employee_forget_id_cloud_sync1";
    public static String kRunningCount = "Running Count";

    public double earthRadius = 3958.75;

    public static String constantOTPRequestDateTime = "otp_request_date_time";
    public static String constantOTPValidity = "otp_validity";
    public static String constantOTPValue = "otp";
    public static String constantOTPrequestMobile = "otp_phone_number";

    public static String kconstantStationeryItemEntry = "StationeryItemEntry";
    public static String kconstantStationeryDeviceID = "device_id";
    public static String kconstantStationeryDeviceLocation ="device_location";
    public static String kconstantStationeryAppVersion = "app_version";
    public static String kconstantStationeryVisitorOfficeLocation = "visitor_office_location";
    public static String kconstantStationeryVistiorOfficeLocationCode = "visitor_office_location_code";

    public static String kConstantDataCentreDateFormat = "dd-MMM-yyyy";

    //intent put extra
    public static String putExtraFromActivity = "from_activity";
    //Key management system
    public static final String kConstantAddKeyList = "/form/key_list/record/add/";
    public static final String kConstantUpdateKeyList = "/form/key_list/record/update/";
    public static final String kConstantAllocateKeys = "/form/key_allocation/record/add/";
    public static final String kConstantDeAllocateKeys = "/form/key_allocation/record/update/";
    public static final String AuthTokenVidyalayaAcc = "041f0572c22702ed8c40ad2c369a53b8";
    public static final String KeyApplicationNameCreator = "keys-management-system";
    //fields

    public static String formNameKeyList = "key_list";
    public static String fieldKeyID = "key_id";
    public static String fieldKeyPhoto = "key_photo";
    public static String fieldKeyType = "key_type";
    public static String fieldSecurityDetails = "security_details";
    public static String fieldFurnitureType = "furniture_type";
    public static String fieldVehicleNumber = "vehicle_number";
    public static String fieldRoomDetails = "room_details";
    public static String fieldOfficeLocation = "office_location";
    public static String fieldLocationInsideOffice = "location_inside_office";
    public static String fieldStatus = "status";
    public static String fieldSpareKeyCount = "spare_key_count";
    public static String fieldIsActive = "is_active";
    public static String fieldDescription = "description";
    //Key_allocation
    public static String formNameKeyAllocation = "key_allocation";
    public static String fieldAllKeyID = "key_id";
    public static String fieldAllOfficeLocation = "office_location";
    public static String fieldAllLocationInsideOffice = "location_inside_office";
    public static String fieldAllAllocatedTime = "allocated_time";
    public static String fieldAllDeAllocatedTime = "de_allocated_time";
    public static String fieldAllReturnedBy = "returned_by";
    public static String fieldAllPersonThumbPhoto = "person_thumb_photo";
    public static String fieldAllPersonName = "person_name";
    public static String fieldAllPersonPhoto = "person_Photo";
    public static String fieldAllEmail = "person_email";
    public static String fieldAllMobileNo = "mobile_no";
    public static String fieldAllPersonSignature = "person_signature";
    public static String fieldAllReturnedPersonName = "returned_visitor_name";
    public static String fieldAllReturnedPersonContact = "returned_person_contact";
    public static final String key_management_post = "https://creator.zoho.com/api/json/keys-management-system/form/app_upload_data/record/add/" +
            "authtoken=a405e2f5b1479bb38f971a99f8a2c599&zc_ownername=frontdesk10&scope=creatorapi&json_data=";
    public static final String key_view_record_url = "https://creator.zoho.com/api/json/keys-management-system/view/key_list_report?authtoken=a405e2f5b1479bb38f971a99f8a2c599&zc_ownername=frontdesk10&scope=creatorapi";
}