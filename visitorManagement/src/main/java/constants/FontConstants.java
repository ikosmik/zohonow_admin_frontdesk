package constants;

public class FontConstants {
	public static String fontConstantJosefinSans="font/JosefinSans-Bold.ttf"; 
	public static String fontConstantJosefinSansReg = "font/JosefinSans-Regular.ttf";
	public static String fontConstantLatoReg= "font/Lato-Reg.ttf";
	public static String fontMontserratReg = "font/Montserrat-Regular.ttf";
	public static String fontConstantLinBiolinum="font/LinBiolinum_R.otf";
    public static String fontConstantLatoRegIta="font/Lato-RegIta.ttf";
    public static String fontConstantProximaNovaReg="font/ProximaNova-Regular.otf";
    public static String fontConstantProximaNovaBold="font/MarkSimonson-ProximaNovaBold.otf";
    public static String fontConstantSourceSansProSemibold="font/SourceSansPro-Semibold.ttf";
    public static String fontConstantSourceSansProbold="font/SourceSansPro-Bold.ttf";
    public static String fontConstantSourceSansProLight="font/SourceSansPro-Light.otf";
    public static String fontConstantSourceSansProBlack="font/SourceSansPro-Black.ttf";
    public static String fontConstantBlackJerk="font/black_jack.ttf";
    public static String fontConstantJournal="font/journal.ttf";
    public static String fontConstantNoteThisReg="font/palitoon.otf";
    public static String fontConstantWorkSans="font/WorkSans-Bold.otf";
    public static String fontConstantCaptureIt="font/capture_it.ttf";
    public static String fontConstantWorkSansSemiBold = "font/WorkSans-SemiBold.ttf";
    
    public static String fontConstantHKGroteskBold = "font/HKGrotesk-Bold.otf";
    public static String fontConstantHKGroteskRegular = "font/HKGrotesk-Regular.otf";
    public static String fontConstantHKGroteskSemiBold = "font/HKGrotesk-SemiBold.otf";
    public static String fontConstantHKGroteskItalic = "font/HKGrotesk-Italic.otf";

    public static String fontConstantPoppinsBold = "font/poppins_bold.ttf";
    public static String fontConstantPoppinsRegular = "font/poppins_regular.ttf";
    public static String fontConstantPoppinsSemiBold = "font/poppins_semibold.ttf";
    public static String fontConstantPoppinsMedium = "font/poppins_medium.ttf";
    public static String fontConstantPoppinslight = "font/poppins_light.ttf";

}
