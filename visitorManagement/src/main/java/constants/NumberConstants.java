package constants;

/**
 * Created by Karthick on 14/08/17.
 */

public class NumberConstants {

    public static final int kConstantZero=0;
    public static final int requestAllPermission = 2001;
    public static final int requestExternalPermissionForBG = 2002;
    public static final int requestExternalPermissionForExportDB = 2003;
    public static final int requestExternalPermissionForStorage = 2004;

    public static final long kConstantMinusOneLong = -1;
    public static final int kConstantMinusOne = -1;
    public static final Integer invalidNumber = -1;


    public static final int kConstantBackgroundStarted = 1;
    public static final int kConstantBackgroundStopped = 0;

    public static final Integer kConstantProduction = 02;

    public static Integer connectionTimeout = 1000 * 30;
    public static Integer backgroundProcessTime = 1000 * 60 * 15;

    public static final Integer kConstantSuccessCode = 200;

    public static final Integer kConstantSignatureImageWidth = 400;
    public static final Integer kConstantSignatureImageHeight = 240;

    public static Integer uploadedImageResizedWidth = 50;
    public static Integer uploadedImageResizedHeight = 50;

    public static Integer connectionFileTimeout = 1000 * 100;
    public static Integer connectionFileReadTimeout = 1000 * 120;
    public static Integer maximumMinutes = 60;
    public static Integer readTimeout = 1000 * 25;
    public static Integer kConstantBufferSize = 1024;

    public static Integer kConstantDataCentreMode = 2;
    public static Integer kConstantNormalMode = 1;


    public static final Integer dialogPassword = 10;

//    Datacentre OTP Format
    public static final Integer kConstantOTPValid = 1;
    public static final Integer kConstantOTPExpired = 2;
    public static final Integer kConstantOTPNotValid = 3;
    public static final Integer kConstantOTPResponseEmpty = 4;
    public static final Integer kConstantOTPNotMatch = 5;
}
