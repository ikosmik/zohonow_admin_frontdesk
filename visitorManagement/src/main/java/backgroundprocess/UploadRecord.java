package backgroundprocess;

import java.util.Date;

import appschema.SingletonVisitorProfile;
import zohocreator.VisitorZohoCreator;

import constants.MainConstants;
import database.dbhelper.DbHelper;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

public class UploadRecord extends Service{
	private static final String KEY = null;
	VisitorZohoCreator creator = new VisitorZohoCreator(this);
	DbHelper database = new DbHelper(this);
	String contact;

	@Override
	public IBinder onBind(Intent intent) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void onCreate() {
		//Toast.makeText(this, "Service was Created", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// Perform your long running operations here.
		// Execute the background process
		Date date=new Date();
		//Log.d("window", "date:"+date.toString());
		UploadCreatorRecord upload = new UploadCreatorRecord();
		upload.execute();
		 //Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onDestroy() {
		//super.onDestroy();
		// Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
	}

	/*
	 * Created by Jayapriya On 30/09/2014 Call AsyncTask to upload details into
	 * the creator.
	 */
	// Async Task Class
	class UploadCreatorRecord extends AsyncTask<String, String, String> {

		// Search records in creator and upload new records
		@Override
		protected String doInBackground(String... f_url) {

			try {
				//Date date=new Date();
				//Log.d("window", "date1:"+date.toString());
				//creator.searchPurposeRecord();
				 //date=new Date();
				//Log.d("window", "date1:"+date.toString());
               if((SingletonVisitorProfile.getInstance().getCodeNo() == MainConstants.kConstantQrCodeEntry))
                {

                    creator.updateCreatorInQrcode();
                }
                else {
                    creator.searchVisitorRecord(getApplicationContext());

                }
				
				//checkVisitorRecordInLocal();
				/*
				 * ArrayList<VisitRecord> visitRecord =
				 * database.getVisitRecordListInOffMode(); if(visitRecord.size()
				 * > MainConstants.ConstantZero){ for(int i=
				 * MainConstants.ConstantZero; i< visitRecord.size(); i++){
				 * String purpose = visitRecord.get(i).getPurpose();
				 * creator.searchPurposeRecordInWifiOn(purpose);
				 * 
				 * } }
				 */

				// else
				// if(utility.Validation.validateMail(visitorRecord.get(i).getMailId())
				// == true){
				// contact = visitorRecord.get(i).getMailId();
				// creator.searchVisitorRecordInWifiOn(contact,
				// listOfVisitors);
				// }

			} catch (Exception e) {
				// Log.e("Error: ", e.getMessage());
			}
			return null;
		}

	}

	/*private void checkVisitorRecordInLocal() {

		ArrayList<VisitorRecord> visitorRecord = database
				.getVisitorRecordListInOffMode();
        ArrayList<VisitorVisitLinkRecord> visitorVisitList=database.getAllVisitorVisitLinkRecord();
        Log.d("visitor","visitId" +visitorVisitList.size());
		if (visitorRecord.size() > MainConstants.kConstantZero) {

			//new UploadCreatorRecordWhenWifiOn().execute(visitorRecord);
			
			//uploadVisitorRecordInCreatorWhenWifiOn(visitorRecord);
			//uploadPurposeRecordInCreatorWhenWifiOn();

		}

		// else
		// if(utility.Validation.validateMail(visitorRecord.get(i).getMailId())
		// == true){
		// contact = visitorRecord.get(i).getMailId();
		// creator.searchVisitorRecordInWifiOn(contact,
		// listOfVisitors);
		// }

	}

	

	/*
	 * Created by jayapriya On 5/12/2014 upload the purpose record in creator when
	 * the wifi on.
	 */

	/*private void uploadPurposeRecordInCreatorWhenWifiOn() {
		ArrayList<VisitRecord> visitRecordList = database
				.getAllVisitRecordListInOffMode();
			creator.searchPurposeRecordInWifiOn(visitRecordList);
			
		
	}
	
/*
 * Created by jayapriya On 5/12/2014 upload the Visitor record in creator when wifi on.
 */
	/*private void uploadVisitorRecordInCreatorWhenWifiOn(
			ArrayList<VisitorRecord> visitorRecord) {

		ArrayList<HashMap<String, String>> listOfVisitors = new ArrayList<HashMap<String, String>>();
		if (visitorRecord.size() > MainConstants.kConstantZero) {
			for (int i = MainConstants.kConstantZero; i < visitorRecord.size(); i++) {

				HashMap<String, String> mapVisitorRecord = new HashMap<String, String>();

				//Log.d("VisitorRecord", "getVisitorRecordName"
						//+ visitorRecord.get(i).getName());
				//Log.d("VisitorRecord", "getVisitorRecordAddress"
						////+ visitorRecord.get(i).getAddress());
				//Log.d("VisitorRecord", "getVisitorRecordMailId"
						//+ visitorRecord.get(i).getMailId());
				//Log.d("VisitorRecord", "getVisitorRecordMobileNo"
					//	+ visitorRecord.get(i).getMobileNo());
				//Log.d("VisitorRecord", "getVisitorRecordCompanyWebsite"
						//+ visitorRecord.get(i).getCompanyWebsite());
				//Log.d("VisitorRecord", "getVisitorRecordBusinessCard"
						//+ visitorRecord.get(i).getBusinessCard());
				mapVisitorRecord.put(DataBaseConstants.visitorId, visitorRecord
						.get(i).getVisitorIdInOffMode().toString());
				mapVisitorRecord.put(DataBaseConstants.name,
						visitorRecord.get(i).getName());
				mapVisitorRecord.put(DataBaseConstants.location, visitorRecord
						.get(i).getAddress());
				mapVisitorRecord.put(DataBaseConstants.visitorMailId,
						visitorRecord.get(i).getMailId());
				mapVisitorRecord.put(DataBaseConstants.visitorMobileNo,
						visitorRecord.get(i).getMobileNo());
				mapVisitorRecord.put(DataBaseConstants.visitorCompany,
						visitorRecord.get(i).getCompany());
				mapVisitorRecord.put(DataBaseConstants.visitorCompanyWebsite,
						visitorRecord.get(i).getCompanyWebsite());
				mapVisitorRecord.put(DataBaseConstants.visitorDesignation,
						visitorRecord.get(i).getDesignation());
				mapVisitorRecord.put(DataBaseConstants.visitorFax,
						visitorRecord.get(i).getFax());
				mapVisitorRecord.put(DataBaseConstants.visitorBusinessCard,
						visitorRecord.get(i).getBusinessCard());
				// selectedStationeryListFinal.put(visitorRecord, visitorRecord.get(i));
				// Log.d("hashMap"," " +selectedStationeryListFinal.get(visitorRecord) );
				listOfVisitors.add(mapVisitorRecord);
				//Log.d("mapvalue", "hashMap" + listOfVisitors.size());

			}

			//Log.d("mapvalue", "hashMap" + listOfVisitors.size());
			if (listOfVisitors.size() > MainConstants.kConstantZero) {

				for (int j = 0; j < listOfVisitors.size(); j++) {

					creator.searchVisitorRecordInWifiOn(listOfVisitors);

				}
			}
		}
	}
	/*
	// Async Task Class

		class UploadCreatorRecordWhenWifiOn extends AsyncTask<ArrayList<VisitorRecord>, String, String> {

			@Override
			protected String doInBackground(ArrayList<VisitorRecord>... params) {
				//ArrayList<VisitorRecord> visitorRecord = new ArrayList<VisitorRecord>();
				ArrayList<VisitorRecord> visitorRecordList = params[0];
				uploadPurposeRecordInCreatorWhenWifiOn();
				uploadVisitorRecordInCreatorWhenWifiOn(visitorRecordList);
				return null;
			}

		}
		*/
		

}
