package backgroundprocess;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;

import com.zoho.app.frontdesk.android.R;

import constants.MainConstants;
import zohocreator.VisitorZohoCreator;

public class CreatorForgetIDSync extends BroadcastReceiver {

    private VisitorZohoCreator creatorVisitorDetail ;

    public void onReceive(Context context, Intent intent)
    {
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        String applicationMode = context.getResources().getString( R.string.device_mode );
        if(applicationMode!=null && !applicationMode.isEmpty() && applicationMode.equalsIgnoreCase("Normal Mode")) {
            if (creatorVisitorDetail == null) {
                creatorVisitorDetail = new VisitorZohoCreator(context);
            }
            GetEmployeeForgetIDNotCheckOut getEmployeeForgetIDNotCheckOut = new GetEmployeeForgetIDNotCheckOut();
            if (getEmployeeForgetIDNotCheckOut != null) {
                getEmployeeForgetIDNotCheckOut.execute();
            }
        }
    }

    /**
     * Get existing visitor details from cloud
     *
     * @author Karthick
     * @created 20170525
     */
    class GetEmployeeForgetIDNotCheckOut extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
            creatorVisitorDetail.getEmployeeForgetIDNotCheckOutDetails(
                    MainConstants.kConstantViewCreatorForgetID
                            + MainConstants.kConstantViewEmployeeForgetIDNotCheckOut
                    ,"ForgetIdNotCheckOutDetails");
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }
}
