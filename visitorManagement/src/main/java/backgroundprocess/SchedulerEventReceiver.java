/**
 *Created by Jayapriya
 * Date 21/8/2014
 */

package backgroundprocess;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import utility.WriteErrorToCSV;

import constants.ErrorConstants;
import constants.MainConstants;


import backgroundprocess.GetAllRecords;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zoho.app.frontdesk.android.ApplicationController;

/**
 * 
 * @author jayapriya on 21/8/2014
 * Activate the broadCastReceiver to launch the backgroundProcess
 */
public class SchedulerEventReceiver extends BroadcastReceiver {
	private static final String APP_TAG = "com.hascode.android";

	public void onReceive(final Context ctx, final Intent intent) {
		deleteLastWeekCSVData();
		Intent eventService = new Intent(ctx, GetAllRecords.class);
		ctx.startService(eventService);
	}
	/**
	 * Function to delete Last week CSV ErrorLog Data
	 * 
	 * @author Shiva
	 * @created 12/10/2016
	 */
	public static void deleteLastWeekCSVData() {
		if(ApplicationController.getInstance()!=null && ApplicationController.getInstance().checkExternalPermission()){

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			String LastWeekDateString = dateFormat.format(cal.getTime());
			Date LastweekDate = new Date();
			try {
				LastweekDate = dateFormat.parse(LastWeekDateString);
			} catch (ParseException e1) {
			}
			File fileDir = new File(MainConstants.kConstantFileDirectory);
			String fileName = ErrorConstants.errorCodeCSV;
			String TempfileName = ErrorConstants.errorCodeCSVTemp;
			String line;
			String CSVDate;
			File file = new File(fileDir, fileName);
			File TempFile = new File(fileDir, TempfileName);
			if (file.exists()) {
				try {
					FileInputStream fileInputStream = new FileInputStream(file);
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
					try {
						bufferedReader.readLine();
						while ((line = bufferedReader.readLine()) != null) {
							String[] RowData = line.split(",");
							CSVDate = RowData[MainConstants.kConstantZero];
							java.util.Date dt = new java.util.Date(CSVDate);
							String CSVDateFormatString = new SimpleDateFormat("dd-MM-yyyy").format(dt);
							try {
								Date CSVDateFormat = dateFormat.parse(CSVDateFormatString);
								if (CSVDateFormat.compareTo(LastweekDate) > 0) {
									WriteErrorToCSV.writeTempData(CSVDate, RowData[MainConstants.kConstantOne], RowData[MainConstants.kConstantTwo]);
								}
							} catch (ParseException e) {
							}
						}
						if (TempFile.renameTo(file)) {
							TempFile.delete();
						}
					} catch (IOException e) {
					}
				} catch (FileNotFoundException e) {
				}
			}
		}
	}

}
