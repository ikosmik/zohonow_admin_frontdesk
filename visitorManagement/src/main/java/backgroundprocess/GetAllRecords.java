package backgroundprocess;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageReq;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.zoho.app.frontdesk.android.ApplicationController;
import com.zoho.app.frontdesk.android.R;

import constants.NumberConstants;
import constants.StringConstants;
import utility.JSONParser;

import logs.InternalAppLogs;

import constants.ErrorConstants;
import constants.MainConstants;
import database.dbschema.Stationery;
import database.dbhelper.DbHelper;
import database.dbschema.EmployeeFrequency;
import database.dbschema.PurposeOfVisitRecord;
import database.dbschema.ZohoEmployeeRecord;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Bitmap;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import appschema.ExistingVisitorDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utility.NetworkConnection;
import utility.UsedBitmap;
import zohocreator.VisitorZohoCreator;
import zohocreator.ZohoPeopleZohoEmployeeDetails;

/**
 * Its a background process when the app will be install first time this service
 * class will be started. And then daily 4 O'Clock it will start service. This
 * process get existing visitor detail, visit purpose in the zoho creator. And
 * get the employee details into the people.
 *
 * @author jayapriya
 * @created 18/08/2014
 */
public class GetAllRecords extends Service {

    private LocationManager locationManager;
    private LocationListener locationListener;
    Intent intent;
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private VisitorZohoCreator creatorVisitorDetail = new VisitorZohoCreator(
            this);
    private ZohoPeopleZohoEmployeeDetails creatorEmployeeDetails = new ZohoPeopleZohoEmployeeDetails(
            this);
    private UsedBitmap usedBitmap = new UsedBitmap();
    private DbHelper database = new DbHelper(this);
    private JSONParser jsonParser;
    private JSONArray jsonArray;

    // RequestQueue mRequestQueue;

    /**
     * Service Handler class handle the service process to start the service.
     */
    public final class ServiceHandler extends Handler {
        // RequestQueue mRequestQueue;
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        // Normally we would do some work here, like download a file.
        // For our sample, we just sleep for 2 seconds.
        public void handleMessage(Message msg) {

            long endTime = System.currentTimeMillis() + 2 * 1000;

            while (System.currentTimeMillis() < endTime)
                synchronized (this) {
                    try {
                        try {
                            wait(endTime - System.currentTimeMillis());
                        } catch (InterruptedException e) {
                        }
                        String applicationMode = getResources().getString( R.string.device_mode );
//						 Log.d("window","background started");
//						UploadCreatorRecord upload = new UploadCreatorRecord();
//						upload.execute();
//						Log.d("DSK","Collect Background Button Call");
                        if(applicationMode!=null && !applicationMode.isEmpty()) {
                            if(applicationMode.equalsIgnoreCase(StringConstants.kStringConstantsDeviceModeDataCenter)) {
                                getPreApprovalVisitorDetails preApprovalVisitorDetails = new getPreApprovalVisitorDetails();
                                preApprovalVisitorDetails.execute();
                            }
                            if(applicationMode.equalsIgnoreCase(StringConstants.kStringConstantsDeviceModeNormal)) {
                                getStationeryList itemList = new getStationeryList();
                                itemList.execute();
                                getNewStationeryList newItemList = new getNewStationeryList();
                                newItemList.execute();
                            }
                            GetEmployeeIDExtensionDetails getEmployeeIDExtensionDetails = new GetEmployeeIDExtensionDetails();
                            getEmployeeIDExtensionDetails.execute();
                            getEmployeeDetails employee = new getEmployeeDetails();
                            employee.execute();
                            if(applicationMode.equalsIgnoreCase(StringConstants.kStringConstantsDeviceModeDataCenter)) {
                                getDataCenterEmployeeList dataCenterEmployee = new getDataCenterEmployeeList();
                                dataCenterEmployee.execute();
                            }
                            if(applicationMode.equalsIgnoreCase(StringConstants.kStringConstantsDeviceModeNormal)) {
                                getEmployeeFrequency employeeFrequency = new getEmployeeFrequency();
                                employeeFrequency.execute();
                                GetExistingVisitor getExistingVisitor = new GetExistingVisitor();
                                getExistingVisitor.execute();
                            }
                            if (checkPermission()) {
                                File backupDB = new File(MainConstants.kConstantDBExportedPath);
                                if (backupDB != null && backupDB.exists()) {
                                    backupDB.delete();
                                }
                            }
                        }
                    } catch (SQLiteConstraintException e) {
                        // Log.d("window","background" +e);
                        continue;
                    }
                }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1);
        }
    }

    /**
     * It is a implementation of the Service class that allows applications to
     * bind to it and interact with it
     *
     * @param intent
     * @return
     */

    public IBinder onBind(Intent intent) {
        return null;
    }

    // Create background process here.

    public void onCreate() {
        super.onCreate();
        intent = new Intent("BROADCAST_ACTION");
        HandlerThread thread = new HandlerThread("ServiceStartArguments");

        thread.start();
        getCurrentLocationCode();
        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        // Log.d("window","handleMessage");
    }

    // For each start request, send a message to start a job and deliver the
    // start ID so we know which request we're stopping when we finish the
    // job

    public int onStartCommand(Intent intent, int flags, int startId) {

        // mRequestQueue = Volley.newRequestQueue(this);
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        getCurrentLocationCode();
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    /*
     * Get currentLocation
     *
     * @author jayapriya
     *
     * @created 18/08/2014
     */
    private void getCurrentLocationCode() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new utility.CompareLocationListener();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 500, 10, locationListener);
            sendBroadcast(intent);
        }
    }

    /**
     * The class will be finished then destroy it.
     */
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
            // mRequestQueue.stop();
            // mRequestQueue=null;
        }
    }

    public boolean stopService(Intent name) {
        return super.stopService(name);
    }

    /**
     * @author jayapriya
     * @created 27/02/2015
     */
    class UploadCreatorRecord extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        // Search records in creator and upload new records

        protected String doInBackground(String... f_url) {

//			getStationeryList itemList = new getStationeryList();
//			itemList.execute();
//			getEmployeeDetails employee = new getEmployeeDetails();
//			employee.execute();
//			getEmployeeFrequency employeeFrequency = new getEmployeeFrequency();
//			employeeFrequency.execute();
//			GetExistingVisitor getExistingVisitor = new GetExistingVisitor();
//			getExistingVisitor.execute();

            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Aysnc task get visitPurposeDetail in creator
     *
     * @author jayapriya
     * @created 11/03/2015
     */
    class getPurposeDetail extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... params) {
            // Get purpose of Visit
            ArrayList<PurposeOfVisitRecord> purposeOfVisitList = new ArrayList<PurposeOfVisitRecord>();
            purposeOfVisitList = creatorVisitorDetail
                    .getPurposeOfVisitorRecord();
            if ((purposeOfVisitList != null)
                    && (purposeOfVisitList.size() > MainConstants.kConstantZero)) {
                database.deletePurposeOfVisit();
                database.insertPurposeOfVisit(purposeOfVisitList);

            }
            // }
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Aysnc task get employeeDetail in people
     *
     * @author jayapriya
     * @created 11/03/2015
     * @modified Karthikeyan D S 06/06/2018
     * For DataCenter Employee Filter set if Employee is DataCenter Employee,
     * set field IsDataCenterEmployee is true as value 1.
     */
    class getEmployeeDetails extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... params) {

            database.createNewEmployeeTable();
            // Get the all Employee records in creator
            ArrayList<ZohoEmployeeRecord> arrayListOfEmployee = new ArrayList<ZohoEmployeeRecord>();
            try {
                arrayListOfEmployee = creatorEmployeeDetails
                        .getAllEmployeeRecord();

            } catch (TimeoutException e) {

            } catch (IOException e) {

            } catch (JSONException e) {

            }
            Calendar calander = Calendar.getInstance();
            Calendar date = null;
            long installed;
            try {
                //Get application installed  date
                installed = getPackageManager()
                        .getPackageInfo(getPackageName(), 0)
                        .firstInstallTime;
                date = Calendar.getInstance();
                date.setTimeInMillis(installed);
            } catch (NameNotFoundException e) {
                date = Calendar.getInstance();
            }
            File fileDirectory = new File(MainConstants.kConstantFileDirectory
                    + MainConstants.kConstantFileEmployeeImageFolder);
            boolean isHaveEmpFolder = false;
            if (fileDirectory != null && fileDirectory.exists()) {
                isHaveEmpFolder = true;
            }
            //Employee photo get weekly once
            if (isHaveEmpFolder == false || (date != null && calander != null && calander.get(Calendar.DATE) == date.get(Calendar.DATE) || (calander.get(Calendar.HOUR_OF_DAY) == Calendar.WEDNESDAY && (calander.get(Calendar.DATE) % 7 == 0)))) {

                if ((arrayListOfEmployee != null)
                        && (arrayListOfEmployee.size() > 0)) {
                    downloadVisitorImage();
                } else {
                }
            }
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Aysnc task get employeeDetail in people
     *
     * @author jayapriya
     * @created 11/03/2015
     */
    class getEmployeeFrequency extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {

            String url = MainConstants.kConstantViewRecord
                    + MainConstants.kConstantEmpFreqReport
                    + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantRawAndOwnerName;

            sendEmpFreqRequest(url, "EmployeeFrequency");

            url = MainConstants.kConstantViewInterviewRecord
                    + MainConstants.kConstantEmpFreqReport
                    + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantRawAndOwnerName;

            sendEmpFreqRequest(url, "EmployeeFrequency");

            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Get existing visitor details from cloud
     *
     * @author Karthick
     * @created 20170525
     */
    class GetExistingVisitor extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
            getExistingVisitor();
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Get existing visitor details from cloud
     *
     * @author Karthick
     * @created 20170525
     */
    class GetEmployeeForgetIDNotCheckOut extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
            creatorVisitorDetail.getEmployeeForgetIDNotCheckOutDetails(
                    MainConstants.kConstantViewCreatorForgetID
                    + MainConstants.kConstantViewEmployeeForgetIDNotCheckOut
                    ,"ForgetIdNotCheckOutDetails");
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }
    /**
     * Get existing visitor details from cloud
     *
     * @author Karthick
     * @created 20170525
     */
    class GetEmployeeIDExtensionDetails extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
            creatorVisitorDetail.getEmployeeExtensionFromCreatorCommon(
                    MainConstants.kConstantViewCreatorEmployeeCommon
                            + MainConstants.kConstantViewEmployeeExtensionReport
                    ,"GetEmployeeExtensionDetails");
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }
    /**
     * Get Stationery Details By sending Volley request
     *
     * @author jayapriya
     * @created 11/03/2015
     * @Modified by Karthikeyan D S
     */
    class getStationeryList extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
//			creatorVisitorDetail.sendVolleyRequest(
//					MainConstants.kConstantViewStationeryApp
//							+ MainConstants.kConstantViewStationeryListReport,
//					"StationeryList");
            creatorVisitorDetail.sendVolleyRequest(
                    MainConstants.kConstantViewStationeryApp
                            + MainConstants.kConstantViewStationeryEntryReport,
                    "StationeryEntry");
            creatorVisitorDetail
                    .sendVolleyRequest(
                            MainConstants.kConstantViewStationeryApp
                                    + MainConstants.kConstantViewStationeryRequestReport,
                            "StationeryRequest");

            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Async task to get stationery Details by Sending VolleyRequest
     * <p>
     * currently Using
     *
     * @author Karthikeyan D S
     * @created 25/05/2018
     */
    class getNewStationeryList extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
//			Log.d("DSKNewStationeryList ","NewStationeryList");
            creatorVisitorDetail.sendVolleyRequest(
                    MainConstants.kConstantViewStationeryApp
                            + MainConstants.kConstantViewNewStationeryListReport,
                    "NewStationeryList");
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Async task get stationery Details on SendVolleyRequest
     *
     * @author Karthikeyan D S
     * @created 25/05/2018
     */
    class getDataCenterEmployeeList extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
//			Log.d("DSKNewStationeryList ","NewStationeryList");
            creatorVisitorDetail.sendEmployeeDatacenterRequestVolleyRequest(
                    MainConstants.kConstantDataCenterViewRecord
                            + MainConstants.kConstantViewDataCenterEmployeeDetails,
                    "DataCenterEmployeeList");
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Async task to get employee Pre-Approval Visitor Details For Data Center by Sending VolleyRequest
     *
     * @author Karthikeyan D S
     * @created 29JUN2018
     */
    class getPreApprovalVisitorDetails extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(final String... data) {
//			Log.d("DSK","VisitorPreApprovalDetails");
            String mDateOfVisit = getTodayDateValueasString();
            creatorVisitorDetail.sendEmployeeVisitorPreApprovalRequestVolleyRequest(
                    MainConstants.kConstantDataCenterViewRecord
                            + MainConstants.kConstantViewDataCenterEmployeePreApprovalDetails
                            + mDateOfVisit,
                    "VisitorPreApprovalDetails");
            return null;
        }

        protected void onPostExecute(String file_url) {

        }
    }

    /**
     * Download employee image from the Zoho Contacts.
     *
     * @author karthick
     * @created 20150819
     */
    private void downloadVisitorImage() {
        ArrayList<ZohoEmployeeRecord> zohoEmployeeRecordList=null;
        ArrayList<ZohoEmployeeRecord> zohoEmployeeRecords=null;
        if (checkPermission()) {
            File fileDirectory = new File(MainConstants.kConstantFileDirectory
                    + MainConstants.kConstantFileEmployeeImageFolder);

            if (!fileDirectory.exists()) {
                fileDirectory.mkdirs();
            }
            DbHelper sqliteHelper = new DbHelper(this);
             zohoEmployeeRecordList = new ArrayList<ZohoEmployeeRecord>();
             zohoEmployeeRecords = new ArrayList<ZohoEmployeeRecord>();
             zohoEmployeeRecords = sqliteHelper.getEmployeeList();
            if(zohoEmployeeRecords!=null && !zohoEmployeeRecords.isEmpty()) {
                zohoEmployeeRecordList.addAll(zohoEmployeeRecords);
                for (int i = 0; i < zohoEmployeeRecordList.size(); i++) {
                    sendVolleyRequestImage(zohoEmployeeRecordList.get(i)
                                    .getEmployeeZuid(),
                            MainConstants.kConstantDownloadEmployeeImage
                                    + zohoEmployeeRecordList.get(i).getEmployeeZuid()
                                    + MainConstants.kConstantImageSource);
                }
            }
            if(zohoEmployeeRecords!=null)
            {
                zohoEmployeeRecords=null;
            }
            if(zohoEmployeeRecordList!=null)
            {
                zohoEmployeeRecordList=null;
            }
        }

    }

    /**
     * Download employee image through volley
     *
     * @author karthick
     * @created 20150819
     */
    private void sendVolleyRequestImage(String employeeZUID, String photoUrl) {
        ImageReq request = new ImageReq(photoUrl,
                new Response.Listener<Bitmap>() {
                    public void onResponse(final Bitmap bitmap, String id) {
//                        Log.d("DSK ","bitmap "+bitmap.getByteCount());
                        if (bitmap.getByteCount() != 10506)
                        {
                            usedBitmap
                                    .storeBitmap(
                                            bitmap,
                                            MainConstants.kConstantFileDirectory
                                                    + MainConstants.kConstantFileEmployeeImageFolder
                                                    + id
                                                    + MainConstants.kConstantImageFileFormat);
                        } else {
                            getEmployeePhoto(id);
                        }
                    }

                }, 0, 0,  Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
            public void onErrorResponse(final VolleyError error,
                                        String id) {
                setInternalLogs(
                        getApplicationContext(),
                        ErrorConstants.backgroundProcessDownloadImageVolleyError,
                        error.getLocalizedMessage() );
            }
        }, employeeZUID);

        ApplicationController.getInstance().getRequestQueue().add(request);

    }

    /**
     * Download employee image through volley
     *
     * @author karthick
     * @created 20150819
     */
    private void sendVolleyRequestPeopleImage(String employeeZUID,
                                              String photoUrl) {
        ImageReq request = new ImageReq(photoUrl,
                new Response.Listener<Bitmap>() {
                    public void onResponse(final Bitmap bitmap, String id) {

                        usedBitmap.storeBitmap(
                                bitmap,
                                MainConstants.kConstantFileDirectory
                                        + MainConstants.kConstantFileEmployeeImageFolder
                                        + id
                                        + MainConstants.kConstantImageFileFormat);
                    }

                }, 0, 0, null, new Response.ErrorListener() {
            public void onErrorResponse(final VolleyError error,
                                        String id) {
                setInternalLogs(
                        getApplicationContext(),
                        ErrorConstants.backgroundProcessDownloadImageVolleyError,
                        error.getLocalizedMessage());

            }
        }, employeeZUID);

        ApplicationController.getInstance().getRequestQueue().add(request);

    }

    /**
     * Download employee image from the Zoho People.
     *
     * @author karthick
     * @created 20150819
     */
    private void getEmployeePhoto(String employeeZUID) {

        if (checkPermission()) {
            DbHelper sqliteHelper = new DbHelper(this);
            ZohoEmployeeRecord zohoEmployeeRecord = sqliteHelper
                    .getEmployeeDetails(employeeZUID);

            if (zohoEmployeeRecord != null
                    && zohoEmployeeRecord.getEmployeePhoto() != null
                    && !zohoEmployeeRecord.getEmployeePhoto().isEmpty()) {
                sendVolleyRequestPeopleImage(employeeZUID,
                        zohoEmployeeRecord.getEmployeePhoto()
                                + MainConstants.kConstantPeopleAuthToken);
            }
        }
    }

    /**
     * Download employee image from the Zoho People.
     *
     * @author jayapriya
     * @created 13/03/2015
     */
//	class DownloadImage extends AsyncTask<String, String, String> {
//
//		int total;
//		int count = 0;
//
//		protected String doInBackground(final String... zuid) {
//
//			for (int i = MainConstants.kConstantZero; i < zuid.length; i++) {
//
//				File fileDirectory = new File(
//						MainConstants.kConstantFileDirectory
//								+ MainConstants.kConstantFileEmployeeImageFolder);
//
//				if (!fileDirectory.exists()) {
//					fileDirectory.mkdirs();
//				}
//
//				ImageReq request = new ImageReq(
//						MainConstants.kConstantDownloadEmployeeImage + zuid[i]
//								+ MainConstants.kConstantImageSource,
//						new Response.Listener<Bitmap>() {
//
//							public void onResponse(final Bitmap bitmap,
//									final String id) {
//								usedBitmap
//										.storeBitmap(
//												bitmap,
//												MainConstants.kConstantFileDirectory
//														+ MainConstants.kConstantFileEmployeeImageFolder
//														+ id
//														+ MainConstants.kConstantImageFileFormat);
//							}
//
//						}, 0, 0, null, new Response.ErrorListener() {
//							public void onErrorResponse(
//									final VolleyError error, String id) {
//
//							}
//						}, zuid[i]);
//				ApplicationController.getInstance().getRequestQueue()
//						.add(request);
//
//			}
//			return null;
//		}
//	}
    public void sendEmpFreqRequest(String url, String responseCode) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            public void onResponse(String response, String reponseID) {
                JSONArray jsonArray;
                JSONObject jsonObject;
                if (reponseID.equals("EmployeeFrequency")) {
                    if ((response != null)
                            && (response != MainConstants.kConstantEmpty)) {
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject != null) {
                                jsonArray = jsonObject
                                        .getJSONArray(MainConstants.kConstantempFrequency);
                                // Log.d("creator",
                                // "jsonArray" + jsonArray.length());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    EmployeeFrequency employeeFrequency = new EmployeeFrequency();
                                    employeeFrequency
                                            .setEmployeeID(jsonArray
                                                    .getJSONObject(i)
                                                    .getString(
                                                            MainConstants.kConstantEmp_id));
                                    employeeFrequency
                                            .setVisitFrequency(jsonArray
                                                    .getJSONObject(i)
                                                    .getLong(
                                                            MainConstants.kConstantFreqCount));
                                    if (employeeFrequency != null) {
                                        database.updateEmployeeVisitFrequency(employeeFrequency);

                                    }
                                }
                            }
                        } catch (JSONException e) {

                        }
                    }
                } else if (reponseID.equals("StationeryList")) {
                    ArrayList<Stationery> itemList = creatorVisitorDetail
                            .processStationery(response);
                    database.deleteAllStationery();
                    database.insertBulkStationeryRecords(itemList);
                } else if (reponseID.equals("NewStationeryList")) {
                    ArrayList<Stationery> newItemList = creatorVisitorDetail
                            .newProcessStationeryList(response);
                    database.deleteAllNewStationery();
                    database.insertBulkNewStationeryRecords(newItemList);
                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error,
                                        String employeeID) {

            }
        }, responseCode);

        ApplicationController.getInstance().getRequestQueue()
                .add(stringRequest);
    }

    /**
     * Get existing visitor details from creator
     *
     * @author Karthick
     * @created 20170525
     */
    private void getExistingVisitor() {
        ArrayList<ExistingVisitorDetails> arrayListOfEmployee = new ArrayList<ExistingVisitorDetails>();
        JSONParser jsonParser = new JSONParser();
        int startIndex = 0;
        if (database == null) {
            database = DbHelper.getInstance(this);
        }
        if (database != null) {
            database.deleteAllExistVisitor();
        }
        do {
            arrayListOfEmployee.clear();
            try {
                String response = creatorVisitorDetail.getExistingVisitorDetails(startIndex, MainConstants.creatorRecordLimit);
                ArrayList<ExistingVisitorDetails> existingVisitorDetailsList = jsonParser.parseExitVisitorDetails(response);
//				Log.d("size", "size"+existingVisitorDetailsList.size());
                if (database != null && existingVisitorDetailsList != null && arrayListOfEmployee != null) {
                    arrayListOfEmployee.addAll(existingVisitorDetailsList);
//					database.insertBulkVisitorRecords(arrayListOfEmployee);
                    for (int index = 0; index < arrayListOfEmployee.size(); index++) {
                        database.insertExistVsitor(arrayListOfEmployee.get(index));
                    }
                }
            } catch (MalformedURLException e) {

            } catch (ProtocolException e) {

            } catch (IOException e) {

            } catch (JSONException e) {

            }
            startIndex = startIndex + MainConstants.creatorRecordLimit;
        }
        while (arrayListOfEmployee != null && arrayListOfEmployee.size() == MainConstants.creatorRecordLimit);
    }

    /*
     * set internal log and insert to local sqlite
     *
     *
     * @created On 20150910
     *
     * @author karthick
     */
    public void setInternalLogs(Context context, int errorCode,
                                String errorMessage) {
        // SingletonErrorMessage.getInstance().setErrorMessage(errorMessage);
        MainConstants.alreadyStartedBackgroundProcess =NumberConstants.kConstantBackgroundStopped;
        NetworkConnection networkConnection = new NetworkConnection();
        InternalAppLogs internalAppLogs = new InternalAppLogs();
        internalAppLogs.setErrorCode(errorCode);
        internalAppLogs.setErrorMessage(errorMessage);
        internalAppLogs.setLogDate(new Date().getTime());
        internalAppLogs.setWifiSignalStrength(networkConnection
                .getCurrentWifiSignalStrength(context));
        DbHelper.getInstance(context).insertLogs(internalAppLogs);
    }

    /**
     * Check the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    /**
     * Get todays Date Value as string and return to api call to fetch details
     *
     * @Author Karthikeyan D S
     * @Created on 25JUL2018
     */
    private String getTodayDateValueasString() {
        SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = new Date();
        String mdateofvisit = currentDate.format(todayDate);
        return mdateofvisit;
    }

}
