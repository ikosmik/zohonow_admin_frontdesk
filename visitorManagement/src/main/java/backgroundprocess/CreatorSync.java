package backgroundprocess;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import appschema.VisitorDetailsDatabase;

import appschema.SharedPreferenceController;
import constants.NumberConstants;
import constants.StringConstants;
import utility.UsedCustomToast;
import zohocreator.VisitorZohoCreator;
import constants.MainConstants;
import database.dbhelper.DbHelper;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.widget.Toast;

/**
 * Upload visitor photo & signature to cloud
 * Then delete unwanted entries in cloud
 * 
 * @author Karthick
 * @created on 20161114
 */
public class CreatorSync extends BroadcastReceiver {

	private DbHelper database;
	private ArrayList<String> unCompleteJsonDataList;
	private VisitorZohoCreator visitorZohoCreator;
	private backgroundProcess backgroundProcess;
	private Context context;
	public void onReceive(Context context, Intent arg) {
		if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		//Log.d("CreatorSync","CreatorSync");
		visitorZohoCreator = new VisitorZohoCreator(context);
		database = new DbHelper(context);
		if(database!=null) {
			database.deleteOldVisitor();
		}
		backgroundProcess = new backgroundProcess();
		if(backgroundProcess!=null) {
			backgroundProcess.execute();
		}
		this.context=context;
//		 Toast.makeText(
//					 context,
//					 "Creator Photo sync"
//					 , Toast.LENGTH_LONG).show();
		 
//		unCompleteJsonDataList = new ArrayList<String>();
//		if(unCompleteJsonDataList!=null && database!=null) {
//			unCompleteJsonDataList.addAll(database.getUnCompletedVisitor());	
//		
//		for(int i=0;i<unCompleteJsonDataList.size();i++){
//			unCompleteJsonDataList.get(i);
//		}
//		}
	}
	
	/**
	 * Get visitor photo upload url
	 * 
	 * @param cloudId
	 * 
	 * @author Karthick
	 * @created on 20161114
	 */
	private String getFilePhotoUploadUrl(String cloudId, String purpose){
//		if(purpose.equals(MainConstants.interViewPurpose)) {
//			return MainConstants.kConstantFileUploadUrl
//					+ MainConstants.kConstantCreatorAuthTokenAndScope
//					+ MainConstants.kConstantFileUploadInterviewAppNameAppJsonDetail
//					+ MainConstants.kConstantFileUploadFieldName
//					+ cloudId
//					+ MainConstants.kConstantFileAccessTypeAndName
//					+ cloudId
//					+ MainConstants.creatorPhotoName
//					+ MainConstants.kConstantImageFileFormat
//					+ MainConstants.kConstantFileSharedBy;
//		}
		return MainConstants.kConstantFileUploadUrl
		+ MainConstants.kConstantCreatorAuthTokenAndScope
		+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetail
		+ MainConstants.kConstantFileUploadFieldName
		+ cloudId
		+ MainConstants.kConstantFileAccessTypeAndName
		+ cloudId
		+ MainConstants.creatorPhotoName
		+ MainConstants.kConstantImageFileFormat
		+ MainConstants.kConstantFileSharedBy;
	}
	
	/**
	 * Get visitor signature upload url
	 * 
	 * @param cloudId
	 * 
	 * @author Karthick
	 * @created on 20161114
	 */
	private String getFileSignUploadUrl(String cloudId, String purpose){
//		if(purpose.equals(MainConstants.interViewPurpose)) {
//			return MainConstants.kConstantFileUploadUrl
//					+ MainConstants.kConstantCreatorAuthTokenAndScope
//					+ MainConstants.kConstantFileUploadInterviewAppNameAppJsonDetail
//					+ MainConstants.kConstantFileUploadFieldJSONInVisitorSign
//					+ cloudId
//					+ MainConstants.kConstantFileAccessTypeAndName
//					+ cloudId
//					+ MainConstants.creatorSignatureName
//					+ MainConstants.kConstantImageFileFormat
//					+ MainConstants.kConstantFileSharedBy;
//		}
		return MainConstants.kConstantFileUploadUrl
		+ MainConstants.kConstantCreatorAuthTokenAndScope
		+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetail
		+ MainConstants.kConstantFileUploadFieldJSONInVisitorSign
		+ cloudId
		+ MainConstants.kConstantFileAccessTypeAndName
		+ cloudId
		+ MainConstants.creatorSignatureName
		+ MainConstants.kConstantImageFileFormat
		+ MainConstants.kConstantFileSharedBy;
	}

	/**
	 * Get visitor photo upload url
	 *
	 * @param cloudId
	 *
	 * @author Karthick
	 * @created on 20161114
	 */
	private String getFilePhotoUploadUrlFrontDesk(String cloudId, String purpose){

		return MainConstants.kConstantFileUploadUrl
				+ MainConstants.kConstantFrontdeskDCAuthTokenAndScope
				+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetailFrontdeskDC
				+ MainConstants.kConstantFileUploadFieldName
				+ cloudId
				+ MainConstants.kConstantFileAccessTypeAndName
				+ cloudId
				+ MainConstants.creatorPhotoName
				+ MainConstants.kConstantImageFileFormat
				+ MainConstants.kConstantFileSharedByFrontdeskDC;
	}

	/**
	 * Get visitor signature upload url
	 *
	 * @param cloudId
	 *
	 * @author Karthick
	 * @created on 20161114
	 */
	private String getFileSignUploadUrlFrontDesk(String cloudId, String purpose){
		return MainConstants.kConstantFileUploadUrl
				+ MainConstants.kConstantFrontdeskDCAuthTokenAndScope
				+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetailFrontdeskDC
				+ MainConstants.kConstantFileUploadFieldJSONInVisitorSign
				+ cloudId
				+ MainConstants.kConstantFileAccessTypeAndName
				+ cloudId
				+ MainConstants.creatorSignatureName
				+ MainConstants.kConstantImageFileFormat
				+ MainConstants.kConstantFileSharedByFrontdeskDC;
	}

	/*
	 * background process for upload visitor details
	 */
	public class backgroundProcess extends AsyncTask<String, String, String> {
		SharedPreferenceController sharedPreferenceController;
		public backgroundProcess() {

		}

		protected void onPreExecute() {
			super.onPreExecute();

		}

		protected String doInBackground(String... f_url) {
	         sharedPreferenceController = new SharedPreferenceController();
	         int applicationMode= sharedPreferenceController.getApplicationMode(context);
			//Photo upload
			ArrayList<VisitorDetailsDatabase> visitorPhotoNotSyncList = new ArrayList<VisitorDetailsDatabase>();
			visitorPhotoNotSyncList.addAll(database.getVisitorPhotoNotSync());
			for(int i=0;i<visitorPhotoNotSyncList.size();i++){
				String url =MainConstants.kConstantEmpty;
				if(applicationMode==NumberConstants.kConstantDataCentreMode) {
					url = getFilePhotoUploadUrlFrontDesk(visitorPhotoNotSyncList.get(i).getJsonDataId(),visitorPhotoNotSyncList.get(i).getVisitPurpose())  ;
				}
				else
				{
					url = getFilePhotoUploadUrl(visitorPhotoNotSyncList.get(i).getJsonDataId(),visitorPhotoNotSyncList.get(i).getVisitPurpose())  ;
				}
				int uploadImageCode  = visitorZohoCreator.uploadFile(url, visitorPhotoNotSyncList.get(i).getPhotoUrl());
				int updateCode = visitorZohoCreator.updateVisitJsonDataIsUpload(visitorPhotoNotSyncList.get(i).getJsonDataId(),
						MainConstants.kConstantIsPhotoUpload,visitorPhotoNotSyncList.get(i).getVisitPurpose());
				if (uploadImageCode == MainConstants.kConstantSuccessCode && updateCode == MainConstants.kConstantSuccessCode) {
					database.UpdateVisitorPhotoUploaded(visitorPhotoNotSyncList.get(i));
				}
			}
			
			//sign upload
			ArrayList<VisitorDetailsDatabase> visitorSignNotSyncList = new ArrayList<VisitorDetailsDatabase>();
			visitorSignNotSyncList.addAll(database.getVisitorSignNotSync());
			for(int i=0;i<visitorSignNotSyncList.size();i++){
				String url = MainConstants.kConstantEmpty;
				if(applicationMode==NumberConstants.kConstantDataCentreMode) {
					url = getFileSignUploadUrlFrontDesk(visitorSignNotSyncList.get(i).getJsonDataId(), visitorSignNotSyncList.get(i).getVisitPurpose());
				}
				else
				{
					url = getFileSignUploadUrl(visitorSignNotSyncList.get(i).getJsonDataId(), visitorSignNotSyncList.get(i).getVisitPurpose());
				}
				int uploadImageCode  = visitorZohoCreator.uploadFile(url, visitorSignNotSyncList.get(i).getSignatureUrl());
				int updateCode = visitorZohoCreator.updateVisitJsonDataIsUpload(visitorSignNotSyncList.get(i).getJsonDataId(),MainConstants.kConstantIsSignUpload,visitorSignNotSyncList.get(i).getVisitPurpose());
				if (uploadImageCode == MainConstants.kConstantSuccessCode && updateCode == MainConstants.kConstantSuccessCode) {
					database.UpdateVisitorSignUploaded(visitorSignNotSyncList.get(i));
				}
			}
			
			//delete old visitor data
			database.deleteVisitorOldData();
			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 *
		 * **/
		protected void onPostExecute(String responseParam) {
			deleteVisitorPhotos();
//			Log.d("DSK ","responseParam "+responseParam);
			if(context!=null && responseParam!=null ) {
			UsedCustomToast.makeCustomToast(
					context,
					StringConstants.kConstantMessageBackgroundFinished, Toast.LENGTH_LONG)
					.show();
			}
		}
	}
	/**
	 * delete locally stored photos.
	 * 
	 * @author jayapriya
	 * @created on 13/04/16
	 */
	public void deleteVisitorPhotos() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, -5);
		String path = Environment.getExternalStorageDirectory().toString()
				+ "/Frontdesk/Temp";
		// Log.d("Files", "Path: " + path);
		File f = new File(path);
		// File file[] = f.listFiles();
		String[] children = f.list();
		if (children != null && children.length > 0) {
			for (int i = 0; i < children.length; i++) {
				File deletefile = new File(f, children[i]);
				if (calendar.getTime().getTime() > deletefile.lastModified()) {
					deletefile.delete();
				}
			}
		}

		// Log.d("window: ","delete:"+fileDirectory.delete());
		// fileDirectory.mkdirs();
	}

	
}
