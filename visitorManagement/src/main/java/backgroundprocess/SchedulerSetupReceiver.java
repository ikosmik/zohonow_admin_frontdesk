/**
 *Created by Jayapriya
 * Date 21/8/2014
 */
package backgroundprocess;

import utility.UsedAlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 
 * @author jayapriya on 21/08/2014
 * check the alarm manager activation and Activate the
 *         alarmManager
 */
public class SchedulerSetupReceiver extends BroadcastReceiver {
	private static final String APP_TAG = "com.hascode.android";

	private static final int EXEC_INTERVAL = 30 * 1000;

	@Override
	public void onReceive(final Context ctx, final Intent intent) {
		//Log.d(APP_TAG, "SchedulerSetupReceiver.onReceive() called");
		boolean alarmUp = (PendingIntent.getBroadcast(ctx, 0, new Intent(ctx,
				SchedulerEventReceiver.class), PendingIntent.FLAG_NO_CREATE) != null);
		//Log.d(APP_TAG, "Alarm is already active" + alarmUp);
		if (alarmUp) {
			//Log.d(APP_TAG, "Alarm is already active");
			if ("android.intent.action.BOOT_COMPLETED".equals(intent
					.getAction())) {
				UsedAlarmManager.deactivateAlarmManager(ctx);
				UsedAlarmManager.activateAlarmManager(ctx);
			}
		} else {
			//Log.d("APP_TAG", "Alarm is not actived");
			UsedAlarmManager.activateAlarmManager(ctx);			

		}
		
	}
	
	

}
