package utility;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;


/**
 * This class is used to find wether location(GPS) is enable or not
 * 
 * @author Shiva
 * @created 12/10/2016
 */
public class LocationManager {
	
	public static void TurnGPSOn(Context mContext)
	{
		String provider = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if(!provider.contains("gps")){ //if gps is disabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3")); 
	        mContext.sendBroadcast(poke);
	    }
	}
}
