package utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.R;

import java.util.List;

import database.dbschema.Stationery;

/**
 * Created by Karthick on 12/08/17.
 */

public class stationeryListGridViewAdaptor  extends BaseAdapter {
    private Context context;
    private List<Stationery> listOfStationery;
    private Typeface fontJournal, fontAnuDaw;
    private View.OnClickListener listenerStationeryItemSelection;
    public stationeryListGridViewAdaptor(Context context,
                                         List<Stationery> items, Typeface fontJournal,
                                         Typeface fontAnuDaw, View.OnClickListener listenerStationeryItemSelection) {
        // super(context,items);

        this.context = context;
        this.listOfStationery = items;
        this.fontJournal = fontJournal;
        this.fontAnuDaw = fontAnuDaw;
        this.listenerStationeryItemSelection = listenerStationeryItemSelection;
    }

    public class ViewHolder {
        public EditText editTextQuantity;
        public RelativeLayout relativeLayoutIncreaseCount,
                relativeLayoutDecreaseCount;
        public RelativeLayout stationeryLayout;
        public TextView textStationeryName;
        public int isSelected;
        public int stationery_id;

        public ViewHolder getTag() {

            return null;
        }

        public EditText findViewById(int stationeryQuantity) {

            return null;
        }

        // public int selected;
    }

    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        Stationery stationeryItem = listOfStationery.get(position);
        if (listOfStationery.get(position) != null) {
            final ViewHolder view;
            LayoutInflater inflater = ((Activity) context)
                    .getLayoutInflater();
            // View gridViewstationery;
            if (convertView == null) {
                view = new ViewHolder();
                convertView = inflater.inflate(
                        R.layout.grid_view_single_stationery_item, null);
                view.stationeryLayout = (RelativeLayout) convertView
                        .findViewById(R.id.stationery_layout);
                view.textStationeryName = (TextView) convertView
                        .findViewById(R.id.text_stationery_name);
                view.relativeLayoutDecreaseCount = (RelativeLayout) convertView
                        .findViewById(R.id.relativeLayout_stationery_decrease_count);
                view.relativeLayoutIncreaseCount = (RelativeLayout) convertView
                        .findViewById(R.id.relativeLayout_stationery_increase_count);
                view.editTextQuantity = (EditText) convertView
                        .findViewById(R.id.stationery_quantity);
                view.editTextQuantity
                        .setInputType(InputType.TYPE_CLASS_NUMBER);
                view.editTextQuantity.setTypeface(fontAnuDaw);
                view.editTextQuantity.setTextColor(Color.GRAY);
                view.textStationeryName.setTypeface(fontJournal);
                convertView.setTag(view);
            } else {
                view = (ViewHolder) convertView.getTag();
            }
            view.isSelected = position;
            view.stationery_id = stationeryItem.getStationeryCode();

            if ((stationeryItem != null)
                    && (stationeryItem.getStationeryName() != "")) {
                view.textStationeryName.setText(stationeryItem
                        .getStationeryName());
                if ((view.editTextQuantity != null)
                        && (view.editTextQuantity.getText().toString() != "")
                        && (view.editTextQuantity.getText().toString()
                        .length() > 0)
                        && (Integer.parseInt(view.editTextQuantity
                        .getText().toString()) > 0)) {
                    view.textStationeryName.setTextColor(Color.WHITE);
                    view.editTextQuantity.setTextColor(Color.WHITE);
                    view.stationeryLayout.setBackgroundColor(Color.rgb(25,
                            138, 206));
                } else {
                    view.textStationeryName.setTextColor(Color.rgb(86, 86,
                            86));
                    view.editTextQuantity.setTextColor(Color
                            .rgb(86, 86, 86));
                    view.stationeryLayout.setBackgroundColor(Color.WHITE);

                }
            }

            /**
             * Modified by gokul based on selection and deselection of the
             * stationary item the count is increased and color gets changed
             *
             *
             * **/
            view.stationeryLayout.setTag(view);
            if(view.stationeryLayout!=null && listenerStationeryItemSelection!=null) {
                view.stationeryLayout
                        .setOnClickListener(listenerStationeryItemSelection);
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {

        // ////////////Log.d("getcount", "count" +
        // this.listOfStationery.size());
        return this.listOfStationery.size();
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        // Return true for clickable, false for not
        return false;
    }

}

