package utility;

import appschema.SharedPreferenceController;
import constants.NumberConstants;
import zohocreator.VisitorZohoCreator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.zoho.app.frontdesk.android.ActivityVisitorSignature;
import com.zoho.app.frontdesk.android.ApplicationController;
import appschema.SingletonVisitorProfile;

import constants.ErrorConstants;
import constants.MainConstants;

public class BackgroundUploadVisitorData  extends AsyncTask<String, String, String> {

	private int uploadedCode;
	private int jsonRequestCode = MainConstants.jsonCreateUploadPhoto;
	private Context context;
	private VisitorZohoCreator creator = null;
	private SharedPreferenceController sharedPreferenceController;
	private Utility utility = new Utility();
	public BackgroundUploadVisitorData(Context context, int jsonRequestCode) {
		this.jsonRequestCode = jsonRequestCode;
		this.context=context;
		creator = new VisitorZohoCreator(context);
	}

	protected void onPreExecute() {
		super.onPreExecute();
	}

	protected String doInBackground(String... f_url) {

			if ((SingletonVisitorProfile.getInstance() == null
					|| SingletonVisitorProfile.getInstance().getVisitId() == null
					|| SingletonVisitorProfile.getInstance().getVisitId()
							.isEmpty())) {
				uploadedCode = creator.addJsonEmptyRecord(MainConstants.visitorPurpose );
//				Log.d("DSK ","uploadedCodeaddJsonEmptyRecord "+uploadedCode);
			}
			if ((SingletonVisitorProfile.getInstance() != null
					&& SingletonVisitorProfile.getInstance().getVisitId() != null
					&& !SingletonVisitorProfile.getInstance().getVisitId()
							.isEmpty())) {
			if (SingletonVisitorProfile.getInstance() != null
					&& SingletonVisitorProfile.getInstance()
							.getVisitorThumbPhotoUploaded() != 1 
					&& SingletonVisitorProfile.getInstance().getVisitorImage()!=null 
					&& SingletonVisitorProfile.getInstance()
									.getVisitorImage().length>0) {
				UsedBitmap usedBitmap = new UsedBitmap();

//				removed by Karthikeyan D S based on directly upload Original image as full clarity
//				Bitmap bitmap = usedBitmap.decodeBitmap(
//						SingletonVisitorProfile.getInstance()
//								.getVisitorImage(), MainConstants.visitorThumbImageWidth, MainConstants.visitorThumbImageHeight);

				Bitmap bitmap = usedBitmap.decodeBitmap(
					SingletonVisitorProfile.getInstance()
								.getVisitorImage());

				if(bitmap!=null) {
				usedBitmap.storeBitmap(bitmap, context.getFilesDir()
						+ MainConstants.kConstantImageThumbFile);
				//Upload visitor photo to cloud
					String upLoadVisitorImageUri="";
					sharedPreferenceController = new SharedPreferenceController();
					int applicationMode =sharedPreferenceController.getApplicationMode(context);
					if(applicationMode==NumberConstants.kConstantDataCentreMode) {
						upLoadVisitorImageUri = utility.getFileUploadUrlDataCentre();
					}
					else
					{
						upLoadVisitorImageUri = utility.getFileUploadUrl();
					}

				uploadedCode = creator.uploadThupFile(
						upLoadVisitorImageUri, context.getFilesDir()
								+ MainConstants.kConstantImageThumbFile);
//					Log.d("DSK ","uploadedCodeuploadThupFile "+uploadedCode);
				bitmap.recycle();
				}
				if (uploadedCode == MainConstants.kConstantSuccessCode) {
					SingletonVisitorProfile.getInstance().setVisitorThumbPhotoUploaded(MainConstants.kConstantOne);
				}
			}
			if (jsonRequestCode == MainConstants.jsonUploadJsonData) {
				if (SingletonVisitorProfile.getInstance() != null
						&& SingletonVisitorProfile.getInstance().getVisitId() != null
						&& !SingletonVisitorProfile.getInstance().getVisitId()
								.isEmpty() && 
						((SingletonVisitorProfile.getInstance()
						.getVisitorThumbPhotoUploaded() == 1))) {
					uploadedCode = creator
							.updateVisitorJsonRecord(utility.convertVisitorDetailsJsonData(),SingletonVisitorProfile.getInstance().getPurposeOfVisitCode());

//					Log.d("DSK ","uploadedCodeupdateVisitorJsonRecord "+uploadedCode);
				}
			
			if(uploadedCode==ErrorConstants.visitorUpdateJsonDataCreatorError) {
				uploadedCode = creator.updateWithoutMailField();
//				Log.d("DSK ","uploadedCodeupdateWithoutMailField "+uploadedCode);
			}
			}
			}
		return null;
	}

	/**
	 * Updating progress bar
	 * */
	protected void onProgressUpdate(String... progress) {
		// setting progress percentage
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String responseParam) {
		if(SingletonVisitorProfile.getInstance()!=null) {
			SingletonVisitorProfile.getInstance().setUploadedCode(uploadedCode);
		}
		if(jsonRequestCode == MainConstants.jsonUploadJsonData) {
			Intent intent=new Intent(ApplicationController.getInstance(),ActivityVisitorSignature.class);
			NotifyObservable.getInstance().updateValue(intent);
		}
	}
}