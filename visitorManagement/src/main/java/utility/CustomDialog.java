package utility;

import com.zoho.app.frontdesk.android.R;

import constants.FontConstants;
import constants.MainConstants;
import constants.StringConstants;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CustomDialog {

	private Dialog clearlogDataDialog;
	private EditText clearlogDataMsg;
	private TextView clearlogDataTitle;
	private Button clearlogDataYESButton,clearlogDataNOButton;
	private Typeface fontTextview,fontButton;
	/*
	 * Show clear dialog
	 * 
	 * @param context
	 * @param listenerClear
	 * 
	 * Created on 20150722 
	 * Created by karthick
	 */
	public void viewClearDialog(Context context, OnClickListener listenerClear) {
		clearlogDataDialog= new Dialog(context, R.style.TransparentDialog);
		clearlogDataDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		clearlogDataDialog.setContentView(R.layout.dialog_activity);
		clearlogDataDialog.show();
		clearlogDataDialog.setCanceledOnTouchOutside(true);  
		clearlogDataMsg = (EditText) clearlogDataDialog.findViewById(R.id.dialog_message);
		clearlogDataMsg.setSingleLine(false);
		clearlogDataTitle = (TextView) clearlogDataDialog.findViewById(R.id.dialog_title);
	    clearlogDataYESButton = (Button) clearlogDataDialog.findViewById(R.id.dialog_yes_button);
	    clearlogDataNOButton = (Button) clearlogDataDialog.findViewById(R.id.dialog_no_button);
	    clearlogDataMsg.setFocusableInTouchMode(true);
	    clearlogDataMsg.setGravity(Gravity.CENTER);
	    clearlogDataMsg.setBackgroundColor(Color.argb(0, 0, 0, 0));
	    clearlogDataMsg.setText(StringConstants.clearLogMsg);
	    clearlogDataMsg.setFocusable(false);
	    clearlogDataTitle.setVisibility(View.INVISIBLE);
	    fontTextview = Typeface.createFromAsset(context.getAssets(),
				FontConstants.fontConstantSourceSansProSemibold);
	    fontButton = Typeface.createFromAsset(context.getAssets(),
				FontConstants.fontConstantSourceSansProbold);
	    
	    clearlogDataTitle.setTypeface(fontTextview);
	    clearlogDataYESButton.setTypeface(fontButton);
	    clearlogDataNOButton.setTypeface(fontButton);
	    clearlogDataNOButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				clearlogDataDialog.dismiss();
			}
		});
	    clearlogDataYESButton.setOnClickListener(listenerClear);
	}
	/*
	 * Hide dialog
	 * 
	 * Created on 20150722 
	 * Created by karthick
	 */
	public void hideClearDialog() {
		if((clearlogDataDialog!=null)&&(clearlogDataDialog.isShowing())) {
			clearlogDataDialog.dismiss();
		}
	}
}
