package utility;

import constants.FontConstants;
import constants.MainConstants;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.R;

public class UsedDialogBox {
	private static TextView textViewDialogErrorMessage,textViewDialogHomeMessage;
	private static TextView textViewErrorMessageConfirm, textViewHomeMessageConfirm, textViewHomeMessageCancel;
	private static ImageView imageViewErrorMessageConfirm,imageViewHomeMessageConfirm;
	static Dialog dialogGetErrorMessage,  dialogGetHomeMessage;
    private static Typeface typefaceFontSetMessage,typefaceFontSetMessageOk;
	
	public static void intializeDialogBox(Context context, String message) {
		dialogGetErrorMessage = new Dialog(context);
		dialogGetErrorMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogGetErrorMessage
				.setContentView(R.layout.dialog_box_error_message);
		// dialog.(MessageConstant.dialogTitle);
		dialogGetErrorMessage.setCanceledOnTouchOutside(true);
		dialogGetErrorMessage.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		imageViewErrorMessageConfirm =(ImageView) dialogGetErrorMessage.findViewById(R.id.image_error_message_ok);
		textViewDialogErrorMessage = (TextView) dialogGetErrorMessage
				.findViewById(R.id.text_view_error_message);
		textViewErrorMessageConfirm = (TextView) dialogGetErrorMessage
				.findViewById(R.id.text_view_error_message_confirm);
		textViewDialogErrorMessage
				.setText(message);
        typefaceFontSetMessage = Typeface.createFromAsset(context.getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
        typefaceFontSetMessageOk = Typeface.createFromAsset(context.getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
        textViewDialogErrorMessage.setTypeface(typefaceFontSetMessage);
        textViewErrorMessageConfirm.setTypeface(typefaceFontSetMessageOk);
		showErrorDialogBox();
	    imageViewErrorMessageConfirm.setOnClickListener(errorMessageConfirm);

	}

	public static OnClickListener errorMessageConfirm = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// dialogGetErrorMessage.setCanceledOnTouchOutside(true);
			dialogGetErrorMessage.dismiss();
			// callAgain();
			//Log.d("visitor", "finish");
			// finish();
		}
	};
	/*
	 * Created by Jayapriya On 15/11/2014 Show dialogBox
	 */
	public static void showErrorDialogBox( ) {

		if ((dialogGetErrorMessage != null)
				&& (!dialogGetErrorMessage.isShowing())) {
			
			dialogGetErrorMessage.show();
		}
	}
	public void removeDialogBox(){
		dialogGetErrorMessage.dismiss();
	}
	
	
	public static void intializeDialogBoxForHomePage(Context context, String message) {
		dialogGetHomeMessage = new Dialog(context);
		dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogGetHomeMessage
				.setContentView(R.layout.dialog_box_confirm_message);
		// dialog.(MessageConstant.dialogTitle);
		dialogGetHomeMessage.setCanceledOnTouchOutside(true);
		dialogGetHomeMessage.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		imageViewHomeMessageConfirm =(ImageView) dialogGetHomeMessage.findViewById(R.id.image_view_error_message_ok);
		textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message);
		textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_confirm);
		textViewHomeMessageCancel = (TextView) dialogGetHomeMessage.findViewById(R.id.text_view_error_message_cancel);
		textViewDialogHomeMessage
				.setText(message);
		showHomeDialogBox();
		//textViewHomeMessageConfirm.setText(MainConstants.kConstantOk);
		imageViewHomeMessageConfirm.setOnClickListener(HomeMessageConfirm);
		textViewHomeMessageCancel.setOnClickListener(listenerMessageCancel);

	}
	public static void showHomeDialogBox( ) {
Log.d("dsk","dialog1");
		if ((dialogGetHomeMessage != null)
				&& (!dialogGetHomeMessage.isShowing())) {
			Log.d("dsk","dialog2");
			dialogGetHomeMessage.show();
		}
	}
	public static OnClickListener listenerMessageCancel = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// dialogGetErrorMessage.setCanceledOnTouchOutside(true);
			dialogGetHomeMessage.dismiss();
			// callAgain();
			//Log.d("visitor", "finish");
			// finish();
		}
	};
	public static OnClickListener HomeMessageConfirm = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// dialogGetErrorMessage.setCanceledOnTouchOutside(true);
			dialogGetHomeMessage.dismiss();
			// callAgain();
			//Log.d("visitor", "finish");
			 //finish();
		}
	};
	


	
	

}
