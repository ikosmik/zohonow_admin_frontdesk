package utility;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;


public class NetworkConnection {

	/*
	 * Get wifi is connected or not
	 * 
	 * Created on 20150725
	 * Created by karthick
	 */
	public boolean wifiConnected(Context context) {
		if (context!=null) {
			ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (mWifi.isConnected()) {
				return true;
			}
		}
			return false;
		}
	/*
	 * Get wifi is enabled
	 * 
	 * Created on 20150725
	 * Created by karthick
	 */
	public static boolean isWiFiEnabled(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wifiManager.isWifiEnabled();
	}
	
	/*
	 * WiFi reconnect
	 * 
	 * Created on 20150725
	 * Created by karthick
	 */
	public static void wifiReconnect(Context context) {
		
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		wifiManager.setWifiEnabled(true);
		wifiManager.reconnect();
		}
	
	/*
	 * Get wifi signal strength
	 * 
	 * @return wifi signal strength
	 * @created on 20150921
	 * @author karthick
	 */
	public int getCurrentWifiSignalStrength(Context context) {
		//Log.d("context",""+context);
		WifiManager wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		if (wifiManager != null) {
			int rssi = wifiManager.getConnectionInfo().getRssi();
			if(rssi > -80){
				return rssi;
			}
			return 0;
		}
		return 0;
	}
	
	
	public void checkOnlineState(Context context) {
	    ConnectivityManager CManager =
	        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo NInfo = CManager.getActiveNetworkInfo();
	    if (NInfo != null && NInfo.isConnectedOrConnecting()) {
	        try {
				if (InetAddress.getByName("www.google.com").isReachable(1000))
				{  
				 //Log.d("reachable","network");
				}
				 else
				 {    
					 //Log.d("not reachable","network");
				 }
			} catch (UnknownHostException e) {

			} catch (IOException e) {
			}  
	    }
	    return;
	}
	
	
	public String getWifiName(Context context) {
	    WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    if (manager.isWifiEnabled()) {
	       WifiInfo wifiInfo = manager.getConnectionInfo();
	       if (wifiInfo != null) {
	          DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
	          if (state == DetailedState.CONNECTED || state == DetailedState.OBTAINING_IPADDR) {
	              return wifiInfo.getSSID();
	          }
	       }
	    }
	    return "";
	}
	/**
	 * TurnOn GPS Connection
	 * @param mContext
	 * @author Shiva
	 */
	public static void TurnGPSOn(Context mContext)
	{
		String provider = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	    if(!provider.contains("gps")){ //if gps is disabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3")); 
	        mContext.sendBroadcast(poke);
	    }
	}
}
