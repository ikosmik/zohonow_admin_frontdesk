package utility;

import android.view.View;

import appschema.SingletonCode;

import constants.MainConstants;

/**
 * Created by Karthikeyan D S on 08August2018
 */
public class PrintThread extends Thread {

    private View view;
    private String ipAddress = MainConstants.kConstantEmpty;

    public PrintThread(View view, String ipAddress) {
        this.view = view;
        this.ipAddress = ipAddress;
    }

    public void run() {
        PrintBadge printBadge = new PrintBadge();
        if (printBadge != null && view != null) {
            printBadge.storeBitmapView(view);
            if (!printBadge.printBadge(ipAddress)) {
                if((SingletonCode.getSingletonPrinterErrorCode()== null) || (SingletonCode.getSingletonPrinterErrorCode()=="")){
                    SingletonCode.setSingletonPrinterErrorCode("Other issues");
                }
//    			Intent intent=new Intent(view.getContext(),WelcomeActivity.class);
//    			NotifyObservable.getInstance().updateValue(intent);
//    			UsedCustomToast.makeCustomToast(view.getContext(),MainConstants.kConstantErrorMsgBadgePrint
//    					+ MainConstants.kConstantNewLine
//    					+ MainConstants.errorCodeText
//    					+ ErrorConstants.badgePrint, Toast.LENGTH_LONG, Gravity.TOP).show();
//                Log.d("DSK","1");
            }
        } else {
            if((SingletonCode.getSingletonPrinterErrorCode()== null) || (SingletonCode.getSingletonPrinterErrorCode()=="")){
                SingletonCode.setSingletonPrinterErrorCode("Success");
            }
//    			Intent intent=new Intent(view.getContext(),WelcomeActivity.class);
//    			NotifyObservable.getInstance().updateValue(intent);
//    			UsedCustomToast.makeCustomToast(ApplicationController.getInstance(),MainConstants.kConstantBadgeViewError
//    					+ MainConstants.kConstantNewLine
//    					+ MainConstants.errorCodeText
//    					+ ErrorConstants.badgeViewError,Toast.LENGTH_LONG,Gravity.TOP).show();
//            Log.d("DSK","2");
        }
    }
};

