package utility;

import com.zoho.app.frontdesk.android.ApplicationController;

import constants.MainConstants;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

public class CurrentLocationCode {
	LocationManager locationManager;
	LocationListener locationListener;

	/**
	 *
	 * @param mContext get the Application context
	 * @return void
	 *
	 * @author Shiva
	 * @created 06/10/2016
	 * */
	public CurrentLocationCode(Context mContext) {
		locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		locationListener = new utility.CompareLocationListener();
	}

	/**
	 * function to get the current LocationCode and assign location code to variable in ApplicationController
	 *
	 * @author Shiva
	 * @created 06/10/2016
	 * */
	public void setCurrentLocationCode() {
		if ((ApplicationController.CurrentLocationCode != null)
				&& ((ApplicationController.CurrentLocationCode == MainConstants.kConstantMinusOne)
				|| (ApplicationController.CurrentOfficeLocation == MainConstants.kConstantEmpty))) {
			if (ActivityCompat.checkSelfPermission(ApplicationController.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ApplicationController.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

				return;
			}
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER,
					MainConstants.kConstantZero, MainConstants.kConstantZero,
					locationListener);
		}
	}
	/**
	 * function to get the current LocationCode and assign location code to variable in ApplicationController
	 * 
	 * @author Shiva
	 * @created 06/10/2016
	 * */
	public void removeLocationCode(){
		if ((ApplicationController.CurrentOfficeLocation != null)
				&& (ApplicationController.CurrentOfficeLocation != MainConstants.kConstantEmpty)) {
			locationManager.removeUpdates(locationListener);
		}
	}
}
