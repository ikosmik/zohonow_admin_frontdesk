package utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Hashtable;
import java.util.List;

import com.zoho.app.frontdesk.android.R;

import constants.FontConstants;
public class SpinnerViewAdapter extends ArrayAdapter<String>{
		 
	private Context context;
	private ViewHolder[] holderArray;
	private Hashtable<String,ViewHolder> holderObjects=new Hashtable<String, ViewHolder>();
	private Typeface fontStyle;
	private int resourceId;
	    public SpinnerViewAdapter(Context context, int resourceId,List<String> items) {
	        super(context, resourceId, items);
	        this.context = context;
			this.resourceId = resourceId;
	        holderArray =new ViewHolder[items.size()];
	        for(int i=0;i<items.size();i++)
	        	holderArray[i]=null;
	    }

	    public class ViewHolder{
	       TextView accountNameTextView;
	    }

	@Override
	public View getDropDownView(int position, View convertView,
								ViewGroup parent) {
		ViewHolder holder;
		final String spinnerString = getItem(position);
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(resourceId, null);
			holder = new ViewHolder();

			holder.accountNameTextView=(TextView)convertView.findViewById(R.id.textview_spinner);

			fontStyle = Typeface.createFromAsset(context.getAssets(),
					FontConstants.fontConstantHKGroteskSemiBold);
			holder.accountNameTextView.setTypeface(fontStyle);
			holder.accountNameTextView.setTextSize(23);

			holder.accountNameTextView.setPadding(15, 25, 15, 25);
			holder.accountNameTextView.setBackgroundColor(Color.rgb(119,119,119));
			if(resourceId==R.layout.custom_spinner_white) {
				holder.accountNameTextView.setBackgroundColor(Color.rgb(219,219,219));
			}
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
			/*
	         * set name to the textview
	         */
		if(spinnerString!=null)
		{
			holder.accountNameTextView.setText(spinnerString);
		}
		return convertView;
	}

	    int Position;
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	Position=position;
	    	ViewHolder holder;
	        //Log.d("window","size "+holderArray.length);
	        final String spinnerString = getItem(position);
	        LayoutInflater mInflater = (LayoutInflater) context
	                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	        if (convertView == null) {
	            convertView = mInflater.inflate(resourceId, null);
	            holder = new ViewHolder();
	            
	            holder.accountNameTextView=(TextView)convertView.findViewById(R.id.textview_spinner);
				holder.accountNameTextView.setGravity(Gravity.CENTER_HORIZONTAL);
				fontStyle = Typeface.createFromAsset(context.getAssets(),
						FontConstants.fontConstantHKGroteskSemiBold);
	            holder.accountNameTextView.setTypeface(fontStyle);
		        convertView.setTag(holder);
	            
	        } else {
				holder = (ViewHolder) convertView.getTag();
			}
	        /*
	         * set name to the textview
	         */
	        if(spinnerString!=null)
	        {
	        holder.accountNameTextView.setText(spinnerString);
	        }
	        return convertView;
	    }    

}
