package utility;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.support.v4.print.PrintHelper;
import android.view.View;
import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterInfo.Halftone;
import com.brother.ptouch.sdk.PrinterStatus;
import com.zoho.app.frontdesk.android.EmployeeBadge;
import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonVisitorProfile;
import com.zoho.app.frontdesk.android.VisitorBadge;

import java.io.File;

import constants.MainConstants;

public class PrintBadge {

	public void storeBitmap(VisitorBadge visitorBadge) {

            String visitorBadgeSourceFile = createBitmap(visitorBadge, "Badge");
            SingletonVisitorProfile.getInstance().setVisitorBadge(visitorBadgeSourceFile);
           // Log.d("visitor", "badged" + visitorBadgeSourceFile + " " + SingletonVisitorProfile.getInstance().getVisitorBadge());
	}
	
	public void storeEmployeeBitmap(EmployeeBadge badge) {

        String employeeBadgeSourceFile = createBitmap(badge, "Badge");
        SingletonEmployeeProfile.getInstance().setEmployeeBadge(employeeBadgeSourceFile);
        //Log.d("visitor", "badged" + employeeBadgeSourceFile + " " + SingletonEmployeeProfile.getInstance().getEmployeeBadge());
}

	/*
	 * Created by jayapriya On 15/11/2014 create bitmap to get the visitor Badge.
	 */
	private String createBitmap(View view, String fileName) {

		if (view != null) {
			String sourceFile = constants.MainConstants.kConstantFileDirectory + MainConstants.kConstantFileTemporaryFolder+ fileName
					+ MainConstants.kConstantImageFileFormat;
			view.setDrawingCacheEnabled(true);
			view.buildDrawingCache(true);
			view.setBackgroundColor(Color.WHITE);
			Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
			UsedBitmap usedBitmap = new UsedBitmap();
			usedBitmap.storeBitmap(bitmap, MainConstants.kConstantVisitorBadgeWidth, sourceFile);

			bitmap.recycle();
			return sourceFile;
		} else {
			return "view null";
		}

	}
	
	
	/*
	 * created by jayapriya on 18/11/2014 print the visitorBadge.
	 */
	public void printBitmapOfVisitorBadge1(Context context) {

       // if ((SingletonVisitorProfile.getInstance().getVisitorBadge() != null)
              //  && (SingletonVisitorProfile.getInstance().getVisitorBadge().toString() != MainConstants.kConstantEmpty)
               // &&(new File(SingletonVisitorProfile.getInstance().getVisitorBadge()).exists()) ) {
            
            PrintHelper photoPrinter = new PrintHelper(context);
            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
            photoPrinter.setScaleMode(photoPrinter.COLOR_MODE_COLOR);
            //Log.d("window","listenerprinter"+photoPrinter.systemSupportsPrint());
            // Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
            // R.drawable.business_card);
            // photoPrinter.printBitmap("droids.jpg - test print", bitmap);

            // File filePath = new File(Singleton.getInstance().getVisitorBadge());
            Bitmap bitmap = BitmapFactory.decodeFile(SingletonVisitorProfile.getInstance()
                    .getVisitorBadge());
            photoPrinter.printBitmap("VisitorBadge", bitmap);
            //Log.d("window","photoPrinter"+SingletonVisitorProfile.getInstance()
              //      .getVisitorBadge());
           // bitmap.recycle();

	}


	public boolean printBadge() 
    {
		//Log.d("print","badge");
		Printer myPrinter = new Printer();
    	PrinterInfo myPrinterInfo = new PrinterInfo();
    	//myPrinterInfo.margin = 
    	PrinterStatus printResult = new PrinterStatus();
    	try{
    		//Log.d("print","badge");
    		// Retrieve printer informations
    		myPrinterInfo = myPrinter.getPrinterInfo();
    		//Log.d("print","badge get left "+myPrinterInfo.margin.left+" top "+myPrinterInfo.margin.top);
    		
    		// Set printer informations
    		myPrinterInfo.printerModel = PrinterInfo.Model.QL_720NW;
    		myPrinterInfo.port=PrinterInfo.Port.NET;
    		//Log.d("print","badge");
    	 	if(SingletonCode.getSingletonWifiPrinterIP()!=null && SingletonCode.getSingletonWifiPrinterIP().toString().length()>0) {
    		myPrinterInfo.ipAddress=SingletonCode.getSingletonWifiPrinterIP();
    		}
    	 	//Log.d("print","badge"+myPrinterInfo.ipAddress);
    		myPrinterInfo.printMode=PrinterInfo.PrintMode.FIT_TO_PAGE;
    		myPrinterInfo.orientation = PrinterInfo.Orientation.PORTRAIT;
    		myPrinterInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
    		//myPrinterInfo.margin =new PrinterInfo.Margin(0, 0);
    	/*	myPrinterInfo.align=PrinterInfo.Align.LEFT;
    		myPrinterInfo.margin.left=-20;
    		//myPrinterInfo.align=PrinterInfo.Align.;
    		myPrinterInfo.valign=PrinterInfo.VAlign.TOP;
    		myPrinterInfo.margin.top=-20;*/
        	myPrinterInfo.halftone=Halftone.ERRORDIFFUSION;
    		myPrinterInfo.rjDensity=-5;
    		//myPrinterInfo.pjDensity=0;
    		//myPrinterInfo.customPaperWidth=2464;
    		//myPrinterInfo.customPaperLength=4000;
    		//Log.d("print","badge"+myPrinterInfo.ipAddress);
    		LabelInfo mLabelInfo = new LabelInfo();
    		mLabelInfo.labelNameIndex = 15;
    		mLabelInfo.isAutoCut = true;
    		mLabelInfo.isEndCut = true;
    		mLabelInfo.isHalfCut = false;
    		
    		myPrinter.setPrinterInfo(myPrinterInfo);
    		myPrinter.setLabelInfo(mLabelInfo);
    		//Log.d("print","badge"+myPrinterInfo.ipAddress);
    		// Print file
    		String path = Environment.getExternalStorageDirectory().toString() + "/FrontDesk/Temp/Badge.jpg"; 
    		//Log.d("print","badge left "+myPrinterInfo.margin.left+" top "+myPrinterInfo.margin.top);
    		
    		// Boolean val= myPrinter.startPTTPrint(6, null);
             //Log.d("print", "startPTTPrint "+val);

            // BitmapFactory.Options options = new BitmapFactory.Options();
            // options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            // Bitmap bitmap = BitmapFactory.decodeFile(path, options);
 
             //PrinterStatus status=myPrinter.printImage(bitmap);//ERROR thrown here
            // Log.i("print", "PrinterStatus  err"+status.errorCode);
    		//Testing printer 
    		try{
    		  printResult = myPrinter.printFile(path);
    		}
    		catch(Exception e){
    			SingletonCode.setSingletonPrinterErrorCode(""+e);
    	    	return false;
    		}
    		if((printResult!=null)||(printResult.errorCode.toString().contains("ERROR_NONE"))){
    			SingletonCode.setSingletonPrinterErrorCode("Success");
    			return true;
    		}
    		else
    		{
    			SingletonCode.setSingletonPrinterErrorCode(printResult.errorCode.toString());
    			return false;
    		}
    		//Log.d("print","badge result");
    		//Log.d("Android", myPrinterInfo.halftone+":"+printResult.errorCode); 
    		//Log.d("Android", myPrinterInfo.customPaperWidth+":"+myPrinterInfo.customPaperLength); 
    		
    	}catch(Exception e){	
    		//Log.d("Android", "Exception:"+e); 
    		SingletonCode.setSingletonWifiPrinterIP("");
    		return false;
    	}
    	//return true;
    }

	/**
	 * author Karthikeyan D S
	 * @created on 08Aug2018
	 *
	 * @param visitorBadge
	 * @return
	 */
	public String storeBitmapView(View visitorBadge) {
		return createBitmapDetails(visitorBadge);
	}

	private String createBitmapDetails(View view) {
       File file = new File( MainConstants.kConstantFileDirectory + MainConstants.kConstantFileTemporaryFolder);

		if (view != null) {
		    if(!file.exists()) {
		        file.mkdir();
            }
			String sourceFile = MainConstants.kConstantFileDirectory + MainConstants.kConstantFileVisitorBadge+
					SingletonVisitorProfile.getInstance().getVisitorBadgeFileName()+".jpg";
			view.setDrawingCacheEnabled(true);
			view.buildDrawingCache(true);
			view.setBackgroundColor(Color.WHITE);
			Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
				UsedBitmap usedBitmap = new UsedBitmap();
				usedBitmap.storeBitmap(bitmap, MainConstants.kConstantVisitorBadgeWidth, sourceFile);

				bitmap.recycle();
			return sourceFile;
		} else {
			return "view null";
			}

	}


	public boolean printBadge(String ipAddress)
	{
		Printer myPrinter = new Printer();
		PrinterInfo myPrinterInfo = new PrinterInfo();
		//myPrinterInfo.margin =
		PrinterStatus printResult = new PrinterStatus();
		try{
			// Retrieve printer informations
			myPrinterInfo = myPrinter.getPrinterInfo();

			// Set printer informations
			myPrinterInfo.printerModel = PrinterInfo.Model.QL_720NW;
			myPrinterInfo.port=PrinterInfo.Port.NET;
			if(ipAddress!=null && ipAddress.length()>0) {
				myPrinterInfo.ipAddress=ipAddress;
			}
			myPrinterInfo.printMode=PrinterInfo.PrintMode.FIT_TO_PAGE;
			myPrinterInfo.orientation = PrinterInfo.Orientation.PORTRAIT;
			myPrinterInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
			myPrinterInfo.halftone=Halftone.ERRORDIFFUSION;
			myPrinterInfo.rjDensity=-1;
			//myPrinterInfo.pjDensity=0;
			//myPrinterInfo.customPaperWidth=2464;
			//myPrinterInfo.customPaperLength=4000;
			LabelInfo mLabelInfo = new LabelInfo();
			mLabelInfo.labelNameIndex = 15;
			mLabelInfo.isAutoCut = true;
			mLabelInfo.isEndCut = true;
			mLabelInfo.isHalfCut = false;

			myPrinter.setPrinterInfo(myPrinterInfo);
			myPrinter.setLabelInfo(mLabelInfo);
			// Print file
			String path = MainConstants.kConstantFileDirectory + MainConstants.kConstantFileBadge;

			//Print label
			try{
				printResult = myPrinter.printFile(path);
			}
			catch(Exception e){
				SingletonCode.setSingletonPrinterErrorCode(""+e.getLocalizedMessage());
				return false;
			}
			if((printResult!=null)&&(printResult.errorCode!=null && printResult.errorCode.toString().contains("ERROR_NONE"))){
				SingletonCode.setSingletonPrinterErrorCode("Success");
				return true;
			}
			else
			{
				SingletonCode.setSingletonPrinterErrorCode(printResult.errorCode.toString());
				return false;
			}
		}catch(Exception e){
			SingletonCode.setSingletonPrinterErrorCode(""+e.getLocalizedMessage());
			return false;
		}
	}
}
