package utility;

import com.zoho.app.frontdesk.android.R;

import constants.FontConstants;
import constants.MainConstants;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Decision dialog
 * 
 * @author Karthick
 * @created 20170130
 */
public class DialogViewWarning {

	private Dialog dialogView;
	private TextView textViewTitle,textViewErrorCode;
	private RelativeLayout relativeLayoutTwoButton, relativeLayoutOneButton;
	private Button buttonAction, buttonCancel, buttonOkay;

	/**
	 * Assign dialog variable
	 * 
	 * @author Karthick
	 * @created 20170130
	 */
	public DialogViewWarning(Context context) {
		dialogView = new Dialog(context);
		dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogView.setContentView(R.layout.dialog_view_warning);

		relativeLayoutTwoButton = (RelativeLayout) dialogView
				.findViewById(R.id.relative_layout_two_buttons);
		relativeLayoutOneButton = (RelativeLayout) dialogView
				.findViewById(R.id.relative_layout_one_button);
		textViewTitle = (TextView) dialogView.findViewById(R.id.dialog_title);
		textViewErrorCode = (TextView) dialogView.findViewById(R.id.dialog_error_code);
		buttonAction = (Button) dialogView.findViewById(R.id.dialog_yes_button);
		buttonCancel = (Button) dialogView.findViewById(R.id.dialog_no_button);
		buttonOkay = (Button) dialogView.findViewById(R.id.dialog_ok_button);

		Typeface fontStyle = Typeface.createFromAsset(context.getAssets(),
				FontConstants.fontConstantHKGroteskSemiBold);
		if (fontStyle != null && buttonCancel != null && buttonAction != null) {
			buttonCancel.setTypeface(fontStyle);
			buttonAction.setTypeface(fontStyle);
		}
		if (fontStyle != null && textViewTitle != null) {
			textViewTitle.setTypeface(fontStyle);
		}

		if (buttonCancel != null && listenerCancel != null) {
			buttonCancel.setOnClickListener(listenerCancel);
		}
		if (buttonOkay != null && listenerCancel != null) {
			buttonOkay.setOnClickListener(listenerCancel);
		}
	}

	/**
	 * Hide dialog
	 * 
	 * @author Karthick
	 * @created on 20170128
	 */
	private OnClickListener listenerCancel = new OnClickListener() {
		public void onClick(View v) {
			hideDialog();
		}
	};

	/**
	 * Set error message
	 * 
	 * @param dialogMsg
	 * 
	 * @author Karthick
	 * @created 20170131
	 */
	public void setDialogMessage(String dialogMsg) {
		if (textViewTitle != null && dialogMsg != null) {
			textViewTitle.setText(dialogMsg);
		}
	}
	
	/**
	 * Set error code
	 * 
	 * @param errorCode
	 * 
	 * @author Karthick
	 * @created 20170131
	 */
	public void setDialogErrorCode(String errorCode) {
		if (textViewErrorCode != null && errorCode != null) {
			textViewErrorCode.setText(errorCode);
		}
	}

	/**
	 * Set visibility depends on param
	 * 
	 * @param dialogCode
	 * 
	 * @author Karthick
	 * @created 20170131
	 */
	public void setDialogCode(int dialogCode) {
		if (dialogCode == MainConstants.dialogDecision) {
			dialogView.setCanceledOnTouchOutside(false);
			if (relativeLayoutTwoButton != null) {
				relativeLayoutTwoButton.setVisibility(View.VISIBLE);
			}
			if (relativeLayoutOneButton != null) {
				relativeLayoutOneButton.setVisibility(View.GONE);
			}
			if (textViewTitle != null) {
				textViewTitle.setGravity(Gravity.CENTER);
			}

		} else if (dialogCode == MainConstants.dialogConfirm) {
			dialogView.setCanceledOnTouchOutside(false);
			if (relativeLayoutTwoButton != null) {
				relativeLayoutTwoButton.setVisibility(View.VISIBLE);
			}
			if (relativeLayoutOneButton != null) {
				relativeLayoutOneButton.setVisibility(View.GONE);
			}
			if (textViewTitle != null) {
				textViewTitle.setGravity(Gravity.CENTER);
			}

		} else if (dialogCode == MainConstants.dialogInfo) {
			if (relativeLayoutTwoButton != null) {
				relativeLayoutTwoButton.setVisibility(View.GONE);
			}
			if (relativeLayoutOneButton != null) {
				relativeLayoutOneButton.setVisibility(View.VISIBLE);
			}
			if (textViewTitle != null) {
				textViewTitle
						.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
			}
		}
	}

	/**
	 * Set listener for button
	 * 
	 * @author Karthick
	 * @created 20170131
	 */
	public void setActionListener(OnClickListener listenerOkay) {
		if (buttonAction != null && listenerOkay != null) {
			buttonAction.setOnClickListener(listenerOkay);
		}
	}

	/**
	 * Set listener for button
	 * 
	 * @author Karthick
	 * @created 20170131
	 */
	public void setActionButtonText(String buttonText) {

		if (buttonAction != null && buttonText!=null) {
			buttonAction.setText(buttonText);
		}
	}

	/**
	 * Show dialog
	 * 
	 * @author Karthick
	 * @created 20170130
	 */
	public void showDialog() {
		if (dialogView != null && !dialogView.isShowing()) {
			dialogView.show();
		}
	}

	/**
	 * Hide dialog
	 * 
	 * @author Karthick
	 * @created 20170130
	 */
	public void hideDialog() {
		if (dialogView != null && dialogView.isShowing()) {
			dialogView.dismiss();
		}
	}
}
