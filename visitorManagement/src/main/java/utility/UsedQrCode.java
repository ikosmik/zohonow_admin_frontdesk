package utility;

public class UsedQrCode {
	/**
	 * Created by Jayapriya On 13/12/14 Generate the QrCode using the zxing
	 * library.
	 */
	static Long currentTime;

	/*public static void generateQrCode() {

		Log.d("generateQrCode", "generateQrCode" );
		String qrCodeText = setQrCodeText();

		if ((qrCodeText != null)
				&& (qrCodeText.length() > MainConstants.kConstantZero)){
				//&& (SingletonVisitorProfile.getInstance()
						//.getCreatorVisitorUploadCode() == MainConstants.kConstantTwo)) {
			Log.d("start6", "qrcode" );

			currentTime = System.currentTimeMillis();
			/**MultiFormatWriter writer = new MultiFormatWriter();
			BitMatrix result;/close command line
			try {
				result = writer.encode(qrCodeText, BarcodeFormat.QR_CODE, 200,
						200);

				int width = result.getWidth();
				int height = result.getHeight();
				int[] pixels = new int[width * height];
				// All are 0, or black, by default
				for (int y = MainConstants.kConstantZero; y < height; y++) {
					int offset = y * width;
					for (int x = MainConstants.kConstantZero; x < width; x++) {
						if (result.get(x, y))
							pixels[offset + x] = Color.BLACK;
						else
							pixels[offset + x] = Color.WHITE;
					}
				}

				Bitmap bitmap = Bitmap.createBitmap(width, height,
						Bitmap.Config.ARGB_8888);

				bitmap.setPixels(pixels, MainConstants.kConstantZero, width,
						MainConstants.kConstantZero,
						MainConstants.kConstantZero, width, height);

				/*Bitmap resized = Bitmap.createScaledBitmap(bitmap, 100, 100,
						true);
				bitmap.recycle();
				bitmap=null;/close command line
				// resized.eraseColor(Color.WHITE);
				utility.UsedBitmap.storeBitmap(bitmap, 100,
						MainConstants.kConstantFileTemporaryFolder,
						SingletonVisitorProfile.getInstance()
								.getVisitorName()
								+ currentTime
								+ MainConstants.kConstantImageFileFormat);
				SingletonVisitorProfile.getInstance().setQrcode(
						MainConstants.kConstantFileDirectory
								+ MainConstants.kConstantFileTemporaryFolder
								+ SingletonVisitorProfile.getInstance()
										.getVisitorName() + currentTime
								+ MainConstants.kConstantImageFileFormat);

				bitmap.recycle();
				bitmap=null;
				writer=null;
				result=null;
			} catch (WriterException e) {
				// Log.e("QR ERROR", ""+e);

				//SingletonVisitorProfile.getInstance().setQrcode(
						//MainConstants.kConstantEmpty);
			}
		}
	}

	private static String setQrCodeText() {
		String qrCodeText = null;
		if ((SingletonVisitorProfile.getInstance() != null)
				&& (SingletonVisitorProfile.getInstance()
						.getEmail() != null) && (SingletonVisitorProfile.getInstance().getEmail() != MainConstants.kConstantEmpty)
                && (SingletonVisitorProfile.getInstance().getEmail().length() > MainConstants.kConstantZero)) {

			qrCodeText = MainConstants.kConstantQrCodeVisitorName
					+ SingletonVisitorProfile.getInstance()
							.getVisitorName()
					+ MainConstants.kConstantNewLine
					+ MainConstants.kConstantQrCodeVisitorEmailID
					+ SingletonVisitorProfile.getInstance().getEmail()
					+ MainConstants.kConstantNewLine
					+ MainConstants.kConstantQrCodeCheckInDate
					+ SingletonVisitorProfile.getInstance()
							.getDateOfVisitLocal()
					+ MainConstants.kConstantNewLine
					+ MainConstants.kConstantQrCodePurposeOfVisit
					+ SingletonVisitorProfile.getInstance()
							.getPurposeOfVisit();

		}
		else if ((SingletonVisitorProfile.getInstance() != null)
				 && (SingletonVisitorProfile.getInstance()
						.getPhoneNo() != null) && (SingletonVisitorProfile.getInstance().getPhoneNo() != MainConstants.kConstantEmpty)
                && (SingletonVisitorProfile.getInstance().getPhoneNo().length() > MainConstants.kConstantZero)) {

            qrCodeText = MainConstants.kConstantQrCodeVisitorName
					+ SingletonVisitorProfile.getInstance()
							.getVisitorName()
					+ MainConstants.kConstantNewLine
					+ MainConstants.kConstantQrCodeVisitorPhone
					+ SingletonVisitorProfile.getInstance().getPhoneNo()
					+ MainConstants.kConstantNewLine
					+ MainConstants.kConstantQrCodeCheckInDate
					+ SingletonVisitorProfile.getInstance()
							.getDateOfVisitLocal()
					+ MainConstants.kConstantNewLine
					+ MainConstants.kConstantQrCodePurposeOfVisit
					+ SingletonVisitorProfile.getInstance()
							.getPurposeOfVisit();
		}

		return qrCodeText;

	}*/

}
