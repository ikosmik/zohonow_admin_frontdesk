package utility;


import java.util.Observable;

/**
 * Notify to Activity class
 *
 * @author Karthick
 * @created 20160530
 */
public class NotifyObservable  extends Observable {
    private static NotifyObservable instance = new NotifyObservable();

    public static NotifyObservable getInstance() {
        return instance;
    }

    private NotifyObservable() {
    }

    public void updateValue(Object data) {
        synchronized (this) {
            setChanged();
            notifyObservers(data);
        }
    }

}
