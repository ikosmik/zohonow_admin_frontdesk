package utility;

import java.util.List;

import com.zoho.app.frontdesk.android.R;

import appschema.SingletonCode;
import appschema.SingletonVisitorProfile;

import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DialogView {
    private static Dialog reportDialog;
    private static ImageView reportImage;
    private static TextView textViewReportMsg, textViewReportFirstMsg, textViewReportErrorCode;
    private static EditText editTextValue;
    private static ProgressBar progressBar;
    private static Button tellUs, buttonRetry;
    private static View seprator;
    private static RelativeLayout relativeLayout;
    private static LinearLayout linearLayoutRetry;
    private static Typeface fontFirstMsg, fontMsg;

    /**
     * @param context       :dialog show which page
     * @param msg           :What message to show the dialog
     * @param dialogMsgFont :font typeface
     */
    public static void showReportDialog(final Context context, final int whichActivityDialog, final int reportStatus) {
        /*
         * This dialog used for display message to user
         *
         */
        if (reportDialog == null) {

            reportDialog = new Dialog(context,
                    android.R.style.Theme_InputMethod);
            reportDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            reportDialog.setContentView(R.layout.dialog_report);

            if (!((Activity) context).isFinishing()) {
                reportDialog.show();
            }

            textViewReportMsg = (TextView) reportDialog
                    .findViewById(R.id.textView_report_msg);
            textViewReportFirstMsg = (TextView) reportDialog
                    .findViewById(R.id.textView_report_first_msg);
            textViewReportErrorCode = (TextView) reportDialog
                    .findViewById(R.id.textView_report_error_code);
            progressBar = (ProgressBar) reportDialog
                    .findViewById(R.id.progressBar_report);
            reportImage = (ImageView) reportDialog
                    .findViewById(R.id.imageView_report);
            tellUs = (Button) reportDialog.findViewById(R.id.button_tellus);
            linearLayoutRetry = (LinearLayout) reportDialog.findViewById(R.id.linear_layout_retry);
            linearLayoutRetry.setVisibility(View.GONE);
            if (whichActivityDialog == MainConstants.badgeDialog) {
                if (tellUs != null) {
                    tellUs.setVisibility(View.GONE);
                }
                tellUs = (Button) reportDialog.findViewById(R.id.button_tellus_mail);
            }

            buttonRetry = (Button) reportDialog.findViewById(R.id.button_retry);
            seprator = (View) reportDialog.findViewById(R.id.firstSeparator);
            relativeLayout = (RelativeLayout) reportDialog.findViewById(R.id.relativeLayout_dialog);
            fontFirstMsg = Typeface.createFromAsset(context.getAssets(),
                    FontConstants.fontConstantSourceSansProbold);
            fontMsg = Typeface.createFromAsset(context.getAssets(),
                    FontConstants.fontConstantSourceSansProSemibold);
            tellUs.setTypeface(fontFirstMsg);
            buttonRetry.setTypeface(fontFirstMsg);
            textViewReportMsg.setTypeface(fontMsg);
            textViewReportFirstMsg.setTypeface(fontFirstMsg);
            textViewReportErrorCode.setTypeface(fontMsg);
            setReportImage(context, whichActivityDialog, reportStatus);
            buttonRetry.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    reportDialog.dismiss();
                }
            });
            tellUs.setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    reportDialog.dismiss();

                    if (whichActivityDialog == MainConstants.badgeDialog) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType(MainConstants.mailType);
                        intent.putExtra(Intent.EXTRA_EMAIL,
                                new String[]{MainConstants.mailID});
                        intent.putExtra(Intent.EXTRA_SUBJECT,
                                MainConstants.mailSubject);

                        intent.putExtra(Intent.EXTRA_TEXT,
                                MainConstants.errorCode + SingletonCode.getSingletonVisitorUploadErrorCode() +
                                        MainConstants.kConstantNewLine
                                        + MainConstants.kConstantNewLine
                                        + MainConstants.sentFrom);

                        //context.startActivity(Intent.createChooser(intent,
                        //MainConstants.mailSend));
                        PackageManager pm = context.getPackageManager();
                        List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
                        for (final ResolveInfo app : activityList) {
                            if ((app.activityInfo.name).contains("android.gm")) {
                                final ActivityInfo activity = app.activityInfo;
                                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                intent.setComponent(name);
                                context.startActivity(intent);
                                break;
                            }
                        }
                        SingletonCode.setSingletonTellUsCode(MainConstants.tellUsCode);

                    } else if (whichActivityDialog == MainConstants.printerIssue) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType(MainConstants.mailType);
                        intent.putExtra(Intent.EXTRA_EMAIL,
                                new String[]{MainConstants.supportMail});
                        intent.putExtra(Intent.EXTRA_SUBJECT,
                                "Frontdesk App: Printer Issue");

                        intent.putExtra(Intent.EXTRA_TEXT,
                                SingletonCode.getSingletonPrinterErrorCode()
                        );
                        PackageManager pm = context.getPackageManager();
                        List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
                        for (final ResolveInfo app : activityList) {
                            if ((app.activityInfo.name).contains("android.gm")) {
                                final ActivityInfo activity = app.activityInfo;
                                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                intent.setComponent(name);
                                context.startActivity(intent);
                                break;
                            }
                        }
                        // intent.setType(IKStringContants.mailType);
                        //context.startActivity(Intent.createChooser(intent,
                        //MainConstants.mailSend));

                    } else if (whichActivityDialog == MainConstants.uploadStationeryFailure) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType(MainConstants.mailType);
                        intent.putExtra(Intent.EXTRA_EMAIL,
                                new String[]{"frontdesk@zohocorp.com"});
                        intent.putExtra(Intent.EXTRA_SUBJECT,
                                "Frontdesk App: Upload Stationery Error ");

                        intent.putExtra(Intent.EXTRA_TEXT,
                                SingletonCode.getSingletonVolleyExceptionError()
                        );
                        // intent.setType(IKStringContants.mailType);
                        //context.startActivity(Intent.createChooser(intent,
                        //MainConstants.mailSend));
                        PackageManager pm = context.getPackageManager();
                        List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
                        for (final ResolveInfo app : activityList) {
                            if ((app.activityInfo.name).contains("android.gm")) {
                                final ActivityInfo activity = app.activityInfo;
                                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                intent.setComponent(name);
                                context.startActivity(intent);
                                break;
                            }
                        }
                    }

                }
            });

            textViewReportMsg.setGravity(Gravity.CENTER);
            // textViewReportMsg.setText(msg);
        } else if (reportDialog != null && reportDialog.isShowing()) {
            setReportImage(context, whichActivityDialog, reportStatus);
        } else if (reportDialog != null && !reportDialog.isShowing()) {
            setReportImage(context, whichActivityDialog, reportStatus);
            /**TODO : Show error dialog , previously commented. Now, to enable the dialog show remove the commented  **/
            if ((context != null && reportDialog != null) && !reportDialog.isShowing() && !((Activity) context).isFinishing()) {
                reportDialog.show();
            }
        }

    }

    /*
     *
     * @param context
     *            :dialog show which page
     * @param msg
     *            :What message to show the dialog
     * @param dialogMsgFont
     *            :font typeface
     */
    public static void showReportDialog(final Context context, final int whichActivityDialog, final int reportStatus, OnClickListener listenerRetry) {

        showReportDialog(context, whichActivityDialog, reportStatus);
        if (listenerRetry != null && buttonRetry != null) {
            buttonRetry.setOnClickListener(listenerRetry);
        }
    }

    public static void setReportImage(final Context context, int whichActivityDialog, int reportStatus) {

        if (textViewReportMsg != null) {
            textViewReportMsg.setTextSize(22);
            textViewReportMsg.setGravity(Gravity.CENTER);
            textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_loading_dialog_msg_min_height));
        }
        if (linearLayoutRetry != null) {
            linearLayoutRetry.setVisibility(View.GONE);
        }
        if (reportStatus == MainConstants.forgotIDUploadCode) {

            //Log.d("Android", "if");
            if (relativeLayout != null) {
                relativeLayout.getLayoutParams().width = (int) (400 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            }
            if (textViewReportMsg != null) {
                textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_loading_dialog_check_in_min_height));
            }
            if (progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }
            if (reportImage != null) {
                reportImage.setVisibility(View.GONE);
            }
            reportDialog.setCanceledOnTouchOutside(false);
            if (textViewReportMsg != null) {
                textViewReportMsg.setTextColor(Color.rgb(51, 51, 51));
            }
            if (relativeLayout != null) {
                relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));
            }
            if (whichActivityDialog == MainConstants.forgotIDDialog) {
                if (textViewReportMsg != null) {
                    textViewReportMsg.setText(StringConstants.processingTemporaryIdMsg);
                }
            } else if (whichActivityDialog == MainConstants.printBadgeDialog) {
                relativeLayout.setBackgroundColor(Color.argb(247, 255, 74, 93));
                //tellUs.setTextColor(Color.rgb(159, 19, 55));
                seprator.setBackgroundColor(Color.rgb(198, 52, 90));
                //Log.d("Android", "else if");
                textViewReportMsg.setText(StringConstants.visitorPrintBadgeMsg);
            } else if (whichActivityDialog == MainConstants.checkOutProcessing) {
                textViewReportMsg.setTextColor(Color.parseColor("#FFFFFF"));
                relativeLayout.setBackgroundColor(Color.argb(150,0, 0, 0));
                if (textViewReportMsg != null) {
                    textViewReportMsg.setGravity(Gravity.CENTER);
                    textViewReportMsg.setText(StringConstants.checkOutProcessingMsg);
                    textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_loading_dialog_check_in_min_height));
                }
            } else if (whichActivityDialog == MainConstants.checkInProcessing) {
                textViewReportMsg.setTextColor(Color.parseColor("#FFFFFF"));
                relativeLayout.setBackgroundColor(Color.argb(150,0, 0, 0));
                if (textViewReportMsg != null) {
                    textViewReportMsg.setGravity(Gravity.CENTER);
                    textViewReportMsg.setText(StringConstants.visitorUploadProcessingMsg);
                    textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_loading_dialog_check_in_min_height));
                }
            } else {
                if (textViewReportMsg != null) {
                    textViewReportMsg.setGravity(Gravity.CENTER);
                    textViewReportMsg.setText(StringConstants.visitorUploadProcessingMsg);
                    textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_loading_dialog_check_in_min_height));
                }
            }
            if (textViewReportFirstMsg != null) {
                textViewReportFirstMsg.setVisibility(View.GONE);
            }
            if (textViewReportErrorCode != null) {
                textViewReportErrorCode.setVisibility(View.GONE);
            }
            if (seprator != null) {
                seprator.setVisibility(View.GONE);
            }
            if (tellUs != null) {
                tellUs.setVisibility(View.GONE);
            }
            reportDialog.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode,
                                     KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                    }
                    return true;
                }
            });
        } else if (reportStatus != MainConstants.forgotIDUploadCode && whichActivityDialog == MainConstants.checkOutProcessing) {
            //Log.d("Android", "if");
            relativeLayout.getLayoutParams().width = (int) (400 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_loading_dialog_check_in_min_height));
            progressBar.setVisibility(View.VISIBLE);
            reportImage.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(false);
            textViewReportMsg.setTextColor(Color.rgb(51, 51, 51));
            relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));

            textViewReportErrorCode.setVisibility(View.GONE);
            seprator.setVisibility(View.GONE);
            tellUs.setVisibility(View.GONE);

            textViewReportMsg.setGravity(Gravity.CENTER);
            textViewReportMsg.setText(StringConstants.checkOutProcessingMsg);
        } else if ((reportStatus != MainConstants.forgotIDUploadCode) && (whichActivityDialog == MainConstants.forgotIDDialog)) {

            reportImage.setBackgroundResource(R.drawable.icon_forgotid_error_smiley);
            progressBar.setVisibility(View.GONE);
            textViewReportFirstMsg.setVisibility(View.GONE);
            textViewReportErrorCode.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(true);
            textViewReportMsg.setText(StringConstants.forgotIdErrorMsg);
            reportImage.setVisibility(View.VISIBLE);
            textViewReportMsg.setTextColor(Color.rgb(255, 255, 255));
            relativeLayout.setBackgroundColor(Color.argb(230, 255, 74, 93));
            reportDialog.setOnKeyListener(null);
            seprator.setVisibility(View.GONE);
            tellUs.setVisibility(View.GONE);
        } else if ((reportStatus == ErrorConstants.visitorAllSuccessCode) && (whichActivityDialog == MainConstants.badgeDialog)) {
            relativeLayout.getLayoutParams().width = (int) (400 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            reportImage.setVisibility(View.VISIBLE);
            textViewReportFirstMsg.setVisibility(View.VISIBLE);
            textViewReportErrorCode.setVisibility(View.GONE);
            reportImage.setBackgroundResource(R.drawable.icon_dialog_success_smiley);
            progressBar.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(false);
            reportDialog.setOnKeyListener(null);
            seprator.setVisibility(View.GONE);
            tellUs.setVisibility(View.GONE);
            textViewReportFirstMsg.setText(StringConstants.welcomeToZohoMsg);
            textViewReportFirstMsg.setTextColor(Color.rgb(51, 51, 51));
            textViewReportMsg.setText(StringConstants.visitorUploadCompletedMsg);
            textViewReportMsg.setTextColor(Color.rgb(51, 51, 51));
            relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));
            textViewReportFirstMsg.setMinHeight(100);
            //relativeLayout.setMinimumHeight(200);
            textViewReportMsg.setMinHeight(70);

            textViewReportFirstMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_dialog_success_first_msg_min_height));
            textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_dialog_success_first_msg_min_height));

            textViewReportFirstMsg.setGravity(Gravity.BOTTOM);
            textViewReportMsg.setTextSize(20);
            textViewReportMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        } else if (((reportStatus == ErrorConstants.ForgetIdAllCheckInSuccessCode) || (reportStatus == ErrorConstants.ForgetIdAllCheckOutSuccessCode)) && (whichActivityDialog == MainConstants.badgeDialog)) {
            relativeLayout.getLayoutParams().width = (int) (400 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            reportImage.setVisibility(View.VISIBLE);
            textViewReportFirstMsg.setVisibility(View.VISIBLE);
            textViewReportErrorCode.setVisibility(View.GONE);

            progressBar.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(false);
            reportDialog.setOnKeyListener(null);
            seprator.setVisibility(View.GONE);
            tellUs.setVisibility(View.GONE);

            if (reportStatus == ErrorConstants.ForgetIdAllCheckInSuccessCode ||
                    reportStatus == ErrorConstants.ForgetIdAllCheckOutSuccessCode ) {
                textViewReportFirstMsg.setTextColor(Color.parseColor("#FFFFFF"));
                textViewReportMsg.setTextColor(Color.parseColor("#FFFFFF"));
                relativeLayout.setBackgroundColor(Color.argb(150,0, 0, 0));
                textViewReportFirstMsg.setText(StringConstants.kConstantMessageGreatDay);
                textViewReportMsg.setText(StringConstants.forgetIDCheckInCompletedMsg);
                reportImage.setBackgroundResource(R.drawable.icon_dialog_success_smiley_white);
            } else {
                textViewReportFirstMsg.setTextColor(Color.rgb(51, 51, 51));
                relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));
                textViewReportMsg.setTextColor(Color.rgb(51, 51, 51));
                textViewReportFirstMsg.setText("Thank You");
                textViewReportMsg.setText(StringConstants.forgetIDCheckOutCompletedMsg);
                reportImage.setBackgroundResource(R.drawable.icon_dialog_success_smiley);
            }
            textViewReportFirstMsg.setMinHeight(100);
            //relativeLayout.setMinimumHeight(200);
            textViewReportMsg.setMinHeight(70);

            textViewReportFirstMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_dialog_success_first_msg_min_height));
            textViewReportMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_dialog_success_first_msg_min_height));

            textViewReportFirstMsg.setGravity(Gravity.BOTTOM);
            textViewReportMsg.setTextSize(20);
            textViewReportMsg.setGravity(Gravity.CENTER_HORIZONTAL);

        } else if ((reportStatus != ErrorConstants.visitorAllSuccessCode) && (whichActivityDialog == MainConstants.badgeDialog)) {
            relativeLayout.getLayoutParams().width = (int) (450 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            reportImage.setVisibility(View.VISIBLE);
            textViewReportFirstMsg.setVisibility(View.VISIBLE);
            reportImage.setBackgroundResource(R.drawable.icon_dialog_failure_smiley);
            progressBar.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(true);
            reportDialog.setOnKeyListener(null);
            textViewReportErrorCode.setVisibility(View.VISIBLE);
            seprator.setVisibility(View.VISIBLE);
            tellUs.setVisibility(View.VISIBLE);
            tellUs.setText(StringConstants.tellUs);
            textViewReportFirstMsg.setText(StringConstants.visitorUploadErrorFirstMsg);
            textViewReportMsg.setText(StringConstants.visitorUploadErrorSecondMsg);
            textViewReportErrorCode.setText(StringConstants.visitorUploadErrorCodeText + reportStatus);
            textViewReportMsg.setTextColor(Color.WHITE);
            relativeLayout.setBackgroundColor(Color.argb(247, 255, 74, 93));
            //tellUs.setTextColor(Color.rgb(159, 19, 55));
            seprator.setBackgroundColor(Color.rgb(198, 52, 90));
            textViewReportMsg.setTextSize(20);
            textViewReportFirstMsg.setMinHeight(80);
            textViewReportFirstMsg.setGravity(Gravity.TOP);
            textViewReportFirstMsg.setMinHeight((int) context.getResources().getDimension(R.dimen.visitor_dialog_fail_first_msg_min_height));


            //retry code
            if (whichActivityDialog == MainConstants.badgeDialog) {
                linearLayoutRetry.setVisibility(View.VISIBLE);
            }
            if (SingletonVisitorProfile.getInstance().getRetryCount() >= MainConstants.retryMaxCount) {
                buttonRetry.setText(StringConstants.kConstantDialogHome);
            }
        } else if (reportStatus == MainConstants.reportStatusPrinterFailure) {
            //imageviewReport.setVisibility(View.GONE);
            //imageviewReport.setBackgroundResource(R.drawable.icon_error);
            progressBar.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(true);
            textViewReportMsg.setText("Printer issue: " + SingletonCode.getSingletonPrinterErrorCode());
            reportDialog.setOnKeyListener(null);
            //viewSeprator.setVisibility(View.VISIBLE);
            relativeLayout.setBackgroundColor(Color.argb(247, 255, 74, 93));
            //tellUs.setTextColor(Color.rgb(159, 19, 55));
            seprator.setBackgroundColor(Color.rgb(198, 52, 90));

            tellUs.setVisibility(View.VISIBLE);
            tellUs.setText(StringConstants.tellUs);
            tellUs.setBackgroundColor(Color.TRANSPARENT);
            //tellUs
            //.setTextColor(ColorConstants.colorButtonBackroundTellUsGreen);
            tellUs.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType(MainConstants.mailType);
                    intent.putExtra(Intent.EXTRA_EMAIL,
                            new String[]{MainConstants.supportMail});
                    intent.putExtra(Intent.EXTRA_SUBJECT,
                            "Frontdesk App: Printer Issue");

                    intent.putExtra(Intent.EXTRA_TEXT,
                            SingletonCode.getSingletonPrinterErrorCode()
                    );
                    // intent.setType(IKStringContants.mailType);
                    //context.startActivity(Intent.createChooser(intent,
                    //MainConstants.mailSend));
                    PackageManager pm = context.getPackageManager();
                    List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
                    for (final ResolveInfo app : activityList) {
                        if ((app.activityInfo.name).contains("android.gm")) {
                            final ActivityInfo activity = app.activityInfo;
                            final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                            intent.addCategory(Intent.CATEGORY_LAUNCHER);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                            intent.setComponent(name);
                            context.startActivity(intent);
                            break;
                        }
                    }

                }
            });

        } else if (whichActivityDialog == MainConstants.uploadStationeryFailure || reportStatus == MainConstants.reportStatusStationeryItemUploadFailureError || reportStatus == ErrorConstants.stationeryRequestURLError || reportStatus == ErrorConstants.stationeryRequestJSONError || reportStatus == ErrorConstants.stationeryRequestIOError || reportStatus == ErrorConstants.stationeryRequestJsonDataError) {
            relativeLayout.getLayoutParams().width = (int) (450 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            reportImage.setVisibility(View.VISIBLE);
            textViewReportFirstMsg.setVisibility(View.VISIBLE);
            reportImage.setBackgroundResource(R.drawable.icon_dialog_failure_smiley);
            progressBar.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(true);
            reportDialog.setOnKeyListener(null);
            textViewReportErrorCode.setVisibility(View.VISIBLE);
            seprator.setVisibility(View.VISIBLE);
            tellUs.setVisibility(View.VISIBLE);
            tellUs.setText(StringConstants.tellUs);
            textViewReportFirstMsg.setText(StringConstants.visitorUploadErrorFirstMsg);
            textViewReportMsg.setText(StringConstants.kConstantSendStationeryRequestError);
            textViewReportErrorCode.setText(StringConstants.visitorUploadErrorCodeText + reportStatus);
            textViewReportMsg.setTextColor(Color.WHITE);
            relativeLayout.setBackgroundColor(Color.argb(247, 255, 74, 93));
            //tellUs.setTextColor(Color.rgb(159, 19, 55));
            seprator.setBackgroundColor(Color.rgb(198, 52, 90));
            textViewReportMsg.setTextSize(20);
            textViewReportFirstMsg.setMinHeight(80);
            textViewReportFirstMsg.setGravity(Gravity.TOP);
        } else if (whichActivityDialog == MainConstants.uploadCheckInOutFailure || reportStatus == MainConstants.reportCheckInCheckOutFailureError || reportStatus == ErrorConstants.checkInCheckOutRequestURLError || reportStatus == ErrorConstants.checkInCheckOutRequestJSONError || reportStatus == ErrorConstants.checkInCheckOutyRequestIOError || reportStatus == ErrorConstants.checkInCheckOutRequestJsonDataError) {
            relativeLayout.getLayoutParams().width = (int) (450 * context.getApplicationContext().getResources().getDisplayMetrics().density);
            reportImage.setVisibility(View.VISIBLE);
            textViewReportFirstMsg.setVisibility(View.VISIBLE);
            reportImage.setBackgroundResource(R.drawable.icon_dialog_failure_smiley);
            progressBar.setVisibility(View.GONE);
            reportDialog.setCanceledOnTouchOutside(true);
            reportDialog.setOnKeyListener(null);
            textViewReportErrorCode.setVisibility(View.VISIBLE);
            seprator.setVisibility(View.VISIBLE);
            tellUs.setVisibility(View.VISIBLE);
            tellUs.setText(StringConstants.tellUs);
            textViewReportFirstMsg.setText(StringConstants.visitorUploadErrorFirstMsg);
            textViewReportMsg.setText(StringConstants.forgotIdErrorMsg);
            textViewReportErrorCode.setText(StringConstants.visitorUploadErrorCodeText + reportStatus);
            textViewReportMsg.setTextColor(Color.WHITE);
            relativeLayout.setBackgroundColor(Color.argb(247, 255, 74, 93));
            //tellUs.setTextColor(Color.rgb(159, 19, 55));
            seprator.setBackgroundColor(Color.rgb(198, 52, 90));
            textViewReportMsg.setTextSize(20);
            textViewReportFirstMsg.setMinHeight(80);
            textViewReportFirstMsg.setGravity(Gravity.TOP);
        }

    }

    public static void hideDialog() {

        if (reportDialog != null && reportDialog.isShowing()) {
            reportDialog.dismiss();
        }
    }

    public static void setNullDialog() {
        if (textViewReportFirstMsg != null) {
            textViewReportFirstMsg = null;
        }
        if (textViewReportErrorCode != null) {
            textViewReportErrorCode = null;
        }
        if (editTextValue != null) {
            editTextValue = null;
        }
        if (buttonRetry != null) {
            buttonRetry = null;
        }
        if (linearLayoutRetry != null) {
            linearLayoutRetry = null;
        }
        if (fontFirstMsg != null) {
            fontFirstMsg = null;
        }
        if (fontMsg != null) {
            fontMsg = null;
        }
        if (reportDialog != null && !reportDialog.isShowing()) {
            reportDialog = null;
        }
        if (reportImage != null) {
            reportImage = null;
        }
        if (textViewReportMsg != null) {
            textViewReportMsg = null;
        }
        if (progressBar != null) {
            progressBar = null;
        }
        if (tellUs != null) {
            tellUs = null;
        }
        if (seprator != null) {
            seprator = null;
        }
        if (relativeLayout != null) {
            relativeLayout = null;
        }
    }
}