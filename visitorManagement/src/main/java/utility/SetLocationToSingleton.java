package utility;


import appschema.SingletonVisitorProfile;

import constants.MainConstants;

public class SetLocationToSingleton {
	private static int locationCode;
    private static String officeLocation;
    
	public static void setSingletonValue(double latitude,double longitude)
	{
//		//Log.d("utility", "current location" + latitude + " , " + longitude);
//        //Log.d("",""+ distance(latitude, longitude,
//               // MainConstants.kConstantZohoOfficeIndiaChennaiDLFLatitude,
//               // MainConstants.kConstantZohoOfficeIndiaChennaiDLFLongitude));
//
//
//		// if distance < 0.5 miles we take locations as equal
//		// do what you want to do...
//		if (distance(latitude, longitude,
//				MainConstants.kConstantZohoOfficeIndiaChennaiDLFLatitude,
//				MainConstants.kConstantZohoOfficeIndiaChennaiDLFLongitude) < MainConstants.distanceInMiles) {
//
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationIndiaChennaiDLF);
//            officeLocation = MainConstants.kConstantOfficeLocationIndiaChennaiDLF;
//            locationCode = MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF;
//
//			/*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiDLFLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiDLFLongitude);*/
//			// if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		} else if (distance(latitude, longitude,
//				MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLatitude,
//				MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLongitude) < MainConstants.distanceInMiles) {
//
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationIndiaChennaiEstancia);
//            officeLocation = MainConstants.kConstantOfficeLocationIndiaChennaiEstancia;
//            locationCode = MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia;
//
//            /*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLongitude);*/
//			// if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		}else if (distance(latitude, longitude,
//				MainConstants.kConstantZohoOfficeIndiaReniguntaLatitude,
//				MainConstants.kConstantZohoOfficeIndiaReniguntaLongitude) < MainConstants.distanceInMiles) {
//
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeIndiaRenigunta);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta);
//			officeLocation = MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta;
//			locationCode = MainConstants.kConstantOfficeLocationCodeIndiaRenigunta;
//
//            /*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLongitude);*/
//			// if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		}else  if (distance(
//				latitude,
//				longitude,
//				(double) MainConstants.kConstantZohoOfficeIndiaTenkasiLatitude,
//				(double) MainConstants.kConstantZohoOfficeIndiaTenkasiLongitude) < MainConstants.distanceInMiles) {
//			/*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeIndiaTenkasiLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeIndiaTenkasiLongitude);*/
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeIndiaTenkasi);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationIndiaTenkasi);
//            officeLocation = MainConstants.kConstantOfficeLocationIndiaTenkasi;
//            locationCode = MainConstants.kConstantOfficeLocationCodeIndiaTenkasi;
//
//            // if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		} else if (distance(
//				latitude,
//				longitude,
//				(double) MainConstants.kConstantZohoOfficeUSPleasantonLatitude,
//				(double) MainConstants.kConstantZohoOfficeUSPleasantonLongitude) < MainConstants.distanceInMiles) {
//			/*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeUSPleasantonLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeUSPleasantonLongitude);*/
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeUSPleasanton);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationUSPleasanton);
//            officeLocation = MainConstants.kConstantOfficeLocationUSPleasanton;
//            locationCode = MainConstants.kConstantOfficeLocationCodeUSPleasanton;
//
//
//            // if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		} else if (distance(latitude, longitude,
//				(double) MainConstants.kConstantZohoOfficeUSAustinLatitude,
//				(double) MainConstants.kConstantZohoOfficeUSAustinLongitude) < MainConstants.distanceInMiles) {
//			/*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeUSAustinLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeUSAustinLongitude);*/
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeUSAustin);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationUSAustin);
//            officeLocation = MainConstants.kConstantOfficeLocationUSAustin;
//            locationCode = MainConstants.kConstantOfficeLocationCodeUSAustin;
//
//
//            // if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		} else if (distance(
//				latitude,
//				longitude,
//				(double) MainConstants.kConstantZohoOfficeJapanYokohamaLatitude,
//				(double) MainConstants.kConstantZohoOfficeJapanYokohamaLongitude) < MainConstants.distanceInMiles) {
//			/*Log.d("getlocationlistener",
//					"latlngtrue"
//							+ MainConstants.kConstantZohoOfficeJapanYokohamaLatitude
//							+ " "
//							+ MainConstants.kConstantZohoOfficeJapanYokohamaLongitude);*/
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeJapanYokohama);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationJapanYokohama);
//            officeLocation = MainConstants.kConstantOfficeLocationJapanYokohama;
//            locationCode = MainConstants.kConstantOfficeLocationCodeJapanYokohama;
//
//
//            // if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		} else if (distance(
//				latitude,
//				longitude,
//				(double) MainConstants.kConstantZohoOfficeChinaBeijingLatitude,
//				(double) MainConstants.kConstantZohoOfficeChinaBeijingLongitude) < MainConstants.distanceInMiles) {
//			/*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeChinaBeijingLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeChinaBeijingLongitude);*/
//			SingletonVisitorProfile.getInstance().setLocationCode(
//					MainConstants.kConstantOfficeLocationCodeChinaBeijing);
//			SingletonVisitorProfile.getInstance().setOfficeLocation(
//					MainConstants.kConstantOfficeLocationChinaBeijing);
//            officeLocation = MainConstants.kConstantOfficeLocationChinaBeijing;
//            locationCode = MainConstants.kConstantOfficeLocationCodeChinaBeijing;
//
//
//            // if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//		}
//		 //Log.d("window","location:"+SingletonVisitorProfile.getInstance().getLocationCode());
	}
	private static double distance(double currentLatitude, double currentLongitude,
			double compareLatitude, double compareLongitude) {

		double earthRadius = 3958.75; // in miles, change to 6371 for
		// kilometer
		// output

		double dLat = Math.toRadians(compareLatitude - currentLatitude);
		double dLng = Math.toRadians(compareLongitude - currentLongitude);

		double sindLat = Math.sin(dLat / MainConstants.kConstantTwo);
		double sindLng = Math.sin(dLng / MainConstants.kConstantTwo);

		double lengthOfTwoPoints = Math.pow(sindLat, MainConstants.kConstantTwo)
				+ Math.pow(sindLng, MainConstants.kConstantTwo)
				* Math.cos(Math.toRadians(currentLatitude))
				* Math.cos(Math.toRadians(compareLatitude));

		double angularDistance = MainConstants.kConstantTwo
				* Math.atan2(Math.sqrt(lengthOfTwoPoints), Math
						.sqrt(MainConstants.kConstantOne - lengthOfTwoPoints));

		double dist = earthRadius * angularDistance;
		// Log.d("getLocationListener", "locationdistance" + dist);
		return dist; // output distance, in MILES
	}
}
