package utility;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appschema.VisitorOTPDetails;
import constants.ErrorConstants;
import constants.NumberConstants;
import constants.StringConstants;
import database.dbschema.EmployeeCheckInOut;
import utility.TimeZoneConventer;

import android.content.Context;
import android.util.Log;

import appschema.ExistingVisitorDetails;
import constants.MainConstants;


/**
 * parse
 *
 * @author jayapriya
 * @Created 05/09/2014
 */

public class JSONParser {

    static InputStream is = null;
    static JSONObject jsonObject = null;
    static String json = "";
    static JSONArray jsonArray;
    static long total;

    /**
     * /** Created by jayapriya On 25/10/14 get JsonObject and it store
     *
     * @param stringUrl is a web address in a specific character string that
     *                  constitutes a reference to a resource.
     * @return JSONObject get the result as JSONObject.
     * @throws IOException
     */
    public JSONObject getJsonArrayInGetMethod(String stringUrl) throws JSONException, TimeoutException, IOException {

        HttpURLConnection con = null;
        //Log.d("get", "method" +stringUrl);

        URL url = new URL(stringUrl);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(MainConstants.kConstantGetMethod);
        con.setDoInput(true);
        con.setReadTimeout(MainConstants.connectionTimeout);
        con.setConnectTimeout(MainConstants.connectionTimeout);

        con.connect();
        int status = con.getResponseCode();
        //Log.d("get", "method" + status);
        if (status == MainConstants.kConstantSuccessCode) {
            //https://creator.zoho.com/api/json/copy-of-visitormanagement/view/Visitor_Report?authtoken=e05d9de0d0fd1466825ef6bb74215072&scope=creatorapi&raw=true&zc_ownername=frontdesk10&criteria=Name=="917906467809"
            //https://creator.zoho.com/api/json/copy-of-visitormanagement/view/Visitor_Report?authtoken=e05d9de0d0fd1466825ef6bb74215072&scope=creatorapi&raw=true&zc_ownername=frontdesk10&criteria=Phone=="916753243356"
            //https://creator.zoho.com/api/json/copy-of-zohonow-interview/view/interview_report?authtoken=e05d9de0d0fd1466825ef6bb74215072&scope=creatorapi&raw=true&zc_ownername=frontdesk10
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = in.readLine()) != null) {
                stringBuilder.append(line + MainConstants.kConstantNewLine);
            }
            //Log.d("get","response"+stringBuilder.toString());
            json = stringBuilder.toString();

            if ((json != MainConstants.kConstantEmpty)
                    && (json.length() > MainConstants.kConstantZero)
                    && (json != null)) {
                jsonObject = new JSONObject(json);
                return jsonObject;
            }
        }
        return null;
    }

    /**
     * created by jayapriya On 25/10/2014
     * <p>
     * read the response in post method
     *
     * @param url
     * @param setParameter
     * @return JSONObject
     * @throws IOException
     * @throws JSONException
     */
    public JSONObject getJsonArrayInPostMethod(URL url, String setParameter) throws IOException, JSONException {

//        Log.d("DSK ","postmethod "+url+setParameter);
        HttpURLConnection con = null;
        con = (HttpURLConnection) url.openConnection();
        //Log.d("get","update");
        con.setDoOutput(true);
        //Log.d("get","update");
        con.setRequestMethod(MainConstants.kConstantPostMethod);
        //Log.d("get","update");
        con.setRequestProperty(MainConstants.kConstantUserAgent,
                MainConstants.user_Agent);
        //Log.d("get","update");
        con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                MainConstants.acceptLanguage);
        con.setConnectTimeout(MainConstants.connectionTimeout);
        con.setReadTimeout(MainConstants.readTimeout);

        //Log.d("get","update");
        DataOutputStream wr = new DataOutputStream(
                con.getOutputStream());

        wr.writeBytes(setParameter);
        wr.flush();
        wr.close();

        int status = con.getResponseCode();
//        Log.d("post ","getResponseCode "+status );
        if (status == MainConstants.kConstantSuccessCode) {
            // read the response
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            StringBuffer response = new StringBuffer();
            String line = null;

            while ((line = in.readLine()) != null) {

                response.append(line + MainConstants.kConstantNewLine);
                json = response.toString();

            }

            jsonObject = new JSONObject(response.toString());
        }
//        Log.d("DSK Post Method ","ResponseMessage "+con.getResponseMessage());
//        Log.d("DSK Post Method ","ErrorStream "+con.getErrorStream());
        return jsonObject;
    }


    /**
     * Download the image in People api using employee zuid and store image into
     * device. And store the image path into singleton variable.
     *
     * @param zuid zuid is the employeezuid, this id is used to identify the
     *             imagefile.
     * @author jayapriya
     * @created 23/02/2015
     */
    public static String downloadEmployeeImage(String zuid) {
        int count;
        String photoUrl = null;
        try {
            photoUrl = MainConstants.kConstantDownloadEmployeeImage + zuid
                    + MainConstants.kConstantImageSource;
            //Log.d("photo","url" +photoUrl);
            URL url = new URL(photoUrl);
            URLConnection conection = url.openConnection();
            conection.connect();
            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);
            File fileDirectory = new File(MainConstants.kConstantFileDirectory
                    + MainConstants.kConstantFileEmployeeImageFolder);

            if (!fileDirectory.exists()) {
                fileDirectory.mkdirs();
            }
            final File file = new File(fileDirectory.getAbsolutePath(),
                    zuid + MainConstants.kConstantImageFileFormat);

            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            // Output stream to write file
            OutputStream output = new FileOutputStream(file);
            byte data[] = new byte[MainConstants.kConstantBufferSize];
            total = MainConstants.kConstantZero;
            while ((count = input.read(data)) != MainConstants.kConstantMinusOne) {
                total += count;
                // writing data to file
                output.write(data, MainConstants.kConstantZero, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();

            //SingletonEmployeeProfile.getInstance().setEmployeePhoto(
            //MainConstants.kConstantFileDirectory
            //+ MainConstants.kConstantFileTemporaryFolder
            //+ SingletonEmployeeProfile.getInstance()
            //	.getEmployeeFirstName()
            //+ MainConstants.kConstantImageFileFormat);

            photoUrl = MainConstants.kConstantFileDirectory
                    + MainConstants.kConstantFileEmployeeImageFolder
                    + zuid
                    + MainConstants.kConstantImageFileFormat;

        } catch (IOException e) {

            photoUrl = MainConstants.kConstantEmpty;
            //Log.d("ioexception","" +e);
            //SingletonEmployeeProfile.getInstance().setEmployeePhoto(
            //MainConstants.kConstantEmpty);
        }
        return photoUrl;

    }

    /**
     * Parse Exist visitor details
     *
     * @param response
     * @return existingVisitorDetailsList
     * @throws JSONException
     * @author Karthick
     * @created 20170525
     */
    public ArrayList<ExistingVisitorDetails> parseExitVisitorDetails(String response) throws JSONException {
        ArrayList<ExistingVisitorDetails> existingVisitorDetailsList = new ArrayList<ExistingVisitorDetails>();
        if (response != null && !response.isEmpty()) {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);

            if (jsonObject != null) {
                JSONArray fromJsonArray = null;

                fromJsonArray = jsonObject.getJSONArray(StringConstants.parseExitVisitorForm);

                for (int i = MainConstants.kConstantZero; fromJsonArray != null && i < fromJsonArray.length(); i++) {
                    JSONObject jsonObjectLoop = new JSONObject();
                    try {
                        jsonObjectLoop = (JSONObject) fromJsonArray.get(i);
                    } catch (JSONException jsonExcep) {

                    }
                    ExistingVisitorDetails existingVisitorDetailsDetails = new ExistingVisitorDetails();
                    if (jsonObjectLoop != null && existingVisitorDetailsDetails != null) {

                        existingVisitorDetailsDetails.setCreatorVisitID(MainConstants.kConstantEmpty);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitId) != null) {
                                existingVisitorDetailsDetails.setCreatorVisitID(jsonObjectLoop.getString(StringConstants.parseExitVisitId));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setCreatorVisitID(MainConstants.kConstantEmpty);
                        }
                        existingVisitorDetailsDetails.setName(MainConstants.kConstantEmpty);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitorName) != null) {
                                existingVisitorDetailsDetails.setName(jsonObjectLoop.getString(StringConstants.parseExitVisitorName));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setName(MainConstants.kConstantEmpty);
                        }

                        existingVisitorDetailsDetails.setEmployeeEmail(MainConstants.kConstantEmpty);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitEmployeeEmail) != null) {
                                existingVisitorDetailsDetails.setEmployeeEmail(jsonObjectLoop.getString(StringConstants.parseExitEmployeeEmail));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setEmployeeEmail(MainConstants.kConstantEmpty);
                        }

//                        Added for DataCenter Mode to track employee Company Name
                        existingVisitorDetailsDetails.setCompanyName(MainConstants.kConstantEmpty);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitorCompany) != null) {
                                existingVisitorDetailsDetails.setCompanyName(jsonObjectLoop.getString(StringConstants.parseExitVisitorCompany));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setCompanyName(MainConstants.kConstantEmpty);
                        }

                        existingVisitorDetailsDetails.setPurpose(MainConstants.kConstantEmpty);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitPurpose) != null) {
                                existingVisitorDetailsDetails.setPurpose(jsonObjectLoop.getString(StringConstants.parseExitVisitPurpose));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setPurpose(MainConstants.kConstantEmpty);
                        }

                        existingVisitorDetailsDetails.setContact(MainConstants.kConstantEmpty);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitorEmail) != null) {
                                existingVisitorDetailsDetails.setContact(jsonObjectLoop.getString(StringConstants.parseExitVisitorEmail));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setContact(MainConstants.kConstantEmpty);
                        }
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitorPhone) != null) {
                                existingVisitorDetailsDetails.setContact(jsonObjectLoop.getString(StringConstants.parseExitVisitorPhone));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setContact(MainConstants.kConstantEmpty);
                        }
                        existingVisitorDetailsDetails.setVisitedDate(MainConstants.kConstantZero);
                        try {
                            if (jsonObjectLoop != null && jsonObjectLoop.getString(StringConstants.parseExitVisitAddedTime) != null) {
                                existingVisitorDetailsDetails.setVisitedDate(TimeZoneConventer
                                        .getDateTimeInMilliSeconds(jsonObjectLoop.getString(StringConstants.parseExitVisitAddedTime)));
                            }
                        } catch (JSONException jsonExcep) {
                            existingVisitorDetailsDetails.setVisitedDate(MainConstants.kConstantZero);
                        }

                        if (existingVisitorDetailsList != null && existingVisitorDetailsDetails != null) {
                            existingVisitorDetailsList.add(existingVisitorDetailsDetails);
                        }
                    }
                }
            }
        }
        return existingVisitorDetailsList;
    }

    /**
     * Parse Vendor Details Add Response and return Vendor Creator ID
     *
     * @author Karthikeyan D S
     * @created on 20180222
     */
    public ArrayList<EmployeeCheckInOut> parseEmployeeCheckINDetails(String response) {
        String mvendorAddressID = MainConstants.kConstantEmpty;
        EmployeeCheckInOut mVendorAddressList = new EmployeeCheckInOut();
        ArrayList<EmployeeCheckInOut> checkedInCheckedOutDetails = new ArrayList<EmployeeCheckInOut>();
//		Log.d("DSK response ",""+response);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
        } catch (JSONException e) {
//			setInternalLogs(context,ErrorConstants.VendorDetailsJSONDataEmptyError,e.getLocalizedMessage());
        }
        if (jsonObject != null) {
            jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
            JSONObject jsonSubObject = null;
            try {
                if (jsonArray != null) {
                    jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
                }
            } catch (JSONException e) {
//				setInternalLogs(context,ErrorConstants.parseVendorAddressJSONDataEmptyError,e.getLocalizedMessage());
            }

            JSONArray jsonSubArray = null;
            try {
                if (jsonSubObject != null) {
                    jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
                }
            } catch (JSONException e) {
//				setInternalLogs(context,ErrorConstants.VendorAddressDetailsJSONDataEmptyError,e.getLocalizedMessage());
//                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, StringConstants.emptyString);
//                    return ErrorConstants.visitorJsonDataJSONError;
            }
            JSONObject jsonSubOfSubObject = null;
            try {
                if (jsonSubArray != null) {
                    jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
                }
            } catch (JSONException e) {
//				setInternalLogs(context,ErrorConstants.VendorAddressDetailsJSONDataEmptyError,e.getLocalizedMessage());
//                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, StringConstants.emptyString);
//                    return ErrorConstants.visitorJsonDataJSONError;
            }
            JSONObject jsonSubOfSubArray = null;
            try {
                if (jsonSubOfSubObject != null) {
                    jsonSubOfSubArray = jsonSubOfSubObject.getJSONObject(MainConstants.kConstantValues);
                }
            } catch (JSONException e) {
//				setInternalLogs(context,ErrorConstants.VendorAddressDetailsJSONDataEmptyError,e.getLocalizedMessage());
//                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, StringConstants.emptyString);
//                    return ErrorConstants.visitorJsonDataJSONError;
            }

            try {
                if (jsonSubOfSubArray != null) {
                    //Visitor  Creator ID
                    if (jsonSubOfSubArray.getString(MainConstants.kConstantVisitorId) != null &&
                            !jsonSubOfSubArray.getString(MainConstants.kConstantVisitorId).isEmpty()) {
//						Log.d("Message",""+jsonSubOfSubArray.getString(MainConstants.parseExitVisitId));
                        mVendorAddressList.setIsSynced(jsonSubOfSubArray.getString(MainConstants.kConstantVisitorId));
                        checkedInCheckedOutDetails.add(mVendorAddressList);
                    }
                }
            } catch (JSONException e) {
//				setInternalLogs(context,ErrorConstants.parseVendorAddressJSONException,e.getLocalizedMessage());
            }
        }

        return checkedInCheckedOutDetails;
    }

    /**
     * Call GET or POST method Rest API call
     *
     * @param restApiMethod String
     * @param url           URL
     * @param setParameter  String
     * @return responseString String
     * @throws IOException
     * @author Karthikeyan D S
     * @created 09July2018
     */
    public String restAPICall(String restApiMethod, URL url, String setParameter) throws IOException {
        String responseString = MainConstants.kConstantEmpty;
        if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
            Log.d("DSK RestapiMethod", "" + restApiMethod);
            Log.d("DSK RestapiURL", "" + url + setParameter);
        }
        if (restApiMethod != null && !restApiMethod.isEmpty() && url != null && !url.toString().isEmpty()) {
            HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();
            if (con != null) {
                con.setDoOutput(true);
                con.setRequestMethod(restApiMethod);

                con.setConnectTimeout(NumberConstants.connectionTimeout);
                con.setReadTimeout(NumberConstants.connectionTimeout);

                if (restApiMethod == MainConstants.kConstantPostMethod && setParameter != null && !setParameter.isEmpty()) {
                    con.setRequestProperty(MainConstants.kConstantUserAgent,
                            MainConstants.user_Agent);
                    con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                            MainConstants.acceptLanguage);
                    DataOutputStream dataOutputStream = new DataOutputStream(
                            con.getOutputStream());
                    if (dataOutputStream != null) {
                        dataOutputStream.writeBytes(setParameter);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                    }
                } else {
//                    con.connect();
                }
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatus ", "message " + con.getResponseMessage() + "");
                }
                int status = con.getResponseCode();
                if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
                    Log.d("DSK restApiStatusCode", status + "");
                }
                if (status == NumberConstants.kConstantSuccessCode) {
                    // read the response
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

                    StringBuffer response = new StringBuffer();
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        response.append(line + MainConstants.kConstantNewLine);
                    }
                    responseString = response.toString();
                    if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                        Log.d("DSK ", "response:" + responseString);
                    }
                }
            }
        }
        return responseString;
    }

    /**
     * Parse Employee OTP is Valid Response
     *
     * @author Karthikeyan D S
     * @created on 17JUL2018
     */
    public VisitorOTPDetails parseVisitorOTPisValidResponse(Context context, String response) {
        String creatorID = MainConstants.kConstantEmpty;
//        Log.d("dsk response", "ActivityItemDetails" + response);
        VisitorOTPDetails visitorOTPDetails = null;

        if (response != null && !response.isEmpty() && (response.length() > NumberConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kconstantVisitorOTPDetails);
            } else {
                return null;
            }
            if (jsonArray != null) {
                visitorOTPDetails = new VisitorOTPDetails();
//                 Log.d("jsonArray", "array" + jsonArray);
                for (int i = NumberConstants.kConstantZero; i < jsonArray.length(); i++) {
//                     Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    try {
                        visitorOTPDetails.setmCreatorID(jsonArray.getJSONObject(i).getString(MainConstants.kConstantVisitorId));
                    } catch (JSONException e) {
//                        setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsResponseParseJSONDataError, e.getLocalizedMessage());
                    }
                    try {
                        visitorOTPDetails.setmOTPRequestedTime(jsonArray.getJSONObject(i).getString(MainConstants.constantOTPRequestDateTime));
                    } catch (JSONException e) {
//                        setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsResponseParseJSONDataError, e.getLocalizedMessage());
                    }
                    try {
                        visitorOTPDetails.setmOTPValidity(jsonArray.getJSONObject(i).getString(MainConstants.constantOTPValidity));
                    } catch (JSONException e) {
//                        setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsResponseParseJSONDataError, e.getLocalizedMessage());
                    }
                    try {
                        String dateTime = jsonArray.getJSONObject(i).getString(MainConstants.constantOTPValidity);
                        long dateTimeLong = NumberConstants.kConstantMinusOne;
//                        format Date Value as Long to Process Internally
                         dateTimeLong = getDateTimeLong(dateTime);
                        if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                            Log.d("DSK OTP ", " dateTimeLong " + dateTimeLong);
                        }
                        visitorOTPDetails.setmOTPValidityLong(dateTimeLong);
                    } catch (JSONException e) {
//                        setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsResponseParseJSONDataError, e.getLocalizedMessage());
                    }
                    try {
                        visitorOTPDetails.setmRequestedOTPNumber(jsonArray.getJSONObject(i).getString(MainConstants.constantOTPValue));
                    } catch (JSONException e) {
//                        setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsResponseParseJSONDataError, e.getLocalizedMessage());
                    }
                }
            }
        }
        if(NumberConstants.kConstantProduction==NumberConstants.kConstantZero) {
            Log.d("DSK OTP ", "CreatorID " + creatorID );
        }
        return visitorOTPDetails;
    }


    /**
     * return Current Date in ANDROID long value
     *
     * @author Karthikeyan D S
     * @created 17JUL2018
     */
    private long getDateTimeLong(String dateTime)
    {
//        Log.d("DSK ", "datetime " + dateTime);
        long joinDate = NumberConstants.kConstantMinusOne;
        if(dateTime!=null && !dateTime.isEmpty()) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss");
                Date date = sdf.parse(dateTime);
                joinDate = date.getTime();
//                Log.d("DSK ", "datetime " + date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return joinDate;
    }

}
