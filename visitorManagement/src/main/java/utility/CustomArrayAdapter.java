package utility;

import java.util.List;

import com.zoho.app.frontdesk.android.ApplicationController;
import com.zoho.app.frontdesk.android.R;

import constants.FontConstants;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Set font style for adaptor view
 * 
 * @author Karthick
 * @created 20160620
 */
public class CustomArrayAdapter extends ArrayAdapter<String> {
	private Typeface fontStyle,fontRegular;
	private int dropdownFontSize = 26;
    private Context context;
    public CustomArrayAdapter(Context context, int textViewResourceId,List<String> listProjects) {
        super(context, textViewResourceId,listProjects);
      //font
		fontStyle = Typeface.createFromAsset(ApplicationController.getInstance().getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		fontRegular = Typeface.createFromAsset(ApplicationController.getInstance().getAssets(),
				FontConstants.fontConstantHKGroteskSemiBold);
		 dropdownFontSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20, context.getResources().getDisplayMetrics());
        this.context=context;
    }

    public TextView getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) super.getView(position, convertView, parent);
        if(textView!=null && fontStyle!=null) {
        	textView.setTypeface(fontStyle);
        textView.setPadding(20, 10, 20, 10);

            if (context != null) {
                int side = (int) context.getResources().getDimension(
                        R.dimen.custom_adaptor_side);
                int top = (int) context.getResources().getDimension(
                        R.dimen.custom_adaptor_top);
                textView.setPadding(side, top, side, top);
                int dropdownFontSize = (int) context.getResources().getDimension(
                        R.dimen.visitor_edittext);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
            }
        textView.setTextColor(Color.WHITE);
        	textView.setGravity(Gravity.CENTER);
        	textView.setAllCaps(true);
        }
        return textView;
    }

    public TextView getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) super.getView(position, convertView, parent);
        
        if(textView!=null && fontRegular!=null) {
        	textView.setPadding(20, 30, 20, 30);
        textView.setTypeface(fontRegular);textView.setTextSize(28);
            if (context != null) {
                int dropdownFontSize = (int) context.getResources().getDimension(
                        R.dimen.custom_adaptor_dropdown_text);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
            }
        textView.setTextColor(Color.DKGRAY);
        textView.setBackgroundColor(Color.WHITE);
        textView.setAllCaps(true);
        }
        return textView;
    }


}

