package utility;

import appschema.SingletonVisitorProfile;

import constants.MainConstants;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * 
 * @author jayapriya On 07/11/14 Compare the officeLocation and current location
 *         to get the location code and officeLocation.
 * 
 */
public class CompareLocationListener implements LocationListener {

    private int locationCode;
    private String officeLocation;
    Context context;

	@Override
	public void onLocationChanged(Location location) {
//		//Log.d("utility", "locationChanged");
//		if (location != null) {
//			// This needs to stop getting the location data and save the battery
//			// power.
//
//			double longitude = location.getLongitude();
//			double latitude = location.getLatitude();
////			SingletonVisitorProfile.getInstance().setLongLat();
////			Log.d("utility", "current location" + latitude + " , " + longitude);
//            //Log.d("",""+ distance(latitude, longitude,
//                   // MainConstants.kConstantZohoOfficeIndiaChennaiDLFLatitude,
//                    //MainConstants.kConstantZohoOfficeIndiaChennaiDLFLongitude));
//
//
//			// if distance < 0.5 miles we take locations as equal
//			// do what you want to do...
//			if (distance(latitude, longitude,
//					MainConstants.kConstantZohoOfficeIndiaChennaiDLFLatitude,
//					MainConstants.kConstantZohoOfficeIndiaChennaiDLFLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationIndiaChennaiDLF);
//                officeLocation = MainConstants.kConstantOfficeLocationIndiaChennaiDLF;
//                locationCode = MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF;
//
//
//				// if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			} else if (distance(latitude, longitude,
//					MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLatitude,
//					MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationIndiaChennaiEstancia);
//                officeLocation = MainConstants.kConstantOfficeLocationIndiaChennaiEstancia;
//                locationCode = MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia;
//
//                //Log.d("getlocationlistener", "latlngtrue"
//					//	+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLatitude
//					//	+ " "
//					//	+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLongitude);
//				// if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			}else if (distance(latitude, longitude,
//					MainConstants.kConstantZohoOfficeIndiaReniguntaLatitude,
//					MainConstants.kConstantZohoOfficeIndiaReniguntaLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeIndiaRenigunta);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta);
//				officeLocation = MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta;
//				locationCode = MainConstants.kConstantOfficeLocationCodeIndiaRenigunta;
//
//            /*Log.d("getlocationlistener", "latlngtrue"
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLatitude
//					+ " "
//					+ MainConstants.kConstantZohoOfficeIndiaChennaiEstanciaLongitude);*/
//				// if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			}else  if (distance(
//					latitude,
//					longitude,
//					(double) MainConstants.kConstantZohoOfficeIndiaTenkasiLatitude,
//					(double) MainConstants.kConstantZohoOfficeIndiaTenkasiLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeIndiaTenkasi);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationIndiaTenkasi);
//                officeLocation = MainConstants.kConstantOfficeLocationIndiaTenkasi;
//                locationCode = MainConstants.kConstantOfficeLocationCodeIndiaTenkasi;
//
//                // if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			} else if (distance(
//					latitude,
//					longitude,
//					(double) MainConstants.kConstantZohoOfficeUSPleasantonLatitude,
//					(double) MainConstants.kConstantZohoOfficeUSPleasantonLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeUSPleasanton);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationUSPleasanton);
//                officeLocation = MainConstants.kConstantOfficeLocationUSPleasanton;
//                locationCode = MainConstants.kConstantOfficeLocationCodeUSPleasanton;
//
//
//                // if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			} else if (distance(latitude, longitude,
//					(double) MainConstants.kConstantZohoOfficeUSAustinLatitude,
//					(double) MainConstants.kConstantZohoOfficeUSAustinLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeUSAustin);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationUSAustin);
//                officeLocation = MainConstants.kConstantOfficeLocationUSAustin;
//                locationCode = MainConstants.kConstantOfficeLocationCodeUSAustin;
//
//
//                // if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			} else if (distance(
//					latitude,
//					longitude,
//					(double) MainConstants.kConstantZohoOfficeJapanYokohamaLatitude,
//					(double) MainConstants.kConstantZohoOfficeJapanYokohamaLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeJapanYokohama);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationJapanYokohama);
//                officeLocation = MainConstants.kConstantOfficeLocationJapanYokohama;
//                locationCode = MainConstants.kConstantOfficeLocationCodeJapanYokohama;
//
//
//                // if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
//			} else if (distance(
//					latitude,
//					longitude,
//					(double) MainConstants.kConstantZohoOfficeChinaBeijingLatitude,
//					(double) MainConstants.kConstantZohoOfficeChinaBeijingLongitude) < MainConstants.distanceInMiles) {
//
//				SingletonVisitorProfile.getInstance().setLocationCode(
//						MainConstants.kConstantOfficeLocationCodeChinaBeijing);
//				SingletonVisitorProfile.getInstance().setOfficeLocation(
//						MainConstants.kConstantOfficeLocationChinaBeijing);
//                officeLocation = MainConstants.kConstantOfficeLocationChinaBeijing;
//                locationCode = MainConstants.kConstantOfficeLocationCodeChinaBeijing;
//
//
//                // if distance < 0.5 miles we take locations as equal
//				// do what you want to do...
////			} else  if (distance(
////					latitude,
////					longitude,
////					(double) MainConstants.kConstantDeveloperTestLatitude,
////					(double) MainConstants.kConstantDeveloperTestLongitude) < MainConstants.distanceInMiles) {
////
////				SingletonVisitorProfile.getInstance().setLocationCode(
////						MainConstants.kConstantDeveloperOfficeLocationCode);
////				SingletonVisitorProfile.getInstance().setOfficeLocation(
////						MainConstants.kConstantDeveloperOfficeLocation);
////				officeLocation = MainConstants.kConstantDeveloperOfficeLocation;
////				locationCode = MainConstants.kConstantDeveloperOfficeLocationCode;
////
////				// if distance < 0.5 miles we take locations as equal
////				// do what you want to do...
////			}else {
//
//				//Assume location as chennai,India
//				//Assume location code as 1
//				//Log.d("getlocationlistener", "latlngfalse");
//				SingletonVisitorProfile.getInstance().setLocationCode(MainConstants.kConstantMinusTwo);
//
//                //Log.d("compare", "singletonCode:"+ SingletonVisitorProfile.getInstance().getLocationCode());
//                //Log.d("compare", "singletonCode:"+ SingletonVisitorProfile.getInstance().getOfficeLocation());
//			}
//			/* if(locationCode != SingletonVisitorProfile.getInstance().getLocationCode()) {
//            	Log.d("compare", "singletonCode if"
//                        + SingletonVisitorProfile.getInstance().getOfficeLocation()+":"+locationCode);
//
//                //SingletonVisitorProfile.getInstance().setLocationCode(locationCode);
//                //SingletonVisitorProfile.getInstance().setOfficeLocation(officeLocation);
//                //context.startService(new Intent(context, com.ikosmik.app_visitormgmt_android.getVisitPurpose.class));
//            }*/
//
//		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	/*
	 * Created by Jayapriya On 07/11/14calculates the distance between two
	 * locations in MILES
	 * 
	 * the Haversine Formula (shown below) expressed in terms of a two-argument
	 * inverse tangent function to calculate the great circle distance between
	 * two points on the Earth. This is the method recommended for calculating
	 * short distances
	 * 
	 * dlon = lon2 - lon1 dlat = lat2 - lat1 a = (sin(dlat/2))^2 + cos(lat1) *
	 * cos(lat2) * (sin(dlon/2))^2 c = 2 * atan2( sqrt(a), sqrt(1-a) ) d = R * c
	 *  c is the angular distance in radians, and a is the square of half the
	 * chord length between the points. (where R is the radius of the Earth)
	 */

	private double distance(double currentLatitude, double currentLongitude,
			double compareLatitude, double compareLongitude) {

		double earthRadius = 3958.75; // in miles, change to 6371 for
		// kilometer
		// output

		double dLat = Math.toRadians(compareLatitude - currentLatitude);
		double dLng = Math.toRadians(compareLongitude - currentLongitude);

		double sindLat = Math.sin(dLat / MainConstants.kConstantTwo);
		double sindLng = Math.sin(dLng / MainConstants.kConstantTwo);

		double lengthOfTwoPoints = Math.pow(sindLat, MainConstants.kConstantTwo)
				+ Math.pow(sindLng, MainConstants.kConstantTwo)
				* Math.cos(Math.toRadians(currentLatitude))
				* Math.cos(Math.toRadians(compareLatitude));

		double angularDistance = MainConstants.kConstantTwo
				* Math.atan2(Math.sqrt(lengthOfTwoPoints), Math
						.sqrt(MainConstants.kConstantOne - lengthOfTwoPoints));

		double dist = earthRadius * angularDistance;
		// Log.d("getLocationListener", "locationdistance" + dist);
		return dist; // output distance, in MILES
	}

}
