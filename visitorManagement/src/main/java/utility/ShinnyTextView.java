package utility;

/*
 * Copyright 2014 Pierre Degand
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * https://gist.github.com/pdegand/835d3598b4b3343ba545
 */

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.util.AttributeSet;

import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

public class ShinnyTextView extends TextView {

	private boolean isColorChanged;
	private float mGradientDiameter = 0.4f;
	private int colorNow = Color.rgb(238, 17, 17), colorChangeCount = 0;
	private ValueAnimator mAnimator;
	private float mGradientCenter;
	private PaintDrawable mShineDrawable;
	private Shader shader = null;

	public ShinnyTextView(final Context context) {
		this(context, null);
	}

	public ShinnyTextView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ShinnyTextView(final Context context, final AttributeSet attrs,
			final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	}

	protected void onSizeChanged(final int w, final int h, final int oldw,
			final int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		// Log.d("AnimationUpdate", "onSizeChanged");
		if (!isInEditMode()) {
			if (mAnimator != null) {
				mAnimator.cancel();
			}

			mShineDrawable = new PaintDrawable();
			mShineDrawable.setBounds(0, 0, w, h);
			mShineDrawable.getPaint().setShader(
					generateGradientShader(getWidth(), 0, 0, 0));
			mShineDrawable.getPaint().setXfermode(
					new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

			mAnimator = ValueAnimator.ofFloat(0, 1);
			mAnimator.setDuration(4 * w); // custom
			mAnimator.setRepeatCount(ValueAnimator.INFINITE);
			mAnimator.setRepeatMode(ValueAnimator.RESTART);
			mAnimator.setInterpolator(new LinearInterpolator()); // Custom
			mAnimator
					.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

						public void onAnimationUpdate(
								final ValueAnimator animation) {

							// Log.d("AnimationUpdate", "onAnimationUpdate");
							final float value = animation.getAnimatedFraction();
							mGradientCenter = (1 + 2 * mGradientDiameter)
									* value - mGradientDiameter;
							final float gradientStart = mGradientCenter
									- mGradientDiameter;
							final float gradientEnd = mGradientCenter
									+ mGradientDiameter;
							shader = generateGradientShader(w, gradientStart,
									mGradientCenter, gradientEnd);
							mShineDrawable.getPaint().setShader(shader);
							invalidate();
							if (value > 0.0 && value < 0.1 && !isColorChanged) {
								colorChange();
								// Log.d("AnimationUpdate",
								// "onAnimationUpdate"+value);
								isColorChanged = true;
							}
							if (value > 0.9) {
								isColorChanged = false;

							}
						}
					});
			mAnimator.start();
		}
	}

	protected void onDraw(final Canvas canvas) {
		super.onDraw(canvas);

		if (!isInEditMode() && mShineDrawable != null) {

			mShineDrawable.draw(canvas);
		}
	}

	private Shader generateGradientShader(int width, float... positions) {
		// Log.d("AnimationUpdate", "generateGradientShader");
		int[] colorRepartition = { colorNow, Color.WHITE, colorNow };
		return new LinearGradient(0, 0, width, 0, colorRepartition, positions,
				Shader.TileMode.REPEAT);
	}

	public void colorChange() {

		if (colorChangeCount == 0) {
			colorNow = Color.rgb(7, 170, 238);
		} else if (colorChangeCount == 1) {
			colorNow = Color.rgb(13, 210, 117);
		} else if (colorChangeCount == 2) {
			colorNow = Color.rgb(252, 78, 74);
		} else if (colorChangeCount == 3) {
			colorNow = Color.rgb(192, 173, 34);
		}
		colorChangeCount++;
		if (colorChangeCount > 3) {
			colorChangeCount = 0;
		}
	}
}