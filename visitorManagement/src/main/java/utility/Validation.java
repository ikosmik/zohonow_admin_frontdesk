package utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonVisitorProfile;
import constants.MainConstants;

/*
 * validation
 *
 * @author jayapriya
 * @created 25/09/14
 */
public class Validation {

	/**
	 * Return valid mobile number
	 * 
	 * @author Karthick
	 * @created on 20161115
	 */
	public String getValidMobileNumber(String mobileNumber) {

		if (mobileNumber != null) {
			mobileNumber = mobileNumber.replaceAll(
					MainConstants.kConstantRemoveNonNumeric,
					MainConstants.kConstantEmpty);
		}

		if (mobileNumber != null && !mobileNumber.isEmpty()) {
			if (mobileNumber.length() <= MainConstants.kConstantTen ) {
				return mobileNumber;
			} else if (mobileNumber.length() > MainConstants.kConstantTen) {
				return mobileNumber.substring(mobileNumber.length()
						- MainConstants.kConstantTen);
			}
		}
		return mobileNumber;
	}

	/**
	 * validate mobileNo
	 * 
	 * @param mobileNo
	 * @return
	 * @author jayapriya
	 * @created 25/09/2014
	 */
	public boolean validateMobileNo(String mobileNo) {

		if ((mobileNo != null)
				&& (mobileNo.matches(MainConstants.kConstantValidPhoneNo) == true)) {
			return true;
		} else {
			return false;
		}

	}

	public boolean validateIndiaMobileNo(String mobileNo) {

		if ((mobileNo != null)
				&& (mobileNo.matches(MainConstants.kConstantValidIndianPhoneNo) == true)) {
			return true;
		} else {
			return false;
		}

	}
	/**
	 * validate text
	 * 
	 * @param text
	 * @return
	 * @author jayapriya
	 * @created 25/09/2014
	 */
	public boolean validateName(String text) {
		if ((text != null)
				&& (text.matches(MainConstants.kConstantValidText) == true)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * validate text
	 * 
	 * @param emailId
	 * @return
	 * @author jayapriya
	 * @created 25/09/2014
	 */
	public boolean validateMail(String emailId) {

		Pattern pattern;
		Matcher m;
		pattern = Pattern.compile(MainConstants.kConstantValidEmail);

		m = pattern.matcher(emailId);
		// check = m.matches();

		if (m.matches() == true) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * created by jayapriya On 25/10/14
	 * 
	 * @param oldrecord
	 * @param newrecord
	 * @return
	 */
	public String CompareCreatorRecord(String oldrecord, String newrecord) {

		if ((newrecord != null)
				&& (newrecord.length() > MainConstants.kConstantZero)
				&& (oldrecord.equals(newrecord) != true)) {
			return newrecord;
		} else {
			return oldrecord;
		}

	}

	/*
	 * Created by 25/10/14 Validate the contact details.
	 */
	public boolean validateContacddtDetails(String phoneNo, String email) {

		if ((phoneNo == null) && (email == null)) {

			return false;
		} else {
			if ((phoneNo == null)
					|| (phoneNo.trim().length() == MainConstants.kConstantZero))

			{

				if (validateMail(email) == true) {
					SingletonVisitorProfile.getInstance().setContact(email);
					SingletonVisitorProfile.getInstance().setEmail(email);
					// Log.d("text","email"
					// +SingletonVisitorProfile.getInstance().getEmail());
					return true;
				} else {
					return false;
				}
			} else if (email == null) {

				if (validateMobileNo(phoneNo) == true) {
					SingletonVisitorProfile.getInstance().setContact(phoneNo);
					SingletonVisitorProfile.getInstance().setPhoneNo(phoneNo);

					return true;
				} else {

					return false;
				}
			} else {
				if (validateMobileNo(phoneNo) == true) {
					SingletonVisitorProfile.getInstance().setContact(phoneNo);
					SingletonVisitorProfile.getInstance().setPhoneNo(phoneNo);
					return true;
				} else if (validateMail(email) == true) {
					SingletonVisitorProfile.getInstance().setContact(email);
					SingletonVisitorProfile.getInstance().setEmail(email);
					return true;
				}
			}
			return true;
		}

	}

	public boolean validateContact(String phoneNo, String email) {
		if (((phoneNo == null) || (phoneNo == MainConstants.kConstantEmpty) || (phoneNo
				.length() == MainConstants.kConstantZero))
				&& ((email == null) || (email == MainConstants.kConstantEmpty) || (email
						.length() == MainConstants.kConstantZero))) {

			SingletonVisitorProfile.getInstance().setContact(
					MainConstants.kConstantEmpty);
			return false;
		} else if (((phoneNo != null)
				&& (phoneNo != MainConstants.kConstantEmpty) && (phoneNo
				.length() != MainConstants.kConstantZero))
				&& ((email == null) || (email == MainConstants.kConstantEmpty) || (email
						.length() == MainConstants.kConstantZero))) {
			SingletonVisitorProfile.getInstance().setContact(phoneNo);
			if (validateMobileNo(phoneNo) == true) {
				SingletonVisitorProfile.getInstance().setPhoneNo(phoneNo);
			}
			return true;
		} else if (((phoneNo == null)
				|| (phoneNo == MainConstants.kConstantEmpty) || (phoneNo
				.length() == MainConstants.kConstantZero))
				&& ((email != null) && (email != MainConstants.kConstantEmpty) && (email
						.length() != MainConstants.kConstantZero))) {

			SingletonVisitorProfile.getInstance().setContact(email);
			if (validateMail(email) == true) {
				SingletonVisitorProfile.getInstance().setEmail(email);
			}
			return true;
		} else {

			if (validateMobileNo(phoneNo) == true) {
				SingletonVisitorProfile.getInstance().setContact(phoneNo);
				SingletonVisitorProfile.getInstance().setPhoneNo(phoneNo);
				return true;
			}
			if (validateMail(email) == true) {
				SingletonVisitorProfile.getInstance().setContact(email);
				SingletonVisitorProfile.getInstance().setEmail(email);
				return true;
			}
			SingletonVisitorProfile.getInstance().setContact(phoneNo);
			return true;

		}

	}

	/*
	 * Created by 25/10/14 Validate the officelocation and get location code.
	 */
	public int validateLocationToGetCode(String location) {

		if ((location != null) && (location != MainConstants.kConstantEmpty)
				&& (location.length() > MainConstants.kConstantZero)) {
			if (location
					.equals(MainConstants.kConstantOfficeLocationIndiaChennaiDLF)) {
				return MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF;

			} else if (location
					.equals(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia)) {
				return MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia;

			} else if (location
					.equals(MainConstants.kConstantOfficeLocationIndiaTenkasi)) {
				return MainConstants.kConstantOfficeLocationCodeIndiaTenkasi;

			} else if (location
					.equals(MainConstants.kConstantOfficeLocationChinaBeijing)) {
				return MainConstants.kConstantOfficeLocationCodeChinaBeijing;

			} else if (location
					.equals(MainConstants.kConstantOfficeLocationJapanYokohama)) {
				return MainConstants.kConstantOfficeLocationCodeJapanYokohama;

			} else if (location
					.equals(MainConstants.kConstantOfficeLocationUSAustin)) {
				return MainConstants.kConstantOfficeLocationCodeUSAustin;
			} else if (location
					.equals(MainConstants.kConstantOfficeLocationUSPleasanton)) {
				return MainConstants.kConstantOfficeLocationCodeUSPleasanton;
			}
		}
		return MainConstants.kConstantZero;
	}

	public Boolean validateContact(String contact) {

		if ((contact != null)
				&& (contact.length() > MainConstants.kConstantZero)) {
			contact = contact.replaceAll(
					MainConstants.kConstantRemoveSpecialCharacter, "");
			String firstLetter = String.valueOf(contact.toString().charAt(
					MainConstants.kConstantZero));
			if ((firstLetter
					.matches(MainConstants.kConstantValidMailOrMobileNo))
					&& (validateMail(contact) == true)) {
				// Log.d("email", "email");
				SingletonVisitorProfile.getInstance().setContact(contact);
				// SingletonVisitorProfile.getInstance().setPhoneNo(
				// MainConstants.kConstantEmpty);
				SingletonVisitorProfile.getInstance().setEmail(contact);
				return true;

			} else if ((!firstLetter
					.matches(MainConstants.kConstantValidMailOrMobileNo))
					&& (validateMobileNo(contact) == true)) {
				// Log.d("phone", "phone" +
				// SingletonVisitorProfile.getInstance().getPhoneNo());
				SingletonVisitorProfile.getInstance().setContact(contact);
				SingletonVisitorProfile.getInstance().setPhoneNo(contact);
				// SingletonVisitorProfile.getInstance().setEmail(
				// MainConstants.kConstantEmpty);
				if ((SingletonVisitorProfile.getInstance().getOfficeLocation() != null)
						&& (SingletonVisitorProfile.getInstance()
								.getOfficeLocation() != MainConstants.kConstantEmpty)
						&& (SingletonVisitorProfile.getInstance()
								.getOfficeLocation().length() > MainConstants.kConstantZero)
						&& ((SingletonVisitorProfile.getInstance()
								.getOfficeLocation()
								.equals(MainConstants.kConstantOfficeLocationIndiaChennaiDLF))
								|| (SingletonVisitorProfile.getInstance()
										.getOfficeLocation()
										.equals(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia)) || (SingletonVisitorProfile
								.getInstance().getOfficeLocation()
									.equals(MainConstants.kConstantOfficeLocationIndiaTenkasi)))) {
					// Log.d("phone", "phone" +
					// SingletonVisitorProfile.getInstance().getPhoneNo());

					SetMobileNumberWithLocation(SingletonVisitorProfile
							.getInstance().getPhoneNo());
				}

				return true;
			} else {
				// Log.d("contact1", "false");
				return false;
			}
		} else {
			// Log.d("contact2", "false");
			return false;
		}

	}

	public Boolean validateVisitorContact(String contact) {
			if ((contact != null)
					&& (contact.length() > MainConstants.kConstantZero)) {
				contact = contact.replaceAll(
						MainConstants.kConstantRemoveSpecialCharacter, "");
				String firstLetter = String.valueOf(contact.toString().charAt(
						MainConstants.kConstantZero));
				if ((firstLetter
						.matches(MainConstants.kConstantValidMailOrMobileNo))
						&& (validateMail(contact) == true)) {
					// Log.d("email", "email");
					SingletonVisitorProfile.getInstance().setContact(contact);
					// SingletonVisitorProfile.getInstance().setPhoneNo(
					// MainConstants.kConstantEmpty);
					SingletonVisitorProfile.getInstance().setEmail(contact);
					return true;

				} else if ((!firstLetter
						.matches(MainConstants.kConstantValidMailOrMobileNo))
						&& contact.length() >= MainConstants.kConstantSix
						&& (validateMobileNo(contact) == true)) {
					// Log.d("phone", "phone" +
					SingletonVisitorProfile.getInstance().setPhoneNo(contact);
					SingletonVisitorProfile.getInstance().setContact(contact);
					return true;
				} else {
					return false;
				}

			} else {
				// Log.d("contact1", "false");
				return false;
			}

	}

	 /**
	 * @author Karthikeyan D S
	 * @Created on 03JUL2018
	 *
	 * @param contact
	 * @return true/false
	 */
	public boolean validateDataCenterPhoneNumber(String contact) {
		if ((contact != null)
				&& (contact.length() > MainConstants.kConstantZero)) {
			contact = contact.replaceAll(
					MainConstants.kConstantRemoveSpecialCharacter, "");
			String firstLetter = String.valueOf(contact.toString().charAt(
					MainConstants.kConstantZero));
			if ((!firstLetter
					.matches(MainConstants.kConstantValidMailOrMobileNo))
					&& contact.length() == MainConstants.kConstantTen
					&& (validateMobileNo(contact) == true)) {
				// Log.d("phone", "phone" +
				SingletonVisitorProfile.getInstance().setPhoneNo(contact);
				SingletonVisitorProfile.getInstance().setContact(contact);
				return true;
			} else {
				return false;
			}
		}
		else {
				return false;
			}
	}
	/**
	 * Created by jayapriya on 28/01/14 get the worklocation in singleton value,
	 * if worklocation is india the set the phone number in india format like
	 * number start with 91.
	 */
	public void SetMobileNumberWithLocation(String phoneNo) {

		// testing
		if ((phoneNo != null) && (phoneNo != MainConstants.kConstantEmpty)
				&& (phoneNo.length() > MainConstants.kConstantZero)) {
			// if
			// (SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
			// .length() > MainConstants.kConstantTen) {
			String mobileNo = phoneNo.length() > MainConstants.kConstantTen ? phoneNo
					.substring(phoneNo.length() - MainConstants.kConstantTen)
					: phoneNo;
			// .substring(
			// SingletonEmployeeProfile.getInstance()
			// .getEmployeeMobileNo().length()
			// - MainConstants.kConstantTen,
			// SingletonEmployeeProfile.getInstance()
			// .getEmployeeMobileNo().length());
			SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
					mobileNo);
			/*
			 * } else if (SingletonEmployeeProfile.getInstance()
			 * .getEmployeeMobileNo().length() == MainConstants.kConstantTen) {
			 * 
			 * SingletonEmployeeProfile.getInstance().setEmployeeMobileNo( "91"
			 * + SingletonEmployeeProfile.getInstance() .getEmployeeMobileNo());
			 * } else
			 */if ((phoneNo.length() == MainConstants.kConstantSix)
					&& (phoneNo.length() < MainConstants.kConstantEight)) {
				SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
						MainConstants.kConstantTenkasiStdCode
								+ SingletonEmployeeProfile.getInstance()
										.getEmployeeMobileNo());

			} else if ((phoneNo.length() == MainConstants.kConstantEight)
					&& (phoneNo.length() < MainConstants.kConstantTen)) {

				SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
						MainConstants.kConstantChennaiStdCode
								+ SingletonEmployeeProfile.getInstance()
										.getEmployeeMobileNo());

			}
		}
		// Log.d("phone", "phone"
		// +SingletonVisitorProfile.getInstance().getPhoneNo());

	}

	/**
	 * Created by jayapriya on 28/01/14 get the worklocation in singleton value,
	 * if worklocation is india the set the phone number in india format like
	 * number start with 91.
	 */
	public void SetVisitorMobileNumberWithLocation(String phoneNo) {

		// testing
		if ((phoneNo != null) && (phoneNo != MainConstants.kConstantEmpty)
				&& (phoneNo.length() > MainConstants.kConstantZero)) {
			// if
			// (SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
			// .length() > MainConstants.kConstantTen) {
			String mobileNo = phoneNo.length() > MainConstants.kConstantTen ? phoneNo
					.substring(phoneNo.length() - MainConstants.kConstantTen)
					: phoneNo;
			// .substring(
			// SingletonEmployeeProfile.getInstance()
			// .getEmployeeMobileNo().length()
			// - MainConstants.kConstantTen,
			// SingletonEmployeeProfile.getInstance()
			// .getEmployeeMobileNo().length());
			SingletonVisitorProfile.getInstance().setPhoneNo(
					mobileNo);
			/*
			 * } else if (SingletonEmployeeProfile.getInstance()
			 * .getEmployeeMobileNo().length() == MainConstants.kConstantTen) {
			 * 
			 * SingletonEmployeeProfile.getInstance().setEmployeeMobileNo( "91"
			 * + SingletonEmployeeProfile.getInstance() .getEmployeeMobileNo());
			 * } else
			 */if ((phoneNo.length() == MainConstants.kConstantSix)
					&& (phoneNo.length() < MainConstants.kConstantEight)) {
				SingletonVisitorProfile.getInstance().setPhoneNo(
						MainConstants.kConstantTenkasiStdCode
								+ SingletonVisitorProfile.getInstance()
										.getPhoneNo());

			} else if ((phoneNo.length() == MainConstants.kConstantEight)
					&& (phoneNo.length() < MainConstants.kConstantTen)) {

				SingletonVisitorProfile.getInstance().setPhoneNo(
						MainConstants.kConstantChennaiStdCode
								+ SingletonVisitorProfile.getInstance()
										.getPhoneNo());

			}
		}
		// Log.d("phone", "phone"
		// +SingletonVisitorProfile.getInstance().getPhoneNo());

	}

}