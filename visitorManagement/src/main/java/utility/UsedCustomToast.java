package utility;

import com.zoho.app.frontdesk.android.ApplicationController;
import com.zoho.app.frontdesk.android.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import constants.FontConstants;

/**
 * Created by jayapriya on 31/03/15.
 */
public class UsedCustomToast {

    Context context;
    /*
    * Returns a custom toast object
    * @param         textForToast        the text to be displayed in the toast
    * @return        the customized Toast object
    * @created        20131204
    */
    public static Toast makeCustomToast ( Context context, String toastText, int duration ) {

        if ((context == null) || (toastText == null) || (toastText.length() == 0)) {
            //Log.e (logtag, "ERROR : getCustomToast : invalid input context or toasttext");
            return null;
        }

       // Log.d("toast", "toast");
        // Set text properties
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                FontConstants.fontConstantSourceSansProbold);
        Toast toast = Toast.makeText(context, toastText, duration);
         //Toast.
        TextView textView = (TextView) toast.getView().findViewById(
                android.R.id.message);
        textView.setTextColor(Color.rgb(51, 51, 51));
        textView.setShadowLayer(0, 0, 0, Color.rgb(68,152,17));
        textView.setTextSize(23.0f);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
        }
        textView.setTypeface(typeface);

        // Set view properties
        toast.getView().setBackgroundColor(Color.argb(230, 220, 220, 220));
        textView.setBackgroundColor(Color.TRANSPARENT);
        return toast;
    }
    
    /*
     * Returns a custom toast object
     * @param         textForToast        the text to be displayed in the toast
     * @return        the customized Toast object
     * @created        20131204
     */
     public static Toast makeCustomToast ( Context context, String toastText, int duration, int gravity ) {
         if ((context == null) || (toastText == null) || (toastText.length() == 0)) {
             //Log.e (logtag, "ERROR : getCustomToast : invalid input context or toasttext");
             return null;
         }
         //Log.d("toast", "toast");
         // Set text properties
         Typeface typeface = Typeface.createFromAsset(context.getAssets(),FontConstants.fontConstantSourceSansProSemibold);
         Toast toast = Toast.makeText(context, toastText, duration);
          //Toast.
         TextView textView = (TextView) toast.getView().findViewById(
	                android.R.id.message);
	        textView.setTextColor(Color.rgb(51, 51, 51));
	        textView.setShadowLayer(0, 0, 0, Color.rgb(68,152,17));
	        textView.setTextSize(30.0f);
	        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
	            textView.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
	        }
	        textView.setMinHeight((int) context.getResources().getDimension(R.dimen.toast_min_height));
	        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,(int) context.getResources().getDimension(R.dimen.toast_text));
			
         textView.setGravity(Gravity.CENTER);
         textView.setPadding(10, 10, 10, 10);
         
         textView.setTypeface(typeface);
         if(gravity==Gravity.TOP) {
         toast.setGravity(gravity|Gravity.CENTER_HORIZONTAL, 0, 50);
         }
         // Set view properties Color.argb(217,197, 164, 99)
         toast.getView().setBackgroundColor(Color.argb(230, 220, 220, 220));
         textView.setBackgroundColor(Color.TRANSPARENT);
         return toast;
     }
	public static Toast makeText(ApplicationController applicationController,
			String toastText, int duration) {
		 if ((applicationController == null) || (toastText == null) || (toastText.length() == 0)) {
	            //Log.e (logtag, "ERROR : getCustomToast : invalid input context or toasttext");
	            return null;
	        }

	       // Log.d("toast", "toast");
	        // Set text properties
	        Typeface typeface = Typeface.createFromAsset(applicationController.getAssets(),
	                FontConstants.fontConstantSourceSansProSemibold);
	        Toast toast = Toast.makeText(applicationController, toastText, duration);
	         //Toast.
	        TextView textView = (TextView) toast.getView().findViewById(
	                android.R.id.message);
	        textView.setTextColor(Color.rgb(51, 51, 51));
	        textView.setShadowLayer(0, 0, 0, Color.rgb(68,152,17));
	        textView.setTextSize(23.0f);
	        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
	            textView.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
	        }
	        textView.setTypeface(typeface);

	        // Set view properties
	        toast.getView().setBackgroundColor(Color.argb(230, 220, 220, 220));
            textView.setBackgroundColor(Color.TRANSPARENT);
	        return toast;
	}
}
