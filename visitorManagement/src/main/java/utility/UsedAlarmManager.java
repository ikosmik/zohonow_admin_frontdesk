package utility;

import java.util.Calendar;

import backgroundprocess.CreatorForgetIDSync;
import backgroundprocess.CreatorSync;

import backgroundprocess.SchedulerEventReceiver;

import constants.MainConstants;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
/**
 * Created by jayapriya On 11/10/14
 *  Used to activate and deactivate the AlarmManager 
 */
public class UsedAlarmManager {
	/**
	 * Created by jayapriya On 11/10/14
	 * context fuction is used to activate the AlarmManager
	 * @param context
	 */
	public static void activateAlarmManager(Context context){
		
		Intent intentGetClass = new Intent(context, SchedulerEventReceiver.class);
		PendingIntent pendingIntentBroadCastReceiver = PendingIntent.getBroadcast(context, 0, intentGetClass,
				PendingIntent.FLAG_UPDATE_CURRENT);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.AM_PM, Calendar.AM);
		//calendar.setTimeInMillis(System.currentTimeMillis());
		//calendar.add(Calendar.MINUTE, 1);
		//Log.d("ActivateAlarmManager", "AlarmManager called");
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,
				pendingIntentBroadCastReceiver);
		
		setAlarmCreatorSync(context);
		setAlarmCreatorSyncForgetId(context);
	}

	public static void deactivateAlarmManager(Context context)
	{
		Intent intentGetClass = new Intent(context, SchedulerEventReceiver.class);
		PendingIntent pendingIntentBroadCastReceiver = PendingIntent.getBroadcast(context, MainConstants.kConstantZero, intentGetClass,
				PendingIntent.FLAG_UPDATE_CURRENT);
		//PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intentGetClass,
				//PendingIntent.FLAG_UPDATE_CURRENT);
		pendingIntentBroadCastReceiver.cancel();
		
		Intent intentClass = new Intent(context, CreatorSync.class);
		 pendingIntentBroadCastReceiver = PendingIntent.getBroadcast(context, 0, intentClass,
				PendingIntent.FLAG_UPDATE_CURRENT);
		 pendingIntentBroadCastReceiver.cancel();

		Intent intentForgetIDClass = new Intent(context, CreatorForgetIDSync.class);
		pendingIntentBroadCastReceiver = PendingIntent.getBroadcast(context, 0, intentForgetIDClass,
				PendingIntent.FLAG_UPDATE_CURRENT);
		pendingIntentBroadCastReceiver.cancel();
	}
	
	private static void setAlarmCreatorSync(Context context){
		
		Intent intentClass = new Intent(context, CreatorSync.class);
		PendingIntent pendingIntentBroadCastReceiver = PendingIntent.getBroadcast(context, 0, intentClass,
				PendingIntent.FLAG_UPDATE_CURRENT);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.AM_PM, Calendar.AM);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,
				pendingIntentBroadCastReceiver);
	}

	/**
	 * @created by Karthikeyan D S
	 * @created on 27sep2018
	 *
	 * call for Alarm Manager to sync Forget ID Data for every Hour
	 * @param context
	 */
	private static void setAlarmCreatorSyncForgetId(Context context)
	{
		Intent intent = new Intent(context, CreatorForgetIDSync.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.AM_PM, Calendar.AM);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(),AlarmManager.INTERVAL_HOUR,
				pendingIntent);
	}
}
