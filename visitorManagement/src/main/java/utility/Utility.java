package utility;

import android.content.res.Configuration;
import android.os.Build;
import android.view.Display;

import com.zoho.app.frontdesk.android.ApplicationController;
import com.zoho.app.frontdesk.android.BuildConfig;
import com.zoho.app.frontdesk.android.R;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonVisitorProfile;

import java.io.File;

import appschema.SingletonMetaData;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import database.dbschema.ZohoEmployeeRecord;

public class Utility {

	/**
	 * Store employeedetails into the singleton variables
	 *
	 * @author Karthick
	 * @created on 20170516
	 */
	public void storeEmployeeIntoSingleton(ZohoEmployeeRecord employee) {

		if ((employee != null)
				&& (employee.getEmployeeName() != MainConstants.kConstantEmpty)
				&& (employee.getEmployeeName().length() > MainConstants.kConstantZero)) {
			Validation validation = new Validation();
			SingletonEmployeeProfile.getInstance().setEmployeeName(
					employee.getEmployeeName());
			SingletonEmployeeProfile.getInstance().setEmployeeExtentionNumber(
					employee.getEmployeeExtentionNumber());
			SingletonEmployeeProfile.getInstance().setEmployeeId(
					employee.getEmployeeId());
			SingletonEmployeeProfile.getInstance().setEmployeeMailId(
					employee.getEmployeeEmailId());
			SingletonEmployeeProfile.getInstance().setEmployeeDepartment(
					employee.getEmployeeDept());

			if (((SingletonVisitorProfile.getInstance().getLocationCode() != null))
					&& ((SingletonVisitorProfile.getInstance()
					.getLocationCode() == MainConstants.kConstantMinusOne)
					|| (SingletonVisitorProfile.getInstance()
					.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
					|| (SingletonVisitorProfile.getInstance()
					.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) || (SingletonVisitorProfile
					.getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
				validation.SetMobileNumberWithLocation(employee
						.getEmployeeMobileNumber());
			} else {
				SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
						employee.getEmployeeMobileNumber());
			}
			SingletonEmployeeProfile.getInstance().setEmployeeZuid(
					employee.getEmployeeZuid());
			SingletonEmployeeProfile.getInstance().setEmployeeSeatingLocation(
					employee.getEmployeeSeatingLocation());
		}
	}

	/**
	 * Return file upload url
	 *
	 * @author Karthick
	 * @created on 20161111
	 */
	public String getFileUploadUrl() {

		return MainConstants.kConstantFileUploadUrl
				+ MainConstants.kConstantCreatorAuthTokenAndScope
				+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetail
				+ MainConstants.kConstantFileUploadFieldName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.kConstantFileAccessTypeAndName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.creatorPhotoName
				+ MainConstants.kConstantImageFileFormat
				+ MainConstants.kConstantFileSharedBy;
	}

	/**
	 * Return file upload url
	 *
	 * @author Karthick
	 * @created on 20161111
	 */
	public String getFileUploadUrlDataCentre() {

		return MainConstants.kConstantFileUploadUrl
				+ MainConstants.kConstantFrontdeskDCAuthTokenAndScope
				+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetailFrontdeskDC
				+ MainConstants.kConstantFileUploadFieldName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.kConstantFileAccessTypeAndName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.creatorPhotoName
				+ MainConstants.kConstantImageFileFormat
				+ MainConstants.kConstantFileSharedByFrontdeskDC;
	}

	/**
	 * Return file upload url
	 *
	 * @author Karthick
	 * @created on 20161111
	 */
	public String getVisitorBadgeFileUploadUrlDataCentre() {

		File file = new File(MainConstants.kConstantFileDirectory + MainConstants.kConstantFileTemporaryFolder);
		if (!file.exists()) {
			file.mkdir();
		}
		String sourceFile = MainConstants.kConstantFileDirectory + MainConstants.kConstantFileVisitorBadge +
				SingletonVisitorProfile.getInstance().getVisitorBadgeFileName() + ".jpg";
		return MainConstants.kConstantFileUploadUrl
				+ MainConstants.kConstantFrontdeskDCAuthTokenAndScope
				+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetailFrontdeskDC
				+ MainConstants.kConstantVisitorBadgeFileUploadFieldName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.kConstantFileAccessTypeAndName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ sourceFile
				+ MainConstants.kConstantFileSharedByFrontdeskDC;
	}

	/**
	 * Prepare to upload data to cloud
	 *
	 * @author Karthick
	 * @created on 20161110
	 */
	public String convertVisitorDetailsJsonData() {

		String jsonString = MainConstants.kConstantOpenBraces;
		if (SingletonVisitorProfile.getInstance() != null) {
			if (SingletonVisitorProfile.getInstance().getVisitorName() != null
					&& !SingletonVisitorProfile.getInstance().getVisitorName()
					.isEmpty()) {
				jsonString = jsonString
						+ MainConstants.jsonVisitorName
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
						.getVisitorName().toUpperCase()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getPhoneNo() != null
					&& !SingletonVisitorProfile.getInstance().getPhoneNo()
					.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorPhone
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getPhoneNo()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getEmail() != null
					&& !SingletonVisitorProfile.getInstance().getEmail()
					.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorEmail
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getEmail()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getCompany() != null
					&& !SingletonVisitorProfile.getInstance().getCompany()
					.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorCompany
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getCompany()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getPurposeOfVisit() != null
					&& !SingletonVisitorProfile.getInstance()
					.getPurposeOfVisit().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOldPurpose
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
						.getPurposeOfVisit()
						+ MainConstants.kConstantSingleQuotes;
			}
			jsonString = jsonString
					+ MainConstants.kConstantComma
					+ MainConstants.jsonVisitorisPreApproved
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ SingletonVisitorProfile.getInstance()
					.isVisitorPreApproved()
					+ MainConstants.kConstantSingleQuotes;

			if (SingletonMetaData.getInstance() != null
					&& SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantNormalMode) {
				if (SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null
						&& !SingletonVisitorProfile.getInstance()
						.getPurposeOfVisitCode().isEmpty()) {
					jsonString = jsonString
							+ MainConstants.kConstantComma
							+ MainConstants.jsonVisitorVisitPurpose
							+ MainConstants.kConstantColon
							+ MainConstants.kConstantSingleQuotes
							+ SingletonVisitorProfile.getInstance()
							.getPurposeOfVisitCode().toUpperCase()
							+ MainConstants.kConstantSingleQuotes;
				}
			} else if (SingletonMetaData.getInstance() != null
					&& SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantDataCentreMode) {
				if (SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null
						&& !SingletonVisitorProfile.getInstance()
						.getPurposeOfVisitCode().isEmpty()) {
					jsonString = jsonString
							+ MainConstants.kConstantComma
							+ MainConstants.jsonVisitorVisitPurpose
							+ MainConstants.kConstantColon
							+ MainConstants.kConstantSingleQuotes
							+ MainConstants.purposeDataCenter
							+ MainConstants.kConstantSingleQuotes;
				}
			}

			jsonString = jsonString
					+ MainConstants.kConstantComma
					+ MainConstants.jsonVisitorCheckInTime
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ TimeZoneConventer.getCurrentDateTime()
					+ MainConstants.kConstantSingleQuotes;

			if (SingletonVisitorProfile.getInstance().getOfficeLocation() != null
					&& !SingletonVisitorProfile.getInstance()
					.getOfficeLocation().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOfficeLocation
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
						.getOfficeLocation()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getLocationCode() != null) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOfficeLocationCode
						+ MainConstants.kConstantColon
						+ SingletonVisitorProfile.getInstance()
						.getLocationCode();
			}

			jsonString = jsonString
					+ MainConstants.kConstantComma
					+ MainConstants.jsonVisitorTempId
					+ MainConstants.kConstantColon
					+ SingletonVisitorProfile.getInstance()
					.getTempIDNumber();


			if (SingletonVisitorProfile.getInstance().getCodeNo() != null
					&& SingletonVisitorProfile.getInstance().getCodeNo() > 0) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorAppRegistration
						+ MainConstants.kConstantColon
						+ SingletonVisitorProfile.getInstance().getCodeNo();
			}

			jsonString = jsonString + MainConstants.kConstantComma
					+ MainConstants.jsonVisitorApptoval
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ MainConstants.approval
					+ MainConstants.kConstantSingleQuotes;

			if (SingletonEmployeeProfile.getInstance().getEmployeeName() != null
					&& !SingletonEmployeeProfile.getInstance()
					.getEmployeeName().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeName
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
						.getEmployeeName()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance().getEmployeeId() != null
					&& !SingletonEmployeeProfile.getInstance().getEmployeeId()
					.isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeId
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
						.getEmployeeId()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance().getEmployeeMailId() != null
					&& !SingletonEmployeeProfile.getInstance()
					.getEmployeeMailId().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeEmail
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
						.getEmployeeMailId()
						+ MainConstants.kConstantSingleQuotes;
			}

			Validation validation = new Validation();
			if (SingletonEmployeeProfile.getInstance().getEmployeeMobileNo() != null
					&& !SingletonEmployeeProfile.getInstance()
					.getEmployeeMobileNo().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeePhone
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ validation.getValidMobileNumber(SingletonEmployeeProfile.getInstance()
						.getEmployeeMobileNo().trim())
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance()
					.getEmployeeSeatingLocation() != null
					&& !SingletonEmployeeProfile.getInstance()
					.getEmployeeSeatingLocation().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeSeatingLocation
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
						.getEmployeeSeatingLocation()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance()
					.getEmployeeExtentionNumber() != null
					&& !SingletonEmployeeProfile.getInstance()
					.getEmployeeExtentionNumber().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeExtension
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
						.getEmployeeExtentionNumber()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance().getEmployeeZuid() != null
					&& !SingletonEmployeeProfile.getInstance()
					.getEmployeeZuid().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeZuid
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
						.getEmployeeZuid()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getDeviceMake() != null
					&& !SingletonVisitorProfile.getInstance().getDeviceMake()
					.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorDeviceMake
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getDeviceMake()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getDeviceSerialNumber() != null
					&& !SingletonVisitorProfile.getInstance()
					.getDeviceSerialNumber().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorDeviceSerial
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
						.getDeviceSerialNumber()
						+ MainConstants.kConstantSingleQuotes;
			}

			String externalDevice = MainConstants.kConstantEmpty;
			if (SingletonVisitorProfile.getInstance().isExtDeviceOne()) {
				externalDevice = externalDevice + ApplicationController.getInstance().getResources().getString(R.string.external_device_one) + MainConstants.kConstantComma;
			}
			if (SingletonVisitorProfile.getInstance().isExtDeviceTwo()) {
				externalDevice = externalDevice + ApplicationController.getInstance().getResources().getString(R.string.external_device_two) + MainConstants.kConstantComma;
			}
			if (SingletonVisitorProfile.getInstance().isExtDeviceThree()) {
				externalDevice = externalDevice + ApplicationController.getInstance().getResources().getString(R.string.external_device_three) + MainConstants.kConstantComma;
			}
			if (SingletonVisitorProfile.getInstance().isExtDeviceFour()) {
				externalDevice = externalDevice + ApplicationController.getInstance().getResources().getString(R.string.external_device_four) + MainConstants.kConstantComma;
			}
			if (SingletonVisitorProfile.getInstance().isExtDeviceFive()) {
				externalDevice = externalDevice + ApplicationController.getInstance().getResources().getString(R.string.external_device_five) + MainConstants.kConstantComma;
			}
			if (externalDevice != null && !externalDevice.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorExtDevice
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ externalDevice
						+ MainConstants.kConstantSingleQuotes;
			}
			jsonString = jsonString + MainConstants.kConstantComma
					+ MainConstants.jsonVisitorExpectedVisitDate
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ StringConstants.creatorExpectedVisitedDate
					+ MainConstants.kConstantSingleQuotes;

			if (SingletonVisitorProfile.getInstance().getDeviceLocation() != null
					&& !SingletonVisitorProfile.getInstance()
					.getDeviceLocation().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonDeviceLocation
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
						.getDeviceLocation()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getOtherDeviceDetails() != null
					&& !SingletonVisitorProfile.getInstance().getOtherDeviceDetails()
					.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOtherDeviceDetails
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getOtherDeviceDetails()
						+ MainConstants.kConstantSingleQuotes;
			}

//			if(>0) {
//				jsonString = jsonString + MainConstants.kConstantComma
//						+ MainConstants.jsonAppVersion
//						+ MainConstants.kConstantColon
//						+ MainConstants.kConstantSingleQuotes
//						+ appVersion
//						+ MainConstants.kConstantSingleQuotes;
//			}

			if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getApplicationMode() > 0) {
				String applicationModeData = MainConstants.kConstantEmpty;
				if (SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantNormalMode) {
					applicationModeData = MainConstants.spKeyNormalMode;
				} else if (SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantDataCentreMode) {
					applicationModeData = MainConstants.purposeDataCenter;
				}
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.spKeyApplicationMode
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ applicationModeData
						+ MainConstants.kConstantSingleQuotes;
			}

			//set app version
			String appVersion = BuildConfig.VERSION_NAME;
			if (appVersion != null) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonAppVersion
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ appVersion
						+ MainConstants.kConstantSingleQuotes;
			}

			//get Device details.
			if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getDeviceID() != null
					&& !SingletonMetaData.getInstance().getDeviceID().isEmpty()) {
				String androidDeviceID = SingletonMetaData.getInstance().getDeviceID();
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.kConstantDeviceID
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ androidDeviceID
						+ MainConstants.kConstantSingleQuotes;
			}
			String deviceName = Build.MANUFACTURER + " " + Build.MODEL;
			if (deviceName != null && !deviceName.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.kConstantDeviceName
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ deviceName
						+ MainConstants.kConstantSingleQuotes;
			}
			jsonString = jsonString + MainConstants.kConstantCloseBraces;
		}
		return jsonString;
	}

}
