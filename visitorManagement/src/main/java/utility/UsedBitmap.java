package utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import appschema.SharedPreferenceController;
import constants.MainConstants;
import constants.NumberConstants;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.PorterDuff.Mode;
import android.util.Log;
import android.view.Surface;
import android.view.View;

/*
 * Created by jayapriya On 30/10/14
 */
public class UsedBitmap {
	public Integer newImageHeight = MainConstants.kConstantZero;
	public Integer bitmapWidth;
	public Integer bitmapHeight;
	private Context context;

	/*
	 * created by jayapriya On 30/09/2014 Create Signature as a bitmap and store
	 * it to the local device.
	 */
	public Bitmap createBitmap(View v, String fileName) {

		v.setDrawingCacheEnabled(true);
		v.buildDrawingCache(true);
		v.setBackgroundColor(Color.WHITE);
		Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
		v.setDrawingCacheEnabled(false);
		Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), bitmap.getConfig());

		Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, MainConstants.kConstantBusinessCardImageWidth, MainConstants.kConstantBusinessCardImageHeight, false);

//		Canvas canvas = new Canvas(resizedBitmap);
//
//		// Paint it white (or whatever color you want)
//		canvas.drawColor(Color.WHITE);
//
//		// Draw the old bitmap ontop of the new white one
//		canvas.drawBitmap(bitmap, 0, 0, null);
		bitmap.recycle();
		newBitmap.recycle();
		bitmap = null;
		newBitmap = null;
		return resizedBitmap;
	}

	public Bitmap createBitmap(View v, boolean isWantBG) {

		//v.setVisibility(View.INVISIBLE);
		v.setDrawingCacheEnabled(true);
		v.buildDrawingCache(true);
		if(isWantBG) {
			v.setBackgroundColor(Color.rgb(63,68,70));
		}
		Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
		v.setDrawingCacheEnabled(false);
		Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), bitmap.getConfig());

		Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, MainConstants.kConstantBusinessCardImageWidth, MainConstants.kConstantBusinessCardImageHeight, false);
		bitmap.recycle();
		newBitmap.recycle();
		bitmap = null;
		newBitmap = null;
		return resizedBitmap;
	}

	/**
	 * Store Bitmap to the Details
	 *
	 * @author Karthikeyan D S
	 * @created on 11Aug2018
	 */
	public Bitmap storeBitmap(Bitmap bitmap, int newBitmapWidth) {
		// Get bitmap width and height.
		Integer newImageHeight, bitmapWidth = bitmap.getWidth();
		Integer bitmapHeight = bitmap.getHeight();
		Bitmap resized = null;
		newImageHeight = (newBitmapWidth * bitmapHeight) / bitmapWidth;
		if (newImageHeight != MainConstants.kConstantZero) {
			resized = Bitmap.createScaledBitmap(bitmap,
					newBitmapWidth, 700, true);
			bitmap.recycle();
			bitmap = null;
			bitmapWidth = resized.getWidth();
			bitmapHeight = resized.getHeight();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			resized.compress(Bitmap.CompressFormat.JPEG,
					MainConstants.compressQuality, bytes);
			return resized;
		}
		return resized;
	}

	/*
	 *
	 */
	public void storeBitmap(Bitmap bitmap, int newBitmapWidth, String fileName) {

		// Get bitmap width and height.
		Integer newImageHeight, bitmapWidth = bitmap.getWidth();
		Integer bitmapHeight = bitmap.getHeight();
		// if ((newBitmapWidth != 0) && (bitmapHeight != 0) && (bitmapWidth !=
		// 0))
		{
			newImageHeight = (newBitmapWidth * bitmapHeight) / bitmapWidth;

			if (newImageHeight != MainConstants.kConstantZero) {

				Bitmap resized = Bitmap.createScaledBitmap(bitmap,
						newBitmapWidth, newImageHeight, true);
				bitmap.recycle();
				bitmap = null;
				bitmapWidth = resized.getWidth();
				bitmapHeight = resized.getHeight();
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				resized.compress(Bitmap.CompressFormat.JPEG,
						MainConstants.compressQuality, bytes);
				// String sourceFileUri="/sdcard/VisitorMgmt/"+ "ikosmik1.jpg";
				File file = new File(fileName);
				// Log.d("scanning activity", "uploadbusinesscard:" + fileName);
				// sourceFileUri=/sdcard/VisitorMgmt/+ "ikosmik1.jpg";

				try {
					if (file.exists()) {
						// Log.d("scanning activity", "exist:" + fileName);
						file.delete();
					}

					file.createNewFile();
					FileOutputStream fileOutputStream = new FileOutputStream(
							file);
					fileOutputStream.write(bytes.toByteArray());
					fileOutputStream.close();

				} catch (IOException e1) {

					//

				}
				resized.recycle();
				resized = null;
				bytes = null;
				file = null;
			}
		}
	}

	/**
	 *
	 * @author Karthick
	 * @created on 20161126
	 *
	 * @param path
	 * @return byte[]
	 */
	public byte[] getByteFromFile(String path) {

		File file = new File(path);
		int size = (int) file.length();
		byte[] bytes = new byte[size];
		try {
			BufferedInputStream buf = new BufferedInputStream(
					new FileInputStream(file));
			buf.read(bytes, 0, bytes.length);
			buf.close();
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
		return bytes;
	}

	/**
	 *
	 * @author Karthick
	 * @created on 20161126
	 *
	 * @param path
	 * @return byte[]
	 */
	public boolean writeFile(byte[] fileBytes,String path) {
		BufferedOutputStream bos;
		try {
			File file = new File(path);
			if(file.exists()) {
				file.delete();
			}
			file.createNewFile();

			bos = new BufferedOutputStream(new FileOutputStream(file));

			bos.write(fileBytes);
			bos.flush();
			bos.close();
			return true;
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Resized image
	 *
	 * @author Karthick
	 * @created on 20161126
	 *
	 * @param bitmap
	 * @return byte[]
	 */
	public byte[] getBytes(Bitmap bitmap) {
		if (bitmap != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, MainConstants.compressQuality, stream);
			bitmap.recycle();
			return stream.toByteArray();
		}
		return null;
	}

	/**
	 * Resized image
	 *
	 * @author Karthick
	 * @created on 20161126
	 *
	 * @param bitmap
	 * @return byte[]
	 */
	public byte[] getBytesPNG(Bitmap bitmap) {
		if (bitmap != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, MainConstants.compressQuality, stream);
			return stream.toByteArray();
		}
		return null;
	}

	/*
	 *
	 */
	public void storeBitmap(Bitmap bitmap, String fileName) {

		if(bitmap!=null) {
			int width=bitmap.getWidth(),height=bitmap.getHeight();
//		if( bitmap.getWidth()>=4000) {
////			Log.d("DSK"," Employee Photo bitmapgetWidth "+bitmap.getWidth()+" bitmapgetHeight "+bitmap.getHeight());
//			width=bitmap.getWidth()/26;
//			height=bitmap.getHeight()/26;
//		}
// 		else if( bitmap.getWidth()<4000 && bitmap.getWidth()>=3000) {
//			width=bitmap.getWidth()/20;
//			height=bitmap.getHeight()/20;
//		} else if( bitmap.getWidth()<3000 && bitmap.getWidth()>=2000) {
//			width=bitmap.getWidth()/13;
//			height=bitmap.getHeight()/13;
//		}
//		else if( bitmap.getWidth()<2000 && bitmap.getWidth()>=1000) {
//			width=bitmap.getWidth()/6;
//			height=bitmap.getHeight()/6;
//		} else if( bitmap.getWidth()<1000 && bitmap.getWidth()>=500) {
//			width=bitmap.getWidth()/3;
//			height=bitmap.getHeight()/3;
//		}
//		else if( bitmap.getWidth()<500 && bitmap.getWidth()>=300) {
//			width=bitmap.getWidth()/2;
//			height=bitmap.getHeight()/2;
//		}
		if(bitmap.getWidth()>=150)
		{
			//int radio = bitmap.getWidth()/300;
			int width_ratio = 70*bitmap.getWidth();
			int height_ratio = 70*bitmap.getHeight();
			Log.e("DSK1"," *** Employee Photo bitmapgetWidth "+bitmap.getWidth()+" bitmapgetHeight "+bitmap.getHeight());
			width = width_ratio/100;
			height = height_ratio/100;
			Log.e("DSK2"," *** Employee Photo Width "+width+" Height "+height);
		}

			Bitmap resizeBitmap = Bitmap.createScaledBitmap(
					bitmap, width,height, false);

			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			resizeBitmap.compress(Bitmap.CompressFormat.JPEG,
					MainConstants.compressQuality, bytes);
			File file = new File(fileName);
			try {
				if (file.exists()) {
					file.delete();
				}

				file.createNewFile();
				FileOutputStream fileOutputStream = new FileOutputStream(file);
				fileOutputStream.write(bytes.toByteArray());
				fileOutputStream.close();
				bytes.flush();
				bytes.close();
			} catch (IOException e) {

			}
			resizeBitmap.recycle();
			bitmap.recycle();
			bitmap = null;
			resizeBitmap=null;
			bytes = null;
			file = null;
		}

	}

	public void storeBitmap(Bitmap bitmap, int newImageWidth, String folder,
							String fileName) {
		// Get bitmap width and height.
		Integer newImageHeight, bitmapWidth = bitmap.getWidth();
		Integer bitmapHeight = bitmap.getHeight();
		// Log.d("filename", "filename" + bitmapWidth + bitmapHeight);
		if ((newImageWidth != MainConstants.kConstantZero)
				&& (bitmapHeight != MainConstants.kConstantZero)
				&& (bitmapWidth != MainConstants.kConstantZero)) {
			newImageHeight = (newImageWidth * bitmapHeight) / bitmapWidth;

			if (newImageHeight != MainConstants.kConstantZero) {

				Bitmap resized = Bitmap.createScaledBitmap(bitmap,
						newImageWidth, newImageHeight, true);
				bitmap.recycle();
				bitmap = null;
				bitmapWidth = resized.getWidth();
				bitmapHeight = resized.getHeight();
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				resized.compress(Bitmap.CompressFormat.JPEG,
						MainConstants.compressQuality, bytes);

				try {
					File fileDirectory = new File(
							MainConstants.kConstantFileDirectory + folder);

					if (!fileDirectory.exists()) {

						fileDirectory.mkdirs();

					}

					final File file = new File(fileDirectory.getAbsolutePath(),
							fileName);

					if (file.exists()) {
						file.delete();
					}

					file.createNewFile();
					FileOutputStream fileOutputStream;
					fileOutputStream = new FileOutputStream(file);
					fileOutputStream.write(bytes.toByteArray());
					fileOutputStream.close();

				} catch (FileNotFoundException e) {
					// e.printStackTrace();
				} catch (IOException e) {
					// e.printStackTrace();
				}
				resized.recycle();
				resized = null;
				bytes = null;

				newImageHeight = MainConstants.kConstantZero;
				bitmapWidth = MainConstants.kConstantZero;
				bitmapHeight = MainConstants.kConstantZero;

			}
		}
	}

	public void storeImage(String fileUri) {
		if ((fileUri != null)
				&& (fileUri.toString().length() > MainConstants.kConstantZero)) {
			Bitmap bitmap = BitmapFactory.decodeFile(fileUri.toString());
			if (bitmap != null) {

				Integer newImageHeight, bitmapWidth = bitmap.getWidth();
				Integer bitmapHeight = bitmap.getHeight();
				// Log.d("filename", "filename" + bitmapWidth + bitmapHeight);
				if ((MainConstants.resizedImageWidth != MainConstants.kConstantZero)
						&& (bitmapHeight != MainConstants.kConstantZero)
						&& (bitmapWidth != MainConstants.kConstantZero)) {
					newImageHeight = (MainConstants.resizedImageWidth * bitmapHeight)
							/ bitmapWidth;

					if (newImageHeight != MainConstants.kConstantZero) {

						Bitmap resized = Bitmap.createScaledBitmap(bitmap,
								MainConstants.resizedImageWidth,
								newImageHeight, true);
						ByteArrayOutputStream bytes = new ByteArrayOutputStream();
						resized.compress(Bitmap.CompressFormat.JPEG,
								MainConstants.compressQuality, bytes);
						// String sourceFileUri="/sdcard/VisitorMgmt/"+
						// "ikosmik1.jpg";
						File imageFile = new File(fileUri.toString());
						// Log.d("scanning activity", "uploadbusinesscard" + f);
						// sourceFileUri=/sdcard/VisitorMgmt/+ "ikosmik1.jpg";

						try {

							imageFile.createNewFile();
							FileOutputStream fileOutStream = new FileOutputStream(
									imageFile);
							fileOutStream.write(bytes.toByteArray());
							fileOutStream.close();
							bitmap.recycle();
							bitmap = null;
							resized.recycle();
							resized = null;
						} catch (IOException e) {
							// Log.d("exception", "ioexception" + e);
						}
					}
				}
			}
		}
	}

	public void storeresizedImage(String fileUri, Integer rotation) {
		// Bitmap bitmap;
		Long currentTime = System.currentTimeMillis();
		Matrix mat = new Matrix();
		try {
			if ((fileUri != null)
					&& (fileUri.toString().length() > MainConstants.kConstantZero)) {
				Bitmap bitmap = BitmapFactory.decodeFile(fileUri.toString());
				int rotateangle = 0;
				// Log.d("orientation", ":" + Surface.ROTATION_90);
				// Log.d("orientation", ":" + Surface.ROTATION_90);
				// Log.d("orientation", ":" + Surface.ROTATION_270);
				rotateangle = getDeviceRotationAngle(rotation);
				mat.setRotate(rotateangle, (float) bitmap.getWidth() / 2,
						(float) bitmap.getHeight() / 2);

				/*
				 * File f = new File(fileUri); Bitmap bmpPic;
				 *
				 * bmpPic = BitmapFactory.decodeStream(new FileInputStream(f),
				 * null, null); Bitmap bmpPic1 = Bitmap.createBitmap(bmpPic, 0,
				 * 0, bmpPic.getWidth(), bmpPic.getHeight(), mat, true); } }
				 * catch (FileNotFoundException e1) { } catch (IOException e) {
				 * e.printStackTrace(); } } /* ExifInterface exif; try { exif =
				 * new ExifInterface(fileUri.toString());
				 *
				 * int orientation =
				 * exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
				 * Log.e("orientation ",""+orientation);
				 *
				 *
				 * if((orientation==3)){
				 *
				 * m.postRotate(180); m.postScale((float)bitmap.getWidth(),
				 * (float)bitmap.getHeight());
				 *
				 * // if(m.preRotate(90)){
				 * Log.e("in orientation 180",""+orientation);
				 *
				 * // bitmap = Bitmap.createBitmap(bm, 0,
				 * 0,bm.getWidth(),bm.getHeight(), m, true); //return bitmap; }
				 * else if(orientation==6){
				 *
				 * m.postRotate(90);
				 *
				 * Log.e("in orientation 90",""+orientation);
				 *
				 * // bitmap = Bitmap.createBitmap(bm, 0,
				 * 0,bm.getWidth(),bm.getHeight(), m, true); // return bitmap; }
				 *
				 * else if(orientation==8){
				 *
				 * m.postRotate(270);
				 *
				 * Log.e("in orientation 270",""+orientation);
				 *
				 * // bitmap = Bitmap.createScaledBitmap(bitmap, //
				 * MainConstants.ImageWidth, MainConstants.ImageHeight, //true);
				 * // return bitmap; } // return bitmap;
				 *
				 * } catch (IOException e1) { }
				 */
				if (bitmap != null) {
					Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0,
							bitmap.getWidth(), bitmap.getHeight(), mat, true);
					// Log.d("window", "getWidth" + bitmap.getWidth());
					// Log.d("window", "getHeight" + bitmap.getHeight());
					Bitmap resized = Bitmap.createScaledBitmap(newBitmap,
							MainConstants.visitorImageWidth,
							MainConstants.visitorImageHeight, true);

					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					resized.compress(Bitmap.CompressFormat.JPEG,
							MainConstants.compressQuality, bytes);
					// String sourceFileUri="/sdcard/VisitorMgmt/"+
					// "ikosmik1.jpg";
					File imageFile = new File(fileUri);
					// Log.d("scanning activity", "uploadbusinesscard" + f);
					// sourceFileUri=/sdcard/VisitorMgmt/+ "ikosmik1.jpg";

					imageFile.createNewFile();
					FileOutputStream fileOutStream = new FileOutputStream(
							imageFile);
					fileOutStream.write(bytes.toByteArray());
					fileOutStream.close();

					bitmap.recycle();
					bitmap = null;
					newBitmap.recycle();
					newBitmap = null;
					resized.recycle();
					resized = null;
				}
			}
		} catch (IOException e) {
			// Log.d("exception", "ioexception" + e);
		}

	}


	public byte[] storeresizedImage(byte[] fileUri, Integer rotation) {
		byte[] bytesArray = null;
		Matrix mat = new Matrix();

		if ((fileUri != null)
				&& (fileUri.toString().length() > MainConstants.kConstantZero)) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(fileUri,0,fileUri.length);
			int rotateangle = 0;
//				Get Device Angle to Set image.
			rotateangle = getDeviceRotationAngle(rotation);
			mat.setRotate(rotateangle, (float) bitmap.getWidth() / 2,
					(float) bitmap.getHeight() / 2);

			if (bitmap != null) {
				Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), mat, true);
				Bitmap resized ;
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				SharedPreferenceController sharedPreferenceController= new SharedPreferenceController();
//					Altered by Karthikeyan D S due to upload images as full clarity.
				int applicationMode = sharedPreferenceController.getApplicationMode(context);
				if(applicationMode==2) {
					resized = Bitmap.createBitmap(newBitmap);
				}
				else {
					resized = Bitmap.createScaledBitmap(newBitmap,
							MainConstants.visitorImageWidth,
							MainConstants.visitorImageHeight, true);
					resized.compress(Bitmap.CompressFormat.JPEG,
							MainConstants.compressQuality, bytes);
				}
				if(bytes!=null) {
					bytesArray=bytes.toByteArray();
				}
//					File imageFile = new File(fileUri);
//
//					imageFile.createNewFile();
//					FileOutputStream fileOutStream = new FileOutputStream(
//							imageFile);
//					fileOutStream.write(bytes.toByteArray());
//					fileOutStream.close();

				bitmap.recycle();
				bitmap = null;
				newBitmap.recycle();
				newBitmap = null;
				resized.recycle();
				resized = null;
			}
		}

		return bytesArray;
	}

	public int getDeviceRotationAngle(int rotation) {
		int rotateangle = 0;
		switch (rotation) {
//		case Surface.ROTATION_0:
//			// Log.d("orientation", " normal");
//			break;
//		case Surface.ROTATION_90:
//			rotateangle = 90;
//
//			// Log.d("orientation", " 90");
//
//			break;
//		case Surface.ROTATION_180:
//			rotateangle = 180;
//			// Log.d("orientation", " 180");
//			break;
//		case Surface.ROTATION_270:
//			rotateangle = 270;
//			// Log.d("orientation", " 270");
//			break;
//		default:
//			// Log.d("orientation", " default");

			case Surface.ROTATION_0:
				rotateangle = 0;
				break;
			case Surface.ROTATION_90:
				rotateangle = 0;
				break;
			case Surface.ROTATION_180:
				rotateangle = 180;
				break;
			case Surface.ROTATION_270:
				rotateangle = 90;
				break;
		}
		return rotateangle;
	}

	public byte[] storeresizedImage(byte[] fileUri, Integer rotation,int cameraID,Context context) {
		byte[] bytesArray = null;
		Matrix mat = new Matrix();
		if ((fileUri != null)
				&& (fileUri.toString().length() > NumberConstants.kConstantZero)) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(fileUri,0,fileUri.length);
			int rotateangle = 0;
			rotateangle = getDeviceRotationAngle(rotation, cameraID);
			mat.setRotate(rotateangle, (float) bitmap.getWidth() / 2,
					(float) bitmap.getHeight() / 2);
			if (bitmap != null) {
				Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), mat, true);
				Bitmap resized=null;
//				Log.d("DSK ","orientation"+context.getResources().getConfiguration().orientation);
				if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
					// directly add images without resizing to the bitmap
//					resized = Bitmap.createScaledBitmap(newBitmap,
//							MainConstants.visitorImageWidth,
//							MainConstants.visitorImageHeight, true);

//                    Log.d("DSK ","orientation Landscape");

					resized = Bitmap.createBitmap(newBitmap);
				}
				else if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
				{
					// directly add images without resizing to the bitmap
//					resized = Bitmap.createScaledBitmap(newBitmap,
//							MainConstants.visitorImageHeight,
//							MainConstants.visitorImageWidth, true);

//                    Log.d("DSK ","orientation Portrait");

					resized = Bitmap.createBitmap(newBitmap);
				}

				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				resized.compress(Bitmap.CompressFormat.JPEG,
						MainConstants.compressQuality, bytes);
				if(bytes!=null) {
					bytesArray=bytes.toByteArray();
				}
//					File imageFile = new File(fileUri);
//
//					imageFile.createNewFile();
//					FileOutputStream fileOutStream = new FileOutputStream(
//							imageFile);
//					fileOutStream.write(bytes.toByteArray());
//					fileOutStream.close();

				bitmap.recycle();
				bitmap = null;
				newBitmap.recycle();
				newBitmap = null;
				resized.recycle();
				resized = null;
			}
		}
		return bytesArray;
	}

	public int getDeviceRotationAngle(int rotation,int cameraID) {
		int rotateangle = 0;
//        Log.d("DSK ","rotation "+rotation+" cameraID "+cameraID);
		if(cameraID<0) {
			switch (rotation) {
				case Surface.ROTATION_0:
//					Log.d("DSK ","rotation1 "+rotation+" cameraID "+cameraID);
					rotateangle = 0;
					break;
				case Surface.ROTATION_90:
//					Log.d("DSK ","rotation2 "+rotation+" cameraID "+cameraID);
					rotateangle = 90;
					break;
				case Surface.ROTATION_180:
//					Log.d("DSK ","rotation3 "+rotation+" cameraID "+cameraID);
					rotateangle = 180;
					break;
				case Surface.ROTATION_270:
//					Log.d("DSK ","rotation4 "+rotation+" cameraID "+cameraID);
					rotateangle = 270;
					break;
				default:
					break;
			}
		} else {
			switch (rotation) {
				case Surface.ROTATION_0:
//					Log.d("DSK ","rotation1 "+rotation+" cameraID "+cameraID);
					rotateangle = 270;
					break;
				case Surface.ROTATION_90:
//					Log.d("DSK ","rotation2 "+rotation+" cameraID "+cameraID);
					rotateangle = 270;
					break;
				case Surface.ROTATION_180:
//					/Log.d("DSK ","rotation3 "+rotation+" cameraID "+cameraID);
					rotateangle = 180;
					break;
				case Surface.ROTATION_270:
//					Log.d("DSK ","rotation4 "+rotation+" cameraID "+cameraID);
					rotateangle = 270;
					break;
				default:
					break;
			}
		}
		return rotateangle;
	}

	/**
	 * Returns bitmap as a roundShape
	 *
	 * @param bitmap
	 *            pass bitmap as a original size, it used for resized bitmap as
	 *            rounded shape
	 * @return
	 */
	public Bitmap getRoundedRectBitmap(Bitmap bitmap) {
		Bitmap result = null;
		try {
			result = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(result);

			int color = Color.GREEN;
			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, 400, 400);

			paint.setAntiAlias(true);

			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawCircle(100, 100, 95, paint);
			// RectF oval = new RectF(80, 80, 80, 80);
			// canvas.drawOval(oval, paint);
			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);
			bitmap.recycle();
			bitmap = null;
		} catch (NullPointerException e) {
		} catch (OutOfMemoryError o) {
		}
		return result;
	}

	public Bitmap cropImage(Bitmap srcBmp) {
		Bitmap dstBmp = null;
		if (srcBmp != null) {
			// srcBmp
			// =BitmapFactory.decodeFile(StringConstants.kConstantFileDirectory
			// +StringConstants.kConstantFileFolder+file+
			// StringConstants.kConstantImageFileFormat);
			// Log.d("imageWidth",srcBmp.getWidth()+" height "+srcBmp.getHeight());
			if (srcBmp.getWidth() >= srcBmp.getHeight()) {

				// Log.d("if","crop");
				dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getHeight(),
						srcBmp.getHeight());

			} else {
				// Log.d("else","crop");
				dstBmp = Bitmap.createBitmap(srcBmp, 0, 0, srcBmp.getWidth(),
						srcBmp.getWidth());
			}
		}
		// storeBitmap(dstBmp, StringConstants.kConstantFileDirectory
		// +StringConstants.kConstantFileFolder+dstFileName+
		// StringConstants.kConstantImageFileFormat);
		return dstBmp;
	}

	// Load a bitmap from a resource with a target size
	public Bitmap decodeBitmap(String path, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(path, options);
	}

	// Load a bitmap from a resource with a target size
	public Bitmap decodeBitmap(byte[] bitmapdata, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		if(bitmapdata!=null && bitmapdata.length>0) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
//			removed by Karthikeyan D S based on to upload original Quality
//			Bitmap resizeBitmap = Bitmap.createScaledBitmap(
//					bitmap, reqWidth, reqHeight, false);

			Bitmap resizeBitmap = Bitmap.createBitmap(bitmap);
			return resizeBitmap;
		}
		return null;
	}

	/**
	 * removed based on to upload original Quality
	 *
	 * @author Karthikeyan D S
	 * @created on 12AUG2018
	 */
	public Bitmap decodeBitmap(byte[] bitmapdata) {
		// First decode with inJustDecodeBounds=true to check dimensions
		if(bitmapdata!=null && bitmapdata.length>0) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
			Bitmap resizeBitmap = Bitmap.createBitmap(bitmap);
			return resizeBitmap;
		}
		return null;
	}

	// Given the bitmap size and View size calculate a subsampling size (powers
	// of 2)
	int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
							  int reqHeight) {
		int inSampleSize = 1; // Default subsampling size
		// See if image raw height and width is bigger than that of required
		// view
		if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
			// bigger
			final int halfHeight = options.outHeight / 2;
			final int halfWidth = options.outWidth / 2;
			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		return inSampleSize;
	}

	// convert from byte array to bitmap
	public Bitmap getImage(byte[] bytes) {
		if(bytes != null && bytes.length > MainConstants.kConstantZero)
		{
			return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
		}
		else
		{
			return null;
		}
	}

	public Bitmap applyNoiceEffect(Bitmap source) {
		if(source!=null) {
			// get image size
			int width = source.getWidth();
			int height = source.getHeight();
			int[] pixels = new int[width * height];
			// get pixel array from source
			source.getPixels(pixels, 0, width, 0, 0, width, height);
			// a random object
			Random random = new Random();

			int index = 0;
			// iteration through pixels
			for(int y = 0; y < height; ++y) {
				for(int x = 0; x < width; ++x) {
					// get current index in 2D-matrix
					index = y * width + x;
					// get random color
					int colo =random.nextInt(255);
					int randColor = Color.rgb(colo,
							colo, colo);
					// OR
					pixels[index] |= randColor;
				}
			}
			// output bitmap
			Bitmap bmOut = Bitmap.createBitmap(width, height, source.getConfig());
			bmOut.setPixels(pixels, 0, width, 0, 0, width, height);
			source.recycle();
			pixels = new int[0];
			source=null;
			return bmOut;
		}
		return source;
	}

	public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
		Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

		float scaleX = newWidth / (float) bitmap.getWidth();
		float scaleY = newHeight / (float) bitmap.getHeight();
		float pivotX = 0;
		float pivotY = 0;

		Matrix scaleMatrix = new Matrix();
		scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

		Canvas canvas = new Canvas(scaledBitmap);
		canvas.setMatrix(scaleMatrix);
		canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

		return scaledBitmap;
	}

}
