package utility;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.zoho.app.frontdesk.android.R;
import java.util.ArrayList;

public class CustomKeyListAdapter extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> Item_title;
    private ArrayList<String> description;
    private ArrayList<String> KeyID;
    private ArrayList<Integer> item_image;
    private ArrayList<String> spare_key;
    private ArrayList<Integer> selection_image_set;
    private boolean item_selected=false;
    private ArrayList<String> bg_color_selected;
    public CustomKeyListAdapter(Activity context, ArrayList<String> Item_title, ArrayList<Integer> item_image,ArrayList<String> description,ArrayList<String> KeyID,ArrayList<String> spare_key,
                                ArrayList<Integer> selection_image_set,ArrayList<String> bg_color_selected) {
        super(context, R.layout.custom_array_adapter, Item_title);
        this.context = context;
        this.Item_title = Item_title;
        this.item_image = item_image;
        this.description = description;
        this.KeyID = KeyID;
        this.spare_key = spare_key;
        this.selection_image_set = selection_image_set;
        this.bg_color_selected = bg_color_selected;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.custom_array_adapter, null, true);
        TextView Item_Title = (TextView) rowView.findViewById(R.id.textView_item_title);
        ImageView Item_image = (ImageView) rowView.findViewById(R.id.imageView_key_picture);
        TextView text_description = rowView.findViewById(R.id.textView_description);
        TextView key_id = rowView.findViewById(R.id.textView_key_id);
        ImageView Item_selected_image = (ImageView) rowView.findViewById(R.id.imageView_Selected);
        TextView is_spare_key = rowView.findViewById(R.id.textView_key_is_spare);
        RelativeLayout layout_list_item = rowView.findViewById(R.id.layout_list_item);
        layout_list_item.setBackgroundColor(Color.parseColor(bg_color_selected.get(position)));
        Item_Title.setText(Item_title.get(position));
        Item_image.setImageResource(item_image.get(position));
        text_description.setText(description.get(position));
        key_id.setText(KeyID.get(position));
        Item_selected_image.setImageResource(selection_image_set.get(position));
        is_spare_key.setText(spare_key.get(position));
        return rowView;
    }
    public void selectItem(){
        item_selected = true;
    }
    public void de_selectItem(){
        item_selected = false;
    }
}

