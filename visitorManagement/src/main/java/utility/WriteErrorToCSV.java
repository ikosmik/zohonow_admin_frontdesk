package utility;

import com.zoho.app.frontdesk.android.ApplicationController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import constants.ErrorConstants;
import constants.MainConstants;

public class WriteErrorToCSV {

	/*
	 * 
	 * write csv file to External Storage Public Directory
	 */
	public static void writeData(String error, int errorCode) {
		if(ApplicationController.getInstance()!=null && ApplicationController.getInstance().checkExternalPermission()){

			File fileDir = new File(MainConstants.kConstantFileDirectory);

			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}

			Date date = new Date();
			String fileName = ErrorConstants.errorCodeCSV;

			File file = new File(fileDir, fileName);
			// Log.d("Android","1"+file.exists());

			FileWriter filewriter;
			// Log.d("Android","done");
			try {
				// file.createNewFile();
				if (!file.exists()) {
					// Log.d("Android","exists");

					filewriter = new FileWriter(file);
					BufferedWriter out = new BufferedWriter(filewriter);
					// Log.d("Android","done");
					out.write(ErrorConstants.dateFieldCSV + ","
							+ ErrorConstants.ErrorMsgFieldCSV + ","
							+ ErrorConstants.errorCodeFieldCSV
							+ MainConstants.kConstantNewLine);

					out.write(date.toString() + "," + error + "," + errorCode
							+ MainConstants.kConstantNewLine);
					// Log.d("Android","done");
					out.close();

					// Log.d("window","value:");
				} else {

					filewriter = new FileWriter(file, true);
					BufferedWriter out = new BufferedWriter(filewriter);
					// Log.d("Android","done");

					out.write(date.toString() + "," + error + "," + errorCode
							+ MainConstants.kConstantNewLine);
					// Log.d("Android","done");
					out.close();
				}
			} catch (IOException e) {
			}
		}
	}

	public static void writeTempData(String CSVDate, String error, String errorCode){
		if(ApplicationController.getInstance()!=null && ApplicationController.getInstance().checkExternalPermission()){

			File fileDir = new File(MainConstants.kConstantFileDirectory);

			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}

//		Date date = new Date();
			String fileName = ErrorConstants.errorCodeCSVTemp;

			File file = new File(fileDir, fileName);
			// Log.d("Android","1"+file.exists());

			FileWriter filewriter;
			// Log.d("Android","done");
			try {
				// file.createNewFile();
				if (!file.exists()) {
					// Log.d("Android","exists");

					filewriter = new FileWriter(file);
					BufferedWriter out = new BufferedWriter(filewriter);
					// Log.d("Android","done");
					out.write(ErrorConstants.dateFieldCSV + ","
							+ ErrorConstants.ErrorMsgFieldCSV + ","
							+ ErrorConstants.errorCodeFieldCSV
							+ MainConstants.kConstantNewLine);

					out.write(CSVDate.toString() + "," + error + "," + errorCode
							+ MainConstants.kConstantNewLine);
					// Log.d("Android","done");
					out.close();

					// Log.d("window","value:");
				} else {

					filewriter = new FileWriter(file, true);
					BufferedWriter out = new BufferedWriter(filewriter);
					// Log.d("Android","done");

					out.write(CSVDate.toString() + "," + error + "," + errorCode
							+ MainConstants.kConstantNewLine);
					// Log.d("Android","done");
					out.close();
				}
			} catch (IOException e) {

			}
		}

	}
}
