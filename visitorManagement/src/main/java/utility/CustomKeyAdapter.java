package utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.ApplicationController;
import com.zoho.app.frontdesk.android.R;

import java.util.List;

import constants.FontConstants;

public class CustomKeyAdapter extends ArrayAdapter<String> {

    private Activity context;
    private String[] Item_title;
    private String[] description;
    private String[] KeyID;
    private Integer[] item_image;
    private String[] spare_key;
    public CustomKeyAdapter(Activity context, String[] Item_title, Integer[] item_image,String[] description,String[] KeyID,String[] spare_key) {
        super(context, R.layout.custom_array_adapter, Item_title);
        this.context = context;
        this.Item_title = Item_title;
        this.item_image = item_image;
        this.description = description;
        this.KeyID = KeyID;
        this.spare_key = spare_key;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.custom_array_adapter, null, true);
        TextView Item_Title = (TextView) rowView.findViewById(R.id.textView_item_title);
        ImageView Item_image = (ImageView) rowView.findViewById(R.id.imageView_key_picture);
        TextView text_description = rowView.findViewById(R.id.textView_description);
        TextView key_id = rowView.findViewById(R.id.textView_key_id);
        ImageView Item_selected_image = (ImageView) rowView.findViewById(R.id.imageView_Selected);
        TextView is_spare_key = rowView.findViewById(R.id.textView_key_is_spare);
        Item_Title.setText(Item_title[position]);
        Item_image.setImageResource(item_image[position]);
        text_description.setText(description[position]);
        key_id.setText(KeyID[position]);
        Item_selected_image.setImageResource(R.drawable.green_no_tick);
        is_spare_key.setText(spare_key[position]);
        return rowView;
    }
}

