package utility;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * this class is used to compress the image to utilize the memory usage 
 * @author Shiva
 * @created 07/10/2016
 */
public class CompressImage {
	/**
	 * Function to calculate Samplesize of the image 
	 * 
	 * @param options BitmapFactory
	 * @param reqWidth require Width to compress
	 * @param reqHeight require Height to compress
	 * @return inSampleSize
	 * 
	 * @author Shiva
	 * @created 07/10/2016
	 */
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = constants.MainConstants.kConstantOne;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / constants.MainConstants.kConstantTwo;
        final int halfWidth = width / constants.MainConstants.kConstantTwo;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) >= reqHeight
                && (halfWidth / inSampleSize) >= reqWidth) {
            inSampleSize *= constants.MainConstants.kConstantTwo;
        }
    }

    return inSampleSize;
	}
	/**
	 * Function to decode the image
	 * 
	 * @param res
	 * @param resId
	 * @param reqWidth
	 * @param reqHeight
	 * @return Bitmap
	 * 
	 * @author Shiva
	 * @created 07/10/2016
	 */
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
		
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	public static Bitmap decodeSampleBitmapFromSdCard(Resources res, String resLocation,
	        int reqWidth, int reqHeight){
		
		// First decode with inJustDecodeBounds=true to check dimensions
		
	    BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(resLocation, options);
	    

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeFile(resLocation, options);
		
	}
}
