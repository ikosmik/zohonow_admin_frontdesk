package utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;



import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;

public class TimeZoneConventer {

	private static final SimpleDateFormat formatter = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss a");

	// String CurrentTime;

	public static Integer getGMTTime() {
		TimeZone tz = TimeZone.getDefault();
		Date now = new Date();
		int offsetFromUtc = tz.getOffset(now.getTime()) / (1000 * 60);
		//Log.d("gmttime", "minutes" + offsetFromUtc);
		return offsetFromUtc;

	}
	
	/**
	 * @author jayapriya
	 * @created on 28/09/15
	 * 
	 * Get date string into milliseconds
	 */
	 public static Long getLocalDateTimeInMilliSeconds(String date){
		 SimpleDateFormat f = new SimpleDateFormat(MainConstants.creatorDateFormat);
		 Date d = null;
		try {
			d = f.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			//Log.d("time", "parseException" +e.getLocalizedMessage());
			//e.printStackTrace();
		}
		long milliseconds = -1;
		if(d!=null) {
		  milliseconds = d.getTime();
		}
		 ////Log.d("time","milliseconds"+milliseconds);
		return milliseconds;
	 }
	 
	 /**
		 * @author Karthick
		 * @created on 20170525
		 * 
		 * Get date string into milliseconds
		 */
		 public static Long getDateTimeInMilliSeconds(String date){
			 SimpleDateFormat f = new SimpleDateFormat(MainConstants.kConstantDateTimeFormat);
			 Date d = null;
			try {
				d = f.parse(date);
			} catch (ParseException e) {

			}
			long milliseconds = -1;
			if(d!=null) {
			  milliseconds = d.getTime();
			}
			return milliseconds;
		 }

	/**
	 * @author Karthikeyan D S
	 * @created on 26Sep2018
	 *
	 * Get date string into milliseconds
	 */
	public static long getDateTimeInMilliSeconds(String date,String dateFormat){
		SimpleDateFormat f = new SimpleDateFormat(dateFormat);
		Date d = null;
		try {
			d = f.parse(date);
		} catch (ParseException e) {

		}
		long milliseconds = -1;
		if(d!=null) {
			milliseconds = d.getTime();
		}
		return milliseconds;
	}
	/**
	 * Return date in specified format.
	 * @param milliSeconds Date in milliseconds
	 * @param dateFormat Date format
	 * @return String representing date in specified format
	 */
	public static String getDate(long milliSeconds,String dateFormat)
	{
		// Create a DateFormatter object for displaying date in specified format.
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	/**
	  * Return date in specified format.
	  * @param milliSeconds Date in milliseconds
	  *
	  * @return String representing date in specified format
	  */
	 public static String getDate(long milliSeconds)
	 {
	     // Create a DateFormatter object for displaying date in specified format.
	     SimpleDateFormat formatter = new SimpleDateFormat(MainConstants.creatorDateFormat);

	     // Create a calendar object that will convert the date and time value in milliseconds to date.
	      Calendar calendar = Calendar.getInstance();
	      calendar.setTimeInMillis(milliSeconds);
	      return formatter.format(calendar.getTime());
	 }

	/**
	 * Return date in specified format.
	 * @param milliSeconds Date in milliseconds
	 * @param timeZone Date format
	 * @return String representing date in specified format
	 */
	public static String getTimeForCheckedInTime(long milliSeconds,String timeZone)
	{
		String timeCheckedIn = MainConstants.kConstantEmpty;
		String diff = MainConstants.kConstantEmpty;

//		// Create a DateFormatter object for displaying date in specified format.
		SimpleDateFormat formatter = new SimpleDateFormat(timeZone);

		// Create a calendar object that will convert the date and time value in milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		timeCheckedIn = formatter.format(calendar.getTime());

		return timeCheckedIn;
	}

    /**
     *
     * @param context
     *            the context object is used to get ConnectivityManager
     * @return
     */
    public static Boolean checkConnection(Context context) {
        int netType = 0;
        int netSubtype = 0;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
		/*
		 * NetworkInfo activeNetwork = cm.getActiveNetworkInfo(); isConnected =
		 * activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		 * Log.d("connected", "connection" + isConnected); return isConnected;
		 */
        NetworkInfo wifiNetwork = cm.getActiveNetworkInfo();
        if(wifiNetwork != null&& wifiNetwork.isConnected()){
            netType = wifiNetwork.getType();
            netSubtype = wifiNetwork.getSubtype();

        }
        //WifiManager wifiManger = (WifiManager) context
        //.getSystemService(Context.WIFI_SERVICE);
        //Wifi//Info wifiInfo = wifiManger.getConnectionInfo();
        //int speedMbps = wifiInfo.getLinkSpeed();

        if (wifiNetwork != null && wifiNetwork.isConnected()
                && (netType == ConnectivityManager.TYPE_WIFI)) {

            return true;

        } else if ((wifiNetwork != null) && (wifiNetwork.isConnected())
                && (netType == ConnectivityManager.TYPE_MOBILE)) {

            return isMobileConnectionAvailable(netType, netSubtype);

        }

        return false;
    }
    
    /*
	 * Get wifi is enabled
	 * 
	 * Created on 20150725
	 * Created by karthick
	 */
	public boolean isWiFiEnabled(Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if(wifiManager!=null && wifiManager.isWifiEnabled())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	 * WiFi reconnect
	 * 
	 * Created on 20150725
	 * Created by karthick
	 */
	public void wifiReconnect(Context context) {
		
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		wifiManager.setWifiEnabled(true);
		wifiManager.reconnect();
		}

    /**
     *
     * @param type
     *            get the network type in integer
     * @param subType
     *            get network subtype in integer
     * @return boolean value - if the network is available then return true else
     *         return false.
     */
    public static boolean isMobileConnectionAvailable(int type, int subType) {
        if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    //Log.d("network1", "" + TelephonyManager.NETWORK_TYPE_1xRTT);
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    //Log.d("network2", "" + TelephonyManager.NETWORK_TYPE_CDMA);
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    //Log.d("network3", "" + TelephonyManager.NETWORK_TYPE_EDGE);
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    //Log.d("network4", "" + TelephonyManager.NETWORK_TYPE_EVDO_0);
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    //Log.d("network5", "" + TelephonyManager.NETWORK_TYPE_EVDO_A);
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    //Log.d("network6", "" + TelephonyManager.NETWORK_TYPE_GPRS);
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    //Log.d("network7", "" + TelephonyManager.NETWORK_TYPE_HSDPA);
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    //Log.d("network8", "" + TelephonyManager.NETWORK_TYPE_HSPA);
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    //Log.d("network9", "" + TelephonyManager.NETWORK_TYPE_HSUPA);
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                   // Log.d("network10", "" + TelephonyManager.NETWORK_TYPE_UMTS);
                    return true; // ~ 400-7000 kbps
				/*
				 * Above API level 7, make sure to set android:targetSdkVersion
				 * to appropriate level to use these
				 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    //Log.d("network11", "" + TelephonyManager.NETWORK_TYPE_EHRPD);
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    //Log.d("network12", "" + TelephonyManager.NETWORK_TYPE_EVDO_B);
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    //Log.d("network13", "" + TelephonyManager.NETWORK_TYPE_HSPAP);
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    //Log.d("network14", "" + TelephonyManager.NETWORK_TYPE_IDEN);
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    //Log.d("network15", "" + TelephonyManager.NETWORK_TYPE_LTE);
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    //Log.d("network16", "" + TelephonyManager.NETWORK_TYPE_UNKNOWN);
                    return true;
                default:
                    //Log.d("network17", "false");
                    return false;
            }
        } else {
            return false;
        }
    }

    /**
     * check the date as the future date or not.
     * @param date
     * @author jayapriya
     * @created 06/03/2015
     */
    public static long checkExpectedDateOfVisit(Context context, String date){
        long different =0l;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        long currentDate=System.currentTimeMillis();
        try {
            Date mDate = sdf.parse(date);
             long timeInMilliseconds = mDate.getTime();
            different=timeInMilliseconds - currentDate;
            } catch (ParseException e) {
            //e.printStackTrace();
        }
        //Log.d("diffrent","" +different);
        return different;
    }

    /**
     * get current date and time
     *
     * @author jayapriya
     * @created 06/03/2015
     */
	public static String getCurrentDateAndTime() {
		String daySplit = "";
		String currentDate = "", hour, month;
		Integer day, year;
		SimpleDateFormat formatter, timeFormatter;

		day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		formatter = new SimpleDateFormat("MMMM");
		month = formatter.format(Calendar.getInstance().getTime());
		//Log.d("welcome", "month" + month);
		year = Calendar.getInstance().get(Calendar.YEAR);
		// hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		timeFormatter = new SimpleDateFormat("hh:mm a");
		hour = timeFormatter.format(Calendar.getInstance().getTime());
		// minute = Calendar.getInstance().get(Calendar.MINUTE);
		// am_pm = Calendar.getInstance().get(Calendar.AM_PM);

		if (day <= 31) {
			if ((day == 11) || (day == 12) || (day == 13))
				{
				daySplit = "th";
				//Log.d("welcome if ", "day" + day);
			}
			else
			{
			 int n = day % 10;
			if(n == 1)
			{
				daySplit = "st";
				//Log.d("welcome switch 1", "day");
			}
			else if(n == 2)
			{
				//Log.d("welcome switch 2", "day");
				daySplit = "nd";
			}
			else if(n == 3){
				
				//Log.d("welcome switch 3", "day");
				daySplit = "rd";
			}
			else
			{
				//Log.d("welcome switch default ", "day");
				daySplit = "th";
			}
			}
		}
		currentDate = day + daySplit + " " + month + " " + year + "\n"
				+ hour;
		//Log.d("Android", "date: "+currentDate);
    	
		return currentDate;
	}

    /**
     * @created by jayapriya on 14/01/15 get the different between two times.
     * @param oldTime - it will be a forgotId requested time, get it from the local forgotIdRequested table
     * @param currentTime =  it is a current system time
     * @return String it has the difference of this two times.
     */
    public static String getDifferentBetweenTwoTimes(String oldTime,
                                                     String currentTime) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        Date Date1, Date2;
        long Hours = MainConstants.kConstantZero, Mins = MainConstants.kConstantZero;
        String issuedTime = null;
        try {
            Date1 = format.parse(oldTime);
            Date2 = format.parse(currentTime);
            long mills = Date1.getTime() - Date2.getTime();

            Hours = TimeUnit.MILLISECONDS.toHours(mills);
           // Mins = TimeUnit.MILLISECONDS.toMinutes(mills) % MainConstants.kConstantMaximumMinutes;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
           // e.printStackTrace();
        }
        if((Hours ==  MainConstants.kConstantZero) && (Mins >  MainConstants.kConstantZero)){
            //issuedTime =  Mins
                  //  + MainConstants.kConstantMinSymbolAndTextAgo;
        }
        else if((Hours >  MainConstants.kConstantZero) && (Mins >  MainConstants.kConstantZero))
        {
            //issuedTime =Hours + MainConstants.kConstantHourSymbol + Mins
                   // + MainConstants.kConstantMinSymbolAndTextAgo;
        }
        else if((Hours >  MainConstants.kConstantZero) && (Mins ==  MainConstants.kConstantZero))
        {
            //issuedTime = Hours
                  //  + MainConstants.kConstantHourSymbolAndTextAgo;
        }
        else
        {
            issuedTime =MainConstants.kConstantEmpty;
        }
        return issuedTime;
    }
    /*
         * get current date and time return
         * String - current will be return in given format.
         *
         * @author jayapriya
         * @created Jan 2015
         */
    public static String getCurrentDateTime() {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                MainConstants.kConstantDateTimeFormat);
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Created by jayapriya on 02/02/2015
     * get current GmtTime.
     * @return
     */
    public static String getGmtTime(){
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat(MainConstants.kConstantDateTimeFormat);
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        System.out.println(dateFormatGmt.format(new Date())+ MainConstants.kConstantEmpty);
        return dateFormatGmt.format(new Date());
    }
    
    
    public static String showHappyGreeting(){
    	Calendar c = Calendar.getInstance();
    	int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

    	if(timeOfDay >= 0 && timeOfDay < 12){
    	    return StringConstants.kConstantGreetingMorning;
    	}else if(timeOfDay >= 12 && timeOfDay < 16){
    		return StringConstants.kConstantGreetingNoon;
    	}else if(timeOfDay >= 16 && timeOfDay < 21){
    		return StringConstants.kConstantGreetingEvening;
    	}else if(timeOfDay >= 21 && timeOfDay < 24){
    		return StringConstants.kConstantGreetingNight;
    	}
		return "";
    }

	/**
	 * get Time Difference.
	 *
	 * @author Karthikeyan D S
	 * @created on 20180405
	 */
	public static String TimerFunction(String checkedInTime,String CheckedOutCurrentTime){
		Date Start = null;
		Date End = null;
		String timeDiffernce = MainConstants.kConstantEmpty;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MainConstants.creatorDateFormatForgetID);
		try {
			Start = simpleDateFormat.parse(checkedInTime);
			End = simpleDateFormat.parse(CheckedOutCurrentTime);}
		catch(ParseException e){
			//Some thing if its not working
		}

		if(End!=null) {
			long difference = End.getTime() - Start.getTime();
			int days = (int) (difference / (1000 * 60 * 60 * 24));
			int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
			int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
			if (hours < 0) {
				hours += 24;
			}
			if (min < 0) {
				float newone = (float) min / 60;
				min += 60;
				hours = (int) (hours + newone);
			}

			if (days > 0) {
				timeDiffernce = days + " d";
			}
			if (hours > 0) {
				if(days > 0) {
					timeDiffernce = timeDiffernce + ": ";
				}
				timeDiffernce = timeDiffernce + hours + " h";
			}
			if (min > 0) {
				if (hours > 0) {
					timeDiffernce = timeDiffernce + ": ";
				}
				timeDiffernce = timeDiffernce + min + " m";
			}
		}
//		Log.d("ANSWER",timeDiffernce);
		return timeDiffernce;
	}

	/**
	 * get Time in Milliseconds for given days before or after
	 *
	 * @author Karthikeyan D S
	 * @created on 28Sep2018
	 */
	public static long getRequiredDateinMilliSeconds(String currentDateFormat,int daysBeforeorAfter,String toDateFormat) {
		long dateValueinMillis = NumberConstants.kConstantMinusOne;
		if(currentDateFormat!=null && !currentDateFormat.isEmpty() &&toDateFormat!=null && !toDateFormat.isEmpty()) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(currentDateFormat);
			Date myDate = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(myDate);
			calendar.add(Calendar.DAY_OF_YEAR, daysBeforeorAfter);
			Date newDate = calendar.getTime();
			String date = dateFormat.format(newDate);
//		    Log.d("Date ", " " + date);
// 			append start time of the Day for the date which is get from conversion.
			date = date + " 00:00:00";
//		    Log.d("Date ", " " + date);
//		    Log.d("Date ", " " + TimeZoneConventer.getDateTimeInMilliSeconds(date, toDateFormat));
			dateValueinMillis = TimeZoneConventer.getDateTimeInMilliSeconds(date, toDateFormat);
		}
			return dateValueinMillis;
	}
}
