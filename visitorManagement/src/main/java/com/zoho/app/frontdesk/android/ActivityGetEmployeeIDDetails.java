package com.zoho.app.frontdesk.android;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.stationery.ActivityStationeryDragDrop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;
import appschema.SharedPreferenceController;
import utility.Validation;

public class ActivityGetEmployeeIDDetails extends Activity {

    private RelativeLayout relativeLayoutExisitingVisitor;
    private ImageView imageviewBack, imageviewHome, imageviewPrevious,
            imageviewNext, image_view_next,imageViewIndicator;
    private Typeface typeFaceTitle, typefaceVisitorTempID;
    private int actionCode = MainConstants.backPress;
    private Handler noActionHandler;
    private EditText editTextEmpIdNumber;
    private Spinner spinnerEmployeeExtension;
    //	private int fromPage = MainConstants.fromHaveDevice;
    private TextToSpeech textToSpeech;
    private TextView textviewEmployeeEmpIDTitle, textviewEmployeeEmpIDErrorTitle;
    private DbHelper database;
    private SharedPreferenceController sharedPreferenceController;
    private int applicationMode = NumberConstants.kConstantZero;
    private Validation validation;
    private List<String> employeeExtensionDetails = new ArrayList<String>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.employee_id);

        assignObjects();
        setAppBackground();
        assignFont();
        assignListeners();
    }

    /**
     * Assign Objects.
     *
     * @author Karthikeyan D S
     * @created on 05042018
     */
    private void assignObjects()
    {
        editTextEmpIdNumber = (EditText) findViewById(R.id.editText_emp_id);
//		textViewErrorInTempID = (TextView) findViewById(R.id.textViewErrorInTempID);
        textviewEmployeeEmpIDTitle = (TextView) findViewById(R.id.textview_emp_id_title);
        textviewEmployeeEmpIDErrorTitle = (TextView) findViewById(R.id.textview_emp_id_error_title);
        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);
        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);
        spinnerEmployeeExtension = (Spinner) findViewById(R.id.spinner_employee_extension);
        imageViewIndicator = (ImageView)findViewById(R.id.image_view_indicator);

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

//		if(getIntent()!=null) {
//			fromPage = getIntent().getIntExtra(MainConstants.putExtraFromPage, MainConstants.fromContact);
//		}

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

//      get Application Mode based on device Location assigned in Settings Page
        if (sharedPreferenceController == null) {
            sharedPreferenceController = new SharedPreferenceController();
        }
        if (sharedPreferenceController != null) {
            applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
        }
//        Animation animation = new AlphaAnimation(1,0);
//        animation.setDuration(2000);
//        animation.setInterpolator(new LinearInterpolator());
//        animation.setRepeatCount(2);
//        animation.setRepeatMode(Animation.REVERSE);
//        imageViewIndicator.startAnimation(animation);

        ObjectAnimator
                .ofFloat(imageViewIndicator, "translationX", 0, 25, -25, 25, -25,15, -15, 6, -6, 0)
                .setDuration(2000)
                .start();
        loadSpinnerValues();
    }

    /**
     * Assign FontConstants.
     *
     * @author Karthikeyan D S
     * @created on 05042018
     */
    private void assignFont() {
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceVisitorTempID = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);

        if (editTextEmpIdNumber != null && typefaceVisitorTempID != null) {
            editTextEmpIdNumber.setTypeface(typefaceVisitorTempID);
        }
        if (textviewEmployeeEmpIDTitle != null && typeFaceTitle != null) {
            textviewEmployeeEmpIDTitle.setTypeface(typeFaceTitle);
        }
        if (textviewEmployeeEmpIDErrorTitle != null && typeFaceTitle != null) {
            textviewEmployeeEmpIDErrorTitle.setTypeface(typeFaceTitle);
        }
    }

    /**
     * Assign FontConstants.
     *
     * @author Karthikeyan D S
     * @created on 05042018
     */
    private void assignListeners() {

        if(editTextEmpIdNumber!=null && listenerDonePressed!=null) {
            editTextEmpIdNumber.setOnEditorActionListener(listenerDonePressed);
        }
        if(editTextEmpIdNumber!=null && textWatcherTempIDChanged!=null) {
            editTextEmpIdNumber.addTextChangedListener(textWatcherTempIDChanged);
        }
        // Onclick listeners
        if (imageviewPrevious != null && listenerPrevious != null) {
            imageviewPrevious.setOnClickListener(listenerPrevious);
        }
        if (imageviewNext != null && listenerConfirm != null) {
            imageviewNext.setOnClickListener(listenerConfirm);
        }
        if (image_view_next != null && listenerConfirm != null) {
            image_view_next.setOnClickListener(listenerConfirm);
        }
        if (imageviewHome != null && listenerHomePage != null) {
            imageviewHome.setOnClickListener(listenerHomePage);
        }
        if (imageviewBack != null && listenerBack != null) {
            imageviewBack.setOnClickListener(listenerBack);
        }
        if (spinnerEmployeeExtension != null && listenerSpinnerSelection != null) {
            spinnerEmployeeExtension.setOnItemSelectedListener(listenerSpinnerSelection);
        }
    }

    /**
     * Set background color
     *
     * @author Karthikeyan D S
     * @created on 05042018
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFfa654b, 0xFFfc5d55, 0xFFfd575d, 0xFFff5066});
                relativeLayoutExisitingVisitor.setBackground(gd);
                textviewEmployeeEmpIDErrorTitle.setTextColor(Color.parseColor("#FFFFFF"));
            }
        }
    }

    /**
     * load Spinner Values
     *
     * @author Karthikeyan D S
     * @created on 25Sep2018
     */
    private void loadSpinnerValues() {
        String[] arrayList = null;
        String employeeExtension = MainConstants.kConstantEmpty;
        if (sharedPreferenceController == null) {
            sharedPreferenceController = new SharedPreferenceController();
        }
        employeeExtension = sharedPreferenceController.getEmployeeExtension(getApplicationContext());
//        Log.d("DSK "," employeeExtension "+employeeExtension);
        if (spinnerEmployeeExtension != null && employeeExtension != null && !employeeExtension.isEmpty()) {
            hideKeyboard();
            if (employeeExtensionDetails != null) {
                employeeExtensionDetails.clear();
            }
            arrayList = employeeExtension.split(MainConstants.kConstantComma);
            for (int i = 0; i < Arrays.asList(arrayList).size(); i++) {
//                Log.d("DSK "," arrayList "+arrayList[i]);
                if (arrayList != null && arrayList[i] != null && !arrayList[i].isEmpty()) {
//                    Log.d("DSK "," arrayList "+arrayList[i]);
                    employeeExtensionDetails.add(arrayList[i].trim());
                }
            }
            employeeExtensionDetails.add(StringConstants.kConstantEmployeeExtensionAll);
            Collections.sort(employeeExtensionDetails);
        }
        else
        {
            employeeExtensionDetails.add(StringConstants.kConstantEmployeeExtensionAll);
        }
        if(employeeExtensionDetails!=null) {
            setSpinnerDetails(employeeExtensionDetails);
        }
    }

    /**
     * set Spinner Values
     *
     * @author Karthikeyan D S
     * @created on 25Sep2018
     */
    private void setSpinnerDetails(List<String> employeeExtensionDetails)
    {
        if(employeeExtensionDetails!=null) {
            ArrayAdapter<String> employeeExtensionAdapter = new ArrayAdapter<String>(this,
                    R.layout.employee_extension_spinner, employeeExtensionDetails) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view= super.getView(position, convertView, parent);
                    ((TextView) view).setTypeface(typefaceVisitorTempID);
                    return view;
                }

                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    ((TextView) view).setTypeface(typefaceVisitorTempID);
                    view.setBackgroundColor(getResources().getColor(R.color.spinner_background_employee_forget_id));
                    return view;
                }
            };
            spinnerEmployeeExtension.setAdapter(employeeExtensionAdapter);
            spinnerEmployeeExtension.setSelection(employeeExtensionAdapter.getPosition(StringConstants.kConstantEmployeeExtensionAll));
            employeeExtensionAdapter.notifyDataSetChanged();
        }
    }
    /**
     * Hide keyboard
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show keyboard
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!imm.isActive()) {
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    /**
     * Check is existing user or not
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private boolean validateTempID() {
        if(editTextEmpIdNumber!=null && editTextEmpIdNumber.getText()!=null) {
            String employeeId = editTextEmpIdNumber.getText().toString().trim();
            if (employeeId != null && !employeeId.trim().isEmpty()
                    && employeeId.length() > MainConstants.kConstantZero) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Launch previous activity
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void launchHomePage() {
        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            launchNextPage();
        }
    }

    /**
     * Launch previous activity.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void launchPreviousPage() {
        Intent intent = new Intent(getApplicationContext(),
                HomePage.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch next activity based on the deviceLocation.
     * if applicationMode
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void launchNextPage() {
        if (applicationMode == NumberConstants.kConstantNormalMode) {
            if (SingletonCode.getSingletonSelectionCode() != null &&
                    SingletonCode.getSingletonSelectionCode() == MainConstants.employeeSingletonCode) {
                stationaryActivityCall();
            } else if (SingletonCode.getSingletonSelectionCode() != null &&
                    SingletonCode.getSingletonSelectionCode() == MainConstants.employeeSingletonCodeTenkasi) {
                callEmployeeOptionGetActivity();
            }
        } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
//            DataCentre Mode Do Nothing.
        } else {
            launchPreviousPage();
        }
    }

    /**
     * Launch next Page activity to get Tenkasi Employee Option for Stationery and ForgetID.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void callEmployeeOptionGetActivity() {
        Intent nextActivityIntent = new Intent(getApplicationContext(), ActivityGetEmployeeActionOption.class);
        startActivity(nextActivityIntent);
        finish();
    }
    /**
     * Goto home page when no action.
     *
     * @author Karthick
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };

    /**
     * set visibility for nect button
     *
     * @author Karthick
     * @created 20170615
     */
    private AdapterView.OnItemSelectedListener listenerSpinnerSelection = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parentView,
                                   View selectedItemView, int position, long id) {
//            Log.d("DSK ", "No Action");
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            if (editTextEmpIdNumber != null) {
                editTextEmpIdNumber.setFocusable(true);
            }
            showKeyboard();
        }

        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private View.OnClickListener listenerConfirm = new View.OnClickListener() {
        public void onClick(View v) {
            if (storeEmpIDNumber()) {
                textviewEmployeeEmpIDErrorTitle.setVisibility(View.GONE);
                launchNextPage();
            } else {
                //    UsedCustomToast.makeCustomToast(ActivityGetEmployeeIDDetails.this, MainConstants.kConstantEmployeeIDNotFound, Toast.LENGTH_SHORT, Gravity.TOP).show();
//                    launchPreviousPage();
            }
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private View.OnClickListener listenerConfirms = new View.OnClickListener() {
        public void onClick(View v) {
            if (storeEmpIDNumber()) {
                textviewEmployeeEmpIDErrorTitle.setVisibility(View.GONE);
                launchNextPage();
            } else {
                textviewEmployeeEmpIDErrorTitle.setVisibility(View.VISIBLE);
                textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEnterEmployeeID);
//                UsedCustomToast.makeCustomToast(ActivityGetEmployeeIDDetails.this, MainConstants.kConstantEnterEmployeeID, Toast.LENGTH_SHORT, Gravity.TOP).show();
//                    launchPreviousPage();
            }
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private View.OnClickListener listenerPrevious = new View.OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };
    /**
     * intent to launch previous page
     *
     * @author karthikeyan D S
     * @created on 05/04/18
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            //showDetailDialog();
        }
    };
    /**
     * intent to launch previous page
     *
     * @author karthikeyan D S
     * @created on 05/04/18
     */
    private View.OnClickListener listenerHomePage = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.homePress;
        }
    };
    /**
     * when the done pressed then process start.
     *
     * @author karthikeyan D S
     * @created on 05/04/18
     */
    private TextView.OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                if (validateTempID()) {
                    if (storeEmpIDNumber()) {
                        launchNextPage();
                        return true;
                    } else {
                        if(textviewEmployeeEmpIDErrorTitle!=null) {
                            textviewEmployeeEmpIDErrorTitle.setVisibility(View.VISIBLE);
                            textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEmployeeIDNotFound);
                        }
                        return false;
                    }
                } else {
                    if(textviewEmployeeEmpIDErrorTitle!=null) {
                        textviewEmployeeEmpIDErrorTitle.setVisibility(View.VISIBLE);
                        textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEnterEmployeeID);
                    }
                }
            }
            return false;
        }
    };
    /**
     * textWatcher to validate the Visitor MobileNo
     *
     * @author Karthick
     * @created on 20180408
     */
    private TextWatcher textWatcherTempIDChanged = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            validateTempIDTyped();
            if(textviewEmployeeEmpIDErrorTitle!=null) {
                textviewEmployeeEmpIDErrorTitle.setVisibility(View.GONE);
            }
            if(noActionHandler!=null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            }
        }

        public void afterTextChanged(Editable s) {

        }

    };

    /**
     * Function to Launch Stationary Activity
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void stationaryActivityCall() {
        Intent nextActivityIntent = new Intent(getApplicationContext(), ActivityStationeryDragDrop.class);
        startActivity(nextActivityIntent);
        finish();
    }

    /**
     * Function to Launch Forget ID Activity on call.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void forgotIdActivity() {
        Intent nextActivityIntent = new Intent(
                getApplicationContext(),
                EmployeeTemporaryIdActivity.class);
        startActivity(nextActivityIntent);
        finish();
    }

    /**
     * Play text to voice
     *
     * @author karthikeyan D S
     * @created on 05/04/18
     */
    private void textSpeach(int textSpeechCode) {

        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeechCode == MainConstants.kConstantOne
                    && (editTextEmpIdNumber != null
                    && editTextEmpIdNumber.getText() != null && editTextEmpIdNumber
                    .getText().toString().length() <= 0)) {
                textToSpeech.speak(TextToSpeachConstants.speachEmpId,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeechCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * store EmployeeIDNumber
     *
     * @author karthikeyan D S
     * @created on 05/04/18
     */
    private boolean storeEmpIDNumber() {
        boolean booleanData = false;
        String employeeId = MainConstants.kConstantEmpty;
        String spinnerValue = MainConstants.kConstantEmpty;
        String editTextEmployeeID = MainConstants.kConstantEmpty;
        if (editTextEmpIdNumber != null && editTextEmpIdNumber.getText()!=null ) {
            editTextEmployeeID = editTextEmpIdNumber.getText().toString().trim();
        }
        if (spinnerEmployeeExtension != null) {
            spinnerValue = spinnerEmployeeExtension.getSelectedItem().toString().trim();
        }
        if (editTextEmployeeID != null && !editTextEmployeeID.isEmpty()
                && (editTextEmployeeID.length() > MainConstants.kConstantZero)) {
//            Log.d("DSK ","editTextEmployeeID "+editTextEmployeeID);
            int haveCharacters = checkSpinnerValueSelected(editTextEmployeeID);
//            Log.d("DSK ","storeEmpIDNumber "+booleanData);
            if (haveCharacters == 1)
            {
//                Log.d("DSK ","storeEmpIDNumber spinnerValue "+spinnerValue+" editTextEmployeeID "+editTextEmployeeID);
                if (editTextEmployeeID != null && !editTextEmployeeID.isEmpty()
                        && spinnerValue != null && !spinnerValue.isEmpty() &&
                        !spinnerValue.equals(StringConstants.kConstantEmployeeExtensionAll)) {
                    employeeId = spinnerValue + editTextEmployeeID;
//                Log.d("DSK ","storeEmpIDNumber1 "+employeeId);
//                Log.d("DSK ","storeEmpIDNumber "+booleanData);
                } else {
                    employeeId = editTextEmployeeID;
//                    Log.d("DSK ","storeEmpIDNumber2 "+employeeId);
                }
                booleanData = saveEmployeeDetails(employeeId);
//                Log.d("DSK ","storeEmpIDNumber "+booleanData);
                if (!booleanData) {
                    textviewEmployeeEmpIDErrorTitle.setVisibility(View.VISIBLE);
                    textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEmployeeIDNotFound);
                }
            } else if (haveCharacters == 0) {
                booleanData = false;
                textviewEmployeeEmpIDErrorTitle.setVisibility(View.VISIBLE);
                textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEmployeeExtensionError);
            } else {
                booleanData = saveEmployeeDetails(editTextEmployeeID);
                textviewEmployeeEmpIDErrorTitle.setVisibility(View.VISIBLE);
                textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEmployeeIDNotFound);
            }
        } else {
            booleanData = false;
        }
        return booleanData;
    }

    /**
     * check selected Spinner Value
     *
     * @author Karthikeyan D S
     * @created on 25Sep2018
     */
    private int checkSpinnerValueSelected(String editTextEmployeeID) {
        int haveCharacters = NumberConstants.kConstantZero;
        String spinnerValue = MainConstants.kConstantEmpty;
        if (spinnerEmployeeExtension != null) {
            spinnerValue = spinnerEmployeeExtension.getSelectedItem().toString();
        }
        if (spinnerValue != null && !spinnerValue.isEmpty()
                && editTextEmployeeID != null && !editTextEmployeeID.isEmpty()) {
//            Log.d("DSK ", "spinnerValue is " + spinnerValue);
            if (checkEditTextHaveOnlyNumbers(editTextEmployeeID)
                    || spinnerValue.equalsIgnoreCase(StringConstants.kConstantEmployeeExtensionAll)) {
//                Log.d("DSK ","checkSpinnerValueSelected" +
//                        " in "+haveCharacters);
                haveCharacters = 1;
            }
//            Log.d("DSK ","checkSpinnerValueSelected in "+haveCharacters);
        }

//        Log.d("DSK ","checkSpinnerValueSelected( in "+haveCharacters);
        return haveCharacters;
    }

    /**
     * check Edit Text Have only Numbers
     *
     * @author Karthikeyan D S
     * @created on 25Sep2018
     */
    private boolean checkEditTextHaveOnlyNumbers(String editTextEmployeeID)
    {
        boolean booleanData = false;
        if (editTextEmployeeID != null && !editTextEmployeeID.isEmpty())
        {
//            Log.d("DSK ","checkEditTextHaveanyAlphaCharacters in "+editTextEmployeeID);
            booleanData = editTextEmployeeID.matches(MainConstants.kConstantValidEmployeeNoText);
//            Log.d("DSK ","checkEditTextHaveanyAlphaCharacters in "+booleanData);
        } else {
            booleanData = false;
        }
//        Log.d("DSK ","checkEditTextHaveanyAlphaCharacters in "+booleanData);
        return booleanData;
    }

    /**
     * check and Save Employee Details
     *
     * @author Karthikeyan D S
     * @created on 25Sep2018
     */
    private boolean saveEmployeeDetails(String employeeId) {
        boolean booleanData = false;
//        Log.d("DSK ","saveEmployeeDetails in "+employeeId);
        if (employeeId != null && !employeeId.isEmpty()
                && (employeeId.length() > MainConstants.kConstantZero)) {
//            Log.d("Message", "" + employeeId);
            if (database == null) {
                database = new DbHelper(this);
            }
            if (database != null) {
                ZohoEmployeeRecord employee = database.getEmployee(employeeId);
//                Log.d("DSK ","saveEmployeeDetails in "+employee.getEmployeeName());
                if (employee != null && employee.getEmployeeName() != null
                        && !employee.getEmployeeName().isEmpty())
                {
                    // set employee details into singleton variable
                    if (setEmployeeDetails(employee)) {
//                    Log.d("Message", "" + employeeId);
                        booleanData = true;
                    }
                }
            } else {
                booleanData = false;
            }
        }
        return booleanData;
    }

    /**
     * Save Employee Details to Singleton
     *
     * @param employee
     * @author Karthikeyan D S
     * @created on 05/04/18
     */
    public boolean setEmployeeDetails(ZohoEmployeeRecord employee) {
        boolean booleanData = false;
        if (validation == null) {
            validation = new Validation();
        }
//        Log.d("Message", "" + employee.getEmployeeId());
        if (employee != null) {
            SingletonEmployeeProfile.getInstance().setEmployeeName(
                    employee.getEmployeeName());
            SingletonEmployeeProfile.getInstance().setEmployeeId(
                    employee.getEmployeeId());
            SingletonEmployeeProfile.getInstance().setEmployeeZuid(
                    employee.getEmployeeZuid());
            SingletonEmployeeProfile.getInstance().setEmployeePhoto(employee.getEmployeePhoto());
            if (((SingletonVisitorProfile.getInstance().getLocationCode() != null))
                    // && (SingletonVisitorProfile.getInstance()
                    // .getLocationCode() != MainConstants.kConstantMinusOne)
                    && ((SingletonVisitorProfile.getInstance().getLocationCode() == MainConstants.kConstantMinusOne)
                    || (SingletonVisitorProfile.getInstance()
                    .getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
                    || (SingletonVisitorProfile.getInstance()
                    .getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) ||
                    (SingletonVisitorProfile.getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
                validation.SetMobileNumberWithLocation(employee
                        .getEmployeeMobileNumber().replace(
                                MainConstants.kConstantRemoveSpecialCharacter, MainConstants.kConstantEmpty));
            } else {
                SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
                        employee.getEmployeeMobileNumber());
            }
            SingletonEmployeeProfile.getInstance().setEmployeeSeatingLocation(
                    employee.getEmployeeSeatingLocation());
            SingletonEmployeeProfile.getInstance().setEmployeePhoto(
                    employee.getEmployeePhoto());
            SingletonEmployeeProfile.getInstance().setEmployeeZuid(
                    employee.getEmployeeZuid());
            SingletonEmployeeProfile.getInstance().setEmployeeExtentionNumber(
                    employee.getEmployeeExtentionNumber());
            SingletonEmployeeProfile.getInstance().setEmployeeMailId(
                    employee.getEmployeeEmailId());
            SingletonEmployeeProfile.getInstance().setEmployeeRecordId(
                    employee.getEmployeeRecordId());
            booleanData = true;
        }
        return booleanData;
    }

    /**
     * Check is existing user or not
     *
     * @author Karthick
     * @created 20170508
     */
    private void validateTempIDTyped() {
        String editTextEmployeeID = editTextEmpIdNumber.getText().toString().trim();
        if (editTextEmployeeID != null && !editTextEmployeeID.isEmpty()
                && editTextEmployeeID.length() > MainConstants.kConstantZero)
        {
            if (validateTempID()) {
            } else {
                textviewEmployeeEmpIDErrorTitle.setText(StringConstants.kConstantEnterEmployeeID);
            }
        }
        if (imageviewNext != null) {
            imageviewNext.setVisibility(View.GONE);
            image_view_next.setVisibility(View.GONE);
            if (validateTempID()) {
                imageviewNext.setVisibility(View.VISIBLE);
                image_view_next.setVisibility(View.GONE);

            } else {
                imageviewNext.setVisibility(View.GONE);
                image_view_next.setVisibility(View.GONE);
            }
        }
        editTextEmployeeID = MainConstants.kConstantEmpty;
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStart() {
        super.onStart();
        setOldValue();
    }

    /**
     * This function is called, when the activity is stopped.
     *
     * @author Karthikeyan D S
     * @created on 20180408
     */
    public void onStop() {
        super.onStop();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthikeyan D S
     * @created on 20180408
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthikeyan D S
     * @created on 20180408
     */
    public void onBackPressed() {
        launchPreviousPage();
    }

    /**
     * Set singleton name value to edittext
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void setOldValue() {
        if (editTextEmpIdNumber != null
                && SingletonEmployeeProfile.getInstance() != null
                && SingletonEmployeeProfile.getInstance().getEmployeeId() != null
                && !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()) {
            editTextEmpIdNumber
                    .removeTextChangedListener(textWatcherTempIDChanged);
            editTextEmpIdNumber.setText(MainConstants.kConstantEmpty + SingletonEmployeeProfile.getInstance().getEmployeeId());
            editTextEmpIdNumber.setSelection((MainConstants.kConstantEmpty + SingletonEmployeeProfile.getInstance().getEmployeeId()).length());
            editTextEmpIdNumber.addTextChangedListener(textWatcherTempIDChanged);
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.VISIBLE);
                if (image_view_next != null) {
                    image_view_next.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.INVISIBLE);
                if (image_view_next != null) {
                    image_view_next.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void deallocateObjects() {
        editTextEmpIdNumber = null;
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        imageViewIndicator = null;
        textviewEmployeeEmpIDTitle = null;
        textviewEmployeeEmpIDErrorTitle = null;
        image_view_next = null;
        imageviewPrevious = null;
        typeFaceTitle = null;
        spinnerEmployeeExtension = null;
        typefaceVisitorTempID = null;
        if (relativeLayoutExisitingVisitor != null) {
            relativeLayoutExisitingVisitor
                    .setBackgroundResource(MainConstants.kConstantZero);
        }
        relativeLayoutExisitingVisitor = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        applicationMode = NumberConstants.kConstantZero;
        sharedPreferenceController = null;
        validation = null;
    }

}
