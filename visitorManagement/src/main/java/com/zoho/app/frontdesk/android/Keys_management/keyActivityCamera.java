package com.zoho.app.frontdesk.android.Keys_management;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

import com.zoho.app.frontdesk.android.Keys_management.keyUtility.keyCameraTwoApiFragment;
import com.zoho.app.frontdesk.android.R;

import utility.CameraTwoApiFragment;

public class keyActivityCamera extends Activity{

    int curBrightnessValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set the orientation as Portrait
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
        {
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        }
        setContentView(R.layout.activity_camera);
        if (  null== savedInstanceState ) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, keyCameraTwoApiFragment.newInstance())
                    .commit();
        }

//        Log.d("DSK","ActivityCameraTwoApi");
    }

//    /**
//     * Set screen brightness mode manual 255
//     *
//     * @author Karthick
//     * @created on 20170503
//     */
//    protected void onResume() {
//        super.onResume();
//        try {
//            int br = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
//            WindowManager.LayoutParams lp = getWindow().getAttributes();
//            lp.screenBrightness = (float) br / 255;
//            getWindow().setAttributes(lp);
//
//        } catch (Exception e) {
//
//        }
//    }

    /**
     * Set screen brightness mode automatic
     *
     * @author Karthick
     * @created on 20170503
     */
    protected void onPause() {
        super.onPause();
//        try {
//        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);  //this will set the manual mode (set the automatic mode off)
//    } catch (Exception e) {
//
//    }
//        android.provider.Settings.System.putInt(getContentResolver(),
//                android.provider.Settings.System.SCREEN_BRIGHTNESS, curBrightnessValue);
    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case 1: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }
}
