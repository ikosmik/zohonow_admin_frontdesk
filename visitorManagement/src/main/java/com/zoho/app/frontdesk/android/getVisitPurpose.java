package com.zoho.app.frontdesk.android;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import java.util.ArrayList;

import constants.MainConstants;
import database.dbhelper.DbHelper;
import database.dbschema.PurposeOfVisitRecord;
import zohocreator.VisitorZohoCreator;

/**
 * Created by jayapriya on 02/04/15.
 */
public class getVisitPurpose extends  Service{


        private static final String KEY = null;
        VisitorZohoCreator creator = new VisitorZohoCreator(this);
        DbHelper database = new DbHelper(this);
        String contact;

        @Override
        public IBinder onBind(Intent intent) {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        @Override
        public void onCreate() {
            //Toast.makeText(this, "Service was Created", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onStart(Intent intent, int startId) {
            // Perform your long running operations here.
            // Execute the background process
            UploadCreatorRecord upload = new UploadCreatorRecord();
            upload.execute();
            //Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onDestroy() {
            //super.onDestroy();
            // Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        }

        /*
         * Created by Jayapriya On 30/09/2014 Call AsyncTask to upload details into
         * the creator.
         */
        // Async Task Class
        class UploadCreatorRecord extends AsyncTask<String, String, String> {

            // Search records in creator and upload new records
            @Override
            protected String doInBackground(String... f_url) {

                try {
                    ArrayList<PurposeOfVisitRecord> purposeOfVisitList = new ArrayList<PurposeOfVisitRecord>();
                    purposeOfVisitList = creator
                            .getPurposeOfVisitorRecord();
                    if ((purposeOfVisitList != null)
                            && (purposeOfVisitList.size() > MainConstants.kConstantZero)) {
                        database.deletePurposeOfVisit();
                        database.insertPurposeOfVisit(purposeOfVisitList);
                    }

                } catch (Exception e) {
                    // Log.e("Error: ", e.getMessage());
                }
                return null;
            }

        }


	}

