package com.zoho.app.frontdesk.android.Keys_management.roomDataBase;

import android.arch.persistence.room.Room;
import android.content.Context;

import java.util.List;

public class DataBaseRepository {

    //here setting the database name
    final String DataBaseName = "keys";
    public AppDatabase appDatabase;
    public keyEntity entity = new keyEntity();
//creating constructor for this class
    public DataBaseRepository(Context context)
    {
        //here creating the database
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, DataBaseName).allowMainThreadQueries().build();
    }
//here is the function to store given details as a record in room database
    public void InsertKeys(String key_id,String key_title,String description,Integer key_image,
                           String spare,String key_type,String office_location,String location_inside_office,
                           String security_details,String vehicle_details,String room_details,String furniture_type,
                           String spare_key_count,String status,String is_active)
    {
        //here setting the entity values from parameters
        entity.setKey_id(key_id);
        entity.setKey_title(key_title);
        entity.setDescription(description);
        entity.setKey_image(key_image);
        entity.setSpare(spare);
        entity.setKey_type(key_type);
        entity.setOffice_location(office_location);
        entity.setLocation_inside_office(location_inside_office);
        entity.setSecurity_details(security_details);
        entity.setVehicle_details(vehicle_details);
        entity.setRoom_details(room_details);
        entity.setFurniture_type(furniture_type);
        entity.setSpare_key_count(spare_key_count);
        entity.setStatus(status);
        entity.setIs_active(is_active);
        //here adding the entity as a record in room database
        appDatabase.appDatabaseObject().addKeys(entity);
    }

    //here fetching all details from
    public List<keyEntity> fetchAllKeys()
    {
        if(appDatabase.appDatabaseObject().getRecordCount() > 0)
        {
            return appDatabase.appDatabaseObject().getAllKeys();
        }
        return null;
    }
//here fetching a single key record from given key_id
    public List<keyEntity> fetchKey(String key_id)
    {
        if(appDatabase.appDatabaseObject().getRecordCount() > 0)
        {
            return appDatabase.appDatabaseObject().getKeys(key_id);
        }
        return null;
    }
    // here removing a key from database with given key_id
    public void removeKey(String key_id){
        appDatabase.appDatabaseObject().deleteKey(key_id);
    }

    // here removing all keys from database
    public void removeAllKeys(){
        appDatabase.appDatabaseObject().deleteAllKeys();
    }

    // here removing a key from database with given entity
    public void removeEntity(keyEntity entity)
    {
        appDatabase.appDatabaseObject().deleteKeys(entity);
    }
//here updating a whole entity in table
    public void UpdateKeys(String key_id,String key_title,String description,Integer key_image,
                           String spare,String key_type,String office_location,String location_inside_office,
                           String security_details,String vehicle_details,String room_details,String furniture_type,
                           String spare_key_count,String status,String is_active)
    {
        //here setting the entity values from parameters
        entity.setKey_id(key_id);
        entity.setKey_title(key_title);
        entity.setDescription(description);
        entity.setKey_image(key_image);
        entity.setSpare(spare);
        entity.setKey_type(key_type);
        entity.setOffice_location(office_location);
        entity.setLocation_inside_office(location_inside_office);
        entity.setSecurity_details(security_details);
        entity.setVehicle_details(vehicle_details);
        entity.setRoom_details(room_details);
        entity.setFurniture_type(furniture_type);
        entity.setSpare_key_count(spare_key_count);
        entity.setStatus(status);
        entity.setIs_active(is_active);
        //here adding the entity as a record in room database
        appDatabase.appDatabaseObject().updateKeys(entity);
    }
}
