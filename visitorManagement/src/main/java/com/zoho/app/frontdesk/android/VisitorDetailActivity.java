package com.zoho.app.frontdesk.android;

import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import appschema.VisitorDetailsDatabase;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import utility.CompressImage;
import utility.DialogView;
import utility.CustomArrayAdapter;
import utility.NetworkConnection;
import appschema.SharedPreferenceController;
import utility.TimeZoneConventer;
import utility.UsedBitmap;
import utility.UsedCustomToast;
import utility.Validation;
import zohocreator.VisitorZohoCreator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.LruCache;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;
import appschema.ExistingVisitorDetails;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.zoho.app.frontdesk.android.R.id;
import com.zoho.app.frontdesk.android.R.layout;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;

/**
 * This activity display the visitor detail, Take the visitor photo and display
 * the location.
 * 
 * @author jayapriya
 * @created 22/09/2014
 * 
 */
public class VisitorDetailActivity extends Activity {

	private Animation animationZoom;
	private String employeePhoto;
	private CustomAutoCompleteView textGetEmployeeName;
	private SingleTouchView singleTouchViewGetVisitorSign;
	private LinearLayout linearLayoutVisitorName,linearLayoutVisitorContact,linearlayoutVisitorEmployeeName,linearlayoutTempID;
	private RelativeLayout relativeLayoutHaveDevice,relativeLayoutCheckIn,relativeLayoutShootMe, relativeLayoutVisitorDetail,
			relativeLayoutGetVisitorSign, relativeLayoutVisitorSign;
	private EditText editTextVisitorName,editTextCompany, editTextVisitorContact,
			editTextTempIdNumber, editTextSerialNumber, editTextMake;
	private RadioButton radioButtonVisitorPurposeSelected,
			radioButtonVisitorPurposeInterview,
			radioButtonVisitorPurposeOfficial,
			radioButtonVisitorPurposePersonal;
	private RadioGroup radioGroupVisitorPurpose;
	private LinearLayout linearLayoutPurpose;
	private LinearLayout linearlayoutDevices;
	private boolean isPurposeSelected=false, isHaveDevice=true, booleanExtDeviceOne=false,booleanExtDeviceTwo=false,booleanExtDeviceThree=false,
	booleanExtDeviceFive=false,booleanExtDeviceFour=false;
	private ImageView imageviewExtDeviceOne,imageviewExtDeviceTwo,imageviewExtDeviceThree,
	imageviewExtDeviceFive,imageviewExtDeviceFour,imageviewNext;
	private TextView textviewHaveExtDeviceAccept,textviewHaveDeviceTitle,textviewExtDeviceOne,textviewExtDeviceTwo,textviewExtDeviceThree,
	textviewExtDeviceFour,textviewExtDeviceFive,textviewExtDeviceSix,textviewExtDeviceAccept;
	private TextView textviewPurposeTitle, textviewVisitorNameTitle,textviewContactTitle,textviewEmployeeTitle,
	textviewTempIdTitle,textviewExtDeviceTitle;
	private ImageView imageViewHomePage,imageViewBack, imageViewErrorInVisitorSign,
			imageViewVisitorSignClear, imageGetLaptopDetails,
			imageViewPrevious,imageViewScanBarCode,imageviewTexture;
	private TextView textViewDialogHomeMessage, textViewErrorInName,
			textViewErrorInContact, textViewHomeMessageConfirm,
			textViewHomeMessageCancel, textViewMandory,
			textViewVisitorPageHeading, textEmployeeErrorMsg,
			textViewVisitorSignature, textViewGetVisitorPurposeConfirm,
			textViewGetVisitorPurposeTitle, textViewDisplayFieldName;
	// textViewShootMe,
	private Spinner spinnerPurpose;
	private Button buttonHaveDeviceYes,buttonHaveDeviceNo,buttonVisitorDetailConfirm,buttonSkip;
	private Typeface typeFaceTitle, typefaceHeading, dialogMessageFont,
			dialogButtonFont, fontEditText, employeeVisitFreqHigh,
			employeeVisitFreqMedium, employeeVisitFreqLow,
			typefacedialogChooseVisitorPurposeTitle,
			typefacedialogChooseVisitorPurposeRadioButtons,
			typefacedialogChooseVisitorPurposeConfirm;
	private byte[] imagepath;
	private ArrayList<String> purposeList = new ArrayList<String>();
	private CustomArrayAdapter adapterPurpose;
	private Dialog dialogGetHomeMessage, dialogGetLaptopDetails,
			dialogChooseVisitorPurpose;
	private VisitorZohoCreator creator = new VisitorZohoCreator(this);
	private String checkMailOrMobileNo;
	private int validateCodeForPreviousAndNextButton;
	private ArrayList<ZohoEmployeeRecord> employeeList;
	private AutocompleteCustomArrayAdapter myAdapter;
	private ZohoEmployeeRecord employee;
	private DbHelper database;
	private Handler handlerPurpose, handlerHideDialog, noActionHandler,signValidateHandler,handlerShowPurposeDialog,handlerAutoCheckIn;
	private backgroundProcess backgroundProcess;
	private Validation validation =new Validation();
	private LruCache<String, Bitmap> mMemoryCache;
	private int retryCount=0;
	private MediaPlayer mPlayer;
	private LayoutParams params;
	private int dialogConfirmCode;
	private TextToSpeech textToSpeech;
	private SharedPreferenceController sharedPreferenceController;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove the title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// hide the status bar.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_visitor_visit_details);
		assignObjects();
		// setLocation();
		backgroundProcess = new backgroundProcess(
				MainConstants.jsonCreateUploadPhoto);
		//TODO remove command
		if (backgroundProcess != null) {
			backgroundProcess.execute();
		}
		if(ApplicationController.getInstance()!=null && (editTextVisitorName!=null && editTextVisitorName.getText()!=null && editTextVisitorName.getText().toString().length()<=0)) {
            ApplicationController.getInstance().textSpeach(validateCodeForPreviousAndNextButton);
        }	
	}

	/**
	 * When the activity started then execute this function.
	 * 
	 * @author jayapriya
	 * @created 27/09/2014
	 */
	public void onStart() {
		super.onStart();
		if (SingletonVisitorProfile.getInstance() != null) {
			// get the singleton values and set it to the separate edit text.
			getSingletonValuesToSetEditText();
		}
		if (imageviewNext != null && imageviewNext.isShown()) {
			imageviewNext.setVisibility(View.GONE);
		}
	}

	/**
	 * OnWhen the activity stopped then execute this function. (non-Javadoc)
	 * 
	 * @author jayapriya
	 * @created 27/09/2014
	 * @see android.app.Activity#onStop()
	 */
	public void onStop() {
		super.onStop();
	}

	/*
	 * Deallocate the objects fuction call and set dialog to null.
	 * 
	 * @author karthick
	 * @created 22/04/2014
	 */
	protected void onDestroy() {
		super.onDestroy();
		mMemoryCache.evictAll();
		DialogView.setNullDialog();
		deallocateObjects();
	}
	
	/**
	 * Set list of purpose to spinner
	 * 
	 * @author Karthick
	 * @created 20170420
	 */
	private void setPurposeList() {

		if(purposeList!=null) {
			purposeList.clear();
		
		if (employee!=null && employee.getEmployeeDept() != null
				&& (employee.getEmployeeDept().equals(
						MainConstants.HRDepartment)
				|| employee.getEmployeeDept().equals(
						MainConstants.HRDepartmentAnotherNamePlural)
				|| employee.getEmployeeDept().equals(
						MainConstants.HRDepartmentAnotherNameSingular))) {
			purposeList.add(MainConstants.purposeInterview);
		}
		purposeList.add(MainConstants.purposeOfficial);
		purposeList.add(MainConstants.purposePersonal);
		purposeList.add(MainConstants.purposeGadgetSpecialist);
		purposeList.add(MainConstants.purposeBankingExecutive);
		purposeList.add(MainConstants.purposeAuditor);
		purposeList.add(MainConstants.purposeHealthCareExpert);
		purposeList.add(MainConstants.purposeDoctor);
		purposeList.add(MainConstants.purposePress);
		purposeList.add(MainConstants.purposePartner);
		purposeList.add(MainConstants.purposeGovernment);
		}
		if(adapterPurpose==null) {
			
		adapterPurpose = new CustomArrayAdapter(this,
				android.R.layout.simple_spinner_item, purposeList);
		adapterPurpose
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerPurpose.setAdapter(adapterPurpose);	
		} else {
			
			if(SingletonVisitorProfile.getInstance()!=null 
					&& SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()!=null
					&& !SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty()
					&& spinnerPurpose!=null  && purposeList!=null && purposeList.size()>0
					&& purposeList.indexOf(SingletonVisitorProfile.getInstance().getPurposeOfVisitCode())>=0) {
				spinnerPurpose.setSelection(purposeList.indexOf(SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()), false);	
			} else if(SingletonVisitorProfile.getInstance()!=null 
					&& SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()!=null
					&& SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty()
					&& spinnerPurpose!=null  && purposeList!=null && purposeList.size()>0) {
				spinnerPurpose.setSelection(0, false);
			}
			isPurposeSelected=true;
			adapterPurpose.notifyDataSetChanged();
			
		}
	}
	
	/*
	 * Assign the values to the objects.
	 * 
	 * @author jayapriya
	 * @created 22/09/2014
	 */
	private void assignObjects() {

		validateCodeForPreviousAndNextButton = MainConstants.kConstantZero;
		params = new LayoutParams(
		        LayoutParams.MATCH_PARENT,      
		        LayoutParams.WRAP_CONTENT
		);
		if(TextToSpeechListener!=null) {
		textToSpeech = new TextToSpeech(getApplicationContext(), TextToSpeechListener);
		}
		database = new DbHelper(this);
		handlerHideDialog = new Handler();
		handlerPurpose = new Handler();
		noActionHandler = new Handler();
		handlerShowPurposeDialog = new Handler();
		handlerAutoCheckIn = new Handler();
		signValidateHandler = new Handler();
		noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);
		buttonVisitorDetailConfirm = (Button) findViewById(R.id.button_visit_detail_ok);
		buttonSkip = (Button) findViewById(R.id.button_skip);
		buttonHaveDeviceYes = (Button) findViewById(R.id.button_have_device_yes);
		buttonHaveDeviceNo = (Button) findViewById(R.id.button_have_device_no);
		editTextVisitorName = (EditText) findViewById(R.id.editTextVisitorName);
		editTextCompany = (EditText) findViewById(R.id.edittext_visitor_company);
		editTextVisitorContact = (EditText) findViewById(R.id.editTextVisitorContact);
		imageViewBack = (ImageView) findViewById(R.id.imageview_visitor_back);
		imageViewHomePage = (ImageView) findViewById(R.id.imageViewVisitorPageHome);
		imageGetLaptopDetails = (ImageView) findViewById(R.id.addLaptopDetails);
		imageViewPrevious = (ImageView) findViewById(R.id.imageview_previous);
		imageviewTexture = (ImageView)findViewById(R.id.imageview_texture);
		imageviewNext = (ImageView) findViewById(R.id.imageview_next);
			
		relativeLayoutHaveDevice = (RelativeLayout) findViewById(R.id.relative_layout_have_device);
		relativeLayoutCheckIn = (RelativeLayout) findViewById(R.id.relativeLayout_visitor_register);
		relativeLayoutShootMe = (RelativeLayout) findViewById(R.id.relativeLayoutShootMe);
		textviewHaveDeviceTitle = (TextView)findViewById(R.id.textview_have_device_title);
		textviewHaveExtDeviceAccept = (TextView) findViewById(R.id.textview_have_external_accept);
		
		textviewPurposeTitle = (TextView)findViewById(R.id.textview_purpose_title);
		textviewVisitorNameTitle = (TextView)findViewById(R.id.textview_visitor_name_title);
		textviewContactTitle = (TextView)findViewById(R.id.textview_visitor_contact_title);
		textviewEmployeeTitle = (TextView)findViewById(R.id.textview_visitor_employee_title);
		textviewTempIdTitle = (TextView)findViewById(R.id.textview_temp_id_title);
		textviewExtDeviceTitle = (TextView)findViewById(R.id.textview_device_title);
		
		textViewDisplayFieldName = (TextView)findViewById(R.id.text_View_Name_Of_Edit_Text);
		textViewErrorInContact = (TextView) findViewById(R.id.textViewErrorInContact);
		textViewErrorInName = (TextView) findViewById(R.id.textViewErrorInName);
		textViewMandory = (TextView) findViewById(R.id.textViewMandatory);
		textViewVisitorPageHeading = (TextView) findViewById(R.id.textViewVisitorPageTitle);
		relativeLayoutVisitorDetail = (RelativeLayout) findViewById(R.id.relativeLayoutVisitorDetail);
		linearLayoutVisitorContact = (LinearLayout) findViewById(R.id.linearlayout_visitor_contact);
		linearlayoutVisitorEmployeeName = (LinearLayout) findViewById(R.id.linearlayout_visitor_employee_name);
		linearlayoutTempID = (LinearLayout) findViewById(R.id.linearlayout_temp_id);
		singleTouchViewGetVisitorSign = (SingleTouchView) findViewById(id.singletouchview_sign);
		relativeLayoutGetVisitorSign = (RelativeLayout) findViewById(id.relativeLayoutGetVisitorSign);
		relativeLayoutVisitorSign = (RelativeLayout) findViewById(R.id.relativeLayout_visitor_signature);
		linearLayoutVisitorName = (LinearLayout) findViewById(R.id.relativeLayout_visitor_name);
		imageViewErrorInVisitorSign = (ImageView) findViewById(id.image_view_error_in_visitor_sign);
		imageViewVisitorSignClear = (ImageView) findViewById(id.imageViewSignClear);

		imageviewExtDeviceOne = (ImageView) findViewById(id.imageview_external_device_one);
		imageviewExtDeviceTwo = (ImageView) findViewById(id.imageview_external_device_two);
		imageviewExtDeviceThree = (ImageView) findViewById(id.imageview_external_device_three);
		imageviewExtDeviceFour = (ImageView) findViewById(id.imageview_external_device_four);
		imageviewExtDeviceFive = (ImageView) findViewById(id.imageview_external_device_five);
//		imageviewExtDeviceSix = (ImageView) findViewById(id.imageview_external_device_six);
		
		textviewExtDeviceOne = (TextView) findViewById(id.textview_external_device_one);
		textviewExtDeviceTwo = (TextView) findViewById(id.textview_external_device_two);
		textviewExtDeviceThree = (TextView) findViewById(id.textview_external_device_three);
		textviewExtDeviceFour = (TextView) findViewById(id.textview_external_device_four);
		textviewExtDeviceFive = (TextView) findViewById(id.textview_external_device_five);
		textviewExtDeviceSix = (TextView) findViewById(id.textview_laptop);
		textviewExtDeviceAccept = (TextView) findViewById(id.textview_external_accept);
		
		linearlayoutDevices = (LinearLayout) findViewById(R.id.linearlayout_devices);
		linearLayoutPurpose = (LinearLayout) findViewById(R.id.relativeLayout_visitor_purpose);
		
		spinnerPurpose = (Spinner) findViewById(R.id.spinner_purpose);
		
		editTextSerialNumber = (EditText) findViewById(R.id.edit_Text_serial_number);
		editTextMake = (EditText) findViewById(R.id.edit_Text_make);
		imageViewScanBarCode = (ImageView)findViewById(R.id.imageview_scan_icon);

		textGetEmployeeName = (CustomAutoCompleteView) findViewById(id.textGetEmployeeName);
		textEmployeeErrorMsg = (TextView) findViewById(id.textViewEmployeeDetailErrorMsg);
		textViewVisitorSignature = (TextView) findViewById(id.textView_visitor_signature);
		editTextTempIdNumber = (EditText) findViewById(id.editText_temp_id);

		//visitorBadge = (VisitorBadge) findViewById(R.id.visitorbadge);
		// Hidden the keyboard variation
		editTextVisitorName
				.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		editTextCompany.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		textGetEmployeeName
				.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
						| InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		editTextTempIdNumber.setInputType(InputType.TYPE_CLASS_NUMBER);

		// set font
		employeeVisitFreqHigh = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		employeeVisitFreqMedium = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		employeeVisitFreqLow = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		typefaceHeading = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		textViewVisitorPageHeading.setTypeface(typefaceHeading);
		typeFaceTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskRegular);
		typefacedialogChooseVisitorPurposeTitle = Typeface.createFromAsset(
				getAssets(), FontConstants.fontConstantHKGroteskSemiBold);
		typefacedialogChooseVisitorPurposeRadioButtons = Typeface
				.createFromAsset(getAssets(),
						FontConstants.fontConstantHKGroteskSemiBold);
		typefacedialogChooseVisitorPurposeConfirm = Typeface.createFromAsset(
				getAssets(), FontConstants.fontConstantHKGroteskSemiBold);
		fontEditText = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		
		dialogMessageFont = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskSemiBold);
		dialogButtonFont = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskSemiBold);
		
		editTextVisitorName.setTypeface(fontEditText);
		editTextCompany.setTypeface(fontEditText);
		editTextVisitorContact.setTypeface(fontEditText);
		textGetEmployeeName.setTypeface(fontEditText);
		            
		editTextTempIdNumber.setTypeface(fontEditText);
		
		textViewVisitorSignature.setTypeface(typefaceHeading);
		textviewPurposeTitle.setTypeface(typefaceHeading);
		textviewVisitorNameTitle.setTypeface(typefaceHeading);
		textviewContactTitle.setTypeface(typefaceHeading);
		textviewEmployeeTitle.setTypeface(typefaceHeading);
		textviewTempIdTitle.setTypeface(typefaceHeading);
		textviewExtDeviceTitle.setTypeface(typefaceHeading);
		if(typefacedialogChooseVisitorPurposeConfirm!=null) {
		textviewHaveDeviceTitle.setTypeface(typefacedialogChooseVisitorPurposeConfirm);
		}
		
		Typeface typefaceErrorMsg = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskItalic);
		textViewErrorInContact.setTypeface(typefaceErrorMsg);
		textViewErrorInName.setTypeface(typefaceErrorMsg);
		textViewMandory.setTypeface(typefaceErrorMsg);
		textEmployeeErrorMsg.setTypeface(typefaceErrorMsg);

		buttonVisitorDetailConfirm.setTypeface(fontEditText);
		buttonHaveDeviceNo.setTypeface(fontEditText);
		buttonHaveDeviceYes.setTypeface(fontEditText);
		buttonSkip.setTypeface(typefacedialogChooseVisitorPurposeTitle);
		
		textviewExtDeviceOne.setTypeface(typefaceHeading);
		textviewExtDeviceTwo.setTypeface(typefaceHeading);
		textviewExtDeviceThree.setTypeface(typefaceHeading);
		textviewExtDeviceFour.setTypeface(typefaceHeading);
		textviewExtDeviceFive.setTypeface(typefaceHeading);
		textviewExtDeviceSix.setTypeface(typefaceHeading);

		editTextMake.setTypeface(fontEditText);
		editTextSerialNumber.setTypeface(fontEditText);
		
		textviewExtDeviceAccept.setTypeface(typefaceHeading);
		textviewHaveExtDeviceAccept.setTypeface(typefaceHeading);
		
		//set app Background
		setAppBackground();
		
		// Load Employee Details
		ZohoEmployeeRecord[] employeeArray = new ZohoEmployeeRecord[0];
		// set the custom ArrayAdapter
		myAdapter = new AutocompleteCustomArrayAdapter(this,
				layout.custom_spinner, employeeArray);
		textGetEmployeeName.setAdapter(myAdapter);

		// TextChangedListener
		// add the listener so it will tries to suggest while the user types
		textGetEmployeeName
				.addTextChangedListener(textWatcherEmployeeNameChanged);
		editTextTempIdNumber.addTextChangedListener(textWatcherTempIDChanged);
		
		editTextSerialNumber.addTextChangedListener(textWatcherLaptopSerialNo);
		editTextMake.addTextChangedListener(textWatcherLaptopMake);
		
		spinnerPurpose.setOnItemSelectedListener(listenerPurposeSelection);

		// Onclick listeners
		// textViewShootMe.setOnClickListener(listenerOpenCamera);
		imageViewBack.setOnClickListener(listenerButtonPrevious);
		imageViewHomePage.setOnClickListener(listenerGoToHomePage);
		buttonHaveDeviceNo.setOnClickListener(listenerHaveDeviceNo);
		buttonHaveDeviceYes.setOnClickListener(listenerHaveDeviceYes);
		
		//imageGetLaptopDetails.setOnClickListener(listenerGetLaptopDetails);
		// imageViewDeleteVisitorImage.setOnClickListener(listenerDeleteVisitorImage);
		buttonVisitorDetailConfirm
				.setOnClickListener(listenerVisitorDetailConfirm);
		imageviewNext.setOnClickListener(listenerimageviewNext);
		buttonSkip.setOnClickListener(listenerButtonSkip);
//		 buttonPrevious.setOnClickListener(listenerButtonPrevious);
		imageViewPrevious.setOnClickListener(listenerButtonPrevious);
		editTextVisitorName.addTextChangedListener(textWatcherNameChanged);
		editTextVisitorContact
				.addTextChangedListener(textWatcherContactChanged);
		editTextVisitorContact.setTextColor(Color
				.parseColor(MainConstants.color_code_efefef));
		// editTextVisitorContact
		// .setOnFocusChangeListener(listenerSetFocusOnContact);
		textGetEmployeeName.setOnItemClickListener(listenerChooseEmployee);
		textGetEmployeeName
				.setOnFocusChangeListener(listenerSetFocusOnEmployee);
		editTextTempIdNumber.setOnEditorActionListener(listenerNextPressed);
		editTextVisitorName.setOnEditorActionListener(listenerNextPressed);
		editTextVisitorContact.setOnEditorActionListener(listenerNextPressed);
		textGetEmployeeName.setOnEditorActionListener(listenerNextPressed);
		singleTouchViewGetVisitorSign
				.setOnTouchListener(errorInVisitorSignListner);
		imageViewVisitorSignClear.setOnClickListener(listenerVisitorSignClear);
		imageViewScanBarCode.setOnClickListener(openBarcodeScanListener);
		
		imageviewExtDeviceOne.setOnClickListener(listenerExtDeviceOne);
		imageviewExtDeviceTwo.setOnClickListener(listenerExtDeviceTwo);
		imageviewExtDeviceThree.setOnClickListener(listenerExtDeviceThree);
		imageviewExtDeviceFour.setOnClickListener(listenerExtDeviceFour);
		imageviewExtDeviceFive.setOnClickListener(listenerExtDeviceFive);
		//imageviewExtDeviceSix.setOnClickListener(listenerExtDeviceOne);
		
		editTextTempIdNumber.setOnTouchListener(tempIDNumberTouchListner);
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getByteCount() / 1024;
			}
		};	}

	/*
	 * TouchListner for focus and open keyboard
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	private OnTouchListener tempIDNumberTouchListner = new OnTouchListener() {
		public boolean onTouch(View v, MotionEvent event) {
			editTextTempIdNumber.requestFocus();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(editTextTempIdNumber,
					InputMethodManager.SHOW_IMPLICIT);
			return false;
		}
	};

	/*
	 * textWatcher to validate the tempID
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	public TextWatcher textWatcherTempIDChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editTextTempIdNumber != null) {
				if (imageviewNext != null) {
					imageviewNext.setVisibility(View.GONE);
					if (editTextTempIdNumber.getText().toString().trim()
							.length() >= MainConstants.kConstantOne) {
						imageviewNext.setVisibility(View.VISIBLE);
					}
				}
				setTempIdFontSize();
				SingletonVisitorProfile.getInstance().setTempIDNumber(0);
				noActionHandler.removeCallbacks(noActionThread);
				noActionHandler.postDelayed(noActionThread,
						MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			}
		}

		public void afterTextChanged(Editable s) {

		}
	};
	
	
	/*
	 * textWatcher to validate the Laptop SerialNo
	 * 
	 * @author Karthick
	 * 
	 * @created on 20161230
	 */
	public TextWatcher textWatcherLaptopSerialNo = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			setAutoCheckinDeviceHandler();
		}

		public void afterTextChanged(Editable s) {
			setBorder();
		}
	};
	
	/*
	 * textWatcher to validate the Laptop SerialNo
	 * 
	 * @author Karthick
	 * 
	 * @created on 20161230
	 */
	public TextWatcher textWatcherLaptopMake = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			setAutoCheckinDeviceHandler();
		}

		public void afterTextChanged(Editable s) {
			setBorder();
		}
	};

	/**
	 * Set incharge email depends selected location
	 * 
	 * @author Karthick
	 * 
	 * @created 20160526
	 */
	private OnItemSelectedListener listenerPurposeSelection = new OnItemSelectedListener() {
		public void onItemSelected(AdapterView<?> parentView,
				View selectedItemView, int position, long id) {
			if(isPurposeSelected==true) {
//				try {
//					TextView selectedText = (TextView) parentView.getChildAt(0);
//					   if (selectedText != null) {
//					      selectedText.setTextColor(Color.RED);
//					   }
////				((TextView) selectedItemView).setTextColor(Color.WHITE);
////				((TextView) selectedItemView).setBackgroundColor(Color.BLACK);
//				} catch (Exception e) {
//					
//				}
			       
//			validateCodeForPreviousAndNextButton=validateCodeForPreviousAndNextButton+1;
//			showHideComponents(validateCodeForPreviousAndNextButton);
				if(SingletonVisitorProfile.getInstance()!=null && spinnerPurpose!=null && spinnerPurpose.getSelectedItem()!=null && !spinnerPurpose.getSelectedItem().toString().isEmpty()) {
					SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(spinnerPurpose.getSelectedItem().toString().toUpperCase());	
				}
				handlerPurpose.postDelayed(purposeSeletionTask, 1000);
				
			}
			isPurposeSelected=true;
		}
		public void onNothingSelected(AdapterView<?> parentView) {
		}
	};
	
	/**
	 * textWatcher to validate the Laptop make
	 * 
	 * @author Karthick
	 * 
	 * @created on 20161230
	 */
	private void setBorder() {
		if(editTextSerialNumber!=null && !editTextSerialNumber.getText().toString().trim().isEmpty()
				&& editTextMake!=null && !editTextMake.getText().toString().trim().isEmpty()) {
			imageGetLaptopDetails.setBackgroundResource(R.drawable.icon_laptop_has_data);
		} else {
			imageGetLaptopDetails.setBackgroundResource(R.drawable.icon_laptop_no_data);
		}

		noActionHandler.removeCallbacks(noActionThread);
		noActionHandler.postDelayed(noActionThread,
				MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
	}
	
	/*
	 * set font size to tempID edittext
	 * 
	 * @author karthick
	 * 
	 * @created apr-2014
	 */
	private void setTempIdFontSize() {

		if (editTextTempIdNumber != null
				&& editTextTempIdNumber.getText().toString().trim().length() > MainConstants.kConstantZero) {
			editTextTempIdNumber.setTextSize(46);
		} else if (editTextTempIdNumber != null) {
			editTextTempIdNumber.setTextSize(25);
		}
	}

	private OnTouchListener errorInVisitorSignListner = new OnTouchListener() {

		public boolean onTouch(View v, MotionEvent event) {
			imageViewErrorInVisitorSign.setVisibility(View.INVISIBLE);
			textViewVisitorSignature.setVisibility(View.INVISIBLE);
			if(signValidateHandler!=null) {
			signValidateHandler.removeCallbacks(validateSignThread);
			signValidateHandler.postDelayed(validateSignThread, MainConstants.signValidateTime);
			}
			return false;
		}

	};
	/*
	 * Clear the VisitorSign path.
	 * 
	 * @author jayapriya
	 * 
	 * @created 28/09/2014
	 */
	private OnClickListener listenerVisitorSignClear = new OnClickListener() {

		public void onClick(View v) {
			removeView(relativeLayoutGetVisitorSign,
					singleTouchViewGetVisitorSign);
			textViewVisitorSignature.setVisibility(View.VISIBLE);
			signValidateHandler.removeCallbacks(validateSignThread);
			imageviewNext.setVisibility(View.GONE);
		}
	};

	/*
	 * Created by jayapriya On 30/09/2014 Created by jayapriya This fuction
	 * clear the path and create the new path.
	 */
	private void removeView(RelativeLayout layout, SingleTouchView view) {
		view.signAgain();

	}

	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170405
	 */
	private OnInitListener TextToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if(textToSpeech!=null && status != TextToSpeech.ERROR) {
            		textToSpeech.setLanguage(Locale.ENGLISH);
            }
            
        }
    };
	
	/*
	 * when done pressed then process started
	 * 
	 * @author jayapriya
	 * 
	 * @created02/12/2014
	 */
	private OnEditorActionListener listenerNextPressed = new EditText.OnEditorActionListener() {

		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {
				nextPressed();
				return true;
			}
			return false;
		}

	};

	/*
	 * textWatcher to validate the VisitorName
	 * 
	 * @author jayapriya
	 * 
	 * @created 14/11/2014
	 */
	private TextWatcher textWatcherNameChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence charSequence, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence charSequence, int start, int before,
				int count) {

			if ((editTextVisitorName != null)
					&& (!editTextVisitorName.getText().toString().trim()
							.isEmpty())
					&& (editTextVisitorName.getText().toString().trim()
							.length() > MainConstants.kConstantZero)) {
				validateName();
				editTextVisitorName.setTextSize(46);
			} else {
				editTextVisitorName.setTextSize(25);
				textViewErrorInName.setVisibility(View.INVISIBLE);
			}
			if (imageviewNext != null) {
				imageviewNext.setVisibility(View.GONE);
				if (editTextVisitorName != null
						&& editTextVisitorName.getText().toString().trim()
								.length() >= MainConstants.kConstantThree) {
					imageviewNext.setVisibility(View.VISIBLE);
				} else {
					imageviewNext.setVisibility(View.GONE);
				}
				noActionHandler.removeCallbacks(noActionThread);
				noActionHandler.postDelayed(noActionThread,
						MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);	
			}
		}
		public void afterTextChanged(Editable editable) {
		}
	};

	/*
	 * textWatcher to validate the VisitorMobileNo
	 * 
	 * @author jayapriya
	 * 
	 * @created 14/11/2014
	 */
	private TextWatcher textWatcherContactChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			validateContact();
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
		}

		public void afterTextChanged(Editable s) {
		}

	};

	/*
	 * listener to move home page
	 * 
	 * @author jayapriya
	 * 
	 * @created 14/11/2014
	 */
	private OnClickListener listenerGoToHomePage = new OnClickListener() {

		public void onClick(View v) {
			hideKeyboard();
			showDetailDialog();

		}
	};
	
	/**
	 * listener to change image and set boolean true/false
	 * 
	 * @author Karthick
	 * 
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceOne = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceOne==true) {
				booleanExtDeviceOne=false;
			imageviewExtDeviceOne.setBackgroundResource(R.drawable.icon_dvd_deselected);
			} else {
				booleanExtDeviceOne=true;
				imageviewExtDeviceOne.setBackgroundResource(R.drawable.icon_dvd_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};
	
	/**
	 * listener to change image and set boolean true/false
	 * 
	 * @author Karthick
	 * 
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceTwo = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceTwo==true) {
				booleanExtDeviceTwo=false;
			imageviewExtDeviceTwo.setBackgroundResource(R.drawable.icon_pendrive_deselcted);
			} else {
				booleanExtDeviceTwo=true;
				imageviewExtDeviceTwo.setBackgroundResource(R.drawable.icon_pendrive_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};
	
	/**
	 * listener to change image and set boolean true/false
	 * 
	 * @author Karthick
	 * 
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceThree = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceThree==true) {
				booleanExtDeviceThree=false;
			imageviewExtDeviceThree.setBackgroundResource(R.drawable.icon_externalcard_deselected);
			} else {
				booleanExtDeviceThree=true;
				imageviewExtDeviceThree.setBackgroundResource(R.drawable.icon_externalcard_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};
	
	/**
	 * listener to change image and set boolean true/false
	 * 
	 * @author Karthick
	 * 
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceFour = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceFour==true) {
				booleanExtDeviceFour=false;
			imageviewExtDeviceFour.setBackgroundResource(R.drawable.icon_harddisk_deselected);
			} else {
				booleanExtDeviceFour=true;
				imageviewExtDeviceFour.setBackgroundResource(R.drawable.icon_harddisk_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};
	
	/**
	 * listener to change image and set boolean true/false
	 * 
	 * @author Karthick
	 * 
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceFive = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceFive==true) {
				booleanExtDeviceFive=false;
			imageviewExtDeviceFive.setBackgroundResource(R.drawable.icon_tablet_deselected);
			} else {
				booleanExtDeviceFive=true;
				imageviewExtDeviceFive.setBackgroundResource(R.drawable.icon_tablet_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};
	
	/*
	 * listener to retry upload data or goto home
	 * 
	 * @author Karthick
	 * 
	 * @created 20161222
	 */
	private OnClickListener listenerRetry = new OnClickListener() {

		public void onClick(View v) {
			if(SingletonVisitorProfile.getInstance().getRetryCount()<MainConstants.retryMaxCount)
			{
			validDetails();
			} else {
				DialogView.hideDialog();
				getHomePage(MainConstants.fromDialogConfirm);
			}
		}
	};
	
	/**
	 * Auto check in started
	 * 
	 * @author Karthick
	 * @created 20160123
	 * 
	 */
	public Runnable autoCheckInThread = new Runnable() {
		public void run() {
			
			if (editTextMake != null && editTextMake.getText() != null) {
				SingletonVisitorProfile.getInstance().setDeviceMake(
						editTextMake.getText().toString().trim());
			}
			if (editTextSerialNumber != null
					&& editTextSerialNumber.getText() != null) {
				SingletonVisitorProfile.getInstance().setDeviceSerialNumber(
						editTextSerialNumber.getText().toString().trim());
			}
			retryCount=0;
			if(noActionHandler!=null) {
				noActionHandler.removeCallbacks(noActionThread);
			}
			
			if(signValidateHandler!=null){
				signValidateHandler.removeCallbacks(validateSignThread);
			}
			validDetails();
		}
	};
	
	/**
	 * Goto home page when no action.
	 * 
	 * @author karthick
	 * @created 16/06/2015
	 * 
	 */
	public Runnable noActionThread = new Runnable() {
		public void run() {
			
			getHomePage(MainConstants.fromAutoHandler);
		}
	};
	
	/**
	 * Show purpose dialog for get from visitor
	 * 
	 * @author Karthick
	 * @created 20170102
	 * 
	 */
	public Runnable threadShowPurpose = new Runnable() {
		public void run() {
			showDialogToChooseVisitorPurpose(employee.getEmployeeDept());
		}
		};
	
	/**
	 * Set zoom animation
	 * 
	 * @author Karthick
	 * @created 20161221
	 * 
	 */
	public Runnable validateSignThread = new Runnable() {
		public void run() {
			//setZoomAnimation();
			imageviewNext.setVisibility(View.VISIBLE);
		}
	};

	/**
	 * Validate Contact
	 * 
	 * @author Karthick
	 * @created 20170406
	 * 
	 */
	private void validateContact() {
		if ((editTextVisitorContact != null)
				&& (editTextVisitorContact.getText().toString().trim()
						.length() > MainConstants.kConstantZero)) {
			if (imageviewNext != null) {
				imageviewNext.setVisibility(View.GONE);
				if (editTextVisitorContact.getText().toString().trim()
						.length() >= MainConstants.kConstantSix) {
					imageviewNext.setVisibility(View.VISIBLE);
				}
			}
			editTextVisitorContact.setTextSize(46);
			checkMailOrMobileNo = String.valueOf(editTextVisitorContact
					.getText().toString().trim()
					.charAt(MainConstants.kConstantZero));
			if (checkMailOrMobileNo
					.matches(MainConstants.kConstantValidMailOrMobileNo)) {
				validateMailText();
			} else {
				validateMobileNoText();
			}
		} else {
			if(editTextVisitorContact!=null) {
				editTextVisitorContact.setTextSize(25);
				}
				if(textViewErrorInContact!=null) {
				textViewErrorInContact.setVisibility(View.INVISIBLE);
				}
			if (imageviewNext != null) {
				imageviewNext.setVisibility(View.GONE);
			}
		}
	}

	private void showDetailDialog() {

		if (((editTextVisitorName != null)
				&& (editTextVisitorName.getText().toString().trim() != MainConstants.kConstantEmpty) && (editTextVisitorName
				.getText().toString().trim().length() > MainConstants.kConstantZero))
				|| (!singleTouchViewGetVisitorSign.isPathNotEmpty())
				|| ((editTextVisitorContact != null)
						&& (editTextVisitorContact.getText().toString().trim() != MainConstants.kConstantEmpty) && (editTextVisitorContact
						.getText().toString().trim().length() > MainConstants.kConstantZero))
				|| ((textGetEmployeeName != null)
						&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty) && (textGetEmployeeName
						.getText().toString().trim().length() > MainConstants.kConstantZero))
				|| ((editTextTempIdNumber != null)
						&& (editTextTempIdNumber.getText().toString().trim() != MainConstants.kConstantEmpty) && (editTextTempIdNumber
						.getText().toString().trim().length() > MainConstants.kConstantZero))
				|| (SingletonVisitorProfile.getInstance()
						.getDeviceSerialNumber() != null
						&& !SingletonVisitorProfile.getInstance()
								.getDeviceSerialNumber().trim().isEmpty()
						&& SingletonVisitorProfile.getInstance()
								.getDeviceMake() != null && !SingletonVisitorProfile
						.getInstance().getDeviceMake().trim().isEmpty())) {
			dialogConfirmCode=MainConstants.dialogConfirmHome;
			intializeDialogBoxForHomePage();
			showHomeDialogBox();
			textSpeach(MainConstants.speachAreYouSureCode);
		} else {
			getHomePage(MainConstants.fromBackpress);
		}
	}

	/*
	 * When choose the employee name to visit ask the visitor to get the purpose
	 * of visit
	 * 
	 * @author Gokul
	 * 
	 * @created 20160813
	 */
	private void showDialogToChooseVisitorPurpose(String empDept) {
		
		
		if (dialogChooseVisitorPurpose == null) {
			dialogChooseVisitorPurpose = new Dialog(this);
			dialogChooseVisitorPurpose
					.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialogChooseVisitorPurpose
					.setContentView(R.layout.dialog_choose_visitor_purpose);
			dialogChooseVisitorPurpose.setCanceledOnTouchOutside(false);
			radioGroupVisitorPurpose = (RadioGroup) dialogChooseVisitorPurpose
					.findViewById(R.id.radioVisitorPurpose);
			radioButtonVisitorPurposePersonal = (RadioButton) dialogChooseVisitorPurpose
					.findViewById(R.id.radioPurposePersonal);
			radioButtonVisitorPurposeOfficial = (RadioButton) dialogChooseVisitorPurpose
					.findViewById(R.id.radioPurposeBusiness);

			radioButtonVisitorPurposeInterview = (RadioButton) dialogChooseVisitorPurpose
					.findViewById(R.id.radioPurposeInterview);
			// Always when shows the dialog it will be true

			if (employee.getEmployeeDept() != null
					&& employee.getEmployeeDept().equals(
							MainConstants.HRDepartment)
					|| employee.getEmployeeDept().equals(
							MainConstants.HRDepartmentAnotherNamePlural)
					|| employee.getEmployeeDept().equals(
							MainConstants.HRDepartmentAnotherNameSingular)) {
				radioButtonVisitorPurposeInterview.setVisibility(View.VISIBLE);
				radioButtonVisitorPurposeInterview.setChecked(true);
			} else {
				radioButtonVisitorPurposeInterview.setVisibility(View.GONE);
				radioButtonVisitorPurposeOfficial.setChecked(true);
				radioButtonVisitorPurposeOfficial.setTextColor(getResources()
						.getColor(R.color.teal_green));
			}
			/*
			 * change the text color of the radio button when changes made
			 * 
			 * @author Gokul
			 * 
			 * @created 20160819
			 */
			radioGroupVisitorPurpose
					.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

						public void onCheckedChanged(RadioGroup group,
								int checkedId) {
							if (noActionHandler != null) {
								noActionHandler.removeCallbacks(noActionThread);
								noActionHandler
										.postDelayed(
												noActionThread,
												MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
							}
							radioButtonVisitorPurposeInterview
									.setTextColor(getResources().getColor(
											R.color.deselected_radio_button));
							radioButtonVisitorPurposePersonal
									.setTextColor(getResources().getColor(
											R.color.deselected_radio_button));
							radioButtonVisitorPurposeOfficial
									.setTextColor(getResources().getColor(
											R.color.deselected_radio_button));
							int selectedRadioButtonId = radioGroupVisitorPurpose
									.getCheckedRadioButtonId();
							radioButtonVisitorPurposeSelected = (RadioButton) dialogChooseVisitorPurpose
									.findViewById(selectedRadioButtonId);
							radioButtonVisitorPurposeSelected
									.setTextColor(getResources().getColor(
											R.color.teal_green));
						}
					});
			textViewGetVisitorPurposeConfirm = (TextView) dialogChooseVisitorPurpose
					.findViewById(R.id.text_view_visitor_purpose_confirm);
			dialogChooseVisitorPurpose.setCancelable(false);
			textViewGetVisitorPurposeTitle = (TextView) dialogChooseVisitorPurpose
					.findViewById(R.id.text_view_choose_purpose);
			textViewGetVisitorPurposeTitle
					.setTypeface(typefacedialogChooseVisitorPurposeTitle);
			radioButtonVisitorPurposeInterview
					.setTypeface(typefacedialogChooseVisitorPurposeRadioButtons);
			radioButtonVisitorPurposeOfficial
					.setTypeface(typefacedialogChooseVisitorPurposeRadioButtons);
			radioButtonVisitorPurposePersonal
					.setTypeface(typefacedialogChooseVisitorPurposeRadioButtons);
			textViewGetVisitorPurposeConfirm
					.setTypeface(typefacedialogChooseVisitorPurposeConfirm);
			textViewGetVisitorPurposeConfirm
					.setOnClickListener(DialogGetVisitorPurposeConfirm);
		}
		if ((dialogChooseVisitorPurpose != null)
				&& (!dialogChooseVisitorPurpose.isShowing())) {
			if (employee.getEmployeeDept().equals(MainConstants.HRDepartment)
					|| employee.getEmployeeDept().equals(
							MainConstants.HRDepartmentAnotherNamePlural)
					|| employee.getEmployeeDept().equals(
							MainConstants.HRDepartmentAnotherNameSingular)) {
				radioButtonVisitorPurposeInterview.setChecked(true);
				radioButtonVisitorPurposeInterview.setVisibility(View.VISIBLE);
			} else {
				radioButtonVisitorPurposeInterview.setVisibility(View.GONE);
				radioButtonVisitorPurposeOfficial.setChecked(true);
			}
			dialogChooseVisitorPurpose.show();
		}
	}

	/*
	 * tapped the next button to validate the visitordetail and launch next
	 * visit page
	 * 
	 * @author jayapriya
	 * 
	 * @created 14/11/2014
	 */
	private OnClickListener listenerVisitorDetailConfirm = new OnClickListener() {
		public void onClick(View v) {
			retryCount=0;
			
			validDetails();
		}
	};

	/*
	 * listener for button next - hide and show the components to get the
	 * visitor details and checkin to the app based on the parameter the
	 * validation will be done ,if the validation is success then the components
	 * will be hidden and shown based on the parameter
	 * 
	 * @author Gokul
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerimageviewNext = new OnClickListener() {
		public void onClick(View v) {
			nextPressed();
		}
	};
	
	/**
	 * Skip employee name 
	 * 
	 * @author Karthick
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerButtonSkip = new OnClickListener() {
		public void onClick(View v) {
			hideKeyboard();
			dialogConfirmCode=MainConstants.dialogConfirmSkipEmployee;
			intializeDialogBoxForHomePage();
			showHomeDialogBox();
		}
	};
	
	/**
	 * Skip employee name 
	 * 
	 * @author Karthick
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerHaveDeviceNo = new OnClickListener() {
		public void onClick(View v) {
			isHaveDevice=false;
			validateCodeForPreviousAndNextButton=validateCodeForPreviousAndNextButton+2;
			showHideComponents(validateCodeForPreviousAndNextButton);
		}
	};
	
	
	/**
	 * Skip employee name 
	 * 
	 * @author Karthick
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerHaveDeviceYes = new OnClickListener() {
		public void onClick(View v) {
			isHaveDevice=true;
			validateCodeForPreviousAndNextButton=validateCodeForPreviousAndNextButton+1;
			showHideComponents(validateCodeForPreviousAndNextButton);
			
		}
	};
	
	
	/*
	 * listener for button next - hide and show the components to get the
	 * visitor details and checkin to the app based on the parameter the
	 * validation will be done ,if the validation is success then the components
	 * will be hidden and shown based on the parameter
	 * 
	 * @author Gokul
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerButtonPrevious = new OnClickListener() {
		public void onClick(View v) {
			previousPressed();
		}
	};

	/**
	 * Change no action handler time
	 * And show next component
	 * 
	 * @author Karthick
	 * @created on 20170102
	 */
	private void clickSkip() {
			if (noActionHandler != null) {
				noActionHandler.removeCallbacks(noActionThread);
				noActionHandler.postDelayed(noActionThread,
						MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			}
			textGetEmployeeName.setText(MainConstants.kConstantEmpty);
			validateCodeForPreviousAndNextButton=validateCodeForPreviousAndNextButton+1;
			showHideComponents(validateCodeForPreviousAndNextButton);
	}
	
	/*
	 * if keyboard done pressed then validate the visitor detail and then launch
	 * the visit page.
	 * 
	 * 
	 * @author jayapriya
	 * 
	 * @created 02/12/2014
	 */
	private void nextPressed() {
		
		if(handlerPurpose!=null) {
			handlerPurpose.removeCallbacks(purposeSeletionTask);
		}
		
		if (noActionHandler != null) {
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
		}
		if (validateCodeForPreviousAndNextButton != MainConstants.kConstantMinusOne) {
			// show the textbox visitor name
			validateCodeForPreviousAndNextButton++;
			showHideComponentsAndValidate(validateCodeForPreviousAndNextButton);
		}
		
	}
	
	/*
	 * if keyboard previous pressed then goto back
	 * 
	 * @author jayapriya
	 * @created 02/12/2014
	 */
	private void previousPressed() {
	if (noActionHandler != null) {
		noActionHandler.removeCallbacks(noActionThread);
		noActionHandler
				.postDelayed(
						noActionThread,
						MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
	}
	if (validateCodeForPreviousAndNextButton >= MainConstants.kConstantZero) {
		if (validateCodeForPreviousAndNextButton > MainConstants.kConstantZero) {
			// show the textbox visitor name
			validateCodeForPreviousAndNextButton--;
			if(isHaveDevice==false) {
				validateCodeForPreviousAndNextButton--;
				isHaveDevice=true;
			}
			showHideComponents(validateCodeForPreviousAndNextButton);
		}
	}
	}

	/*
	 * get the singleton values and set it to the separate edit text.
	 * 
	 * @author jayapriya
	 * 
	 * @created 22/09/2014
	 */
	private void getSingletonValuesToSetEditText() {

		if (SingletonVisitorProfile.getInstance() != null) {
			if ((SingletonVisitorProfile.getInstance().getVisitorName() != null)
					&& (SingletonVisitorProfile.getInstance().getVisitorName()
							.toString() != MainConstants.kConstantEmpty)
					&& (SingletonVisitorProfile.getInstance().getVisitorName()
							.toString().length() > MainConstants.kConstantZero)) {
				editTextVisitorName.setText(SingletonVisitorProfile
						.getInstance().getVisitorName().trim().toUpperCase());
				editTextVisitorName.setSelection(editTextVisitorName.getText()
						.toString().trim().length());
				validateName();
			}

			if ((SingletonVisitorProfile.getInstance().getContact() != null)
					&& (SingletonVisitorProfile.getInstance().getContact() != MainConstants.kConstantEmpty)
					&& (SingletonVisitorProfile.getInstance().getContact()
							.length() > MainConstants.kConstantZero)) {
				editTextVisitorContact.setTextSize(25);
				editTextVisitorContact.setText(SingletonVisitorProfile
						.getInstance().getContact());
				if (editTextVisitorContact.getText().toString().trim().length() > MainConstants.kConstantZero) {
					editTextVisitorContact.setSelection(editTextVisitorContact
							.getText().toString().trim().length());
				}
			}

			if ((SingletonVisitorProfile.getInstance().getVisitorImage() != null)
					&& (SingletonVisitorProfile.getInstance().getVisitorImage()
							.toString() != MainConstants.kConstantEmpty)
					&& (SingletonVisitorProfile.getInstance().getVisitorImage()
							.length > MainConstants.kConstantZero)) {
				imagepath = SingletonVisitorProfile.getInstance()
						.getVisitorImage();
			}
		}

		if ((SingletonEmployeeProfile.getInstance() != null)
				&& (SingletonEmployeeProfile.getInstance().getEmployeeName() != null)
				&& (SingletonEmployeeProfile.getInstance().getEmployeeName() != MainConstants.kConstantEmpty) && textGetEmployeeName!=null) {

			textGetEmployeeName.removeTextChangedListener(textWatcherEmployeeNameChanged);
			textGetEmployeeName.setText(SingletonEmployeeProfile.getInstance()
					.getEmployeeName().toUpperCase());
			textGetEmployeeName.setSelection(textGetEmployeeName.getText()
					.toString().trim().length());
			textGetEmployeeName.addTextChangedListener(textWatcherEmployeeNameChanged);
			
			if(database!=null) {
			employeeList = database.getEmployeeListWithName(textGetEmployeeName
					.getText().toString().trim().replaceAll("'", "''"));
			if ((employeeList != null)
					&& (employeeList.size() == MainConstants.kConstantOne)) {
				employee = employeeList.get(MainConstants.kConstantZero);
			}
			}
		}
	}

	/*
	 * Created by jayapriya On 24/09/2014 get the edittext values and set it to
	 * the singletonVariable.
	 * 
	 * @author jayapriya
	 * 
	 * @created 24/09/2014
	 */
	private void setSingletonValues() {

		storeEmployeeIntoSingleton(employee);
		if ((editTextVisitorName!=null && editTextVisitorName.getText() != null)
				&& (editTextVisitorName.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (editTextVisitorName.getText().toString().trim().length() > MainConstants.kConstantZero)) {
			SingletonVisitorProfile.getInstance().setVisitorName(
					editTextVisitorName.getText().toString().trim());
		} else {
			SingletonVisitorProfile.getInstance().setVisitorName(
					MainConstants.kConstantEmpty);
		}
		
		if ((editTextCompany.getText() != null)
				&& (editTextCompany.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (editTextCompany.getText().toString().trim().length() > MainConstants.kConstantZero)) {
			SingletonVisitorProfile.getInstance().setCompany(
					editTextCompany.getText().toString().trim());
		} else {
			SingletonVisitorProfile.getInstance().setCompany(MainConstants.kConstantEmpty);
		}

		if (SingletonEmployeeProfile.getInstance().getEmployeeName() != null
				&& SingletonVisitorProfile
						.getInstance()
						.getPurposeOfVisitCode()
						.equals(getResources().getString(
								R.string.radio_string_purpose_interview))) {
			SingletonVisitorProfile.getInstance().setPurposeOfVisit(
					MainConstants.interViewPurpose);
		} else if (SingletonEmployeeProfile.getInstance().getEmployeeName() != null) {
			SingletonVisitorProfile.getInstance().setPurposeOfVisit(
					MainConstants.visitorPurpose);
		}

		if ((editTextVisitorContact != null)) {

			if ((editTextVisitorContact.getText().toString().trim() != MainConstants.kConstantEmpty)
					&& (editTextVisitorContact.getText().toString().trim()
							.length() > MainConstants.kConstantZero)) {

				SingletonVisitorProfile.getInstance().setContact(
						editTextVisitorContact.getText().toString().trim());
			} else {
				SingletonVisitorProfile.getInstance().setContact(
						MainConstants.kConstantEmpty);
			}
		} else {
			SingletonVisitorProfile.getInstance().setContact(
					MainConstants.kConstantEmpty);
		}
		if (imagepath != null) {
			SingletonVisitorProfile.getInstance().setVisitorImage(
					imagepath);
		} else {
			SingletonVisitorProfile.getInstance().setVisitorImage(
					new byte[0]);
		}
	}

	/*
	 * store TempIDNumber
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	private void storeTempIDNumber() {

		if ((editTextTempIdNumber != null)
				&& (editTextTempIdNumber.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (editTextTempIdNumber.getText().toString().trim().length() > MainConstants.kConstantZero)) {
			long tempID = 0;
			try {
				if (editTextTempIdNumber != null) {
					tempID = Long.parseLong(editTextTempIdNumber.getText()
							.toString().trim());
				}
			} catch (NumberFormatException e) {
				tempID = 0;
			}
			SingletonVisitorProfile.getInstance().setTempIDNumber(tempID);
		}

	}

	/*
	 * store employeedetails into the singleton variables
	 */
	private void storeEmployeeIntoSingleton(ZohoEmployeeRecord employee) {

		if ((employee != null)
				&& (employee.getEmployeeName() != MainConstants.kConstantEmpty)
				&& (employee.getEmployeeName().length() > MainConstants.kConstantZero)) {

			SingletonEmployeeProfile.getInstance().setEmployeeName(
					employee.getEmployeeName());
			SingletonEmployeeProfile.getInstance().setEmployeeExtentionNumber(
					employee.getEmployeeExtentionNumber());
			SingletonEmployeeProfile.getInstance().setEmployeeId(
					employee.getEmployeeId());
			SingletonEmployeeProfile.getInstance().setEmployeeMailId(
					employee.getEmployeeEmailId());
			SingletonEmployeeProfile.getInstance().setEmployeeDepartment(
					employee.getEmployeeDept());

			if (((SingletonVisitorProfile.getInstance().getLocationCode() != null))
					&& ((SingletonVisitorProfile.getInstance()
							.getLocationCode() == MainConstants.kConstantMinusOne)
							|| (SingletonVisitorProfile.getInstance()
									.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
							|| (SingletonVisitorProfile.getInstance()
									.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) || (SingletonVisitorProfile
							.getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
				validation.SetMobileNumberWithLocation(employee
						.getEmployeeMobileNumber());
			} else {
				SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
						employee.getEmployeeMobileNumber());
			}
			SingletonEmployeeProfile.getInstance().setEmployeeZuid(
					employee.getEmployeeZuid());
			SingletonEmployeeProfile.getInstance().setEmployeeSeatingLocation(
					employee.getEmployeeSeatingLocation());
		}
	}

	/*
	 * validate the visitor Image
	 * 
	 * @author jayapriya
	 * 
	 * @created 09/03/2015
	 */
	private Boolean validateImage() {
		if ((SingletonVisitorProfile.getInstance().getOfficeLocation() != null)
				&& (SingletonVisitorProfile.getInstance().getOfficeLocation() != MainConstants.kConstantEmpty)
				&& (SingletonVisitorProfile.getInstance().getOfficeLocation()
						.indexOf(MainConstants.kConstantOfficeInIndia) != MainConstants.kConstantMinusOne)) {
			if ((SingletonVisitorProfile.getInstance().getVisitorImage() != null)
					&& (SingletonVisitorProfile.getInstance().getVisitorImage().length>0)) {
				return true;
			} else {
				return false;
			}
		} else 
		{
			return true;
		}

	}

	/*
	 * validate the visitor Details and activate the next activity.
	 * 
	 * @author jayapriya
	 * 
	 * @created 25/09/2014
	 */
	private void validDetails() {
		setSingletonValues();
		storeTempIDNumber();
		if ((editTextVisitorName != null)
				&& (editTextVisitorName.getText().toString().trim().length() >=MainConstants.kConstantThree)
				&& (validation.validateName(editTextVisitorName.getText()
						.toString().trim()) == true)
				&& (validateImage() == true)
				&& (editTextVisitorContact != null)
				&& (editTextVisitorContact.getText().toString() != MainConstants.kConstantEmpty)
				&& ((validation
						.validateVisitorContact(editTextVisitorContact
								.getText().toString().trim())) == true)
				&& (!singleTouchViewGetVisitorSign.isPathNotEmpty())
				&& (SingletonVisitorProfile.getInstance().getTempIDNumber() > 0)) {

			GetSign();
			nextProcessing();
			employeeNameValidate();

			if(animationZoom!=null) {
				animationZoom.cancel();
			}
		} else {
			hideKeyboard();
			utility.UsedDialogBox.intializeDialogBox(this,
					StringConstants.kConstantErrorMsgToValidDetail);

			if ((validation.validateVisitorContact(editTextVisitorContact
					.getText().toString().trim()) == false)) {
			}

			if (singleTouchViewGetVisitorSign.isPathNotEmpty()) {
				imageViewErrorInVisitorSign.setVisibility(View.VISIBLE);
			}
			if (SingletonVisitorProfile.getInstance().getTempIDNumber() <= 0) {
			}
			if (validation.validateName(editTextVisitorName.getText()
					.toString().trim()) == false || editTextVisitorName.getText().toString().trim().length() <MainConstants.kConstantThree) {
			}

			if (validateImage() == false) {
				relativeLayoutShootMe
						.setBackgroundResource(R.drawable.error_photo_border);
			}
		}

	}

	/*
	 * validate employee name
	 * 
	 * @author Jayapriya
	 * 
	 * @created 29/09/2014
	 */
	private void employeeNameValidate() {

		if ((textGetEmployeeName.getText() != null)
				&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (textGetEmployeeName.getText().toString().trim().length() > MainConstants.kConstantZero)
				&& ((employee == null) || (employee.getEmployeeName() == null) || (employee
						.getEmployeeName() == MainConstants.kConstantEmpty))) {
			utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
					StringConstants.kConstantErrorMsgInValidEmployeeDetail,
					Toast.LENGTH_LONG).show();
			utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
					StringConstants.kConstantErrorMsgInValidEmployeeDetail,
					Toast.LENGTH_LONG).show();
		}
	}

	/*
	 * Created by Jayapriya On 29/09/2014
	 * 
	 * Get the visitor signature and security signature, convert signature into
	 * bitmap. Store bitmap path into the singleton variable. And upload details
	 * into the creator
	 */
	private void GetSign() {

		Date date = new Date();
		UsedBitmap usedBitmap = new UsedBitmap();
		
		Bitmap visitorSign = usedBitmap.createBitmap(singleTouchViewGetVisitorSign,
				date.getTime() + StringConstants.kConstantVisitorSign);

		if ((visitorSign != null)) {
			SingletonVisitorProfile.getInstance().setVisitorSign(usedBitmap.getBytes(visitorSign));
			visitorSign.recycle();
			visitorSign=null;
		}

		if ((SingletonVisitorProfile.getInstance().getVisitorSign() != null)
				&& (SingletonVisitorProfile.getInstance().getVisitorSign().length>0)
				&& (SingletonVisitorProfile.getInstance().getContact() != null)
				&& (SingletonVisitorProfile.getInstance().getContact().length() > MainConstants.kConstantZero)
				&& (SingletonVisitorProfile.getInstance().getVisitorName() != null)
				&& (SingletonVisitorProfile.getInstance().getVisitorName() != MainConstants.kConstantEmpty)
				&& (SingletonVisitorProfile.getInstance().getVisitorName()
						.length() > MainConstants.kConstantZero)) {

			if ((SingletonVisitorProfile.getInstance().getContact() != null)
					&& (SingletonVisitorProfile.getInstance().getContact() != MainConstants.kConstantEmpty)
					&& (SingletonVisitorProfile.getInstance().toString()
							.length() > MainConstants.kConstantZero)) {
				String contact = SingletonVisitorProfile
						.getInstance()
						.getContact()
						.replace(MainConstants.kConstantWhiteSpace,
								MainConstants.kConstantEmpty);
				if (contact.matches(MainConstants.kConstantValidPhoneNo) == true) {

					if (((SingletonVisitorProfile.getInstance()
							.getLocationCode() != null))
							&& ((SingletonVisitorProfile.getInstance()
									.getLocationCode() == MainConstants.kConstantMinusOne)
									|| (SingletonVisitorProfile.getInstance()
											.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
									|| (SingletonVisitorProfile.getInstance()
											.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) || (SingletonVisitorProfile
									.getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
						validation
								.SetVisitorMobileNumberWithLocation(SingletonVisitorProfile
										.getInstance().getContact());
					} else {
						SingletonVisitorProfile.getInstance().setPhoneNo(
								SingletonVisitorProfile.getInstance()
										.getContact());
					}
				} else {
					SingletonVisitorProfile.getInstance().setEmail(
							SingletonVisitorProfile.getInstance().getContact());
				}
			}
			if (SingletonVisitorProfile.getInstance().getVisitorBusinessCard() != null
					&& SingletonVisitorProfile.getInstance()
							.getVisitorBusinessCard().trim().length() > 0) {
				usedBitmap.storeImage(SingletonVisitorProfile
						.getInstance().getVisitorBusinessCard());
			}
			if (utility.TimeZoneConventer.checkConnection(this) == true) {
			} else {
				utility.UsedCustomToast.makeCustomToast(
						getApplicationContext(),
						StringConstants.kConstantErrorMsgInNetworkError,
						Toast.LENGTH_LONG).show();
				utility.UsedCustomToast.makeCustomToast(
						getApplicationContext(),
						StringConstants.kConstantErrorMsgInNetworkError,
						Toast.LENGTH_LONG).show();
			}
		}
	}

	/*
	 * Intent to launch the next activity.
	 * 
	 * @author jayapriya
	 * 
	 * @created 25/09/2014
	 */
	private void nextProcessing() {
		hideKeyboard();
		DialogView.showReportDialog(this, MainConstants.badgeDialog,
				MainConstants.forgotIDUploadCode);
		if ((SingletonVisitorProfile.getInstance() == null
				|| SingletonVisitorProfile.getInstance().getVisitId() == null
				|| SingletonVisitorProfile.getInstance().getVisitId()
						.isEmpty())) {
			backgroundProcess.cancel(true);
		}
		
		if(noActionHandler!=null) {
			noActionHandler.removeCallbacks(noActionThread);
		}
		if(signValidateHandler!=null) {
			signValidateHandler.removeCallbacks(validateSignThread);
		}
		if (handlerAutoCheckIn != null) {
			handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
		}
			
		backgroundProcess = new backgroundProcess(
				MainConstants.jsonUploadJsonData);
		if (backgroundProcess != null) {
			backgroundProcess.execute();
			if(animationZoom!=null) {
				animationZoom.cancel();
				buttonVisitorDetailConfirm.clearAnimation();
			}
		}
	}

	/*
	 * when back pressed then only call this function. (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 * 
	 * @author jayapriya
	 * 
	 * @created 25/09/2014
	 */
	public void onBackPressed() {
		if (validateCodeForPreviousAndNextButton > MainConstants.kConstantZero) {
			previousPressed();
		} else {
		
		showDetailDialog();
		}
		
	}

	protected void onResume() {
		super.onResume();
		editTextVisitorName.requestFocus();
		if (noActionHandler != null) {
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
		}
		// default when launch only the visitor name shown
		 showHideComponents(validateCodeForPreviousAndNextButton);
	}

	/*
	 * Created by Jayapriya On 26/09/2014 validate the visitorName.
	 */
	private void validateName() {

		if ((editTextVisitorName != null)
				&& (editTextVisitorName.getText().length() > MainConstants.kConstantZero)) {
			if (validation.validateName(editTextVisitorName.getText()
					.toString()) == false) {
				textViewErrorInName.setVisibility(View.VISIBLE);
			} else {
				textViewErrorInName.setVisibility(View.INVISIBLE);
				editTextVisitorName.setTextColor(Color
						.parseColor(MainConstants.color_code_efefef));
			}
		}
	}

	/*
	 * Created by jayapriya On 01/12/2014 Intialize the dialogbox for home page
	 */
	private void intializeDialogBoxForHomePage() {
		dialogGetHomeMessage = new Dialog(this);
		dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogGetHomeMessage
				.setContentView(R.layout.dialog_box_confirm_message);
		dialogGetHomeMessage.setCanceledOnTouchOutside(true);
		dialogGetHomeMessage.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message);
		textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_confirm);
		textViewHomeMessageCancel = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_cancel);
		
		if(dialogConfirmCode==MainConstants.dialogConfirmHome) {
			textViewDialogHomeMessage
			.setText(StringConstants.kConstantHomePageErrorMessage);
		} else {
			textViewDialogHomeMessage
			.setText(StringConstants.kConstantSkipEmployeeMessage);
		}
		
		textViewDialogHomeMessage.setTypeface(dialogMessageFont);
		textViewHomeMessageCancel.setTypeface(dialogButtonFont);
		textViewHomeMessageConfirm.setTypeface(dialogButtonFont);
		textViewHomeMessageConfirm.setOnClickListener(discardConfirm);
		textViewHomeMessageCancel.setOnClickListener(discardCancel);

	}

	/*
	 * Created by jayapriya On 01/12/2014 show dialogbox for home page
	 */
	private void showHomeDialogBox() {

		if ((dialogGetHomeMessage != null)
				&& (!dialogGetHomeMessage.isShowing())) {
			dialogGetHomeMessage.show();
		}
	}

	/*
	 * Created by Jayapriya On 26/09/2014 Click the errorMessageConfirm to
	 * activate the next processing.
	 */
	private OnClickListener discardConfirm = new OnClickListener() {
		public void onClick(View v) {
			if ((dialogGetHomeMessage != null)) {
				dialogGetHomeMessage.dismiss();
			}
			
			if(dialogConfirmCode==MainConstants.dialogConfirmHome) {
			getHomePage(MainConstants.fromDialogConfirm);
			} else {
				clickSkip();
				showKeyboard();
			}
		}
	};
	
	/**
	 * Open barcode scan
	 * 
	 * @author Karthick
	 * @created on 20161221
	 */
	private OnClickListener openBarcodeScanListener = new OnClickListener() {
		
		public void onClick(View v) {
			launchBarcodeScan();
			if (handlerAutoCheckIn != null) {
				handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
			}
		}
	};
	
	/*
	 * the dialog of get purpose of visit from the visitor and store that into
	 * the singleton object
	 * 
	 * @author Gokul
	 * 
	 * @created 20160813
	 */
	private OnClickListener DialogGetVisitorPurposeConfirm = new OnClickListener() {

		public void onClick(View v) {
			if (radioGroupVisitorPurpose != null) {
				int selectedRadioButtonId = radioGroupVisitorPurpose
						.getCheckedRadioButtonId();
				radioButtonVisitorPurposeSelected = (RadioButton) dialogChooseVisitorPurpose
						.findViewById(selectedRadioButtonId);
				if (radioButtonVisitorPurposeSelected != null
						&& radioButtonVisitorPurposeSelected.getText() != null) {

					SingletonVisitorProfile.getInstance()
							.setPurposeOfVisitCode(
									radioButtonVisitorPurposeSelected.getText()
											.toString());
				}
				if (dialogChooseVisitorPurpose != null) {
					dialogChooseVisitorPurpose.dismiss();
				}
			}
			if(textGetEmployeeName!=null) {
			textGetEmployeeName.requestFocus();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
			}
		}
	};

	/*
	 * Control hide and show the components to get the visitor details and
	 * checkin to the app based on the parameter the validation will be done ,if
	 * the validation is succceded then the components will be hidden and shown
	 * based on the parameter
	 * 
	 * @author Gokul
	 * 
	 * @created 20160913
	 */

	private void showHideComponentsAndValidate(int validateCode) {
		if (validateCode >= MainConstants.kConstantZero) {
			boolean isValidated = validateComponents(validateCode);
			//4 is for purpose page
			if (isValidated || validateCode == 4) {
				showHideComponents(validateCode);
			} else if (validateCodeForPreviousAndNextButton > MainConstants.kConstantZero) {
				validateCodeForPreviousAndNextButton--;
			}
		}
	}

	/*
	 * hide and show the components to get the visitor details and checkin to
	 * the app. the components will be hidden and shown based on the parameter
	 * 
	 * @author Gokul
	 * 
	 * @created 20160913
	 */
	private void showHideComponents(int validateCode) {
		
		buttonSkip.setVisibility(View.GONE);
		if(params!=null && textViewDisplayFieldName!=null) {
		params.setMargins(0, 100, 0, 0);
		textViewDisplayFieldName.setLayoutParams(params);
		}
		
		if (handlerAutoCheckIn != null) {
			handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
		}
	
		if(animationZoom!=null && buttonVisitorDetailConfirm!=null) {
			animationZoom.cancel();
			buttonVisitorDetailConfirm.clearAnimation();
		}
		buttonVisitorDetailConfirm.setVisibility(View.INVISIBLE);
		relativeLayoutHaveDevice.setVisibility(View.GONE);
		
		if (validateCode != MainConstants.kConstantMinusOne) {
			textViewDisplayFieldName.setVisibility(View.VISIBLE);
			linearLayoutVisitorName.setVisibility(View.GONE);
			relativeLayoutCheckIn.setVisibility(View.GONE);
			linearLayoutVisitorContact.setVisibility(View.GONE);
			linearlayoutVisitorEmployeeName.setVisibility(View.GONE);
			linearlayoutTempID.setVisibility(View.GONE);
			relativeLayoutVisitorSign.setVisibility(View.GONE);
			relativeLayoutHaveDevice.setVisibility(View.GONE);
			linearLayoutPurpose.setVisibility(View.GONE);
			linearlayoutDevices.setVisibility(View.GONE);
			// show previous and next buttons
			imageViewPrevious.setVisibility(View.VISIBLE);
			imageViewBack.setVisibility(View.VISIBLE);
			buttonVisitorDetailConfirm.setVisibility(View.INVISIBLE);
			if (validateCode == MainConstants.kConstantZero) {
				textViewDisplayFieldName.setText(getResources().getString(
						R.string.edittext_visitor_name_hint).toUpperCase());
				imageviewNext.setVisibility(View.GONE);
				if (editTextVisitorName != null
						&& editTextVisitorName.getText().toString().trim()
								.length() >= MainConstants.kConstantThree) {
					imageviewNext.setVisibility(View.VISIBLE);
				}
				
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next_violet);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous_violet);
				}
				
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFF4bbcef,0xFF46c0e5,0xFF3ec5d4,0xFF35ccc1});
			    relativeLayoutVisitorDetail.setBackground(gd);
			    
				linearLayoutVisitorName.setVisibility(View.VISIBLE);
				editTextVisitorName.requestFocus();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(editTextVisitorName,
						InputMethodManager.SHOW_IMPLICIT);
				imageViewPrevious.setVisibility(View.GONE);
				imageViewBack.setVisibility(View.GONE);
			} else if (validateCode == MainConstants.kConstantOne) {
				textViewDisplayFieldName.setText(getResources().getString(
						R.string.edittext_visitor_contact_hint).toUpperCase());
				imageviewNext.setVisibility(View.GONE);
				if (editTextVisitorContact != null
						&& editTextVisitorContact.getText().toString().trim()
								.length() >= MainConstants.kConstantSix) {
					imageviewNext.setVisibility(View.VISIBLE);
				}
				
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next_green);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous_green);
				}
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFFba6fb6,0xFF8e76c3,0xFF8478c6,0xFF8278c7});
			    
				relativeLayoutVisitorDetail.setBackground(gd);
		
				// show the text box to get Visitor email/phone
				linearLayoutVisitorContact.setVisibility(View.VISIBLE);
				editTextVisitorContact.requestFocus();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(editTextVisitorContact,
						InputMethodManager.SHOW_IMPLICIT);
			} else if (validateCode == MainConstants.kConstantTwo) {
				textViewDisplayFieldName.setText(getResources().getString(
						R.string.edittext_visitor_employee_hint).toUpperCase());
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFFfa654b,0xFFfc5d55,0xFFfd575d,0xFFff5066});
			    relativeLayoutVisitorDetail.setBackground(gd);
				// show the text box to get employee
				linearlayoutVisitorEmployeeName.setVisibility(View.VISIBLE);
				
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous);
				}

				imageviewNext.setVisibility(View.VISIBLE);
				if(employee==null) {
				imageviewNext.setVisibility(View.GONE);
				buttonSkip.setVisibility(View.VISIBLE);
				}
				textGetEmployeeName.requestFocus();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(textGetEmployeeName,
						InputMethodManager.SHOW_IMPLICIT);
			} else if (validateCode == MainConstants.kConstantThree) {
				isPurposeSelected=false;
				setPurposeList();
				
				textViewDisplayFieldName.setText(getResources().getString(
						R.string.text_purpose_title).toUpperCase());
				imageviewNext.setVisibility(View.VISIBLE);
				
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next_violet);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous_violet);
				}
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFFba6fb6,0xFF8e76c3,0xFF8478c6,0xFF8278c7});
			    relativeLayoutVisitorDetail.setBackground(gd);
			    linearLayoutPurpose.setVisibility(View.VISIBLE);
				
			} else if (validateCode == MainConstants.kConstantFour) {
				
				textViewDisplayFieldName.setText(getResources().getString(
						R.string.edittext_temp_id_hint).toUpperCase());
				imageviewNext.setVisibility(View.GONE);
				if (editTextTempIdNumber != null
						&& editTextTempIdNumber.getText().toString().trim()
								.length() >= MainConstants.kConstantOne) {
					imageviewNext.setVisibility(View.VISIBLE);
				}
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next_green);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous_green);
				}
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFF4bbcef,0xFF46c0e5,0xFF3ec5d4,0xFF35ccc1});
			    relativeLayoutVisitorDetail.setBackground(gd);
			    
				// show the text box to get temporary id
				linearlayoutTempID.setVisibility(View.VISIBLE);
				editTextTempIdNumber.requestFocus();
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(editTextTempIdNumber,
						InputMethodManager.SHOW_IMPLICIT);
			} else if (validateCode == MainConstants.kConstantFive) {
				textViewDisplayFieldName.setVisibility(View.INVISIBLE);
				imageviewNext.setVisibility(View.GONE);
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFFfa654b,0xFFfc5d55,0xFFfd575d,0xFFff5066});
			    relativeLayoutVisitorDetail.setBackground(gd);

				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous);
				}
				
				if (!singleTouchViewGetVisitorSign.isPathNotEmpty()) {
					
				imageviewNext.setVisibility(View.VISIBLE);
				}
				// show the text box to get visitor sign and laptop details if
				// have
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(
						editTextTempIdNumber.getWindowToken(), 0);
				relativeLayoutVisitorSign.setVisibility(View.VISIBLE);
				buttonVisitorDetailConfirm.setVisibility(View.INVISIBLE);
			} else if (validateCode == MainConstants.kConstantSix) {
				relativeLayoutHaveDevice.setVisibility(View.VISIBLE);
				imageviewNext.setVisibility(View.INVISIBLE);
				buttonVisitorDetailConfirm.setVisibility(View.INVISIBLE);
				
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next_green);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous_green);
				}
				
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFF4bbcef,0xFF46c0e5,0xFF3ec5d4,0xFF35ccc1});
			    relativeLayoutVisitorDetail.setBackground(gd);
				
				if (handlerAutoCheckIn != null) {
					handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
					handlerAutoCheckIn.postDelayed(autoCheckInThread,
							MainConstants.autoCheckInTime);
				}
			} else if (validateCode == MainConstants.kConstantSeven) {
				textViewDisplayFieldName.setText(getResources().getString(
						R.string.external_device_heading));
				
				textViewDisplayFieldName.setVisibility(View.VISIBLE);
				imageviewNext.setVisibility(View.VISIBLE);
				// show the text box to get visitor sign and laptop details if
				// have
				linearlayoutDevices.setVisibility(View.VISIBLE);
				buttonVisitorDetailConfirm.setVisibility(View.INVISIBLE);
				editTextSerialNumber.requestFocus();

				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous);
				}
				
				if(textViewDisplayFieldName!=null && params!=null) {
				params.setMargins(0, 0, 0, 0);
				textViewDisplayFieldName.setLayoutParams(params);
				}
				
				if (handlerAutoCheckIn != null) {
					handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
					handlerAutoCheckIn.postDelayed(autoCheckInThread,
							MainConstants.autoCheckInTimeDevice);
				}
				
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFFfa654b,0xFFfc5d55,0xFFfd575d,0xFFff5066});
			    relativeLayoutVisitorDetail.setBackground(gd);
				
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(editTextSerialNumber,
						InputMethodManager.SHOW_IMPLICIT);
				
			} else if (validateCode == MainConstants.kConstantEight) {
				textViewDisplayFieldName.setVisibility(View.INVISIBLE);
				imageviewNext.setVisibility(View.INVISIBLE);
				relativeLayoutCheckIn.setVisibility(View.VISIBLE);
				buttonVisitorDetailConfirm.setVisibility(View.VISIBLE);
				
				if (handlerAutoCheckIn != null) {
					handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
					handlerAutoCheckIn.postDelayed(autoCheckInThread,
							MainConstants.autoCheckInTime);
				}
				
				if(imageviewNext!=null) {
					imageviewNext.setImageResource(R.drawable.icon_next_violet);
				}
				if(imageViewPrevious!=null) {
					imageViewPrevious.setImageResource(R.drawable.icon_previous_violet);
				}
				
				GradientDrawable gd = new GradientDrawable(
			            GradientDrawable.Orientation.BOTTOM_TOP,
			            new int[] {0xFFba6fb6,0xFF8e76c3,0xFF8478c6,0xFF8278c7});
			    relativeLayoutVisitorDetail.setBackground(gd);
			    
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editTextSerialNumber.getWindowToken(), 0);
				setZoomAnimation();
			}
		}
		textSpeach(validateCodeForPreviousAndNextButton);
	}

	/*
	 * validate the components to get the visitor details and checkin to the
	 * app. the components will be validate based on the parameter
	 * 
	 * @author Gokul
	 * @created 20160913
	 */
	private Boolean validateComponents(int validateCode) {
		Boolean isValidate = MainConstants.boolFalse;
		if (validateCode != MainConstants.kConstantMinusOne) {
			if (validateCode == MainConstants.kConstantOne) {
				// validate the text box to get Visitor name
				if ((editTextVisitorName != null)
						&& (editTextVisitorName.getText().toString().trim() != MainConstants.kConstantEmpty)
						&& (validation.validateName(editTextVisitorName
								.getText().toString().trim()) == true)
						&& editTextVisitorName.getText().toString().trim()
								.length() >= MainConstants.kConstantThree) {
					return MainConstants.boolTrue;
				} else if (editTextVisitorName != null
						&& !editTextVisitorName.getText().toString().trim()
								.isEmpty()) {
					textViewErrorInName.setVisibility(View.VISIBLE);
					return false;
				} else if (editTextVisitorName != null
						&& editTextVisitorName.getText().toString().trim()
								.length() < MainConstants.kConstantThree) {
					return false;
				}
			} else if (validateCode == MainConstants.kConstantTwo) {
				// validate the text box to get email/phone
				if ((editTextVisitorContact != null)
						&& (!editTextVisitorContact.getText().toString().trim()
								.isEmpty())
						&& ((validation
								.validateVisitorContact(editTextVisitorContact
										.getText().toString().trim())) == true)) {
					return true;
				} else if ((editTextVisitorContact != null)
						&& (editTextVisitorContact.getText().toString().trim()
								.isEmpty())) {
					return false;
				} else if (editTextVisitorContact != null
						&& (!editTextVisitorContact.getText().toString().trim()
								.isEmpty())) {
					if (editTextVisitorContact.getText().toString().trim()
							.matches("[0-9]+")) {
						if (!validation.validateMobileNo(editTextVisitorContact
								.getText().toString().trim())) {
							textViewErrorInContact
									.setText(StringConstants.kConstantErrorMsgToCheckPhoneNo);
						}
					} else {
						if (!validation.validateMail(editTextVisitorContact
								.getText().toString().trim())) {
							textViewErrorInContact
									.setText(StringConstants.kConstantErrorMsgToValidMailId);
						}
					}
					textViewErrorInContact.setVisibility(View.VISIBLE);
					return false;
				}

			} else if (validateCode == MainConstants.kConstantThree) {

				// validate the text box to get temporary id
				if (((textGetEmployeeName.getText() != null)
						&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
						&& (textGetEmployeeName.getText().toString().trim()
								.length() > MainConstants.kConstantZero) && (employee != null))) {
					textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
					return true;
				} else {
					textEmployeeErrorMsg.setVisibility(View.VISIBLE);
					return false;
				}
//			} else if (validateCode == MainConstants.kConstantFour) {
//				if(isPurposeSelected) {
//				return true;
//				} 
//				return false;
			} else if (validateCode == MainConstants.kConstantFive) {
				// validate the text box to get visitor sign and laptop details
				// if have
				if ((editTextTempIdNumber != null)
						&& (editTextTempIdNumber.getText().toString().trim() != MainConstants.kConstantEmpty)
						&& (editTextTempIdNumber.getText().toString().trim()
								.length() > MainConstants.kConstantZero)
						&& (Integer.parseInt(editTextTempIdNumber.getText()
								.toString().trim()) > MainConstants.kConstantZero)) {
					return true;
				} else {
					return false;
				}

			} else if (validateCode == MainConstants.kConstantSix) {
				if (singleTouchViewGetVisitorSign.isPathNotEmpty()) {

					imageViewErrorInVisitorSign.setVisibility(View.VISIBLE);
				}
				
				if (!singleTouchViewGetVisitorSign.isPathNotEmpty()) {
					return true;
				} else {
					return false;
				}

			} else if (validateCode == MainConstants.kConstantEight) {
				Boolean isValid = false;
				

				if (editTextMake != null
						&& editTextMake.getText() != null
						&& !editTextMake.getText().toString().trim().isEmpty()
						&& editTextSerialNumber != null
						&& editTextSerialNumber.getText() != null
						&& !editTextSerialNumber.getText().toString().trim()
								.isEmpty()) {
					isValid = true;
				}

				if (editTextMake != null
						&& editTextMake.getText() != null
						&& editTextMake.getText().toString().trim().isEmpty()
						&& editTextSerialNumber != null
						&& editTextSerialNumber.getText() != null
						&& editTextSerialNumber.getText().toString().trim()
								.isEmpty()) {
					isValid = true;
				}

				// assign value to singleton object
				if (editTextMake != null && editTextMake.getText() != null
						&& isValid) {
					SingletonVisitorProfile.getInstance().setDeviceMake(
							editTextMake.getText().toString().trim());

				}
				if (editTextSerialNumber != null
						&& editTextSerialNumber.getText() != null && isValid) {
					SingletonVisitorProfile.getInstance().setDeviceSerialNumber(
							editTextSerialNumber.getText().toString().trim());
				}
				if (((SingletonVisitorProfile.getInstance() != null && !SingletonVisitorProfile
						.getInstance().getDeviceMake().isEmpty()) && (SingletonVisitorProfile
						.getInstance() != null && !SingletonVisitorProfile
						.getInstance().getDeviceSerialNumber().isEmpty()))
						&& (isValid)) {
					imageGetLaptopDetails
							.setBackgroundResource(R.drawable.icon_laptop_has_data);
				} else if (((SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile
						.getInstance().getDeviceMake().isEmpty()) && (SingletonVisitorProfile
						.getInstance() != null && SingletonVisitorProfile
						.getInstance().getDeviceSerialNumber().isEmpty()))) {
					imageGetLaptopDetails
							.setBackgroundResource(R.drawable.icon_laptop_no_data);
				}
				
				if (editTextMake != null && editTextMake.getText() != null
						&& !editTextMake.getText().toString().trim().isEmpty()) {
					editTextMake
							.setBackgroundResource(R.drawable.cell_underline_white);
				} else if(isValid==false){
					UsedCustomToast.makeCustomToast(this, "Enter Laptop Make", Toast.LENGTH_SHORT, Gravity.TOP).show();
				}

				if (editTextSerialNumber != null
						&& editTextSerialNumber.getText() != null
						&& !editTextSerialNumber.getText().toString().trim()
								.isEmpty()) {
				} else if(isValid==false){
					UsedCustomToast.makeCustomToast(this, "Enter Laptop Serial Number", Toast.LENGTH_SHORT, Gravity.TOP).show();
				}
				return isValid;
			}
		}
		return isValidate;
	}

	/*
	 * Created by karthick On 28/04/2015 intent to launch the home page.
	 */
	private void getHomePage(int fromToClose) {
		clearData();
		if (dialogGetLaptopDetails != null
				&& dialogGetLaptopDetails.isShowing()) {
			dialogGetLaptopDetails.dismiss();
		}
		DialogView.hideDialog();
		buttonSkip.setVisibility(View.GONE);
		Intent intent = new Intent(getApplicationContext(),
				WelcomeActivity.class);
		startActivity(intent);
		if(noActionHandler!=null) {
		noActionHandler.removeCallbacks(noActionThread);
		}
		finish();
	}

	/*
	 * Created by Jayapriya On 02/12/2014 Click the errorMessageCancel to
	 * dismiss the dialogBox
	 */
	private OnClickListener discardCancel = new OnClickListener() {

		public void onClick(View v) {
			if ((dialogGetHomeMessage != null)
					&& (dialogGetHomeMessage.isShowing())) {
				dialogGetHomeMessage.dismiss();
			}
			if(dialogConfirmCode==MainConstants.dialogConfirmSkipEmployee) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!imm.isActive()){
	            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY); // show
	        }
			}
		}
	};

	/*
	 * Created by Jayapriya On 27/09/2014 Deallocate the Objects.
	 */
	private void deallocateObjects() {

		singleTouchViewGetVisitorSign = null;
		editTextTempIdNumber = null;
		textGetEmployeeName = null;
		employeeList = null;
		editTextVisitorName = null;
		editTextCompany = null;
		editTextVisitorContact = null;
		imageViewHomePage = null;
		imageViewPrevious = null;
		imageGetLaptopDetails = null;
		relativeLayoutShootMe = null;
		relativeLayoutCheckIn=null;
		buttonVisitorDetailConfirm = null;
		imageviewNext = null;
		imagepath = null;
		creator = null;
		typeFaceTitle = null;
		textViewDialogHomeMessage = null;
		textViewErrorInName = null;
		textViewErrorInContact = null;
		textViewHomeMessageConfirm = null;
		textViewHomeMessageCancel = null;
		textViewMandory = null;
		textViewDisplayFieldName = null;
		textViewVisitorPageHeading = null;
		typeFaceTitle = null;
		typefaceHeading = null;
		dialogGetHomeMessage = null;
		dialogGetLaptopDetails = null;
		dialogChooseVisitorPurpose = null;
		creator = null;
		checkMailOrMobileNo = null;
		textViewVisitorSignature = null;
		if (backgroundProcess != null) {
			backgroundProcess.cancel(true);
			backgroundProcess = null;
		}
		if(relativeLayoutVisitorDetail!=null) {
		relativeLayoutVisitorDetail.setBackground(null);
		relativeLayoutVisitorDetail=null;
		}
		if(imageviewTexture!=null) {
			imageviewTexture.setBackground(null);
			imageviewTexture=null;
		}
		if(animationZoom!=null) {
		animationZoom.cancel();
		animationZoom=null;
		}
		if(signValidateHandler!=null) {
		signValidateHandler.removeCallbacks(validateSignThread);
		}
		if(handlerShowPurposeDialog!=null) {
		handlerShowPurposeDialog.removeCallbacks(threadShowPurpose);
		}
		if(handlerAutoCheckIn!=null) {
			handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
			}
		if(handlerPurpose!=null) {
			handlerPurpose.removeCallbacks(purposeSeletionTask);
		}
		if(mPlayer!=null) {
			mPlayer.release();
			mPlayer=null;
		}
		if(textToSpeech!=null) {
			textToSpeech.stop();
			textToSpeech=null;
		}
	}

	/*
	 * Created by Jayapriya On 13/12/2014 Validate the mobileNumber
	 */
	private void validateMobileNoText() {

		if ((editTextVisitorContact != null)
				&& (editTextVisitorContact.getText().toString().trim().length() > MainConstants.kConstantZero)
				&& ((editTextVisitorContact.getText().toString().trim()
						.matches(MainConstants.kConstantValidPhoneNoText)) == false)) {
			textViewErrorInContact.setVisibility(View.VISIBLE);
			textViewErrorInContact
					.setText(StringConstants.kConstantErrorMsgValidPhoneNo);
		} else {
			editTextVisitorContact.setTextColor(Color
					.parseColor(MainConstants.color_code_efefef));
			textViewErrorInContact.setVisibility(View.INVISIBLE);
		}
	}

	/*
	 * validate the mailId
	 * 
	 * @author jayapriya
	 * 
	 * @created 13/12/2014
	 */
	private void validateMailText() {
		if ((editTextVisitorContact != null)
				&& (editTextVisitorContact.getText().toString().trim().length() > MainConstants.kConstantZero)
				&& ((editTextVisitorContact.getText().toString().trim()
						.matches(MainConstants.kConstantValidEmailText)) == false)) {
			textViewErrorInContact.setVisibility(View.VISIBLE);
			textViewErrorInContact
					.setText(StringConstants.kConstantErrorMsgValidMailText);
		} else {
			editTextVisitorContact.setTextColor(Color
					.parseColor(MainConstants.color_code_efefef));
			textViewErrorInContact.setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * Choose the purpose of visit
	 * 
	 * @author jayapriya
	 * 
	 * @created 27/02/2015
	 */
	private AdapterView.OnItemClickListener listenerChooseEmployee = new AdapterView.OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
//			if(textGetEmployeeName!=null){
//				textGetEmployeeName.setFocusable(false);
//				textGetEmployeeName.setFocusableInTouchMode(true);
//				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//				 if (imm.isActive()){
//			            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
//				 }
//			}

			employee = (ZohoEmployeeRecord) parent.getAdapter().getItem(
					position);
			buttonSkip.setVisibility(View.INVISIBLE);
			imageviewNext.setVisibility(View.VISIBLE);
			textGetEmployeeName
					.setBackgroundResource(R.drawable.cell_underline_white);

			String name = ((ZohoEmployeeRecord) parent.getAdapter().getItem(
					position)).getEmployeeName();
			textGetEmployeeName
					.removeTextChangedListener(textWatcherEmployeeNameChanged);
			textGetEmployeeName.setText(name.toUpperCase());

			textGetEmployeeName.setSelection(textGetEmployeeName.getText()
					.length());
			textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
			textGetEmployeeName
					.addTextChangedListener(textWatcherEmployeeNameChanged);
			// show dialog box to get the visitor purpose
			// when visitor visits want to visit the HR person
			if (employee.getEmployeeDept().equals(MainConstants.HRDepartment)
					|| employee.getEmployeeDept().equals(
							MainConstants.HRDepartmentAnotherNamePlural)
					|| employee.getEmployeeDept().equals(
							MainConstants.HRDepartmentAnotherNameSingular)) {
			}
			// get the purpose of visitor - It is personal visit
			// TODO : now it is default personal visit after it will be changed
			else {
				dialogChooseVisitorPurpose = null;
//				visitingPurposeCheckbox = (RelativeLayout) findViewById(R.id.relativeLayout_visitor_purpose);
				
//				checkboxVisitorPurpose = (CheckBox) findViewById(R.id.checkboxVisitorPurpose);
				SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
						getResources().getString(
								R.string.radio_string_purpose_personal));
			}
			if (noActionHandler != null) {
				noActionHandler.removeCallbacks(noActionThread);
				noActionHandler.postDelayed(noActionThread,
								MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			}
//			handlerShowPurposeDialog.postDelayed(threadShowPurpose, MainConstants.purposeDialogShowTime);
			handlerPurpose.postDelayed(purposeSeletionTask, 1000);
//			nextPressed();
		}
	};

	private View.OnFocusChangeListener listenerSetFocusOnEmployee = new View.OnFocusChangeListener() {

		public void onFocusChange(View v, boolean hasFocus) {
			if (textGetEmployeeName.hasFocus() == false) {

				if ((textGetEmployeeName != null)
						&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
						&& (textGetEmployeeName.getText().toString().trim()
								.length() > MainConstants.kConstantZero)
						&& ((employee == null)
								|| (employee.getEmployeeName() == null) || (employee
								.getEmployeeName() == MainConstants.kConstantEmpty))) {

					textEmployeeErrorMsg.setVisibility(View.VISIBLE);
				} else {
					textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
				}
			}

		}
	};

	/*
	 * textWatcher to validate the employeeName
	 * 
	 * @author jayapriya
	 * 
	 * @created 28/02/2015
	 */
	private TextWatcher textWatcherEmployeeNameChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		}
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			setCustomAdaptor(count);
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			buttonSkip.setVisibility(View.VISIBLE);
			imageviewNext.setVisibility(View.GONE);
		}
		public void afterTextChanged(Editable s) {

		}
	};

	private void setCustomAdaptor(int count) {
		setEmployeeNameFont();
		if (count == MainConstants.kConstantZero) {
			SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
					MainConstants.kConstantEmpty);
//			if (visitingPurposeCheckbox != null
//					&& visitingPurposeCheckbox.isShown()) {
//			}
			textGetEmployeeName
					.setBackgroundResource(R.drawable.cell_underline_white);
		}
		employee = null;
		if ((textGetEmployeeName != null)
				&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (textGetEmployeeName.getText().toString().trim().length() >= MainConstants.kConstantThree)) {

			textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
			// get suggestions from the database
			employeeList = database.getEmployeeListWithName(textGetEmployeeName
					.getText().toString().trim().replaceAll("'", "''"));
			if ((employeeList != null)
					&& (employeeList.size() > MainConstants.kConstantZero)) {
				setEmployeeMaxLength(MainConstants.maxLengthTracedEmployee);
				ZohoEmployeeRecord[] employeeArray = employeeList
						.toArray(new ZohoEmployeeRecord[employeeList.size()]);
				if ((employeeArray != null)
						&& (employeeArray.length > MainConstants.kConstantZero)) {
					// update the adapter
					myAdapter = new AutocompleteCustomArrayAdapter(this,
							layout.custom_spinner, employeeArray);
					textGetEmployeeName.setAdapter(myAdapter);
					// update the adapater
					myAdapter.notifyDataSetChanged();
				}
			} else {
				try {
					myAdapter.clear();
					} catch(Exception e) {	
					}
				myAdapter.notifyDataSetChanged();
				textEmployeeErrorMsg.setVisibility(View.VISIBLE);
				
				setEmployeeMaxLength(MainConstants.maxLengthUnTracedEmployee);
			}
		}
		else {
			if ((SingletonEmployeeProfile.getInstance() != null)
					|| (employee == null)) {
				SingletonEmployeeProfile.getInstance().clearInstance();
			}
			if (myAdapter != null) {
				try {
				myAdapter.clear();
				} catch(Exception e) {	
				}
				myAdapter.notifyDataSetChanged();
			}
			textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * Set max length for employee field
	 * 
	 * @author Karthick
	 * @created 20170424
	 */
	private void setEmployeeMaxLength(int length) {
		InputFilter[] filterArray = new InputFilter[1];
		if(filterArray!=null && filterArray.length>0) {
	    filterArray[0] = new InputFilter.LengthFilter(length);
		}
		if(textGetEmployeeName!=null && filterArray!=null && filterArray[0]!=null) {
	    textGetEmployeeName.setFilters(filterArray);
		}
	}
	
	private void setAutoCheckinDeviceHandler() {
		if(noActionHandler!=null) {
			noActionHandler.removeCallbacks(noActionThread);
		}
		if (handlerAutoCheckIn != null) {
			handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
			handlerAutoCheckIn.postDelayed(autoCheckInThread,
					MainConstants.autoCheckInTimeDevice);
		}
	}
	
	/*
	 * set employee name font size
	 * 
	 * @author karthick
	 * 
	 * @created april - 2015
	 */
	private void setEmployeeNameFont() {

		if (textGetEmployeeName != null
				&& textGetEmployeeName.getText().toString().trim().length() > MainConstants.kConstantZero) {
			textGetEmployeeName.setTextSize(46);
		} else if (textGetEmployeeName != null) {
			textGetEmployeeName.setTextSize(25);
		}
	}

	/*
	 * Autocomplete Custom Array Adapter for custom dialog
	 * 
	 * @author jayapriya
	 * @created 2014
	 */

	public class AutocompleteCustomArrayAdapter extends
			ArrayAdapter<ZohoEmployeeRecord> {

		final String TAG = "AutocompleteCustomArrayAdapter.java";

		Context mContext;
		int layoutResourceId;
		ZohoEmployeeRecord employee[] = null;
		long minFrequency, maxFrequency, avarageFrequency;

		public AutocompleteCustomArrayAdapter(Context mContext,
				int layoutResourceId, ZohoEmployeeRecord[] employee) {

			super(mContext, layoutResourceId, employee);

			this.layoutResourceId = layoutResourceId;
			this.mContext = mContext;
			this.employee = employee;
			if (employee.length > MainConstants.kConstantZero) {
				this.minFrequency = employee[employee.length - 1]
						.getEmployeeVisitFrequency();
				this.maxFrequency = employee[0].getEmployeeVisitFrequency();
				this.avarageFrequency = (maxFrequency - minFrequency) / 3;
			}
		}

		public class ViewHolder {
			public RelativeLayout relativeLayoutCustomSpinner;
			public TextView textEmployeeName;
			public TextView textEmployeePhoneNo;
			public TextView textEmployeeDept;
			public ImageView employeeImage;

			public ViewHolder getTag() {
				return null;
			}
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			try {

				// object item based on the position
				ZohoEmployeeRecord employees = employee[position];
				if (employees != null) {
					final ViewHolder view;
					if (convertView == null) {
						view = new ViewHolder();
						// inflate the layout
						LayoutInflater inflater = ((VisitorDetailActivity) mContext)
								.getLayoutInflater();
						convertView = inflater.inflate(layoutResourceId,
								parent, false);
						view.relativeLayoutCustomSpinner = (RelativeLayout) convertView
								.findViewById(id.relativeLayout_custom_spinner);
						view.textEmployeeName = (TextView) convertView
								.findViewById(id.text_view_employee_name);
						view.textEmployeePhoneNo = (TextView) convertView
								.findViewById(id.text_view_employee_Phone);
						view.textEmployeeDept = (TextView) convertView
								.findViewById(id.text_view_employee_Dept);
						view.employeeImage = (ImageView) convertView
								.findViewById(id.image_view_employee);
						convertView.setTag(view);
					} else {
						view = (ViewHolder) convertView.getTag();
					}

					view.textEmployeeName.setText(employees.getEmployeeName()
							.toUpperCase());
					view.textEmployeeName.setTypeface(typeFaceTitle);
					view.textEmployeePhoneNo.setText(employees
							.getEmployeeMobileNumber());
					view.textEmployeePhoneNo.setTypeface(typeFaceTitle);
					view.textEmployeeDept.setText(employees.getEmployeeDept());
					view.textEmployeeDept.setTypeface(typeFaceTitle);

					if ((this.maxFrequency >= employees
							.getEmployeeVisitFrequency())
							&& ((this.maxFrequency - this.avarageFrequency) <= employees
									.getEmployeeVisitFrequency())
							&& this.maxFrequency > 0) {

						view.employeeImage.getLayoutParams().height = 300;
						view.employeeImage.getLayoutParams().width = 300;
						view.textEmployeeName.setTextSize(30);
						view.textEmployeeDept.setTextSize(26);
						view.textEmployeePhoneNo.setTextSize(26);
						view.textEmployeeName
								.setTypeface(employeeVisitFreqHigh);
						view.relativeLayoutCustomSpinner
								.setBackgroundColor(Color.rgb(248, 125, 35));
					} else if (((this.maxFrequency - this.avarageFrequency) > employees
							.getEmployeeVisitFrequency())
							&& ((this.maxFrequency - (MainConstants.kConstantTwo * this.avarageFrequency)) <= employees
									.getEmployeeVisitFrequency())
							&& this.maxFrequency > 0) {

						view.employeeImage.getLayoutParams().height = 250;
						view.employeeImage.getLayoutParams().width = 250;
						view.textEmployeeName.setTextSize(26);
						view.textEmployeeDept.setTextSize(23);
						view.textEmployeePhoneNo.setTextSize(23);
						view.textEmployeeName
								.setTypeface(employeeVisitFreqMedium);
						view.relativeLayoutCustomSpinner
								.setBackgroundColor(Color.rgb(186, 166, 150));
						
					} else {

						view.employeeImage.getLayoutParams().height = 160;
						view.employeeImage.getLayoutParams().width = 160;
						view.textEmployeeName.setTextSize(20);
						view.textEmployeeDept.setTextSize(18);
						view.textEmployeePhoneNo.setTextSize(20);
						view.textEmployeeName.setTypeface(employeeVisitFreqLow);
						view.relativeLayoutCustomSpinner
								.setBackgroundColor(Color.rgb(184, 183, 183));
					}
					employeePhoto = MainConstants.kConstantFileDirectory
							.concat(MainConstants.kConstantFileEmployeeImageFolder)
							.concat(employees.getEmployeeZuid())
							.concat(MainConstants.kConstantImageFileFormat);
					if ((employees.getEmployeePhoto() != null)
							&& (employees.getEmployeePhoto() != MainConstants.kConstantEmpty)
							&& (new File(employeePhoto).exists())) {
						if (getBitmapFromMemCache(employees.getEmployeeZuid()
								.toString()) != null) {
							view.employeeImage
									.setImageBitmap(getBitmapFromMemCache(employees
											.getEmployeeZuid().toString()));
						} else {
							Bitmap drawableEmployeeImage=CompressImage
										.decodeSampleBitmapFromSdCard(
												getResources(), employeePhoto,
												100, 100);
						if (drawableEmployeeImage != null) {
							Bitmap bitmap = BitmapFactory.decodeFile(employeePhoto);
							view.employeeImage.setImageBitmap(bitmap);
									
							BitmapWorkerTask task = new BitmapWorkerTask(
									employees.getEmployeeZuid()
											.toString(),
											drawableEmployeeImage);
							task.execute(employees.getEmployeeZuid()
									.toString());
						} else {
							if (view.employeeImage != null) {
								view.employeeImage.setImageResource( R.drawable.photo);		
							}
						}
						}	
					} else {
						if (view.employeeImage != null) {
							view.employeeImage.setImageResource( R.drawable.photo);		
						}
					}
				}
			} catch (NullPointerException e) {
			} catch (Exception e) {
			}
			return convertView;
		}
	}

	/*
	 * background process for upload visitor details
	 */
	private class backgroundProcess extends AsyncTask<String, String, String> {

		private int uploadedCode;
//		int deviceDetailsUploadedCode;
//		String response = MainConstants.kConstantEmpty;
		private int jsonRequestCode = MainConstants.jsonCreateUploadPhoto;

		public backgroundProcess(int jsonRequestCode) {
			this.jsonRequestCode = jsonRequestCode;
		}

		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected String doInBackground(String... f_url) {
	
				if ((SingletonVisitorProfile.getInstance() == null
						|| SingletonVisitorProfile.getInstance().getVisitId() == null
						|| SingletonVisitorProfile.getInstance().getVisitId()
								.isEmpty())) {
					uploadedCode = creator.addJsonEmptyRecord(MainConstants.visitorPurpose );
				}
				if ((SingletonVisitorProfile.getInstance() != null
						&& SingletonVisitorProfile.getInstance().getVisitId() != null
						&& !SingletonVisitorProfile.getInstance().getVisitId()
								.isEmpty())) {

				if (SingletonVisitorProfile.getInstance() != null
						&& SingletonVisitorProfile.getInstance()
								.getVisitorThumbPhotoUploaded() != 1 
						&& SingletonVisitorProfile.getInstance().getVisitorImage()!=null 
						&& SingletonVisitorProfile.getInstance()
										.getVisitorImage().length>0) {
					UsedBitmap usedBitmap = new UsedBitmap();
					Bitmap bitmap = usedBitmap.decodeBitmap(
							SingletonVisitorProfile.getInstance()
									.getVisitorImage(), MainConstants.visitorThumbImageWidth, MainConstants.visitorThumbImageHeight);
					if(bitmap!=null) {
					usedBitmap.storeBitmap(bitmap, getFilesDir()
							+ MainConstants.kConstantImageThumbFile);
						//Upload visitor photo to cloud
						String upLoadVisitorImageUri="";
						sharedPreferenceController = new SharedPreferenceController();
						int applicationMode =sharedPreferenceController.getApplicationMode(getApplicationContext());
						if(applicationMode==NumberConstants.kConstantDataCentreMode) {
							upLoadVisitorImageUri = getFileUploadUrlDataCentre();
						}
						else
						{
							upLoadVisitorImageUri = getFileUploadUrl();
						}

						uploadedCode = creator.uploadThupFile(
								upLoadVisitorImageUri, getFilesDir()
										+ MainConstants.kConstantImageThumbFile);
						bitmap.recycle();
					}
					if (uploadedCode == MainConstants.kConstantSuccessCode) {
						SingletonVisitorProfile.getInstance().setVisitorThumbPhotoUploaded(MainConstants.kConstantOne);
					}
				}
				if (jsonRequestCode == MainConstants.jsonUploadJsonData) {
					if (SingletonVisitorProfile.getInstance() != null
							&& SingletonVisitorProfile.getInstance().getVisitId() != null
							&& !SingletonVisitorProfile.getInstance().getVisitId()
									.isEmpty() && 
							((SingletonVisitorProfile.getInstance()
							.getVisitorThumbPhotoUploaded() == 1))) {
						uploadedCode = creator
								.updateVisitorJsonRecord(convertVisitorDetailsJsonData(),SingletonVisitorProfile.getInstance().getPurposeOfVisitCode());
					}
				}
				if(uploadedCode==ErrorConstants.visitorUpdateJsonDataCreatorError) {
					uploadedCode = creator.updateWithoutMailField();	
				}
				}
			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String responseParam) {
			if (jsonRequestCode != MainConstants.jsonCreateUploadPhoto) {
				if (uploadedCode == MainConstants.kConstantSuccessCode) {
					hideKeyboard();
					DialogView.showReportDialog(getApplicationContext(),
							MainConstants.badgeDialog,
							ErrorConstants.visitorAllSuccessCode);
					handlerHideDialog.postDelayed(hideDialogTask, 3000);
					insertVisitorDetails();
					textSpeach(MainConstants.speachWelcomeMsgCode);
				} else {
					
					SingletonVisitorProfile.getInstance().setRetryCount(retryCount);
					retryCount=retryCount+1;
					errorMusicPlay(R.raw.bonk);
					SingletonCode
							.setSingletonVisitorUploadErrorCode(uploadedCode);
					DialogView.showReportDialog(
							ApplicationController.getInstance(),
							MainConstants.badgeDialog, uploadedCode,listenerRetry);
					if(uploadedCode == ErrorConstants.visitorJsonDataIOError || uploadedCode == ErrorConstants.visitorUpdateJsonDataIOError) {
						reConnectWifi();
					}
					
					if(noActionHandler!=null) {
						noActionHandler.removeCallbacks(noActionThread);
						noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);
						}
				}
			}
		}
	}

	/**
	 * Reconnect wifi
	 * 
	 * @author Karthick
	 * @created on 20170417
	 */
	private void reConnectWifi() {
		NetworkConnection.wifiReconnect(getApplicationContext());
	}
	
	private void errorMusicPlay(int rawId) {
		if(mPlayer!=null) {
			mPlayer.release();
		}
		mPlayer = MediaPlayer.create(this, rawId);
		mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			public void onPrepared(MediaPlayer mediaPlayer) {
				mediaPlayer.start();
			}
		});
	}

	Runnable hideDialogTask = new Runnable() {
		public void run() {
			DialogView.hideDialog();
			getHomePage(MainConstants.fromSuccess);
		}
	};
	
	/**
	 * Next process
	 * 
	 * @author Karthick
	 * @created on 20161114
	 */
	Runnable purposeSeletionTask = new Runnable() {
		public void run() {
			nextPressed(); 
		}
	};

	/**
	 * Insert visitor details
	 * 
	 * @author Karthick
	 * @created on 20161114
	 */
	private void insertVisitorDetails() {
		long visitDate  = new Date().getTime();
		VisitorDetailsDatabase visitorDetailsDb = new VisitorDetailsDatabase();
		visitorDetailsDb.setJsonData(convertVisitorDetailsJsonData());
		visitorDetailsDb.setPhotoUrl(SingletonVisitorProfile.getInstance().getVisitorImage());
		visitorDetailsDb.setSignatureUrl(SingletonVisitorProfile.getInstance().getVisitorSign());		
		visitorDetailsDb.setPhotoUploaded(MainConstants.kConstantPhotoNotUploaded);
		visitorDetailsDb.setSignatureUploaded(MainConstants.kConstantSignatureNotUploaded);
		visitorDetailsDb.setVisitDate(visitDate);
		if(SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
				.getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {
		visitorDetailsDb.setVisitPurpose(MainConstants.interViewPurpose);
		} else {
			visitorDetailsDb.setVisitPurpose(MainConstants.visitorPurpose);		
		}
		visitorDetailsDb.setJsonDataId(SingletonVisitorProfile.getInstance().getVisitId());
		database.insertVisitorRecord(visitorDetailsDb);
		
		ExistingVisitorDetails existVisitorDetails = new ExistingVisitorDetails();
		if(existVisitorDetails!=null) {
			existVisitorDetails.setVisitedDate(visitDate);
			if(SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().getVisitorName()!=null) {
				existVisitorDetails.setName(SingletonVisitorProfile.getInstance().getVisitorName());
			}
			if(SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().getContact()!=null) {
				existVisitorDetails.setContact(SingletonVisitorProfile.getInstance().getContact());
			}
			if(SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()!=null) {
				existVisitorDetails.setPurpose(SingletonVisitorProfile.getInstance().getPurposeOfVisitCode());
			}
			if(SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().getVisitId()!=null) {
				existVisitorDetails.setCreatorVisitID(SingletonVisitorProfile.getInstance().getVisitId());
			}
			if(SingletonEmployeeProfile.getInstance()!=null && SingletonEmployeeProfile.getInstance().getEmployeeMailId()!=null) {
			existVisitorDetails.setEmployeeEmail(SingletonEmployeeProfile.getInstance().getEmployeeMailId());
			}
			database.insertExistVsitor(existVisitorDetails);
		}
	}

	private void clearData() {
		editTextVisitorName.setText(MainConstants.kConstantEmpty);
		editTextCompany.setText(MainConstants.kConstantEmpty);
		editTextVisitorContact.setText(MainConstants.kConstantEmpty);
		editTextTempIdNumber.setText(MainConstants.kConstantEmpty);
		textGetEmployeeName.setText(MainConstants.kConstantEmpty);
		removeView(relativeLayoutGetVisitorSign, singleTouchViewGetVisitorSign);
		imageViewErrorInVisitorSign.setVisibility(View.INVISIBLE);
	}

	/**
	 * @author jayapriya
	 * 
	 */
	public void onPause() {
		super.onPause();
		noActionHandler.removeCallbacks(noActionThread);
	}

	/**
	 * Prepare to upload data to cloud
	 * 
	 * @author Karthick
	 * @created on 20161110
	 */
	private String convertVisitorDetailsJsonData() {

		String jsonString = MainConstants.kConstantOpenBraces;
		if (SingletonVisitorProfile.getInstance() != null) {
			if (SingletonVisitorProfile.getInstance().getVisitorName() != null
					&& !SingletonVisitorProfile.getInstance().getVisitorName()
							.isEmpty()) {
				jsonString = jsonString
						+ MainConstants.jsonVisitorName
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
								.getVisitorName().toUpperCase()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getPhoneNo() != null
					&& !SingletonVisitorProfile.getInstance().getPhoneNo()
							.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorPhone
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getPhoneNo()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getEmail() != null
					&& !SingletonVisitorProfile.getInstance().getEmail()
							.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorEmail
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getEmail()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getCompany() != null
					&& !SingletonVisitorProfile.getInstance().getCompany()
							.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorCompany
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getCompany()
						+ MainConstants.kConstantSingleQuotes;
			}
			
			if (SingletonVisitorProfile.getInstance().getPurposeOfVisit() != null
					&& !SingletonVisitorProfile.getInstance()
							.getPurposeOfVisit().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOldPurpose
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
								.getPurposeOfVisit()
						+ MainConstants.kConstantSingleQuotes;
			}

			//Read purpose from spinner otherwise set offcial
			if(spinnerPurpose!=null && spinnerPurpose.getSelectedItem()!=null && !spinnerPurpose.getSelectedItem().toString().isEmpty()) {
				SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(spinnerPurpose.getSelectedItem().toString().toUpperCase());	
			} else {
				SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(MainConstants.purposeOfficial.toUpperCase());	
			}
			if(SingletonMetaData.getInstance()!=null
					&& SingletonMetaData.getInstance().getApplicationMode()==NumberConstants.kConstantNormalMode) {
				if (SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null
						&& !SingletonVisitorProfile.getInstance()
						.getPurposeOfVisitCode().isEmpty()) {
					jsonString = jsonString
							+ MainConstants.kConstantComma
							+ MainConstants.jsonVisitorVisitPurpose
							+ MainConstants.kConstantColon
							+ MainConstants.kConstantSingleQuotes
							+ SingletonVisitorProfile.getInstance()
							.getPurposeOfVisitCode().toUpperCase()
							+ MainConstants.kConstantSingleQuotes;
				}
			}
			else if(SingletonMetaData.getInstance()!=null
					&& SingletonMetaData.getInstance().getApplicationMode()== NumberConstants.kConstantDataCentreMode)
			{
				if (SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null
						&& !SingletonVisitorProfile.getInstance()
						.getPurposeOfVisitCode().isEmpty()) {
					jsonString = jsonString
							+ MainConstants.kConstantComma
							+ MainConstants.jsonVisitorVisitPurpose
							+ MainConstants.kConstantColon
							+ MainConstants.kConstantSingleQuotes
							+ MainConstants.purposeDataCenter
							+ MainConstants.kConstantSingleQuotes;
				}
			}

				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorCheckInTime
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ TimeZoneConventer.getCurrentDateTime()
						+ MainConstants.kConstantSingleQuotes;
			
			if (SingletonVisitorProfile.getInstance().getOfficeLocation() != null
					&& !SingletonVisitorProfile.getInstance()
							.getOfficeLocation().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOfficeLocation
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
								.getOfficeLocation()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getLocationCode() != null) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOfficeLocationCode
						+ MainConstants.kConstantColon
						+ SingletonVisitorProfile.getInstance()
								.getLocationCode();
			}

			if (SingletonVisitorProfile.getInstance().getTempIDNumber() > 0) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorTempId
						+ MainConstants.kConstantColon
						+ SingletonVisitorProfile.getInstance()
								.getTempIDNumber();
			}

			if (SingletonVisitorProfile.getInstance().getCodeNo() != null
					&& SingletonVisitorProfile.getInstance().getCodeNo() > 0) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorAppRegistration
						+ MainConstants.kConstantColon
						+ SingletonVisitorProfile.getInstance().getCodeNo();
			}

			jsonString = jsonString + MainConstants.kConstantComma
					+ MainConstants.jsonVisitorApptoval
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ MainConstants.approval
					+ MainConstants.kConstantSingleQuotes;

			if (SingletonEmployeeProfile.getInstance().getEmployeeName() != null
					&& !SingletonEmployeeProfile.getInstance()
							.getEmployeeName().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeName
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
								.getEmployeeName()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance().getEmployeeId() != null
					&& !SingletonEmployeeProfile.getInstance().getEmployeeId()
							.isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeId
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
								.getEmployeeId()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance().getEmployeeMailId() != null
					&& !SingletonEmployeeProfile.getInstance()
							.getEmployeeMailId().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeEmail
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
								.getEmployeeMailId()
						+ MainConstants.kConstantSingleQuotes;
			}

			Validation validation=new Validation();
			if (SingletonEmployeeProfile.getInstance().getEmployeeMobileNo() != null
					&& !SingletonEmployeeProfile.getInstance()
							.getEmployeeMobileNo().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeePhone
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ validation.getValidMobileNumber(SingletonEmployeeProfile.getInstance()
								.getEmployeeMobileNo().trim())
						+ MainConstants.kConstantSingleQuotes;
			}

				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorisPreApproved
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
						.isVisitorPreApproved()
						+ MainConstants.kConstantSingleQuotes;

			if (SingletonEmployeeProfile.getInstance()
					.getEmployeeSeatingLocation() != null
					&& !SingletonEmployeeProfile.getInstance()
							.getEmployeeSeatingLocation().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeSeatingLocation
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
								.getEmployeeSeatingLocation()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance()
					.getEmployeeExtentionNumber() != null
					&& !SingletonEmployeeProfile.getInstance()
							.getEmployeeExtentionNumber().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeExtension
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
								.getEmployeeExtentionNumber()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonEmployeeProfile.getInstance().getEmployeeZuid() != null
					&& !SingletonEmployeeProfile.getInstance()
							.getEmployeeZuid().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonEmployeeZuid
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonEmployeeProfile.getInstance()
								.getEmployeeZuid()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getDeviceMake() != null
					&& !SingletonVisitorProfile.getInstance().getDeviceMake()
							.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorDeviceMake
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getDeviceMake()
						+ MainConstants.kConstantSingleQuotes;
			}

			if (SingletonVisitorProfile.getInstance().getDeviceSerialNumber() != null
					&& !SingletonVisitorProfile.getInstance()
							.getDeviceSerialNumber().isEmpty()) {
				jsonString = jsonString
						+ MainConstants.kConstantComma
						+ MainConstants.jsonVisitorDeviceSerial
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance()
								.getDeviceSerialNumber()
						+ MainConstants.kConstantSingleQuotes;
			}
			if (SingletonVisitorProfile.getInstance().getOtherDeviceDetails() != null
					&& !SingletonVisitorProfile.getInstance().getOtherDeviceDetails()
					.isEmpty()) {
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorOtherDeviceDetails
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ SingletonVisitorProfile.getInstance().getOtherDeviceDetails()
						+ MainConstants.kConstantSingleQuotes;
			}
			String externalDevice=MainConstants.kConstantEmpty;
			if(booleanExtDeviceOne) {
				externalDevice=externalDevice+getResources().getString(R.string.external_device_one)+MainConstants.kConstantComma;
			}
			if(booleanExtDeviceTwo) {
				externalDevice=externalDevice+getResources().getString(R.string.external_device_two)+MainConstants.kConstantComma;
			}
			if(booleanExtDeviceThree) {
				externalDevice=externalDevice+getResources().getString(R.string.external_device_three)+MainConstants.kConstantComma;
			}
			if(booleanExtDeviceFour) {
				externalDevice=externalDevice+getResources().getString(R.string.external_device_four)+MainConstants.kConstantComma;
			}
			if(booleanExtDeviceFive) {
				externalDevice=externalDevice+getResources().getString(R.string.external_device_five)+MainConstants.kConstantComma;
			}
			if(externalDevice!=null && !externalDevice.isEmpty()){
				jsonString = jsonString + MainConstants.kConstantComma
						+ MainConstants.jsonVisitorExtDevice
						+ MainConstants.kConstantColon
						+ MainConstants.kConstantSingleQuotes
						+ externalDevice
						+ MainConstants.kConstantSingleQuotes;
			}
			jsonString = jsonString + MainConstants.kConstantComma
					+ MainConstants.jsonVisitorExpectedVisitDate
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ StringConstants.creatorExpectedVisitedDate
					+ MainConstants.kConstantSingleQuotes;
			//set app version 
			String appVersion=MainConstants.kConstantEmpty;
			try {
				appVersion = this.getPackageManager().getPackageInfo(
						this.getPackageName(), 0).versionName;
			} catch (NameNotFoundException e) {
				
			}
			if(appVersion!=null) {
			jsonString = jsonString + MainConstants.kConstantComma
					+ MainConstants.jsonAppVersion
					+ MainConstants.kConstantColon
					+ MainConstants.kConstantSingleQuotes
					+ appVersion
					+ MainConstants.kConstantSingleQuotes;
			}
			jsonString = jsonString + MainConstants.kConstantCloseBraces;
		}
		return jsonString;
	}

	/**
	 * Return file upload url
	 * 
	 * @author Karthick
	 * @created on 20161111
	 */
	private String getFileUploadUrl() {

		return MainConstants.kConstantFileUploadUrl
		+ MainConstants.kConstantCreatorAuthTokenAndScope
		+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetail
		+ MainConstants.kConstantFileUploadFieldName
		+ SingletonVisitorProfile.getInstance().getVisitId()
		+ MainConstants.kConstantFileAccessTypeAndName
		+ SingletonVisitorProfile.getInstance().getVisitId()
		+ MainConstants.creatorPhotoName
		+ MainConstants.kConstantImageFileFormat
		+ MainConstants.kConstantFileSharedBy;
	}

	/**
	 * Return file upload url
	 *
	 * @author Karthick
	 * @created on 20161111
	 */
	public String getFileUploadUrlDataCentre() {

		return MainConstants.kConstantFileUploadUrl
				+ MainConstants.kConstantFrontdeskDCAuthTokenAndScope
				+ MainConstants.kConstantFileUploadAppNameAndFormNameAppJsonDetailFrontdeskDC
				+ MainConstants.kConstantFileUploadFieldName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.kConstantFileAccessTypeAndName
				+ SingletonVisitorProfile.getInstance().getVisitId()
				+ MainConstants.creatorPhotoName
				+ MainConstants.kConstantImageFileFormat
				+ MainConstants.kConstantFileSharedByFrontdeskDC;
	}


	/**
	 * Add bitmap to memory cache
	 * 
	 * @author karthick
	 * @created 27/05/2015
	 */
	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}
	
	/**
	 * Get bitmap from memory cache
	 * 
	 * @author karthick
	 * 
	 * @created 27/05/2015
	 */
	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}
	
	/*
	 * Add bitmap to memory cache
	 * 
	 * 
	 * @author karthick
	 * @created 27/05/2015
	 */
	class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

		String zuid;
		Bitmap empPhotoInBitmap;
		
		BitmapWorkerTask(String zuid, Bitmap empPhoto) {
			this.zuid = zuid;
			empPhotoInBitmap = empPhoto;
		}

		// Decode image in background.
		protected Bitmap doInBackground(String... params) {
			if(empPhotoInBitmap!=null) {
			addBitmapToMemoryCache(zuid, empPhotoInBitmap);
			}
			return empPhotoInBitmap;
		}
	}
	
	/**
	 * Set zoom animation for 'Check now' button after validate given details
	 * 
	 * @author Karthick
	 * @created on 20161221
	 */
	private void setZoomAnimation() {
		
		setSingletonValues();
		storeTempIDNumber();
		if ((editTextVisitorName != null)
				&& (editTextVisitorName.getText().toString().trim().length() >= 3)
				&& (validation.validateName(editTextVisitorName.getText()
						.toString().trim()) == true)
				&& (validateImage() == true)
				&& (editTextVisitorContact != null)
				&& (editTextVisitorContact.getText().toString() != MainConstants.kConstantEmpty)
				&& ((validation
						.validateVisitorContact(editTextVisitorContact
								.getText().toString().trim())) == true)
				&& (singleTouchViewGetVisitorSign.isPathNotEmpty()==false)
				&& (SingletonVisitorProfile.getInstance().getTempIDNumber() > 0)
				&& (((textGetEmployeeName.getText() != null)
						&& (!textGetEmployeeName.getText().toString().trim().isEmpty())
						&& (textGetEmployeeName.getText().toString().trim().length() > MainConstants.kConstantZero)
						&& ((employee != null) 
								&& (employee.getEmployeeName() != null) 
								&& (!employee.getEmployeeName().isEmpty()))) 
								|| (textGetEmployeeName!=null 
								&& textGetEmployeeName.getText().toString().trim().isEmpty() ))) {
			if(animationZoom==null) {
			animationZoom = AnimationUtils.loadAnimation(this, R.drawable.zoom_in_out_anim);
			}
			if(animationZoom!=null && buttonVisitorDetailConfirm.getAnimation()==null) {
			buttonVisitorDetailConfirm.startAnimation(animationZoom);
			}
		} else {
			if(animationZoom!=null) {
				animationZoom.cancel();
				buttonVisitorDetailConfirm.clearAnimation();
			}
		}
	}
	
	/**
	 * Launch Barcode Scan
	 * 
	 * @author Karthick
	 * @created on 20161221
	 */
	private void launchBarcodeScan() {
		try {
			 Class.forName(MainConstants.barcodeScanClassName);
			 Intent intent = new Intent(MainConstants.barcodeScanIntent);
			 intent.putExtra(MainConstants.barcodeScanFormatKey, MainConstants.barcodeScanFormat);
			 startActivityForResult(intent, MainConstants.barcodeScanRequestCode);
		} catch(ClassNotFoundException excep) {
			UsedCustomToast.makeCustomToast(this,MainConstants.barcodeScanErrorMsg,Toast.LENGTH_LONG,Gravity.TOP).show();
		} 
	}
	
	/**
	 * Hide keyboard
	 * 
	 * @author Karthick
	 * @created on 20161222
	 */
	private void hideKeyboard() {
	View view = this.getCurrentFocus();
	if (view != null) {  
	    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	}
	
	/**
	 * Hide keyboard
	 * 
	 * @author Karthick
	 * @created on 20170102
	 */
	private void showKeyboard() {
	InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	if (!imm.isActive()){
        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY); 
    }
	}
	
	/**
	 * Set background color
	 * 
	 * @author Karthick
	 * @created on 20170103
	 */
	private void setAppBackground() {
		if( SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getThemeCode()>MainConstants.kConstantZero)
		{
			if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantOne) {
				relativeLayoutVisitorDetail.setBackgroundColor(Color.BLACK);
			}
			else if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantTwo)
			{
			}
			GradientDrawable gd = new GradientDrawable(
					GradientDrawable.Orientation.BOTTOM_TOP,
					new int[] {0xFFfa654b,0xFFfc5d55,0xFFfd575d,0xFFff5066});
			relativeLayoutVisitorDetail.setBackground(gd);
			}

	}
	
	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170405
	 */
	private void textSpeach(int textSpeachCode) {
		
		if(textToSpeech!=null) {
			textToSpeech.stop();
			if (textSpeachCode == MainConstants.kConstantZero && (editTextVisitorName!=null && editTextVisitorName.getText()!=null && editTextVisitorName.getText().toString().length()<=0)) {
				textToSpeech.speak(TextToSpeachConstants.speachName, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.kConstantOne && (editTextVisitorContact!=null && editTextVisitorContact.getText()!=null && editTextVisitorContact.getText().toString().length()<=0)) {
				textToSpeech.speak(TextToSpeachConstants.speachContact, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.kConstantTwo && (textGetEmployeeName!=null && textGetEmployeeName.getText()!=null && textGetEmployeeName.getText().toString().length()<=0)) {
				textToSpeech.speak(TextToSpeachConstants.speachEmployeeName, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.kConstantThree && (editTextTempIdNumber!=null && editTextTempIdNumber.getText()!=null && editTextTempIdNumber.getText().toString().length()<=0)) {
				textToSpeech.speak(TextToSpeachConstants.speachPurpose, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.kConstantFour && (editTextTempIdNumber!=null && editTextTempIdNumber.getText()!=null && editTextTempIdNumber.getText().toString().length()<=0)) {
				textToSpeech.speak(TextToSpeachConstants.speachTempId, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.kConstantFive && (singleTouchViewGetVisitorSign!=null && singleTouchViewGetVisitorSign.isPathNotEmpty())) {
				textToSpeech.speak(TextToSpeachConstants.speachSignature, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.kConstantSix) {
				textToSpeech.speak(TextToSpeachConstants.speachHaveLaptop, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.speachWelcomeMsgCode) {
				textToSpeech.speak(TextToSpeachConstants.speachWelcome, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode == MainConstants.speachAreYouSureCode) {
				textToSpeech.speak(TextToSpeachConstants.speachAreYouSure, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if(textSpeachCode  == MainConstants.kConstantEight) {
				textToSpeech.speak(TextToSpeachConstants.speachCheckInNow, TextToSpeech.QUEUE_FLUSH, null,null);
			} 
		}
	}
	
	/**
	 * Read scan result and set dialog edittext
	 * 
	 * @author Karthick
	 * @created on 20161221
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == MainConstants.barcodeScanRequestCode) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				if (handlerAutoCheckIn != null) {
					handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
					handlerAutoCheckIn.postDelayed(autoCheckInThread,
							MainConstants.autoCheckInTimeDevice);
				}
				//String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				if(editTextSerialNumber!=null && contents!=null && !contents.isEmpty()) {
					editTextSerialNumber.setText(contents);
					editTextSerialNumber.setSelection(contents.length());
				}	
			}
		}
	}
}