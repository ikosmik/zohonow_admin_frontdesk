package com.zoho.app.frontdesk.android;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import constants.NumberConstants;
import constants.StringConstants;
import appschema.SharedPreferenceController;
import utility.UsedBitmap;
import utility.UsedCustomToast;
import backgroundprocess.GetAllRecords;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import constants.MainConstants;
import constants.TextToSpeachConstants;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Application;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class ApplicationController extends Application {

	public static final String TAG = ApplicationController.class
			.getSimpleName();
	private RequestQueue mRequestQueue;
	private static ApplicationController mInstance;
	public static Integer CurrentLocationCode = MainConstants.kConstantMinusOne;
	public static String CurrentOfficeLocation = MainConstants.kConstantEmpty;
	utility.CurrentLocationCode locationCode;
	private TextToSpeech textToSpeech;
	private Bitmap bitmap;
	private SharedPreferenceController sharedPreferenceController;
	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		showToastAppDetails();
		assignObjects();
		locationCode.setCurrentLocationCode();
	}

	private void assignObjects() {
		locationCode = new utility.CurrentLocationCode(getApplicationContext());
		if(textToSpeechListener!=null) {
			textToSpeech = new TextToSpeech(getApplicationContext(), textToSpeechListener);
			}
//			assign ApplicationMode
		assignApplicationMode();
	}

	/**
	 * Set Application Mode by fetching from strings.xml file
	 *
	 * @author Karthikeyan D S
	 * @created on 21Aug2018
 	 */
	private void assignApplicationMode()
	{
		String applicationMode = getResources().getString( R.string.device_mode );
		sharedPreferenceController = new SharedPreferenceController();
		if (sharedPreferenceController!=null ) {
			if (applicationMode != null && !applicationMode.isEmpty()) {
				if(applicationMode.equalsIgnoreCase( StringConstants.kStringConstantsDeviceModeDataCenter)) {
//					if appMode is DataCenter then set value as 2
					sharedPreferenceController.saveApplicationMode(getApplicationContext(), NumberConstants.kConstantDataCentreMode);
					sharedPreferenceController.saveDeviceLocation(getApplicationContext(),MainConstants.purposeDataCenter);
					String applicationSettingsPassword = sharedPreferenceController.getAppSettingsPassword(getApplicationContext());
					if(applicationSettingsPassword!=null && !applicationSettingsPassword.isEmpty())
					{
					}
					else
					{
//						callSettingsPage();
					}
				}
				else if(applicationMode.equalsIgnoreCase(StringConstants.kStringConstantsDeviceModeNormal))
				{
//					if appMode is NormalMode then set value as 1
					sharedPreferenceController.saveApplicationMode(getApplicationContext(), NumberConstants.kConstantNormalMode);
					sharedPreferenceController.saveDeviceLocation(getApplicationContext(),MainConstants.kConstantEmpty);
				}
			} else {
//				if appMode is empty then default AppMode set as 1
				sharedPreferenceController.saveApplicationMode(getApplicationContext(),  NumberConstants.kConstantNormalMode);
				sharedPreferenceController.saveDeviceLocation(getApplicationContext(),MainConstants.kConstantEmpty);
			}
		}
		else
		{
//				if sharedPreference is not initialized then set appMode default set as 1
			sharedPreferenceController.saveApplicationMode(getApplicationContext(),  NumberConstants.kConstantNormalMode);
			sharedPreferenceController.saveDeviceLocation(getApplicationContext(),MainConstants.kConstantEmpty);
		}
//		Log.d("DSK ",""+sharedPreferenceController.getDeviceLocation(getApplicationContext()));
	}

    /**
     * call SettingsPage Activity
     *
     * @author Karthikeyan D S
     * @created by 25OCT2018
     */
	public void callSettingsPage()
	{
		Intent intent = new Intent(this,ActivityStartBackground.class);
		startActivity(intent);
	}

	public static synchronized ApplicationController getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}
		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		// req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	/*
	 * Show toast
	 * 
	 * @Created by karthick
	 * 
	 * @created on 20150824
	 */
	public void showToastAppDetails() {
		String appVersionNameOld = getAppVersionName();
		String appVersionNameNew = StringConstants.AppVersionStart;
		try {
			appVersionNameNew = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			appVersionNameNew = StringConstants.AppVersionStart;
		}

		if (appVersionNameOld == null) {
			appVersionNameOld = "";
		}
		
		if (!appVersionNameOld.equals(appVersionNameNew)) {
			startBackgroundProcess();
			UsedCustomToast.makeText(
					this,
					StringConstants.AppVersionName + appVersionNameNew
							+ StringConstants.AppVersionDescTitle+StringConstants.AppVersionDescValue,
					Toast.LENGTH_LONG).show();
		}
		createAppVersionNameFile(appVersionNameNew);
	}

	/*
	 * Get app version name from local txt file
	 * 
	 * @return : appVersionName
	 * 
	 * @Created by karthick
	 * 
	 * @created on 20150824
	 */
	public String getAppVersionName() {
		String appVersionName = null;
		File internalDateFile = new File(getFilesDir() + File.separator
				+ StringConstants.AppVersionNameFile);
		if (internalDateFile.exists()) {
			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(
						internalDateFile));
			} catch (FileNotFoundException e) {

			}

			String read;
			StringBuilder builder = new StringBuilder(
					MainConstants.kConstantEmpty);
			if (bufferedReader != null) {
				try {
					while ((read = bufferedReader.readLine()) != null) {
						builder.append(read);
					}
					//Log.d("Android", "read" + builder.toString());
					appVersionName = builder.toString();
					bufferedReader.close();
				} catch (IOException e) {
				}
			}
		}
		return appVersionName;
	}

	/*
	 * Write app version name to local txt file
	 * 
	 * @param : appVersionName
	 * 
	 * @Created by karthick
	 * 
	 * @created on 20150824
	 */
	public void createAppVersionNameFile(String appVersionName) {
		File internalDateFile = new File(getFilesDir() + File.separator
				+ StringConstants.AppVersionNameFile);
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					internalDateFile));
			bufferedWriter.write(appVersionName);
			bufferedWriter.close();
		} catch (IOException e) {
		}
	}
	
	/*
	 * Finish the activity
	 * 
	 * Created on 20150820
	 * Created by karthick
	 */
	private void startBackgroundProcess() {
		Intent eventService = new Intent(getApplicationContext(), GetAllRecords.class);
		//Log.d("onstoped", "eventService:"+isServiceRunning());
	if(!isServiceRunning()) {
		getApplicationContext().startService(eventService);
		utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
				StringConstants.kConstantBackgroundStarted,
				Toast.LENGTH_LONG).show();	
	}
	}
	
	/*
	 * Find background service is running
	 * 
	 * Created on 20150820
	 * Created by karthick
	 */
	private boolean isServiceRunning() {
		  ActivityManager manager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
		  for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
		    if (service.service.getClassName().contains("GetAllRecords")) {
		      return true;
		    }
		  }
		  return false;
		}
	
	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170405
	 */
	private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if(textToSpeech!=null && status != TextToSpeech.ERROR) {
            		textToSpeech.setLanguage(Locale.US);
            }
        }
    };
    
    /**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170405
	 */
	public void textSpeach(int textSpeachCode) {
		if(textToSpeech!=null) {
			textToSpeech.stop();
			if (textSpeachCode == MainConstants.kConstantZero) {
				textToSpeech.speak(TextToSpeachConstants.speachName, TextToSpeech.QUEUE_FLUSH, null,null);
			} else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
				textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
						TextToSpeech.QUEUE_FLUSH, null, null);
			}
		}
	}

	/**
	 * Play text to voice
	 *
	 * @author Karthick
	 * @created on 20170405
	 */
	public void textSpeach(String textSpeach) {
		if(textToSpeech!=null && textSpeach!=null) {
			textToSpeech.stop();
			textToSpeech.speak(textSpeach,
						TextToSpeech.QUEUE_FLUSH, null, null);
		}
	}

	/**
	 * Play text to voice
	 *
	 * @author Karthick
	 * @created on 20170405
	 */
	public void stopTextSpeech() {
		if(textToSpeech!=null) {
			textToSpeech.stop();
		}
	}

	/**
	 * Check the app permission
	 *
	 * @author Karthick
	 * @created 20170208
	 */
	public boolean checkExternalPermission() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

			if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
					== PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
					== PackageManager.PERMISSION_GRANTED) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Get noise bitmap
	 *
	 * @author Karthick
	 * @created on 20170822
	 */
	public Bitmap getNoiseBitmap() {
		if(bitmap==null) {
//			Log.d("getNoiseBitmap","getNoiseBitmap");
			UsedBitmap usedBitmap = new UsedBitmap();
			bitmap = Bitmap.createBitmap(300, 480, Bitmap.Config.ARGB_8888);
			bitmap = usedBitmap.applyNoiceEffect(bitmap);
			usedBitmap=null;
		}
		return bitmap;
	}

	/**
	 * Clear noise bitmap
	 *
	 * @author Karthick
	 * @created on 20170822
	 */
	public void clearNoiseBitmap() {
		if(bitmap!=null) {
			bitmap.recycle();
			bitmap=null;
		}
	}
}