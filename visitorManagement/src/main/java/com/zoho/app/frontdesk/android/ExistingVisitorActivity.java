package com.zoho.app.frontdesk.android;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import appschema.SharedPreferenceController;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import database.dbhelper.DbHelper;
import database.dbschema.EmployeeVisitorPreApproval;
import utility.DialogViewWarning;
import utility.NetworkConnection;
import utility.UsedCustomToast;
import utility.Validation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import appschema.ExistingVisitorDetails;
import zohocreator.VisitorZohoCreator;

/**
 * Check the visitor is existing person or not if visitor detail was already
 * entered then get that detail and process it to the existing visitor. Else its
 * look like manual entry to get visitor detail.
 *
 * @author Karthick
 * @created on 20170508
 */
public class ExistingVisitorActivity extends Activity {
    private DialogViewWarning dialog;
    private EditText edittextContact;
    private ProgressBar progressBarLoading;
    private RelativeLayout relativeLayoutExisitingVisitor;
    private ImageView imageviewBack, imageviewHome, imageviewNext, image_view_next;
    private TextView textViewErrorMessage, textviewVisitorContactTitle,
            textviewVisitorContactMandatory, textViewPhoneNumberExtension;
    private DbHelper dbHelper;
    private Typeface typeFaceTitle, typefaceVisitorContact, typefaceErrorMsg;
    private int actionCode = MainConstants.backPress;
    private Handler noActionHandler;
    private TextToSpeech textToSpeech;
    private BackgroundProcess bgProcess;
    private int applicationMode =  NumberConstants.kConstantZero;
    private VisitorZohoCreator creator = null;
    private NetworkConnection networkConnection;
    private FrameLayout frameLayout;
    private CameraPreview preview;
    private int rotation, faceDetectionCount;
    private SharedPreferenceController spController;
    private boolean TimerFinished = false, isOneTimeSmilePlay = false,
            isOneTimeNoFacePlay = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_existing_visitor);

        assignObjects();
        setOldValue();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Assign Objects to the variable
     *
     * @author Karthick
     * @created on 20170508
     */
    private void assignObjects() {
        progressBarLoading = (ProgressBar) findViewById(R.id.progressbar_report);
        edittextContact = (EditText) findViewById(R.id.edittext_visitor_contact);
        textViewErrorMessage = (TextView) findViewById(R.id.textview_error_contact);
        textviewVisitorContactTitle = (TextView) findViewById(R.id.textview_visitor_contact_title);
        textviewVisitorContactMandatory = (TextView) findViewById(R.id.textview_mandatory);

        textViewPhoneNumberExtension = (TextView) findViewById(R.id.text_view_phone_number_extension);

        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

        relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);

//        Initialize Camera Preview Layout for device Density >=2 devices to avoid black screen issue.
        float density = getResources().getDisplayMetrics().density;
        if(density>=2) {
            preview = new CameraPreview(this, null);
            frameLayout = (FrameLayout) findViewById(R.id.framePreview);
            frameLayout.addView(preview);
        }

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        assignApplicationModeDetails();

        assignFontConstants();

        assignListeners();

        setAppBackground();

//        if application mode is Datacentre then Check For Printer to Print ID Card for DataCentre Visitor's.
//        if(applicationMode==2) {
//            if (SingletonCode.getSingletonWifiPrinterIP() == null
//                    || SingletonCode.getSingletonWifiPrinterIP().toString()
//                    .length() <= MainConstants.kConstantZero) {
//              UsedCustomToast.makeCustomToast(getApplicationContext(),"Printer Not Connected",Toast.LENGTH_SHORT,Gravity.TOP).show();
//            }
//        }
    }

    /**
     * set editText maxLength
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    public void setEditTextMaxLength(EditText editText, int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        editText.setFilters(FilterArray);
    }

    /**
     * Assign Application Mode Based on that hide and show fields.
     *
     * @author Karthikeyan D S
     * @created on 24JUL2018
     */
    public void assignApplicationModeDetails() {
        if (SingletonMetaData.getInstance() != null) {
            applicationMode = SingletonMetaData.getInstance().getApplicationMode();
        }
        if (applicationMode == NumberConstants.kConstantNormalMode) {
            edittextContact.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            textViewPhoneNumberExtension.setVisibility(View.GONE);
        } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
//            Set Phone Number TextView align to centre based on the PhoneNumber based on Datacentre Mode.
            // Get the TextView current LayoutParams
            LinearLayout.LayoutParams textviewVisitorPhoneNumber = (LinearLayout.LayoutParams) textviewVisitorContactTitle.getLayoutParams();
            // Set TextView layout margin 25 pixels to Left side
            // Left Top Right Bottom Margin
            textviewVisitorPhoneNumber.setMargins(20,20,20,20);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) edittextContact.getLayoutParams();
            Display display = getWindowManager().getDefaultDisplay();
            int screenWidth = display.getWidth();

            params.width = screenWidth / 2;
            edittextContact.setLayoutParams(params);

            edittextContact.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
            // Apply the updated layout parameters to TextView
            textviewVisitorContactTitle.setLayoutParams(textviewVisitorPhoneNumber);

            textViewPhoneNumberExtension.setVisibility(View.VISIBLE);
            textviewVisitorContactTitle.setText(StringConstants.kConstantVisitorPhoneNumberText);
            edittextContact.setInputType(InputType.TYPE_CLASS_PHONE);
            setEditTextMaxLength(edittextContact, 10);

            textViewErrorMessage.setText(StringConstants.kConstantVisitorErrorPhoneNumber);
            textViewErrorMessage.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Assign Listeners
     *
     * @author Karthikeyan D S
     * @created on 24JUL2018
     */
    private void assignListeners() {
        // Onclick listeners
        if (image_view_next != null && listenerConfirm != null) {
            image_view_next.setOnClickListener(listenerConfirm);
        }
        if (imageviewNext != null && listenerConfirm != null) {
            imageviewNext.setOnClickListener(listenerConfirm);
        }
        if (imageviewHome != null && listenerHomePage != null) {
            imageviewHome.setOnClickListener(listenerHomePage);
        }
        if (imageviewBack != null && listenerBack != null) {
            imageviewBack.setOnClickListener(listenerBack);
        }
        edittextContact.setOnEditorActionListener(listenerDonePressed);
        edittextContact.addTextChangedListener(textWatcherContactChanged);
    }

    /**
     * Assign Font Constants
     *
     * @author Karthikeyan D S
     * @created on 24JUL2018
     */
    private void assignFontConstants() {
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceErrorMsg = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskItalic);

        typefaceVisitorContact = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);

        if (edittextContact != null && typefaceVisitorContact != null) {
            edittextContact.setTypeface(typefaceVisitorContact);
        }
        if (textViewErrorMessage != null && typefaceErrorMsg != null) {
            textViewErrorMessage.setTypeface(typefaceErrorMsg);
        }
        if (textviewVisitorContactTitle != null && typeFaceTitle != null) {
            textviewVisitorContactTitle.setTypeface(typeFaceTitle);
        }
        if (textviewVisitorContactMandatory != null && typeFaceTitle != null) {
            textviewVisitorContactMandatory.setTypeface(typeFaceTitle);
        }
        if (textViewPhoneNumberExtension != null && typeFaceTitle != null) {
            textViewPhoneNumberExtension.setTypeface(typeFaceTitle);
        }
    }

    /**
     * Set background color
     *
     * @author Karthickeyan D S
     * @created on 20170508
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFba6fb6, 0xFF8e76c3, 0xFF8478c6, 0xFF8278c7});
                relativeLayoutExisitingVisitor.setBackground(gd);
            }
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerBack = new OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            hideKeyboard();
            showDetailDialog();
        }
    };

    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerHomePage = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.homePress;
            hideKeyboard();
            showDetailDialog();
        }
    };

    /**
     * when the done pressed then process start.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                if (validateContact()) {
                    isExistingVisitor();
                }
                return true;
            }
            return false;
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerConfirm = new OnClickListener() {
        public void onClick(View v) {
            if (validateContact()) {
                isExistingVisitor();
            }
        }
    };

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchHomePage() {
        if(SingletonVisitorProfile.getInstance().getOfficeLocation().equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia))
        {
           Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        }
        else if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        }
        else
            {
            Intent intent = new Intent(this, HomePage.class);
            startActivity(intent);
        }
        finishAffinity();
    }
    /**
     * getDeviceLocation from SharedPreferenceController
     * @return deviceLocation
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private String getDeviceLocation()
    {
        String deviceLocation = MainConstants.kConstantEmpty;
        if(spController==null)
        {
            spController = new SharedPreferenceController();
        }
        deviceLocation = spController.getDeviceLocation(this);
        return deviceLocation;
    }
    /**
     * Launch Welcome activity
     *
     * @author Karthikeyan D S
     * @created on 2018/06/02
     */
    private void launchWelcomePage() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    /**
     * Launch next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchVisitorDetailPage() {
        Intent intent = new Intent(getApplicationContext(),
                ActivityExistingVisitorDetails.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchNamePage() {
        Intent intent = new Intent(getApplicationContext(),
                ActivityVisitorName.class);
        intent.putExtra(MainConstants.putExtraFromPage, MainConstants.fromContact);
        startActivity(intent);
        finish();
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthick
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };

    /**
     * This function is called when the back pressed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        showDetailDialog();
    }

    /**
     * textWatcher to validate the VisitorMobileNo
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextWatcher textWatcherContactChanged = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            validateContactTyped();
            if (SingletonVisitorProfile.getInstance() != null) {
                SingletonVisitorProfile.getInstance().clearInstance();
            }
            if (SingletonEmployeeProfile.getInstance() != null) {
                SingletonEmployeeProfile.getInstance().clearInstance();
            }
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
        }

        public void afterTextChanged(Editable s) {
        }

    };

    /**
     * Check is existing user or not
     *
     * @author Karthick
     * @created 20170508
     */
    private void isExistingVisitor() {
        if (dbHelper == null) {
            dbHelper = DbHelper.getInstance(getApplicationContext());
        }
        String contact = MainConstants.kConstantEmpty;
        if (edittextContact != null && edittextContact.getText() != null) {
            contact = edittextContact.getText().toString().trim();
        }
        if (contact != null && !contact.isEmpty()) {
            if(applicationMode==NumberConstants.kConstantNormalMode)
            {
                bgProcess = new BackgroundProcess(this, contact);
                bgProcess.execute();
            }
            else if(applicationMode==NumberConstants.kConstantDataCentreMode)
            {
                if (progressBarLoading != null) {
                    progressBarLoading.setVisibility(View.VISIBLE);
                }
                networkConnection = new NetworkConnection();
                if(networkConnection.isWiFiEnabled(this))
                {
                    bgProcess = new BackgroundProcess(this, contact);
                    bgProcess.execute();
                }
                else
                {
                    if (progressBarLoading != null) {
                        progressBarLoading.setVisibility(View.GONE);
                    }
                    UsedCustomToast.makeCustomToast(ExistingVisitorActivity.this,"Network Not Available Please Connect to the Network",Toast.LENGTH_SHORT,Gravity.TOP).show();
                }
            }
        }
    }

    /**
     * Validate Contact when typed
     *
     * @author Karthick
     * @created 20170508
     */
    private void validateContactTyped() {
        String contact = MainConstants.kConstantEmpty;
        if (edittextContact != null && edittextContact.getText() != null) {
            contact = edittextContact.getText().toString().trim();
        }
        if ((contact != null)
                && (contact.length() > MainConstants.kConstantZero)) {

            if (applicationMode == NumberConstants.kConstantNormalMode) {
                if (contact.length() >= MainConstants.kConstantSix) {
                    if (imageviewNext != null) {
                        imageviewNext.setVisibility(View.VISIBLE);
                    }
                    if (image_view_next != null) {
                        image_view_next.setVisibility(View.VISIBLE);
                    }
                }
            } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                if (contact.length() == MainConstants.kConstantTen) {
                    if (imageviewNext != null) {
                        imageviewNext.setVisibility(View.VISIBLE);
                    }
                    if (image_view_next != null) {
                        image_view_next.setVisibility(View.VISIBLE);
                    }
                } else if (contact.length() > MainConstants.kConstantTen || contact.length() < MainConstants.kConstantTen) {
                    if (imageviewNext != null) {
                        imageviewNext.setVisibility(View.GONE);
                    }
                    if (image_view_next != null) {
                        image_view_next.setVisibility(View.GONE);
                    }
                    textViewErrorMessage.setText("Seem's Not a Valid Number");
                }
            }
            String checkMailOrMobileNo = String.valueOf(contact
                    .charAt(MainConstants.kConstantZero));
            if (checkMailOrMobileNo
                    .matches(MainConstants.kConstantValidMailOrMobileNo)) {
                validateMailText();
            } else {
                validateMobileNoText();
            }
        } else {
            if (textViewErrorMessage != null) {
                textViewErrorMessage.setVisibility(View.INVISIBLE);
            }

        }
    }

    /**
     * Validate Contact
     *
     * @author Karthick
     * @created 20170508
     */
    private boolean validateContact() {

        Validation validation = new Validation();
        String contact = MainConstants.kConstantEmpty;
        if (edittextContact != null && edittextContact.getText() != null) {
            contact = edittextContact.getText().toString().trim();
        }

        if (textViewErrorMessage != null) {
            textViewErrorMessage.setVisibility(View.INVISIBLE);
        }

        if (applicationMode == NumberConstants.kConstantNormalMode) {
            if ((contact != null && !contact.isEmpty())
                    && ((validation.validateVisitorContact(contact)) == true)) {
                return true;
            } else if (contact.isEmpty()) {
                if (textViewErrorMessage != null) {
                    textViewErrorMessage.setText(StringConstants.kConstantErrorMsgMandatory);
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                }
                return false;
            } else if (contact != null && (!contact.isEmpty())) {
                if (contact.matches("[0-9]+")) {
                    if (!validation.validateMobileNo(contact)) {
                        textViewErrorMessage
                                .setText(StringConstants.kConstantErrorMsgToCheckPhoneNo);
                    }
                } else {
                    if (!validation.validateMail(contact)) {
                        textViewErrorMessage
                                .setText(StringConstants.kConstantErrorMsgToValidMailId);
                    }
                }
            }
        } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
            if ((contact != null && !contact.isEmpty())
                    && ((validation.validateDataCenterPhoneNumber(contact)) == true)) {
                return true;
            } else if (contact.isEmpty()) {
                if (textViewErrorMessage != null) {
                    textViewErrorMessage.setText(StringConstants.kConstantErrorMsgPhoneNumberMandatory);
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                }
                return false;
            } else if (contact != null && (!contact.isEmpty())) {
                if (contact.matches("[0-9]+")) {
                    if (!validation.validateIndiaMobileNo(contact)) {
                        textViewErrorMessage
                                .setText(StringConstants.kConstantErrorMsgToCheckPhoneNo);
                        textViewErrorMessage.setVisibility(View.VISIBLE);
                    }
                } else {

                }
            }
        }
        textViewErrorMessage.setVisibility(View.VISIBLE);
        return false;
    }

    /**
     * Validate the phone
     *
     * @author Karthick
     * @created on 20170508
     */
    private void validateMobileNoText() {
        if (applicationMode == NumberConstants.kConstantNormalMode) {
            if ((edittextContact != null)
                    && (edittextContact.getText().toString().trim().length() > MainConstants.kConstantZero)
                    && ((edittextContact.getText().toString().trim()
                    .matches(MainConstants.kConstantValidPhoneNoText)) == false)) {
                textViewErrorMessage.setVisibility(View.VISIBLE);
                textViewErrorMessage
                        .setText(StringConstants.kConstantErrorMsgValidPhoneNo);
            } else {
                edittextContact.setTextColor(Color
                        .parseColor(MainConstants.color_code_efefef));
                textViewErrorMessage.setVisibility(View.INVISIBLE);
            }
        } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
            if ((edittextContact != null)
                    && (edittextContact.getText().toString().trim().length() > MainConstants.kConstantZero)
                    && ((edittextContact.getText().toString().trim().matches(MainConstants.kConstantValidIndianPhoneNoText)) == false)) {
                if (edittextContact.getText().toString().trim().length() > MainConstants.kConstantThirteen) {
                    imageviewNext.setVisibility(View.GONE);
                    image_view_next.setVisibility(View.GONE);
                    textViewErrorMessage.setText("Seem's Not a Valid Number");
                } else if (edittextContact.getText().toString().trim().length() < MainConstants.kConstantTen) {
//					textViewErrorMessage.setVisibility(View.VISIBLE);
//					textViewErrorMessage
//							.setText(MainConstants.kConstantErrorMsgValidIndianPhoneNo);
                } else if (edittextContact.getText().toString().trim().length() == MainConstants.kConstantTen || edittextContact.getText().toString().trim().length() == MainConstants.kConstantThirteen) {
                    textViewErrorMessage.setVisibility(View.GONE);
                } else if (edittextContact.getText().toString().trim().length() < 1) {
                    textViewErrorMessage.setText("Format should be 8526603722, Indian Number's only allowed");
                    textViewErrorMessage.setVisibility(View.VISIBLE);
                }
            } else {
                edittextContact.setTextColor(Color
                        .parseColor(MainConstants.color_code_efefef));
                textViewErrorMessage.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Validate the mailId
     *
     * @author Karthick
     * @created on 20170508
     */
    private void validateMailText() {
        if ((edittextContact != null)
                && (edittextContact.getText().toString().trim().length() > MainConstants.kConstantZero)
                && ((edittextContact.getText().toString().trim()
                .matches(MainConstants.kConstantValidEmailText)) == false)) {
            textViewErrorMessage.setVisibility(View.VISIBLE);
            textViewErrorMessage
                    .setText(StringConstants.kConstantErrorMsgValidMailText);
        } else {
            edittextContact.setTextColor(Color
                    .parseColor(MainConstants.color_code_efefef));
            textViewErrorMessage.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Hide keyboard
     *
     * @author Karthick
     * @created on 20170508
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showDetailDialog() {
        if (((edittextContact != null) && (SingletonVisitorProfile
                .getInstance().getVisitorName().length() > MainConstants.kConstantZero))
                || (SingletonVisitorProfile.getInstance().getVisitorSign().length > MainConstants.kConstantZero)
                || ((edittextContact != null)
                && (edittextContact.getText().toString().trim() != MainConstants.kConstantEmpty) && (edittextContact
                .getText().toString().trim().length() > MainConstants.kConstantZero))
                || ((SingletonVisitorProfile.getInstance()
                .getPurposeOfVisitCode().length() > MainConstants.kConstantZero))
                || ((SingletonVisitorProfile.getInstance().getTempIDNumber() > MainConstants.kConstantZero))
                || (SingletonVisitorProfile.getInstance()
                .getDeviceSerialNumber() != null
                && !SingletonVisitorProfile.getInstance()
                .getDeviceSerialNumber().trim().isEmpty()
                && SingletonVisitorProfile.getInstance()
                .getDeviceMake() != null && !SingletonVisitorProfile
                .getInstance().getDeviceMake().trim().isEmpty())) {
            showWarningDialog();
            textSpeach(MainConstants.speachAreYouSureCode);
        } else {
            if (applicationMode == NumberConstants.kConstantNormalMode) {
                launchHomePage();
            } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                launchWelcomePage();
            }
        }
    }

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showWarningDialog() {
        hideKeyboard();
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    /**
     * Go to Action Choose Activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            if (applicationMode == NumberConstants.kConstantNormalMode) {
                launchHomePage();
            } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                launchWelcomePage();
            }
        }
    };

    /**
     * Validate the phone
     *
     * @author Karthick
     * @created on 20170508
     */
    private void setOldValue() {
        if (edittextContact != null && SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getContact() != null
                && !SingletonVisitorProfile.getInstance().getContact().trim().isEmpty()) {
            edittextContact.removeTextChangedListener(textWatcherContactChanged);
            String phoneNumber = SingletonVisitorProfile.getInstance().getContact().trim().replaceFirst(Pattern.quote("+91"),"");
//            Log.d("DSK"," "+phoneNumber+" "+SingletonVisitorProfile.getInstance().getContact());
            edittextContact.setText(phoneNumber);
//            edittextContact.setSelection(SingletonVisitorProfile.getInstance().getContact().trim().length());
            edittextContact.addTextChangedListener(textWatcherContactChanged);
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.VISIBLE);
            }
            if (image_view_next != null) {
                image_view_next.setVisibility(View.VISIBLE);
            }
        } else {
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.INVISIBLE);
            }
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {
        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (edittextContact != null
                    && edittextContact.getText() != null && edittextContact
                    .getText().toString().length() <= 0)) {
                if (applicationMode == NumberConstants.kConstantNormalMode) {
                    textToSpeech.speak(TextToSpeachConstants.speachContact,
                            TextToSpeech.QUEUE_FLUSH, null, null);
                } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                    textToSpeech.speak(TextToSpeachConstants.speachContactDatacenter,
                            TextToSpeech.QUEUE_FLUSH, null, null);
                }
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void deallocateObjects() {
        edittextContact = null;
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        image_view_next = null;
        textViewErrorMessage = null;
        textviewVisitorContactTitle = null;
        textviewVisitorContactMandatory = null;
        dbHelper = null;
        typeFaceTitle = null;
        typefaceVisitorContact = null;
        relativeLayoutExisitingVisitor
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutExisitingVisitor = null;
        applicationMode=  NumberConstants.kConstantZero;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (bgProcess != null) {
            bgProcess.cancel(true);
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
        }

        if(frameLayout!=null) {
            frameLayout = null;
        }
        if (preview != null) {
            preview.stopFaceDetectionListener();
            preview = null;
        }
    }


    /*
     * background process for upload app settings details
     */
    public class BackgroundProcess extends AsyncTask<String, String, String> {
        Context context;
        ExistingVisitorDetails existingVisitorDetails = new ExistingVisitorDetails();
        EmployeeVisitorPreApproval employeeVisitorPreApproval = new EmployeeVisitorPreApproval();
        String contact;
        private int isOTPSend = 0;

        public BackgroundProcess(Context context, String contact) {
            this.context = context;
            this.contact = contact;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            enableViews(false);
        }

        protected String doInBackground(String... f_url) {
            if (dbHelper != null)
            {
//				Log.d("dsk","contact "+contact+ " ApplicationMode "+ SingletonMetaData.getInstance().getApplicationMode() );
                String visitPurpose = MainConstants.purposeDataCenter;
                if (SingletonMetaData.getInstance() != null
                        && SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantNormalMode) {
                    existingVisitorDetails = dbHelper.searchExistVisitor( this.contact, visitPurpose);
//                    Log.d("DSK "," existingVisitorDetails1 "+existingVisitorDetails.getEmployeeEmail());
                } else if (SingletonMetaData.getInstance() != null
                        && SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantDataCentreMode) {
                    long mDateOfVisitToday = getTodayLongValue();
                    if (this.contact != null && !this.contact.isEmpty() && mDateOfVisitToday > 0) {
                        employeeVisitorPreApproval = new EmployeeVisitorPreApproval();
                        employeeVisitorPreApproval = dbHelper.searchEmployeeVisitorPreApprovalRecords(this.contact, mDateOfVisitToday);
                        if (employeeVisitorPreApproval == null) {
                            existingVisitorDetails = dbHelper.searchExistVisitor( this.contact, visitPurpose);
                        }
                        if (sendOTPDetailstoCreator(contact)) {
//                               OTP Request Sent.
                        } else {
//                            Error OTP Request Not Sent to Creator.
                        }
                    }
//				Log.d("dsk","existingVisitorDetails "+contact);
                }
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {

        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String responseParam) {
            progressBarLoading.setVisibility(View.GONE);
            enableViews(true);
            if (existingVisitorDetails != null
                    && existingVisitorDetails.getName() != null && !existingVisitorDetails.getName().isEmpty()
                    && existingVisitorDetails.getContact() != null && !existingVisitorDetails.getContact().isEmpty()
                    && existingVisitorDetails.getPurpose() != null && !existingVisitorDetails.getPurpose().isEmpty()
                    && existingVisitorDetails.getCreatorVisitID() != null && !existingVisitorDetails.getCreatorVisitID().isEmpty()) {
                launchVisitorDetailPage();
            } else if (employeeVisitorPreApproval != null && employeeVisitorPreApproval.getmVisitorName() != null && !employeeVisitorPreApproval.getmVisitorName().isEmpty()
                    && employeeVisitorPreApproval.getmPhoneNumber() != null && !employeeVisitorPreApproval.getmPhoneNumber().isEmpty()
                    && employeeVisitorPreApproval.isVisitorActive()) {
                launchVisitorDetailPage();
            } else if (SingletonMetaData.getInstance() != null
                    && SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantNormalMode) {
                launchNamePage();
            } else if (SingletonMetaData.getInstance() != null
                    && SingletonMetaData.getInstance().getApplicationMode() == NumberConstants.kConstantDataCentreMode) {
                launchOTPDetailsPage();
            }
        }
    }

    private void enableViews(boolean setView)
    {
        if (edittextContact != null) {
            edittextContact.setEnabled(setView);
        }
        if (imageviewBack != null) {
            imageviewBack.setEnabled(setView);
        }
        if (imageviewHome != null) {
            imageviewHome.setEnabled(setView);
        }
        if (imageviewNext != null) {
            imageviewNext.setEnabled(setView);
        }
        if (image_view_next != null) {
            image_view_next.setEnabled(setView);
        }
    }

    /**
     * get Today's date long value
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    private long getTodayLongValue() {
        long getVisitorDateLong = NumberConstants.kConstantMinusOne;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MainConstants.kConstantDataCentreDateFormat);
        Date date = Calendar.getInstance().getTime();
        long joinDate = NumberConstants.kConstantMinusOne;
        String strDate = simpleDateFormat.format(date);
        try {
            date = simpleDateFormat.parse(strDate);
            joinDate = date.getTime();
//             Log.d("DSK",""+joinDate);
            return joinDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getVisitorDateLong;
    }

    /**
     * launch OTP details Activity.
     *
     * @author Karthikeyan D S
     * @created on 26JUL2018
     */
    private void launchOTPDetailsPage() {
        Intent otpDetailsActivity = new Intent(this, ActivityOTPDetails.class);
        startActivity(otpDetailsActivity);
        finish();
    }

    /**
     * send OTP Request to Creator.
     *
     * @author Karthikeyan D S
     * @created on 26JUL2018
     */
    private boolean sendOTPDetailstoCreator(String mobileNumber) {
        boolean sendOTPDetailstoCreator = false;
        String mCreatorID = MainConstants.kConstantEmpty;

        if (mobileNumber != null && !mobileNumber.isEmpty()) {
            creator = new VisitorZohoCreator(this);
            if(creator!=null) {
                mCreatorID = creator.sendOTPDetailsCreator(this,mobileNumber);
            }
            if(mCreatorID!=null && !mCreatorID.isEmpty()){
                if(SingletonVisitorProfile.getInstance()!=null) {
                    SingletonVisitorProfile.getInstance().setOTPRequestCreatorID(mCreatorID);
                }
                sendOTPDetailstoCreator = true;
            }
            else
            {
                sendOTPDetailstoCreator =false;
            }
        } else {
//            Mobile Number is Empty.
            sendOTPDetailstoCreator = false;
        }
        return sendOTPDetailstoCreator;
    }
}
