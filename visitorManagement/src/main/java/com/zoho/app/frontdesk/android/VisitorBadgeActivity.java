package com.zoho.app.frontdesk.android;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import appschema.SingletonCode;
import appschema.SingletonVisitorProfile;
import constants.StringConstants;
import utility.DialogView;
import utility.UsedBitmap;
import zohocreator.VisitorZohoCreator;

import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.print.PrintHelper;

import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * 
 * @author jayapriya on 14/11/2014 Set the visitorBadgeDetails and control the
 *         email, printer, bluetooth and wifi.
 * 
 */
public class VisitorBadgeActivity extends Activity {

	ImageView  imageviewMailMapTouch,imageViewEmail, imageViewHomePage, imageViewQrCode, imageViewPrint, imageViewEmailMap;// imageViewBluetooth;
    Dialog dialogGetHomeMessage;
	String visitorBadgeSourceFile;
	String sourceFile;
	VisitorBadge visitorBadge;
	private Handler handlerHideDialog;
    Typeface typefacedialog;
    String[] date;
    String dateWithOfficeLocation;
	TextView textViewEmailThis, //textViewShareWifi,//textViewShareBlueTooth,
			textViewPrintThis,
            textViewGetDate, textViewGetVisitorName,
			textViewWifiMessage, //textViewHelp,
			textViewStuck,
			textViewWifiMessageSecond, //textViewHelp,
			textViewStuckSecond,
            textViewDialogHomeMessage,textViewHomeMessageConfirm,textViewHomeMessageCancel,
			textViewVisitorBadgeTitle, textViewVisitorBadgeMessage;
    RelativeLayout relativeLayoutMail,relativeLayoutPrint,relativeLayoutVisitorBadge;
    VisitorZohoCreator creator = new VisitorZohoCreator(this);
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set the orientation as Portrait
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		setContentView(R.layout.activity_visitor_badge);
		 DialogView.showReportDialog(this,MainConstants.badgeDialog,MainConstants.forgotIDUploadCode);
		 new backgroundProcess().execute("");
	}

	/*
	 * Created by jayapriya On 17/11/2014 when the activity started then call this
	 * function.
	 */
	public void onStart() {
		super.onStart();
		 //Log.d("window","onStart");
		assignObjects();
	}

	public void onResume() {
		super.onResume();
		
		if(SingletonCode.getSingletonTellUsCode()==MainConstants.tellUsCode) {
			
			Intent nextActivityIntent = new Intent(this, WelcomeActivity.class);
			startActivity(nextActivityIntent);
			finish();	
			SingletonCode.setSingletonTellUsCode(MainConstants.kConstantMinusOne);
		}
	}
	/**
	 * Created by jayapriya On 24/11/2014
	 * 
	 */
	public void onStop() {
		super.onStop();
		 //Log.d("window","onStop");
		deallocateObjects();
	}

	/*
	 * Created by Jayapriya On 24/11/2014 deallocate the objects in the variable
	 */
	private void deallocateObjects() {

        imageViewQrCode = null;
		imageViewEmail = null;
		imageViewHomePage = null;
        //imageViewBluetooth =null;
		textViewVisitorBadgeTitle = null;
		visitorBadgeSourceFile = null;
		sourceFile = null;
		visitorBadge = null;
        dialogGetHomeMessage =null;
        visitorBadge =null;
		textViewEmailThis = null;
		//textViewShareWifi = null;
		
		//textViewShareBlueTooth = null;
		textViewPrintThis = null;
		textViewGetDate = null;
		textViewGetVisitorName = null;
        textViewVisitorBadgeMessage = null;
        textViewWifiMessage=null;
        textViewWifiMessageSecond=null;
		textViewStuckSecond=null;
       // textViewHelp=null;
        textViewStuck=null;
        textViewDialogHomeMessage=null;
        textViewHomeMessageConfirm=null;
        textViewHomeMessageCancel=null;
        textViewVisitorBadgeMessage=null;
        relativeLayoutVisitorBadge.setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutVisitorBadge=null;
	}

	/*
	 * Created by jayapriya On 17/11/2014 when the activity started then call this
	 * function to assign the values.
	 */

	private void assignObjects() {
		
		handlerHideDialog = new Handler();
		textViewEmailThis = (TextView) findViewById(R.id.textViewEmail);
		textViewPrintThis = (TextView) findViewById(R.id.textViewPrint);
		//textViewShareBlueTooth = (TextView) findViewById(R.id.textViewShareBluetooth);
		//textViewShareWifi = (TextView) findViewById(R.id.textViewShareWifi);
		textViewVisitorBadgeTitle = (TextView) findViewById(R.id.textViewVisitorBadgeTitle);
		textViewVisitorBadgeMessage = (TextView) findViewById(R.id.textViewVisitorBadgeMessage);
		textViewWifiMessage = (TextView) findViewById(R.id.textViewWifiMessage);
		textViewWifiMessageSecond = (TextView) findViewById(R.id.textViewWifiMessageSecond);
		textViewStuckSecond = (TextView) findViewById(R.id.textViewStuckSecond);

		//textViewHelp = (TextView) findViewById(R.id.textViewHelp);
		textViewStuck = (TextView) findViewById(R.id.textViewStuck);
		imageViewEmailMap=(ImageView) findViewById(R.id.imageViewMailMap);
		imageviewMailMapTouch=(ImageView) findViewById(R.id.imageviewMailMapTouch);
		visitorBadge = (VisitorBadge) findViewById(R.id.visitorbadge);
		imageViewEmail = (ImageView) findViewById(R.id.imageViewEmail);
		imageViewHomePage = (ImageView) findViewById(R.id.imageViewHomePage);
        //imageViewBluetooth =(ImageView) findViewById(R.id.imageViewBluetooth);
        imageViewPrint = (ImageView) findViewById(R.id.imageViewPrint);
        relativeLayoutPrint =(RelativeLayout) findViewById(R.id.relativeLayoutPrint);
        relativeLayoutMail =(RelativeLayout) findViewById(R.id.relativeLayoutMail);
        relativeLayoutVisitorBadge=(RelativeLayout) findViewById(R.id.relativeLayoutVisitorBadge);
        // set Font
        Typeface typeFaceMessage = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantJosefinSansReg);
        textViewVisitorBadgeMessage.setTypeface(typeFaceMessage);
		Typeface typeFaceTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantJosefinSans);
		textViewVisitorBadgeTitle.setTypeface(typeFaceTitle);
		
		//textViewShareBlueTooth.setTypeface(typeFaceTitle);
		//textViewShareWifi.setTypeface(typeFaceTitle);
        typefacedialog = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
		textViewWifiMessage.setTypeface(typeFaceTitle);
		//textViewHelp.setTypeface(typeFaceTitle);
		textViewStuck.setTypeface(typeFaceTitle);
		
		Typeface typefaceBottomText = Typeface.createFromAsset(getAssets(),
	                FontConstants.fontConstantLatoReg);
		textViewWifiMessageSecond.setTypeface(typefaceBottomText);
		textViewStuckSecond.setTypeface(typefaceBottomText);
		Typeface typefaceMailText = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
		textViewEmailThis.setTypeface(typefaceMailText);
		textViewPrintThis.setTypeface(typefaceMailText);
		//textViewEmailThis.setOnClickListener(listenerEmailThis);
		relativeLayoutMail.setOnClickListener(listenerSendEmail);
		imageViewHomePage.setOnClickListener(listenerHomePage);
		relativeLayoutPrint.setOnClickListener(listenerPrintThis);
        imageViewPrint.setOnClickListener(listenerPrintThis);
        imageViewEmail.setOnClickListener(listenerSendEmail);
        imageViewEmailMap.setOnClickListener(listenerSendEmailWithMap);
        imageviewMailMapTouch.setOnClickListener(listenerSendEmailWithMap);
        date = SingletonVisitorProfile.getInstance().getDateOfVisitLocal().split(MainConstants.kConstantWhiteSpace);
        //Log.d("date","date" +date[0].toString());
        if((SingletonVisitorProfile.getInstance().getOfficeLocation()!= null)&&(SingletonVisitorProfile.getInstance().getOfficeLocation() != MainConstants.kConstantEmpty) && (SingletonVisitorProfile.getInstance().getOfficeLocation().length() > MainConstants.kConstantZero)){
            dateWithOfficeLocation = date[0].toString() + " at " +SingletonVisitorProfile.getInstance().getOfficeLocation();
        }
        else
        {
            dateWithOfficeLocation=date[0].toString();
        }
		//imageViewBluetooth
				//.setOnClickListener(listenerSendImageViaBluetooth);

        //relativeLayoutVisitorBadge.setBackgroundResource(R.drawable.background_visitor_badge);
	}

	private OnClickListener listenerSendImageViaBluetooth = new OnClickListener() {

		@Override
		public void onClick(View v) {


            File sourceFile =new File(SingletonVisitorProfile.getInstance().getVisitorBadge());
            if (sourceFile.exists()) {
                sendViaBluetooth();
            }else
            {
                storeBitmap();
                sendViaBluetooth();
            }
            storeBitmap();

		}
	};


    private void sendViaBluetooth(){
        File sourceFile = new File(SingletonVisitorProfile.getInstance()
                .getVisitorBadge());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/jpg");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(sourceFile));
        startActivity(intent);
    }
	/*
	 * created by jayapriya on 19/11/2014 listener to set the printer and print
	 * the visitorBadge.
	 */
	private OnClickListener listenerPrintThis = new OnClickListener() {

		@Override
		public void onClick(View v) {

            if ((SingletonVisitorProfile.getInstance().getVisitorBadge() != null)
                    && (SingletonVisitorProfile.getInstance().getVisitorBadge().toString() == MainConstants.kConstantEmpty)
                    &&( new File(SingletonVisitorProfile.getInstance().getVisitorBadge()).exists()) ) {
                printBitmapOfVisitorBadge();
            }else
            {
                storeBitmap();
                printBitmapOfVisitorBadge();
            }
    	}
	};
	/*
	 * created by jayapriya On 14/11/2014 set the email
	 */

	private View.OnClickListener listenerEmailThis = new OnClickListener() {

		@Override
		public void onClick(View v) {

            //Log.d("touch","listenermail");
               storeBitmap();
				sendEmail();


		}

	};
	/*
	 * created by jayapriya On 14/11/2014
	 */
	private OnClickListener listenerSendEmail = new OnClickListener() {

		@Override
		public void onClick(View v) {
            //Log.d("touch","listenermail");
            if ((SingletonVisitorProfile.getInstance().getVisitorBadge() != null)
                    && (SingletonVisitorProfile.getInstance().getVisitorBadge().toString() != MainConstants.kConstantEmpty)
                    &&( new File(SingletonVisitorProfile.getInstance().getVisitorBadge()).exists()) ) {

                         sendEmail();
            }else
            {
                storeBitmap();
                sendEmail();
            }

		}
	};

    /*
	 * created by jayapriya On 14/11/2014
	 */
    private OnClickListener listenerSendEmailWithMap = new OnClickListener() {

        @Override
        public void onClick(View v) {
            //Log.d("touch","listenermailattached");
           sendEmailToAttachedImage();
        }
    };
	/*
	 * Created by jayapriya On 15/11/2014 Intent to launch the home activity.
	 */
	private OnClickListener listenerHomePage = new OnClickListener() {

		@Override
		public void onClick(View v) {
            intializeDialogBoxForHomePage();
            showHomeDialogBox();

		}
	};

	/**
	 * Created by jayapriya On 14/11/2014 When back Pressed then only call this
	 * function.
	 */
	public void onBackPressed() {
        intializeDialogBoxForHomePage();
        showHomeDialogBox();
	}

	/*
	 * Created by jayapriya On 15/11/2014 create intent to get the homePage.
	 */
	protected void getHomePage() {
		Intent intent = new Intent(getApplicationContext(),
				WelcomeActivity.class);
		startActivity(intent);
		finish();
	}

	/*
	 * Created by jayapriya On 15/11/2014 create bitmap to get the visitor Badge.
	 */
	private String createBitmap(View view, String fileName) {

		if (view != null) {
			sourceFile = constants.MainConstants.kConstantFileDirectory + MainConstants.kConstantFileTemporaryFolder+ fileName
					+ MainConstants.kConstantImageFileFormat;
			view.setDrawingCacheEnabled(true);
			view.buildDrawingCache(true);
			view.setBackgroundColor(Color.WHITE);
			Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
			UsedBitmap usedBitmap = new UsedBitmap();
			usedBitmap.storeBitmap(bitmap, 800, sourceFile);

			bitmap.recycle();
			return sourceFile;
		} else {
			return "view null";
		}

	}

	/*
	 * created By jayapriya On 14/11/2014 this function send the email, and the
	 * attached the image file.
	 */
	protected void sendEmail() {

        if((SingletonVisitorProfile.getInstance().getVisitorBadge() !=null) && (SingletonVisitorProfile.getInstance().getVisitorBadge() !=MainConstants.kConstantEmpty)
               ) {

            //Log.i("Send email", "");
            //Log.d("Send email", "Singleton.getInstance().getVisitorBadge"
                 //   + SingletonVisitorProfile.getInstance().getVisitorBadge());
            //String[] date= SingletonVisitorProfile.getInstance().getDateOfVisitLocal().split("");
            //Log.d("date","date" +date[0]);


            // String[] CC = {"mcmohd@gmail.com"};
           File sourcefile = new File(SingletonVisitorProfile.getInstance().getVisitorBadge());

            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:?subject=" + "Your Zoho Visitor Badge for " +dateWithOfficeLocation + "&body=" + "Sent from Zoho Frontdesk App"));
            //emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            // emailIntent.putExtra(Intent.EXTRA_CC, CC);
            //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your Zoho Visitor Badge for " +dateWithOfficeLocation);
            //emailIntent.putExtra(Intent.EXTRA_TEXT, "Sent from Zoho Frontdesk App");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(sourcefile));
            emailIntent.setType("message/rfc822");
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            //finish();
            //Log.i("Finished  email...", "");
            /*Intent emailIntent = new Intent(Intent.ACTION_VIEW);
            emailIntent.setType("message/rfc822");
            emailIntent.setData(Uri.parse("mailto:?subject=" + "Your Zoho Visitor Badge for " +dateWithOfficeLocation + "&body=" + "Sent from Zoho Frontdesk App"));
            emailIntent.putExtra(Intent.EXTRA_CC, CC);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your Zoho Visitor Badge for " +dateWithOfficeLocation);
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Sent from Zoho Frontdesk App");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(sourcefile));

            emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivity");
            startActivity(emailIntent);*/

        }
        /*File sourcefile = new File(SingletonVisitorProfile.getInstance().getVisitorBadge());

        Intent shareIntent = findEmail();
        //shareIntent.setData(Uri.parse(MainConstants.kConstantMailTo));
        //emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        // emailIntent.putExtra(Intent.EXTRA_CC, CC);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Your Zoho Visitor Badge for " +dateWithOfficeLocation);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Sent from Zoho Frontdesk App");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(sourcefile));
        shareIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(shareIntent, "Send mail..."));
        //startActivity(Intent.createChooser(shareIntent, "Share"));*/
		
	}

	/*
	 * Created by jayapriya on 14/11/2014this function get the drawable image to
	 * attached the email intent to send email.
	 */
	private void sendEmailToAttachedImage() {
      /*  Bitmap bm = BitmapFactory.decodeResource( getResources(), drawable.icon_map_dlf);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();

        File file = new File(extStorageDirectory, "icon_map_dlf.png");
        try {
            FileOutputStream outStream = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (IOException e) {
            Log.d("EXception","" +e);
        }*/

		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("message/rfc822");
		//emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Few things that might help in your Zoho visit today");
		//Uri uri = Uri.fromFile(new File(Environment
				//.getExternalStorageDirectory(), "image.png"));
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Sent from Zoho Frontdesk App");

        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri

                .parse("android.resource://"

                        + getPackageName() + "/"+R.drawable.icon_map_dlf));
		startActivity(Intent.createChooser(emailIntent, "Send mail..."));
}

	/*
	 * Created by jayapriya On 17/11/2014 Copy inputfile into the outputfile.
	 */
	private void copyFile(InputStream inputFile, OutputStream outputFile)
			throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = inputFile.read(buffer)) != -1) {
			outputFile.write(buffer, 0, read);
		}
	}

	/*
	 * created by jayapriya on 18/11/2014 print the visitorBadge.
	 */
	private void printBitmapOfVisitorBadge() {

       // if ((SingletonVisitorProfile.getInstance().getVisitorBadge() != null)
              //  && (SingletonVisitorProfile.getInstance().getVisitorBadge().toString() != MainConstants.kConstantEmpty)
               // &&(new File(SingletonVisitorProfile.getInstance().getVisitorBadge()).exists()) ) {
            //Log.d("window","listenerprinter");
            PrintHelper photoPrinter = new PrintHelper(this);
            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
            // Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
            // R.drawable.business_card);
            // photoPrinter.printBitmap("droids.jpg - test print", bitmap);

            // File filePath = new File(Singleton.getInstance().getVisitorBadge());
            Bitmap bitmap = BitmapFactory.decodeFile(SingletonVisitorProfile.getInstance()
                    .getVisitorBadge());
            photoPrinter.printBitmap("VisitorBadge", bitmap);
            //Log.d("window","photoPrinter"+SingletonVisitorProfile.getInstance()
               //     .getVisitorBadge());
            bitmap.recycle();

		/*
		 * if ((Singleton.getInstance().getVisitorBadge() != null) &&
		 * (Singleton.getInstance().getVisitorBadge().toString() !=
		 * MainConstants.kConstantEmpty)) { try {
		 * photoPrinter.printBitmap("badge", Uri.fromFile(filePath)); } catch
		 * (FileNotFoundException e) { // TODO Auto-generated catch block
		 * 
		 * }
		 * 
		 * }
		 */
       // }
	}

	private void storeBitmap() {
        if ((SingletonVisitorProfile.getInstance().getVisitorBadge() == null)
                || (SingletonVisitorProfile.getInstance().getVisitorBadge().toString() == MainConstants.kConstantEmpty)
                ||(! new File(SingletonVisitorProfile.getInstance().getVisitorBadge()).exists()) ) {

            visitorBadgeSourceFile = createBitmap(visitorBadge, "Badge");
            SingletonVisitorProfile.getInstance().setVisitorBadge(visitorBadgeSourceFile);
            //Log.d("visitor", "badged" + visitorBadgeSourceFile + " " + SingletonVisitorProfile.getInstance().getVisitorBadge());
        }
	}

    /*
	 * Created by jayapriya On 01/12/2014 Intialize the dialogbox for home page
	 */
    private void intializeDialogBoxForHomePage() {
        dialogGetHomeMessage = new Dialog(this);
        dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGetHomeMessage
                .setContentView(R.layout.dialog_box_confirm_message);
        // dialog.(MessageConstant.dialogTitle);
        dialogGetHomeMessage.setCanceledOnTouchOutside(true);
        dialogGetHomeMessage.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message);
        textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message_confirm);
        textViewHomeMessageCancel = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message_cancel);
        textViewDialogHomeMessage
                .setText(StringConstants.kConstantHomePageMessageInVisitorBadge);
        textViewHomeMessageConfirm.setText(StringConstants.kConstantDialogMessageAmSure);
        textViewHomeMessageCancel.setText(StringConstants.kConstantDialogMessageNotyet);

        Typeface typefaceFontSetMessageOk = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
        textViewDialogHomeMessage.setTypeface(typefacedialog);
        textViewHomeMessageCancel.setTypeface(typefaceFontSetMessageOk);
        textViewHomeMessageConfirm.setTypeface(typefaceFontSetMessageOk);
        //textViewHomeMessageConfirm.setText(MainConstants.kConstantOka);
        textViewHomeMessageConfirm.setOnClickListener(listenerConfirmHomePage);
        textViewHomeMessageCancel.setOnClickListener(listenerCancelDialog);

    }


    /*
     * Created by jayapriya On 01/12/2014 show dialogbox for home page
     */
    private void showHomeDialogBox() {

        if ((dialogGetHomeMessage != null)
                && (!dialogGetHomeMessage.isShowing())) {

            dialogGetHomeMessage.show();
        }
    }


    /*
     * Created by Jayapriya On 26/09/2014 Click the errorMessageConfirm to
     * activate the next processing.
     */
    private OnClickListener listenerConfirmHomePage = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub

            dialogGetHomeMessage.dismiss();
            getHomePage();
        }
    };

    /*
    * Created by Jayapriya On 02/12/2014 Click the errorMessageCancel to dismiss
    * the dialogBox
    */
    private OnClickListener listenerCancelDialog = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if((dialogGetHomeMessage != null) &&(dialogGetHomeMessage.isShowing())) {
                dialogGetHomeMessage.dismiss();
            }
        }
    };

    private Intent findEmail() {
        final String[] emailApps = {
                // package // name - nb installs (thousands)
                "com.gmail.com", // official - 10 000
                "com.email.com", // twidroid - 5 000
                "com.yahoo.com", // Tweecaster - 5 000
                 }; // TweetDeck - 5 000 };
        Intent mailIntent = new Intent();
        mailIntent.setType("text/plain");
        final PackageManager packageManager = getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(
                mailIntent, PackageManager.MATCH_DEFAULT_ONLY);

        for (int i = 0; i < emailApps.length; i++) {
            for (ResolveInfo resolveInfo : list) {
                String p = resolveInfo.activityInfo.packageName;
                if (p != null && p.startsWith(emailApps[i])) {
                    mailIntent.setPackage(p);
                    return mailIntent;
                }
            }
        }
        return null;
    }
    
   
	/*
	 * background process for upload visitor details
	 */
	public class backgroundProcess extends AsyncTask<String, String, String> {

		int uploadedCode;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		protected String doInBackground(String... f_url) {
			//Log.d("window", "successCount" + f_url[0]);
			try {
				//creator.searchPurposeRecord();
				SingletonVisitorProfile.getInstance().setDateOfVisit(
		                utility.TimeZoneConventer.getGmtTime());
		        SingletonVisitorProfile.getInstance().setDateOfVisitLocal(utility.TimeZoneConventer.getCurrentDateTime());
		        SingletonVisitorProfile.getInstance().setGmtdiff(
		                utility.TimeZoneConventer.getGMTTime());
               if((SingletonVisitorProfile.getInstance().getCodeNo() == MainConstants.kConstantQrCodeEntry))
                {

                    creator.updateCreatorInQrcode();
                }
                else {
                	    uploadedCode=creator.searchVisitorRecord(getApplicationContext());
                    //Log.d("window: ", "uploadedCode:"+uploadedCode);
                }

			} catch (Exception e) {
				// Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			if(uploadedCode>ErrorConstants.visitorUploadVisitIOError||uploadedCode==MainConstants.kConstantMinusOne) {
				
			DialogView.showReportDialog(getApplicationContext(),MainConstants.badgeDialog,ErrorConstants.visitorAllSuccessCode);
			handlerHideDialog.postDelayed(hideDialogTask, 3000);
			}
			else {
				MediaPlayer mp = MediaPlayer.create(getApplicationContext(),R.raw.bonk);
	 			mp.start();
				SingletonCode.setSingletonVisitorUploadErrorCode(uploadedCode);
				DialogView.showReportDialog(getApplicationContext(),MainConstants.badgeDialog,uploadedCode);
				}
		}
	}
	
	Runnable hideDialogTask = new Runnable() {

		@Override
		public void run() {
			DialogView.hideDialog();
			 getHomePage();
		}
	};
	protected void onDestroy()
 	{
 		super.onDestroy();
 		DialogView.setNullDialog();
 	}
}
