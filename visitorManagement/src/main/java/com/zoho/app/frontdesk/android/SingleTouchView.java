package com.zoho.app.frontdesk.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import appschema.SingletonMetaData;

/**
 * 
 * @author jayapriya Create the new view in the path and
 */
public class SingleTouchView extends View {
	private Paint paint;
	private Path path = new Path();

	// create Constructor
	public SingleTouchView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// Log.d("VisitorAgreement", "Signarr:" +attrs.getAttributeCount());

		paint = new Paint();
		paint.setAntiAlias(true);
		int dpSize =  3;
		DisplayMetrics dm = getResources().getDisplayMetrics() ;
		float strokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, dpSize, dm);
		paint.setStrokeWidth(strokeWidth);
		if( SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getThemeCode()>0)
		{
			if(SingletonMetaData.getInstance().getThemeCode()==1) {
				paint.setColor(Color.WHITE);
			}
			else if(SingletonMetaData.getInstance().getThemeCode()==2)
			{
				paint.setColor(Color.BLACK);
			}
		}
//		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// Log.d("VisitorAgreement", "OnDraw");

		canvas.drawPath(path, paint);

	}

	/**
	 * Created by jayapriya This fuction reset the path.
	 */
	public void signAgain() {
		Canvas canvas = new Canvas();
		// mPath.lineTo(mX, mY);
		// Log.d("VisitorAgreement", "Sign:" +path.isEmpty());
		path.reset();
		path = new Path();
		// Log.d("VisitorAgreement", "Sign:" +path.isEmpty());
		// commit the path to our offscreen
		canvas.drawPath(path, paint);
		// kill this so we don't double draw
		invalidate();

		// Canvas canvas = new Canvas();

		// canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
	}

	public Boolean isPathNotEmpty() {
		return path.isEmpty();
	}

	public boolean onTouchEvent(MotionEvent event) {

		float eventX = event.getX();
		float eventY = event.getY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			path.moveTo(eventX, eventY);
			return true;
		case MotionEvent.ACTION_MOVE:
			path.lineTo(eventX, eventY);
			break;
		case MotionEvent.ACTION_UP:
			// nothing to do
			break;
		default:
			return false;
		}

		// public void reset() {
		// this.drawCanvas.drawColor(0, Mode.CLEAR);
		// invalidate();
		// }
		// Schedules a repaint.
		invalidate();
		return true;

	}
}