package com.zoho.app.frontdesk.android;

import java.util.Locale;

import com.zoho.app.frontdesk.android.R.id;

import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import constants.StringConstants;
import utility.DialogViewWarning;
import appschema.SharedPreferenceController;
import utility.UsedCustomToast;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;

public class ActivityDeviceDetails extends Activity {

	private DialogViewWarning dialog;
	private EditText editTextSerialNumber, editTextMake,editTextOtherDetails;
	private RelativeLayout relativeLayoutExisitingVisitor;
	private ImageView imageviewBack, imageviewHome, imageviewPrevious,image_view_next,
			imageviewNext, imageViewScanBarCode, imageGetLaptopDetails;
	private ImageView imageviewExtDeviceOne,imageviewExtDeviceTwo,imageviewExtDeviceThree,
	imageviewExtDeviceFive,imageviewExtDeviceFour;
	private TextView textviewExtDeviceOne, textviewExtDeviceTwo,
			textviewExtDeviceThree, textviewExtDeviceFour,
			textviewExtDeviceFive,
			textviewExtDeviceAccept, textviewExtDeviceTitle, textviewSerialOptional;
	private Typeface typeFaceTitle, typeFaceAccept, typeFaceWarning;
	private int actionCode = MainConstants.backPress;
	private Handler noActionHandler;
	private TextToSpeech textToSpeech;
	private boolean booleanExtDeviceOne=false,booleanExtDeviceTwo=false,booleanExtDeviceThree=false,
	booleanExtDeviceFive=false,booleanExtDeviceFour=false;
    private int applicationMode = NumberConstants.kConstantZero;
    private SharedPreferenceController sharedPreferenceController;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove the title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// hide the status bar.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_devices_details);
		assignObjects();
		setOldValue();
	}

	/**
	 * This function is called, when the activity is started.
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onStart() {
		super.onStart();

	}

	/**
	 * This function is called, when the activity is stopped.
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onStop() {
		super.onStop();
	}

	/**
	 * This function is called, when the activity is destroyed.
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onDestroy() {
		super.onDestroy();
		deallocateObjects();
	}

	/**
	 * Assign Objects to the variable
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private void assignObjects()
	{
		textviewExtDeviceTitle = (TextView) findViewById(R.id.textview_have_device_title);

		imageviewBack = (ImageView) findViewById(R.id.imageview_back);
		imageviewHome = (ImageView) findViewById(R.id.imageview_home);
		imageviewNext = (ImageView) findViewById(R.id.imageview_next);
		imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);

		imageviewExtDeviceOne = (ImageView) findViewById(R.id.imageview_external_device_one);
		imageviewExtDeviceTwo = (ImageView) findViewById(R.id.imageview_external_device_two);
		imageviewExtDeviceThree = (ImageView) findViewById(R.id.imageview_external_device_three);
		imageviewExtDeviceFour = (ImageView) findViewById(R.id.imageview_external_device_four);
		imageviewExtDeviceFive = (ImageView) findViewById(R.id.imageview_external_device_five);
		imageViewScanBarCode = (ImageView)findViewById(R.id.imageview_scan_icon);
		imageGetLaptopDetails = (ImageView) findViewById(R.id.addLaptopDetails);

		relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);
		textviewExtDeviceOne = (TextView) findViewById(R.id.textview_external_device_one);
		textviewExtDeviceTwo = (TextView) findViewById(R.id.textview_external_device_two);
		textviewExtDeviceThree = (TextView) findViewById(R.id.textview_external_device_three);
		textviewExtDeviceFour = (TextView) findViewById(R.id.textview_external_device_four);
		textviewExtDeviceFive = (TextView) findViewById(R.id.textview_external_device_five);
		textviewExtDeviceTitle = (TextView) findViewById(R.id.textview_device_title);
		textviewExtDeviceAccept = (TextView) findViewById(R.id.textview_external_accept);
		textviewSerialOptional = (TextView) findViewById(R.id.textview_serial_optional);
		editTextSerialNumber = (EditText) findViewById(R.id.edit_Text_serial_number);
		editTextMake = (EditText) findViewById(R.id.edit_Text_make);

		editTextOtherDetails = (EditText) findViewById(R.id.edit_text_other_data);

		noActionHandler = new Handler();
		noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

		if (textToSpeechListener != null) {
			textToSpeech = new TextToSpeech(getApplicationContext(),
					textToSpeechListener);
		}

		// set font
		typeFaceTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		typeFaceAccept = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskSemiBold);
		typeFaceWarning = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskItalic);
		textviewExtDeviceAccept.setTypeface(typeFaceTitle);
		textviewExtDeviceTitle.setTypeface(typeFaceAccept);

		textviewExtDeviceOne.setTypeface(typeFaceTitle);
		textviewExtDeviceTwo.setTypeface(typeFaceTitle);
		textviewExtDeviceThree.setTypeface(typeFaceTitle);
		textviewExtDeviceFour.setTypeface(typeFaceTitle);
		textviewExtDeviceFive.setTypeface(typeFaceTitle);

		editTextMake.setTypeface(typeFaceTitle);
		editTextOtherDetails.setTypeface(typeFaceTitle);
		editTextSerialNumber.setTypeface(typeFaceTitle);
		textviewSerialOptional.setTypeface(typeFaceWarning);

		// Onclick listeners
		imageviewPrevious.setOnClickListener(listenerPrevious);
		imageviewNext.setOnClickListener(listenerConfirm);
		image_view_next = (ImageView) findViewById(id.imageview_next_page);
		image_view_next.setOnClickListener(listenerConfirm);
		imageviewHome.setOnClickListener(listenerHomePage);
		imageviewBack.setOnClickListener(listenerBack);

		if(sharedPreferenceController==null) {
			sharedPreferenceController = new SharedPreferenceController();
		}

		applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());

		if(applicationMode == NumberConstants.kConstantNormalMode) {
			imageviewExtDeviceOne.setOnClickListener(listenerExtDeviceOne);
			imageviewExtDeviceTwo.setOnClickListener(listenerExtDeviceTwo);
			imageviewExtDeviceThree.setOnClickListener(listenerExtDeviceThree);
			imageviewExtDeviceFour.setOnClickListener(listenerExtDeviceFour);
			imageviewExtDeviceFive.setOnClickListener(listenerExtDeviceFive);
			imageViewScanBarCode.setOnClickListener(openBarcodeScanListener);
		}
		else if(applicationMode == NumberConstants.kConstantDataCentreMode)
		{
			textviewExtDeviceOne.setVisibility(View.GONE);
			textviewExtDeviceTwo.setVisibility(View.GONE);
			textviewExtDeviceThree.setVisibility(View.GONE);
			textviewExtDeviceFour.setVisibility(View.GONE);
			textviewExtDeviceFive.setVisibility(View.GONE);
			textviewExtDeviceTitle.setVisibility(View.GONE);
			imageviewExtDeviceOne.setVisibility(View.GONE);
			imageviewExtDeviceTwo.setVisibility(View.GONE);
			imageviewExtDeviceThree.setVisibility(View.GONE);
			imageviewExtDeviceFour.setVisibility(View.GONE);
			imageviewExtDeviceFive.setVisibility(View.GONE);
			imageViewScanBarCode.setVisibility(View.GONE);

			editTextOtherDetails.setVisibility(View.VISIBLE);
		}
		editTextSerialNumber.addTextChangedListener(textWatcherLaptopSerialNo);
		editTextMake.addTextChangedListener(textWatcherLaptopMake);

		editTextOtherDetails.addTextChangedListener(textWatcherothers);
		setAppBackground();
	}

	/**
	 * Set background color
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private void setAppBackground() {
		if( SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getThemeCode()>MainConstants.kConstantZero)
		{
			if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantOne) {
				relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
			}
			else if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantTwo)
			{
				GradientDrawable gd = new GradientDrawable(
						GradientDrawable.Orientation.BOTTOM_TOP,
						new int[] {0xFFfa654b,0xFFfc5d55,0xFFfd575d,0xFFff5066});
				relativeLayoutExisitingVisitor.setBackground(gd);
			}
		}

	}

	/**
	 * Play text to voice
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
		@Override
		public void onInit(int status) {
			if (textToSpeech != null && status != TextToSpeech.ERROR) {
				textToSpeech.setLanguage(Locale.ENGLISH);
			}
			textSpeach(MainConstants.kConstantOne);
		}
	};

	/**
	 * intent to launch previous page
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private OnClickListener listenerBack = new OnClickListener() {

		public void onClick(View v) {
			actionCode = MainConstants.backPress;
			launchPreviousPage();
			// showDetailDialog();
		}
	};

	/**
	 * listener to change image and set boolean true/false
	 *
	 * @author Karthick
	 *
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceOne = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceOne==true) {
				booleanExtDeviceOne=false;
			imageviewExtDeviceOne.setBackgroundResource(R.drawable.icon_dvd_deselected);
			} else {
				booleanExtDeviceOne=true;
				imageviewExtDeviceOne.setBackgroundResource(R.drawable.icon_dvd_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};

	/**
	 * listener to change image and set boolean true/false
	 *
	 * @author Karthick
	 *
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceTwo = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceTwo==true) {
				booleanExtDeviceTwo=false;
			imageviewExtDeviceTwo.setBackgroundResource(R.drawable.icon_pendrive_deselcted);
			} else {
				booleanExtDeviceTwo=true;
				imageviewExtDeviceTwo.setBackgroundResource(R.drawable.icon_pendrive_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};

	/**
	 * listener to change image and set boolean true/false
	 *
	 * @author Karthick
	 *
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceThree = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceThree==true) {
				booleanExtDeviceThree=false;
			imageviewExtDeviceThree.setBackgroundResource(R.drawable.icon_externalcard_deselected);
			} else {
				booleanExtDeviceThree=true;
				imageviewExtDeviceThree.setBackgroundResource(R.drawable.icon_externalcard_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};

	/**
	 * listener to change image and set boolean true/false
	 *
	 * @author Karthick
	 *
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceFour = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceFour==true) {
				booleanExtDeviceFour=false;
			imageviewExtDeviceFour.setBackgroundResource(R.drawable.icon_harddisk_deselected);
			} else {
				booleanExtDeviceFour=true;
				imageviewExtDeviceFour.setBackgroundResource(R.drawable.icon_harddisk_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};

	/**
	 * listener to change image and set boolean true/false
	 *
	 * @author Karthick
	 *
	 * @created 20161230
	 */
	private OnClickListener listenerExtDeviceFive = new OnClickListener() {
		public void onClick(View v) {
			if(booleanExtDeviceFive==true) {
				booleanExtDeviceFive=false;
			imageviewExtDeviceFive.setBackgroundResource(R.drawable.icon_tablet_deselected);
			} else {
				booleanExtDeviceFive=true;
				imageviewExtDeviceFive.setBackgroundResource(R.drawable.icon_tablet_selected);
			}
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
			setAutoCheckinDeviceHandler();
		}
	};

	/**
	 * Open barcode scan
	 *
	 * @author Karthick
	 * @created on 20161221
	 */
	private OnClickListener openBarcodeScanListener = new OnClickListener() {

		public void onClick(View v) {
			launchBarcodeScan();
//			if (handlerAutoCheckIn != null) {
//				handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
//			}
		}
	};

	/**
	 * textWatcher to validate the Laptop SerialNo
	 *
	 * @author Karthick
	 *
	 * @created on 20170519
	 */
	public TextWatcher textWatcherLaptopSerialNo = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			setAutoCheckinDeviceHandler();
		}

		public void afterTextChanged(Editable s) {
			setBorder();
		}
	};

	/**
	 * textWatcher to validate the others Field Value
	 *
	 * @author Karthikeyan D S
	 * @created on 02Jul2018
	 */
	public TextWatcher textWatcherothers = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
									  int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
								  int count) {
			setAutoCheckinDeviceHandler();
		}

		public void afterTextChanged(Editable s) {
			setBorder();
		}
	};


	/**
	 * textWatcher to validate the Laptop make
	 *
	 * @author Karthick
	 *
	 * @created on 20170519
	 */
	public TextWatcher textWatcherLaptopMake = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			setAutoCheckinDeviceHandler();
		}

		public void afterTextChanged(Editable s) {
			setBorder();
		}
	};

	/**
	 * intent to launch previous page
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private OnClickListener listenerHomePage = new OnClickListener() {

		public void onClick(View v) {
			actionCode = MainConstants.homePress;
			hideKeyboard();
			showDetailDialog();
		}
	};

	/**
	 * Verify Existing visitor in creator and launched next activity.
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private OnClickListener listenerConfirm = new OnClickListener() {
		public void onClick(View v) {
			if(validate()) {
				storeValues();
				launchNextPage();
			}
		}
	};

	/**
	 * Launch previous activity.
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private OnClickListener listenerPrevious = new OnClickListener() {
		public void onClick(View v) {
			launchPreviousPage();
		}
	};

	/**
	 * Launch previous activity
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private void launchHomePage() {

		if (actionCode == MainConstants.homePress) {
			Intent intent = new Intent(this, WelcomeActivity.class);
			startActivity(intent);
		} else {
			launchPreviousPage();
		}

		finish();
	}

	/**
	 * Launch previous activity.
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private void launchPreviousPage() {
		Intent intent = new Intent(getApplicationContext(),
				ActivityHaveAnyDevices.class);
		startActivity(intent);
		finish();
	}

	/**
	 * Launch next activity.
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private void launchNextPage() {
		Intent intent = new Intent(getApplicationContext(),
					ActivityVisitorTempID.class);
		startActivity(intent);
		finish();
	}

	/**
	 * Launch Barcode Scan
	 *
	 * @author Karthick
	 * @created on 20161221
	 */
	private void launchBarcodeScan() {
		try {
			 Class.forName(MainConstants.barcodeScanClassName);
			 Intent intent = new Intent(MainConstants.barcodeScanIntent);
			 intent.putExtra(MainConstants.barcodeScanFormatKey, MainConstants.barcodeScanFormat);
			 startActivityForResult(intent, MainConstants.barcodeScanRequestCode);
		} catch(ClassNotFoundException excep) {
			UsedCustomToast.makeCustomToast(this,MainConstants.barcodeScanErrorMsg,Toast.LENGTH_LONG,Gravity.TOP).show();
		}
	}

	/**
	 * Store value to singleton
	 *
	 * @author Karthick
	 *
	 * @created on 20170520
	 */
	private void storeValues() {
		if(SingletonVisitorProfile.getInstance()!=null) {
			if(applicationMode==NumberConstants.kConstantNormalMode)
			{
				SingletonVisitorProfile.getInstance().setExtDeviceOne(booleanExtDeviceOne);
				SingletonVisitorProfile.getInstance().setExtDeviceTwo(booleanExtDeviceTwo);
				SingletonVisitorProfile.getInstance().setExtDeviceThree(booleanExtDeviceThree);
				SingletonVisitorProfile.getInstance().setExtDeviceFour(booleanExtDeviceFour);
				SingletonVisitorProfile.getInstance().setExtDeviceFive(booleanExtDeviceFive);
			}
			else if(applicationMode==NumberConstants.kConstantDataCentreMode)
			{
				if(editTextOtherDetails.getText().toString().trim()!=null && !editTextOtherDetails.getText().toString().trim().isEmpty()
						&&editTextOtherDetails.getText().toString().trim().length()>0) {
					SingletonVisitorProfile.getInstance().setOtherDeviceDetails(editTextOtherDetails.getText().toString().trim());
				}
			}
		}
	}

	/**
	 * validate the Laptop details
	 *
	 * @author Karthick
	 * @created on 20170520
	 */
	private boolean validate() {

		boolean isValid=false;
		if (editTextMake != null
				&& editTextMake.getText() != null
				&& !editTextMake.getText().toString().trim().isEmpty()
				&& editTextSerialNumber != null
				&& editTextSerialNumber.getText() != null
				&& !editTextSerialNumber.getText().toString().trim()
						.isEmpty()) {
			isValid = true;
		} else if ((editTextMake != null
				&& editTextMake.getText() != null
				&& editTextMake.getText().toString().trim().isEmpty())
				&& (editTextSerialNumber != null
				&& editTextSerialNumber.getText() != null
				&& editTextSerialNumber.getText().toString().trim()
						.isEmpty())) {
			isValid = true;
		}
		else if ((editTextMake != null
				&& editTextMake.getText() != null
				&& !editTextMake.getText().toString().trim().isEmpty())
				&& (editTextSerialNumber != null
				&& editTextSerialNumber.getText() != null
				&& editTextSerialNumber.getText().toString().trim()
						.isEmpty())) {
			isValid = true;
		}

		// assign value to singleton object
		if (editTextMake != null && editTextMake.getText() != null
				&& isValid) {
			SingletonVisitorProfile.getInstance().setDeviceMake(
					editTextMake.getText().toString().trim());

		}
		if (editTextSerialNumber != null
				&& editTextSerialNumber.getText() != null && isValid) {
			SingletonVisitorProfile.getInstance().setDeviceSerialNumber(
					editTextSerialNumber.getText().toString().trim());
		}
		if (((SingletonVisitorProfile.getInstance() != null && !SingletonVisitorProfile
				.getInstance().getDeviceMake().isEmpty()))
				&& (isValid)) {
			imageGetLaptopDetails
					.setBackgroundResource(R.drawable.icon_laptop_has_data);
		} else if (((SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile
				.getInstance().getDeviceMake().isEmpty()) && (SingletonVisitorProfile
				.getInstance() != null))) {
			imageGetLaptopDetails
					.setBackgroundResource(R.drawable.icon_laptop_no_data);
		}

		if (editTextMake != null && editTextMake.getText() != null
				&& !editTextMake.getText().toString().trim().isEmpty()) {
			editTextMake
					.setBackgroundResource(R.drawable.cell_underline_white);
		} else if(isValid==false){
			UsedCustomToast.makeCustomToast(this, StringConstants.enterLaptopMake, Toast.LENGTH_SHORT, Gravity.TOP).show();
		}

//		if (editTextSerialNumber != null
//				&& editTextSerialNumber.getText() != null
//				&& !editTextSerialNumber.getText().toString().trim()
//						.isEmpty()) {
//		} else if(isValid==false){
//			UsedCustomToast.makeCustomToast(this, MainConstants.enterLaptopSerialNumber, Toast.LENGTH_SHORT, Gravity.TOP).show();
//		}
		return isValid;
	}

	/**
	 * textWatcher to validate the Laptop make
	 *
	 * @author Karthick
	 *
	 * @created on 20161230
	 */
	private void setBorder() {
		if(editTextMake!=null && !editTextMake.getText().toString().trim().isEmpty()) {
			imageGetLaptopDetails.setBackgroundResource(R.drawable.icon_laptop_has_data);
		} else {
			imageGetLaptopDetails.setBackgroundResource(R.drawable.icon_laptop_no_data);
		}

		noActionHandler.removeCallbacks(noActionThread);
		noActionHandler.postDelayed(noActionThread,
				MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
	}

	/**
	 * Goto home page when no action.
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 *
	 */
	public Runnable noActionThread = new Runnable() {
		public void run() {
			actionCode = MainConstants.homePress;
			launchHomePage();
		}
	};

	/**
	 * Next process
	 *
	 * @author Karthick
	 * @created on 20161114
	 */
	Runnable purposeSeletionTask = new Runnable() {
		public void run() {
			launchNextPage();
		}
	};

	private void setAutoCheckinDeviceHandler() {
		if(noActionHandler!=null) {
			noActionHandler.removeCallbacks(noActionThread);
		}
//		if (handlerAutoCheckIn != null) {
//			handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
//			handlerAutoCheckIn.postDelayed(autoCheckInThread,
//					MainConstants.autoCheckInTimeDevice);
//		}
	}

	/**
	 * This function is called when the back pressed.
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onBackPressed() {
		actionCode = MainConstants.backPress;
		launchPreviousPage();
	}

	/**
	 * Hide keyboard
	 *
	 * @author Karthick
	 * @created on 20170520
	 */
	private void setOldValue() {
		if (editTextMake != null && SingletonVisitorProfile.getInstance()!=null
				&& SingletonVisitorProfile.getInstance().getDeviceMake()!=null) {
			editTextMake.setText(SingletonVisitorProfile.getInstance().getDeviceMake());
			editTextMake.setSelection(SingletonVisitorProfile.getInstance().getDeviceMake().length());
		}

		if (editTextSerialNumber != null && SingletonVisitorProfile.getInstance()!=null
				&& SingletonVisitorProfile.getInstance().getDeviceSerialNumber()!=null) {
			editTextSerialNumber.setText(SingletonVisitorProfile.getInstance().getDeviceSerialNumber());
			editTextSerialNumber.setSelection(SingletonVisitorProfile.getInstance().getDeviceSerialNumber().length());
		}

		if(applicationMode==NumberConstants.kConstantNormalMode) {
			editTextOtherDetails.setVisibility(View.GONE);
			if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().isExtDeviceOne()) {
				booleanExtDeviceOne = true;
				imageviewExtDeviceOne.setBackgroundResource(R.drawable.icon_dvd_selected);
			} else {
				booleanExtDeviceOne = false;
				imageviewExtDeviceOne.setBackgroundResource(R.drawable.icon_dvd_deselected);
			}
			if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().isExtDeviceTwo()) {
				booleanExtDeviceTwo = true;
				imageviewExtDeviceTwo.setBackgroundResource(R.drawable.icon_pendrive_selected);
			} else {
				booleanExtDeviceTwo = false;
				imageviewExtDeviceTwo.setBackgroundResource(R.drawable.icon_pendrive_deselcted);
			}
			if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().isExtDeviceThree()) {
				booleanExtDeviceThree = true;
				imageviewExtDeviceThree.setBackgroundResource(R.drawable.icon_externalcard_selected);
			} else {
				booleanExtDeviceThree = false;
				imageviewExtDeviceThree.setBackgroundResource(R.drawable.icon_externalcard_deselected);
			}
			if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().isExtDeviceFour()) {
				booleanExtDeviceFour = true;
				imageviewExtDeviceFour.setBackgroundResource(R.drawable.icon_harddisk_selected);
			} else {
				booleanExtDeviceFour = false;
				imageviewExtDeviceFour.setBackgroundResource(R.drawable.icon_harddisk_deselected);
			}
			if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().isExtDeviceFive()) {
				booleanExtDeviceFive = true;
				imageviewExtDeviceFive.setBackgroundResource(R.drawable.icon_tablet_selected);
			} else {
				booleanExtDeviceFive = false;
				imageviewExtDeviceFive.setBackgroundResource(R.drawable.icon_tablet_deselected);
			}
		}
		else if(applicationMode==NumberConstants.kConstantDataCentreMode)
		{
			editTextOtherDetails.setVisibility(View.VISIBLE);
			if (editTextOtherDetails != null && SingletonVisitorProfile.getInstance()!=null
					&& SingletonVisitorProfile.getInstance().getOtherDeviceDetails()!=null&&
					!SingletonVisitorProfile.getInstance().getOtherDeviceDetails().isEmpty()) {
				editTextOtherDetails.setText(SingletonVisitorProfile.getInstance().getOtherDeviceDetails());
				editTextOtherDetails.setSelection(SingletonVisitorProfile.getInstance().getOtherDeviceDetails().length());
			}

		}

	}

	/**
	 * Hide keyboard
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private void hideKeyboard() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	/**
	 * Show dialog when have user data Otherwise goto welcome page
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private void showDetailDialog() {
		showWarningDialog();
		textSpeach(MainConstants.speachAreYouSureCode);
	}

	/**
	 * Check if have value on edittext Then show warning dialog Otherwise goto
	 * back or home
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private void showWarningDialog() {
		hideKeyboard();
		if (dialog == null) {
			dialog = new DialogViewWarning(this);
		}
		dialog.setDialogCode(MainConstants.dialogDecision);
		dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
		dialog.setActionListener(listenerDialogAction);
		dialog.showDialog();
	}

	/**
	 * Handle tab click for goto settings page
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
		public void onClick(View v) {
			launchHomePage();
		}
	};

	/**
	 * Play text to voice
	 *
	 * @author Karthick
	 * @created on 20170508
	 */
	private void textSpeach(int textSpeachCode) {

		if (textToSpeech != null) {
			textToSpeech.stop();
			if (textSpeachCode == MainConstants.kConstantOne
					&& (SingletonVisitorProfile.getInstance() != null
							&& SingletonVisitorProfile.getInstance()
									.getPurposeOfVisitCode() != null && SingletonVisitorProfile
							.getInstance().getPurposeOfVisitCode().isEmpty())) {
//				textToSpeech.speak(TextToSpeachConstants.speachPurpose,
//						TextToSpeech.QUEUE_FLUSH, null, null);
			} else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
				textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
						TextToSpeech.QUEUE_FLUSH, null, null);
			}
		}
	}

	/**
	 * Deallocate Objects in the activity
	 *
	 * @author Karthick
	 *
	 * @created on 20170508
	 */
	private void deallocateObjects() {
		imageviewBack = null;
		imageviewHome = null;
		imageviewNext = null;
		textviewExtDeviceTitle = null;
		typeFaceTitle = null;
		relativeLayoutExisitingVisitor
				.setBackgroundResource(MainConstants.kConstantZero);
		relativeLayoutExisitingVisitor = null;
		if (noActionHandler != null) {
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler = null;
		}
		if(textToSpeech!=null)
		{
			textToSpeech.stop();
		}
	}


	/**
	 * Read scan result and set dialog edittext
	 *
	 * @author Karthick
	 * @created on 20161221
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == MainConstants.barcodeScanRequestCode) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				//String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				if(editTextSerialNumber!=null && contents!=null && !contents.isEmpty()) {
					editTextSerialNumber.setText(contents);
					editTextSerialNumber.setSelection(contents.length());
				}
			}
		}
	}
}
