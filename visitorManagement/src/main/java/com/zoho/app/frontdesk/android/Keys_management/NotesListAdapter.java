package com.zoho.app.frontdesk.android.Keys_management;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.keyEntity;
import com.zoho.app.frontdesk.android.R;

import java.util.List;

public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.CustomViewHolder> {

    private List<keyEntity> notes;
    public NotesListAdapter(List<keyEntity> notes) {
        this.notes = notes;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_array_adapter, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        keyEntity note = getItem(position);

        holder.itemTitle.setText(note.getKey_id());
        holder.itemDescription.setText(note.getDescription());


    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public keyEntity getItem(int position) {
        return notes.get(position);
    }

    public void addTasks(List<keyEntity> newEntitys) {
        notes.clear();
        notes.addAll(newEntitys);
    }

    protected class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView itemTitle, itemDescription;
        public CustomViewHolder(View itemView) {
            super(itemView);

            itemTitle = itemView.findViewById(R.id.textView_item_title);
            itemDescription = itemView.findViewById(R.id.textView_description);
        }
    }
}
