package com.zoho.app.frontdesk.android.Keys_management.keyUtility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.widget.ImageView;

import constants.MainConstants;

public class keySingleton {

    private static keySingleton person = null;

    private String person_name;
    private String person_contact;
    private Bitmap bitmap_person_photo;
    private Bitmap bitmap_person_thumb_photo;
    private Bitmap bitmap_person_sign;
    private byte[] person_sign;
    private byte[] person_thumb_photo;
    private byte[] person_photo;
    private String signImage_path;
    private boolean is_phone_number;

    public static keySingleton getInstance() {
        if (person == null) {
            person = new keySingleton();
        }
        // Log.d("window", "SingletonObject");
        return person;
    }

    public keySingleton()
    {
        person_name = MainConstants.kConstantEmpty;
        person_contact = MainConstants.kConstantEmpty;
        bitmap_person_photo = null;
        bitmap_person_thumb_photo = null;
        bitmap_person_sign = null;
        person_photo = new byte[0];
        person_sign = new byte[0];
        person_thumb_photo = new byte[0];
        signImage_path = "";
        is_phone_number = false;
    }

    public void clearInstance()
    {
        person_name = MainConstants.kConstantEmpty;
        person_contact = MainConstants.kConstantEmpty;
        bitmap_person_photo = null;
        bitmap_person_thumb_photo = null;
        bitmap_person_sign = null;
        person_photo = new byte[0];
        person_sign = new byte[0];
        person_thumb_photo = new byte[0];
        signImage_path = "";
        is_phone_number = false;
    }

    public static keySingleton getPerson() {
        return person;
    }

    public static void setPerson(keySingleton person) {
        keySingleton.person = person;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getPerson_contact() {
        return person_contact;
    }

    public void setPerson_contact(String person_contact) {
        this.person_contact = person_contact;
    }

    public Bitmap getBitmap_person_photo() {
        return bitmap_person_photo;
    }

    public void setBitmap_person_photo(Bitmap bitmap_person_photo) {
        this.bitmap_person_photo = bitmap_person_photo;
    }

    public Bitmap getBitmap_person_thumb_photo() {
        return bitmap_person_thumb_photo;
    }

    public void setBitmap_person_thumb_photo(Bitmap bitmap_person_thumb_photo) {
        this.bitmap_person_thumb_photo = bitmap_person_thumb_photo;
    }

    public Bitmap getBitmap_person_sign() {
        return bitmap_person_sign;
    }

    public void setBitmap_person_sign(Bitmap bitmap_person_sign) {
        this.bitmap_person_sign = bitmap_person_sign;
    }

    public byte[] getPerson_sign() {
        return person_sign;
    }

    public void setPerson_sign(byte[] person_sign) {
        this.person_sign = person_sign;
    }

    public byte[] getPerson_thumb_photo() {
        return person_thumb_photo;
    }

    public void setPerson_thumb_photo(byte[] person_thumb_photo) {
        this.person_thumb_photo = person_thumb_photo;
    }

    public byte[] getPerson_photo() {
        return person_photo;
    }

    public void setPerson_photo(byte[] person_photo) {
        this.person_photo = person_photo;
    }

    public String getSignImage_path() {
        return signImage_path;
    }

    public void setSignImage_path(String signImage_path) {
        this.signImage_path = signImage_path;
    }

    public boolean Is_phone_number() {
        return is_phone_number;
    }

    public void setIs_phone_number(boolean is_phone_number) {
        this.is_phone_number = is_phone_number;
    }
}
