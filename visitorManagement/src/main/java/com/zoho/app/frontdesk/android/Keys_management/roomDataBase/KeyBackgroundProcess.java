package com.zoho.app.frontdesk.android.Keys_management.roomDataBase;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.zoho.app.frontdesk.android.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;

import utility.JSONParser;

public class KeyBackgroundProcess {
    DataBaseRepository dataBaseRepository;

    public KeyBackgroundProcess(Context context){
        dataBaseRepository = new DataBaseRepository(context);
        Log.e(" ******* ","KeyBackgroundProcess is running");
        RunInBackground runInBackground = new RunInBackground();
        runInBackground.execute();
    }

    class RunInBackground extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try{
                //runInBackground();
                //if error occurs then displayed in catch
                dataBaseRepository.removeAllKeys();
                JSONParser jsonParser = new JSONParser();
                String getUrl = "https://creator.zoho.com/api/json/keys-management-system/view/key_list_report?authtoken=a405e2f5b1479bb38f971a99f8a2c599&zc_ownername=frontdesk10&scope=creatorapi";
                URL url = new URL(getUrl);
                String response = jsonParser.restAPICall("GET",url,"");
                //JSONObject jsonObject = jsonParser.getJsonArrayInGetMethod(getUrl);
                //Log.e("Url Response ","restAPICall ******* "+response);
                response = response.replaceFirst("var zohofrontdesk10view1504 = ","");
                response = response.substring(0,response.length()-1);
                // Log.e("Url Response ","modified ******* "+response);

                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("key_list");
                String json_result = "";
                String spare;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject_keys = jsonArray.getJSONObject(i);
                    json_result = json_result+jsonObject_keys.getString("Key_id");

                    if(jsonObject_keys.getString("status").equalsIgnoreCase("Spare Available"))
                    {
                        spare = "spare";
                    }
                    else{
                        spare = "";
                    }
//                    dataBaseRepository.InsertKeys(jsonObject_keys.getString("Key_id"),
//                            jsonObject_keys.getString("key_title"),jsonObject_keys.getString("description"),
//                            spare, R.drawable.boardclip);
                    Integer key_icon = R.drawable.icons_key_white;
                    if(jsonObject_keys.getString("key_type").equalsIgnoreCase("Room Key"))
                    {
                        key_icon = R.drawable.icon_door;
                    }
                    else if(jsonObject_keys.getString("key_type").equalsIgnoreCase("Security Key"))
                    {
                        key_icon = R.drawable.icon_security;
                    }
                    else if(jsonObject_keys.getString("key_type").equalsIgnoreCase("Furniture Key"))
                    {
                        key_icon = R.drawable.icon_locker;
                    }
                    else if(jsonObject_keys.getString("key_type").equalsIgnoreCase("Vehicle Key"))
                    {
                        key_icon = R.drawable.icon_bike;
                    }
                    dataBaseRepository.InsertKeys(jsonObject_keys.getString("Key_id"),
                            jsonObject_keys.getString("key_title"),
                            jsonObject_keys.getString("description"),
                            key_icon,spare,
                            jsonObject_keys.getString("key_type"),
                            jsonObject_keys.getString("office_location"),
                            jsonObject_keys.getString("location_inside_office"),
                            jsonObject_keys.getString("security_details"),
                            jsonObject_keys.getString("vehicle_number"),
                            jsonObject_keys.getString("room_details"),
                            jsonObject_keys.getString("furniture_type"),
                            jsonObject_keys.getString("spare_key_count"),
                            jsonObject_keys.getString("status"),
                            jsonObject_keys.getString("is_active"));
                }

                Log.e("Url Response ","jsonObject_keys ******* "+json_result);
            }catch (Exception e){
                Log.e("Failed","******************* Try Again :-) : "+e.getMessage());
            }
            return null;
        }
    }
}

