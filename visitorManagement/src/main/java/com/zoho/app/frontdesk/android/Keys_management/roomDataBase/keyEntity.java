package com.zoho.app.frontdesk.android.Keys_management.roomDataBase;


import android.arch.persistence.room.*;

@Entity(tableName="key_list")
public class keyEntity{

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "key_id")
    private String key_id;

    @ColumnInfo(name = "key_title")
    private String key_title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "key_image")
    private int key_image;

    @ColumnInfo(name = "spare")
    private String spare;

    @ColumnInfo(name = "key_type")
    private String key_type;

    @ColumnInfo(name = "office_location")
    private String office_location;

    @ColumnInfo(name = "location_inside_office")
    private String location_inside_office;

    @ColumnInfo(name = "security_details")
    private String security_details;

    @ColumnInfo(name = "vehicle_details")
    private String vehicle_details;

    @ColumnInfo(name = "room_details")
    private String room_details;

    @ColumnInfo(name = "furniture_type")
    private String furniture_type;

    @ColumnInfo(name = "spare_key_count")
    private String spare_key_count;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "is_active")
    private String is_active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey_id() { return key_id; }

    public void setKey_id(String key_id) {
        this.key_id = key_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey_title() {
        return key_title;
    }

    public void setKey_title(String key_title) {
        this.key_title = key_title;
    }

    public void setKey_image(int key_image) {
        this.key_image = key_image;
    }

    public int getKey_image() {
        return key_image;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public String getKey_type() {
        return key_type;
    }

    public void setKey_type(String key_type) {
        this.key_type = key_type;
    }

    public String getOffice_location() {
        return office_location;
    }

    public void setOffice_location(String office_location) {
        this.office_location = office_location;
    }

    public String getLocation_inside_office() {
        return location_inside_office;
    }

    public void setLocation_inside_office(String location_inside_office) {
        this.location_inside_office = location_inside_office;
    }

    public String getSecurity_details() {
        return security_details;
    }

    public void setSecurity_details(String security_details) {
        this.security_details = security_details;
    }

    public String getVehicle_details() {
        return vehicle_details;
    }

    public void setVehicle_details(String vehicle_details) {
        this.vehicle_details = vehicle_details;
    }

    public String getRoom_details() {
        return room_details;
    }

    public void setRoom_details(String room_details) {
        this.room_details = room_details;
    }

    public String getFurniture_type() {
        return furniture_type;
    }

    public void setFurniture_type(String furniture_type) {
        this.furniture_type = furniture_type;
    }

    public String getSpare_key_count() {
        return spare_key_count;
    }

    public void setSpare_key_count(String spare_key_count) {
        this.spare_key_count = spare_key_count;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }
}
