package com.zoho.app.frontdesk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import appschema.VisitorOTPDetails;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import utility.NetworkConnection;
import utility.UsedCustomToast;
import zohocreator.VisitorZohoCreator;

/**
 * Created by Karthikeyan D S on 25July2018
 */
public class ActivityOTPDetails extends Activity {

    private TextView textViewOTPHeader, textViewErrorMessage, textViewResendOTP;
    private EditText editTextOTPNumber;
    private ImageView imageViewNextPage, imageViewBack, imageViewNextIcon, imageViewhome,imageViewPrevious;
    private RelativeLayout relativeLayoutOTPDetails;
    private Handler noActionHandler, resendOTPHandler;
    private TextToSpeech textToSpeech;
    private int applicationMode = NumberConstants.kConstantZero;
    private Typeface typeFaceTitle, typefaceVisitorContact, typefaceErrorMsg;
    private ProgressBar progressBarLoading;
    private int actionCode = MainConstants.backPress;
    private boolean isValidOTP = false;
    private VisitorZohoCreator creator;
    private NetworkConnection networkConnection;
    private int preApprovedMode = MainConstants.kConstantZero;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_otp_details);

//      assign Objects to the View.
        assignObjects();
    }

    /**
     * assign Objects
     *
     * @Author Karthikeyan D S
     * @Created on 25Jul2018
     */
    private void assignObjects() {
        textViewOTPHeader = (TextView) findViewById(R.id.textview_otp_title);
        textViewErrorMessage = (TextView) findViewById(R.id.textViewErrorInName);
        textViewResendOTP = (TextView) findViewById(R.id.textViewResendOTP);

        editTextOTPNumber = (EditText) findViewById(R.id.editTextOTPNumber);

        imageViewBack = (ImageView) findViewById(R.id.imageview_back);
        imageViewhome = (ImageView) findViewById(R.id.imageview_home);
        imageViewNextIcon = (ImageView) findViewById(R.id.imageview_next);
        imageViewNextPage = (ImageView) findViewById(R.id.imageview_next_page);
        imageViewPrevious= (ImageView) findViewById(R.id.imageview_previous);

        relativeLayoutOTPDetails = (RelativeLayout) findViewById(R.id.relativelayout_otp_details);
        progressBarLoading = (ProgressBar) findViewById(R.id.progressbar_report);

        noActionHandler = new Handler();
        resendOTPHandler = new Handler();
//        set no Action Handler delay for 5 minutes because of otp needs to received in Phone.
        noActionHandler.postDelayed(noActionThread, 300000);

//        resendOTPHandler.postDelayed(resendOTPThread, 150000);

        if (SingletonMetaData.getInstance() != null) {
            applicationMode = SingletonMetaData.getInstance().getApplicationMode();
        }

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        textViewErrorMessage.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText("Enter OTP received in your entered mobile number ");

//        editTextOTPNumber.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

//        SetFontConstants.
        setFontConstants();

//        AssignListeners.
        assignListeners();

//        setAppBackground.
        setAppBackground();

        if(applicationMode==NumberConstants.kConstantDataCentreMode) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                preApprovedMode = bundle.getInt("Mode");
            }
        }

//        Resend OTP View Enable.
        resendOTPToMobile();
    }

    /**
     * Set FontConstants
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void setFontConstants() {
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceErrorMsg = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskItalic);
        typefaceVisitorContact = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);

        editTextOTPNumber.setTypeface(typefaceVisitorContact);
        textViewErrorMessage.setTypeface(typefaceErrorMsg);
        textViewOTPHeader.setTypeface(typeFaceTitle);
        textViewResendOTP.setTypeface(typefaceErrorMsg);
    }

    /**
     * Set background color
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutOTPDetails.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFba6fb6, 0xFF8e76c3, 0xFF8478c6, 0xFF8278c7});
                relativeLayoutOTPDetails.setBackground(gd);
            }
        }
    }

    /**
     * Assign Listeners
     *
     * @author Karthikeyan D S
     * @created on 27Jul2018
     */
    private void assignListeners() {
        // Onclick listeners
        if (imageViewNextPage != null && listenerConfirm != null) {
            imageViewNextPage.setOnClickListener(listenerConfirm);
        }
        if (imageViewNextIcon != null && listenerConfirm != null) {
            imageViewNextIcon.setOnClickListener(listenerConfirm);
        }
        if (imageViewBack != null && listenerBack != null) {
            imageViewBack.setOnClickListener(listenerBack);
        }
        if (imageViewPrevious != null && listenerBack != null) {
            imageViewPrevious.setOnClickListener(listenerBack);
        }
        if (textViewResendOTP != null && listenerResendOTP != null) {
            textViewResendOTP.setOnClickListener(listenerResendOTP);
        }

        editTextOTPNumber.setOnEditorActionListener(listenerDonePressed);
        editTextOTPNumber.addTextChangedListener(textWatcherNameChanged);
    }

    /**
     * textWatcher to validate the VisitorMobileNo
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextWatcher textWatcherNameChanged = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            validateOTPTyped();
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
//            resendOTPHandler.removeCallbacks(resendOTPThread);
//            resendOTPHandler.postDelayed(resendOTPThread, 100000);
        }

        public void afterTextChanged(Editable s) {
        }

    };

    /**
     * Check is existing user or not
     *
     * @author Karthikeyan D S
     * @created 26JUL2018
     */
    private void validateOTPTyped() {
        String editTextOTP = editTextOTPNumber.getText().toString().trim();
        if ((editTextOTP != null)
                && (!editTextOTP.isEmpty())
                && (editTextOTP.length() > MainConstants.kConstantZero)) {
            if (editTextOTP != null
                    && editTextOTP.length() >= MainConstants.kConstantThree) {
                if (imageViewNextPage != null) {
                    imageViewNextPage.setVisibility(View.VISIBLE);
                }
                if (imageViewNextIcon != null) {
                    imageViewNextIcon.setVisibility(View.VISIBLE);
                }
            } else {
                if (imageViewNextPage != null) {
                    imageViewNextPage.setVisibility(View.GONE);
                }
                if (imageViewNextIcon != null) {
                    imageViewNextIcon.setVisibility(View.GONE);
                }
            }
        } else {
            textViewErrorMessage.setVisibility(View.VISIBLE);
            textViewErrorMessage.setText("Enter OTP Received in Your Mobile");
        }
    }

    /**
     * Check is existing user or not
     *
     * @author Karthikeyan D S
     * @created 26JUL2018
     */
    public void validateOTP() {
        String editTextOTPValue = editTextOTPNumber.getText().toString().trim();
        if (networkConnection == null) {
            networkConnection = new NetworkConnection();
        }
        if (networkConnection.wifiConnected(getApplicationContext())) {
            if (editTextOTPValue != null && !editTextOTPValue.isEmpty()
                    && (editTextOTPValue.length() > MainConstants.kConstantZero)) {
                textViewResendOTP.setEnabled(false);
                if (progressBarLoading != null) {
                    progressBarLoading.setVisibility(View.VISIBLE);
                }
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setEntredOTPNumber(editTextOTPValue);
                }
                BackGroundProcess backgroundProcess = new BackGroundProcess();
                if (backgroundProcess != null) {
                    backgroundProcess.execute();
                }
            }
        } else {
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, "Network Not Available,Please Connect to the Network", Toast.LENGTH_SHORT, Gravity.TOP).show();
        }
    }


    /**
     * Assign text to voice listener
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * Play text to voice
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void textSpeach(int textSpeachCode) {
        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (editTextOTPNumber != null
                    && editTextOTPNumber.getText() != null && editTextOTPNumber
                    .getText().toString().length() <= 0)) {
                textToSpeech.speak(TextToSpeachConstants.speachOTP,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
            else if (textSpeachCode == MainConstants.kConstantTwo) {
                textToSpeech.speak(TextToSpeachConstants.speachResendOTP,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Hide keyboard
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * intent to launch previous page
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            hideKeyboard();
            launchPreviousPage();
        }
    };


    /**
     * when the done pressed then process start.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private TextView.OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                validateOTP();
                return true;
            }
            return false;
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private View.OnClickListener listenerConfirm = new View.OnClickListener() {
        public void onClick(View v) {
            validateOTP();
        }
    };

    /**
     * Resend OTP Request
     *
     * @author Karthikeyan D S
     * @created on 27Jul2018
     */
    private View.OnClickListener listenerResendOTP = new View.OnClickListener() {
        public void onClick(View v) {
            resendOTPRequest();
        }
    };

    /**
     * Resend OTP Request
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void resendOTPRequest() {
        if(networkConnection==null)
        {
            networkConnection = new NetworkConnection();
        }
        if (networkConnection.wifiConnected(getApplicationContext())) {
            if (progressBarLoading != null) {
                progressBarLoading.setVisibility(View.VISIBLE);
            }
            BackgroundProcessSendOTPRequest backgroundProcessSendOTPRequest = new BackgroundProcessSendOTPRequest();
            backgroundProcessSendOTPRequest.execute();
        } else {
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, "Network Not Available,Please Connect to the Network", Toast.LENGTH_SHORT, Gravity.TOP).show();
        }
    }

    /**
     * send OTP Request to Creator.
     *
     * @author Karthikeyan D S
     * @created on 26JUL2018
     */
    private boolean sendOTPDetailstoCreator() {
        boolean sendOTPDetailstoCreator = false;
        String mCreatorID = MainConstants.kConstantEmpty;
        String mobileNumber = MainConstants.kConstantEmpty;
        if (SingletonVisitorProfile.getInstance() != null) {
            mobileNumber = SingletonVisitorProfile.getInstance().getContact();
            if (mobileNumber != null && !mobileNumber.isEmpty()) {
                creator = new VisitorZohoCreator(this);
                if (creator != null) {
                    mCreatorID = creator.sendOTPDetailsCreator(this, mobileNumber);
                }
                if (mCreatorID != null && !mCreatorID.isEmpty()) {
                    if (SingletonVisitorProfile.getInstance() != null) {
                        SingletonVisitorProfile.getInstance().setOTPRequestCreatorID(mCreatorID);
                    }
                    sendOTPDetailstoCreator = true;
                } else {
                    sendOTPDetailstoCreator = false;
                }
            } else {
//            Mobile Number is Empty.
                sendOTPDetailstoCreator = false;
            }
        }
        return sendOTPDetailstoCreator;
    }

    /**
     * Launch previous activity
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void launchHomePage() {
        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, HomePage.class);
            startActivity(intent);
        }
        finishAffinity();
    }

    /**
     * Launch Previous Page of the activity
     *
     * @author Karthickeyan D S
     * @created on 25Jul2018
     */
    private void launchPreviousPage() {
        Intent intent = new Intent(this, ExistingVisitorActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    /**
     * Launch Previous Page of the activity
     *
     * @author Karthickeyan D S
     * @created on 25Jul2018
     */
    private void launchNextPage() {
        if(preApprovedMode==1) {
            Intent intent = new Intent(this, ActivityVisitorName.class);
            startActivity(intent);
        }
        else if(preApprovedMode==2)
        {
            float density = getResources().getDisplayMetrics().density;
//            Log.d("DSK Density",""+density);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && density < 2) {
//                Log.d("DSK Density","in 1 "+density);
                Intent intent = new Intent(getApplicationContext(),
                        ActivityCameraTwoApi.class);
                startActivity(intent);
            } else {
//                Log.d("DSK Density","in 2 "+density);
                Intent intent = new Intent(getApplicationContext(),
                        GetImageActivity.class);
                startActivity(intent);
            }
        }
        else
        {
            Intent intent = new Intent(this, ActivityVisitorName.class);
            startActivity(intent);
        }
        finishAffinity();
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };

    /**
     * Resend OTP Handler to Send OTP Again
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    public Runnable resendOTPThread = new Runnable() {
        public void run() {
            resendOTPToMobile();
        }
    };

    /**
     * Show textView Option to Resend OTP option
     *
     * @author Karthikeyan D S
     * @created on 27Jul2018
     */
    public void resendOTPToMobile() {
        if (noActionHandler != null) {
            noActionHandler.postDelayed(noActionThread, 200000);
        }
        if (textViewResendOTP != null) {
//            textViewResendOTP.setVisibility(View.VISIBLE);
//            textSpeach(MainConstants.kConstantTwo);
            textViewErrorMessage.setVisibility(View.GONE);
        }
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * This function is called, when the activity is stoped.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    public void onStop() {
        super.onStop();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * This function is called, when the activity is destroyed to deallocate object data.
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void deallocateObjects() {
        typeFaceTitle = null;
        textViewErrorMessage = null;
        editTextOTPNumber = null;
        textViewOTPHeader = null;
        relativeLayoutOTPDetails = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
//        if (resendOTPHandler != null) {
//            resendOTPHandler.removeCallbacks(resendOTPThread);
//            noActionHandler = null;
//        }
        imageViewBack = null;
        imageViewNextIcon = null;
        imageViewNextPage = null;
        imageViewhome = null;
        actionCode =  NumberConstants.kConstantZero;
        applicationMode =  NumberConstants.kConstantZero;
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        isValidOTP = false;
    }

    /**
     * background process for upload app
     */
    public class BackGroundProcess extends AsyncTask<String, String, String> {
        Context context;
        private int isValidOTP = NumberConstants.kConstantZero;

        protected void onPreExecute() {
            super.onPreExecute();
            enableFields(false);
        }

        protected String doInBackground(String... f_url) {
            isValidOTP = validateOTPDetailsFromCreator();
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {

        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String responseParam) {
            if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                Log.d("DSK OTP ", " isValidOTP " + isValidOTP);
            }
            if (progressBarLoading != null) {
                progressBarLoading.setVisibility(View.GONE);
            }
            enableFields(true);
            if (isValidOTP == 1) {
                launchNextPage();
            } else if (isValidOTP == 2) {
//                OTP is Expired.
                showErrorDialog(2);
            } else if (isValidOTP == 3) {
//                OTP is Not Valid
                showErrorDialog(3);
            } else if (isValidOTP == 4) {
//                Details Not Found Empty Response.
                showErrorDialog(4);
            } else if (isValidOTP == 5) {
//                Details Not Found Empty Response.
                showErrorDialog(5);
            } else if (isValidOTP == -1) {
//                Singleton OTPRequest Creator Id Missing.
                showErrorDialog(-1);
            }
        }
    }

    /**
     * enable/disable fields based on user input
     *
     * @author Karthikeyan D S
     * @created on 26Jul2018
     */
    private void enableFields(boolean isEnabled) {
        if (editTextOTPNumber != null) {
            editTextOTPNumber.setEnabled(isEnabled);
        }
        if (imageViewBack != null) {
            imageViewBack.setEnabled(isEnabled);
        }
        if (imageViewhome != null) {
            imageViewhome.setEnabled(isEnabled);
        }
        if (imageViewNextIcon != null) {
            imageViewNextIcon.setEnabled(isEnabled);
        }
        if (imageViewNextPage != null) {
            imageViewNextPage.setEnabled(isEnabled);
        }
    }

    /**
     * Validate Enter OTP Details is correct in Zoho Creator
     *
     * @author Karthikeyan D S
     * @created on 27Jul2018
     */
    private int validateOTPDetailsFromCreator() {
        int isOTPValid = NumberConstants.kConstantMinusOne;
        if (SingletonVisitorProfile.getInstance() != null) {
            String otpRequestCreatorID = SingletonVisitorProfile.getInstance().getOTPRequestCreatorID();
            if (otpRequestCreatorID != null && !otpRequestCreatorID.isEmpty()) {
                isOTPValid = getOTPDetails(otpRequestCreatorID);
            } else {
//                Creator ID from Singleton Missing.
            }
        }
        return isOTPValid;
    }

    /**
     * get OTP Response and Validate OTP
     *
     * @author Karthikeyan D S
     * @created on 27Jul2018
     */
    private int getOTPDetails(String otpRequestCreatorID) {
        int validateOTPDetails = NumberConstants.kConstantZero;
        VisitorZohoCreator visitorZohoCreator = new VisitorZohoCreator(getApplicationContext());
        VisitorOTPDetails visitorOTPDetails = new VisitorOTPDetails();
        String editTextOTP = MainConstants.kConstantEmpty;
        if (visitorZohoCreator != null) {
            try {
                visitorOTPDetails = visitorZohoCreator.checkOTPIsValidinCreartor(getApplicationContext(), otpRequestCreatorID);
                if (visitorOTPDetails != null) {
//                    Get Current Date and todays date start values as long to validate otp.
                    long getTodaysStartDateTimeLong = getCurrentTimeinMillisecods(1);
                    long getCurrentDateTimeLong = getCurrentTimeinMillisecods(2);

                    if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                        Log.d("DSK OTP ", " OTPValidityLong " + visitorOTPDetails.getmOTPValidityLong() + " getTodaysStartDateTimeLong " + getTodaysStartDateTimeLong + " getCurrentDateTimeLong " + getCurrentDateTimeLong);
                    }
                    if (visitorOTPDetails.getmRequestedOTPNumber() != null && visitorOTPDetails.getmOTPValidity() != null
                            && visitorOTPDetails.getmOTPValidityLong() > 0) {
                        if (visitorOTPDetails.getmOTPValidityLong() > getTodaysStartDateTimeLong && visitorOTPDetails.getmOTPValidityLong() > getCurrentDateTimeLong) {
                            if (SingletonVisitorProfile.getInstance() != null) {
                                editTextOTP = SingletonVisitorProfile.getInstance().getEntredOTPNumber();
                            }
                            if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                                Log.d("DSK OTP ", " editTextOTP " + editTextOTP);
                            }
                            if (editTextOTP != null && !editTextOTP.isEmpty()) {
//                                if need to bypass otp just give 922892
                                if (visitorOTPDetails.getmRequestedOTPNumber().equals(editTextOTP)
                                        || editTextOTP.equals(StringConstants.kConstantappPasswordOTPAdminLevel)) {
//                                   OTP Valid
                                    validateOTPDetails = NumberConstants.kConstantOTPValid;
                                } else {
//                                    OTP Not Match.
                                    validateOTPDetails = NumberConstants.kConstantOTPNotMatch;
                                }
                            } else {
//                                OTP Not Match.
                                validateOTPDetails =  NumberConstants.kConstantOTPNotMatch;
                            }
                        } else if (visitorOTPDetails.getmOTPValidityLong() < getCurrentDateTimeLong) {
//                            OTP is Expired.
                            validateOTPDetails = NumberConstants.kConstantOTPExpired;
                        } else {
//                            OTP is Not Valid
                            validateOTPDetails = NumberConstants.kConstantOTPNotValid;
                        }
                    } else {
//                        Details Not Found Empty Response.
                        validateOTPDetails = NumberConstants.kConstantOTPResponseEmpty;
                    }
                }
            } catch (IOException ioException) {
            }
        }
//        if bypass otp UNTAG below code
//        validateOTPDetails = 1;
        return validateOTPDetails;
    }

    /**
     * get Current Time in Milliseconds
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private long getCurrentTimeinMillisecods(int timeCode) {
        long currentTime = NumberConstants.kConstantMinusOne;

        Calendar calendar = Calendar.getInstance();

        if (timeCode == 1) {
//            Set current Date as Morning 00:00:00 AM
            calendar.set(calendar.HOUR, 00);
            calendar.set(calendar.MINUTE, 00);
            calendar.set(calendar.SECOND, 00);
            calendar.set(calendar.AM_PM, Calendar.AM);
            currentTime = calendar.getTimeInMillis();
        } else if (timeCode == 2) {
//           Get Current Date and Time
            currentTime = calendar.getTimeInMillis();
        }
        return currentTime;
    }

    /**
     * Show ErrorDialog based on the code
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    public void showErrorDialog(int code) {
//        Log.d("DSK ","OTP Code "+code);
        if (code == NumberConstants.kConstantOTPExpired) {
            launchPreviousPage();
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, StringConstants.kConstantMessageOTPExpired, Toast.LENGTH_SHORT, Gravity.TOP).show();
        } else if (code == NumberConstants.kConstantOTPNotValid) {
            textViewErrorMessage.setVisibility(View.VISIBLE);
            textViewErrorMessage.setText(StringConstants.kConstantMessageOTPNotValid);
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, StringConstants.kConstantMessageOTPNotValid, Toast.LENGTH_SHORT, Gravity.TOP).show();
        } else if (code == NumberConstants.kConstantOTPResponseEmpty) {
            launchPreviousPage();
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, StringConstants.kConstantMessageOTPNotAbletoProcess, Toast.LENGTH_SHORT, Gravity.TOP).show();
        } else if (code == NumberConstants.kConstantOTPNotMatch) {
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, StringConstants.kConstantMessageOTPWrong, Toast.LENGTH_SHORT, Gravity.TOP).show();
            textViewErrorMessage.setVisibility(View.VISIBLE);
            textViewErrorMessage.setText(StringConstants.kConstantMessageOTPWrong);
        } else if (code == -1) {
            launchPreviousPage();
//            If Creator Id for OTP Request is Missing from singleton.
            UsedCustomToast.makeCustomToast(ActivityOTPDetails.this, StringConstants.kConstantMessageOTPSessionExpired, Toast.LENGTH_SHORT, Gravity.TOP).show();
        }
    }


    /*
     * background process for upload app settings details
     */
    public class BackgroundProcessSendOTPRequest extends AsyncTask<String, String, String> {
        Context context;
        String contact;
        private int isOTPRequestSend = 0;

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... f_url) {
            if (sendOTPDetailstoCreator()) {
//               OTP Request Sent.
                isOTPRequestSend = 1;
            } else {
//               Error OTP Request Not Sent to Creator.
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String responseParam) {
            progressBarLoading.setVisibility(View.GONE);
            if (isOTPRequestSend == 1) {
                showMessage(1);
            }
        }
    }

    private void showMessage(int requestSucessCode) {
        if (requestSucessCode == 1) {
            UsedCustomToast.makeCustomToast(this, "OTP resent", Toast.LENGTH_SHORT, Gravity.TOP).show();
        } else {

        }
    }
}
