package com.zoho.app.frontdesk.android.stationery;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.BuildConfig;
import com.zoho.app.frontdesk.android.R;
import appschema.SingletonVisitorProfile;
import com.zoho.app.frontdesk.android.WelcomeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonStationeryDetails;
import constants.DataBaseConstants;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import appschema.StationeryRequest;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import database.dbhelper.DbHelper;
import database.dbschema.Stationery;
import database.dbschema.StationeryEntry;
import logs.InternalAppLogs;
import utility.DialogView;
import utility.JSONParser;
import utility.NetworkConnection;
import appschema.SharedPreferenceController;
import utility.TimeZoneConventer;
import utility.UsedCustomToast;
import zohocreator.VisitorZohoCreator;

/**
 * Created by Karthikeyan D S on 19/06/18.
 */

public class ActivityStationeryDragDropQuantity extends Activity {

    private Button buttonStationerySubmit, buttonStationeryCancel, buttonStationeryBack, tellUs;
    private RelativeLayout relativeLayoutMain;
    private ListView selectedStationeryItemDetailsListView;
    private TextView noStationeryItemsLogTextView, stationeryItemHeaderTextView;
    private ImageView imageViewNextPage, imageViewPreviousPage;
    private Typeface fontPoppinsBold, fontPoppinsMedium, fontPoppinsSemiBold;
    private HashMap<Integer, StationeryEntry> selectedStationeryListFinal;
    private TextView textViewDialogHomeMessage, textViewHomeMessageConfirm,
            textViewHomeMessageCancel, textViewReportMsg,
            textViewReportFirstMsg, textViewReportErrorCode;
    private VisitorZohoCreator creator;
    private Dialog dialogGetHomeMessage, reportDialog;
    private Handler noActionHandler, handlerHideDialog;
    private DbHelper dbHelper;
    private long currentTime = 0L;
    Context context;
    private int chooseOption = 0;
    private Boolean IsallowedToClickStationaryItems = true;
    private ProgressBar progressBar;
    private RelativeLayout relativeLayout;
    private View seperator;
    private ImageView reportImage;
    private View dragDropViewItem = null;
    private ListviewAdapter mListViewAdapterItemQuantity;
    private ListView mListViewSelectedItemList;
    private ArrayList<Stationery> itemQuantityList = new ArrayList<Stationery>();
    private boolean isBackPressEnabled = true;
    private boolean isSaveData = false;
    private boolean isItemAddListenerPressEnabled = true;
    private TextToSpeech textToSpeech;
    private SharedPreferenceController sharedPreferenceController = new SharedPreferenceController();
    private String mdevicelocation = MainConstants.kConstantEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_item_quantity_details);

        assignObjects();

        setAdapterView();
    }

    /**
     * Assign Objects
     *
     * @author Karthikeyan D S
     * @created on 19/06/18.
     */
    private void assignObjects() {
        selectedStationeryItemDetailsListView = (ListView) findViewById(R.id.list_view_selected_item_stationery);

        noStationeryItemsLogTextView = (TextView) findViewById(R.id.stationery_quantity_no_items_log_text_view);
        stationeryItemHeaderTextView = (TextView) findViewById(R.id.stationery_quantity_heading_text_view);

        mListViewSelectedItemList = (ListView) findViewById(R.id.list_view_selected_item_stationery);
        relativeLayoutMain = (RelativeLayout) findViewById(R.id.relativeLayout_main);
//        imageViewNextPage = (ImageView) findViewById(R.id.image_view_next_page);
//        imageViewPreviousPage = (ImageView) findViewById(R.id.image_view_previous_page);

        buttonStationerySubmit = (Button) findViewById(R.id.button_confirm);
        buttonStationeryCancel = (Button) findViewById(R.id.button_cancel);
        buttonStationeryBack = (Button) findViewById(R.id.button_back);

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        selectedStationeryListFinal = new HashMap<Integer, StationeryEntry>();

        creator = new VisitorZohoCreator(this);

        dbHelper = new DbHelper(this);

        handlerHideDialog = new Handler();
        noActionHandler = new Handler();

        mdevicelocation = sharedPreferenceController.getDeviceLocation(getApplicationContext());

        loadSelectedItemList();

        //Assign Font Style.
        assignFontConstants();
        assignListeners();
    }

    /**
     * Assign Font Style
     *
     * @author Karthikeyan D S
     * @created on 19/06/18.
     */
    private void assignFontConstants() {
        fontPoppinsBold = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsBold);
        fontPoppinsMedium = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsMedium);
        fontPoppinsSemiBold = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsSemiBold);

        if (buttonStationerySubmit != null && fontPoppinsSemiBold != null) {
            buttonStationerySubmit.setTypeface(fontPoppinsSemiBold);
        }
        if (buttonStationeryCancel != null && fontPoppinsSemiBold != null) {
            buttonStationeryCancel.setTypeface(fontPoppinsSemiBold);
        }
        if (buttonStationeryBack != null && fontPoppinsSemiBold != null) {
            buttonStationeryBack.setTypeface(fontPoppinsSemiBold);
        }

        if (noStationeryItemsLogTextView != null && fontPoppinsSemiBold != null) {
            noStationeryItemsLogTextView.setTypeface(fontPoppinsSemiBold);
        }
        if (stationeryItemHeaderTextView != null && fontPoppinsSemiBold != null) {
            stationeryItemHeaderTextView.setTypeface(fontPoppinsSemiBold);
        }
    }

    /**
     * Assign Listeners
     *
     * @author Karthikeyan D S
     * @created on 04JUL2018
     */
    private void assignListeners() {
        if (buttonStationerySubmit != null && listenerSubmit != null) {
            buttonStationerySubmit.setOnClickListener(listenerSubmit);
        }
        if (buttonStationeryBack != null && listenerBack != null) {
            buttonStationeryBack.setOnClickListener(listenerBack);
        }
        if (buttonStationeryCancel != null && listenerCancel != null) {
            buttonStationeryCancel.setOnClickListener(listenerCancel);
        }
    }


    /**
     * Load and assign Selected item List Values
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private void loadSelectedItemList() {
//        Log.d("DSK","selectedStationeryCodesize "+SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails().size());
        if (SingletonStationeryDetails.getStationeryInstance() != null &&
                SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails() != null
                && SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails().size() > 0) {
//            Log.d("DSK","selectedStationeryCodeIN ");
            Map<Long, Integer> selectedStationeryCode = new HashMap<Long, Integer>();
            Stationery stationeryDetails = new Stationery();
            selectedStationeryCode = SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails();
            if (dbHelper == null) {
                dbHelper = new DbHelper(this);
            }

            if (selectedStationeryCode != null && selectedStationeryCode.size() > 0) {
                noStationeryItemsLogTextView.setVisibility(View.GONE);
                for (Map.Entry<Long, Integer> entry : selectedStationeryCode.entrySet()) {
                    long stationeryRecordId = entry.getKey();
                    int stationeryCode = entry.getValue();
                    int Quantity = -1;
                    if (SingletonStationeryDetails.getStationeryInstance() != null &&
                            SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryQuantityDetails().size() > 0
                            && SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryQuantityDetails() != null &&
                            SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryQuantityDetails().get(stationeryCode) != null) {
                        Quantity = SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryQuantityDetails().get(stationeryCode);
//                        Log.d("DSK1 ", "Singleton " + Quantity + " " + stationeryCode);
                    }
//                    Log.d("DSK1 ", " " + stationeryRecordId + " " + stationeryCode);
                    if (dbHelper != null) {
                        if (mdevicelocation != null && !mdevicelocation.isEmpty()) {
                            stationeryDetails = dbHelper.getAllStationeryBasedOnStationeryCode(stationeryCode, mdevicelocation);
                        } else {
                            UsedCustomToast.makeCustomToast(this, "Device Location Not Set,Please Set", Toast.LENGTH_SHORT, Gravity.TOP).show();
                        }
//                        Log.d("DSK", " " + stationeryDetails.getStationeryName() + " " + stationeryDetails.getQuantity());
                    }

                    if (stationeryDetails != null && Quantity >= 0) {
                        stationeryDetails.setQuantity(Quantity);
                    }
//                    Log.d("DSK2 ", " " + stationeryDetails.getRecordId() + " " + stationeryDetails.getQuantity());
                    itemQuantityList.add(stationeryDetails);
                }
            } else {
                noStationeryItemsLogTextView.setVisibility(View.VISIBLE);
                noStationeryItemsLogTextView.setText("Item details Not Present in the List!");
            }
        }
    }

    /**
     * Function to get drag and dropped Selected Item Lists and upload to creator.
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    public void getSelectedItemList() {
        String insertedStationeryRequestResponseMessage = MainConstants.kConstantEmpty;
        if (!isNetworkAvailable()) {
            noActionHandler.removeCallbacks(noActionThread);
            TimeZoneConventer connection = new TimeZoneConventer();
            if ( !connection.isWiFiEnabled(getApplicationContext())) {
                connection.wifiReconnect(getApplicationContext());
            }
            chooseOption = 0;
            utility.UsedCustomToast.makeCustomToast(
                    getApplicationContext(),
                    StringConstants.kConstantErrorMsgInNetworkError,
                    Toast.LENGTH_LONG, Gravity.TOP).show();
            utility.UsedCustomToast.makeCustomToast(
                    getApplicationContext(),
                    StringConstants.kConstantErrorMsgInNetworkError,
                    Toast.LENGTH_LONG, Gravity.TOP).show();

        } else {
            currentTime = System.currentTimeMillis();
            if ((selectedStationeryListFinal != null) && (selectedStationeryListFinal.size() > 0)) {
                disableProcess();
                StationeryRequest request = new StationeryRequest();
                request.setEmployeeId(SingletonEmployeeProfile
                        .getInstance().getEmployeeId());
                request.setRequestTime(currentTime);
                if (dbHelper == null) {
                    dbHelper = new DbHelper(getApplicationContext());
                }
                insertedStationeryRequestResponseMessage = dbHelper.insertStationeryRequest(request);
//                Log.d("keyyyyy", "stationery request" + request);

                if (insertedStationeryRequestResponseMessage != "") {
                    sendFailureRequest(insertedStationeryRequestResponseMessage, ErrorConstants.SqliteError);
                    uploadStationeryItemEntryFailure(ErrorConstants.SqliteError);
                } else {
                    Set<Integer> keys = selectedStationeryListFinal.keySet();
                    for (Integer key : keys) {
//								System.out.println("keyyyyy"+key);
//                        Log.d("keyyyyy", "" + key);
                        StationeryEntry entry = (StationeryEntry) selectedStationeryListFinal
                                .get(key);
                        insertedStationeryRequestResponseMessage = dbHelper.insertStationeryEntry(entry);
//                        Log.d("keyyyyy", "stationery" + entry.getRequestId());
//                        Log.d("keyyyyy", "stationery" + entry.getCreatorId());
//                        Log.d("keyyyyy", "stationery" + entry.getQuantity());
//                        Log.d("keyyyyy", "stationery" + entry.getStationeryItemCode());
                        if (insertedStationeryRequestResponseMessage != MainConstants.kConstantEmpty) {
                            break;
                        }
                    }
                    if (insertedStationeryRequestResponseMessage != MainConstants.kConstantEmpty) {
                        sendFailureRequest(insertedStationeryRequestResponseMessage,
                                ErrorConstants.SqliteError);
                        uploadStationeryItemEntryFailure(ErrorConstants.SqliteError);
                    } else {
                        uploadStationeryItemEntry(keys);
                        // disable backbutton, cancel and confirm button
                        // for not allowed to changes and waiting for
                        // response
                        setEnableViews(false);
                        IsallowedToClickStationaryItems = false;
                    }
                }
            } else {
                initializeErrorDialog();
                showErrorDialogBox();
//                 dialog.hideDialog();
//                 dialog.showReportDialog(StationeryListActivity.this,MainConstants.selectAtleastOneItem,MainConstants.reportNoItemSelection);
//                chooseOption = 0;
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTime);
            }

        }
    }

    /**
     * Set the Data Adapter
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private void setAdapterView() {
        //to set left side List View characters
        if (mListViewAdapterItemQuantity == null) {
            mListViewAdapterItemQuantity = new ActivityStationeryDragDropQuantity.ListviewAdapter(ActivityStationeryDragDropQuantity.this, R.layout.stationery_item_quantity_details_card_view, itemQuantityList);
            // Save the ListView state (= includes scroll position) as a Parcelable
            Parcelable state = mListViewSelectedItemList.onSaveInstanceState();
            // Restore previous state (including selected item index and scroll position)
            if (state != null) {
                mListViewSelectedItemList.onRestoreInstanceState(state);
            }
            mListViewSelectedItemList.setAdapter(mListViewAdapterItemQuantity);

        } else {
            mListViewAdapterItemQuantity.notifyDataSetChanged();
        }
    }

    /**
     * intent to launch the home page.
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private void getHomePage() {
        Intent intent = new Intent(getApplicationContext(),
                WelcomeActivity.class);
        startActivity(intent);
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
        }
        SingletonStationeryDetails.getStationeryInstance().clearInstance();
        DialogView.hideDialog();
        finish();

    }

    /**
     * Check's Network is Available to upload Stationery Details.
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager!=null)
        {
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        return false;
    }

    /**
     * Upload Stationery Details to creator.
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private void uploadStationeryItemEntry(Set<Integer> keys) {
        JSONArray array = new JSONArray();
        String entryItem = MainConstants.kConstantEmpty;
        String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String deviceLocation = SingletonVisitorProfile.getInstance().getDeviceLocation();
        String appVersionCode = BuildConfig.VERSION_NAME;
        for (Integer key : keys) {
            System.out.println(key);
            StationeryEntry entry = (StationeryEntry) selectedStationeryListFinal.get(key);
            JSONObject object = new JSONObject();
            try {
                object.put("code", entry.getStationeryItemCode());
                object.put("quantity", entry.getQuantity());
            } catch (JSONException e) {

            }
            array.put(object);
            object = null;
            // if(entryItem !=""){
            // entryItem =entryItem+ "_";
            // }
            // entryItem = entryItem + entry.getStationeryItemCode() + ":"+
            // entry.getQuantity();

        }

        JSONObject stationeryEntryObject = new JSONObject();
        try {
            stationeryEntryObject.put("StationeryItemEntry", array);
            stationeryEntryObject.put("device_id", deviceId);
            stationeryEntryObject.put("device_location", deviceLocation);
            stationeryEntryObject.put("app_version", appVersionCode);
            stationeryEntryObject.put( "visitor_office_location",SingletonVisitorProfile.getInstance()
                    .getOfficeLocation());
            stationeryEntryObject.put("visitor_office_location_code",SingletonVisitorProfile.getInstance()
                    .getLocationCode());
        } catch (JSONException e) {

        }
//        entryItem = stationeryEntryObject.toString();
        // Log.d("item", "entry" + entryItem);
        String requestParameter = MainConstants.kConstantCreatorAuthTokenAndScope
                // +DataBaseConstants.addStationeryRequestTime
                // +TimeZoneConventer.getDate(currentTime)
                + DataBaseConstants.addStationeryEmployeeId
                + SingletonEmployeeProfile.getInstance().getEmployeeId()
                + DataBaseConstants.addStationeryEmployeeName
                + SingletonEmployeeProfile.getInstance().getEmployeeName()
                + DataBaseConstants.addStationeryEmployeeEmailId
                + SingletonEmployeeProfile.getInstance().getEmployeeMailId()
                + DataBaseConstants.addStationeryEmployeePhoneNo
                + SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
                + DataBaseConstants.addStationeryEmployeeZuid
                + SingletonEmployeeProfile.getInstance().getEmployeeZuid()
                + DataBaseConstants.addStationeryEmployeeLocation
                + SingletonEmployeeProfile.getInstance()
                .getEmployeeSeatingLocation()
                + DataBaseConstants.addStationeryRequestId
                + SingletonEmployeeProfile.getInstance().getEmployeeId() + "_"
                + TimeZoneConventer.getDate(currentTime)
                + DataBaseConstants.addStationeryEntry
                + stationeryEntryObject.toString();
        // + DataBaseConstants.addStationeryEntry + "StationeryItemEntry:[1]";
        utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
                StringConstants.kConstantMsgInStationeryItemSendRequest,
                Toast.LENGTH_LONG, Gravity.TOP).show();
        // sendVolleyStringRequest(
        // (MainConstants.applicationStationeryApp
        // + MainConstants.kConstantAddStationeryRequestRecord +
        // requestParameter)
        // .replace(" ", "%20"), "StationeryRequest");
        ActivityStationeryDragDropQuantity.BackgroundProcess backgroundProcess = new ActivityStationeryDragDropQuantity.BackgroundProcess(
                requestParameter);
        backgroundProcess.execute();
    }

    private void sendFailureRequest(String msg, int code) {
        String requestParameter = MainConstants.kConstantCreatorAuthTokenAndScope
                // +DataBaseConstants.addStationeryRequestTime
                // +TimeZoneConventer.getDate(currentTime)
                + DataBaseConstants.addLogCode + code
                + DataBaseConstants.addLogMsg + msg;
//		sendVolleyStringRequest(
//				(MainConstants.applicationStationeryApp
//						+ MainConstants.kConstantAddStationeryRequestFailureDetail + requestParameter)
//						.replace(" ", "%20"), "RequestFailure");

    }

    private void showErrorDialogBox() {

        if ((reportDialog != null) && (!reportDialog.isShowing())) {
            reportDialog.show();
        }
    }

    private void intializeDialogBoxForHomePage(int code) {
        dialogGetHomeMessage = new Dialog(this);
        dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGetHomeMessage
                .setContentView(R.layout.dialog_box_confirm_message);
        // dialog.(MessageConstant.dialogTitle);
        dialogGetHomeMessage.setCanceledOnTouchOutside(true);
        dialogGetHomeMessage.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogGetHomeMessage.setOnDismissListener(setOnDismissListener);
        textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message);
        textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message_confirm);
        textViewHomeMessageCancel = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message_cancel);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.height = 200;
        textViewDialogHomeMessage.setLayoutParams(params);
        textViewDialogHomeMessage.setTextColor(Color.rgb(102, 102, 102));
        if (code == 1) {
            isSaveData = false;
            textViewDialogHomeMessage.setText(StringConstants.kConstantLostData);
            textViewHomeMessageConfirm
                    .setText(StringConstants.kConstantDialogConfirm);
        } else if (code == 2) {
            isSaveData = true;
            textViewDialogHomeMessage.setText(StringConstants.kConstantSaveData);
            textViewHomeMessageConfirm
                    .setText(StringConstants.kConstantDialogConfirm);
        }
        textViewDialogHomeMessage.setTypeface(fontPoppinsSemiBold);
        textViewHomeMessageCancel.setTypeface(fontPoppinsSemiBold);
        textViewHomeMessageConfirm.setTypeface(fontPoppinsSemiBold);
//        textViewHomeMessageConfirm
//                .setText(MainConstants.kConstantDialogConfirm);
        textViewHomeMessageCancel.setText(StringConstants.kConstantDialogCancel);
        // textViewHomeMessageConfirm.setText(MainConstants.kConstantOka);
        textViewHomeMessageConfirm.setOnClickListener(discardConfirm);
        textViewHomeMessageCancel.setOnClickListener(discardCancel);

    }

    private void showHomeDialogBox() {
        if ((dialogGetHomeMessage != null)
                && (!dialogGetHomeMessage.isShowing())) {
            dialogGetHomeMessage.show();
        }
    }

    // method to Enable , disable the view - means it is not allowed to call the
    // listener
    private void setEnableViews(Boolean isEnable) {
        if (isEnable != null) {
            buttonStationerySubmit.setEnabled(isEnable);
            buttonStationeryCancel.setEnabled(isEnable);
        }
    }

    /**
     * Disable Process
     *
     * @author Karthikeyan D S
     * @created on 04Jul2018
     */
    private void enableProcess() {
        buttonStationeryCancel.setEnabled(true);
        buttonStationerySubmit.setEnabled(true);
        buttonStationeryBack.setEnabled(true);
        isBackPressEnabled = true;
        isItemAddListenerPressEnabled= true;
    }

    /**
     * Disable Process
     *
     * @author Karthikeyan D S
     * @created on 04Jul2018
     */
    private void disableProcess() {
        buttonStationeryCancel.setEnabled(false);
        buttonStationerySubmit.setEnabled(false);
        buttonStationeryBack.setEnabled(false);
        isBackPressEnabled = false;
        isItemAddListenerPressEnabled = false;
    }

    /**
     * Dialog to show Upload Failure Error
     *
     * @author Karthikeyan D S
     * @created on 04Jul2018
     */
    private void uploadStationeryItemEntryFailure(int code) {
        chooseOption = 0;
        enableProcess();
        noActionHandler.postDelayed(noActionThread,
                MainConstants.afterTypingNoFaceTime);

        DialogView.hideDialog();
        DialogView.setNullDialog();
        DialogView.showReportDialog(this,
                MainConstants.uploadStationeryFailure,
                code);

        // }
        // setGridView();
    }

    /**
     * Dialog dismiss Listener for reinitialize list
     *
     * @author karthick
     * @created on 20150618
     */
    DialogInterface.OnDismissListener setOnDismissListener = new DialogInterface.OnDismissListener() {
        public void onDismiss(DialogInterface dialog) {
//            chooseOption = 0;
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTime);
            }
            /*
             * if (employeeID.length() <= NumberConstants.kConstantZero) {
             * checkboxNotifyMe.setChecked(false); } if (isNotifyDialogShow) {
             * isNotifyDialogShow = false; } else {
             * initializeGridAdapter(false); }
             */
        }
    };

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            // Log.d("list", "noActionThread");
            getHomePage();

        }
    };

    /**
     * listener Class for Submit details
     *
     * @author Karthikeyan D S
     * @created on 04JUL2018
     */
    public View.OnClickListener listenerSubmit = new View.OnClickListener() {
        @Override
        public void onClick(View V) {
            if (isValidate()) {
                getSelectedItemList();
            } else {
                UsedCustomToast.makeCustomToast(ActivityStationeryDragDropQuantity.this, "Minimum Quantity is 1", Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        }
    };

    /**
     * listener Class for Submit details
     *
     * @author Karthikeyan D S
     * @created on 04JUL2018
     */
    public View.OnClickListener listenerCancel = new View.OnClickListener() {
        @Override
        public void onClick(View V) {
            if (!isValidate()) {
                getHomePage();
            } else {
                showDetailDialog(1);
                showHomeDialogBox();
            }
        }
    };

    /**
     * listener Class for Submit details
     *
     * @author Karthikeyan D S
     * @created on 04JUL2018
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        @Override
        public void onClick(View V) {
            if (!isValidate()) {
                callPreviousPageActivity();
            } else {
//                showDetailDialog(2);
//                showHomeDialogBox();
                if (saveDatatoSingleton()) {
                    if ((dialogGetHomeMessage != null)) {
                        dialogGetHomeMessage.dismiss();
                    }
                    callPreviousPageActivity();
                }
                else
                {

                }
            }
        }
    };
    /**
     * Dialog Confirm Listener for confirm button where new item Selected
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private View.OnClickListener discardConfirm = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            if (!isSaveData) {
                if ((dialogGetHomeMessage != null)) {
                    dialogGetHomeMessage.dismiss();
                }
                getHomePage();
            } else if (isSaveData) {
                if (saveDatatoSingleton()) {
                    if ((dialogGetHomeMessage != null)) {
                        dialogGetHomeMessage.dismiss();
                    }
                    callPreviousPageActivity();
                }
            }
        }
    };
    /**
     * Click the errorMessageCancel to
     * dismiss the dialogBox
     */
    private View.OnClickListener discardCancel = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if ((dialogGetHomeMessage != null)
                    && (dialogGetHomeMessage.isShowing())) {
                dialogGetHomeMessage.dismiss();
            }
        }
    };

    public boolean saveDatatoSingleton() {
        boolean isdataset = false;
        Map<Integer, Integer> selectedItemDetailsQuantity = new HashMap<Integer, Integer>();
        if (itemQuantityList != null && itemQuantityList.size() >= 0) {
            for (int itemCount = 0; itemCount < itemQuantityList.size(); itemCount++) {
                int itemQuantity = itemQuantityList.get(itemCount).getQuantity();
                int StationeryCode = itemQuantityList.get(itemCount).getStationeryCode();
                selectedItemDetailsQuantity.put(StationeryCode, itemQuantity);
            }
            if (SingletonStationeryDetails.getStationeryInstance() != null && SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryQuantityDetails().size() >= 0) {
//                Log.d("DSK", "Set singletonv" + selectedItemDetailsQuantity.size());
                SingletonStationeryDetails.getStationeryInstance().setSelectedStationeryQuantityDetails(selectedItemDetailsQuantity);
            }
            isdataset = true;
            return true;
        }
        return isdataset;
    }


    public boolean isValidate() {
        boolean isDataPresent = true;
//        ActivityStationeryDragDropQuantity.ListviewAdapter.ViewHolder listViewHolder ;
        for (int itemCount = 0; itemCount < itemQuantityList.size(); itemCount++) {
            int itemQuantity = itemQuantityList.get(itemCount).getQuantity();
            int StationeryCode = itemQuantityList.get(itemCount).getStationeryCode();
            if (itemQuantity > 0) {
                addSelectedItemInHashMap(itemCount, StationeryCode, itemQuantity);
            } else {
                if (selectedStationeryListFinal != null && selectedStationeryListFinal.size() > 0) {
                    selectedStationeryListFinal.clear();
                }
                isDataPresent = false;
            }
        }
        return isDataPresent;
    }

    /**
     * add selected item in hashmap
     */
    private void addSelectedItemInHashMap(int position, int stationeryCode, int quantity) {
        if (quantity > 0) {
            // Log.d("stationery", " selectedStationeryListFinal " + stationeryCode);
            StationeryEntry entry = new StationeryEntry();
            entry.setQuantity(quantity);
            entry.setStationeryItemCode(stationeryCode);
            entry.setEntryTime(// TimeZoneConventer.getDate(
                    (System.currentTimeMillis() / 1000) * 1000);
            if (selectedStationeryListFinal != null) {
                if (selectedStationeryListFinal.get(position) != entry) {
                    selectedStationeryListFinal.put(position, entry);
                } else {
                    removeSelectedItemInHashMap(entry);
                    selectedStationeryListFinal.put(position, entry);
                }
            }
        }

    }

    /**
     * remove selected item in hashmap
     */
    private void removeSelectedItemInHashMap(StationeryEntry entry) {
        if (selectedStationeryListFinal.containsValue(entry)) {
            selectedStationeryListFinal.remove(entry);
        }
    }

    private void initializeErrorDialog() {
        reportDialog = new Dialog(context);
        reportDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        reportDialog.setContentView(R.layout.dialog_report);
        // dialog.(MessageConstant.dialogTitle);
        textViewReportMsg = (TextView) reportDialog
                .findViewById(R.id.textView_report_msg);
        textViewReportFirstMsg = (TextView) reportDialog
                .findViewById(R.id.textView_report_first_msg);
        textViewReportErrorCode = (TextView) reportDialog
                .findViewById(R.id.textView_report_error_code);
        progressBar = (ProgressBar) reportDialog
                .findViewById(R.id.progressBar_report);
        reportImage = (ImageView) reportDialog
                .findViewById(R.id.imageView_report);
        tellUs = (Button) reportDialog.findViewById(R.id.button_tellus);
        seperator = (View) reportDialog.findViewById(R.id.firstSeparator);
        relativeLayout = (RelativeLayout) reportDialog
                .findViewById(R.id.relativeLayout_dialog);
        relativeLayout.getLayoutParams().width = (int) (400 * context
                .getApplicationContext().getResources().getDisplayMetrics().density);
        reportImage.setVisibility(View.GONE);
        textViewReportFirstMsg.setVisibility(View.VISIBLE);
        // reportImage.setBackgroundDrawable(new
        // ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressBar.setVisibility(View.GONE);
        reportDialog.setCanceledOnTouchOutside(true);
        reportDialog.setOnKeyListener(null);
        textViewReportErrorCode.setVisibility(View.GONE);
        seperator.setVisibility(View.VISIBLE);
        tellUs.setVisibility(View.VISIBLE);
        // tellUs.setText(MainConstants.tellUs);
        textViewReportFirstMsg.setVisibility(View.INVISIBLE);// .setText(MainConstants.kConstantAtleastOneItemSelected);
        textViewReportMsg
                .setText(StringConstants.kConstantAtleastOneItemSelected);
        textViewReportErrorCode.setVisibility(View.INVISIBLE);// .setText(MainConstants.visitorUploadErrorCodeText+reportStatus);
        textViewReportMsg.setTextColor(Color.rgb(102, 102, 102));
        relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));
        reportDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tellUs.setTextColor(Color.rgb(102, 102, 102));
        tellUs.setTextSize(20);
        seperator.setBackgroundColor(Color.rgb(171, 171, 171));
        textViewReportMsg.setTextSize(23);
        textViewReportFirstMsg.setMinHeight(80);
        textViewReportFirstMsg.setGravity(Gravity.TOP);
        tellUs.setTypeface(fontPoppinsSemiBold);
        textViewReportMsg.setTypeface(fontPoppinsSemiBold);
        textViewReportFirstMsg.setTypeface(fontPoppinsSemiBold);
        textViewReportErrorCode.setTypeface(fontPoppinsSemiBold);
        tellUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDialog.dismiss();
            }

        });
    }

    /**
     * set internal log and insert to local sqlite
     *
     * @author Karthikeyan D S
     * @created on 04JUL2018
     */
    private void setInternalLogs(int errorCode,
                                 String errorMessage) {

        NetworkConnection networkConnection = new NetworkConnection();
        InternalAppLogs internalAppLogs = new InternalAppLogs();
        internalAppLogs.setErrorCode(errorCode);
        internalAppLogs.setErrorMessage(errorMessage);
        internalAppLogs.setLogDate(new Date().getTime());
        internalAppLogs.setWifiSignalStrength(networkConnection
                .getCurrentWifiSignalStrength(this));
        DbHelper.getInstance(this).insertLogs(internalAppLogs);
    }

    /**
     * background process for upload visitor details
     */
    public class BackgroundProcess extends AsyncTask<String, String, String> {

        String response = MainConstants.kConstantEmpty;
        String requestParameter = MainConstants.kConstantEmpty;
        int responseCode = MainConstants.kConstantMinusOne;

        public BackgroundProcess(String requestParameter) {
            this.requestParameter = requestParameter;
        }

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... f_url) {

            JSONParser jsonParser = new JSONParser();

            URL url;
            try {
                url = new URL(MainConstants.applicationStationeryApp
                        + MainConstants.kConstantAddStationeryRequestRecord);

                JSONObject jsonObject = jsonParser.getJsonArrayInPostMethod(
                        url, requestParameter.replace(" ", "%20"));

                if ((jsonObject != null)) {
                    response = jsonObject.toString();
                    long id = creator.getStationeryRequestId(jsonObject);

                    if ((id > 0L)) {
                        responseCode = ErrorConstants.stationeryRequestSuccess;
                    } else {
                        sendFailureRequest(jsonObject.toString(),
                                ErrorConstants.stationeryRequestJsonDataError);
                        responseCode = ErrorConstants.stationeryRequestJsonDataError;
                    }

                }

            } catch (MalformedURLException e) {
                responseCode = ErrorConstants.stationeryRequestURLError;
            } catch (IOException e) {
                responseCode = ErrorConstants.stationeryRequestIOError;
            } catch (JSONException e) {
                responseCode = ErrorConstants.stationeryRequestJSONError;
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String responseParam) {

            if (responseCode == ErrorConstants.stationeryRequestSuccess) {
                utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
                        StringConstants.kConstantMsgInStationeryItemUploadSuccess,
                        Toast.LENGTH_LONG, Gravity.TOP).show();
                callHomePageActivity();
//                noActionHandler.removeCallbacks(noActionThread);
                finish();
            } else {
                enableProcess();
                setEnableViews(true);
                IsallowedToClickStationaryItems = true;
                uploadStationeryItemEntryFailure(responseCode);
                setInternalLogs(responseCode, response);
            }
        }
    }

    /**
     * List View Adapter Class
     *
     * @author Karthikeyan D S
     * @created on 04JUL2018
     */
    public class ListviewAdapter extends ArrayAdapter<Stationery> {
        Context context;
        int mResourceID;
        Typeface mTypeFaceHeadingSemiBold;
        ArrayList<Stationery> stationeryArrayList = new ArrayList<Stationery>();

        public ListviewAdapter(Context context, int mResourceID, ArrayList<Stationery> stationeryArrayList) {
            super(context, mResourceID, stationeryArrayList);
            this.context = context;
            this.mResourceID = mResourceID;
            this.stationeryArrayList = stationeryArrayList;
        }

        @Override
        public View getView(int position, View convertView, final ViewGroup parent) {
            View characterView = convertView;
            final ActivityStationeryDragDropQuantity.ListviewAdapter.ViewHolder listViewHolder;

            if (characterView == null) {
                final LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                characterView = layoutInflater.inflate(mResourceID, parent, false);

                mTypeFaceHeadingSemiBold = Typeface.createFromAsset(getAssets(), FontConstants.fontConstantPoppinsSemiBold);

                listViewHolder = new ActivityStationeryDragDropQuantity.ListviewAdapter.ViewHolder();

                listViewHolder.mTextViewSelectedItemName = (TextView) characterView.findViewById(R.id.text_view_item_name);
                listViewHolder.textViewrecordID = (TextView) characterView.findViewById(R.id.record_id);

                listViewHolder.editTextSelectedItemQuantity = (EditText) characterView.findViewById(R.id.edit_text_item_quantity);
                listViewHolder.imageViewaddQuantity = (ImageView) characterView.findViewById(R.id.image_view_add_icon);
                listViewHolder.imageViewsubtractQuantity = (ImageView) characterView.findViewById(R.id.image_view_subtract_icon);

                // Set Font Style.
                listViewHolder.mTextViewSelectedItemName.setTypeface(mTypeFaceHeadingSemiBold);
                listViewHolder.editTextSelectedItemQuantity.setTypeface(mTypeFaceHeadingSemiBold);

                // Listener Class For Alphabet Characters.
                listViewHolder.mTextViewSelectedItemName.setTag(listViewHolder);

//                listViewHolder.imageViewaddQuantity.setOnClickListener(listenerAddQuantity);
//                listViewHolder.imageViewsubtractQuantity.setOnClickListener(listenerSubtractQuantity);

                characterView.setTag(listViewHolder);

                listViewHolder.imageViewaddQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int itemQuantityFieldValue = 0;
//                        Log.d("DskQuantity1",""+listViewHolder.editTextSelectedItemQuantity.getText().toString());
                        String editTextQuantityFieldValue = listViewHolder.editTextSelectedItemQuantity.getText().toString().trim();
                        if(isItemAddListenerPressEnabled) {
                            if (editTextQuantityFieldValue != null &&
                                    !editTextQuantityFieldValue.isEmpty()) {
                                try {
                                    itemQuantityFieldValue = Integer.parseInt(listViewHolder.editTextSelectedItemQuantity.getText().toString());
//                                itemQuantityFieldValue = itemQuantityFieldValue + 1;
                                } catch (NumberFormatException e1) {
                                    itemQuantityFieldValue = 1;
                                }
                                if (itemQuantityFieldValue >= 1 && itemQuantityFieldValue < 999) {
                                    itemQuantityFieldValue = itemQuantityFieldValue + 1;
                                    listViewHolder.editTextSelectedItemQuantity.setError(null);
                                    int position = listViewHolder.editTextSelectedItemQuantity.length();
                                    Editable editTextQuantity = listViewHolder.editTextSelectedItemQuantity.getText();
                                    Selection.setSelection(editTextQuantity, position);
                                    listViewHolder.editTextSelectedItemQuantity.setText(MainConstants.kConstantEmpty + itemQuantityFieldValue);
                                } else {
                                    itemQuantityFieldValue = 1;
                                    listViewHolder.editTextSelectedItemQuantity.setError(null);
                                    int position = listViewHolder.editTextSelectedItemQuantity.length();
                                    Editable editTextQuantity = listViewHolder.editTextSelectedItemQuantity.getText();
                                    Selection.setSelection(editTextQuantity, position);
                                    listViewHolder.editTextSelectedItemQuantity.setText(MainConstants.kConstantEmpty + itemQuantityFieldValue);
                                }
                            } else {
                                itemQuantityFieldValue = 1;
                                listViewHolder.editTextSelectedItemQuantity.setText(MainConstants.kConstantEmpty + itemQuantityFieldValue);
                            }
//                        Log.d("DskQuantity2",""+listViewHolder.editTextSelectedItemQuantity.getText().toString());
                        }
                    }
                });


                listViewHolder.imageViewsubtractQuantity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String editTextQuantityValue = listViewHolder.editTextSelectedItemQuantity.getText().toString().trim();
                        int itemQuantityFieldValue;
                        if(isItemAddListenerPressEnabled) {
                            if (editTextQuantityValue != null && !editTextQuantityValue.isEmpty()) {
                                try {
                                    itemQuantityFieldValue = Integer.parseInt(listViewHolder.editTextSelectedItemQuantity.getText().toString());
//                                itemQuantityFieldValue = itemQuantityFieldValue + 1;
                                } catch (NumberFormatException e1) {
                                    itemQuantityFieldValue = 1;
                                }

                                if (itemQuantityFieldValue > 1 && itemQuantityFieldValue < 999) {
                                    itemQuantityFieldValue = itemQuantityFieldValue - 1;
                                    listViewHolder.editTextSelectedItemQuantity.setError(null);
                                    int position = listViewHolder.editTextSelectedItemQuantity.length();
                                    Editable editTextQuantity = listViewHolder.editTextSelectedItemQuantity.getText();
                                    Selection.setSelection(editTextQuantity, position);
                                    listViewHolder.editTextSelectedItemQuantity.setText(MainConstants.kConstantEmpty + itemQuantityFieldValue);
                                } else {
                                    itemQuantityFieldValue = 1;
                                    listViewHolder.editTextSelectedItemQuantity.setError(null);
                                    int position = listViewHolder.editTextSelectedItemQuantity.length();
                                    Editable editTextQuantity = listViewHolder.editTextSelectedItemQuantity.getText();
                                    Selection.setSelection(editTextQuantity, position);
                                    listViewHolder.editTextSelectedItemQuantity.setText(MainConstants.kConstantEmpty + itemQuantityFieldValue);
                                }
                            } else {
                                itemQuantityFieldValue = 1;
                                listViewHolder.editTextSelectedItemQuantity.setText(MainConstants.kConstantEmpty + itemQuantityFieldValue);
                            }
                        }
//                        notifyDataSetChanged();
                    }
                });

                if(isItemAddListenerPressEnabled) {
                    listViewHolder.editTextSelectedItemQuantity.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                      int arg3) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable arg0) {
//                         TODO Auto-generated method stub
                            int itemQuantity = 1;
                            String itemQuantities = listViewHolder.editTextSelectedItemQuantity.getText().toString().trim();
                            if (itemQuantities != null && !itemQuantities.trim().isEmpty()) {
                                try {
                                    itemQuantity = Integer.parseInt(itemQuantities);
                                } catch (NumberFormatException e) {
                                    itemQuantity = 1;
                                }
//                            Log.d("DSK6","if"+itemQuantityList.get(listViewHolder.position).getRecordId()+" "+listViewHolder.recordID);
                                itemQuantityList.get(listViewHolder.position).setQuantity(itemQuantity);
                                listViewHolder.editTextSelectedItemQuantity.setFocusable(true);
                            } else {
                                listViewHolder.editTextSelectedItemQuantity.setFocusable(true);
                                listViewHolder.editTextSelectedItemQuantity.setError("Enter Quantity");
                            }
                            itemQuantityList.get(listViewHolder.position).setQuantity(itemQuantity);
                        }
                    });
                }
                else
                {
                    listViewHolder.editTextSelectedItemQuantity.setEnabled(false);
                }

                //Disable EditText on Submit of the Data
                if(!isItemAddListenerPressEnabled) {
                    listViewHolder.editTextSelectedItemQuantity.setEnabled(false);
                }
                else if(isItemAddListenerPressEnabled)
                {
                    listViewHolder.editTextSelectedItemQuantity.setEnabled(true);
                }


            } else {
                listViewHolder = (ActivityStationeryDragDropQuantity.ListviewAdapter.ViewHolder) characterView.getTag();
            }

            final Stationery characterList = getItem(position);

            listViewHolder.mTextViewSelectedItemName.setText(characterList.getStationeryName().trim());
            listViewHolder.creatorID = (characterList.getCreatorId() + "");
            listViewHolder.textViewrecordID.setText(characterList.getRecordId() + "");
            listViewHolder.recordID = characterList.getRecordId();
            listViewHolder.position = position;

            if (itemQuantityList.get(position).getRecordId() == listViewHolder.recordID) {
//                Log.d("DSK5","if"+itemQuantityList.get(position).getRecordId()+" "+listViewHolder.recordID);
                int itemQuantity = itemQuantityList.get(position).getQuantity();
                if (itemQuantity >= 0) {
                    //  Log.d("DSK5","ifsub"+itemQuantityList.get(position).getRecordId()+" "+listViewHolder.recordID + " "+itemQuantity);
                    listViewHolder.editTextSelectedItemQuantity.setText(itemQuantity + "");
                } else {
//                    Log.d("DSK5","elsesub"+itemQuantityList.get(position).getRecordId()+" "+listViewHolder.recordID);
                    listViewHolder.editTextSelectedItemQuantity.setText(0 + "");
                }
            } else {
//                Log.d("DSK5","else"+itemQuantityList.get(position).getRecordId()+" "+listViewHolder.recordID);
                listViewHolder.editTextSelectedItemQuantity.setText(0 + "");
            }

            listViewHolder.editTextSelectedItemQuantity.setTag(listViewHolder);

            return characterView;
        }

        class ViewHolder {
            TextView mTextViewSelectedItemName, textViewrecordID;
            EditText editTextSelectedItemQuantity;
            ImageView imageViewaddQuantity, imageViewsubtractQuantity;
            String creatorID;
            long recordID;
            int position;
        }
    }


    /**
     * On BackPress
     *
     * @author by Karthikeyan D S
     * @Created on 04JUL2018
     */
    public void onBackPressed() {
        if (isBackPressEnabled) {
            if (!isValidate()) {
                callPreviousPageActivity();
            } else {
//                showDetailDialog(2);
//                showHomeDialogBox();
                if (saveDatatoSingleton()) {
                    if ((dialogGetHomeMessage != null)) {
                        dialogGetHomeMessage.dismiss();
                    }
                    callPreviousPageActivity();
                }
                else
                {

                }
            }
        }
        else
        {

        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthikeyan D S
     * @created on 20180523
     */
    private void showDetailDialog(int code) {
        if (code == 2) {
            intializeDialogBoxForHomePage(2);
            textSpeach(MainConstants.speachAreYouSureSaveCode);
        } else if (code == 1) {
            intializeDialogBoxForHomePage(1);
            textSpeach(MainConstants.speachAreYouSureCode);
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
        }
    };

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {
        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne) {
                textToSpeech.speak(TextToSpeachConstants.speachDragDropItem,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Call Previous Page Activity
     *
     * @author by Karthikeyan D S
     * @Created on 04JUL2018
     */
    public void callPreviousPageActivity() {
        Intent intentPreviousPageActivity = new Intent(ActivityStationeryDragDropQuantity.this, ActivityStationeryDragDrop.class);
        startActivity(intentPreviousPageActivity);
        finish();
    }

    /**
     * Call Home Page Activity
     *
     * @author by Karthikeyan D S
     * @Created on 04JUL2018
     */
    public void callHomePageActivity() {
        getHomePage();
    }

    /**
     * onResume of Activity
     *
     * @author by Karthikeyan D S
     * @Created on 04JUL2018
     */
    public void onResume() {
        super.onResume();
        setAdapterView();
    }

    /**
     * onStart of Activity
     *
     * @author by Karthikeyan D S
     * @Created on 04JUL2018
     */
    public void onStart() {
        super.onStart();
    }

    /**
     * onDestroy of Activity
     *
     * @author by Karthikeyan D S
     * @Created on 05JUL2018
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateobjects();
    }

    /**
     * Deallocate Objects
     *
     * @author by Karthikeyan D S
     * @Created on 05JUL2018
     */
    private void deallocateobjects() {
        buttonStationerySubmit = null;
        buttonStationeryCancel = null;
        buttonStationeryBack = null;
        tellUs = null;
        relativeLayoutMain = null;
        selectedStationeryItemDetailsListView = null;
        noStationeryItemsLogTextView = null;
        stationeryItemHeaderTextView = null;
        imageViewNextPage = null;
        imageViewPreviousPage = null;
        fontPoppinsBold = null;
        fontPoppinsMedium = null;
        fontPoppinsSemiBold = null;
        if (selectedStationeryListFinal != null) {
            selectedStationeryListFinal.clear();
        }
        textViewDialogHomeMessage = null;
        textViewHomeMessageConfirm = null;
        textViewHomeMessageCancel = null;
        textViewReportMsg = null;
        textViewReportFirstMsg = null;
        textViewReportErrorCode = null;
        creator = null;
        dialogGetHomeMessage = null;
        reportDialog = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (handlerHideDialog != null) {
            handlerHideDialog = null;
        }
        dbHelper = null;
        currentTime = 0L;
        context = null;
        chooseOption = 0;
        IsallowedToClickStationaryItems = true;
        progressBar = null;
        relativeLayout = null;
        seperator = null;
        reportImage = null;
        dragDropViewItem = null;
        mListViewAdapterItemQuantity = null;
        mListViewSelectedItemList = null;
        if (itemQuantityList != null) {
            itemQuantityList.clear();
        }
        isBackPressEnabled = true;
        isSaveData = false;
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        mdevicelocation = MainConstants.kConstantEmpty;
    }
}
