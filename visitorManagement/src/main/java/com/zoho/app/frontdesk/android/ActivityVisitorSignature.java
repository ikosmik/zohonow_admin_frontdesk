package com.zoho.app.frontdesk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import appschema.ExistingVisitorDetails;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.zoho.app.frontdesk.android.R.id;

import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import appschema.VisitorDetailsDatabase;
import constants.NumberConstants;
import constants.StringConstants;
import utility.BackgroundUploadVisitorData;
import utility.DialogView;
import utility.DialogViewWarning;
import utility.NotifyObservable;
import utility.PrintThread;
import appschema.SharedPreferenceController;
import utility.UsedBitmap;
import utility.UsedCustomToast;
import utility.Utility;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;
import database.dbhelper.DbHelper;

public class ActivityVisitorSignature extends Activity implements Observer {

    private DialogViewWarning dialog;
    private Button buttonCheckInNow;
    private RelativeLayout relativeLayoutGetVisitorSign, relativeLayoutExisitingVisitor;
    private ImageView imageviewBack, imageviewHome, imageviewPrevious, image_view_next,
            imageviewNext, imageViewErrorInVisitorSign, imageViewVisitorSignClear, imageViewSignDuplicate;
    private SingleTouchView singleTouchViewGetVisitorSign;
    private TextView textViewVisitorSignature;
    private DbHelper database;
    private MediaPlayer mPlayer;
    private Typeface typeFaceTitle;
    private LinearLayout linearLayoutbadge;
    private ImageView imageViewVisitorImage;
    private TextView textViewGetVisitorName, textViewGetTempID, textViewGetEntryTime, textviewPrinterInfo,textViewCompanyName;//,textViewGetDate,textViewGetDateSubTitle;
    private SharedPreferenceController sharedPreferenceController;
    private int actionCode = MainConstants.backPress, retryCount = 0, applicationMode =  NumberConstants.kConstantZero;
    private Handler noActionHandler, signValidateHandler, handlerHideDialog, handlerAutoCheckIn;
    private TextToSpeech textToSpeech;
    private BackgroundUploadVisitorData backgroundProcess;
    private UsedBitmap usedBitmap;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_visitor_signature);

//        Assign objects.
        assignObjects();

//        Assign Old Signature.
        setOldSignature();
    }

    /**
     * CheckPrinterAvailability
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    private void checkPrinterAvailability() {
        if (SingletonCode.getSingletonWifiPrinterIP() == null
                || SingletonCode.getSingletonWifiPrinterIP().toString()
                .length() <= MainConstants.kConstantZero) {
            SearchThread searchPrinter = new SearchThread();
            searchPrinter.start();
            if(textviewPrinterInfo!=null)
            {
                textviewPrinterInfo.setVisibility(View.VISIBLE);

            }
            UsedCustomToast.makeCustomToast(getApplicationContext(),StringConstants.kConstantPrinterNotConnected,Toast.LENGTH_LONG,Gravity.TOP).show();
        }
        else
        {
            if(textviewPrinterInfo!=null)
            {
                textviewPrinterInfo.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Search Thread to find Available Printers
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    public class SearchThread extends Thread {
        /* search for the printer for 10 times until printer has been found. */
        public void run() {
            for (int i = 0; i < MainConstants.kConstantTen; i++) {
                // search for net printer.
                if (netPrinterList(i)) {
                    break;
                }
            }
        }
    }

    /**
     *  find Available Printers and store it to Singleton
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    private boolean netPrinterList(int times) {
        NetPrinter[] mNetPrinter;
        boolean searchEnd = false;
        try {
            // get net printers of the particular model
            Printer myPrinter = new Printer();
            mNetPrinter = myPrinter.getNetPrinters("");
            final int netPrinterCount = mNetPrinter.length;

            // when find printers,set the printers' information to the list.
            if (netPrinterCount > 0) {
                searchEnd = true;

                // String dispBuff[] = new String[netPrinterCount];
                for (int i = 0; i < netPrinterCount; i++) {
//                    Log.d("DSK Android", "IP:" + mNetPrinter[i].ipAddress);
                    SingletonCode
                            .setSingletonWifiPrinterIP(mNetPrinter[i].ipAddress);
                }

            } else if (netPrinterCount == 0
                    && times == (MainConstants.kConstantTen - 1)) { // when no
                // printer
                // is found
                // String dispBuff[] = new String[1];
//                Printer Not Found
                SingletonCode.setSingletonWifiPrinterIP("");
                searchEnd = true;
            }

        } catch (Exception e) {
        }
        return searchEnd;
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStart() {
        super.onStart();
        NotifyObservable.getInstance().addObserver(this);
    }

    /**
     * This function is called, when the activity is stopped.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStop() {
        super.onStop();
        NotifyObservable.getInstance().deleteObserver(this);
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Assign Objects to the variable
     *
     * @author Karthick
     * @created on 20170508
     */
    private void assignObjects() {
        sharedPreferenceController = new SharedPreferenceController();
        if (sharedPreferenceController != null) {
            applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
        }
        textViewVisitorSignature = (TextView) findViewById(R.id.textView_visitor_signature);
        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        imageViewSignDuplicate = (ImageView) findViewById(R.id.image_view_duplicate);

        relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);
        relativeLayoutGetVisitorSign = (RelativeLayout) findViewById(id.relativeLayoutGetVisitorSign);
        singleTouchViewGetVisitorSign = (SingleTouchView) findViewById(R.id.singletouchview_sign);
        imageViewErrorInVisitorSign = (ImageView) findViewById(id.image_view_error_in_visitor_sign);
        imageViewVisitorSignClear = (ImageView) findViewById(id.imageViewSignClear);
        buttonCheckInNow = (Button) findViewById(id.button_visit_detail_ok);

        linearLayoutbadge = (LinearLayout) findViewById(R.id.linearLayoutbadge);

        textviewPrinterInfo = (TextView) findViewById(R.id.text_view_printer_info);
        imageViewVisitorImage = (ImageView) findViewById(R.id.imageViewVisitorBadgeImage);

        textViewGetVisitorName = (TextView) findViewById(R.id.textViewVisitorName);
        textViewGetTempID = (TextView) findViewById(R.id.text_view_visitor_temp_id);
        textViewGetEntryTime = (TextView) findViewById(R.id.text_view_visitor_register_time);
        textViewCompanyName =(TextView) findViewById(R.id.textViewVisitorCompanyName);

        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

        signValidateHandler = new Handler();
        handlerHideDialog = new Handler();
        handlerAutoCheckIn = new Handler();
        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

//        set AppBackground.
        setAppBackground();

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        // set font style
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        textViewVisitorSignature.setTypeface(typeFaceTitle);
        buttonCheckInNow.setTypeface(typeFaceTitle);

//        Assign Listeners
        assignListeners();

        //textViewGetDate = (TextView) findViewById(R.id.textViewGetVisitingDate);
        //textViewGetDateSubTitle=(TextView) findViewById(R.id.textViewDateSubTitle);

        //set font
        //Typeface typefaceSetDate = Typeface.createFromAsset(newContext.getAssets(),
        //FontConstants.fontConstantLatoReg);
        //textViewGetDate.setTypeface(typefaceSetDate);
        //textViewGetDateSubTitle.setTypeface(typefaceSetDate);

//        if App Mode is DataCentre then Check For Printer Connectivity.
//        if(applicationMode==2) {
////       checkPrinterAvailability
//            checkPrinterAvailability();
////       set Printer Mode.
//            setPrinterMode();
//        }
    }

    /**
     * Assign Listeners
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    private void assignListeners() {
        // Onclick listeners
        if (image_view_next != null && listenerConfirm != null) {
            image_view_next.setOnClickListener(listenerConfirm);
        }
        if (imageviewPrevious != null && listenerPrevious != null) {
            imageviewPrevious.setOnClickListener(listenerPrevious);
        }
        if (imageviewNext != null && listenerConfirm != null) {
            imageviewNext.setOnClickListener(listenerConfirm);
        }
        if (imageviewHome != null && listenerHomePage != null) {
            imageviewHome.setOnClickListener(listenerHomePage);
        }
        if (imageviewBack != null && listenerBack != null) {
            imageviewBack.setOnClickListener(listenerBack);
        }
        singleTouchViewGetVisitorSign.setOnTouchListener(visitorSignListner);
        if (imageViewVisitorSignClear != null && listenerVisitorSignClear != null) {
            imageViewVisitorSignClear.setOnClickListener(listenerVisitorSignClear);
        }
        if (buttonCheckInNow != null && listenerConfirm != null) {
            buttonCheckInNow.setOnClickListener(listenerConfirm);
        }
    }

    /**
     * Set Font Style for Printer View
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    private void setPrinterMode() {
        Typeface typefaceSetName = Typeface.createFromAsset(getApplicationContext().getAssets(),
                FontConstants.fontConstantSourceSansProbold);

        textViewGetVisitorName.setTypeface(typefaceSetName);
        textViewGetTempID.setTypeface(typefaceSetName);
        textViewGetEntryTime.setTypeface(typefaceSetName);
        textViewCompanyName.setTypeface(typefaceSetName);

        assignPrinterDetailstotheView();
    }

    /**
     * Assign Details to the Printer View.
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    private void assignPrinterDetailstotheView() {
        if ((SingletonVisitorProfile.getInstance().getVisitorName() != null) && (SingletonVisitorProfile.getInstance().getVisitorName() != MainConstants.kConstantEmpty)) {

            textViewGetVisitorName.setText(SingletonVisitorProfile.getInstance().getVisitorName());
        }
        else {
            textViewGetVisitorName.setVisibility(View.GONE);
        }

        if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getTempIDNumber() > 0) {
            textViewGetTempID.setText("" + SingletonVisitorProfile.getInstance().getTempIDNumber());
        }
        else {
            textViewGetTempID.setVisibility(View.GONE);
        }
        String currentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm a").format(new Date());

        if (currentTime != null && currentTime != "") {
            textViewGetEntryTime.setText("" + currentTime);
            textViewGetEntryTime.setTextSize(20);
        }
        else {
            textViewGetEntryTime.setVisibility(View.GONE);
        }

        if ((SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().getVisitorImage() != null)
                && (SingletonVisitorProfile.getInstance().getVisitorImage().length > 0)) {

//           Bitmap bitmap;
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            bitmap = BitmapFactory.decodeFile(SingletonVisitorProfile.getInstance().getVisitorImage(), options);
////            if (bitmap != null) {
//
//                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
//                        300,
//                        300, true);
//                // //Log.d("bitap","bitmap" +bitmap);
//                // set circle bitmap
//                Bitmap resizedBitmap = usedBitmap
//                        .getRoundedRectBitmap(resized);
//                // //Log.d("bitap","bitmap" +resizedBitmap);
//                imageViewVisitorImage.setImageBitmap(resizedBitmap);
//                 resizedBitmap.recycle();
//                 resized.recycle();
//            }
//            imageViewVisitorImage.setImageURI(Uri.parse(SingletonVisitorProfile.getInstance().getVisitorImage()));
            setVisitorImage(SingletonVisitorProfile.getInstance().getVisitorImage());
        }
        else {
            imageViewVisitorImage.setVisibility(View.GONE);
        }

        if(SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().getCompany()!=null && SingletonVisitorProfile.getInstance().getCompany()!="")
        {
            textViewCompanyName.setText(SingletonVisitorProfile.getInstance().getCompany());
        }
        else {
            textViewCompanyName.setVisibility(View.GONE);
        }
    }

    /**
     * Assign Details to the Printer View.
     *
     * @author Karthikeyan D S
     * @created on 11Aug2018
     */
    private void setVisitorImage(byte[] byteData) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteData, 0, byteData.length);
        usedBitmap = new UsedBitmap();
        if (bitmap != null) {
            if(usedBitmap!=null) {
                Bitmap bitmapDetails = usedBitmap.storeBitmap(bitmap, 400);
                if (bitmapDetails != null) {
                    imageViewVisitorImage.setImageBitmap(bitmapDetails);
                }
            }
        }
    }

    /**
     * Set background color
     *
     * @author Karthick
     * @created on 20170508
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
                imageViewVisitorSignClear.setImageResource(R.drawable.icon_clear_white_signature);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP, new int[]{
                        0xFF4bbcef, 0xFF46c0e5, 0xFF3ec5d4, 0xFF35ccc1});
                relativeLayoutExisitingVisitor.setBackground(gd);
                imageViewVisitorSignClear.setImageResource(R.drawable.icon_clear_black_signature);
            }
        }
    }

    /**
     * Clear the VisitorSign path.
     *
     * @author Karthick
     * @created 20170508
     */
    private void removeView(RelativeLayout layout, SingleTouchView view) {
        view.signAgain();
    }

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchHomePage() {

        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        } else {
            launchPreviousPage();
        }
        finishAffinity();
    }

    /**
     * Launch previous activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchPreviousPage() {
        if (singleTouchViewGetVisitorSign != null && !singleTouchViewGetVisitorSign.isPathNotEmpty()) {
            setSignature();
        }

        Intent intent = new Intent(this,
                ActivityVisitorTempID.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchNextPage() {
        Intent intent = new Intent(this,
                ActivityHaveAnyDevices.class);
        startActivity(intent);
        finish();
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * Set old sign value t imageview and singleton
     *
     * @author Karthick
     * @created on 20170523
     */
    private void setOldSignature() {
        Bitmap visitorSign = null;
        UsedBitmap usedBitmap = new UsedBitmap();
        if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getVisitorSign().length > 0) {
            visitorSign = usedBitmap.getImage(SingletonVisitorProfile.getInstance().getVisitorSign());
        }
        if (imageViewSignDuplicate != null && visitorSign != null) {
            imageViewSignDuplicate.setVisibility(View.VISIBLE);
            imageViewSignDuplicate.setImageBitmap(visitorSign);
            if (textViewVisitorSignature != null) {
                textViewVisitorSignature.setVisibility(View.INVISIBLE);
            }
            if (buttonCheckInNow != null) {
                buttonCheckInNow.setVisibility(View.VISIBLE);
            }
            if (handlerAutoCheckIn != null) {
                handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
                handlerAutoCheckIn.postDelayed(autoCheckInThread,
                        MainConstants.autoCheckInTime);
            }
        }
    }

    /**
     * Set sign value t imageview and singleton
     *
     * @author Karthick
     * @created on 20170523
     */
    private void setSignature() {
        UsedBitmap usedBitmap = new UsedBitmap();
        Bitmap visitorSign = null;
        if ((SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getVisitorSign() != null
                && SingletonVisitorProfile.getInstance().getVisitorSign().length > 0)) {

        } else {
            visitorSign = usedBitmap.createBitmap(singleTouchViewGetVisitorSign,
                    false);
            if (imageViewSignDuplicate != null && visitorSign != null) {
                imageViewSignDuplicate.setVisibility(View.VISIBLE);
                imageViewSignDuplicate.setImageBitmap(visitorSign);
            }
            if ((visitorSign != null && usedBitmap != null)) {
                byte[] sign = usedBitmap.getBytesPNG(visitorSign);
                if (sign != null) {
                    SingletonVisitorProfile.getInstance().setVisitorSign(sign);
                }
            }
            if (usedBitmap != null) {
                singleTouchViewGetVisitorSign.setVisibility(View.INVISIBLE);
                Bitmap visitorSignWithBG = usedBitmap.createBitmap(singleTouchViewGetVisitorSign,
                        true);
                if (visitorSignWithBG != null) {
                    byte[] sign = usedBitmap.getBytesPNG(visitorSignWithBG);
                    if (sign != null) {
                        SingletonVisitorProfile.getInstance().setVisitorSignWithBG(sign);
                    }
                    visitorSignWithBG.recycle();
                    visitorSignWithBG = null;
                }
            }
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showDetailDialog() {
        showWarningDialog();
        textSpeach(MainConstants.speachAreYouSureCode);
    }

    /**
     * Validate signature
     *
     * @author Karthick
     * @created on 20170508
     */
    private boolean validateSingature() {
        if ((singleTouchViewGetVisitorSign != null
                && !singleTouchViewGetVisitorSign.isPathNotEmpty())
                || (SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getVisitorSign() != null
                && SingletonVisitorProfile.getInstance().getVisitorSign().length > 0)) {
            return true;
        } else {
            imageViewErrorInVisitorSign.setVisibility(View.VISIBLE);
            return false;
        }
    }

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showWarningDialog() {
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    /**
     * Show dialog box loading and start background task to upload visitor data
     *
     * @author Karthick
     * @created on 20170508
     */
    private void nextProcessing() {

        DialogView.showReportDialog(this, MainConstants.badgeDialog,
                MainConstants.forgotIDUploadCode);

        if (backgroundProcess != null && (SingletonVisitorProfile.getInstance() == null
                || SingletonVisitorProfile.getInstance().getVisitId() == null
                || SingletonVisitorProfile.getInstance().getVisitId()
                .isEmpty())) {
            backgroundProcess.cancel(true);
        }

        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
        }
        if (signValidateHandler != null) {
            signValidateHandler.removeCallbacks(validateSignThread);
        }
        if (handlerAutoCheckIn != null) {
            handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
        }
//        Log.d("DSK "," VisitId1 "+ SingletonVisitorProfile.getInstance().getVisitId());
//        Log.d("DSK "," applicationMode1 "+applicationMode);
        backgroundProcess = new BackgroundUploadVisitorData(this,
                MainConstants.jsonUploadJsonData);

        if (backgroundProcess != null) {
            backgroundProcess.execute();
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {

        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne && SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getVisitorSign().length == 0) {
                textToSpeech.speak(TextToSpeachConstants.speachSignature,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachWelcomeMsgCode) {
                textToSpeech.speak(TextToSpeachConstants.speachWelcome,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void handleResponse() {
//        boolean isSuccess = false;
        if (SingletonVisitorProfile.getInstance() != null &&
                SingletonVisitorProfile.getInstance().getUploadedCode() == MainConstants.kConstantSuccessCode) {
            DialogView.showReportDialog(this,
                    MainConstants.badgeDialog,
                    ErrorConstants.visitorAllSuccessCode);
            textSpeach(MainConstants.speachWelcomeMsgCode);
            handlerHideDialog.postDelayed(hideDialogTask, 3000);
            insertVisitorDetails();
//            isSuccess = true;
        } else {
            SingletonVisitorProfile.getInstance().setRetryCount(SingletonVisitorProfile.getInstance().getRetryCount());
            retryCount = retryCount + 1;
            errorMusicPlay(R.raw.bonk);
            SingletonCode
                    .setSingletonVisitorUploadErrorCode(SingletonVisitorProfile.getInstance().getUploadedCode());
            DialogView.showReportDialog(
                    ApplicationController.getInstance(),
                    MainConstants.badgeDialog, SingletonVisitorProfile.getInstance().getUploadedCode(), listenerRetry);

            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);
            }
//            isSuccess = false;
        }

//        if (isSuccess) {
//            if (applicationMode == 2) {
////                printBadge();
//            }
//        }
    }

    /**
     * Insert visitor details
     *
     * @author Karthick
     * @created on 20170522
     */
    private void insertVisitorDetails() {
        if (database == null) {
            database = DbHelper.getInstance(this);
        }
        long visitDate = new Date().getTime();
        Utility utility = new Utility();
        VisitorDetailsDatabase visitorDetailsDb = new VisitorDetailsDatabase();
        visitorDetailsDb.setJsonData(utility.convertVisitorDetailsJsonData());
        visitorDetailsDb.setPhotoUrl(SingletonVisitorProfile.getInstance().getVisitorImage());
        visitorDetailsDb.setSignatureUrl(SingletonVisitorProfile.getInstance().getVisitorSign());
        visitorDetailsDb.setPhotoUploaded(MainConstants.kConstantPhotoNotUploaded);
        visitorDetailsDb.setSignatureUploaded(MainConstants.kConstantSignatureNotUploaded);
        visitorDetailsDb.setVisitDate(visitDate);
        if (SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                .getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {
            visitorDetailsDb.setVisitPurpose(MainConstants.interViewPurpose);
        } else {
            visitorDetailsDb.setVisitPurpose(MainConstants.visitorPurpose);
        }
        visitorDetailsDb.setJsonDataId(SingletonVisitorProfile.getInstance().getVisitId());
        try {
            database.insertVisitorRecord(visitorDetailsDb);
        }
        catch (SQLiteDatabaseLockedException e1)
        {

        }

        ExistingVisitorDetails existVisitorDetails = new ExistingVisitorDetails();
        if (existVisitorDetails != null) {
            existVisitorDetails.setVisitedDate(visitDate);
            if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getVisitorName() != null) {
                existVisitorDetails.setName(SingletonVisitorProfile.getInstance().getVisitorName());
            }
            if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getContact() != null) {
                existVisitorDetails.setContact(SingletonVisitorProfile.getInstance().getContact());
            }
            if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null) {
                existVisitorDetails.setPurpose(SingletonVisitorProfile.getInstance().getPurposeOfVisitCode());
            }
            if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getVisitId() != null) {
                existVisitorDetails.setCreatorVisitID(SingletonVisitorProfile.getInstance().getVisitId());
            }
            if (SingletonEmployeeProfile.getInstance() != null && SingletonEmployeeProfile.getInstance().getEmployeeMailId() != null) {
                existVisitorDetails.setEmployeeEmail(SingletonEmployeeProfile.getInstance().getEmployeeMailId());
            }
            try {
                database.insertExistVsitor(existVisitorDetails);
            }
            catch (SQLiteDatabaseLockedException e1)
            {

            }
        }
    }

    /**
     * Play error music
     *
     * @author Karthick
     * @created on 20170522
     */
    private void errorMusicPlay(int rawId) {
        if (mPlayer != null) {
            mPlayer.release();
        }
        mPlayer = MediaPlayer.create(this, rawId);
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
            }
        });
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void deallocateObjects() {
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        textViewVisitorSignature = null;
        textViewGetVisitorName = null;
        textviewPrinterInfo = null;
        textViewGetEntryTime = null;
        textViewGetTempID = null;
        imageViewVisitorImage = null;
        linearLayoutbadge = null;
        typeFaceTitle = null;
        relativeLayoutExisitingVisitor
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutExisitingVisitor = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (signValidateHandler != null) {
            signValidateHandler.removeCallbacks(validateSignThread);
            signValidateHandler = null;
        }
        if (handlerHideDialog != null) {
            handlerHideDialog.removeCallbacks(hideDialogTask);
            handlerHideDialog = null;
        }
        if (handlerAutoCheckIn != null) {
            handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
        }
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech = null;
        }
        applicationMode= NumberConstants.kConstantZero;
            DialogView.setNullDialog();
    }

    @Override
    public void update(Observable observable, Object data) {
        handleResponse();
    }

    /**
     * Hide keyboard
     *
     * @author Karthick
     * @created on 20161222
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    /**
     * Print Badge
     *
     * @author Karthick
     * @created on 20161222
     */
    private void printBadge() {
        if ((SingletonCode.getSingletonWifiPrinterIP() == null)
                || (SingletonCode.getSingletonWifiPrinterIP().isEmpty())) {
            UsedCustomToast.makeCustomToast(
                    this, StringConstants.kConstantNoPrinter
                            + MainConstants.kConstantNewLine
                            + StringConstants.errorCodeText
                            + ErrorConstants.badgeNoPrinter, Toast.LENGTH_SHORT,
                    Gravity.TOP).show();

            if (textviewPrinterInfo != null) {
                textviewPrinterInfo.setText(StringConstants.kConstantNoPrinter);
                textviewPrinterInfo.setVisibility(View.VISIBLE);
            }
        } else {
            // try {
            // Bitmap Bitmap = TextToImageEncode("3063125000005789015");
            // if(Bitmap!=null) {
            // imageViewPropsRight.setImageBitmap(Bitmap);
            // }
            // } catch (WriterException e) {
            //
            // }
            // if(handlerQRCodePrint!=null) {
            // handlerQRCodePrint.removeCallbacks(printRunnable);
            // handlerQRCodePrint.post(printRunnable);
            // }
            PrintThread PrintThread = new PrintThread(linearLayoutbadge,
                    SingletonCode.getSingletonWifiPrinterIP());
            PrintThread.start();
            UsedCustomToast.makeCustomToast(getApplicationContext(),StringConstants.kConstantPrinterPrintingIDCard,
                    Toast.LENGTH_SHORT,Gravity.TOP).show();
        }

        // PrintBadge printBadge = new PrintBadge();
        // printBadge.storeBitmap(linearLayoutBadge);
        //
        // if ((SingletonCode.getSingletonWifiPrinterIP() == null)
        // || (SingletonCode.getSingletonWifiPrinterIP().isEmpty())) {
        // UsedCustomToast.makeCustomToast(this,StringConstants.kConstantErrorMsgBadgePrint,Toast.LENGTH_LONG,Gravity.TOP).show();
        //
        // } else if (!printBadge.printBadge()) {
        //
        // UsedCustomToast.makeCustomToast(this,StringConstants.kConstantErrorMsgBadgePrint,Toast.LENGTH_LONG,Gravity.TOP).show();
        //
        // } else {
        // UsedCustomToast.makeCustomToast(this,StringConstants.kConstantErrorMsgBadgePrint,Toast.LENGTH_LONG,Gravity.TOP).show();
        //
        // }
    }
    /**
     * Auto check in started
     *
     * @author Karthick
     * @created 20160123
     */
    public Runnable autoCheckInThread = new Runnable() {
        public void run() {

            retryCount = 0;
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
            }
            if (signValidateHandler != null) {
                signValidateHandler.removeCallbacks(validateSignThread);
            }
            nextProcessing();
        }
    };
    /**
     * Set visible next button
     *
     * @author Karthick
     * @created 20161221
     */
    public Runnable validateSignThread = new Runnable() {
        public void run() {
            buttonCheckInNow.setVisibility(View.VISIBLE);
            if (handlerAutoCheckIn != null) {
                handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
                handlerAutoCheckIn.postDelayed(autoCheckInThread,
                        MainConstants.autoCheckInTime);
            }
        }
    };
    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };
    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerBack = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            //showDetailDialog();
        }
    };
    /**
     * Clear the VisitorSign path.
     *
     * @author Karthick
     * @created 20170508
     */
    private OnClickListener listenerVisitorSignClear = new OnClickListener() {

        public void onClick(View v) {
            removeView(relativeLayoutGetVisitorSign,
                    singleTouchViewGetVisitorSign);
            textViewVisitorSignature.setVisibility(View.VISIBLE);
            signValidateHandler.removeCallbacks(validateSignThread);
            buttonCheckInNow.setVisibility(View.GONE);
            if (handlerAutoCheckIn != null) {
                handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
            }
            if (imageViewSignDuplicate != null) {
                imageViewSignDuplicate.setVisibility(View.INVISIBLE);
                imageViewSignDuplicate.setImageBitmap(null);
            }
            SingletonVisitorProfile.getInstance().setVisitorSign(new byte[0]);
            SingletonVisitorProfile.getInstance().setVisitorSignWithBG(new byte[0]);
        }
    };
    /**
     * Handle visitor signature layout
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnTouchListener visitorSignListner = new OnTouchListener() {

        public boolean onTouch(View v, MotionEvent event) {
            imageViewErrorInVisitorSign.setVisibility(View.INVISIBLE);
            if (imageViewSignDuplicate != null) {
                imageViewSignDuplicate.setVisibility(View.INVISIBLE);
            }
            textViewVisitorSignature.setVisibility(View.INVISIBLE);
            if (signValidateHandler != null) {
                signValidateHandler.removeCallbacks(validateSignThread);
                signValidateHandler.postDelayed(validateSignThread, MainConstants.signValidateTime);
            }
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.noActionTime);
            }
            if (handlerAutoCheckIn != null) {
                handlerAutoCheckIn.removeCallbacks(autoCheckInThread);
                handlerAutoCheckIn.postDelayed(autoCheckInThread,
                        MainConstants.autoCheckInTime);
            }
            return false;
        }

    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerConfirm = new OnClickListener() {
        public void onClick(View v) {
            if (validateSingature()) {
                setSignature();
                nextProcessing();
            }
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerPrevious = new OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };
    /*
     * listener to retry upload data or goto home
     *
     * @author Karthick
     *
     * @created 20161222
     */
    private OnClickListener listenerRetry = new OnClickListener() {

        public void onClick(View v) {
            if (SingletonVisitorProfile.getInstance().getRetryCount() < MainConstants.retryMaxCount) {
                nextProcessing();
            } else {
                DialogView.hideDialog();
                actionCode = MainConstants.homePress;
                launchHomePage();
            }
        }
    };
    /**
     * Handle tab click for goto settings page
     *
     * @author Karthick
     * @created on 20170508
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            launchHomePage();
        }
    };
    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerHomePage = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.homePress;
            showDetailDialog();
        }
    };
    /**
     * Goto home page when no action.
     *
     * @author Karthick
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };
    /**
     * Hide dialog
     *
     * @author Karthick
     * @created 20161221
     */
    Runnable hideDialogTask = new Runnable() {
        public void run() {
            DialogView.hideDialog();
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };
}
