package com.zoho.app.frontdesk.android.Keys_management;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import constants.DataBaseConstants;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "keylist.db";
    private static final String dbKeyList = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameKeyList
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbColumnKeyID + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnKeyType + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnKeyPhoto + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnSecurityDetails + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnVehicleNumber + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnRoomDetails + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnFurnitureType + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnOfficeLocation + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnLocationInsideOffice + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnStatus + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnSpareKeyCount + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbColumnIsActive + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnDescription + " Text)";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(dbKeyList);
        Log.e(" dbKeyList"," ************** "+dbKeyList);
        db.execSQL("create table test(NAME TEXT,AGE INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL("DROP TABLE "+DataBaseConstants.tableNameKeyList);sa[p
        db.execSQL("DROP TABLE test");
    }
}

