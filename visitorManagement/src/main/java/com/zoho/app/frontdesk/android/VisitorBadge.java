package com.zoho.app.frontdesk.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import appschema.SingletonVisitorProfile;
import constants.FontConstants;
import constants.MainConstants;


/**
 * Created by jayapriya On 14/11/2014
 * Created the constructor to get the details into the singleton variable.
 *
 *
 */
public class VisitorBadge extends LinearLayout {
	
	Context newContext;
    Boolean color;
    String title;
    LayoutInflater inflater;
    RelativeLayout layout;
    ImageView imageViewVisitorImage;
    ImageView imageViewQrCode;
    TextView  textViewGetVisitorName;//,textViewGetDate,textViewGetDateSubTitle;


     /**
     * Created by jayapriya On 14/11/2014
     * Created the constructor to get the details into the singleton variable.
     * @param context
     * @param attrs
     */
	public VisitorBadge(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.newContext = context;
		LayoutInflater.from(context).inflate(R.layout.visitor_badge, this);
		imageViewVisitorImage = (ImageView) findViewById(R.id.imageViewVisitorBadgeImage);
        imageViewQrCode =(ImageView) findViewById(R.id.imageViewVisitorQrcode);
		textViewGetVisitorName = (TextView)findViewById(R.id.textViewVisitorName);
		//textViewGetDate = (TextView) findViewById(R.id.textViewGetVisitingDate);
        //textViewGetDateSubTitle=(TextView) findViewById(R.id.textViewDateSubTitle);

        //set font
       // Typeface typefaceSetDate = Typeface.createFromAsset(newContext.getAssets(),
            //FontConstants.fontConstantLatoReg);
       //textViewGetDate.setTypeface(typefaceSetDate);
       //textViewGetDateSubTitle.setTypeface(typefaceSetDate);
       Typeface typefaceSetName = Typeface.createFromAsset(context.getAssets(),
               FontConstants.fontConstantSourceSansProbold);
       textViewGetVisitorName.setTypeface(typefaceSetName);
        setView();

       	}


    /*
     * set view informations like visitor image, qrcode and check in date.
     * @author jayapriya
     * @created 14/11/2014
     */
    private void setView(){
        if ((SingletonVisitorProfile.getInstance().getVisitorImage() != null)
                && (SingletonVisitorProfile.getInstance().getVisitorImage().length>0)) {

          /*  Bitmap bitmap;
            BitmapFactory.Options options = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(SingletonVisitorProfile.getInstance().getVisitorImage(), options);
            if (bitmap != null) {

                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                        300,
                        300, true);
                // //Log.d("bitap","bitmap" +bitmap);
                // set circle bitmap
                Bitmap resizedBitmap = utility.UsedBitmap
                        .getRoundedRectBitmap(resized);
                // //Log.d("bitap","bitmap" +resizedBitmap);
                imageViewVisitorImage.setImageBitmap(resizedBitmap);
                // resizedBitmap.recycle();
                // resized.recycle();

            }*/
            //imageViewVisitorImage.setImageURI(Uri.parse(SingletonVisitorProfile.getInstance().getVisitorImage()));
            setVisitorImage(SingletonVisitorProfile.getInstance().getVisitorImage());
        }

        //if((SingletonVisitorProfile.getInstance().getQrcode()!=null)&&(SingletonVisitorProfile.getInstance().getQrcode() != MainConstants.kConstantEmpty)){

        //	Log.d("window","bitmap" +SingletonVisitorProfile.getInstance().getQrcode());
           // imageViewQrCode.setImageURI(Uri.parse(SingletonVisitorProfile.getInstance().getQrcode()));
        //}

        // imageViewVisitorImage.setImageURI(Uri.parse(SingletonVisitorProfile.getInstance().getVisitorImage()));
        //textViewGetDate.setText(SingletonVisitorProfile.getInstance().getDateOfVisitLocal());
        //textViewGetVisitorName.setText(SingletonVisitorProfile.getInstance().getVisitorName());

    }
    
    private void setVisitorImage(String path) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        bitmap = BitmapFactory.decodeFile(path, options);
        if (bitmap != null) {

            //Bitmap resized = Bitmap.createScaledBitmap(bitmap, MainConstants.visitorBadgeImageWidth,MainConstants.visitorBadgeImageHeight, true);
            
            imageViewVisitorImage.setImageBitmap(bitmap);
            //resized.recycle();
            //resized=null;
        }
        //bitmap.recycle();
        //bitmap=null;  
    }
    
    private void setVisitorImage(byte[] byteData) {
    		Bitmap bitmap = BitmapFactory.decodeByteArray(byteData, 0, byteData.length);
        if (bitmap != null) {
            imageViewVisitorImage.setImageBitmap(bitmap);
        }  
    }
    
    
    public void setVisitorDetails() { 
    	
    	//Log.d("Android", "name:"+SingletonVisitorProfile.getInstance().getVisitorName());
    	if((SingletonVisitorProfile.getInstance().getVisitorName()!=null)&&(SingletonVisitorProfile.getInstance().getVisitorName() != MainConstants.kConstantEmpty)){
    	
    		textViewGetVisitorName.setText(SingletonVisitorProfile.getInstance().getVisitorName());
    	}
    	
    	if((SingletonVisitorProfile.getInstance().getQrcode()!=null)&&(SingletonVisitorProfile.getInstance().getQrcode() != MainConstants.kConstantEmpty)){

            //	Log.d("window","bitmap" +SingletonVisitorProfile.getInstance().getQrcode());
                imageViewQrCode.setImageURI(Uri.parse(SingletonVisitorProfile.getInstance().getQrcode()));
    }
    }
    
}
