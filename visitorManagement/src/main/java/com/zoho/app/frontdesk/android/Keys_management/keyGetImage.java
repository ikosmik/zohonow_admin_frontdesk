package com.zoho.app.frontdesk.android.Keys_management;

import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import appschema.SharedPreferenceController;
import utility.UsedBitmap;
import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;
import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.Keys_management.keyUtility.keySingleton;
import com.zoho.app.frontdesk.android.R;

//import com.zoho.app.frontdesk.android.*;

/**
 * get the visitor image and store it to the singleton and local storage.
 *
 * @author jayapriya
 * @created 31/10/2014
 */
public class keyGetImage extends Activity {
    private TextView textViewSec, textViewGetImageTitle, textViewGetImageSubContent,
            textViewGetImageContent;
    private Handler photoHandler, noFaceHandler;
    private FrameLayout frameLayout;
    private ImageView imageViewGetImage, imageViewBack;
    // imageViewContentPage;
    private int[] drawableImage;
    private Long currentTime;
    private Long countSecs = MainConstants.countSecZero;
    private keyCameraPreview preview;
    private Typeface typeFaceTitle;
    private int rotation, faceDetectionCount;
    private CountDownTimer countDownTimer;
    private RelativeLayout relativeLayoutGetImage, relativeLayoutTitle,
            relativeLayoutFace;
    private MediaPlayer mp;
    private boolean TimerFinished = false, isOneTimeSmilePlay = false,
            isOneTimeNoFacePlay = false;
    //	double currentLightSensorReading;
//	SensorManager sensorManager;
//	Sensor lightSensor;
    private TextToSpeech textToSpeech;
    private boolean isBackPressed = false;
    private boolean isPermissionDialogShow = false;
    private int applicationMode= NumberConstants.kConstantZero;
    private SharedPreferenceController sharedPreferenceController ;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set the orientation as Portrait
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        }
//        Log.d("DSK","GetImageActivity");
        // this.overridePendingTransition(R.anim.animation_enter,
        // R.anim.animation_leave);
        setContentView(R.layout.activity_get_image);
        assignObjects();
//		checkPermission();

        isPermissionDialogShow=true;
        if(isPermissionDialogShow) {
            assignCameraObjects();
            if(countDownTimer!=null) {
                countDownTimer.start();
            }
        }

        int br;
        try {
            br = Settings.System.getInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);

            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = (float) br / 255;
            getWindow().setAttributes(lp);
        } catch (SettingNotFoundException e) {
//			Log.d("SettingNotFoundException", "SettingNotFoundException"+e.getLocalizedMessage());
        }

    }

    /**
     * This fuction is called, when the backPressed.
     *
     * @author jayapriya
     * @created 31/10/2014
     */
    public void onBackPressed() {
        gotoPreviousPage();
    }

    /**
     * When the activity is stopped, then call this fuction.
     *
     * @author jayapriya
     * @created 31/10/2014
     */
    public void onStop() {
        super.onStop();
        countDownTimer.cancel();
    }

    /**
     * When the activity is started, then this fuction is called.
     *
     * @author jayapriya
     * @created 31/10/2014
     */
    public void onStart() {
        super.onStart();
        if(isPermissionDialogShow) {
            countDownTimer.start();
        }
    }

    /**
     * When the activity is resumed, then this fuction is called.
     *
     * @author Gokul
     * @created 20160928
     */
    public void onResume() {
        super.onResume();
        // sensorManager.registerListener(lightSensorEventListener,
        // lightSensor,
        // SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * When the activity is resumed, then this fuction is called.
     *
     * @author Gokul
     * @created 20160928
     */
    public void onPause() {
        super.onPause();
        // sensorManager.unregisterListener(lightSensorEventListener);
    }

    /**
     * This method is called to takePicture and video in the android function.
     *
     * @author jayapriya
     * @created 22/08/2014
     *
     */
    public Runnable takeImage = new Runnable() {
        public void run() {
            try {
                TimerFinished = false;
                // Log.d("current", "Time" + System.currentTimeMillis());
                // if (preview.safeToTakePicture) {
                if (preview != null && preview.camera != null) {
                    preview.camera.takePicture(null, null, jpegCallback);
                }
            } catch (Exception e) {

                gotoHomePage();
            }
            // preview.safeToTakePicture = false;
            // }

        }
    };

    /**
     * Goto home page when no face detection after some time.
     *
     * @author karthick
     * @created 16/06/2015
     *
     */
    public Runnable noFaceThread = new Runnable() {
        public void run() {

            if (faceDetectionCount <= 0) {

                gotoHomePage();
            }
        }
    };

    private void CaptureMusicPlay() {

        mp.start();
    }

    /*
     * this function check the camera availability in device and open the front
     * facing camera.
     *
     * @author jayapriya
     *
     * @created 22/08/2014
     */
    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
            // //Log.d(TAG, "onShutter'd");
        }
    };

    /** Handles data for raw picture */

    PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {

        }
    };

    /*
     * Call capture photo api when face detection
     *
     * @author karthick
     *
     * @created 15/06/2014
     */
    private FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {

        public void onFaceDetection(Face[] faces, Camera camera) {
            faceDetectionCount = faces.length;

            if (faceDetectionCount > 0) {

                if (TimerFinished) {

                    TimerFinished = false;
                    // textViewGetImageTitle.setVisibility(View.INVISIBLE);
                    textViewGetImageTitle.setText(R.string.textGetImageTitle);
                    if (isOneTimeSmilePlay == false) {
                        textSpeach(MainConstants.speachSmilePleaseMsgCode);
                        isOneTimeSmilePlay = true;
                    }
                    isOneTimeNoFacePlay = false;
                    countDownTimer.start();

                    // TODO
                    // photoHandler.post(takeImage);
                }
            } else {

                if (TimerFinished) {
                    textViewGetImageTitle
                            .setText(R.string.textGetImageFaceFront);
                    isOneTimeSmilePlay = false;
                    if (isOneTimeNoFacePlay == false) {
                        textSpeach(MainConstants.speachFaceNotMsgCode);
                        isOneTimeNoFacePlay = true;
                    }
                    // Log.d("preview","frontFacing else:"+faces.length);
                }
            }
        }
    };

    /*
     * this function check the camera availability in device and open the front
     * facing camera.
     *
     * @author jayapriya
     *
     * @created 22/08/2014
     */
    PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            // Log.d("Android camera","picture taken");
//			FileOutputStream outStream = null;
            try {
                CaptureMusicPlay();
                // Log.d("window: ",
                // "before:"+SingletonVisitorProfile.getInstance().getVisitorImage());
//				File fileDirectory = new File(
//						MainConstants.kConstantFileDirectory
//								+ MainConstants.kConstantFileTemporaryFolder);
//
//				if (!fileDirectory.exists()) {
//					fileDirectory.mkdirs();
//				} else {
                // Calendar calendar = Calendar.getInstance();
                // calendar.setTime(new Date());
                // calendar.add(Calendar.HOUR, -1);
                //
                // String[] children = fileDirectory.list();
                // for (int i = 0; i < children.length; i++) {
                //
                // File file = new File(fileDirectory, children[i]);
                // if(calendar.getTime().getTime()>file.lastModified()) {
                // //Log.d("window: ","delete:"+file.lastModified());
                // file.delete();
                // }
                // }
                // Log.d("window: ","delete:"+fileDirectory.delete());
                // fileDirectory.mkdirs();
//				}

                // Output stream to write file
//				currentTime = System.currentTimeMillis();
                // outStream = new FileOutputStream(
                // String.format(
                // constants.MainConstants.kConstantFileDirectory
                // + MainConstants.kConstantFileTemporaryFolder
                // + MainConstants.kConstantVisitorImageFileFormat,
                // currentTime));
                //
                // outStream.write(data);
                // outStream.close();
                // SingletonVisitorProfile.getInstance().setVisitorImage(
                // constants.MainConstants.kConstantFileDirectory
                // + MainConstants.kConstantFileTemporaryFolder
                // + currentTime
                // + MainConstants.kConstantImageFileFormat);

                //SingletonVisitorProfile.getInstance().setVisitorImage(data);
                keySingleton.getInstance().setPerson_photo(data);
                // getImageArray.add(constants.MainConstants.kConstantFileDirectory+MainConstants.kConstantFileTemporaryFolder
                // + currentTime + MainConstants.kConstantImageFileFormat);
                // Log.d("window: ",
                // "after:"+SingletonVisitorProfile.getInstance().getVisitorImage());
            } catch (Exception e) {
                keySingleton.getInstance().setPerson_photo(new byte[0]);
            } finally {
            }

        }
    };

    /*
     * Assigning values to the variables
     *
     * @author jayapriya
     *
     * @created 22/08/2014
     */
    private void assignObjects() {

        imageViewBack = (ImageView) findViewById(R.id.imageViewBackDetail);
        textViewSec = (TextView) findViewById(R.id.textCount);
        imageViewGetImage = (ImageView) findViewById(R.id.imageGetImage);
        // imageViewContentPage =(ImageView) findViewById(R.id.imageViewMain);
        textViewGetImageTitle = (TextView) findViewById(R.id.textGetImageTitle);
        textViewGetImageSubContent = (TextView) findViewById(R.id.textGetImageSubContent);
        textViewGetImageContent = (TextView) findViewById(R.id.textGetImageContent);
        relativeLayoutGetImage = (RelativeLayout) findViewById(R.id.relativeLayoutGetImage);
        relativeLayoutTitle = (RelativeLayout) findViewById(R.id.relativeLayoutTitle);
        relativeLayoutFace = (RelativeLayout) findViewById(R.id.relativeLayoutFace);

        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantLinBiolinum);

        textViewGetImageSubContent.setTypeface(typeFaceTitle);
        textViewGetImageContent.setTypeface(typeFaceTitle);
        Typeface textCountFont = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
        textViewSec.setTypeface(textCountFont);
        textViewGetImageTitle.setTypeface(textCountFont);
        // drawableImage = new int[] { R.drawable.icon_face_surprise,
        // R.drawable.icon_face_smiling, R.drawable.icon_face_grinning };

        // relativeLayoutGetImage.setBackgroundResource(R.drawable.background_get_image);

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        imageViewBack.setOnClickListener(listenergetVisitorPage);
        // getImageArray = new ArrayList<String>();

//		currentLightSensorReading = MainConstants.kConstantZero;
//		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
//		lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        // Log.d("light maximum","maximum light " +
        // lightSensor.getMaximumRange());


        mp = MediaPlayer.create(this, R.raw.camera_shutter_click);

        mp.setOnCompletionListener(new OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {
                getVisitorPage();
            }
        });
    }

    /*
     * Assigning values to the variables
     *
     * @author jayapriya
     *
     * @created 22/08/2014
     */
    private void assignCameraObjects() {

        preview = new keyCameraPreview(this, faceDetectionListener);

        frameLayout = (FrameLayout) findViewById(R.id.framePreview);
        photoHandler = new Handler();
        noFaceHandler = new Handler();
        noFaceHandler.postDelayed(noFaceThread, MainConstants.noFaceTime);
        frameLayout.addView(preview);
        // preview.setCameraDisplayOrientation(this);
        // get device rotation
        rotation = getWindowManager().getDefaultDisplay().getOrientation();
//		Log.d("DSK ","Camera rotation "+rotation);
        preview.setCameraDisplayOrientation(rotation);
        countDownTimer = new CountDownTimer(
                MainConstants.kConstantMillisInFuture, MainConstants.second) {

            public void onTick(long millisUntilFinished) {
                startCountDownToTakePhotos(millisUntilFinished);
            }

            public void onFinish() {
                TimerFinished = true;
                if(textViewSec!=null) {
                    textViewSec.setText(MainConstants.kConstantCountZero);
                }
                if (faceDetectionCount > 0) {

                    /** **/
                    photoHandler.post(takeImage);
                    if(preview!=null && preview.camera!=null) {
                        preview.camera.setFaceDetectionListener(null);
                    }
                }
            }
        };
    }

    /**
     * Start the countdown to set the resources and take the picture in the
     * activity.
     *
     * @param millisUntilFinished
     *            get the time as a milliseconds in long
     * @author jayapriya
     * @created 22/08/2014
     */
    protected void startCountDownToTakePhotos(long millisUntilFinished) {

        countSecs = millisUntilFinished / MainConstants.second;
        // Log.d("window",
        // "startCountDownToTakePhotos:"+millisUntilFinished+":"+countSecs);
        // Log.d("window",
        // "startCountDownToTakePhotos:"+faceDetectionCount+":face count");
        if ((countSecs != null) && (countSecs >= 0L)) {
            textViewSec.setText(countSecs.toString());

            relativeLayoutFace.setVisibility(View.VISIBLE);
            imageViewGetImage.setVisibility(View.GONE);
            textViewGetImageTitle.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.VISIBLE);
            // imageViewBack.setVisibility(View.INVISIBLE);
            relativeLayoutTitle.setBackgroundColor(Color.TRANSPARENT);
            /*
             * if (countSecs == MainConstants.countSecFive) {
             * frameLayout.setVisibility(View.INVISIBLE);
             * relativeLayoutFace.setVisibility(View.INVISIBLE);
             * imageViewGetImage
             * .setImageResource(drawableImage[MainConstants.kConstantZero]);
             *
             * } else if (countSecs == MainConstants.countSecFour) {
             * frameLayout.setVisibility(View.INVISIBLE);
             * relativeLayoutFace.setVisibility(View.INVISIBLE);
             * imageViewGetImage
             * .setImageResource(drawableImage[MainConstants.kConstantOne]);
             *
             * } else if (countSecs == MainConstants.countSecThree) {
             * frameLayout.setVisibility(View.INVISIBLE);
             * relativeLayoutFace.setVisibility(View.INVISIBLE);
             * imageViewGetImage
             * .setImageResource(drawableImage[MainConstants.kConstantTwo]);
             *
             * } else if (countSecs == MainConstants.countSecTwo) {
             * //imageViewBack.setVisibility(View.INVISIBLE);
             * //imageViewContentPage.setVisibility(View.INVISIBLE);
             * relativeLayoutFace.setVisibility(View.VISIBLE);
             * imageViewGetImage.setVisibility(View.GONE);
             * textViewGetImageTitle.setVisibility(View.INVISIBLE);
             * frameLayout.setVisibility(View.VISIBLE);
             * imageViewBack.setVisibility(View.INVISIBLE);
             * relativeLayoutTitle.setBackgroundColor(Color.TRANSPARENT);
             * textViewGetImageSubContent.setTextColor(Color.rgb(32, 120, 201));
             * textViewSec.setTextColor(Color.rgb(32, 120, 201));
             * textViewGetImageContent.setTextColor(Color.rgb(32, 120, 201));
             * //photoHandler.postDelayed(takeImage, MainConstants.kConstantOne
             * //* MainConstants.kConstantDelayFirstPhoto);
             *
             * //photoHandler.postDelayed(takeImage, MainConstants.kConstantOne
             * //* MainConstants.kConstantDelayThirdPhoto); }
             */
        }

    }

    /*
     * intent to launch the next activity
     *
     * @author jayapriya
     *
     * @created 22/08/2014
     *
     * private void nextProcess() {
     *
     * //Intent intent = new Intent(GetImageActivity.this,
     * //ImageProcessingActivity.class); rotation =
     * getWindowManager().getDefaultDisplay().getOrientation();
     * utility.UsedBitmap
     * .storeresizedImage(SingletonVisitorProfile.getInstance()
     * .getVisitorImage() ,rotation);
     *
     * //Intent intent= new Intent(this,ActivityHaveAnyDevices.class );
     * //intent.putExtra(MainConstants.kConstantGetImage, getImageArray);
     * //startActivity(intent); finish();
     *
     * }
     */

    /*
     * get home page.
     *
     * @author jayapriya
     *
     * @created 17/11/2014
     */
    private OnClickListener listenergetVisitorPage = new OnClickListener() {

        public void onClick(View v) {
            gotoPreviousPage();
        }
    };

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170405
     */
    private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                // Locale loc = new Locale ("en","IN");
                // textToSpeech.setLanguage(loc);
                textSpeach(MainConstants.speachSmilePleaseMsgCode);
            }
        }
    };

    /*
     * deallocate the objects.
     *
     * @author jayapriya
     *
     * @created 31/10/2014
     */
    private void deallocateObjects() {
        noFaceHandler.removeCallbacks(noFaceThread);
        photoHandler.removeCallbacks(takeImage);
        countDownTimer.cancel();
        countSecs = null;
        currentTime = 0L;
        textViewSec = null;
        imageViewGetImage = null;
        textViewGetImageTitle = null;
        textViewGetImageSubContent = null;
        textViewGetImageContent = null;
        applicationMode =  NumberConstants.kConstantZero;
        photoHandler = null;
        noFaceHandler = null;
        frameLayout = null;
        drawableImage = null;
        // imageViewContentPage=null;
        preview = null;
        typeFaceTitle = null;
        rotation = MainConstants.kConstantZero;
        relativeLayoutGetImage
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutGetImage = null;
        relativeLayoutTitle = null;
        // sensorManager.unregisterListener(lightSensorEventListener);
//		sensorManager = null;
//		lightSensor = null;
        if (preview != null) {
            preview.stopFaceDetectionListener();
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech = null;
        }
    }

    /**
     * Goto previous page depends on is new visitor
     *
     * @author Gokul
     * @created 20170518
     */
    private void gotoPreviousPage() {

        isBackPressed = true;
        if (photoHandler != null) {
            photoHandler.removeCallbacks(takeImage);
        }
        if (preview != null) {
            preview.stopFaceDetectionListener();
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        Intent intent = new Intent(this, ActivityKeyMenu.class);
            startActivity(intent);

        finish();
    }

    /*
     * intent to launch the previous Activity.
     *
     * @author jayapriya
     *
     * @created 17/11/2014
     */
    private void getVisitorPage() {
        if (isBackPressed == false) {
            rotation = getWindowManager().getDefaultDisplay().getOrientation();
            UsedBitmap usedBitmap = new UsedBitmap();
            byte[] bytesArray;

            if(sharedPreferenceController==null)
            {
                sharedPreferenceController = new SharedPreferenceController();
            }
            applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
            if(applicationMode==NumberConstants.kConstantDataCentreMode)
            {
                int cameraID = findFrontFacingCamera();
                bytesArray = usedBitmap.storeresizedImage(
                        keySingleton.getInstance().getPerson_photo(),
                        0,cameraID,getApplicationContext());
            }
            else
            {
                bytesArray = usedBitmap.storeresizedImage(
                        keySingleton.getInstance().getPerson_photo(),
                        0);
            }
            if (bytesArray != null) {
//                SingletonVisitorProfile.getInstance().setVisitorImage(
//                        bytesArray);
                keySingleton.getInstance().setPerson_photo(bytesArray);
            }
            Intent intent = new Intent(this, ActivityKeyMenu.class);
            startActivity(intent);
            finish();
        }
    }

    private int findFrontFacingCamera() {
        int cameraId = NumberConstants.kConstantMinusOne;
        // Search for the front facing camera
        int numberOfCameras = NumberConstants.kConstantZero;
        numberOfCameras = Camera.getNumberOfCameras();

        for (int i = NumberConstants.kConstantZero; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    /*
     * Sensor listener for listening the light
     *
     * @author Gokul
     *
     * @created 20160928
     */
//	SensorEventListener lightSensorEventListener = new SensorEventListener() {
//
//		public void onAccuracyChanged(Sensor sensor, int accuracy) {
//		}
//
//		public void onSensorChanged(SensorEvent event) {
//			if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
//				currentLightSensorReading = event.values[0];
//				// Log.d("light current value ",
//				// "light "+currentLightSensorReading);
//				// Toast.makeText(getApplicationContext(),
//				// "current value  "+currentLightSensorReading,
//				// Toast.LENGTH_SHORT).show();
//			}
//		}
//
//	};

    /*
     * intent to launch the Home Activity.
     *
     * @author karthick
     *
     * @created 17/06/2014
     */
    private void gotoHomePage() {
        Intent intent = new Intent(getApplicationContext(), ActivityKeyMenu.class);
        startActivity(intent);
        finish();
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170405
     */
    private void textSpeach(int textSpeachCode) {

        if (textToSpeech != null) {
            if (textSpeachCode == MainConstants.speachSmilePleaseMsgCode) {
                textToSpeech.speak(TextToSpeachConstants.speachSmilePlease,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachFaceNotMsgCode) {
                textToSpeech.speak(TextToSpeachConstants.speachNoface,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Check the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED ) {
                requestCameraPermission();
            } else {
                if(countDownTimer!=null) {
                    countDownTimer.start();
                }
                isPermissionDialogShow=true;
            }
        }
    }

    /**
     * Request the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                NumberConstants.requestAllPermission);
    }

    /**
     * Handle app permission response
     *
     * @author Karthick
     * @created 20170208
     */

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == NumberConstants.requestAllPermission) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isPermissionDialogShow=true;
                assignCameraObjects();
                countDownTimer.start();
            } else {
                gotoPreviousPage();
            }
        } else {
//super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
