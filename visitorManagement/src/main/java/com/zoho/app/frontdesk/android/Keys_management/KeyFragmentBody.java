package com.zoho.app.frontdesk.android.Keys_management;

import android.app.Fragment;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.Keys_management.keyUtility.keySingleton;
import com.zoho.app.frontdesk.android.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import constants.MainConstants;

import static android.content.Context.MODE_PRIVATE;

public class KeyFragmentBody extends Fragment {


    private TextView person_name_title;
    private TextView person_contact_title;
    private TextView person_name_error;
    private TextView person_contact_error;
    private TextView person_photo_error;
    private TextView person_sign_error;
    private EditText person_name;
    private EditText person_contact;
    private ImageView person_photo;
    private Button button_confirm;
    private DrawingView drawingView;
    private ImageView sign_clear;
    private ImageView take_picture;
    private ImageView person_signature;
    private LinearLayout layout_signature;
    public Context context;
    private boolean is_phone_no;
    private byte[] person_photo_byte;

    //here declaring all needed views for this fragment
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //
        View view = inflater.inflate(R.layout.activity_key_person_details, container, false);
        //
        context = this.getActivity();
        assignObjects(view);
        assign_listeners();
        assignValues();
        //
        return view;
    }


    public void assignObjects(View view)
    {
        //here creating objects for all views in the layout

        person_name_title = view.findViewById(R.id.text_view_person_name);
        person_contact_title = view.findViewById(R.id.text_view_person_contact);
        person_name_error = view.findViewById(R.id.text_view_error_name);
        person_contact_error = view.findViewById(R.id.text_view_error_contact);
        person_name = view.findViewById(R.id.edit_text_person_name);
        person_contact = view.findViewById(R.id.edit_text_person_contact);
        person_photo = view.findViewById(R.id.image_view_person_photo);
        person_photo_error = view.findViewById(R.id.text_photo_error);
        person_sign_error = view.findViewById(R.id.text_sign_error);
        button_confirm = view.findViewById(R.id.person_details_confirm);
        sign_clear = view.findViewById(R.id.imageView_clear);
        take_picture = view.findViewById(R.id.imageView_camera);
        drawingView = new DrawingView(this.getActivity());
        person_signature = view.findViewById(R.id.image_view_signature);
        layout_signature=(LinearLayout)view.findViewById(R.id.view_person_signature);
        person_photo.setImageResource(R.drawable.camera_bg_pic);
        person_name_error.setVisibility(View.INVISIBLE);
        person_contact_error.setVisibility(View.INVISIBLE);
        person_signature.setVisibility(View.GONE);
        layout_signature.setVisibility(View.VISIBLE);
        layout_signature.addView(drawingView);
        drawingView.setSign_drawn(false);
        person_name_error.setVisibility(View.INVISIBLE);
        person_contact_error.setVisibility(View.INVISIBLE);
        person_sign_error.setTextColor(Color.parseColor("#99343638"));
        person_photo_error.setTextColor(Color.parseColor("#99343638"));
        is_phone_no = false;
    }

    public void assignValues(){
        if(keySingleton.getInstance().getPerson_name() != null &&
                !keySingleton.getInstance().getPerson_name().isEmpty())
        {
            person_name.setText(keySingleton.getInstance().getPerson_name());
        }
        if(keySingleton.getInstance().getPerson_contact() != null &&
                !keySingleton.getInstance().getPerson_contact().isEmpty())
        {
            person_contact.setText(keySingleton.getInstance().getPerson_contact());
        }
        if(keySingleton.getInstance().getPerson_photo() != null
                && !keySingleton.getInstance().getPerson_photo().toString().isEmpty())
        {
            person_photo_byte = keySingleton.getInstance().getPerson_photo();
            // Log.e("byte data"," *********** "+data);
            Bitmap bmp = BitmapFactory.decodeByteArray(person_photo_byte, 0, person_photo_byte.length);
            person_photo.setImageBitmap(bmp);
        }
        if(keySingleton.getInstance().getSignImage_path() != null && !keySingleton.getInstance().getSignImage_path().isEmpty())
        {
            drawingView.mBitmap = showImageFromPath(keySingleton.getInstance().getSignImage_path());
            person_signature.setImageBitmap(drawingView.mBitmap);
            layout_signature.setVisibility(View.GONE);
            person_signature.setVisibility(View.VISIBLE);
            drawingView.setSign_drawn(true);
        }
        if(person_photo_byte.length>0)
        {
            person_photo_error.setText("Click camera to retake photo");
        }
        else
            {
                person_photo_error.setText("Click camera to take photo");
            }
        keySingleton.getInstance().clearInstance();
    }

    private void StoreToSingleTon() {

        try{
            if (person_name.getText() != null && !person_name.getText().toString().isEmpty()) {
                keySingleton.getInstance().setPerson_name(person_name.getText().toString());
            }
            if (person_contact.getText() != null && !person_contact.getText().toString().isEmpty()) {
                keySingleton.getInstance().setPerson_contact(person_contact.getText().toString());
                if(is_phone_no)
                {
                    keySingleton.getInstance().setIs_phone_number(true);
                }
            }
            if (drawingView.mBitmap != null && person_signature != null && drawingView.isSignDrawn()) {
                keySingleton.getInstance().setSignImage_path(saveImageToStorage(context,drawingView.mBitmap));
            }
            if(person_photo != null && person_photo_byte != null)
            {
                keySingleton.getInstance().setPerson_photo(person_photo_byte);
            }
        }
        catch (Exception e)
        {
          //  Log.e(" StoreToSingleTon"," Error ************ "+e.getMessage());
        }
    }
    //this method will convert bitmap into byte
    public byte[] convertToBytes(Bitmap bitmap){
        ByteBuffer byteBuffer = ByteBuffer.allocate(bitmap.getByteCount());
        bitmap.copyPixelsToBuffer(byteBuffer);
        byte[] bytes = byteBuffer.array();
        return bytes;
    }
    //here assigning the listeners for the views
    public void assign_listeners() {

        person_name.addTextChangedListener(name_changed);
        person_contact.addTextChangedListener(contact_changed);
        button_confirm.setOnClickListener(listener_confirm);
        sign_clear.setOnTouchListener(listener_clear_onTouch);
        take_picture.setOnClickListener(listener_call_camera);
        //layout_signature.setOnTouchListener(listener_touch);
    }


    View.OnClickListener listener_confirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //here calling the functions to validate name
            try{

                if(validate_name() && validate_contact() && validate_signature() && validate_photo())
                {
                    StoreToSingleTon();
                    //Toast.makeText(context,"Stroed to singleton",Toast.LENGTH_LONG).show();
                    ((ActivityKeyMenu) getActivity()).replaceFragment();
                }
                else if(!validate_name() && !validate_contact() && !validate_signature() && !validate_photo())
                {
                    person_name_error.setVisibility(View.VISIBLE);
                    person_contact_error.setVisibility(View.VISIBLE);
                    person_sign_error.setTextColor(Color.parseColor("#BF360C"));
                    person_photo_error.setTextColor(Color.parseColor("#BF360C"));
                    //Toast.makeText(context,"Showing error",Toast.LENGTH_LONG).show();
                }

            }catch (Exception e)
            {
              //  Log.e("Error"," : listener_confirm ****** "+e.getMessage());
            }
        }
    };

    View.OnTouchListener listener_clear_onTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            try {
                if(person_signature.getVisibility() == View.VISIBLE)
                {
                    person_signature.setVisibility(View.GONE);
                }
                if(layout_signature.getVisibility() == View.GONE)
                {
                    layout_signature.setVisibility(View.VISIBLE);
                }
                drawingView.setSign_drawn(false);
                drawingView.clear_bitmap();
                person_signature.setImageBitmap(drawingView.mBitmap);
                if(!keySingleton.getInstance().getSignImage_path().isEmpty())
                {
                    keySingleton.getInstance().setSignImage_path(MainConstants.kConstantEmpty);
                }
            }
            catch (Exception e)
            {
                Log.e("clear","Error ************** "+e.getMessage());
            }
            return false;
        }
    };

//    View.OnClickListener listener_clear = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            try {
//                if(person_signature.getVisibility() == View.VISIBLE)
//                {
//                    person_signature.setVisibility(View.GONE);
//                }
//                if(layout_signature.getVisibility() == View.GONE)
//                {
//                    layout_signature.setVisibility(View.VISIBLE);
//                }
//                drawingView.clear_bitmap();
//                person_signature.setImageBitmap(drawingView.mBitmap);
//                drawingView.setSign_drawn(false);
//            }
//            catch (Exception e)
//            {
//                Log.e("clear","Error ************** "+e.getMessage());
//            }
//        }
//    };

    View.OnClickListener listener_call_camera = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeCameraPictureIntent();
            StoreToSingleTon();
        }
    };

    //this function will call the camera

    private void takeCameraPictureIntent() {
        float density = getResources().getDisplayMetrics().density;
//        //Log.d("DSK Density",""+density);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && density < 2) {
//            //Log.d("DSK Density","in 1 "+density);
            Intent intent = new Intent(getActivity().getApplicationContext(), keyActivityCamera.class);
            startActivity(intent);
        } else {
//            //Log.d("DSK Density","in 2 "+density);
            Intent intent = new Intent(getActivity().getApplicationContext(), keyGetImage.class);
            startActivity(intent);
        }
        getActivity().finish();
    }

    //here creating a text watcher for validation
    TextWatcher name_changed =new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            person_name_error.setText("Please verify your name");
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String name = person_name.getText().toString().toLowerCase().trim();
            name = name.replaceAll("[ ]{2,}","");
            if(!name.isEmpty())
            {
                if (name.matches(MainConstants.kConstantValidText)) {
                    if(person_name_error.getVisibility() == View.VISIBLE)
                    {
                        person_name_error.setVisibility(View.INVISIBLE);
                        person_name.setText(name);
                    }

                } else {
                    if(person_name_error.getVisibility() == View.INVISIBLE)
                    {
                        person_name_error.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    //here creating a text watcher for validation
    TextWatcher contact_changed =new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            person_contact_error.setText("Please verify your contact detail");
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String contact = person_contact.getText().toString().trim();
            contact = contact.replace("[ ]+","");
            if(!contact.isEmpty())
            {
                if (contact.matches(MainConstants.kConstantValidEmailText)
                        || contact.matches(MainConstants.kConstant_Valid_phone_no)) {
                    if(person_contact_error.getVisibility() == View.VISIBLE)
                    {
                        person_contact_error.setVisibility(View.INVISIBLE);
                        person_contact.setText(contact);
                    }

                } else {
                    if(person_contact_error.getVisibility() == View.INVISIBLE)
                    {
                        person_contact_error.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public static String saveImageToStorage(Context mContext, Bitmap bitmap){

        String mTimeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());

        String mImageName = "key_Pers0n_Ph0to.jpg";

        ContextWrapper wrapper = new ContextWrapper(mContext);

        File file = wrapper.getDir("Images",MODE_PRIVATE);

        if(file.exists())
        {
            file.delete();
        }
        file = new File(file, mImageName);

        try{

            OutputStream stream = null;

            stream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);

            stream.flush();

            stream.close();

        }catch (IOException e)
        {
            e.printStackTrace();
        }

        String mImageUri = file.getAbsolutePath();
        //Log.e("filepath"," ********* "+mImageUri);
        return mImageUri;
    }

    public Bitmap showImageFromPath(String filePath){
        File imgFile = new  File(filePath);
        if(imgFile.exists()){
            //Log.e("file ","exists ************ "+imgFile);
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            return myBitmap;
        }
        return null;
    }

    //this function will validate the person's name
    public boolean validate_name()
    {
        String name = person_name.getText().toString().trim();
        name = name.replace("[ ]{2,}","");
        if(!name.isEmpty() && name.length() > 3)
        {
            person_name_error.setText("Please verify your name");
            if(name.matches(MainConstants.kConstantValidText) && !name.matches("[^\\x1F-\\x7F]+"))
            {
                person_name_error.setVisibility(View.INVISIBLE);
                person_name.setText(name);
                return true;
            }
            else
            {
                person_name_error.setVisibility(View.VISIBLE);
                return false;
            }
        }
        else
        {
            person_name_error.setText("Please enter your name");
            person_name_error.setVisibility(View.VISIBLE);
            return false;
        }
    }

    //this function will validate the person's contact details
    public boolean validate_contact()
    {
        String contact_detail = person_contact.getText().toString().trim();
        if(!contact_detail.isEmpty())
        {
            person_contact_error.setText("Please verify your contact detail");
            if(contact_detail.matches(MainConstants.kConstant_Valid_phone_no))
            {
                person_contact_error.setVisibility(View.INVISIBLE);
                person_contact.setText(contact_detail);
                is_phone_no = true;
                return true;
            }
            else  if(contact_detail.matches(MainConstants.kConstantValidEmail))
            {
                person_contact_error.setVisibility(View.INVISIBLE);
                person_contact.setText(contact_detail);
                is_phone_no = false;
                return true;
            }
            else
            {
                person_contact_error.setVisibility(View.VISIBLE);
                return false;
            }
        }
        else
        {
            person_contact_error.setText("Please enter your contact detail");
            person_contact_error.setVisibility(View.VISIBLE);
            return false;
        }
    }

    //this function will validate the person's signature
    public boolean validate_signature()
    {
        //Log.e("validate"," validate signature executed");
        if(drawingView.isSignDrawn() && drawingView.mBitmap != null && person_signature != null)
        {
            person_sign_error.setTextColor(Color.parseColor("#99343638"));
            return true;
        }
        else
        {
            person_sign_error.setTextColor(Color.parseColor("#BF360C"));
            return false;
        }

    }
    //this function will validate the person's photo
    public boolean validate_photo()
    {
        //Log.e("validate"," validate photo executed");
        if(person_photo != null && person_photo_byte != null && person_photo_byte.length>0)
        {
            person_photo_error.setTextColor(Color.parseColor("#99343638"));
            return true;
        }
        else
        {
            person_photo_error.setTextColor(Color.parseColor("#BF360C"));
            return false;
        }
    }

}
