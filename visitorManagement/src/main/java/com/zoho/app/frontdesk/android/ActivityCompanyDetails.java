package com.zoho.app.frontdesk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import utility.DialogViewWarning;
import utility.UsedCustomToast;

public class ActivityCompanyDetails extends Activity {

    private EditText editTextCompanyName;
    private RelativeLayout relativeLayoutCompanyName;
    private ImageView imageviewBack, imageviewHome, imageviewNext, image_view_next,imageViewPrevious;
    private TextView textViewCompanyName, textViewCompanyTitle, textViewCompanyNameError;
    private Typeface typeFaceTitle, typefaceVisitorContact, typefaceErrorMsg;
    private int actionCode = MainConstants.backPress;
    private Handler noActionHandler;
    private TextToSpeech textToSpeech;
    DialogViewWarning dialog;
    private Button button_skip_activity;
    private int applicationMode =  NumberConstants.kConstantZero;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_employee_company_details);

//        assignObjects to the View.
        assignObjects();
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    public void onStart() {
        super.onStart();
        setOldValue();
    }

    /**
     * This function is called, when the activity is stoped.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    public void onStop() {
        super.onStop();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * This function is called, when the activity is destroyed to deallocate object data.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void deallocateObjects() {
        typeFaceTitle = null;
        textViewCompanyNameError = null;
        editTextCompanyName = null;
        textViewCompanyTitle = null;
        relativeLayoutCompanyName = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        image_view_next = null;
        imageViewPrevious = null;

        actionCode =  NumberConstants.kConstantZero;
        applicationMode =  NumberConstants.kConstantZero;
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
    }

    /**
     * Assign Objects to the variable
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void assignObjects() {

        editTextCompanyName = (EditText) findViewById(R.id.editTextcompanyName);
        textViewCompanyNameError = (TextView) findViewById(R.id.textViewErrorInName);
        textViewCompanyTitle = (TextView) findViewById(R.id.textview_company_name_title);

        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);
        imageViewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        button_skip_activity = (Button) findViewById(R.id.button_skip_page);

        relativeLayoutCompanyName = (RelativeLayout) findViewById(R.id.relativelayout_company_details);

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        editTextCompanyName
                .setInputType( InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS|InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceErrorMsg = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskItalic);

        typefaceVisitorContact = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);

        editTextCompanyName.setTypeface(typefaceVisitorContact);
        textViewCompanyNameError.setTypeface(typefaceErrorMsg);
        textViewCompanyTitle.setTypeface(typeFaceTitle);
        Typeface typefaceButton = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskSemiBold);

        // Onclick listeners
        image_view_next.setOnClickListener(listenerConfirm);
        imageviewNext.setOnClickListener(listenerConfirm);
        imageviewBack.setOnClickListener(listenerBack);
        imageViewPrevious.setOnClickListener(listenerBack);
        button_skip_activity.setOnClickListener(listener_skip);
        button_skip_activity.setTypeface(Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskSemiBold));

        editTextCompanyName.setOnEditorActionListener(listenerDonePressed);
        editTextCompanyName.addTextChangedListener(textWatcherNameChanged);

        if (SingletonMetaData.getInstance() != null) {
            applicationMode = SingletonMetaData.getInstance().getApplicationMode();
        }

        setAppBackground();
    }

    //this listener is used to skip the company name field
    View.OnClickListener listener_skip = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //here calling the functions to got to next page
            showWarningDialog(StringConstants.kConstantSkipEmployeeMessage);
        }
    };

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showWarningDialog(String errorMessage) {
        hideKeyboard();
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(errorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //here calling the functions to got to next page
            editTextCompanyName.setText(" ");
            launchEmployeePage();
            SingletonVisitorProfile.getInstance().setCompany("");
        }
    };

    /**
     * Set background color
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutCompanyName.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFba6fb6, 0xFF8e76c3, 0xFF8478c6, 0xFF8278c7});
                relativeLayoutCompanyName.setBackground(gd);
            }
        }
    }

    /**
     * Assign text to voice listener
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * Play text to voice
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void textSpeach(int textSpeachCode) {
        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (editTextCompanyName != null
                    && editTextCompanyName.getText() != null && editTextCompanyName
                    .getText().toString().length() <= 0)) {
                textToSpeech.speak(TextToSpeachConstants.speachCompanyName,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Hide keyboard
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * intent to launch previous page
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            hideKeyboard();
            launchPreviousPage();
        }
    };


    /**
     * when the done pressed then process start.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private TextView.OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                if (validateCompanyName()) {
                    launchEmployeePage();
                } else {
                    UsedCustomToast.makeCustomToast(getApplicationContext(), "Enter Valid Company Name", Toast.LENGTH_SHORT, Gravity.TOP).show();
                }
                return true;
            }
            return false;
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private View.OnClickListener listenerConfirm = new View.OnClickListener() {
        public void onClick(View v) {
            if (validateCompanyName()) {
                launchEmployeePage();
            } else {
                UsedCustomToast.makeCustomToast(getApplicationContext(), "Enter Valid Company Name", Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        }
    };

    /**
     * Launch previous activity
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void launchHomePage() {
        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, HomePage.class);
            startActivity(intent);
        }
        finishAffinity();
    }

    /**
     * Validate Edittext Field Values
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private boolean validateCompanyName() {
        boolean isCompanyStored = false;
        if (editTextCompanyName != null) {
            String edittextValues = editTextCompanyName.getText().toString().trim();
            if (edittextValues != null && !edittextValues.isEmpty() && edittextValues.length() > 2) {
                if(edittextValues.matches(MainConstants.kConstantValidCompanyName)) {
                    SingletonVisitorProfile.getInstance().setCompany(edittextValues);
                    isCompanyStored = true;
                }
                else {
                    isCompanyStored = false;
                }
            } else {
                isCompanyStored = false;
            }
        }
        return isCompanyStored;
    }

    /**
     * Launch Previous Page activity
     *
     * @author Karthickeyan D S
     * @created on 02JUl2018
     */
    private void launchPreviousPage() {
        Intent intent = new Intent(this, ActivityVisitorName.class);
        startActivity(intent);
        finishAffinity();
    }

    /**
     * Launch next activity.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void launchEmployeePage() {
        String deviceLocation = MainConstants.kConstantEmpty;
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getDeviceLocation() != null
                && !SingletonMetaData.getInstance().getDeviceLocation().isEmpty()) {
            deviceLocation = SingletonMetaData.getInstance().getDeviceLocation();
        }
//			Log.d("DSKActivityCode1 ", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//			Log.d("DSKEmployeeId1 ", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
//			Log.d("DSKdeviceLocation ", "" + deviceLocation);
        if (deviceLocation != null && !deviceLocation.isEmpty() &&
                deviceLocation.equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
//			Log.d("DSKActivityCode1 ", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//			Log.d("DSKEmployeeId1 ", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
//			Log.d("DSKdeviceLocation ", "" + deviceLocation);
            if (SingletonVisitorProfile.getInstance() != null &&
                    SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 1) {
//				Log.d("EmployeeActivityCod2", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//				Log.d("EmployeeId2", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                callNextPageActivity(1);
            } else if (SingletonVisitorProfile.getInstance() != null &&
                    SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 2) {
//				Log.d("EmployeeActivityCode3", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//				Log.d("EmployeeId3", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                if (SingletonEmployeeProfile.getInstance() != null &&
                        !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty() &&
                        SingletonEmployeeProfile.getInstance().getEmployeeId().startsWith("ZT-")) {
//					Log.d("EmployeeActivityCode4", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId4", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    callNextPageActivity(2);
                } else if (SingletonEmployeeProfile.getInstance() != null &&
                        !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty() &&
                        SingletonEmployeeProfile.getInstance().getEmployeeId() != null) {
//					Log.d("EmployeeActivityCode5", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId5", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    callNextPageActivity(1);
                } else {
//					Log.d("EmployeeActivityCode6", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId6", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    callNextPageActivity(2);
                }
            } else if (SingletonVisitorProfile.getInstance() != null &&
                    SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 3) {
                callNextPageActivity(1);
            } else {
                if (SingletonEmployeeProfile.getInstance() != null &&
                        SingletonEmployeeProfile.getInstance().getEmployeeId() != null
                        && !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()
                        && SingletonEmployeeProfile.getInstance().getEmployeeId().startsWith("ZT-")) {
//					Log.d("EmployeeActivityCode7", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId7", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());

                    callNextPageActivity(2);
                } else if (SingletonEmployeeProfile.getInstance() != null &&
                        SingletonEmployeeProfile.getInstance().getEmployeeId() != null &&
                        !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()) {
//					Log.d("EmployeeActivityCode8", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId8", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    callNextPageActivity(1);
                } else {
//					Log.d("EmployeeActivityCode9", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId9", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    callNextPageActivity(2);
                }
            }

        } else {
//			if(SingletonVisitorProfile.getInstance()!=null &&
//					SingletonVisitorProfile.getInstance().getEmployeeActivityCode()==1)
//			{
//				callNextPageActivity(1);
//
//			}
//			else if(SingletonVisitorProfile.getInstance()!=null &&
//					SingletonVisitorProfile.getInstance().getEmployeeActivityCode()==2)
//			{
//				callNextPageActivity(2);
//			}
//			else
//			{
            callNextPageActivity(1);
//			}
        }
    }

    /**
     * Launch next page activity.
     *
     * @author Karthikeyan D S
     * @created on 20180403
     */
    private void callNextPageActivity(int nextPageActivityCode) {
        if (nextPageActivityCode == 1) {
            Intent intent = new Intent(getApplicationContext(),
                    ActivityVisitorEmployee.class);
            startActivity(intent);
            finish();
        } else if (nextPageActivityCode == 2) {
            Intent intent = new Intent(getApplicationContext(),
                    ActivityVisitorEmployeeTenkasi.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };

    /**
     * This function is called when the back pressed.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * This function is called to set old values.
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void setOldValue() {
        if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getCompany() != null &&
                !SingletonVisitorProfile.getInstance().getCompany().trim().isEmpty()) {
            if (editTextCompanyName != null) {
                editTextCompanyName.setText(SingletonVisitorProfile.getInstance().getCompany().trim());
            }
        }
    }

    /**
     * textWatcher to validate the VisitorMobileNo
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private TextWatcher textWatcherNameChanged = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            validateNameTyped();
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
        }

        public void afterTextChanged(Editable s) {
        }
    };

    /**
     * Check is existing user or not
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void validateNameTyped() {
        if (editTextCompanyName!=null) {
            String editTextCompanyNameValue = editTextCompanyName.getText().toString().trim();
            if ((editTextCompanyNameValue != null)
                    && (!editTextCompanyNameValue.isEmpty())
                    && (editTextCompanyNameValue.length() > MainConstants.kConstantZero)) {
                validateCompanyName();
            } else {
                textViewCompanyNameError.setVisibility(View.INVISIBLE);
            }
            if (imageviewNext != null) {
                if (editTextCompanyNameValue != null
                        && editTextCompanyNameValue.length() >= MainConstants.kConstantThree) {
                    if (imageviewNext != null) {
                        imageviewNext.setVisibility(View.VISIBLE);
                    }
                    if (image_view_next != null) {
                       image_view_next.setVisibility(View.VISIBLE);
                       button_skip_activity.setVisibility(View.GONE);
                    }
                } else {
                    if (image_view_next != null) {
                        image_view_next.setVisibility(View.GONE);
                        button_skip_activity.setVisibility(View.VISIBLE);
                    }
                    if (imageviewNext != null) {
                        imageviewNext.setVisibility(View.GONE);
                    }
                }
            }
        }
    }
}
