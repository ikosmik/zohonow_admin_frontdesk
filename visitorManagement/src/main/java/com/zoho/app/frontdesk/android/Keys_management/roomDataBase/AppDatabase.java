package com.zoho.app.frontdesk.android.Keys_management.roomDataBase;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = keyEntity.class, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DatabaseAccessInterface appDatabaseObject();
}
