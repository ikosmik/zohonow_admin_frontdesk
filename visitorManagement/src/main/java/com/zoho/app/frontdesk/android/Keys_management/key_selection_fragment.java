package com.zoho.app.frontdesk.android.Keys_management;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.Keys_management.keyUtility.keySingleton;
import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.DataBaseRepository;
import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.keyEntity;
import com.zoho.app.frontdesk.android.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import appschema.SingletonVisitorProfile;
import constants.MainConstants;
import utility.CustomKeyListAdapter;
import utility.JSONParser;

public class key_selection_fragment extends Fragment {

    public ListView list_view_key_list;
    public TextView text_key_count;
    public ImageView image_view_back;
    public ImageView image_view_confirm;
    public ImageView image_view_clear;
    public ImageView image_view_filter;
    public ImageView image_view_count;
    public LinearLayout linearLayout_options;
    public CustomKeyListAdapter adapter_key_list;
    public DataBaseRepository dataBaseRepository;
    public List<keyEntity> list_keys;
    public ArrayList<String> key_list_value = new ArrayList<String>();
    public ArrayList<String> key_list_description = new ArrayList<String>();
    public ArrayList<String> KeyID = new ArrayList<String>();
    public ArrayList<Integer> item_image = new ArrayList<Integer>();
    public ArrayList<String> spare_key = new ArrayList<String>();
    public ArrayList<Integer> selected_image_list = new ArrayList<Integer>();
    public ArrayList<String> selected_keys_list = new ArrayList<String>();
    public ArrayList<String> bg_color_list = new ArrayList<String>();
    Context context ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.key_selection_layout,container,false);
        assign_objects(view);
        try{
            fetchKeys();
            setKeysToList(list_keys);
        }catch (Exception e){
            Log.e("Failed","******************* Try Again "+e.getMessage());
        }
        //here setting listeners for views
        assign_listeners();
        return view;
    }

    public void assign_objects(View view){
        text_key_count = view.findViewById(R.id.textView_selection_count);
        image_view_back = view.findViewById(R.id.image_go_back);
        image_view_confirm = view.findViewById(R.id.image_confirm_keys);
        image_view_count = view.findViewById(R.id.imageView_count);
        image_view_filter = view.findViewById(R.id.image_view_filter);
        image_view_clear = view.findViewById(R.id.image_clear_keys);
        linearLayout_options = view.findViewById(R.id.layout_footer);
        list_view_key_list = view.findViewById(R.id.list_view_keys);
        //to always show to scroll bar
        list_view_key_list.setScrollbarFadingEnabled(false);
        //here the creating the database object
        dataBaseRepository = new DataBaseRepository(getActivity());
        context = getActivity();
        image_view_back.setImageResource(R.drawable.icons_arrow_back);
        image_view_confirm.setImageResource(R.drawable.icons_arrow_next);
        image_view_clear.setImageResource(R.drawable.icons_broom);
        image_view_filter.setImageResource(R.drawable.icons_filter);
        image_view_count.setImageResource(R.drawable.icons_key_selected);
    }
    public void assign_listeners(){
        list_view_key_list.setOnItemClickListener(listener_selection);
        image_view_clear.setOnClickListener(listener_clear_selections);
        image_view_confirm.setOnClickListener(listener_confirm);
    }

    View.OnClickListener listener_confirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RunInBackgroundPost runInBackgroundPost = new RunInBackgroundPost();
            runInBackgroundPost.execute();
        }
    };

    View.OnClickListener listener_clear_selections = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Clear_all_keys();
            text_key_count.setText(String.valueOf(selected_keys_list.size()));
        }
    };

    AdapterView.OnItemClickListener listener_selection = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Toast.makeText(getActivity(),key_list_value.get(position), Toast.LENGTH_SHORT).show();
            if(selected_keys_list.contains(KeyID.get(position)))
            {
                selected_keys_list.remove(KeyID.get(position));
                selected_image_list.set(position,R.drawable.green_no_tick);
                bg_color_list.set(position,"#FFFFFF");
                adapter_key_list.notifyDataSetChanged();

            }
            else
                {
                    selected_keys_list.add(KeyID.get(position));
                    selected_image_list.set(position,R.drawable.green_tick);
                    bg_color_list.set(position,"#60C2C2C2");
                    adapter_key_list.notifyDataSetChanged();
                }

            text_key_count.setText(String.valueOf(selected_keys_list.size()));
//            Log.e("list"," Selected   *********** : "+KeyID.get(position));
//            Log.e("list"," list_items *********** : "+selected_keys_list);
        }
    };
    //here declarations done
    public void setKeysToList(List<keyEntity> key_list)
    {
        adapter_key_list = new CustomKeyListAdapter(getActivity(),key_list_value,item_image,key_list_description,
                KeyID,spare_key,selected_image_list,bg_color_list);
        list_view_key_list.setAdapter(adapter_key_list);
    }
    //this function is used to clear all selection from key
    public void Clear_all_keys(){
        key_list_value.clear();
        key_list_description.clear();
        KeyID.clear();
        item_image.clear();
        spare_key.clear();
        selected_image_list.clear();
        bg_color_list.clear();
        fetchKeys();
        adapter_key_list.notifyDataSetChanged();
        selected_keys_list.clear();
    }
    public void fetchKeys()
    {
        //here fetching all keys records from room database
        list_keys = dataBaseRepository.fetchAllKeys();
        //here separate and store every details is various lists
        for (int i = 0; i < list_keys.size(); i++)
        {
            key_list_value.add(list_keys.get(i).getKey_title());
            key_list_description.add(list_keys.get(i).getDescription());
            KeyID.add(list_keys.get(i).getKey_id());
            item_image.add(list_keys.get(i).getKey_image());
            spare_key.add(list_keys.get(i).getSpare());
            selected_image_list.add(R.drawable.green_no_tick);
            bg_color_list.add("#FFFFFF");
        }
    }

    //this function will return current time & date in format of date_time in zoho creator
    public String get_current_time()
    {
        //here creating a calendar object
        Calendar cal = Calendar.getInstance();
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String month = String.valueOf(cal.get(Calendar.MONTH));
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(cal.get(Calendar.MINUTE));
        String seconds = String.valueOf(cal.get(Calendar.SECOND));
        //here arranging the values in required date & time format
        String date_time = day+"-"+month+"-"+year+" "+hour+":"+minute+":"+seconds;
        return date_time;
    }
    //here going to create json string from selected keys and person's details

    }
    class RunInBackgroundPost extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try{
                String json_data_post = form_json();
                JSONParser jsonParser = new JSONParser();
                String getUrl = MainConstants.key_management_post+json_data_post;
                URL url = new URL(getUrl);
                String response = jsonParser.restAPICall("POST",url,"");
                Log.e("Url Response ","restAPICall ******* "+response);
            }catch (Exception e){
                Log.e("Failed","******************* Try Again :-) : "+e.getMessage());
            }
            return null;
        }
        public String form_json()
        {
            String formed_json_data = "";
            key_selection_fragment key_selection_fragment = new key_selection_fragment();
            try{
                //here declaring all required json objects and arrays
                JSONObject jsonObject_KeyData = new JSONObject();
                JSONObject jsonObject_personData = new JSONObject();
                JSONArray  jsonArray_Keys = new JSONArray();
                //here put the person details into json object
                jsonObject_personData.put("person_name", keySingleton.getInstance().getPerson_name());
                if(keySingleton.getInstance().Is_phone_number())
                {
                    //if person given phone number then set person contact detail to phone
                    jsonObject_personData.put("person_phone",keySingleton.getInstance().getPerson_contact());
                    jsonObject_personData.put("person_email","empty");
                }
                else
                {
                    //if person email then set person contact detail to email
                    jsonObject_personData.put("person_phone",keySingleton.getInstance().getPerson_contact());
                    jsonObject_personData.put("person_email","empty");
                }
                //here storing the location
                jsonObject_personData.put("office_location", SingletonVisitorProfile.getInstance().getOfficeLocation());
                //here storing the current date and time
                jsonObject_personData.put("device_time",key_selection_fragment.get_current_time());

                //here going to get all keys id and set in a json array
                for (int i = 0; i < key_selection_fragment.selected_keys_list.size(); i++) {
                    JSONObject key_json_object = new JSONObject();
                    key_json_object.put("ID",key_selection_fragment.selected_keys_list.get(i));
                    jsonArray_Keys.put(i,key_json_object);
                }
                //here adding the json array to jsonObject_personData
                jsonObject_KeyData.put("person_details",jsonObject_personData);
                jsonObject_KeyData.put("key_details",jsonArray_Keys);
                formed_json_data = jsonObject_KeyData.toString();
                Log.e("json","output : "+jsonObject_KeyData.toString());
            }
            catch (Exception e){
                Log.e("json","Error : "+e.getMessage());
            }
            return formed_json_data;
    }
}