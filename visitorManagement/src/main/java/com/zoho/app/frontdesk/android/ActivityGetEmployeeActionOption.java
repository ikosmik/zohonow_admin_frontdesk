package com.zoho.app.frontdesk.android;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zoho.app.frontdesk.android.stationery.ActivityStationeryDragDrop;

import appschema.SingletonMetaData;
import constants.FontConstants;
import constants.MainConstants;

public class ActivityGetEmployeeActionOption extends Activity implements View.OnClickListener {

    private RelativeLayout relativeLayoutVisitor, relativeLayoutInterview,
            relativeLayoutForgotID, relativeLayoutVisitorHead, relativeLayoutInterviewHead,
            relativeLayoutForgotIDHead, relativeLayoutHomePage;
    private LinearLayout linearLayoutHomePage;
    private TextView textViewEmployee, textViewVisitor, textViewInterview;
    private Typeface textViewFont, fontSourceSansPro;
    private ImageView imageviewTexture;
    private Bitmap bitmap;
    private boolean alarmUp;
    private Handler noActionHandler;
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_homepage);

        assignObjects();
        setFont();
    }

    /**
     * Assign the values to the variable
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void assignObjects()
    {
        relativeLayoutHomePage = (RelativeLayout) findViewById(R.id.relativeLayoutHomePage);
        relativeLayoutVisitor = (RelativeLayout) findViewById(R.id.relativeLayout_visitor);
        relativeLayoutForgotID = (RelativeLayout) findViewById(R.id.relativeLayout_employee);
        relativeLayoutInterview = (RelativeLayout) findViewById(R.id.relativeLayout_interview);

        relativeLayoutVisitorHead = (RelativeLayout) findViewById(R.id.relativeLayout_visitor_head);
        relativeLayoutForgotIDHead = (RelativeLayout) findViewById(R.id.relativeLayout_employee_head);
        relativeLayoutInterviewHead = (RelativeLayout) findViewById(R.id.relativeLayout_interview_head);

        linearLayoutHomePage = (LinearLayout) findViewById(R.id.linear_layout_main);

        imageviewTexture = (ImageView) findViewById(R.id.imageview_texture);

        textViewEmployee = (TextView) findViewById(R.id.textView_employee);
        textViewVisitor = (TextView) findViewById(R.id.textView_visitor);
        textViewInterview = (TextView) findViewById(R.id.textView_interview);

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noFaceDragDropTime);

        setDeviceScreenColor();
        setView();
        onclickListeners();
    }
    /**
     * onClickListeners
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void onclickListeners()
    {
        relativeLayoutVisitor.setOnClickListener(this);
        relativeLayoutForgotID.setOnClickListener(this);
        relativeLayoutInterview.setOnClickListener(this);
    }

    /**
     * set FontStyle
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void setFont()
    {
        textViewFont = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        fontSourceSansPro = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProBlack);

        textViewEmployee.setTypeface(textViewFont);
        textViewVisitor.setTypeface(textViewFont);
        textViewInterview.setTypeface(textViewFont);
    }
    /**
     * show DeviceScreenColor
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void setDeviceScreenColor()
    {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutHomePage.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                relativeLayoutHomePage.setBackgroundColor(Color.rgb(166, 54, 134));
                relativeLayoutHomePage.setBackgroundColor(Color.rgb(10, 204, 205));
            }
        }
    }
    /**
     * show Forgot ID Circle
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void setView()
    {
//	 show  Forgot ID circle by employee Choose based on the Code.
        if(textViewVisitor!=null) {
            textViewVisitor.setText(getResources().getString(R.string.forget_id));
        }
    }

    /**
     * Goto welcomePage Activity
     *
     * @author Karthikeyan D S
     * @created 22Sep2018
     */
    private void callWelcomeActivity()
    {
        Intent intent = new Intent(getApplicationContext(),
                WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private Runnable noActionThread = new Runnable() {
        public void run() {
            // Log.d("visitor", "noActionThread");
            callWelcomeActivity();
        }
    };


    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    protected void onDestroy() {
        super.onDestroy();
        noActionHandler.removeCallbacks(noActionThread);
        deallocateObjects();
    }

    /**
     * onClick Action of Circle
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    public void onClick(View v) {
        if (v.getId() == R.id.relativeLayout_visitor) {
           forgetIDOnClick();
        } else if (v.getId() == R.id.relativeLayout_employee) {
            stationeryOnClick();
        }
    }

    /**
     * Goto forgetID Activity
     *
     * @author Karthikeyan D S
     * @created 22Sep2018
     */
    private void forgetIDOnClick()
    {
        Intent intent = new Intent(getApplicationContext(),
                EmployeeTemporaryIdActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Goto Stationery Activity
     *
     * @author Karthikeyan D S
     * @created 24Sep2018
     */
    private void stationeryOnClick()
    {
        Intent intent = new Intent(getApplicationContext(),
                ActivityStationeryDragDrop.class);
        startActivity(intent);
        finish();
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void deallocateObjects() {
        // Log.d("Android", "deallocate");
        relativeLayoutVisitor = null;
        relativeLayoutForgotID = null;
        if (relativeLayoutHomePage != null) {
            relativeLayoutHomePage
                    .setBackgroundResource(MainConstants.kConstantZero);
            relativeLayoutHomePage = null;
        }
        if (imageviewTexture != null) {
            imageviewTexture.setBackground(null);
            imageviewTexture = null;
        }
        relativeLayoutInterview = null;
        textViewEmployee = null;
        textViewVisitor = null;
        textViewInterview = null;
        textViewFont = null;
        //drawableBitMap = null;
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        if(noActionHandler!=null)
        {
            noActionHandler.removeCallbacks(noActionThread);
        }
    }
    /**
     * Goto welcome page
     *
     * @author Karthick
     * @created on 20161220
     */
    public void onBackPressed() {
        callWelcomeActivity();
    }
}
