package com.zoho.app.frontdesk.android;

import java.util.Locale;

import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import constants.StringConstants;
import utility.BackgroundUploadVisitorData;
import utility.DialogViewWarning;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;
import appschema.SharedPreferenceController;

public class ActivityVisitorTempID extends Activity {

	private DialogViewWarning dialog;
	private EditText editTextTempIdNumber;
	private RelativeLayout relativeLayoutExisitingVisitor;
	private ImageView imageviewBack, imageviewHome, imageviewPrevious,
			imageviewNext,image_view_next;
	private TextView textviewVisitorTempIDTitle;
	private Typeface typeFaceTitle, typefaceVisitorTempID;
	private int actionCode = MainConstants.backPress;
	private Handler noActionHandler;
//	private int fromPage = MainConstants.fromHaveDevice;
	private TextToSpeech textToSpeech;
	private BackgroundUploadVisitorData backgroundProcess;
	private int applicationMode =  NumberConstants.kConstantZero;
	private SharedPreferenceController sharedPreferenceController;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove the title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// hide the status bar.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_visitor_temp_id);
		assignObjects();
		Log.d("DSK "," VisitorImagelength "+ SingletonVisitorProfile.getInstance().getDeviceMake());
		backgroundProcess = new BackgroundUploadVisitorData(this,
				MainConstants.jsonCreateUploadPhoto);
		if (backgroundProcess != null)
		{
			backgroundProcess.execute();	
		}
	}

	/**
	 * This function is called, when the activity is started.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onStart() {
		super.onStart();
		setOldValue();
	}

	/**
	 * This function is called, when the activity is stopped.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onStop() {
		super.onStop();

	}

	/**
	 * This function is called, when the activity is destroyed.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onDestroy() {
		super.onDestroy();
		deallocateObjects();
	}

	/**
	 * Assign Objects to the variable
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void assignObjects() {

		editTextTempIdNumber = (EditText) findViewById(R.id.editText_temp_id);
//		textViewErrorInTempID = (TextView) findViewById(R.id.textViewErrorInTempID);
		textviewVisitorTempIDTitle = (TextView) findViewById(R.id.textview_temp_id_title);
		imageviewBack = (ImageView) findViewById(R.id.imageview_back);
		imageviewHome = (ImageView) findViewById(R.id.imageview_home);
		imageviewNext = (ImageView) findViewById(R.id.imageview_next);
		imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
		relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);
		image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

		if (textToSpeechListener != null) {
			textToSpeech = new TextToSpeech(getApplicationContext(),
					textToSpeechListener);
		}

		noActionHandler = new Handler();
		noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

//		if(getIntent()!=null) {
//			fromPage = getIntent().getIntExtra(MainConstants.putExtraFromPage, MainConstants.fromContact);
//		}

		sharedPreferenceController = new SharedPreferenceController();
		if(sharedPreferenceController!=null)
		{
			applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
		}

		editTextTempIdNumber.setInputType(InputType.TYPE_CLASS_NUMBER);

		assignFontConstants();

		assignListeners();

		setAppBackground();
	}

	/**
	 * AssignListeners
	 *
	 * @author Karthikeyan D S
	 * @created on 28JUL2018
	 */
	private void assignListeners()
	{
		// Onclick listeners
		if(imageviewPrevious!=null &&listenerPrevious!=null) {
			imageviewPrevious.setOnClickListener(listenerPrevious);
		}
		if(imageviewNext!=null &&listenerConfirm!=null) {
		imageviewNext.setOnClickListener(listenerConfirm);
		}
		if(image_view_next!=null &&listenerConfirm!=null) {
		image_view_next.setOnClickListener(listenerConfirm);
		}
		if(imageviewHome!=null &&listenerHomePage!=null) {
		imageviewHome.setOnClickListener(listenerHomePage);
		}
		if(imageviewBack!=null &&listenerBack!=null) {
		imageviewBack.setOnClickListener(listenerBack);
		}
		editTextTempIdNumber.setOnEditorActionListener(listenerDonePressed);
		editTextTempIdNumber.addTextChangedListener(textWatcherTempIDChanged);
	}

	/**
	 * AssignFontConstants
	 *
	 * @author Karthikeyan D S
	 * @created on 28JUL2018
	 */
	private void assignFontConstants()
	{
		// set font
		typeFaceTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);

		typefaceVisitorTempID = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);

		editTextTempIdNumber.setTypeface(typefaceVisitorTempID);
		textviewVisitorTempIDTitle.setTypeface(typeFaceTitle);
	}
	/**
	 * Set background color
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void setAppBackground() {
		if( SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getThemeCode()>MainConstants.kConstantZero)
		{
			if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantOne) {
				relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
			}
			else if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantTwo)
			{
				GradientDrawable gd = new GradientDrawable(
						GradientDrawable.Orientation.BOTTOM_TOP,
						new int[] {0xFFba6fb6,0xFF8e76c3,0xFF8478c6,0xFF8278c7});
				relativeLayoutExisitingVisitor.setBackground(gd);
			}
		}
	}

	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
		@Override
		public void onInit(int status) {
			if (textToSpeech != null && status != TextToSpeech.ERROR) {
				textToSpeech.setLanguage(Locale.ENGLISH);
			}
			textSpeach(MainConstants.kConstantOne);
		}
	};

	/**
	 * intent to launch previous page
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerBack = new OnClickListener() {

		public void onClick(View v) {
			actionCode = MainConstants.backPress;
			launchPreviousPage();
			//showDetailDialog();
		}
	};

	/**
	 * intent to launch previous page
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerHomePage = new OnClickListener() {

		public void onClick(View v) {
			actionCode = MainConstants.homePress;
			hideKeyboard();
			showDetailDialog();
		}
	};

	/**
	 * when the done pressed then process start.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {

		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {
				if (validateTempID()) {
					storeTempIDNumber();
					launchNextPage();
				}
				return true;
			}
			return false;
		}
	};

	/**
	 * Verify Existing visitor in creator and launched next activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerConfirm = new OnClickListener() {
		public void onClick(View v) {
			if (validateTempID()) {
				storeTempIDNumber();
				launchNextPage();
			}
		}
	};

	/**
	 * Verify Existing visitor in creator and launched next activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerPrevious = new OnClickListener() {
		public void onClick(View v) {
			launchPreviousPage();
		}
	};

	/**
	 * Launch previous activity
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void launchHomePage() {

		if (actionCode == MainConstants.homePress) {
			Intent intent = new Intent(this, WelcomeActivity.class);
			startActivity(intent);
		} else {
			launchNextPage();
		}

		finish();
	}

	/**
	 * Launch previous activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void launchPreviousPage() {
		if (SingletonVisitorProfile.getInstance()!=null && SingletonVisitorProfile.getInstance().isHaveDevice()) {
			if(applicationMode == NumberConstants.kConstantNormalMode) {
				Intent intent = new Intent(getApplicationContext(),
						ActivityDeviceDetails.class);
				startActivity(intent);
			}
			else if(applicationMode == NumberConstants.kConstantDataCentreMode)
			{
				Intent intent = new Intent(getApplicationContext(),
						ActivityDataCentreDeviceDetails.class);
				startActivity(intent);
			}
		} else {
			Intent intent = new Intent(getApplicationContext(),
					ActivityHaveAnyDevices.class);
		startActivity(intent);
		}
		finish();
	}

	/**
	 * Launch next activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void launchNextPage() {
		Intent intent = new Intent(getApplicationContext(),
				ActivityVisitorSignature.class);
		startActivity(intent);
		finish();
	}

	/**
	 * Goto home page when no action.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 * 
	 */
	public Runnable noActionThread = new Runnable() {
		public void run() {
			actionCode = MainConstants.homePress;
			launchHomePage();
		}
	};

	/**
	 * This function is called when the back pressed.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onBackPressed() {
		actionCode = MainConstants.backPress;
		launchPreviousPage();
	}

	/**
	 * textWatcher to validate the VisitorMobileNo
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private TextWatcher textWatcherTempIDChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			validateTempIDTyped();
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
		}

		public void afterTextChanged(Editable s) {
		}

	};

	/**
	 * Check is existing user or not
	 * 
	 * @author Karthick
	 * 
	 * @created 20170508
	 */
	private void validateTempIDTyped() {
		if ((editTextTempIdNumber != null)
				&& (!editTextTempIdNumber.getText().toString().trim().isEmpty())
				&& (editTextTempIdNumber.getText().toString().trim().length() > MainConstants.kConstantZero)) {
			if (validateTempID()) {
				if(imageviewNext!=null) {
					imageviewNext.setVisibility(View.VISIBLE);
				}
				if(image_view_next!=null) {
					image_view_next.setVisibility(View.VISIBLE);
				}
			} else {
				if(imageviewNext!=null) {
					imageviewNext.setVisibility(View.GONE);
				}
				if(image_view_next!=null) {
					image_view_next.setVisibility(View.GONE);
				}
			}
		}
	}

	/**
	 * Check is existing user or not
	 * 
	 * @author Karthick
	 * 
	 * @created 20170508
	 */
	private boolean validateTempID() {

		if ((editTextTempIdNumber != null)
				&& (editTextTempIdNumber.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (editTextTempIdNumber.getText().toString().trim()
						.length() > MainConstants.kConstantZero)
				&& (Integer.parseInt(editTextTempIdNumber.getText()
						.toString().trim()) > MainConstants.kConstantZero)) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * store TempIDNumber
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	private void storeTempIDNumber() {
		long tempID = 0;
		if ((editTextTempIdNumber != null)
				&& (editTextTempIdNumber.getText().toString().trim().length() > MainConstants.kConstantZero)) {
			try {
				tempID = Long.parseLong(editTextTempIdNumber.getText()
							.toString().trim());
			} catch (NumberFormatException e) {
				tempID = 0;
			}
		}
		SingletonVisitorProfile.getInstance().setTempIDNumber(tempID);
	}
	
	/**
	 * Hide keyboard
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void hideKeyboard() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	/**
	 * Show dialog when have user data Otherwise goto welcome page
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void showDetailDialog() {
		showWarningDialog();
		textSpeach(MainConstants.speachAreYouSureCode);	
	}

	/**
	 * Check if have value on edittext Then show warning dialog Otherwise goto
	 * back or home
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void showWarningDialog() {
		hideKeyboard();
		if (dialog == null) {
			dialog = new DialogViewWarning(this);
		}
		dialog.setDialogCode(MainConstants.dialogDecision);
		dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
		dialog.setActionListener(listenerDialogAction);
		dialog.showDialog();
	}

	/**
	 * Handle tab click for goto settings page
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
		public void onClick(View v) {
			launchHomePage();
		}
	};

	/**
	 * Set singleton name value to edittext
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void setOldValue() {
		if (editTextTempIdNumber != null
				&& SingletonVisitorProfile.getInstance() != null
				&& SingletonVisitorProfile.getInstance().getTempIDNumber()>0) {
			editTextTempIdNumber
					.removeTextChangedListener(textWatcherTempIDChanged);
			editTextTempIdNumber.setText(MainConstants.kConstantEmpty+SingletonVisitorProfile.getInstance()
					.getTempIDNumber());
			editTextTempIdNumber.setSelection((MainConstants.kConstantEmpty+SingletonVisitorProfile
					.getInstance().getTempIDNumber()).length());
			editTextTempIdNumber.addTextChangedListener(textWatcherTempIDChanged);
			if (imageviewNext != null) {
				imageviewNext.setVisibility(View.VISIBLE);
			}
			if (image_view_next != null) {
				image_view_next.setVisibility(View.VISIBLE);
			}
		} else {
			if (imageviewNext != null) {
				imageviewNext.setVisibility(View.INVISIBLE);
			}
			if (image_view_next != null) {
				image_view_next.setVisibility(View.INVISIBLE);
			}
		}
	}

	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void textSpeach(int textSpeachCode) {

		if (textToSpeech != null) {
			textToSpeech.stop();
			if (textSpeachCode == MainConstants.kConstantOne
					&& (editTextTempIdNumber != null
							&& editTextTempIdNumber.getText() != null && editTextTempIdNumber
							.getText().toString().length() <= 0)) {
				textToSpeech.speak(TextToSpeachConstants.speachTempId,
						TextToSpeech.QUEUE_FLUSH, null, null);
			} else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
				textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
						TextToSpeech.QUEUE_FLUSH, null, null);
			}
		}
	}

	/**
	 * Deallocate Objects in the activity
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void deallocateObjects() {
		editTextTempIdNumber = null;
		imageviewBack = null;
		imageviewHome = null;
		imageviewNext = null;
		image_view_next =null;
		textviewVisitorTempIDTitle = null;
		typeFaceTitle = null;
		typefaceVisitorTempID = null;
		relativeLayoutExisitingVisitor
				.setBackgroundResource(MainConstants.kConstantZero);
		relativeLayoutExisitingVisitor = null;
		if (noActionHandler != null) {
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler = null;
		}
		if(backgroundProcess!=null) {
			backgroundProcess.cancel(true);
		}
		if(textToSpeech!=null)
		{
			textToSpeech.stop();
		}
	}
}
