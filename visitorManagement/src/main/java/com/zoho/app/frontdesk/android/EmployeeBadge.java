package com.zoho.app.frontdesk.android;

import appschema.SingletonEmployeeProfile;
import utility.TimeZoneConventer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import constants.FontConstants;
import constants.MainConstants;

public class EmployeeBadge extends LinearLayout {
		
		Context newContext;
	    Boolean color;
	    String title;
	    LayoutInflater inflater;
	    RelativeLayout layout;
	    ImageView imageViewEmployeeImage;
	   // ImageView imageViewQrCode;
	    TextView  textViewGetEmployeeFirstName,textViewGetEmployeeLastName,textViewEmployeeId,textViewGetDate;//,textViewGetDate,textViewGetDateSubTitle;


	     /**
	     * Created by jayapriya On 14/11/2014
	     * Created the constructor to get the details into the singleton variable.
	     * @param context
	     * @param attrs
	     */
		public EmployeeBadge(Context context, AttributeSet attrs) {
			super(context, attrs);
			this.newContext = context;
			LayoutInflater.from(context).inflate(R.layout.employee_badge, this);
			imageViewEmployeeImage = (ImageView) findViewById(R.id.imageViewEmployeeBadgeImage);
	        //imageViewQrCode =(ImageView) findViewById(R.id.imageViewEmployeeQrcode);
			textViewGetEmployeeFirstName = (TextView)findViewById(R.id.textViewEmployeeFirstName);
			textViewGetEmployeeLastName=(TextView) findViewById(R.id.textViewEmployeeLastName);
			textViewGetDate = (TextView) findViewById(R.id.textViewEmployeeBadgeGenerationDate);
	        textViewEmployeeId=(TextView) findViewById(R.id.textViewEmployeeId);
    
	        //set font
	       Typeface typefaceSetDate = Typeface.createFromAsset(newContext.getAssets(),
	            FontConstants.fontConstantSourceSansProbold);
	       //textViewGetDate.setTypeface(typefaceSetDate);
	       //textViewGetDateSubTitle.setTypeface(typefaceSetDate);
	       Typeface typefaceSetName = Typeface.createFromAsset(context.getAssets(),
	               FontConstants.fontConstantSourceSansProbold);
	       textViewGetEmployeeFirstName.setTypeface(typefaceSetName);
	       textViewGetEmployeeFirstName.setAllCaps(true);
	       textViewGetEmployeeLastName.setAllCaps(true);
	       textViewGetEmployeeLastName.setTypeface(typefaceSetName);
	       textViewEmployeeId.setTypeface(typefaceSetName);
	       textViewGetDate.setTypeface(typefaceSetDate);
	       textViewGetDate.setText(TimeZoneConventer.getCurrentDateAndTime());
	       
	       //setView();
	      // Log.d("Android", "name:"+TimeZoneConventer.getCurrentDateAndTime());
	    	
	       	}


	    /*
	     * set view informations like Employee image, qrcode and check in date.
	     * @author jayapriya
	     * @created 14/11/2014
	     */
	    private void setView(){
	        if ((SingletonEmployeeProfile.getInstance().getEmployeePhoto() != null)
	                && (SingletonEmployeeProfile.getInstance().getEmployeePhoto() != MainConstants.kConstantEmpty)) {

	          /*  Bitmap bitmap;
	            BitmapFactory.Options options = new BitmapFactory.Options();
	            bitmap = BitmapFactory.decodeFile(SingletonEmployeeProfile.getInstance().getEmployeeImage(), options);
	            if (bitmap != null) {

	                Bitmap resized = Bitmap.createScaledBitmap(bitmap,
	                        300,
	                        300, true);
	                // //Log.d("bitap","bitmap" +bitmap);
	                // set circle bitmap
	                Bitmap resizedBitmap = utility.UsedBitmap
	                        .getRoundedRectBitmap(resized);
	                // //Log.d("bitap","bitmap" +resizedBitmap);
	                imageViewEmployeeImage.setImageBitmap(resizedBitmap);
	                // resizedBitmap.recycle();
	                // resized.recycle();

	            }*/
	            //imageViewEmployeeImage.setImageURI(Uri.parse(SingletonEmployeeProfile.getInstance().getEmployeeImage()));
	            setEmployeeImage(SingletonEmployeeProfile.getInstance().getEmployeePhoto());
	        }

	        //if((SingletonEmployeeProfile.getInstance().getQrcode()!=null)&&(SingletonEmployeeProfile.getInstance().getQrcode() != MainConstants.kConstantEmpty)){

	        //	Log.d("window","bitmap" +SingletonEmployeeProfile.getInstance().getQrcode());
	           // imageViewQrCode.setImageURI(Uri.parse(SingletonEmployeeProfile.getInstance().getQrcode()));
	        //}

	        // imageViewEmployeeImage.setImageURI(Uri.parse(SingletonEmployeeProfile.getInstance().getEmployeeImage()));
	        //textViewGetDate.setText(SingletonEmployeeProfile.getInstance().getDateOfVisitLocal());
	        //textViewGetEmployeeFirstName.setText(SingletonEmployeeProfile.getInstance().getEmployeeName());

	    }
	    
	    private void setEmployeeImage(String path) {
	        Bitmap bitmap;
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        bitmap = BitmapFactory.decodeFile(path, options);
	        if (bitmap != null) {

	           // Bitmap resized = Bitmap.createScaledBitmap(bitmap, MainConstants.visitorImageWidth,MainConstants.visitorImageHeight, true);
	           // Log.d("bitmap","size"+bitmap.getHeight());
	            imageViewEmployeeImage.setImageBitmap(bitmap);
	            //resized.recycle();
	            //resized=null;
	        }
	        else
	        {
	        	Bitmap myBitmap = BitmapFactory.decodeResource(newContext.getResources(),
                         R.drawable.photo);
	        	imageViewEmployeeImage.setImageBitmap(myBitmap);
	        }
	        //bitmap.recycle();
	        //bitmap=null;  
	    }
	    public void setEmployeeDetails() { 
	    	String lastName="";
	    	//Log.d("Android", "name:"+SingletonEmployeeProfile.getInstance().getEmployeeName());
	    	if((SingletonEmployeeProfile.getInstance().getEmployeeName()!=null)&&(SingletonEmployeeProfile.getInstance().getEmployeeName() != MainConstants.kConstantEmpty)){
	    	
	    		String name[] =SingletonEmployeeProfile.getInstance().getEmployeeName().split(" ");
	    		//Log.d("firstName","" +name[0]+"last Name "+name[1]);
	    		if((name[1]!=null)&&(name[1].length()>1)&&(!name[1].contains("."))){
	    			//Log.d("replae","name length"+name.length);
	    			textViewGetEmployeeFirstName.setText(name[0]);
	    			for(int i=1;i<name.length;i++){
	    				//Log.d("for","name"+i +"last name "+name[i]);
	    				lastName=lastName+" "+name[i];
	    				//Log.d("for","name"+i +"last name "+lastName);
	    			}
	    			//Log.d("for","last name "+lastName);
	    			//if(lastName!="")
	    			textViewGetEmployeeLastName.setVisibility(View.VISIBLE);
	    			textViewGetEmployeeLastName.setText(lastName.trim());
	    				//	SingletonEmployeeProfile.getInstance().getEmployeeName().replace(name[0]+" ", name[0]+"\n"));
	    		//SingletonEmployeeProfile.getInstance().setEmployeeName(SingletonEmployeeProfile.getInstance().getEmployeeName().replace(name[0]+" ", name[0]+"\n"));
	    		}
	    		else
	    		{
	    		textViewGetEmployeeFirstName.setText(SingletonEmployeeProfile.getInstance().getEmployeeName());
	    		textViewGetEmployeeLastName.setVisibility(View.GONE);
	    		}
	    	}
	    	if((SingletonEmployeeProfile.getInstance().getEmployeeId()!=null)&&(SingletonEmployeeProfile.getInstance().getEmployeeId() != MainConstants.kConstantEmpty)){
	    		if(SingletonEmployeeProfile.getInstance().getEmployeeId().length()>4){
	    			textViewEmployeeId.setTextSize(22);
	    		}
	    		else
	    		{
	    			textViewEmployeeId.setTextSize(30);
	    		}
	    		textViewEmployeeId.setText(SingletonEmployeeProfile.getInstance().getEmployeeId());
	    	}
	    	
	    	  if ((SingletonEmployeeProfile.getInstance().getEmployeePhoto() != null)
		                && (SingletonEmployeeProfile.getInstance().getEmployeePhoto() != MainConstants.kConstantEmpty)) {
	    	       setEmployeeImage(SingletonEmployeeProfile.getInstance().getEmployeePhoto());
	    		      
	    	  }
	    	  else
	    	  {
	    		  Bitmap myBitmap = BitmapFactory.decodeResource(newContext.getResources(),
                           R.drawable.photo);
	    		  imageViewEmployeeImage.setImageBitmap(myBitmap);
	    	  }
	    		       
	    }
	    
}
