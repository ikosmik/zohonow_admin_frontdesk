package com.zoho.app.frontdesk.android;

import java.io.File;
import java.util.ArrayList;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonVisitorProfile;
import constants.StringConstants;
import database.dbhelper.DbHelper;
import database.dbschema.PurposeOfVisitRecord;
import database.dbschema.VisitRecord;
import database.dbschema.VisitorRecord;
import database.dbschema.ZohoEmployeeRecord;
import utility.UsedBitmap;
import utility.Validation;
import zohocreator.VisitorZohoCreator;
import constants.FontConstants;
import constants.MainConstants;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

import static com.zoho.app.frontdesk.android.R.*;

public class VisitActivity extends Activity {

	private String visitorSign;
	private String sourceFile;
	private ImageView imageViewVisitorSignClear, imageViewGet, imageViewBack,
			imageViewErrorInVisitorSign;
	private RelativeLayout relativeLayoutGetVisitorSign,
			relativeLayoutVisitPage;
	private AutoCompleteTextView textGetVisitPurpose;
	private CustomAutoCompleteView textGetEmployeeName;
	private SingleTouchView singleTouchViewGetVisitorSign;
	private Button buttonOkay;
	private String[] arrayOfVisitPurpose;
	private Long currentTime = System.currentTimeMillis();
	private Typeface typeFaceTitle, typefaceButtonTitle, typeFaceSetVisit;
	private VisitorZohoCreator creator = new VisitorZohoCreator(this);
	// private NetworkInfo mWifi;
	private Context context;
	// private ConnectivityManager connManager;
	private EditText editTextTempIdNumber;
	private TextView textViewTempId, textEmployeeErrorMsg,
			textViewEmployeeName, textViewVisitPurpose,
			textViewVisitorSignature;
	private DbHelper database;
	private ArrayList<ZohoEmployeeRecord> employeeList;
	private ArrayList<PurposeOfVisitRecord> visitList;
	AutocompleteCustomArrayAdapter myAdapter;
	ArrayAdapter adapterGetVisitPurpose;
	ZohoEmployeeRecord employee;
	UsedBitmap usedBitmap = new UsedBitmap();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		// Remove the title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// hide the status bar.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// Set the orientation as Portrait
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
		// this.overridePendingTransition(R.anim.animation_enter,
		// R.anim.animation_leave);
		setContentView(R.layout.activity_visit);
	}

	/**
	 * Created by jayapriya On 25/02/2015 This function is called when the
	 * activity is started. (non-Javadoc)
	 * 
	 * @see android.app.Activity#onStart() call Fuction AssignObjects.
	 */
	public void onStart() {

		super.onStart();
		// Log.d("window", "onStart");
		assignObjects();
		getSingletonValues();

	}

	/*
	 * get the singleton values.
	 * 
	 * @author jayapriya
	 * 
	 * @created 16/03/2015
	 */
	private void getSingletonValues() {

		if ((SingletonEmployeeProfile.getInstance() != null)
				&& (SingletonEmployeeProfile.getInstance().getEmployeeName() != null)
				&& (SingletonEmployeeProfile.getInstance().getEmployeeName() != MainConstants.kConstantEmpty)) {
			textGetEmployeeName.setText(SingletonEmployeeProfile.getInstance()
					.getEmployeeName().toUpperCase());
			textGetEmployeeName.setSelection(textGetEmployeeName.getText()
					.toString().trim().length());
		}

		if (SingletonVisitorProfile.getInstance().getPurposeOfVisit() != MainConstants.kConstantEmpty) {
			textGetVisitPurpose.setText(SingletonVisitorProfile.getInstance()
					.getPurposeOfVisit().toUpperCase());
			textGetVisitPurpose.setSelection(textGetVisitPurpose.getText()
					.toString().trim().length());
		}
		if (SingletonVisitorProfile.getInstance().getTempIDNumber() > 0) {
			editTextTempIdNumber.setText(""
					+ SingletonVisitorProfile.getInstance().getTempIDNumber());
			editTextTempIdNumber.setSelection(editTextTempIdNumber.getText()
					.toString().trim().length());
		}
	}

	/**
	 * Created by Jayapriya On 30/10/2014 When the activity stopped then call
	 * deallocateObjects.
	 */
	public void onStop() {
		super.onStop();
		deallocateObjects();
	}

	/*
	 * Assign the values to the variable
	 * 
	 * @author jayapriya
	 * 
	 * @created 27/09/2014
	 */
	private void assignObjects() {

		relativeLayoutGetVisitorSign = (RelativeLayout) findViewById(id.relativeLayoutGetVisitorSign);
		imageViewVisitorSignClear = (ImageView) findViewById(id.imageViewSignClear);
		imageViewBack = (ImageView) findViewById(id.imageViewVisitPageBack);
		imageViewErrorInVisitorSign = (ImageView) findViewById(id.image_view_error_in_visitor_sign);
		buttonOkay = (Button) findViewById(id.button_visit_detail_ok);
		singleTouchViewGetVisitorSign = (SingleTouchView) findViewById(id.singletouchview_sign);
		textGetEmployeeName = (CustomAutoCompleteView) findViewById(id.textGetEmployeeName);
		textGetVisitPurpose = (AutoCompleteTextView) findViewById(id.textGetVisitPurpose);
		textEmployeeErrorMsg = (TextView) findViewById(id.textViewEmployeeDetailErrorMsg);
		textViewTempId = (TextView) findViewById(id.textView_temp_id);
		textViewEmployeeName = (TextView) findViewById(id.textViewEmployeeDetail);
		textViewVisitorSignature = (TextView) findViewById(id.textViewSignature);
		textViewVisitPurpose = (TextView) findViewById(id.textViewChooseVisitPurpose);
		relativeLayoutVisitPage = (RelativeLayout) findViewById(R.id.relativeLayoutVisitPage);
		editTextTempIdNumber = (EditText) findViewById(id.temp_id_number);
		context = this;
		database = new DbHelper(this);

		// set the Font
		typeFaceTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantJosefinSans);
		// typeFaceSetVisit = Typeface.createFromAsset(getAssets(),
		// FontConstants.fontConstantJosefinSemiBold);
		typeFaceSetVisit = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantProximaNovaReg);
		textGetEmployeeName.setTypeface(typeFaceSetVisit);
		textGetVisitPurpose.setTypeface(typeFaceSetVisit);
		textViewVisitPurpose.setTypeface(typeFaceTitle);
		textViewEmployeeName.setTypeface(typeFaceTitle);
		editTextTempIdNumber.setTypeface(typeFaceSetVisit);
		textViewTempId.setTypeface(typeFaceTitle);
		textViewVisitorSignature.setTypeface(typeFaceTitle);
		typefaceButtonTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantSourceSansProSemibold);
		buttonOkay.setTypeface(typefaceButtonTitle);
		// relativeLayoutVisitPage.setBackgroundResource(drawable.background_existing_visitor);

		// set the input type
		textGetEmployeeName
				.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
						| InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

		textGetVisitPurpose
				.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
						| InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

		editTextTempIdNumber
				.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
						| InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		// ObjectItemData has no value at first
		ZohoEmployeeRecord[] employeeArray = new ZohoEmployeeRecord[0];

		// set the custom ArrayAdapter
		myAdapter = new AutocompleteCustomArrayAdapter(context,
				layout.custom_spinner, employeeArray);
		textGetEmployeeName.setAdapter(myAdapter);

		// TextChangedListener
		// add the listener so it will tries to suggest while the user types
		textGetEmployeeName
				.addTextChangedListener(textWatcherEmployeeNameChanged);
		textGetVisitPurpose.addTextChangedListener(textWatcherPurposeChanged);
		editTextTempIdNumber.addTextChangedListener(textWatcherTempIDChanged);
		// OnClickListener
		// textGetEmployeeName.setOnClickListener(listenerGetEmployee);

		imageViewBack.setOnClickListener(listenerTappedBack);
		imageViewVisitorSignClear.setOnClickListener(listenerVisitorSignClear);
		buttonOkay.setOnClickListener(listenerClickAgree);
		editTextTempIdNumber.setOnEditorActionListener(listenerDonePressed);

		// OnItemSelected Listener
		textGetEmployeeName.setOnItemClickListener(listenerChooseEmployee);
		textGetVisitPurpose.setOnItemSelectedListener(listenerChoosePurpose);
		singleTouchViewGetVisitorSign
				.setOnTouchListener(errorInVisitorSignListner);
		textGetVisitPurpose.setOnClickListener(listenerGetPurpose);
		textGetVisitPurpose.setOnTouchListener(purposeTouchListner);
		editTextTempIdNumber.setOnTouchListener(tempIDNumberTouchListner);
		textGetEmployeeName
				.setOnFocusChangeListener(listenerSetFocusOnEmployee);

		// connManager = (ConnectivityManager)
		// getSystemService(this.CONNECTIVITY_SERVICE);
		// mWifi = connManager
		// .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}

	private View.OnFocusChangeListener listenerSetFocusOnEmployee = new View.OnFocusChangeListener() {

		public void onFocusChange(View v, boolean hasFocus) {
			if (textGetEmployeeName.hasFocus() == false) {

				if ((textGetEmployeeName != null)
						&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
						&& (textGetEmployeeName.getText().toString().trim()
								.length() > MainConstants.kConstantZero)
						&& ((employee == null)
								|| (employee.getEmployeeName() == null) || (employee
								.getEmployeeName() == MainConstants.kConstantEmpty))) {

					textEmployeeErrorMsg.setVisibility(View.VISIBLE);
				} else {
					textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
				}
			}

		}
	};

	/*
	 * listener get employee details
	 * 
	 * @author jayapriya
	 * 
	 * @created 11/03/2014
	 */
	private OnClickListener listenerGetEmployee = new OnClickListener() {

		public void onClick(View v) {

			textGetEmployeeName
					.addTextChangedListener(textWatcherEmployeeNameChanged);
		}
	};

	private OnTouchListener purposeTouchListner = new OnTouchListener() {

		public boolean onTouch(View v, MotionEvent event) {
			textGetVisitPurpose.requestFocus();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(textGetVisitPurpose,
					InputMethodManager.SHOW_IMPLICIT);

			return false;

		}

	};

	/*
	 * TouchListner for focus and open keyboard
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	private OnTouchListener tempIDNumberTouchListner = new OnTouchListener() {

		public boolean onTouch(View v, MotionEvent event) {
			editTextTempIdNumber.requestFocus();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(editTextTempIdNumber,
					InputMethodManager.SHOW_IMPLICIT);

			return false;

		}

	};

	/*
	 * listener get employee details
	 * 
	 * @author jayapriya
	 * 
	 * @created 11/03/2014
	 */
	private OnClickListener listenerGetPurpose = new OnClickListener() {

		public void onClick(View v) {
			// textGetVisitPurpose.
			textGetVisitPurpose
					.addTextChangedListener(textWatcherPurposeChanged);

		}
	};

	/*
	 * Clear the VisitorSign path.
	 * 
	 * @author jayapriya
	 * 
	 * @created 28/09/2014
	 */
	private OnClickListener listenerVisitorSignClear = new OnClickListener() {

		public void onClick(View v) {

			removeView(relativeLayoutGetVisitorSign,
					singleTouchViewGetVisitorSign);

		}
	};

	/*
	 * listener to tapped the back button
	 * 
	 * @author jayapriya
	 * 
	 * @created 27/02/2015
	 */
	private OnClickListener listenerTappedBack = new OnClickListener() {

		public void onClick(View v) {

			storePurposeRecord();
			storeTempIDNumber();
			launchVisitorPage();

		}
	};

	/*
	 * textWatcher to validate the employeeName
	 * 
	 * @author jayapriya
	 * 
	 * @created 28/02/2015
	 */
	private TextWatcher textWatcherEmployeeNameChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			employee = null;
			if ((textGetEmployeeName != null)
					&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
					&& (textGetEmployeeName.getText().toString().trim()
							.length() >= MainConstants.kConstantThree)) {

				// Log.d("ontext1","changed");
				textEmployeeErrorMsg.setVisibility(View.INVISIBLE);

				VisitActivity activity = ((VisitActivity) context);

				// get suggestions from the database
				employeeList = database
						.getEmployeeListWithName(textGetEmployeeName.getText()
								.toString().trim().replaceAll("'", "''"));

				if ((employeeList != null)
						&& (employeeList.size() > MainConstants.kConstantZero)) {

					ZohoEmployeeRecord[] employeeArray = employeeList
							.toArray(new ZohoEmployeeRecord[employeeList.size()]);
					// Log.d("emp","" +employeeArray.length);
					// Log.d("emp","employeeArray"+employeeArray.toString());

					if ((employeeArray != null)
							&& (employeeArray.length > MainConstants.kConstantZero)) {
						// update the adapter
						activity.myAdapter = new AutocompleteCustomArrayAdapter(
								activity, layout.custom_spinner, employeeArray);

						activity.textGetEmployeeName
								.setAdapter(activity.myAdapter);
						// update the adapater
						activity.myAdapter.notifyDataSetChanged();
					}

				} else {

					activity.myAdapter.clear();
					activity.myAdapter.notifyDataSetChanged();
					textEmployeeErrorMsg.setVisibility(View.VISIBLE);

				}
			} else {
				if ((SingletonEmployeeProfile.getInstance() != null)
						|| (employee == null)) {
					SingletonEmployeeProfile.getInstance().clearInstance();
				}
				myAdapter.clear();
				myAdapter.notifyDataSetChanged();
				textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
			}

		}

		public void afterTextChanged(Editable s) {
			// Log.d("after", "text");

		}
	};

	/*
	 * textWatcher to validate the VisitPurpose
	 * 
	 * @author jayapriya
	 * 
	 * @created 28/02/2015
	 */
	public TextWatcher textWatcherPurposeChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// Log.d("onText","changed:");

			if ((textGetVisitPurpose != null)
					&& (textGetVisitPurpose.getText().toString().trim() != MainConstants.kConstantEmpty)) {

				// Log.d("onText", "changed1");

				textGetVisitPurpose.setBackgroundDrawable(getResources()
						.getDrawable(drawable.cell_scanning_detail));
				visitList = database.getVisitPurposeList(textGetVisitPurpose
						.getText().toString().trim().replaceAll("'", "''"));

				if ((visitList != null)) {// &&(visitList.size()>
											// MainConstants.kConstantZero)
					// Log.d("vistlist","visit" +visitList.size());
					arrayOfVisitPurpose = new String[visitList.size()];

					for (int i = 0; i < visitList.size(); i++) {
						arrayOfVisitPurpose[i] = visitList.get(i).getPurpose()
								.toUpperCase();
					}
					// Log.d("vistlist","visit" +arrayOfVisitPurpose.length);
					adapterGetVisitPurpose = new ArrayAdapter<String>(context,
							layout.spinner_item, arrayOfVisitPurpose) {

						public View getView(int position, View convertView,
								ViewGroup parent) {
							View v = super.getView(position, convertView,
									parent);
							// Log.d("vistlist","visit"
							// +arrayOfVisitPurpose.length);
							TextView suggestion = (TextView) v
									.findViewById(R.id.text_view_purpose);
							suggestion.setTypeface(typeFaceSetVisit);
							return v;
						}
					};

					TextView textViewPurpose = (TextView) findViewById(R.id.text_view_purpose);
					// textViewPurpose.setTypeface(typeFaceSetVisit);
					// update the adapater
					// adapterGetVisitPurpose.notifyDataSetChanged();

					textGetVisitPurpose.setAdapter(adapterGetVisitPurpose);
					textGetVisitPurpose.setThreshold(0);
					adapterGetVisitPurpose.notifyDataSetChanged();

					// Log.d("vistlist","visit" +arrayOfVisitPurpose.length);
				}
			}

			SingletonVisitorProfile.getInstance().setPurposeOfVisit(
					MainConstants.kConstantEmpty);

		}

		public void afterTextChanged(Editable s) {

		}
	};

	/*
	 * textWatcher to validate the tempID
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	public TextWatcher textWatcherTempIDChanged = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editTextTempIdNumber != null) {
				SingletonVisitorProfile.getInstance().setTempIDNumber(0);
				// Log.d("onText", "changed1");

				editTextTempIdNumber.setBackgroundDrawable(getResources()
						.getDrawable(drawable.cell_scanning_detail));
			}
		}

		public void afterTextChanged(Editable s) {

		}
	};
	// OnClickListener
	/*
	 * Choose the purpose of visit
	 * 
	 * @author jayapriya
	 * 
	 * @created 25/02/2015
	 */
	private OnItemSelectedListener listenerChoosePurpose = new OnItemSelectedListener() {

		public void onItemSelected(AdapterView<?> adapter, View v,
				int position, long id) {

			textGetVisitPurpose
					.removeTextChangedListener(textWatcherPurposeChanged);
			textGetVisitPurpose.setText(adapter.getItemAtPosition(position)
					.toString());
			textGetVisitPurpose.setSelection(textGetVisitPurpose.getText()
					.length());
			textGetVisitPurpose
					.addTextChangedListener(textWatcherPurposeChanged);

			// }
		}

		public void onNothingSelected(AdapterView<?> adapter) {
			// textGetPurposeOfVisit
			// .setText(MainConstants.kConstantTextVisitorPurpose);
			// Log.d("nothingSelected", "nothing selected");

		}
	};

	/*
	 * Choose the purpose of visit
	 * 
	 * @author jayapriya
	 * 
	 * @created 27/02/2015
	 */
	private AdapterView.OnItemClickListener listenerChooseEmployee = new AdapterView.OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			employee = (ZohoEmployeeRecord) parent.getAdapter().getItem(
					position);
			String name = ((ZohoEmployeeRecord) parent.getAdapter().getItem(
					position)).getEmployeeName();
			// Log.d("parent","" + parent.getAdapter().getItemId(position));
			// Log.d("employee","" +employee.employeeName);
			textGetEmployeeName
					.removeTextChangedListener(textWatcherEmployeeNameChanged);
			textGetEmployeeName.setText(name.toUpperCase());
			textGetEmployeeName.setSelection(textGetEmployeeName.getText()
					.length());
			textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
			textGetEmployeeName
					.addTextChangedListener(textWatcherEmployeeNameChanged);
			textGetVisitPurpose.requestFocus();
		}
	};

	/*
	 * Click the agree button to get the signature of Visitor and Security and
	 * convert the signature into the bitmap. The security Signature and visitor
	 * Signature path will be stored into the singleton Variable. And store the
	 * visitor detail, visitor Purpose and visit details into the creator.
	 * 
	 * @author jayapriya
	 * 
	 * @created 29/09/2014
	 */
	private OnClickListener listenerClickAgree = new OnClickListener() {

		public void onClick(View arg0) {

			validDetails();

		}

	};

	private OnTouchListener errorInVisitorSignListner = new OnTouchListener() {

		public boolean onTouch(View v, MotionEvent event) {
			imageViewErrorInVisitorSign.setVisibility(View.INVISIBLE);
			return false;
		}

	};

	/*
	 * @author jayapriya
	 */
	private void validDetails() {
		// Log.d("window","validDetails");
		storeEmployeeIntoSingleton(employee);
		storePurposeRecord();
		storeTempIDNumber();

		if ((!singleTouchViewGetVisitorSign.isPathNotEmpty()
				&& (SingletonVisitorProfile.getInstance() != null)
				&& (SingletonVisitorProfile.getInstance().getPurposeOfVisit() != null)
				&& (SingletonVisitorProfile.getInstance().getPurposeOfVisit() != MainConstants.kConstantEmpty) && (SingletonVisitorProfile
				.getInstance().getPurposeOfVisit().length() > MainConstants.kConstantZero))
				&& ((SingletonVisitorProfile.getInstance().getTempIDNumber() > 0))) {

			// Log.d("window","purpose"
			// +SingletonVisitorProfile.getInstance().getPurposeOfVisit());
			if ((textGetEmployeeName.getText() != null)
					&& (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
					&& (textGetEmployeeName.getText().toString().trim()
							.length() > MainConstants.kConstantZero)
					&& ((employee == null)
							|| (employee.getEmployeeName() == null) || (employee
							.getEmployeeName() == MainConstants.kConstantEmpty))) {
				utility.UsedCustomToast.makeCustomToast(
						getApplicationContext(),
						StringConstants.kConstantErrorMsgInValidEmployeeDetail,
						Toast.LENGTH_LONG).show();
				utility.UsedCustomToast.makeCustomToast(
						getApplicationContext(),
						StringConstants.kConstantErrorMsgInValidEmployeeDetail,
						Toast.LENGTH_LONG).show();
			}
			GetSign();

		} else {
			utility.UsedDialogBox.intializeDialogBox(getApplicationContext(),
					StringConstants.kConstantErrorMsgToValidDetail);
			if ((SingletonVisitorProfile.getInstance().getPurposeOfVisit() == null)
					|| (SingletonVisitorProfile.getInstance()
							.getPurposeOfVisit() == MainConstants.kConstantEmpty)) {

				textGetVisitPurpose.setBackgroundDrawable(getResources()
						.getDrawable(drawable.cell_error_border));
				// Log.d("get2","sign");

			}
			if (SingletonVisitorProfile.getInstance().getTempIDNumber() <= 0) {
				editTextTempIdNumber.setBackgroundDrawable(getResources()
						.getDrawable(drawable.cell_error_border));
			}
			if (singleTouchViewGetVisitorSign.isPathNotEmpty()) {

				imageViewErrorInVisitorSign.setVisibility(View.VISIBLE);
				// Log.d("get3","sign");
			}
		}
	}

	/*
	 * store employeedetails into the singleton variables
	 */
	private void storeEmployeeIntoSingleton(ZohoEmployeeRecord employee) {

		Validation validation =new Validation();
		// Log.d("employee","employee" +employee);
		if ((employee != null)
				&& (employee.getEmployeeName() != MainConstants.kConstantEmpty)
				&& (employee.getEmployeeName().length() > MainConstants.kConstantZero)) {

			// Log.d("employee","employee" +employee.getEmployeePhoto());
			// Log.d("employee","employee" +employee.getEmployeeName());
			// Log.d("employee","employee" +employee.getEmployeeMobileNumber());
			// Log.d("employee","employee" +employee.getEmployeeEmailId());
			// Log.d("employee","employee" +employee.getEmployeeZuid());

			SingletonEmployeeProfile.getInstance().setEmployeeName(
					employee.getEmployeeName());
			SingletonEmployeeProfile.getInstance().setEmployeeExtentionNumber(
					employee.getEmployeeExtentionNumber());
			SingletonEmployeeProfile.getInstance().setEmployeeId(
					employee.getEmployeeId());
			SingletonEmployeeProfile.getInstance().setEmployeeMailId(
					employee.getEmployeeEmailId());

			if (((SingletonVisitorProfile.getInstance().getLocationCode() != null))
					// && (SingletonVisitorProfile.getInstance()
					// .getLocationCode() != MainConstants.kConstantMinusOne)
					&& ((SingletonVisitorProfile.getInstance()
							.getLocationCode() == MainConstants.kConstantMinusOne)
							|| (SingletonVisitorProfile.getInstance()
									.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
							|| (SingletonVisitorProfile.getInstance()
									.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) || (SingletonVisitorProfile
							.getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
				validation.SetMobileNumberWithLocation(employee
						.getEmployeeMobileNumber());
				// Log.d("phone","no"
				// +SingletonEmployeeProfile.getInstance().getEmployeeMobileNo());

			} else {
				SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
						employee.getEmployeeMobileNumber());
				// Log.d("phone","else no"
				// +SingletonEmployeeProfile.getInstance().getEmployeeMobileNo());

			}
			SingletonEmployeeProfile.getInstance().setEmployeeZuid(
					employee.getEmployeeZuid());
			SingletonEmployeeProfile.getInstance().setEmployeeSeatingLocation(
					employee.getEmployeeSeatingLocation());

			// Log.d("getEmpdetails",""
			// +SingletonEmployeeProfile.getInstance().getEmployeeName() +
			// SingletonEmployeeProfile.getInstance().getEmployeeExtentionNumber()
			// +SingletonEmployeeProfile.getInstance().getEmployeeId()
			// +SingletonEmployeeProfile.getInstance().getEmployeeZuid());

		}
	}

	/*
	 * Created by Jayapriya On 29/09/2014
	 * 
	 * Get the visitor signature and security signature, convert signature into
	 * bitmap. Store bitmap path into the singleton variable. And upload details
	 * into the creator
	 */
	private void GetSign() {

		visitorSign = createBitmap(singleTouchViewGetVisitorSign, currentTime
				+ StringConstants.kConstantVisitorSign);
		// Log.d("window","GetSign");

		if ((visitorSign != null)
				&& (visitorSign.length() > MainConstants.kConstantZero)) {
			
			//for byte sign
			//SingletonVisitorProfile.getInstance().setVisitorSign(visitorSign);

		}

		if ((SingletonVisitorProfile.getInstance().getVisitorSign() != null)
				&& (SingletonVisitorProfile.getInstance().getVisitorSign().length>0)
				&& (SingletonVisitorProfile.getInstance().getContact() != null)
				&& (SingletonVisitorProfile.getInstance().getContact().length() > MainConstants.kConstantZero)
				&& (SingletonVisitorProfile.getInstance().getVisitorName() != null)
				&& (SingletonVisitorProfile.getInstance().getVisitorName() != MainConstants.kConstantEmpty)
				&& (SingletonVisitorProfile.getInstance().getVisitorName()
						.length() > MainConstants.kConstantZero)) {

			if ((SingletonVisitorProfile.getInstance().getContact() != null)
					&& (SingletonVisitorProfile.getInstance().getContact() != MainConstants.kConstantEmpty)
					&& (SingletonVisitorProfile.getInstance().toString()
							.length() > MainConstants.kConstantZero)) {
				String contact = SingletonVisitorProfile
						.getInstance()
						.getContact()
						.replace(MainConstants.kConstantWhiteSpace,
								MainConstants.kConstantEmpty);
				if (contact.matches(MainConstants.kConstantValidPhoneNo) == true) {

					if (((SingletonVisitorProfile.getInstance()
							.getLocationCode() != null))
					// && (SingletonVisitorProfile.getInstance()
					// .getLocationCode() != MainConstants.kConstantMinusOne)
							&& ((SingletonVisitorProfile.getInstance()
									.getLocationCode() == MainConstants.kConstantMinusOne)
									|| (SingletonVisitorProfile.getInstance()
											.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
									|| (SingletonVisitorProfile.getInstance()
											.getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) || (SingletonVisitorProfile
									.getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
						// Log.d("phone", "phone" +
						// SingletonVisitorProfile.getInstance().getPhoneNo());
						Validation validation =new Validation();
						validation
								.SetVisitorMobileNumberWithLocation(SingletonVisitorProfile
										.getInstance().getContact());
						// Log.d("phone", "phone" +
						// SingletonVisitorProfile.getInstance().getPhoneNo());
					} else {
						SingletonVisitorProfile.getInstance().setPhoneNo(
								SingletonVisitorProfile.getInstance()
										.getContact());
					}
					// Log.d("contact",""
					// +SingletonVisitorProfile.getInstance().getPhoneNo());

				} else {
					SingletonVisitorProfile.getInstance().setEmail(
							SingletonVisitorProfile.getInstance().getContact());
					// Log.d("email",""
					// +SingletonVisitorProfile.getInstance().getEmail());

				}
			}

			// Log.d("contact",""
			// +SingletonVisitorProfile.getInstance().getContact());
			// Log.d("phone",""
			// +SingletonVisitorProfile.getInstance().getPhoneNo());
			// Log.d("email",""
			// +SingletonVisitorProfile.getInstance().getEmail());

			if (SingletonVisitorProfile.getInstance().getVisitorBusinessCard() != null
					&& SingletonVisitorProfile.getInstance()
							.getVisitorBusinessCard().trim().length() > 0) {
				// Log.d("window", "business");
				usedBitmap.storeImage(SingletonVisitorProfile
						.getInstance().getVisitorBusinessCard());
			}
			// insertRecordIntoLocalDb();
			if (utility.TimeZoneConventer.checkConnection(context) == true) {
				// Start the service
				// Log.d("wifi","on");

				// startService(new Intent(this, UploadRecord.class));

			} else {
				utility.UsedCustomToast.makeCustomToast(
						getApplicationContext(),
						StringConstants.kConstantErrorMsgInNetworkError,
						Toast.LENGTH_LONG).show();
				utility.UsedCustomToast.makeCustomToast(
						getApplicationContext(),
						StringConstants.kConstantErrorMsgInNetworkError,
						Toast.LENGTH_LONG).show();
				// utility.UsedDialogBox.intializeDialogBox(MainConstants.kContext,"Oops!\nLooks like the internet\n connectivity is not there.");

			}

			// Execute the background process
			// UploadCreatorRecord upload = new UploadCreatorRecord();
			// upload.execute();
			nextProcess();
		}
	}

	protected void launchVisitorPage() {
		Intent intent = new Intent(this, VisitorDetailActivity.class);
		startActivity(intent);
		finish();
		// this.overridePendingTransition(R.anim.animation_leave,
		// anim.animation_enter);

	}

	/*
	 * private ArrayList<ZohoEmployeeRecord> getDataInList() {
	 * ArrayList<ZohoEmployeeRecord> employeeArrayList = new
	 * ArrayList<ZohoEmployeeRecord>(); for(int i=0;i<employeeList.size();i++) {
	 * // Create a new object for each list item
	 * 
	 * ZohoEmployeeRecord zohoEmployee = new ZohoEmployeeRecord();
	 * zohoEmployee.setEmployeeName(employeeList.get(i).getEmployeeName());
	 * zohoEmployee.setEmployeePhoto(employeeList.get(i).getEmployeePhoto());
	 * zohoEmployee
	 * .setEmployeeMobileNumber(employeeList.get(i).getEmployeeMobileNumber());
	 * zohoEmployee.setEmployeeStatus(employeeList.get(i).getEmployeeStatus());
	 * zohoEmployee
	 * .setEmployeeSeatingLocation(employeeList.get(i).getEmployeeSeatingLocation
	 * ()); zohoEmployee.setEmployeeId(employeeList.get(i).getEmployeeId());
	 * zohoEmployee
	 * .setEmployeeExtentionNumber(employeeList.get(i).getEmployeeExtentionNumber
	 * ());
	 * zohoEmployee.setEmployeeEmailId(employeeList.get(i).getEmployeeEmailId
	 * ());
	 * 
	 * // Add this object into the ArrayList myList
	 * employeeArrayList.add(zohoEmployee); } return employeeArrayList; }
	 * 
	 * /* Intent to launch the next Activity.
	 * 
	 * @author jayapriya
	 * 
	 * @created 30/09/2014
	 */
	private void nextProcess() {
		// utility.UsedQrCode.generateQrCode();
		Intent intent = new Intent(this, VisitorBadgeActivity.class);
		startActivity(intent);
		finish();
		// this.overridePendingTransition(R.anim.animation_enter,
		// R.anim.animation_leave);
		// Log.d("window","sign");
	}

	/*
	 * Created by jayapriya On 30/09/2014 Created by jayapriya This fuction
	 * clear the path and create the new path.
	 */
	private void removeView(RelativeLayout layout, SingleTouchView view) {
		view.signAgain();

	}

	/*
	 * created by jayapriya On 30/09/2014 Create Signature as a bitmap and store
	 * it to the local device.
	 */
	private String createBitmap(View v, String fileName) {
		// Log.d("window","createBitmap");
		sourceFile = constants.MainConstants.kConstantFileDirectory
				+ MainConstants.kConstantFileTemporaryFolder + fileName
				+ MainConstants.kConstantImageFileFormat;
		v.setDrawingCacheEnabled(true);
		v.buildDrawingCache(true);
		// v.setBackgroundColor(Color.WHITE);
		Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
		v.setDrawingCacheEnabled(false);
		Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), bitmap.getConfig());

		Canvas canvas = new Canvas(newBitmap);

		// Paint it white (or whatever color you want)
		canvas.drawColor(Color.WHITE);

		// Draw the old bitmap ontop of the new white one
		canvas.drawBitmap(bitmap, 0, 0, null);
		usedBitmap.storeBitmap(newBitmap,
				MainConstants.kConstantBusinessCardImageWidth, sourceFile);
		bitmap.recycle();
		newBitmap.recycle();
		// Log.d("window","createBitmap");
		return sourceFile;

	}
	
	/*
	 * created by jayapriya On 30/09/2014 Create Signature as a bitmap and store
	 * it to the local device.
	 */
	private Bitmap getSignBitmap(View v, String fileName) {
		// Log.d("window","createBitmap");
		sourceFile = constants.MainConstants.kConstantFileDirectory
				+ MainConstants.kConstantFileTemporaryFolder + fileName
				+ MainConstants.kConstantImageFileFormat;
		v.setDrawingCacheEnabled(true);
		v.buildDrawingCache(true);
		// v.setBackgroundColor(Color.WHITE);
		Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
		v.setDrawingCacheEnabled(false);
		Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), bitmap.getConfig());

		Canvas canvas = new Canvas(newBitmap);

		// Paint it white (or whatever color you want)
		canvas.drawColor(Color.WHITE);

		// Draw the old bitmap ontop of the new white one
		canvas.drawBitmap(bitmap, 0, 0, null);
		usedBitmap.storeBitmap(newBitmap,
				MainConstants.kConstantBusinessCardImageWidth, sourceFile);
		bitmap.recycle();
		//newBitmap.recycle();
		// Log.d("window","createBitmap");
		return newBitmap;

	}

	/**
	 * created By jayapriya On 30/09/2014 when back pressed then only called
	 * this function.
	 * 
	 */
	public void onBackPressed() {
		storePurposeRecord();

		launchVisitorPage();
		// Log.d("BusinesscardRead", "backpressed");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	protected void onResume() {
		super.onResume();
		// MainConstants.kContext = this;

	}

	/*
	 * Created By jayapriya On 31/10/2014 deallcate the objects
	 */
	private void deallocateObjects() {

		relativeLayoutGetVisitorSign = null;
		imageViewVisitorSignClear = null;
		imageViewGet = null;
		buttonOkay = null;
		singleTouchViewGetVisitorSign = null;
		sourceFile = null;
		visitorSign = null;
		sourceFile = null;
		imageViewBack = null;
		imageViewErrorInVisitorSign = null;
		textGetVisitPurpose = null;
		textGetEmployeeName = null;
		arrayOfVisitPurpose = null;
		currentTime = 0L;
		typeFaceTitle = null;
		typefaceButtonTitle = null;
		typeFaceSetVisit = null;
		creator = null;
		// mWifi = null;
		context = null;
		// connManager = null;
		textEmployeeErrorMsg = null;
		textViewEmployeeName = null;
		editTextTempIdNumber = null;
		textViewVisitPurpose = null;
		textViewVisitorSignature = null;
		database = null;
		employeeList = null;
		visitList = null;
		myAdapter = null;
		adapterGetVisitPurpose = null;
		employee = null;
		currentTime = 0L;
		relativeLayoutVisitPage
				.setBackgroundResource(MainConstants.kConstantZero);
		relativeLayoutVisitPage = null;

	}

	public class AutocompleteCustomArrayAdapter extends
			ArrayAdapter<ZohoEmployeeRecord> {

		final String TAG = "AutocompleteCustomArrayAdapter.java";

		Context mContext;
		int layoutResourceId;
		ZohoEmployeeRecord employee[] = null;

		public AutocompleteCustomArrayAdapter(Context mContext,
				int layoutResourceId, ZohoEmployeeRecord[] employee) {

			super(mContext, layoutResourceId, employee);

			this.layoutResourceId = layoutResourceId;
			this.mContext = mContext;
			this.employee = employee;
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			try {

				/*
				 * The convertView argument is essentially a "ScrapView" as
				 * described is Lucas post
				 * http://lucasr.org/2012/04/05/performance
				 * -tips-for-androids-listview/ It will have a non-null value
				 * when ListView is asking you recycle the row layout. So, when
				 * convertView is not null, you should simply update its
				 * contents instead of inflating a new row layout.
				 */
				if (convertView == null) {
					// inflate the layout
					LayoutInflater inflater = ((VisitActivity) mContext)
							.getLayoutInflater();
					convertView = inflater.inflate(layoutResourceId, parent,
							false);
				}

				// object item based on the position
				ZohoEmployeeRecord employees = employee[position];

				// get the TextView and then set the text (item name) and tag
				// (item ID) values
				// TextView textViewItem = (TextView)
				// convertView.findViewById(R.id.textViewItem);
				// textViewItem.setText(employees.employeeName);

				TextView textEmployeeName = (TextView) convertView
						.findViewById(id.text_view_employee_name);

				textEmployeeName.setText(employees.getEmployeeName().toUpperCase());
				// textEmployeeName.setTextColor(Color.WHITE);
				textEmployeeName.setTypeface(typeFaceSetVisit);

				TextView textEmployeePhoneNo = (TextView) convertView
						.findViewById(id.text_view_employee_Phone);
				textEmployeePhoneNo
						.setText(employees.getEmployeeMobileNumber());
				textEmployeePhoneNo.setTypeface(typeFaceSetVisit);
				// textEmployeePhoneNo.setTextColor(Color.rgb(80,80,80));
				ImageView employeeImage = (ImageView) convertView
						.findViewById(id.image_view_employee);

				// Uri imgUri=Uri.parse(arrayOfEmployeePhoto[position]);

				if ((employees.getEmployeePhoto() != null)
						&& (employees.getEmployeePhoto() != MainConstants.kConstantEmpty)
						&& (new File(employees.getEmployeePhoto()).exists())) {

					Bitmap bitmap;
					BitmapFactory.Options options = new BitmapFactory.Options();
					bitmap = BitmapFactory.decodeFile(employees.getEmployeePhoto(),
							options);

					if (bitmap != null) {

						Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200,
								200, true);
						// set circle bitmap
						Bitmap resizedBitmap = usedBitmap
								.getRoundedRectBitmap(resized);
						employeeImage.setImageBitmap(resizedBitmap);
						// resizedBitmap.recycle();
						// resized.recycle();
					}
					// bitmap.recycle();
				} else {

					Bitmap bitmap;
					// BitmapFactory.Options options = new
					// BitmapFactory.Options();
					bitmap = BitmapFactory.decodeResource(getResources(),
							drawable.photo);

					if (bitmap != null) {

						Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200,
								200, true);
						// set circle bitmap
						Bitmap resizedBitmap = usedBitmap
								.getRoundedRectBitmap(resized);
						employeeImage.setImageBitmap(resizedBitmap);

					}
					// bitmap.recycle();
				}

				// in case you want to add some style, you can do something
				// like:

			} catch (NullPointerException e) {
				// e.printStackTrace();
			} catch (Exception e) {
				// e.printStackTrace();
			}

			return convertView;

		}
	}

	private void insertRecordIntoLocalDb() {
		database.insertVisitorRecordInOffMode();
		ArrayList<VisitorRecord> visitorRecord = database
				.getVisitorRecordListInOffMode();
		Integer visitorId = visitorRecord.get(
				visitorRecord.size() - MainConstants.kConstantOne)
				.getVisitorIdInOffMode();

		/*
		 * ArrayList<database.dbschema.VisitRecord> visitRecordList =
		 * database.getAllVisitRecordListInOffMode();
		 * //Log.d("visitRecord","visitRecord" +visitRecordList.size()); for(int
		 * i =0; i<visitRecordList.size();i++){ Log.d("record","record" +
		 * visitRecordList.get(i).getVisitIdInOffMode());
		 * Log.d("record","record" + visitRecordList.get(i).getVisitorPhoto());
		 * Log.d("record","record" + visitRecordList.get(i).getVisitorSign());
		 * Log.d("record","record" +
		 * visitRecordList.get(i).getGMTDiffMinutes()); Log.d("record","record"
		 * + visitRecordList.get(i).getOfficeLocation());
		 * Log.d("record","record" +
		 * visitRecordList.get(i).getOfficeLocationNumber());
		 * 
		 * }
		 */
		//TODO For command line unwanted tables
//		database.insertVisitRecordInOffMode();

		VisitRecord visitRecord = database
				.getVisitRecordListInOffMode(visitorId);

		// Log.d("visitRecord" ,"visitRecord" +visitRecord.getPurpose());
		// Log.d("visitRecord" ,"visitRecord" +visitRecord.getVisitorPhoto());
		// Log.d("visitRecord" ,"visitRecord"
		// +visitRecord.getEmployeeEmailId());
		Integer visitId = visitRecord.getVisitIdInOffMode();
		// Log.d("visitRecord","visitRecord" +visitRecord.size());
		// Log.d("visitRecord","visitRecord" +visitRecord.get(visitRecord.size()
		// - MainConstants.ConstantOne).getVisitIdInOffMode());
		
		//TODO For command line unwanted tables
//		database.insertVisitorAndVisitLink(visitorId, visitId);
//		visitId = database.getVisitorVisitLinkRecord(visitorId);
		// Log.d("listRecord","listRecord" +visitId);
	}

	private void storePurposeRecord() {
		if ((textGetVisitPurpose != null)
				&& (textGetVisitPurpose.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (textGetVisitPurpose.getText().toString().trim().length() > MainConstants.kConstantZero)) {
			SingletonVisitorProfile.getInstance().setPurposeOfVisit(
					textGetVisitPurpose.getText().toString().trim());
		}

	}

	/*
	 * store TempIDNumber
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	private void storeTempIDNumber() {

		if ((editTextTempIdNumber != null)
				&& (editTextTempIdNumber.getText().toString().trim() != MainConstants.kConstantEmpty)
				&& (editTextTempIdNumber.getText().toString().trim().length() > MainConstants.kConstantZero)) {

			long tempID = 0;
			try {
				if (editTextTempIdNumber != null) {
					tempID = Long.parseLong(editTextTempIdNumber.getText()
							.toString().trim());
				}
			} catch (NumberFormatException e) {
				tempID = 0;
			}
			SingletonVisitorProfile.getInstance().setTempIDNumber(tempID);
		}

	}

	/*
	 * if keyboard done pressed then validate the visitor detail and then launch
	 * the visit page.
	 * 
	 * 
	 * @author jayapriya
	 * 
	 * @created 02/12/2014
	 */

	/*
	 * when done pressed then process started
	 * 
	 * @author karthick
	 * 
	 * @created april /2015
	 */
	private TextView.OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {

		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {
				// Log.d("onEditorAction", "onEditorAction");
				if ((textGetVisitPurpose != null)
						&& (textGetVisitPurpose.isShown())) {
					textGetVisitPurpose.dismissDropDown();
				}
				validDetails();
				return true;
			}

			return false;
		}

	};

	private void convertVisitorDetailsJsonData() {

		String jsonString = MainConstants.kConstantEmpty;
		SingletonVisitorProfile.getInstance().getVisitorName();
	}
}
