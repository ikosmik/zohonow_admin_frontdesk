package com.zoho.app.frontdesk.android.Keys_management.roomDataBase;

import android.arch.persistence.room.*;

import java.util.List;

@Dao
public interface DatabaseAccessInterface {

    final String table_name= "key_list";
    @Insert
    void addKeys(keyEntity entity);

    @Query("Select * from "+table_name+" where key_id = :key_id")
    List<keyEntity> getKeys(String key_id);

    @Query("DELETE FROM "+table_name+" WHERE key_id = :key_id")
    void deleteKey(String key_id);

    @Query("DELETE FROM "+table_name)
    void deleteAllKeys();

    @Query("SELECT * FROM "+table_name)
    List<keyEntity> getAllKeys();

    @Query("SELECT COUNT(key_id) FROM "+table_name)
    int getRecordCount();

    @Update
    void updateKeys(keyEntity entity);

    @Delete
    void deleteKeys(keyEntity entity);
}
