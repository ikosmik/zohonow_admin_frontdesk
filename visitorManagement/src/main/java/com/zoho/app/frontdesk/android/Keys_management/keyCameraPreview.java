// Created By jayapriya On 26/08/2014
package com.zoho.app.frontdesk.android.Keys_management;


import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.PreviewCallback;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

import constants.MainConstants;

/**
 * This class create the surfaceView and find the frontFacingCamera
 *
 * @author jayapriya
 * @created 26/08/2014
 *
 */

public class keyCameraPreview extends SurfaceView implements
        SurfaceHolder.Callback {
    public  Camera camera;
    private SurfaceHolder holder;
    int rotateangle = 0;
    //public boolean safeToTakePicture = false;
    int cameraId = MainConstants.kConstantZero;
    private FaceDetectionListener faceDetectionListener;
    /**
     * Created By jayapriya on 26/08/2014
     * @param context
     * @author jayapriya
     * @created 26/08/2014
     */
    public keyCameraPreview(Context context, FaceDetectionListener faceDetectionListener) {
        super(context);
        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        this.faceDetectionListener= faceDetectionListener;
    }

    /*
     * This function Open the front facing camera
     */
    /*
     * Creates a new Camera object to access  front-facing camera on the
     * device.
     * @see #open(int)
     * @author jayapriya
     * @created 26/08/2014
     */
    private void openCamera() {

        cameraId = findFrontFacingCamera();
        if (cameraId ==  MainConstants.kConstantOne) {
            camera = Camera.open(cameraId);
            if( faceDetectionListener!=null) {
                camera.setFaceDetectionListener(faceDetectionListener);

                camera.startFaceDetection();
            }
        }
    }

    /*
     * This function find the front facing camera in the device.
     * @author jayapriya
     * @created 26/08/2014
     */
    private int findFrontFacingCamera() {
        //Log.d("preview","frontFacing");
        int cameraId = MainConstants.kConstantMinusOne;
        // Search for the front facing camera
        int numberOfCameras = MainConstants.kConstantZero;
        numberOfCameras = Camera.getNumberOfCameras();

        for (int i = MainConstants.kConstantZero; i < numberOfCameras; i++) {
            CameraInfo info = new CameraInfo();
            //Log.d("window", "Camera found" + info.facing);
            Camera.getCameraInfo(i, info);
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                //Log.d("window", "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }
    /**
     * The Surface has been created, acquire the camera and tell it where to
     * draw.
     * @author jayapriya
     * @created 26/08/2014
     */
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

        openCamera();
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.setPreviewCallback(new PreviewCallback() {

                public void onPreviewFrame(byte[] data, Camera arg1) {
                    keyCameraPreview.this.invalidate();
                }
            });
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    /**
     *
     * If your preview can change or rotate, take care of those events here.
     * Make sure to stop the preview before resizing or reformatting it.
     *
     * @author jayapriya
     * @created 26/08/2014
     */
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        //Log.d("preview","SurfaceChanged");
        Camera.Parameters parameters = camera.getParameters();
        parameters.set("orientation", "portrait");
        //parameters.setRotation(90);
        parameters.setPreviewSize(width, height);
        //setCameraDisplayOrientation(null, height, camera)
        camera.setDisplayOrientation(rotateangle);
        // camera.setParameters(parameters);
        camera.startPreview();
        //safeToTakePicture = true;
    }



    /**
     *  Surface will be destroyed when we return, so stop the preview.
     * Because the CameraDevice object is not a shared resource, it's very
     * important to release it when the activity is paused.
     *
     * @author jayapriya
     * @created 26/08/2014
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // Surface will be destroyed when we return, so stop the preview.
        // Because the CameraDevice object is not a shared resource, it's very
        // important to release it when the activity is paused.
        //Log.d("preview","SurfaceDestroyed");
        //Log.d("current","Time"  + System.currentTimeMillis());
        if(camera!=null){
            //stop preview before making changes
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }

    }

    /*
     * set the cameraDisplay orientaion.
     *
     * @author jayapriya
     * @created 26/08/2014
     */
    public  void stopFaceDetectionListener() {

        if( camera!=null) {
            camera.stopFaceDetection();
        }
    }

    /*
     * set the cameraDisplay orientaion.
     *
     * @author jayapriya
     * @created 26/08/2014
     */
    public  void setCameraDisplayOrientation(int rotation) {
        CameraInfo info =
                new CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }


        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            rotateangle = (info.orientation + degrees) % 360;
            rotateangle= (360 - rotateangle) % 360;  // compensate the mirror
        } else {  // back-facing
            rotateangle = (info.orientation - degrees + 360) % 360;
        }
        //Log.d( "result","rotateangle" +rotateangle);
        // camera.setDisplayOrientation(result);
    }


}