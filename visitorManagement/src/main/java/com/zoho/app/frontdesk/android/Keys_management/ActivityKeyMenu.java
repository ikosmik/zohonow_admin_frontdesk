package com.zoho.app.frontdesk.android.Keys_management;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.KeyBackgroundProcess;
import com.zoho.app.frontdesk.android.R;

public class ActivityKeyMenu extends FragmentActivity {

//    private FragmentTransaction transaction;
//    private FragmentManager fragmentManager;
//    private final static String TAG = "MenuActivity";

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.key_homepage);
        //replaceFragment(key_selection_fragment,TAG);
        ImageView image_bg_process = findViewById(R.id.imageView_bg_process);
        image_bg_process.setImageResource(R.drawable.icon_settings_gear);
        image_bg_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_background_process();
            }
        });
    }
    public void replaceFragment() {

        try {
            // Create fragment and give it an argument specifying the article it should show
            key_selection_fragment key_selection_fragment= new key_selection_fragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.animator.fade_in,android.R.animator.fade_out
                                            ,android.R.animator.fade_in,android.R.animator.fade_out);
            transaction.replace(R.id.fragment_body, key_selection_fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } catch (Exception e) {
            Log.e(" Error "," Fragment replacement *********** "+e.getMessage());
        }

    }
    public void start_background_process(){
        try{
            KeyBackgroundProcess keyBackgroundProcess = new KeyBackgroundProcess(this);
            Toast.makeText(this,"Background process Started", Toast.LENGTH_LONG).show();
            finish();
            startActivity(getIntent());
        }catch (Exception e){
            Log.e("Failed","******************* Try Again :-) : "+e.getMessage());
        }
    }
}
