package com.zoho.app.frontdesk.android;

import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import backgroundprocess.SchedulerEventReceiver;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.NumberConstants;
import constants.StringConstants;
import utility.NetworkConnection;
import utility.SetLocationToSingleton;
import appschema.SharedPreferenceController;
import utility.UsedAlarmManager;
import utility.UsedBitmap;
import utility.WriteErrorToCSV;

import com.brother.ptouch.sdk.NetPrinter;
import com.brother.ptouch.sdk.Printer;
import com.zoho.app.frontdesk.android.Keys_management.ActivityKeyList;
import com.zoho.app.frontdesk.android.Keys_management.ActivityKeyMenu;
import com.zoho.app.frontdesk.android.Keys_management.keyUtility.keySingleton;

import constants.MainConstants;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * This is a option page entry. This activity class has three options one will
 * be the manual or existing visitor entry, another one is a business card
 * entry and another one is qrcode entry.
 *
 * @author jayapriya
 * @created 19/08/2014
 */
public class WelcomeActivity extends Activity {

    private boolean alarmUp;
    private RelativeLayout relativeLayoutManualDetail, relativeLayoutMain,
            relativelayoutOverlay;// ,relativeLayoutScannedBusinessCard,relativeLayoutQrCode;
    //	private File file;
//	private Uri sourceFileUri;
    // private ImageView ImageViewHome,imageViewHand;
    private Typeface typefaceTitle;
    private TextView textViewManualEntryTitle, textViewManualEntryShadow,
            textviewTab;// ,
    // textViewQrCodeEntryTitle,textViewBusinessCardEntryTitle;
    private ImageView imageViewPropsRight, imageviewTexture, imageViewSettingsPage;
    private Long currentTime = System.currentTimeMillis();
    // Animation shake = new TranslateAnimation(0, 50, 0, 0);
    // private Handler handlerChangeBackground;
//	private int bgImgCount = MainConstants.kConstantTwo;
    private int tabCount =  NumberConstants.kConstantZero;
    private Handler handlerSettingsPage;
    // private TextView forgetId;
    // private BitmapDrawable drawableBitMap,drawableBitMap_overlay;
    private boolean isHomePageLaunched = false;
    private Handler brightnessDimHandler;
    private boolean isPermissionDialogShow = false;
    private SharedPreferenceController spController;
    private int applicationMode = NumberConstants.kConstantZero;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_welcome);

        assignObjects();
        initializeAlarmManager();

        setMaxVolumeMusic();

//        if application mode is Datacentre then Check For Printer to Print ID Card for DataCentre Visitor's.
//        if(applicationMode==2) {
//            if (SingletonCode.getSingletonWifiPrinterIP() == null
//                    || SingletonCode.getSingletonWifiPrinterIP().toString()
//                    .length() <= MainConstants.kConstantZero) {
//                SearchThread searchPrinter = new SearchThread();
//                searchPrinter.start();
//            }
//        }

//		DialogView.showReportDialog(this, MainConstants.badgeDialog,
//				MainConstants.forgotIDUploadCode);
//		DialogView.showReportDialog(this,
//				MainConstants.badgeDialog,
//				ErrorConstants.visitorAllSuccessCode);
//		DialogView.showReportDialog(
//				this,
//				MainConstants.badgeDialog, SingletonVisitorProfile.getInstance().getUploadedCode(),null);

//		Intent intent = new Intent(getApplicationContext(),
//				ActivityDataCentreDeviceDetails.class);
//		startActivity(intent);

//        set Background For Activity.
        setThemeColorForActivity();

//        Log.d("DSK ","Density "+getResources().getDisplayMetrics().density);

//       check Device Permission
        if (checkPermission()) {
            if (turnGPSOn()) {
//               Log.d("DSKLocation","Location is On");
            } else {
//            turnGPSOff();
                //Enable GPS
//            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//            intent.putExtra("enabled", true);
//            getApplication().sendBroadcast(intent);
//                UsedCustomToast.makeCustomToast(WelcomeActivity.this,"Turn On Location!",Toast.LENGTH_SHORT,TOP).show();
            }
        }

//      Get Location Manager and check for GPS & Network location services
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            WriteErrorToCSV.writeData(ErrorConstants.locationOff,
                    ErrorConstants.locationOffCode);
            NetworkConnection.TurnGPSOn(getApplicationContext());
        }
        if (!NetworkConnection.isWiFiEnabled(getApplicationContext())) {
            WriteErrorToCSV.writeData(ErrorConstants.wifiOff,
                    ErrorConstants.wifiOffCode);
            NetworkConnection.wifiReconnect(getApplicationContext());
        }

        ImageView show_key_list = findViewById(R.id.imageView_key_list);
        show_key_list.setVisibility(View.GONE);
//        show_key_list.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                keySingleton.getInstance().clearInstance();
//                Intent i = new Intent(WelcomeActivity.this, ActivityKeyMenu.class);
//                startActivity(i);
//            }
//        });

    }
    /**
     * Created by jayapriya On 19/08/2014
     * Assign values to the objects.
     */
    private void assignObjects() {
        imageviewTexture = (ImageView) findViewById(R.id.imageview_texture);
        imageViewSettingsPage = (ImageView) findViewById(R.id.image_view_settings_icon);
        relativelayoutOverlay = (RelativeLayout) findViewById(R.id.relativelayoutOverlay);
        textViewManualEntryTitle = (TextView) findViewById(R.id.textViewManualEntry);
        textViewManualEntryShadow = (TextView) findViewById(R.id.textViewManualEntryShadow);
        textviewTab = (TextView) findViewById(R.id.textview_tab);

        relativeLayoutMain = (RelativeLayout) findViewById(R.id.relative_layout_welcome);
        relativeLayoutManualDetail = (RelativeLayout) findViewById(R.id.relativeLayoutManualDetail);

        SingletonVisitorProfile.getInstance().clearInstance();
        SingletonEmployeeProfile.getInstance().clearInstance();
        SingletonMetaData.getInstance().clearInstance();

        setAppLevelSingletonValues();

        // set Texture for background
//		UsedBitmap usedBitmap = new UsedBitmap();
//		bitmap = Bitmap.createBitmap(300, 480, Bitmap.Config.ARGB_8888);
//		bitmap = usedBitmap.applyNoiceEffect(bitmap);

//        if (imageviewTexture != null && ApplicationController.getInstance() != null && ApplicationController.getInstance().getNoiseBitmap() != null) {
//            imageviewTexture
//                    .setImageBitmap(ApplicationController.getInstance().getNoiseBitmap());
//            imageviewTexture.setAlpha(.08f);
//        }

        // handlerChangeBackground = new Handler();
        brightnessDimHandler = new Handler();
        handlerSettingsPage = new Handler();
        relativeLayoutManualDetail.setOnTouchListener(onTouchListener);

        assignSettingsView();

//      assign Font Style to the View
        setFontStyle();
    }

    /**
     * Log page open continue 3 tabs
     * <p>
     * Created by karthick
     * Created on 20150722
     */
    private void tabClick() {
        // Log.d("android","tabClick"+tabCount);
        if (tabCount == MainConstants.kConstantZero) {
            handlerSettingsPage.removeCallbacks(tabTask);
            handlerSettingsPage.postDelayed(tabTask,
                    MainConstants.kConstantTabDuration);
        }
        tabCount++;
        if (tabCount == MainConstants.kConstantThree) {
            tabCount = MainConstants.kConstantZero;
            callSettingsPage();
        }
    }

    /**
     * Created by Jayapriya on 11/10/2014
     * Initialize the alarmManager.
     */
    private void initializeAlarmManager() {
        alarmUp = (PendingIntent.getBroadcast(this,
                MainConstants.kConstantZero, new Intent(this,
                        SchedulerEventReceiver.class),
                PendingIntent.FLAG_NO_CREATE) != null);
        if (alarmUp) {
        } else {
            UsedAlarmManager.activateAlarmManager(this);
        }
    }

//    private void turnGPSOff(){
//        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//
//        if(provider.contains("gps")){ //if gps is enabled
//            final Intent poke = new Intent();
//            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
//            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
//            poke.setData(Uri.parse("3"));
//            sendBroadcast(poke);
//        }
//    }

    /**
     * Check Memory status
     *
     * @author Karthick
     * @created 20170706
     */
    private void memoryStatus() {
        try {
            MemoryInfo mi = new MemoryInfo();
            ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(mi);
            final Runtime runtime = Runtime.getRuntime();
            final long usedMemInMB = (runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
            //Log.d("usedMemInMB", "usedMemInMB"+usedMemInMB);
            if (usedMemInMB > 10) {

                //System.gc();
            }
        } catch (Exception e) {

        }
    }


    /**
     * this function will call when user touch is dispatch from the screen
     *
     * @author Shiva
     * @created 7/10/2016
     */
    public void onUserInteraction() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = (float) 255.0;
        getWindow().setAttributes(lp);
        // textViewManualEntryTitle.setTextColor(Color.parseColor("#91795d"));
    }

    /**
     * This function will call from the onUserIntraction to set the timer to
     * user dispatch time
     *
     * @author Shiva
     * @created 7/10/2016
     */
    private void setBrightnessDimTimer() {
        brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        brightnessDimHandler.postDelayed(brightnessDimCallback,
                MainConstants.BRIGHTNESS_DIM_TIMEOUT);
    }

    /**
     * Created On 31/10/2014 (non-Javadoc)
     *
     * @author jayapriya
     * Allocate the object values and get
     * the current Location
     */
    public void onStart() {
        super.onStart();
        setLocation();
        setBrightnessDimTimer();
        // setBusinessCardVisibility();
    }

    /**
     * Set tabCount is zero
     * <p>
     * Created by karthick
     * Created on 20150722
     */
    Runnable tabTask = new Runnable() {
        public void run() {
            tabCount = MainConstants.kConstantZero;
        }
    };

    /**
     * Set selected location in singleton variable
     *
     * @author Karthick
     * @created 20161018
     */
    public void onResume() {
        super.onResume();
        checkDeviceLocation();
        checkOfficeLocation();
        isHomePageLaunched = false;
    }

    /**
     * Created by Jayapriya On 31/10/2014
     *
     * @watch android.app.Activity#onStop()
     * deallocated the objects in the
     * activity
     */
    public void onStop() {
        super.onStop();
        if (brightnessDimHandler != null && brightnessDimCallback != null) {
            brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Bitmap bitmap = BitmapFactory
                    .decodeFile(constants.MainConstants.kConstantFileDirectory
                            + MainConstants.kConstantFileTemporaryFolder
                            + currentTime
                            + constants.MainConstants.kConstantBusinessCardFileName);
            Matrix mat = new Matrix();
            Bitmap rotatedBitmap;
            if (bitmap != null && bitmap.getWidth() < bitmap.getHeight()) {
                mat.postRotate(-90);
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                        bitmap.getWidth(), bitmap.getHeight(), mat, false);
            } else {
                rotatedBitmap = bitmap;
            }
            if (rotatedBitmap != null) {
                UsedBitmap usedBitmap = new UsedBitmap();
                usedBitmap
                        .storeBitmap(
                                rotatedBitmap,
                                MainConstants.kConstantCreatorImageWidth,
                                constants.MainConstants.kConstantFileDirectory
                                        + MainConstants.kConstantFileTemporaryFolder
                                        + currentTime
                                        + constants.MainConstants.kConstantBusinessCardFileName);
                SingletonVisitorProfile
                        .getInstance()
                        .setVisitorBusinessCard(
                                constants.MainConstants.kConstantFileDirectory
                                        + MainConstants.kConstantFileTemporaryFolder
                                        + currentTime
                                        + constants.MainConstants.kConstantBusinessCardFileName);
                rotatedBitmap.recycle();
                bitmap.recycle();
                finish();
            }
        }
    }

    /**
     * Created by Jayapriya On 05/11/2014
     * intent to launch the next activity to
     * search the existing visitor.
     */
    private void existingEntry() {
        if (SingletonVisitorProfile.getInstance().getCodeNo() == MainConstants.kConstantMinusOne) {
            SingletonVisitorProfile.getInstance().setCodeNo(
                    MainConstants.kConstantManualEntry);
            // handlerChangeBackground.removeCallbacks(changeBackgroundTask);
            if (applicationMode == NumberConstants.kConstantNormalMode) {
                if (isHomePageLaunched == false) {
                    callHomePage();
                    isHomePageLaunched = true;
                }
            } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                launchVisitorPhonePage();
            }
        }
        finishAffinity();
    }

    /**
     * Set max volume music
     *
     * @author Karthick
     * @created on 20170511
     */
    private void setMaxVolumeMusic() {
        try {
            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if (am != null) {
                am.setStreamVolume(AudioManager.STREAM_MUSIC,
                        am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            }
        } catch (Exception e) {
        }
    }



    /**
     * Created by Jayapriya On 31/10/2014 deallocate the Objects.
     */
    private void deallocateObjects() {
        alarmUp = false;
        typefaceTitle = null;
        textViewManualEntryTitle = null;
        if (relativeLayoutMain != null) {
            relativeLayoutMain.setBackground(null);
            relativeLayoutMain = null;
        }
        if (relativelayoutOverlay != null) {
            relativelayoutOverlay.setBackground(null);
            relativelayoutOverlay = null;
        }
        if (imageviewTexture != null) {
            imageviewTexture.setBackground(null);
            imageviewTexture = null;
        }
        brightnessDimHandler.removeCallbacks(brightnessDimCallback);
        brightnessDimHandler = null;
        if (spController != null) {
            spController = null;
        }
//		if (bitmap != null) {
//			bitmap.recycle();
//			bitmap = null;
//		}
        if(imageViewSettingsPage!=null)
        {
        imageViewSettingsPage=null;
        }
        applicationMode= NumberConstants.kConstantMinusOne;
    }

    /**
     * The class will be finished then destroy it.
     *
     * @author jayapriya
     * @created 26/11/2014
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    public void onUserLeaveHint() { // this only executes when Home is selected.
//		 Intent intent = new Intent(getApplicationContext(),
//		 WelcomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		 intent.addCategory(Intent.CATEGORY_HOME);
//		 startActivity(intent);
//		// super.onUserLeaveHint();
//		 Log.d("Android", "Android");
    }

    private void setLocation() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        Criteria locationCritera = new Criteria();
        locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
        locationCritera.setAltitudeRequired(false);
        locationCritera.setBearingRequired(false);
        locationCritera.setCostAllowed(true);
        locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);

        String providerName = locationManager.getBestProvider(locationCritera,
                true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(providerName);
        if (location != null) {
            SetLocationToSingleton.setSingletonValue(location.getLatitude(),
                    location.getLongitude());
        }
        locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location
                // provider.
                SetLocationToSingleton.setSingletonValue(
                        location.getLatitude(), location.getLongitude());
            }

            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

    }

    private boolean netPrinterList(int times) {
        NetPrinter[] mNetPrinter;
        boolean searchEnd = false;
        try {
            // get net printers of the particular model
            Printer myPrinter = new Printer();
            mNetPrinter = myPrinter.getNetPrinters("");
            final int netPrinterCount = mNetPrinter.length;
            // when find printers,set the printers' information to the list.
            if (netPrinterCount > 0) {
                searchEnd = true;

                // String dispBuff[] = new String[netPrinterCount];
                for (int i = 0; i < netPrinterCount; i++) {
//                    Log.d("DSK Android", "IP:"+mNetPrinter[i].ipAddress);
                    SingletonCode
                            .setSingletonWifiPrinterIP(mNetPrinter[i].ipAddress);
                }
            } else if (netPrinterCount == 0
                    && times == (MainConstants.kConstantTen - 1)) { // when no
                // printer
                // is found
                // String dispBuff[] = new String[1];
//                Printer Not Found.
                SingletonCode.setSingletonWifiPrinterIP("");
                searchEnd = true;
            }

        } catch (Exception e) {
        }

        return searchEnd;
    }

    /**
     * Call tabClick function
     * <p>
     * Created on 20150820
     * Created by karthick
     */
    private OnClickListener listenerTab = new OnClickListener() {
        public void onClick(View v) {
            tabClick();
        }
    };


    /**
     * created by karthick on 05/05/2015
     * listener for swipe
     */
    private OnTouchListener onTouchListener = new OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {

            int action = MotionEventCompat.getActionMasked(event);
            switch (action) {
                case (MotionEvent.ACTION_DOWN):
                    // Log.d("android","Action was DOWN");
                    return true;
                case (MotionEvent.ACTION_MOVE):
                    existingEntry();
                    return true;
                case (MotionEvent.ACTION_UP):
                    // Log.d("android","Action was UP");
                    return true;
                case (MotionEvent.ACTION_CANCEL):
                    // Log.d("android","Action was CANCEL");
                    return true;
                case (MotionEvent.ACTION_OUTSIDE):
                    // Log.d("android","Movement occurred outside bounds " +
                    // "of current screen element");
                    return true;
                default:
                    // return super.onTouchEvent(event);
            }

            return true;
        }
    };
    /**
     * click Listener to activate touch Listener when Screen brightness is Dim
     *
     * @author Shiva
     * @created 07/10/2016
     */
    private OnClickListener listenerBrightness = new OnClickListener() {
        public void onClick(View v) {
            relativeLayoutManualDetail.setOnTouchListener(onTouchListener);
        }
    };
    /**
     * Handler to manage Dim Operations
     */
    // private Handler brightnessDimHandler = new Handler();

    private Runnable brightnessDimCallback = new Runnable() {

        public void run() {
            // Log.d("Run method","called");
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = 0;
            getWindow().setAttributes(lp);
            // textViewManualEntryTitle.setTextColor(Color.parseColor("#FFFFFF"));
            relativeLayoutManualDetail.setOnTouchListener(null);
            relativeLayoutManualDetail.setOnClickListener(listenerBrightness);
        }
    };

    /**
     * Check the device location selected or not
     * if selected then set to singleton
     * otherwise goto settings page
     *
     * @author Karthick
     * @created 20170803
     */
    private void checkDeviceLocation() {
        String deviceLocation = MainConstants.kConstantEmpty;

        if (spController == null) {
            spController = new SharedPreferenceController();
        }
        if (spController != null) {
            deviceLocation = spController.getDeviceLocation(this);
        }
//        Log.d("DSK ","deviceLocation "+deviceLocation);
        if (deviceLocation != null && !deviceLocation.isEmpty() && SingletonVisitorProfile.getInstance() != null) {
            SingletonVisitorProfile.getInstance().setDeviceLocation(deviceLocation);
        } else {
            callSettingsPage();
        }

    }
    private void checkOfficeLocation() {
        String OfficeLocation = MainConstants.kConstantEmpty;

        if (spController == null) {
            spController = new SharedPreferenceController();
        }
        if (spController != null) {
            OfficeLocation = spController.getOfficeLocation(this);
        }
//        Log.d("DSK ","deviceLocation "+deviceLocation);
        if (OfficeLocation != null && !OfficeLocation.isEmpty() && SingletonVisitorProfile.getInstance() != null) {
            SingletonVisitorProfile.getInstance().setOfficeLocation(OfficeLocation);
        } else {
            callSettingsPage();
        }

    }
    /**
     * Check the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission();
            }
            return true;
        }
        return false;

    }

    /**
     * Request the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION},
                NumberConstants.requestAllPermission);
        isPermissionDialogShow = true;
    }

    /**
     * Handle app permission response
     *
     * @author Karthick
     * @created 20170208
     */

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == NumberConstants.requestAllPermission) {
            if (grantResults.length == 1 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//				finish();
            }
            if (grantResults.length == 2 && (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED)) {
//				finish();
            }
            if (grantResults.length == 3 && (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED || grantResults[2] != PackageManager.PERMISSION_GRANTED)) {
//				finish();
            }
            isPermissionDialogShow = false;
        } else {
//super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * clear bitmap
     *
     * @author Karthick
     * @created on 20170822
     */
    public void onBackPressed() {
        if (ApplicationController.getInstance() != null) {
            ApplicationController.getInstance().clearNoiseBitmap();
        }
        finish();
    }

    public class SearchThread extends Thread {
        /* search for the printer for 10 times until printer has been found. */
        public void run() {
            for (int i = 0; i < MainConstants.kConstantTen; i++) {
                // search for net printer.
                if (netPrinterList(i)) {
                    break;
                }
            }
        }
    }

    /**
     * Launch Welcome activity
     *
     * @author Karthikeyan D S
     * @created on 2018/06/02
     */
    private void launchVisitorPhonePage() {
        Intent intent = new Intent(this, ExistingVisitorActivity.class);
        startActivity(intent);
        finishAffinity();
    }
    /**
     * Set Font Style
     * <p>
     * Created by Karthikeyan D S
     * Created on 20180527
     */
    private void setFontStyle() {
        typefaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        if(textViewManualEntryTitle!=null && typefaceTitle!=null) {
            textViewManualEntryTitle.setTypeface(typefaceTitle);
        }if(textViewManualEntryShadow!=null && typefaceTitle!=null) {
            textViewManualEntryShadow.setTypeface(typefaceTitle);
        }
    }

    /**
     * assign Settings View based on App Location Mode
     *
     * Created by Karthikeyan D S
     * Created on 24OCT2018
     */
    private void assignSettingsView()
    {
        if (applicationMode == NumberConstants.kConstantNormalMode) {
            if (textviewTab != null) {
                textviewTab.setVisibility(View.VISIBLE);
                textviewTab.setOnClickListener(listenerTab);
            }
            if (imageViewSettingsPage != null) {
                imageViewSettingsPage.setVisibility(View.GONE);
            }
        } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
            if (textviewTab != null) {
                textviewTab.setVisibility(View.GONE);
            }
            if (imageViewSettingsPage != null) {
                imageViewSettingsPage.setVisibility(View.VISIBLE);
                imageViewSettingsPage.setOnClickListener(listenerOpenSettingsPage);
            }
        }
    }

    /**
     * Call Settings Page
     *
     * @author Karthikeyan D S
     * @created on 20180527
     */
    private void callSettingsPage() {
        Intent intentSettingsActivity = new Intent(this,
                ActivityStartBackground.class);
        startActivity(intentSettingsActivity);
        finish();
    }

    /**
     * Set Theme Color For Activity
     *
     * @author Karthikeyan D S
     * @created 20180326
     */
    public void setThemeColorForActivity() {
        if (spController == null) {
            spController = new SharedPreferenceController();
        }
        if (spController != null) {
//         new color
            int colorCodeAppTheme = spController.getAppThemeColor(WelcomeActivity.this);

//        Log.d("DSKcolorcode",""+colorCodeAppTheme);
            if (colorCodeAppTheme > MainConstants.kConstantZero) {
                if (SingletonMetaData.getInstance() != null) {
                    //here the theme is set to black ,wanna change un comment this line and comment line next to this line
                    //SingletonMetaData.getInstance().setThemeCode(colorCodeAppTheme);
                    SingletonMetaData.getInstance().setThemeCode(MainConstants.kConstantOne);
                }
            } else {
                spController.setAppThemeColor(WelcomeActivity.this, MainConstants.kConstantTwo);
                SingletonMetaData.getInstance().setThemeCode(MainConstants.kConstantOne);
            }

            if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
                if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                    relativeLayoutMain.setBackgroundColor(Color.BLACK);
                } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                    relativeLayoutMain.setBackgroundColor(Color.rgb(10, 204, 205));
                }
            }
        }
    }

    /**
     * Set AppLevel singleton values deviceLocation,
     * Application Mode , Device ID
     * <p>
     * Created by Karthikeyan D S
     * Created on 20180527
     */
    private void setAppLevelSingletonValues() {
        if (spController == null) {
            spController = new SharedPreferenceController();
        }
        if (spController != null) {
            applicationMode = spController.getApplicationMode(this);
            if (SingletonMetaData.getInstance() != null &&
                    applicationMode > NumberConstants.kConstantZero) {
                SingletonMetaData.getInstance().setApplicationMode(spController.getApplicationMode(this));
            } else if (applicationMode == NumberConstants.kConstantZero) {
                applicationMode = NumberConstants.kConstantNormalMode;
                spController.saveApplicationMode(getApplicationContext(), applicationMode);
            }
            if (SingletonMetaData.getInstance() != null
                    && spController.getDeviceLocation(this) != null
                    && !spController.getDeviceLocation(this).isEmpty())
            {
                SingletonMetaData.getInstance().setDeviceLocation(spController.getDeviceLocation(this));
                if(spController.getDeviceLocation(this).equalsIgnoreCase(StringConstants.kStringConstantsDeviceModeDataCenter)) {
                    String appSettingsPassword = spController.getAppSettingsPassword(WelcomeActivity.this);
                    if (appSettingsPassword != null && !appSettingsPassword.isEmpty()) {
                        // do nothing
                    } else {
                        callSettingsPage();
                    }
                }
            } else {
                callSettingsPage();
            }

            if (SingletonMetaData.getInstance() != null) {
                SingletonMetaData.getInstance().setDeviceID(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            }
        }
    }

    /**
     * Turn GPS On/Off
     *
     * @author Karthikeyan D S
     * @created 20170706
     */
    public boolean turnGPSOn() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
            return false;
        }
        return true;
    }


    /**
     * call HomePage Activity
     *
     * @author Karthikeyan D S
     * @created on 24OCT2018
     */
    private void callHomePage() {
        Intent nextActivityIntent = new Intent(this, HomePage.class);
        startActivity(nextActivityIntent);
        finishAffinity();
    }

    /**
     * settings Page Listener
     *
     * @author Karthikeyan D S
     * @created on 24OCT2018
     */
    private OnClickListener listenerOpenSettingsPage = new OnClickListener() {
        public void onClick(View v) {
//            Log.d("DSK ","Settings Page Call");
            callSettingsPage();
        }
    };

}
