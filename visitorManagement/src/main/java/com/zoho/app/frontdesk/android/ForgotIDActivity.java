package com.zoho.app.frontdesk.android;

import java.io.File;
import java.util.ArrayList;

import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import constants.StringConstants;
import utility.DialogView;
import utility.PrintBadge;
import utility.UsedBitmap;
import utility.UsedCustomToast;
import zohocreator.ForgotIDZohoCreator;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.R.drawable;
import com.zoho.app.frontdesk.android.R.id;
import com.zoho.app.frontdesk.android.R.layout;

import constants.FontConstants;
import constants.MainConstants;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;

public class ForgotIDActivity extends Activity {
	private EmployeeBadge employeeBadge;
    private RelativeLayout relativeLayoutVisitPage;
	private CustomAutoCompleteView textGetEmployeeName;
	private EditText editTextTempIdNumber;
	Dialog dialogGetHomeMessage;
    private TextView textEmployeeErrorMsg,textViewTempId,textViewName;
	private ImageView imageViewBack;
	private Button buttonOkay,buttonCancel;
	private Typeface typeFaceSetVisit,typeFaceTempID,typefaceButtonTitle,dialogMessageFont,
	dialogButtonFont,typefaceErrorDialogFont;
	private AutocompleteCustomArrayAdapter myAdapter;
	private ZohoEmployeeRecord[] employeeArray = new ZohoEmployeeRecord[0];
	private ZohoEmployeeRecord employee;
	private DbHelper database;
	private Context forgotIdContext;
	private Handler noActionHandler;
	private TextView textViewDialogHomeMessage,
	textViewHomeMessageConfirm,
	textViewHomeMessageCancel;
	private  boolean alarmUp;
	    private ArrayList<ZohoEmployeeRecord> employeeList;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set the orientation as Portrait
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
       // this.overridePendingTransition(R.anim.animation_enter,
                //R.anim.animation_leave);
        setContentView(R.layout.activity_forgot_id);
        assignObjects();
        
    }
	
	 /*
     * Assign the values to the variable
     *
     * @author karthick
     * @created 13/04/2015
     */
    private void assignObjects() {

    	forgotIdContext=this;
    	database = new DbHelper(this);
    	noActionHandler=new Handler();
    	noActionHandler.postDelayed(noActionThread, MainConstants.noFaceTime);
		
    	relativeLayoutVisitPage=(RelativeLayout) findViewById(R.id.relativeLayoutVisitPage);
        imageViewBack=(ImageView) findViewById(id.imageViewVisitPageBack);
        textEmployeeErrorMsg=(TextView) findViewById(id.textViewEmployeeDetailErrorMsg);
        textGetEmployeeName=(CustomAutoCompleteView) findViewById(id.textGetForgotIdEmployeeName);
        editTextTempIdNumber=(EditText) findViewById(id.temp_id_number);
        textViewTempId=(TextView) findViewById(id.textView_temp_id);
        textViewName=(TextView) findViewById(id.textViewEmployeeDetail);
        buttonOkay = (Button) findViewById(id.button_visit_detail_ok);
        employeeBadge = (EmployeeBadge) findViewById(R.id.employeebadge);
        buttonCancel=(Button) findViewById(id.button_forgot_id_cancel);
        typeFaceTempID = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantJosefinSans);
        typeFaceSetVisit = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantProximaNovaReg);
        typefaceButtonTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
        typefaceErrorDialogFont = Typeface.createFromAsset(getAssets(),
                FontConstants. fontConstantSourceSansProbold);
        dialogMessageFont = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantSourceSansProSemibold);
		dialogButtonFont = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantSourceSansProSemibold);
		
        textGetEmployeeName.setTypeface(typeFaceSetVisit);
        editTextTempIdNumber.setTypeface(typeFaceSetVisit);
        textViewTempId.setTypeface(typeFaceTempID);
        textViewName.setTypeface(typeFaceTempID);
        buttonOkay.setTypeface(typefaceButtonTitle);
        //set background image
        //relativeLayoutVisitPage.setBackgroundResource(drawable.bgnd_pattern_dark_blue_mini);

        // set the input type
        textGetEmployeeName
                .setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                        | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        editTextTempIdNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        // set the custom ArrayAdapter
        myAdapter = new AutocompleteCustomArrayAdapter(this, layout.custom_spinner, employeeArray);
        textGetEmployeeName.setAdapter(myAdapter);
        imageViewBack.setOnClickListener(listenerTappedBack);


        textGetEmployeeName.addTextChangedListener(textWatcherEmployeeNameChanged);
        
        textGetEmployeeName.setOnItemClickListener(listenerChooseEmployee);
        textGetEmployeeName.setOnFocusChangeListener(listenerSetFocusOnEmployee);
        
        editTextTempIdNumber.addTextChangedListener(textWatcherTempIDChanged);
        editTextTempIdNumber.setOnEditorActionListener(listenerDonePressed);
        editTextTempIdNumber.setOnTouchListener(tempIDNumberTouchListner);
        buttonCancel.setOnClickListener(listenerTappedBack);
        
        buttonOkay.setOnClickListener(listenerOK);
        
        
    }
    
    private View.OnFocusChangeListener listenerSetFocusOnEmployee = new View.OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (textGetEmployeeName.hasFocus() == false) {

                if ((textGetEmployeeName != null) && (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
                        && (textGetEmployeeName.getText().toString().trim().length() > MainConstants.kConstantZero) && ((employee == null) || (employee.getEmployeeName()== null) || (employee.getEmployeeName() == MainConstants.kConstantEmpty))) {

                    textEmployeeErrorMsg.setVisibility(View.VISIBLE);
                } else {
                    textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
                }
            }

        }
    };
    /*
     * textWatcher to validate the employeeName
     * 
     * @author karthick
     * @created 13/04/2015
     */
    private TextWatcher textWatcherEmployeeNameChanged = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

            employee=null;
            if ((textGetEmployeeName != null) &&(textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
                    &&(textGetEmployeeName.getText().toString().trim().length() >= MainConstants.kConstantThree)) {


                //Log.d("ontext1","changed");
                textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
                textGetEmployeeName.setBackgroundDrawable(getResources().getDrawable(
                        drawable.cell_scanning_detail));


                // get suggestions from the database
                employeeList = database.getEmployeeListWithNameWithoutFrequency(textGetEmployeeName.getText().toString().trim().replaceAll("'","''"));

                if((employeeList !=null) && (employeeList.size() > MainConstants.kConstantZero)){

                    ZohoEmployeeRecord[] employeeArray = employeeList.toArray(new ZohoEmployeeRecord[employeeList.size()]);
                   // Log.d("emp","" +employeeArray.length);
                   // Log.d("emp","employeeArray"+employeeArray.toString());

                    if((employeeArray !=null)&& (employeeArray.length > MainConstants.kConstantZero)) {
                        // update the adapter
                    
                        myAdapter = new AutocompleteCustomArrayAdapter(getApplicationContext(), layout.custom_spinner, employeeArray);
                        textGetEmployeeName.setAdapter(myAdapter);
                        // update the adapater
                        //myAdapter.notifyDataSetChanged();
                    }

                } else {

                    myAdapter.clear();
                    myAdapter.notifyDataSetChanged();
                    textEmployeeErrorMsg.setVisibility(View.VISIBLE);

                }
            }
            else
            {
                if((SingletonEmployeeProfile.getInstance() !=null)|| (employee == null)) {
                    SingletonEmployeeProfile.getInstance().clearInstance();
                }
                myAdapter.clear();
                myAdapter.notifyDataSetChanged();
                textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
          //  Log.d("after", "text");


        }
    };
    
    /*
     *Choose employee from list
     * @author karthick
     * @created 13/04/2015
     */
    private AdapterView.OnItemClickListener listenerChooseEmployee = new AdapterView.OnItemClickListener() {


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            employee = (ZohoEmployeeRecord) parent.getAdapter().getItem(position);
            String name = ((ZohoEmployeeRecord)parent.getAdapter().getItem(position)).getEmployeeName();
           // Log.d("parent","" + parent.getAdapter().getItemId(position));
            //Log.d("employee","" +employee.employeeEmailId);
            //Log.d("employee","" +employee.employeeId);
            //Log.d("employee","" +employee.employeeName);
            textGetEmployeeName.removeTextChangedListener(textWatcherEmployeeNameChanged);
            textGetEmployeeName.setText(name.toUpperCase());
            textGetEmployeeName.setSelection(textGetEmployeeName.getText().length());
            textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
            textGetEmployeeName.addTextChangedListener(textWatcherEmployeeNameChanged);
            editTextTempIdNumber.requestFocus();
        }
    };

    private OnTouchListener tempIDNumberTouchListner = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // TODO Auto-generated method stub
            editTextTempIdNumber.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editTextTempIdNumber, InputMethodManager.SHOW_IMPLICIT);
            return false;
        }


    };
    
    /*
     * TouchListner for focus and open keyboard
     *
     * @author karthick
     * @created 13/04/2015
     */
    public TextWatcher textWatcherTempIDChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
             if (editTextTempIdNumber != null){

            	 editTextTempIdNumber.setBackgroundDrawable(getResources().getDrawable(
                        drawable.cell_scanning_detail));
            }
        }
        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    
    /*
     * when done pressed then process started
     * @author karthick
     * @created 13/04/2015
     */
     private TextView.OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {

         @Override
         public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
             if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                     || (actionId == EditorInfo.IME_ACTION_DONE)) {
                 //Log.d("onEditorAction", "onEditorAction");
            	 validDetails();
                 return true;
             }

             return false;
         }

     };
     /*
      * listener to tapped the back button
      *
      * @author karthick
      * @created 13/04/2015
      *
      */
     private OnClickListener listenerTappedBack = new OnClickListener() {

         @Override
         public void onClick(View v) {
        	 if(((editTextTempIdNumber!=null)&&(editTextTempIdNumber.getText().toString()!="")
 					&&(editTextTempIdNumber.getText().toString().length()>0))||((textGetEmployeeName != null) &&(textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
 		                    &&(textGetEmployeeName.getText().toString().trim().length() >0))){
 				intializeDialogBoxForHomePage();
 				showHomeDialogBox();
 			}
 			else
 			{
 				Intent intent = new Intent(getApplicationContext(),
 						HomePage.class);
 				startActivity(intent);
 				noActionHandler.removeCallbacks(noActionThread);
 				finish();
 			}
         }
     };
     /*
      * @author karthick
      * @created 13/04/2015
      */
     private OnClickListener listenerOK = new OnClickListener() {

         @Override
         public void onClick(View arg0) {
        	 validDetails();
         }
     };
     
     
     private OnClickListener listenerTappedCancel = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(((editTextTempIdNumber!=null)&&(editTextTempIdNumber.getText().toString()!="")
					&&(editTextTempIdNumber.getText().toString().length()>0))||((textGetEmployeeName!=null)&&(textGetEmployeeName.getText().toString()=="")
							&&(textGetEmployeeName.getText().toString().length()>0))){
				intializeDialogBoxForHomePage();
				showHomeDialogBox();
			}
			else
			{
				getHomePage();
			}
			
		}
	};
	
	
	/*
	 * Created by jayapriya On 01/12/2014 Intialize the dialogbox for home page
	 */
	private void intializeDialogBoxForHomePage() {
		dialogGetHomeMessage = new Dialog(this);
		dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogGetHomeMessage
				.setContentView(R.layout.dialog_box_confirm_message);
		// dialog.(MessageConstant.dialogTitle);
		dialogGetHomeMessage.setCanceledOnTouchOutside(true);
		dialogGetHomeMessage.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message);
		textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_confirm);
		textViewHomeMessageCancel = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_cancel);
		textViewDialogHomeMessage
				.setText(StringConstants.kConstantHomePageErrorMessage);
		textViewDialogHomeMessage.setTypeface(dialogMessageFont);
		textViewHomeMessageCancel.setTypeface(dialogButtonFont);
		textViewHomeMessageConfirm.setTypeface(dialogButtonFont);
		// textViewHomeMessageConfirm.setText(MainConstants.kConstantOka);
		textViewHomeMessageConfirm.setOnClickListener(discardConfirm);
		textViewHomeMessageCancel.setOnClickListener(discardCancel);

	}
	
	/*
	 * Created by Jayapriya On 02/12/2014 Click the errorMessageCancel to
	 * dismiss the dialogBox
	 */
	private OnClickListener discardCancel = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if ((dialogGetHomeMessage != null)
					&& (dialogGetHomeMessage.isShowing())) {
				dialogGetHomeMessage.dismiss();
			}
		}
	};


	/*
	 * Created by jayapriya On 01/12/2014 show dialogbox for home page
	 */
	private void showHomeDialogBox() {

		if ((dialogGetHomeMessage != null)
				&& (!dialogGetHomeMessage.isShowing())) {

			dialogGetHomeMessage.show();
		}
	}
	
	/*
	 * Created by Jayapriya On 26/09/2014 Click the errorMessageConfirm to
	 * activate the next processing.
	 */
	private OnClickListener discardConfirm = new OnClickListener() {

		@Override
		public void onClick(View v) {
			
			if ((dialogGetHomeMessage != null)) {
				dialogGetHomeMessage.dismiss();
			}
			Intent intent = new Intent(getApplicationContext(),
						HomePage.class);
				startActivity(intent);
				
				noActionHandler.removeCallbacks(noActionThread);
				finish();
		}
	};

     public void onBackPressed() {
    	 Intent intent = new Intent(getApplicationContext(),
					HomePage.class);
			startActivity(intent);
			
			noActionHandler.removeCallbacks(noActionThread);
			finish();
    		 }
     
     
    
     /*
      * @author karthick
      *
      */
     private void validDetails() {
        long tempID=0;
        try {
    	 		if (editTextTempIdNumber !=null)
    			 {
    		 tempID=Long.parseLong(editTextTempIdNumber.getText().toString().trim());
    			 }
        }
        catch(NumberFormatException e) {
        	tempID=0;
        }
    	 		
    	 		 //Log.d("window","SingletonCode:"+SingletonCode.getSingletonSelectionCode());

         if ((employee !=null) && (employee.getEmployeeName() != null) 
        		 && (employee.getEmployeeName() !=MainConstants.kConstantEmpty)
        		&&tempID>0) {

        	 //Log.d("employee","" +employee.employeeName);
        	 printBadge(employee);
        	 if((SingletonCode.getSingletonPrinterErrorCode()!=null)&&(SingletonCode.getSingletonPrinterErrorCode()!="")&&(SingletonCode.getSingletonPrinterErrorCode().contains("Success"))){
        	 DialogView.showReportDialog(forgotIdContext,MainConstants.forgotIDDialog,MainConstants.forgotIDUploadCode);
        	 String file_url = null;
        	 //Testing printer
        	 new backgroundProcess().execute(file_url);
        	 }
        	 	
         }
         else
         {
        	 utility.UsedDialogBox.intializeDialogBox(this, StringConstants.kConstantErrorMsgToValidDetail);
             
        	 if ((employee ==null) || (employee.getEmployeeName() == null) 
            		 || (employee.getEmployeeName() ==MainConstants.kConstantEmpty)) {

            	 textGetEmployeeName.setBackgroundDrawable(getResources().getDrawable(
                         drawable.cell_error_border));


             }
        	 
             if(tempID<=0) {
            editTextTempIdNumber.setBackgroundDrawable(getResources().getDrawable(
                    drawable.cell_error_border));
             }
             else
             {
            	 editTextTempIdNumber.setBackgroundDrawable(getResources().getDrawable(
                         drawable.cell_scanning_detail));
             }
             	
         }
     }
     /*
      * Created By karthick On 15/04/2014 deallcate the objects
      */
     private void deallocateObjects() {
    		//Log.d("Android", "deallocate");
         imageViewBack = null;
         textGetEmployeeName = null;
         typeFaceSetVisit = null;
         textEmployeeErrorMsg = null;
         editTextTempIdNumber=null;
         database = null;
         employeeList = null;
         myAdapter = null;
         employee = null;
         relativeLayoutVisitPage.setBackgroundResource(MainConstants.kConstantZero);
         relativeLayoutVisitPage=null;

     }
     
     public class AutocompleteCustomArrayAdapter extends ArrayAdapter<ZohoEmployeeRecord> {

         final String TAG = "AutocompleteCustomArrayAdapter.java";

         Context mContext;
         int layoutResourceId;
         ZohoEmployeeRecord employee[] = null;

         public AutocompleteCustomArrayAdapter(Context mContext, int layoutResourceId, ZohoEmployeeRecord[] employee) {

             super(mContext, layoutResourceId, employee);

             this.layoutResourceId = layoutResourceId;
             this.mContext = mContext;
             this.employee = employee;
         }
         
         public class ViewHolder {
 			public  TextView textEmployeeName,textEmployeePhoneNo;
 			public ImageView employeeImage;
 		}

 		public View getView(final int position, View convertView,
 				ViewGroup parent) {
 			ViewHolder view;
 			ZohoEmployeeRecord employees = employee[position];
 			UsedBitmap usedBitmap = new UsedBitmap();
 			//View gridView;

 			if (convertView == null) {
 				LayoutInflater mInflater = (LayoutInflater) mContext
 	 					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                view = new ViewHolder();
 				convertView = mInflater.inflate(layoutResourceId, null);
 				
 				view.textEmployeeName = (TextView) convertView
                        .findViewById(id.text_view_employee_name);

 				
                //textEmployeeName.setTextColor(Color.WHITE);
 				view.textEmployeeName.setTypeface(typeFaceSetVisit);

 				view.textEmployeePhoneNo = (TextView) convertView
                        .findViewById(id.text_view_employee_Phone);
 				
 				view.textEmployeePhoneNo.setTypeface(typeFaceSetVisit);
                //textEmployeePhoneNo.setTextColor(Color.rgb(80,80,80));
 				view.employeeImage = (ImageView) convertView.findViewById(id.image_view_employee);

 				convertView.setTag(view);
 			} else {
 				view = (ViewHolder) convertView.getTag();
 			} 
 			
 			view.textEmployeeName.setText(employees.getEmployeeName().toUpperCase());
 			view.textEmployeePhoneNo.setText(employees.getEmployeeMobileNumber());
 			//Log.d("adapter employee","name"+employees.employeeFirstName+"lastName "+employees.employeeLastName);
 			 
 			String employeePhoto=MainConstants.kConstantFileDirectory
                    + MainConstants.kConstantFileEmployeeImageFolder
                    + employees.getEmployeeZuid()
                    + MainConstants.kConstantImageFileFormat;
			
 			if ((employees.getEmployeePhoto() != null)
                    && (employees.getEmployeePhoto() != MainConstants.kConstantEmpty)
                    && (new File(employeePhoto).exists())) {
                //Log.d("adapter employee","photo"+employees.employeePhoto);
                Bitmap bitmap;
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(employeePhoto, options);

                if (bitmap != null) {

                    Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                            200,
                            200, true);
                    // set circle bitmap
                    Bitmap resizedBitmap = usedBitmap
                            .getRoundedRectBitmap(resized);
                    view.employeeImage.setImageBitmap(resizedBitmap);
                    //resizedBitmap.recycle();
                    resized.recycle();
                }
                bitmap.recycle();
            } else {

                Bitmap bitmap;
                // BitmapFactory.Options options = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeResource(getResources(), drawable.photo);



                if (bitmap != null) {

                    Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                            200,
                            200, true);
                    // set circle bitmap
                    Bitmap resizedBitmap =usedBitmap
                            .getRoundedRectBitmap(resized);
                    view.employeeImage.setImageBitmap(resizedBitmap);
                    resized.recycle();
                }
                 bitmap.recycle();
            }
 			
 			return convertView;
 		}
     }
     
     /*
 	 * background process for upload forgotID 
 	 */
 	public class backgroundProcess extends AsyncTask<String, String, String> {
 		String forgotRecordID="";
 		int PeopleAPICode=0;
 		@Override
 		protected void onPreExecute() {
 			super.onPreExecute();

 		}

 		protected String doInBackground(String... f_url) {

 			ForgotIDZohoCreator forgotIDZohoCreator=new ForgotIDZohoCreator();
 			//VisitorZohoCreator VisitorZohoCreator=new VisitorZohoCreator(getApplicationContext());
 			 forgotRecordID=forgotIDZohoCreator.uploadForgotID(employee, editTextTempIdNumber.getText().toString().trim());
 			//forgotRecordID="123";
 			//Log.d("Android", "forgotRecordID:"+forgotRecordID);
 			if(forgotRecordID!=null&&forgotRecordID.trim().length()>0)
 			{
 			  PeopleAPICode=forgotIDZohoCreator.employeeCheckIn(employee.getEmployeeEmailId());
 			//Log.d("Android", "forgotRecordID:"+forgotRecordID);
 			}
 			//PeopleAPICode=1000;
 			
 			if(forgotRecordID.length()>0)
 			{
 			String upLoadEmployeePhotoUri = MainConstants.kConstantFileUploadUrl
                    + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantFileUploadAppNameAndFormNameInForgotID
                    + MainConstants.kConstantFileUploadFieldNameInEmployeePhoto
                    + forgotRecordID
                    + MainConstants.kConstantFileAccessTypeAndName
                    + MainConstants.kConstantFileSharedBy;
 			String employeePhoto=MainConstants.kConstantFileDirectory
                    + MainConstants.kConstantFileEmployeeImageFolder
                    + employee.getEmployeeZuid()
                    + MainConstants.kConstantImageFileFormat;
 			//Log.d("fileUrl","photo"+upLoadEmployeePhotoUri +"object photo "+employeePhoto);
 			forgotIDZohoCreator.uploadFile(forgotIdContext,upLoadEmployeePhotoUri, employeePhoto);
 			forgotIDZohoCreator.updateEmployeeTempIdUploadCompleted(forgotRecordID);
 			}
 			
 			if(forgotRecordID!=null&&forgotRecordID.length()<=0)
 			{
 				forgotIDZohoCreator.updateEmployeeCheckInErrorCode(forgotRecordID,MainConstants.forgotIdNetworkError);
 			}
 			else if(PeopleAPICode!=0 && PeopleAPICode!=MainConstants.peopleResponseSuccessCode)
 			{ //  Log.d("people", "code"+PeopleAPICode);
 				forgotIDZohoCreator.updateEmployeeCheckInErrorCode(forgotRecordID,PeopleAPICode);
 			}
 			return forgotRecordID;
 		}

 		/**
 		 * Updating progress bar
 		 * */
 		protected void onProgressUpdate(String... progress) {
 			// setting progress percentage
 		}

 		/**
 		 * After completing background task Dismiss the progress dialog
 		 * **/
 		protected void onPostExecute(String file_url) {
 			MediaPlayer mp = MediaPlayer.create(getApplicationContext(),R.raw.bonk);
 			//Log.d("singleton","issue"+SingletonCode.getSingletonPrinterErrorCode());
 			
 			if(forgotRecordID==null)
 			{
 				
 	            mp.start();
 				//Log.d("Android", "network");
 	            DialogView.hideDialog();
 				DialogView.showReportDialog(getApplicationContext(),MainConstants.forgotIDDialog,MainConstants.peopleResponseNetworkError);
 			}
 			else if(forgotRecordID!=null&&forgotRecordID.length()<=0)
 			{
 				DialogView.hideDialog();
 	            mp.start();
 				//Log.d("Android", "no forgotRecordID:"+forgotRecordID);
 				DialogView.showReportDialog(getApplicationContext(),
 						MainConstants.forgotIDDialog,MainConstants.noForgotRecordIDError);
 			}
 			else if(PeopleAPICode!=0 && PeopleAPICode!=MainConstants.peopleResponseSuccessCode)
 			{
 				
 	            mp.start();
 	           DialogView.hideDialog();
 			//Log.d("Android", "status fail :"+PeopleAPICode);
 			DialogView.showReportDialog(getApplicationContext(),MainConstants.forgotIDDialog,
 					PeopleAPICode);
 			}
 			else if(PeopleAPICode==MainConstants.peopleResponseSuccessCode) {
 				//Log.d("Android", "status:"+PeopleAPICode);
 				DialogView.hideDialog();
 				UsedCustomToast.makeCustomToast(getApplicationContext(), StringConstants.forgotToastMsg, Toast.LENGTH_LONG).show();
 	      		//printBadge(employee);
 				Intent intent = new Intent(getApplicationContext(),
 						HomePage.class);
 				startActivity(intent);
 				
 				noActionHandler.removeCallbacks(noActionThread);
 				finish();
 			}
 			
 		}
 	}
 	protected void onDestroy()
 	{
 		super.onDestroy();
 		//Log.d("Android", "forgot activity");
 		deallocateObjects();
 		DialogView.hideDialog();
 	}
 	
 	public void printBadge(ZohoEmployeeRecord employee) {

 		//Log.d("employee","detail" +employee.employeeEmailId +" "+employee.employeeName+ " "+employee.employeeId +" " +employee.employeePhoto);
		PrintBadge printBadge=new PrintBadge();
		SingletonEmployeeProfile.getInstance().setEmployeeName(employee.getEmployeeName());
   	    SingletonEmployeeProfile.getInstance().setEmployeeId(employee.getEmployeeId());
    	String employeePhoto=MainConstants.kConstantFileDirectory
             + MainConstants.kConstantFileEmployeeImageFolder
             + employee.getEmployeeZuid()
             + MainConstants.kConstantImageFileFormat;
		//Log.d("employee","detail"+employee.employeeFirstName+"lastname "+employee.employeeLastName);
   	    SingletonEmployeeProfile.getInstance().setEmployeePhoto(employeePhoto);
   	
        //	SingletonVisitorProfile.getInstance().setVisitorName(editTextVisitorName.getText().toString());
        //	SingletonVisitorProfile.getInstance().setPhoneNo("998877");
        //	utility.UsedQrCode.generateQrCode();
		    employeeBadge.setEmployeeDetails();
        	printBadge.storeEmployeeBitmap(employeeBadge);
        	//employeeBadge.setVisibility(View.VISIBLE);
       	 
        	//Log.d("printer","statement"+SingletonCode.getSingletonWifiPrinterIP());
        	//Testing to remove printer(07/01/16)
        	if((SingletonCode.getSingletonWifiPrinterIP()==null )|| (SingletonCode.getSingletonWifiPrinterIP().toString().length()<=0)){
        		///if((SingletonCode.getSingletonPrinterErrorCode()c== null) || (SingletonCode.getSingletonPrinterErrorCode()=="")){
        			SingletonCode.setSingletonPrinterErrorCode(StringConstants.kConstantErrorMsgInPrinterError);
        		//}
        			//Log.d("printer","statement");
        			DialogView.hideDialog();
      				DialogView.showReportDialog(this,MainConstants.printerIssue,MainConstants.reportStatusPrinterFailure);
      		
        	}
        	else if(!printBadge.printBadge()) {
        		//Log.d("printer","statement1"+SingletonCode.getSingletonWifiPrinterIP());
            	
        		//Log.d("Android", "printBadge");
        		//utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
    					//MainConstants.kConstantErrorMsgBadgePrint,
    					//Toast.LENGTH_LONG).show();
        		//IKDialogView.hideDialog();
        		if((SingletonCode.getSingletonPrinterErrorCode()== null) || (SingletonCode.getSingletonPrinterErrorCode()=="")){
        			SingletonCode.setSingletonPrinterErrorCode("Other issues");
        		}
        		DialogView.hideDialog();
   				DialogView.showReportDialog(this,MainConstants.printerIssue,MainConstants.reportStatusPrinterFailure);
   		     }
        	else
        	{
        		//Log.d("printer","statement3");
            	
        	}
      
        	
       	/*
        	Intent printIntent = new Intent(Intent.ACTION_SEND);
            printIntent.setType("image/*");
            printIntent.putExtra(Intent.EXTRA_TITLE,
                    "some cool title for your document");
           Uri uri = Uri.parse("/sdcard/FrontDesk/Con1.jpg");
           PackageManager pm = getApplicationContext().getPackageManager();
 		   List<ResolveInfo> activityList = pm.queryIntentActivities(printIntent, 0);
 		     
 		   for (final ResolveInfo app : activityList) 
 		     {
 			   Log.d("app ","package name"+app.activityInfo.name);
 		    	 if ((app.activityInfo.name).contains("com.brother.ptouch.iprintandlabel")) 
 		           {
 		             final ActivityInfo activity = app.activityInfo;
 		             final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
 		             printIntent.addCategory(Intent.CATEGORY_LAUNCHER);
 		             printIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
 		             printIntent.setComponent(name);
 		             getApplicationContext().startActivity(printIntent);
 		             break;
 		           }
 		      }
           // startActivity(printIntent);*/

	}
 	
 	
 	/**
	 * Goto home page when no action.
	 * 
	 * @author karthick
	 * @created 16/06/2015
	 * 
	 */
	public Runnable noActionThread = new Runnable() {
		public void run() {
			//Log.d("visitor", "noActionThread");
			if(((editTextTempIdNumber==null)||(editTextTempIdNumber.getText().toString()=="")
					||(editTextTempIdNumber.getText().toString().length()==0))&&((textGetEmployeeName==null)||(textGetEmployeeName.getText().toString()=="")
							||(textGetEmployeeName.getText().toString().length()==0))){
			getHomePage();
			}
			
		}
	};
	
	/*
	 * Created by karthick On 28/04/2015 intent to launch the home page.
	 */
	private void getHomePage() {

		

		Intent intent = new Intent(getApplicationContext(),
				WelcomeActivity.class);
		startActivity(intent);
		
		noActionHandler.removeCallbacks(noActionThread);
		finish();
		
		// this.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
		// this.overridePendingTransition(R.anim.animation_leave,
		// R.anim.animation_enter);
	}

	
}
