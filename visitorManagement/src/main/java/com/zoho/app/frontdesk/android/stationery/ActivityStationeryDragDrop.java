package com.zoho.app.frontdesk.android.stationery;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.BuildConfig;

import appschema.SingletonStationeryDetails;
import constants.NumberConstants;
import constants.StringConstants;
import utility.JSONParser;

import com.zoho.app.frontdesk.android.R;

import appschema.SingletonEmployeeProfile;

import appschema.SingletonVisitorProfile;
import com.zoho.app.frontdesk.android.WelcomeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import constants.DataBaseConstants;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import database.dbschema.Stationery;
import database.dbschema.StationeryEntry;
import appschema.StationeryRequest;
import constants.TextToSpeachConstants;
import database.dbhelper.DbHelper;
import logs.InternalAppLogs;
import utility.DialogView;
import utility.NetworkConnection;
import appschema.SharedPreferenceController;
import utility.TimeZoneConventer;
import utility.UsedCustomToast;
import zohocreator.VisitorZohoCreator;

/**
 * Created by Karthikeyan D S on 2018/05/26
 */
public class ActivityStationeryDragDrop extends Activity {

    private List<Item> allItemListStationery, selectedItemListStationery;
    private ListView listViewAllItems, listViewSelectedItems;
    private ItemListAdapter allItemListAdapter, selectedItemListAdapter;
    private LinearLayoutAbsListView linearLayoutAllItemsPane, linearLayoutSelectedItemsPane;
    private Button buttonStationerySubmit, buttonStationeryCancel;
    private VisitorZohoCreator creator;
    private DbHelper dbHelper;
    private Handler handlerHideDialog;
    private long currentTime = 0L;
    private Dialog dialogGetHomeMessage, reportDialog;
    private TextView textViewDialogHomeMessage, textViewHomeMessageConfirm,
            textViewHomeMessageCancel, textViewReportMsg,
            textViewReportFirstMsg, textViewReportErrorCode, textViewAllItemStationeryHeader,
            textViewSelectedItemStationeryHeader, textViewEmployeeName, textViewNoItemsLogAllStationery, textViewNoItemsLogSelectedStationery;
    private Typeface fontPoppinsBold, fontPoppinsMedium, fontPoppinsSemiBold;
    private Button buttonSubmit, buttonCancel, tellUs;
    private ProgressBar progressBar;
    private RelativeLayout relativeLayout;
    private View seperator;
    private ImageView imageViewBack, reportImage;
    private int chooseOption = NumberConstants.kConstantZero;
    private Boolean IsallowedToClickStationaryItems = true;
    private View dragDropViewItem = null;
    private HashMap<Integer, StationeryEntry> selectedStationeryListFinal;
    private SharedPreferenceController spController ;
    String mdevicelocation = "";
    public HashMap<String, Integer> stationeryDrawableList;
    private Handler noActionHandler;
    private Context context;
    private TextToSpeech textToSpeech;
    private Boolean backpressEnabled = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_drag_drop_list_view);

//        if (android.os.Build.VERSION.SDK_INT > 9) {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                    .permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//        }
//         Set the orientation as Portrait
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
//             this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        }

        assignObjects();

        if ((SingletonEmployeeProfile.getInstance().getEmployeeName() != null)
                && (!SingletonEmployeeProfile.getInstance().getEmployeeName().equals(""))) {
            String[] name = SingletonEmployeeProfile.getInstance()
                    .getEmployeeName().split(" ");
            if (TimeZoneConventer.showHappyGreeting() != "") {
                textViewEmployeeName.setText(TimeZoneConventer.showHappyGreeting() + name[0]);
            } else {
                textViewEmployeeName
                        .setText(StringConstants.kConstantGreetingMorning + name[0]);
            }
        } else {
            getHomePage();
        }

    }

    /**
     * Assign Objects
     *
     * @author Karthikeyan D S
     * @created on 20180526
     */
    private void assignObjects() {
        allItemListStationery = new ArrayList<Item>();
        selectedItemListStationery = new ArrayList<Item>();
        dbHelper = new DbHelper(this);
        creator = new VisitorZohoCreator(this);
        handlerHideDialog = new Handler();
        context = this;
        stationeryDrawableList = new HashMap<String, Integer>();

        handlerHideDialog = new Handler();
        noActionHandler = new Handler();

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        selectedStationeryListFinal = new HashMap<Integer, StationeryEntry>();

        spController = new SharedPreferenceController();
        mdevicelocation = spController.getDeviceLocation(getApplicationContext());

//       noActionHandler.postDelayed(noActionThread, MainConstants.noFaceTime);

        listViewAllItems = (ListView) findViewById(R.id.list_view_all_items);
        listViewSelectedItems = (ListView) findViewById(R.id.list_view_selected_item);

        linearLayoutAllItemsPane = (LinearLayoutAbsListView) findViewById(R.id.linear_layout_all_items_pane);
        linearLayoutSelectedItemsPane = (LinearLayoutAbsListView) findViewById(R.id.linear_layout_selected_items_pane);

        buttonStationerySubmit = (Button) findViewById(R.id.button_stationery_submit);
        buttonStationeryCancel = (Button) findViewById(R.id.button_stationery_cancel);

        textViewSelectedItemStationeryHeader = (TextView) findViewById(R.id.stationery_list_header);
        textViewAllItemStationeryHeader = (TextView) findViewById(R.id.text_view_selected_stationery_header);
        textViewEmployeeName = (TextView) findViewById(R.id.text_view_employee_name_header);
        textViewNoItemsLogAllStationery = (TextView) findViewById(R.id.text_view_all_items_no);
        textViewNoItemsLogSelectedStationery = (TextView) findViewById(R.id.text_view_selected_items_no);

        linearLayoutAllItemsPane.setOnDragListener(myOnDragListener);
        linearLayoutSelectedItemsPane.setOnDragListener(myOnDragListener);

        linearLayoutAllItemsPane.setAbsListView(listViewAllItems);
        linearLayoutSelectedItemsPane.setAbsListView(listViewSelectedItems);

        linearLayoutAllItemsPane.setAbsListView(listViewAllItems);
        linearLayoutSelectedItemsPane.setAbsListView(listViewSelectedItems);

//        load items in view with delay
        noActionHandler.postDelayed(mDragItemsHandlerThread, MainConstants.dragViewLoadTime);

        allItemListAdapter = new ItemListAdapter(this, allItemListStationery);
        selectedItemListAdapter = new ItemListAdapter(this, selectedItemListStationery);

        listViewAllItems.setAdapter(allItemListAdapter);
        listViewSelectedItems.setAdapter(selectedItemListAdapter);

//        listViewAllItems.setOnItemClickListener(listOnItemClickListener);
//        listViewSelectedItems.setOnItemClickListener(listOnItemClickListener);

        if (listViewAllItems != null && myOnItemLongClickListener != null) {
            listViewAllItems.setOnItemLongClickListener(myOnItemLongClickListener);
        }
        if (listViewSelectedItems != null && myOnItemLongClickListener != null) {
            listViewSelectedItems.setOnItemLongClickListener(myOnItemLongClickListener);
        }

        if (buttonStationeryCancel != null && buttonCancelClickListener != null) {
            buttonStationeryCancel.setOnClickListener(buttonCancelClickListener);
        }
        if (buttonStationerySubmit != null && buttonSubmitClickListener != null) {
            buttonStationerySubmit.setOnClickListener(buttonSubmitClickListener);
        }

        if(mdevicelocation!=null && !mdevicelocation.isEmpty() && !mdevicelocation.equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix))
        {
            if(buttonStationerySubmit!=null) {
                buttonStationerySubmit.setText("SUBMIT");
            }
        }

        assignFontConstants();
    }

    /**
     * Assign Font Style
     *
     * @author Karthikeyan D S
     * @created on 20180526
     */
    private void assignFontConstants() {
        fontPoppinsBold = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsBold);
        fontPoppinsMedium = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsMedium);
        fontPoppinsSemiBold = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsSemiBold);

        buttonStationeryCancel.setTypeface(fontPoppinsSemiBold);
        buttonStationerySubmit.setTypeface(fontPoppinsSemiBold);

        textViewAllItemStationeryHeader.setTypeface(fontPoppinsSemiBold);
        textViewSelectedItemStationeryHeader.setTypeface(fontPoppinsSemiBold);
        textViewEmployeeName.setTypeface(fontPoppinsSemiBold);
        textViewNoItemsLogAllStationery.setTypeface(fontPoppinsSemiBold);
        textViewNoItemsLogSelectedStationery.setTypeface(fontPoppinsSemiBold);
    }


    /**
     * initialize Drag and drop view images and items
     *
     * @author Karthikeyan D S
     * @created on 20180526
     */
    private void initItems() {
//        assign drawable image.
        putImageDrawable();

        String stationeryName = MainConstants.kConstantEmpty;

//        Based On the Device Location the Stationery list will fetched from DB and add to ArrayList.
        int id;
        ArrayList<Stationery> stationeryDetailsfromDB = new ArrayList<Stationery>();

        if (SingletonStationeryDetails.getStationeryInstance() != null &&
                SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails().isEmpty()) {
            if (mdevicelocation != null && !mdevicelocation.isEmpty()) {
                if (dbHelper == null) {
                    dbHelper = new DbHelper(getApplicationContext());
                }
                if (dbHelper != null) {
                    stationeryDetailsfromDB = dbHelper.getAllStationeryBasedOnDeviceLocation(mdevicelocation);
                    if (stationeryDetailsfromDB != null) {
                        for (int stationeryCount = 0; stationeryDetailsfromDB != null && stationeryCount < stationeryDetailsfromDB.size(); stationeryCount++) {
                            if (stationeryDetailsfromDB.get(stationeryCount).getStationeryName() != null) {
                                stationeryName = stationeryDetailsfromDB.get(stationeryCount).getStationeryName();
//                            get image from drawable to load in list view
                                Drawable itemImage = null;
                                if (stationeryDrawableList != null && stationeryName != null && stationeryDrawableList.get(stationeryName) != null) {
                                    id = stationeryDrawableList.get(stationeryDetailsfromDB.get(stationeryCount).getStationeryName());
                                    if (id > 0) {
                                        itemImage = getDrawable(id);
                                    }
                                }
                                int stationeryItemCode = stationeryDetailsfromDB.get(stationeryCount).getStationeryCode();
                                Item item = new Item(itemImage, stationeryName, stationeryItemCode);
                                if(item!=null)
                                {
                                    allItemListStationery.add(item);
                                }
                            }
                        }
                    } else {
                        textViewNoItemsLogAllStationery.setVisibility(View.VISIBLE);
                        textViewNoItemsLogAllStationery.setText("No Items in the List!");
                    }
                }
            }
//         Show And Hide Fields based on the Activity.
           assignView(stationeryDetailsfromDB);

            if (allItemListAdapter == null) {
                allItemListAdapter = new ItemListAdapter(getApplicationContext(), allItemListStationery);
                listViewAllItems.setAdapter(allItemListAdapter);
            } else {
                allItemListAdapter.notifyDataSetChanged();
            }
            if (selectedItemListAdapter != null) {
                selectedItemListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * Assign View in the Activity.
     *
     * @author Karthikeyan D S
     * @created on 20180526
     */
    public void assignView(ArrayList<Stationery> stationeryDetailsfromDB )
    {
        if (stationeryDetailsfromDB != null && stationeryDetailsfromDB.size() > 0 && allItemListStationery.size() > 0) {
//            Log.d("DSK","Not NUll List");
            textViewNoItemsLogAllStationery.setVisibility(View.GONE);
            textViewNoItemsLogSelectedStationery.setVisibility(View.VISIBLE);
            textViewSelectedItemStationeryHeader.setVisibility(View.VISIBLE);
            listViewSelectedItems.setVisibility(View.VISIBLE);
            buttonStationerySubmit.setVisibility(View.GONE);
            buttonStationeryCancel.setText(StringConstants.kConstantDialogBack);
            buttonStationeryCancel.setVisibility(View.VISIBLE);

        } else {
//            Log.d("DSK","Null List");
            textViewNoItemsLogAllStationery.setVisibility(View.VISIBLE);
            textViewNoItemsLogAllStationery.setText("Item List is Empty");
            textViewNoItemsLogSelectedStationery.setVisibility(View.VISIBLE);
            textViewSelectedItemStationeryHeader.setVisibility(View.VISIBLE);
            buttonStationerySubmit.setVisibility(View.GONE);
            buttonStationeryCancel.setText(StringConstants.kConstantDialogBack);
            buttonStationeryCancel.setVisibility(View.VISIBLE);

        }

    }
    /**
     * Put image to drawable list
     *
     * @author Karthikeyan D S
     * @created on 20180526
     */
    public void putImageDrawable() {
        stationeryDrawableList.put("Pen", R.drawable.pen);
        stationeryDrawableList.put("Pen - Blue", R.drawable.pen);
        stationeryDrawableList.put("Pen - Black", R.drawable.pen);
        stationeryDrawableList.put("Pen - Red", R.drawable.pen);
        stationeryDrawableList.put("Binder Clips", R.drawable.binderclip);
        stationeryDrawableList.put("Bell Clips", R.drawable.binderclip);
        stationeryDrawableList.put("Board Pins", R.drawable.boardclip);
        stationeryDrawableList.put("clipboard", R.drawable.clipboard);
        stationeryDrawableList.put("Eraser", R.drawable.eraser);
        stationeryDrawableList.put("Rubber", R.drawable.eraser);
        stationeryDrawableList.put("File", R.drawable.file);
        stationeryDrawableList.put("System Screen Cleaning Liquid", R.drawable.liquid);
        stationeryDrawableList.put("Magnet", R.drawable.magnet);
        stationeryDrawableList.put("Long Size Note Book", R.drawable.notebook);
        stationeryDrawableList.put("Small Size Note Book", R.drawable.notebook);
        stationeryDrawableList.put("Notepad", R.drawable.notepad);
        stationeryDrawableList.put("Notepad A4 Size", R.drawable.notepad);
        stationeryDrawableList.put("Notepad Small Size", R.drawable.notepad);
        stationeryDrawableList.put("Long size note", R.drawable.notepad);
        stationeryDrawableList.put("Sticky Notes", R.drawable.stickynotes);
        stationeryDrawableList.put("Pencil", R.drawable.pencil);
        stationeryDrawableList.put("Sketch", R.drawable.markers);
        stationeryDrawableList.put("Sketch Pen", R.drawable.markers);
        stationeryDrawableList.put("Marker", R.drawable.markers);
        stationeryDrawableList.put("Marker - Blue", R.drawable.markers);
        stationeryDrawableList.put("Marker - Black", R.drawable.markers);
        stationeryDrawableList.put("Marker - Green", R.drawable.markers);
        stationeryDrawableList.put("Whitener", R.drawable.markers);
        stationeryDrawableList.put("stapler", R.drawable.markers);
        stationeryDrawableList.put("Staples - Small Size", R.drawable.stapler);
        stationeryDrawableList.put("Envelope", R.drawable.envelope);
        stationeryDrawableList.put("Sharpener", R.drawable.sharpener);
        stationeryDrawableList.put("A3 Cover", R.drawable.cover);
        stationeryDrawableList.put("A4 Cover", R.drawable.cover);
        stationeryDrawableList.put("Office cover - Green", R.drawable.cover);
        stationeryDrawableList.put("Legal Size Courier Cover", R.drawable.cover);
        stationeryDrawableList.put("Office Cover - Brown", R.drawable.cover);
        stationeryDrawableList.put("Green cover", R.drawable.cover);
        stationeryDrawableList.put("Staples - Big Size", R.drawable.stapler);
        stationeryDrawableList.put("Duster", R.drawable.whiteboardduster);
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created 05JUL2018
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            // Log.d("list", "noActionThread");
            getHomePage();
        }
    };

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created 05JUL2018
     */
    public void onCheckSingletonDataAvailable() {
        Map<Long, Integer> selectedItemListSingleton = new HashMap<Long, Integer>();
        if (SingletonStationeryDetails.getStationeryInstance() != null &&
                SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails() != null) {
            selectedItemListSingleton = SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails();
        }

        if (selectedItemListSingleton != null && selectedItemListSingleton.size() > 0) {
            ArrayList<Integer> selectedItemStationerycode = new ArrayList<Integer>();
            Stationery stationeryDetails = new Stationery();
            if (dbHelper == null) {
                dbHelper = new DbHelper(this);
            }
            if (selectedItemListSingleton != null && selectedItemListSingleton.size() > 0) {

                List<Integer> listofCodes = new ArrayList<Integer>();
                for (Map.Entry<Long, Integer> entry : selectedItemListSingleton.entrySet()) {
                    long stationeryRecordId = entry.getKey();
                    int stationeryCode = entry.getValue();
                    if (stationeryCode >= 0) {
                        listofCodes.add(stationeryCode);
                    }
                }
                if (listofCodes != null && !listofCodes.isEmpty() && listofCodes.size() > 0) {
                    selectedItemStationerycode.addAll(listofCodes);
                } else {
//                    Log.d("DSK ","selectedItemStationerysize "+selectedItemStationerycode.size());
                }
            }
            if (stationeryDrawableList != null && stationeryDrawableList.size() > 0) {
                storeDatatoList(selectedItemStationerycode);
            } else {
                putImageDrawable();
                if (stationeryDrawableList != null && stationeryDrawableList.size() > 0) {
                    storeDatatoList(selectedItemStationerycode);
                }
            }
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * Store Data to Array list from singleton and db
     *
     * @author Karthikeyan D S
     * @created on 05JUL2018
     */
    private void storeDatatoList(ArrayList<Integer> selectedItemStationerycode) {
        String stationeryName = MainConstants.kConstantEmpty;
        int id;
        ArrayList<Stationery> stationeryDetailsfromDBSelected = new ArrayList<Stationery>();
        if (selectedItemStationerycode != null && selectedItemStationerycode.size() > 0) {
            if (dbHelper == null) {
                dbHelper = new DbHelper(this);
            }
            stationeryDetailsfromDBSelected = dbHelper.getAllStationeryBasedOnDeviceLocation(mdevicelocation);
            if (stationeryDetailsfromDBSelected != null) {
                if (selectedItemListStationery != null && selectedItemListStationery.size() > 0) {
                    selectedItemListStationery.clear();
                }
                if (allItemListStationery != null && allItemListStationery.size() > 0) {
                    allItemListStationery.clear();
                }
                for (int stationeryCount = 0; stationeryDetailsfromDBSelected != null && stationeryCount < stationeryDetailsfromDBSelected.size(); stationeryCount++) {
                    if (stationeryDetailsfromDBSelected.get(stationeryCount).getStationeryName() != null) {
                        stationeryName = stationeryDetailsfromDBSelected.get(stationeryCount).getStationeryName();
//                            get image from drawable to load in list view
                        Drawable itemImage = null;
                        if (stationeryDrawableList != null && stationeryName != null && stationeryDrawableList.get(stationeryName) != null) {
                            id = stationeryDrawableList.get(stationeryDetailsfromDBSelected.get(stationeryCount).getStationeryName());
                            if (id > 0) {
                                itemImage = getDrawable(id);
                            }
                        }
                        int stationeryItemCode = stationeryDetailsfromDBSelected.get(stationeryCount).getStationeryCode();
                        Item item = new Item(itemImage, stationeryName, stationeryItemCode);
//                        Log.d("DSK", "Contains " + selectedItemStationerycode.contains(stationeryItemCode) + " " + stationeryItemCode);

                        if (selectedItemStationerycode.contains(stationeryItemCode)) {
//                            Log.d("DSK", "if " + stationeryItemCode);
                            selectedItemListStationery.add(item);
                        } else {
//                            Log.d("DSK", "else " + stationeryItemCode);
                            allItemListStationery.add(item);
                        }
                    }
                }
            }
        }
        if (allItemListAdapter == null) {
            allItemListAdapter = new ItemListAdapter(getApplicationContext(), allItemListStationery);
            listViewAllItems.setAdapter(allItemListAdapter);
        } else {
            allItemListAdapter.notifyDataSetChanged();
        }
        if (selectedItemListAdapter != null) {
            selectedItemListAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {
        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne) {
                textToSpeech.speak(TextToSpeachConstants.speachDragDropItem,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthikeyan D S
     * @created on 20180523
     */
    private void showDetailDialog() {
        intializeDialogBoxForHomePage();
        textSpeach(MainConstants.speachAreYouSureCode);
    }

    /**
     * initialize dialog box for goto Home Page
     *
     * @author Karthikeyan D S
     * @created on 20180523
     */
    private void intializeDialogBoxForHomePage() {
        dialogGetHomeMessage = new Dialog(this);
        dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGetHomeMessage
                .setContentView(R.layout.dialog_box_confirm_message);
        // dialog.(MessageConstant.dialogTitle);
        dialogGetHomeMessage.setCanceledOnTouchOutside(true);
        dialogGetHomeMessage.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogGetHomeMessage.setOnDismissListener(setOnDismissListener);
        textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message);
        textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message_confirm);
        textViewHomeMessageCancel = (TextView) dialogGetHomeMessage
                .findViewById(R.id.text_view_error_message_cancel);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.height = 200;
        textViewDialogHomeMessage.setLayoutParams(params);
        textViewDialogHomeMessage.setTextColor(Color.rgb(102, 102, 102));
        textViewDialogHomeMessage.setText(StringConstants.kConstantLostData);
        textViewDialogHomeMessage.setTypeface(fontPoppinsSemiBold);
        textViewHomeMessageCancel.setTypeface(fontPoppinsSemiBold);
        textViewHomeMessageConfirm.setTypeface(fontPoppinsSemiBold);
        textViewHomeMessageConfirm
                .setText(StringConstants.kConstantDialogConfirm);
        textViewHomeMessageCancel.setText(StringConstants.kConstantDialogCancel);
        // textViewHomeMessageConfirm.setText(MainConstants.kConstantOka);
        textViewHomeMessageConfirm.setOnClickListener(discardConfirm);
        textViewHomeMessageCancel.setOnClickListener(discardCancel);

    }

    /**
     * Thread Handler to load item card After Activity Loaded.
     *
     * @author Karthikeyan D S
     * @created on 20180523
     */
    private Thread mDragItemsHandlerThread =
            new Thread(new Runnable() {
                @Override
                public void run() {
//              Log.d("DSK","LOAD TIME Thread");
//              call init items.
                    initItems();
                    onCheckSingletonDataAvailable();
                }
            });

    /**
     * Drag and Drop Item Card Long Click Listener.
     *
     * @author Karthikeyan D S
     * @created on 20180523
     */
    AdapterView.OnItemLongClickListener myOnItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                                       int position, long id) {
            Item selectedItem = (Item) (parent.getItemAtPosition(position));

            ItemBaseAdapter associatedAdapter = (ItemBaseAdapter) (parent.getAdapter());
            List<Item> associatedList = associatedAdapter.getList();

            PassObject passObj = new PassObject(view, selectedItem, associatedList);

            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, passObj, 0);

            AbsListView oldParent = (AbsListView) view.getParent();
            ItemBaseAdapter srcAdapter = (ItemBaseAdapter) (oldParent.getAdapter());

//            LinearLayoutAbsListView newParent = (LinearLayoutAbsListView) view;
//            ItemBaseAdapter destAdapter = (ItemBaseAdapter) (newParent.absListView.getAdapter());
//            List<Item> destList = destAdapter.getList();

//            Log.d("DSK3","listener oldParent "+oldParent.toString());
//            Log.d("DSK3","listener newParent "+newParent.toString());

            if ((oldParent.getId() == R.id.list_view_selected_item) ||
                    (oldParent.getId() == R.id.list_view_all_items) ||
                    (oldParent.getId() == R.id.linear_layout_selected_items_pane) ||
                    (oldParent.getId() == R.id.linear_layout_all_items_pane)) {
//                Log.d("DSK3","listener oldParent "+oldParent.toString());
//                Log.d("DSK3","listener newParent "+newParent.toString());
                view.setVisibility(View.INVISIBLE);
                dragDropViewItem = view;
            } else {
//                Log.d("DSK4","listener oldParent "+oldParent.toString());
//                Log.d("DSK4","listener newParent "+newParent.toString());
                view.setVisibility(View.VISIBLE);
                dragDropViewItem = view;
            }
            return true;
        }

    };


    /**
     * Drag and Drop Item Drag Listener.
     *
     * @author Karthikeyan D S
     * @created on 20180523
     */
    View.OnDragListener myOnDragListener = new View.OnDragListener() {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
//                    Log.d("DSK1","DRAG_STARTED "+v.toString());
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
//                    Log.d("DSK1","DRAG_ENTERED");
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
//                    Log.d("DSK1","DRAG_EXITED");
                    break;
                case DragEvent.ACTION_DROP:
//                    Log.d("DSK1","DROP");
                    PassObject passObj = (PassObject) event.getLocalState();
                    View view = passObj.view;
                    Item passedItem = passObj.item;
                    List<Item> srcList = passObj.srcList;
                    AbsListView oldParent = (AbsListView) view.getParent();
                    ItemBaseAdapter srcAdapter = (ItemBaseAdapter) (oldParent.getAdapter());

                    LinearLayoutAbsListView newParent = (LinearLayoutAbsListView) v;
                    ItemBaseAdapter destAdapter = (ItemBaseAdapter) (newParent.absListView.getAdapter());
                    List<Item> destList = destAdapter.getList();

//                    Log.d("DSK1","oldParent "+oldParent.toString());
//                    Log.d("DSK1","newParent "+newParent.toString());

                    if ((oldParent.getId() == R.id.list_view_all_items && newParent.getId() == R.id.linear_layout_selected_items_pane) ||
                            (oldParent.getId() == R.id.list_view_selected_item && newParent.getId() == R.id.linear_layout_all_items_pane) ||
                            (oldParent.getId() == R.id.list_view_selected_item && newParent.getId() == R.id.list_view_all_items) ||
                            (oldParent.getId() == R.id.list_view_all_items && newParent.getId() == R.id.list_view_selected_item)) {
//                        Log.d("DSK1","oldParent "+oldParent.toString());
//                        Log.d("DSK1","newParent "+newParent.toString());
                        view.setVisibility(View.VISIBLE);
                        textViewNoItemsLogSelectedStationery.setVisibility(View.GONE);
                        textViewSelectedItemStationeryHeader.setVisibility(View.VISIBLE);
                        buttonStationerySubmit.setVisibility(View.VISIBLE);
//                        buttonStationeryCancel.setVisibility(View.VISIBLE);
                        buttonStationeryCancel.setText(StringConstants.kConstantDialogCancel);

                        if (removeItemToList(srcList, passedItem)) {
                            addItemToList(destList, passedItem);
                        }

                        srcAdapter.notifyDataSetChanged();
                        destAdapter.notifyDataSetChanged();

                        if (selectedItemListStationery != null && selectedItemListStationery.size() > 0) {
                            textViewSelectedItemStationeryHeader.setVisibility(View.VISIBLE);
                            buttonStationerySubmit.setVisibility(View.VISIBLE);
                            buttonStationeryCancel.setText(StringConstants.kConstantDialogCancel);
//                            buttonStationeryCancel.setVisibility(View.VISIBLE);
                        } else {
                            textViewNoItemsLogSelectedStationery.setVisibility(View.VISIBLE);
                            buttonStationerySubmit.setVisibility(View.GONE);
                            buttonStationeryCancel.setText(StringConstants.kConstantDialogBack);
//                            buttonStationeryCancel.setVisibility(View.GONE);
                        }
                        if (allItemListStationery == null || allItemListStationery.size() == 0) {
                            textViewNoItemsLogAllStationery.setVisibility(View.VISIBLE);
                            textViewNoItemsLogAllStationery.setText(StringConstants.kConstantDialogHintMsgAllItemsAdded);
                        } else if (allItemListStationery != null || allItemListStationery.size() > 0) {
                            textViewNoItemsLogAllStationery.setVisibility(View.GONE);
                        }
                    } else {
                        if (dragDropViewItem != null) {
                            dragDropViewItem.setVisibility(View.VISIBLE);
                        }
                    }
                    noActionHandler.removeCallbacks(noActionThread);
                    noActionHandler.postDelayed(noActionThread, MainConstants.noFaceDragDropTime);
//                    smooth scroll to bottom
//                    newParent.absListView.smoothScrollToPosition(destAdapter.getCount() - 1);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    if (dragDropViewItem != null) {
                        dragDropViewItem.setVisibility(View.VISIBLE);
                    }
                default:
                    break;
            }
            return true;
        }
    };

    /**
     * Dialog dismiss Listener for reinitialize list
     *
     * @author karthick
     * @created on 20150618
     */
    DialogInterface.OnDismissListener setOnDismissListener = new DialogInterface.OnDismissListener() {
        public void onDismiss(DialogInterface dialog) {
//            chooseOption = 0;
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTime);
            }
            /*
             * if (employeeID.length() <= NumberConstants.kConstantZero) {
             * checkboxNotifyMe.setChecked(false); } if (isNotifyDialogShow) {
             * isNotifyDialogShow = false; } else {
             * initializeGridAdapter(false); }
             */
        }
    };

    /**
     * Dialog submit Listener for reinitialize Selected item List
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    private View.OnClickListener buttonSubmitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            disableProcess();
            if (mdevicelocation != null && !mdevicelocation.isEmpty()) {
                if (mdevicelocation.equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
                    if (validateDetails()) {
                        callQuantityPageActivity();
                    } else {
                        UsedCustomToast.makeCustomToast(context, "Please Drag and Drop the Items", Toast.LENGTH_SHORT, Gravity.TOP).show();
                    }
                } else if (!mdevicelocation.equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
                    if (isValidate()) {
                        getSelectedItemList();
                    } else {
                        UsedCustomToast.makeCustomToast(context, "Please Drag and Drop the Items", Toast.LENGTH_SHORT, Gravity.TOP).show();
                    }
                }
            }
            else {
                UsedCustomToast.makeCustomToast(context, "Device Location is Empty! Select Device Location", Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        }
    };


    /**
     * Call NextPage Activity
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private void callQuantityPageActivity() {
        Intent callNextPage = new Intent(this, ActivityStationeryDragDropQuantity.class);
        startActivity(callNextPage);
        finish();
    }

    /**
     * Dialog Confirm Listener for confirm button where new item Selected
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private View.OnClickListener discardConfirm = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            if ((dialogGetHomeMessage != null)) {
                dialogGetHomeMessage.dismiss();
            }
            Intent intent = new Intent(getApplicationContext(),
                    WelcomeActivity.class);
            startActivity(intent);

            if (SingletonStationeryDetails.getStationeryInstance() != null) {
                SingletonStationeryDetails.getStationeryInstance().clearInstance();
            }

            noActionHandler.removeCallbacks(noActionThread);
            finish();
        }
    };
    /**
     * Click the errorMessageCancel to
     * dismiss the dialogBox
     */
    private View.OnClickListener discardCancel = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if ((dialogGetHomeMessage != null)
                    && (dialogGetHomeMessage.isShowing())) {
                dialogGetHomeMessage.dismiss();
            }
        }
    };
    /**
     * Button Cancel Listener
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    private View.OnClickListener buttonCancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            Log.d("DskSize",""+selectedItemListStationery.size());
            if (selectedItemListStationery.size() > 0) {
//                Log.d("DskSize",""+selectedItemListStationery.size());
//                intializeDialogBoxForHomePage();
                showDetailDialog();
                showHomeDialogBox();
            } else {
                getHomePage();
            }
        }
    };

    /**
     * intent to launch the home page.
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    private void getHomePage() {
        Intent intent = new Intent(getApplicationContext(),
                WelcomeActivity.class);
        startActivity(intent);
//        noActionHandler.removeCallbacks(noActionThread);
        DialogView.hideDialog();
        finish();
    }

    /**
     * Validate and Store Details for selected Stationery List
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private boolean validateDetails() {
        if (selectedItemListStationery != null && selectedItemListStationery.size() > 0) {

            Map<Long, Integer> selectedDetailsMap = new HashMap<Long, Integer>();
            Stationery stationeryDetails = new Stationery();
            for (int selectedItemListStationeryCount = 0; selectedItemListStationeryCount < selectedItemListStationery.size(); selectedItemListStationeryCount++) {
//              Log.d("selectedItemLis"+i,""+selectedItemListStationery.get(i).stationeryCode);
//              addSelectedItemInHashMap(selectedItemListStationeryCount, selectedItemListStationery.get(selectedItemListStationeryCount).stationeryCode);
                stationeryDetails = dbHelper.getAllStationeryBasedOnStationeryCode(selectedItemListStationery.get(selectedItemListStationeryCount).stationeryCode, mdevicelocation);
//                Log.d("DSK","selectedStationeryMap "+stationeryDetails.getRecordId() + " Code "+selectedItemListStationery.get(selectedItemListStationeryCount).stationeryCode);
                selectedDetailsMap.put(stationeryDetails.getRecordId(), selectedItemListStationery.get(selectedItemListStationeryCount).stationeryCode);
            }
            SingletonStationeryDetails.getStationeryInstance().setSelectedStationeryDetails(selectedDetailsMap);
//            Log.d("DSK","selectedStationeryMapsize "+selectedDetailsMap.size());
//            Log.d("DSK","selectedStationeryCodesize1 "+SingletonStationeryDetails.getStationeryInstance().getSelectedStationeryDetails().size());
            return true;
        } else {
            return false;
        }
    }

    /**
     * remove items from Selected items List while drag and drop item
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private boolean removeItemToList(List<Item> listItem, Item it) {
        boolean result = false;
        Iterator lit = listItem.iterator();
        if (lit.hasNext()) {
            lit.next();
            if (listItem.contains(it)) {
                result = listItem.remove(it);
                return result;
            }
        }

        return result;
        }

    /**
     * add items to Selected items List while drag and drop item
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private boolean addItemToList(List<Item> l, Item it) {
        boolean result = false;
        Iterator lit = l.iterator();
        if (l.contains(it)) {
            l.remove(it);
            result = l.add(it);
            return result;
        } else {
            result = l.add(it);
            return result;
        }
        }

    /**
     * show dialog box
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private void showHomeDialogBox() {
        if ((dialogGetHomeMessage != null)
                && (!dialogGetHomeMessage.isShowing())) {
            dialogGetHomeMessage.show();
        }
    }

    // method to Enable , disable the view - means it is not allowed to call the
    // listener
    private void setEnableViews(Boolean isEnable) {
        if (isEnable != null) {
            buttonStationerySubmit.setEnabled(isEnable);
            buttonStationeryCancel.setEnabled(isEnable);
        }
    }

    /**
     * enable process after submit the  data
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private void enableProcess() {
        buttonStationeryCancel.setEnabled(true);
        buttonStationerySubmit.setEnabled(true);
        backpressEnabled = true;
        if (listViewAllItems != null) {
            listViewAllItems.setEnabled(true);
        }
    }

    /**
     * disable process while submit the  data
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private void disableProcess() {
        buttonStationeryCancel.setEnabled(false);
        buttonStationerySubmit.setEnabled(false);
        backpressEnabled = false;
        if (listViewAllItems != null) {
            listViewAllItems.setEnabled(false);
        }
        if (listViewSelectedItems != null) {
            listViewSelectedItems.setEnabled(false);
        }
    }

    public void onStart() {
        super.onStart();
//        onCheckSingletonDataAvailable();
    }

    /**
     * on pause
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    public void onPause() {
        super.onPause();
        // Log.d("onPause", "onPause");
        noActionHandler.removeCallbacks(noActionThread);
    }

    /**
     * onResume()
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    public void onResume() {
        super.onResume();
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTime);
        }
//        allItemListAdapter.notifyDataSetChanged();
//        selectedItemListAdapter.notifyDataSetChanged();
    }

    /**
     * on destroy
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    protected void onDestroy() {
        super.onDestroy();
//         Log.d("on","destroy");
//         mMemoryCache.evictAll();
        deallocateObjects();
        noActionHandler.removeCallbacks(noActionThread);
        DialogView.hideDialog();
        DialogView.setNullDialog();
    }

    /**
     * on BackPress Enabled
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (backpressEnabled) {
            if (selectedItemListStationery.size() > 0) {
//            intializeDialogBoxForHomePage();
                showDetailDialog();
                showHomeDialogBox();
            } else {
                getHomePage();
            }
        }
    }

    /**
     * Deallocate the object access
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    private void deallocateObjects() {
        // creator =null;
        listViewAllItems = null;
        listViewSelectedItems = null;
        buttonStationerySubmit = null;
        buttonStationeryCancel = null;
        selectedStationeryListFinal = null;
        stationeryDrawableList = null;
        allItemListStationery = null;
        selectedItemListStationery = null;
        dbHelper = null;
        fontPoppinsSemiBold = null;
        fontPoppinsMedium = null;
        fontPoppinsBold = null;
        buttonSubmit = null;
        buttonCancel = null;
        handlerHideDialog = null;
        context = null;
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        dragDropViewItem = null;
    }

    /**
     * Drag and Drop List View Adapter Class View Holder
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    static class ViewHolder {
        ImageView itemIcon;
        TextView itemName;
        int itemCode;
        int itemCreatorID;
    }

    //    class to load and store items
    public class Item {
        private Drawable itemImage;
        private String stationeryName;
        private int stationeryCode;
        //private String stationeryImage;

        Item(Drawable drawable, String t, int itemstationeryCode) {
            itemImage = drawable;
            stationeryName = t;
            stationeryCode = itemstationeryCode;
        }
    }

    //    objects passed in Drag and Drop operation
    class PassObject {
        View view;
        Item item;
        List<Item> srcList;

        PassObject(View v, Item i, List<Item> s) {
            view = v;
            item = i;
            srcList = s;
        }
    }

    public class ItemBaseAdapter extends BaseAdapter {

        Context context;
        List<Item> list;
        List<Item> items;

        ItemBaseAdapter(Context c, List<Item> l) {
            context = c;
            list = l;
        }

        public void refresh(List<Item> items) {
            this.items = items;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (list != null) {
                return list.size();
            }
            return 0;

        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public List<Item> getList() {
            return list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    /**
     * Drag and Drop List View Base Adapter Class
     *
     * @author Karthikeyan D S
     * @created on 20180518
     */
    public class ItemListAdapter extends ItemBaseAdapter {

        ItemListAdapter(Context c, List<Item> l) {
            super(c, l);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;

            // reuse views
            if (rowView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                rowView = inflater.inflate(R.layout.drag_drop_list_card_view, null);

                ViewHolder viewHolder = new ViewHolder();
                viewHolder.itemIcon = (ImageView) rowView.findViewById(R.id.image_view_item);
                viewHolder.itemName = (TextView) rowView.findViewById(R.id.text_view_item_name);

                viewHolder.itemName.setTypeface(fontPoppinsSemiBold);

                rowView.setTag(viewHolder);


            }

            ViewHolder holder = (ViewHolder) rowView.getTag();
            holder.itemIcon.setImageDrawable(list.get(position).itemImage);
            holder.itemName.setText(list.get(position).stationeryName);
            holder.itemCode = list.get(position).stationeryCode;
//            holder.itemCreatorID = list.get(position).creatorId;

//            rowView.setOnDragListener(myOnDragListener);

            return rowView;
        }
    }

    /**
     * Function to get drag and dropped Selected Item Lists and upload to creator.
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    public void getSelectedItemList() {
        String insertedStationeryRequestResponseMessage = MainConstants.kConstantEmpty;
        if (!isNetworkAvailable()) {
            noActionHandler.removeCallbacks(noActionThread);
            TimeZoneConventer connection = new TimeZoneConventer();
            if ( !connection.isWiFiEnabled(getApplicationContext())) {
                connection.wifiReconnect(getApplicationContext());
            }
            chooseOption = 0;
            utility.UsedCustomToast.makeCustomToast(
                    getApplicationContext(),
                    StringConstants.kConstantErrorMsgInNetworkError,
                    Toast.LENGTH_LONG, Gravity.TOP).show();
            utility.UsedCustomToast.makeCustomToast(
                    getApplicationContext(),
                    StringConstants.kConstantErrorMsgInNetworkError,
                    Toast.LENGTH_LONG, Gravity.TOP).show();

        } else {
            currentTime = System.currentTimeMillis();
            if ((selectedStationeryListFinal != null) && (selectedStationeryListFinal.size() > 0)) {
                disableProcess();
                StationeryRequest request = new StationeryRequest();
                request.setEmployeeId(SingletonEmployeeProfile
                        .getInstance().getEmployeeId());
                request.setRequestTime(currentTime);
                if (dbHelper == null) {
                    dbHelper = new DbHelper(getApplicationContext());
                }
                insertedStationeryRequestResponseMessage = dbHelper.insertStationeryRequest(request);
//                Log.d("keyyyyy", "stationery request" + request);

                if (insertedStationeryRequestResponseMessage != "") {
                    sendFailureRequest(insertedStationeryRequestResponseMessage, ErrorConstants.SqliteError);
                    uploadStationeryItemEntryFailure(ErrorConstants.SqliteError);
                } else {
                    Set<Integer> keys = selectedStationeryListFinal.keySet();
                    for (Integer key : keys) {
								System.out.println("keyyyyy"+key);
//                        Log.d("keyyyyy", "" + key);
                        StationeryEntry entry = (StationeryEntry) selectedStationeryListFinal
                                .get(key);
                        insertedStationeryRequestResponseMessage = dbHelper.insertStationeryEntry(entry);
//                        Log.d("keyyyyy", "stationery" + entry.getRequestId());
//                        Log.d("keyyyyy", "stationery" + entry.getCreatorId());
//                        Log.d("keyyyyy", "stationery" + entry.getQuantity());
//                        Log.d("keyyyyy", "stationery" + entry.getStationeryItemCode());
                        if (insertedStationeryRequestResponseMessage != MainConstants.kConstantEmpty) {
                            break;
                        }
                    }
                    if (insertedStationeryRequestResponseMessage != MainConstants.kConstantEmpty) {
                        sendFailureRequest(insertedStationeryRequestResponseMessage,
                                ErrorConstants.SqliteError);
                        uploadStationeryItemEntryFailure(ErrorConstants.SqliteError);
                    } else {
                        uploadStationeryItemEntry(keys);
                        // disable backbutton, cancel and confirm button
                        // for not allowed to changes and waiting for
                        // response
                        setEnableViews(false);
                        IsallowedToClickStationaryItems = false;
                    }
                }
            } else {
                initializeErrorDialog();
                showErrorDialogBox();
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTime);
            }

        }
    }


    /*
     * Created by jayapriya On 01/12/2014 Intialize the dialogbox for home page
     */
    private void initializeErrorDialog() {
        reportDialog = new Dialog(context);
        reportDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        reportDialog.setContentView(R.layout.dialog_report);
        // dialog.(MessageConstant.dialogTitle);
        textViewReportMsg = (TextView) reportDialog
                .findViewById(R.id.textView_report_msg);
        textViewReportFirstMsg = (TextView) reportDialog
                .findViewById(R.id.textView_report_first_msg);
        textViewReportErrorCode = (TextView) reportDialog
                .findViewById(R.id.textView_report_error_code);
        progressBar = (ProgressBar) reportDialog
                .findViewById(R.id.progressBar_report);
        reportImage = (ImageView) reportDialog
                .findViewById(R.id.imageView_report);
        tellUs = (Button) reportDialog.findViewById(R.id.button_tellus);
        seperator = (View) reportDialog.findViewById(R.id.firstSeparator);
        relativeLayout = (RelativeLayout) reportDialog
                .findViewById(R.id.relativeLayout_dialog);
        relativeLayout.getLayoutParams().width = (int) (400 * context
                .getApplicationContext().getResources().getDisplayMetrics().density);
        reportImage.setVisibility(View.GONE);
        textViewReportFirstMsg.setVisibility(View.VISIBLE);
        // reportImage.setBackgroundDrawable(new
        // ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressBar.setVisibility(View.GONE);
        reportDialog.setCanceledOnTouchOutside(true);
        reportDialog.setOnKeyListener(null);
        textViewReportErrorCode.setVisibility(View.GONE);
        seperator.setVisibility(View.VISIBLE);
        tellUs.setVisibility(View.VISIBLE);
        // tellUs.setText(MainConstants.tellUs);
        textViewReportFirstMsg.setVisibility(View.INVISIBLE);// .setText(MainConstants.kConstantAtleastOneItemSelected);
        textViewReportMsg
                .setText(StringConstants.kConstantAtleastOneItemSelected);
        textViewReportErrorCode.setVisibility(View.INVISIBLE);// .setText(MainConstants.visitorUploadErrorCodeText+reportStatus);
        textViewReportMsg.setTextColor(Color.rgb(102, 102, 102));
        relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));
        reportDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tellUs.setTextColor(Color.rgb(102, 102, 102));
        tellUs.setTextSize(20);
        seperator.setBackgroundColor(Color.rgb(171, 171, 171));
        textViewReportMsg.setTextSize(23);
        textViewReportFirstMsg.setMinHeight(80);
        textViewReportFirstMsg.setGravity(Gravity.TOP);
        tellUs.setTypeface(fontPoppinsSemiBold);
        textViewReportMsg.setTypeface(fontPoppinsSemiBold);
        textViewReportFirstMsg.setTypeface(fontPoppinsSemiBold);
        textViewReportErrorCode.setTypeface(fontPoppinsSemiBold);
        tellUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDialog.dismiss();
            }

        });
    }

    private void showErrorDialogBox() {
        if ((reportDialog != null) && (!reportDialog.isShowing())) {
            reportDialog.show();
        }
    }
    private void sendFailureRequest(String msg, int code) {
        String requestParameter = MainConstants.kConstantCreatorAuthTokenAndScope
                // +DataBaseConstants.addStationeryRequestTime
                // +TimeZoneConventer.getDate(currentTime)
                + DataBaseConstants.addLogCode + code
                + DataBaseConstants.addLogMsg + msg;
        }
    /*
     * set internal log and insert to local sqlite
     *
     *
     * @created On 20150910
     *
     * @author karthick
     */
    private void setInternalLogs(int errorCode,
                                 String errorMessage) {

        NetworkConnection networkConnection = new NetworkConnection();
        InternalAppLogs internalAppLogs = new InternalAppLogs();
        internalAppLogs.setErrorCode(errorCode);
        internalAppLogs.setErrorMessage(errorMessage);
        internalAppLogs.setLogDate(new Date().getTime());
        internalAppLogs.setWifiSignalStrength(networkConnection
                .getCurrentWifiSignalStrength(this));
        DbHelper.getInstance(this).insertLogs(internalAppLogs);
    }

    /**
     * Dialog to show Upload Failure Error
     *
     * @author Karthikeyan D S
     * @created on 04Jul2018
     */
    private void uploadStationeryItemEntryFailure(int code) {
        chooseOption = 0;
        enableProcess();
        noActionHandler.postDelayed(noActionThread,
                MainConstants.afterTypingNoFaceTime);

        DialogView.hideDialog();
        DialogView.setNullDialog();
        DialogView.showReportDialog(this,
                MainConstants.uploadStationeryFailure,
                code);

        // }
        // setGridView();
    }

    /**
     * Upload Stationery Details to creator.
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private void uploadStationeryItemEntry(Set<Integer> keys) {
        JSONArray array = new JSONArray();
        String entryItem = MainConstants.kConstantEmpty;
        String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String deviceLocation = SingletonVisitorProfile.getInstance().getDeviceLocation();
        String appVersionCode = BuildConfig.VERSION_NAME;
        for (Integer key : keys) {
            System.out.println(key);
            StationeryEntry entry = (StationeryEntry) selectedStationeryListFinal.get(key);
            JSONObject object = new JSONObject();
            try {
                object.put("code", entry.getStationeryItemCode());
                object.put("quantity", entry.getQuantity());
            } catch (JSONException e) {

            }
            array.put(object);
            object = null;
            // if(entryItem !=""){
            // entryItem =entryItem+ "_";
            // }
            // entryItem = entryItem + entry.getStationeryItemCode() + ":"+
            // entry.getQuantity();

        }

        JSONObject stationeryEntryObject = new JSONObject();
        try {
            stationeryEntryObject.put("StationeryItemEntry", array);
            stationeryEntryObject.put("device_id", deviceId);
            stationeryEntryObject.put("device_location", deviceLocation);
            stationeryEntryObject.put("app_version", appVersionCode);
            stationeryEntryObject.put( "visitor_office_location",SingletonVisitorProfile.getInstance()
                    .getOfficeLocation());
            stationeryEntryObject.put("visitor_office_location_code",SingletonVisitorProfile.getInstance()
                    .getLocationCode());
        } catch (JSONException e) {

        }
//        entryItem = stationeryEntryObject.toString();
        // Log.d("item", "entry" + entryItem);
        String requestParameter = MainConstants.kConstantCreatorAuthTokenAndScope
                // +DataBaseConstants.addStationeryRequestTime
                // +TimeZoneConventer.getDate(currentTime)
                + DataBaseConstants.addStationeryEmployeeId
                + SingletonEmployeeProfile.getInstance().getEmployeeId()
                + DataBaseConstants.addStationeryEmployeeName
                + SingletonEmployeeProfile.getInstance().getEmployeeName()
                + DataBaseConstants.addStationeryEmployeeEmailId
                + SingletonEmployeeProfile.getInstance().getEmployeeMailId()
                + DataBaseConstants.addStationeryEmployeePhoneNo
                + SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
                + DataBaseConstants.addStationeryEmployeeZuid
                + SingletonEmployeeProfile.getInstance().getEmployeeZuid()
                + DataBaseConstants.addStationeryEmployeeLocation
                + SingletonEmployeeProfile.getInstance()
                .getEmployeeSeatingLocation()
                + DataBaseConstants.addStationeryRequestId
                + SingletonEmployeeProfile.getInstance().getEmployeeId() + "_"
                + TimeZoneConventer.getDate(currentTime)
                + DataBaseConstants.addStationeryEntry
                + stationeryEntryObject.toString();
        // + DataBaseConstants.addStationeryEntry + "StationeryItemEntry:[1]";
        utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
                StringConstants.kConstantMsgInStationeryItemSendRequest,
                Toast.LENGTH_LONG, Gravity.TOP).show();
        // sendVolleyStringRequest(
        // (MainConstants.applicationStationeryApp
        // + MainConstants.kConstantAddStationeryRequestRecord +
        // requestParameter)
        // .replace(" ", "%20"), "StationeryRequest");
        BackgroundProcess backgroundProcess = new BackgroundProcess(
                requestParameter);
        backgroundProcess.execute();
    }

    /**
     * background process for upload visitor details
     */
    public class BackgroundProcess extends AsyncTask<String, String, String> {

        String response = MainConstants.kConstantEmpty;
        String requestParameter = MainConstants.kConstantEmpty;
        int responseCode = MainConstants.kConstantMinusOne;

        public BackgroundProcess(String requestParameter) {
            this.requestParameter = requestParameter;
        }

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... f_url) {

            JSONParser jsonParser = new JSONParser();

            URL url;
            try {
                url = new URL(MainConstants.applicationStationeryApp
                        + MainConstants.kConstantAddStationeryRequestRecord);

                JSONObject jsonObject = jsonParser.getJsonArrayInPostMethod(
                        url, requestParameter.replace(" ", "%20"));

                if ((jsonObject != null)) {
                    response = jsonObject.toString();
                    long id = creator.getStationeryRequestId(jsonObject);

                    if ((id > 0L)) {
                        responseCode = ErrorConstants.stationeryRequestSuccess;
                    } else {
                        sendFailureRequest(jsonObject.toString(),
                                ErrorConstants.stationeryRequestJsonDataError);
                        responseCode = ErrorConstants.stationeryRequestJsonDataError;
                    }

                }

            } catch (MalformedURLException e) {
                responseCode = ErrorConstants.stationeryRequestURLError;
            } catch (IOException e) {
                responseCode = ErrorConstants.stationeryRequestIOError;
            } catch (JSONException e) {
                responseCode = ErrorConstants.stationeryRequestJSONError;
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String responseParam) {

            if (responseCode == ErrorConstants.stationeryRequestSuccess) {
                utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
                        StringConstants.kConstantMsgInStationeryItemUploadSuccess,
                        Toast.LENGTH_LONG, Gravity.TOP).show();
                getHomePage();
//                noActionHandler.removeCallbacks(noActionThread);
                finish();
            } else {
                enableProcess();
                setEnableViews(true);
                IsallowedToClickStationaryItems = true;
                uploadStationeryItemEntryFailure(responseCode);
                setInternalLogs(responseCode, response);
            }
        }
    }

    public boolean isValidate() {
        boolean isDataPresent = true;
        for (int itemCount = 0; itemCount < selectedItemListStationery.size(); itemCount++) {
            int itemQuantity = 1;
            int StationeryCode = selectedItemListStationery.get(itemCount).stationeryCode;
            if (itemQuantity > 0) {
                addSelectedItemInHashMap(itemCount, StationeryCode, itemQuantity);
            } else {
                if (selectedStationeryListFinal != null && selectedStationeryListFinal.size() > 0) {
                    selectedStationeryListFinal.clear();
                }
                isDataPresent = false;
            }
        }
        return isDataPresent;
    }

    /**
     * add selected item in hashmap
     */
    private void addSelectedItemInHashMap(int position, int stationeryCode, int quantity) {
        if (quantity > 0) {
            // Log.d("stationery", " selectedStationeryListFinal " + stationeryCode);
            StationeryEntry entry = new StationeryEntry();
            entry.setQuantity(quantity);
            entry.setStationeryItemCode(stationeryCode);
            entry.setEntryTime(// TimeZoneConventer.getDate(
                    (System.currentTimeMillis() / 1000) * 1000);
            if (selectedStationeryListFinal != null) {
                if (selectedStationeryListFinal.get(position) != entry) {
                    selectedStationeryListFinal.put(position, entry);
                } else {
                    removeSelectedItemInHashMap(entry);
                    selectedStationeryListFinal.put(position, entry);
                }
            }
        }

    }

    /**
     * remove selected item in hashmap
     */
    private void removeSelectedItemInHashMap(StationeryEntry entry) {
        if (selectedStationeryListFinal.containsValue(entry)) {
            selectedStationeryListFinal.remove(entry);
        }
    }

    /**
     * Check's Network is Available to upload Stationery Details.
     *
     * @author Karthikeyan D S
     * @created on 03JUL2018
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager!=null)
        {
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        return false;
    }
}