package com.zoho.app.frontdesk.android;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import constants.StringConstants;
import database.dbschema.EmployeeVisitorPreApproval;
import utility.CompressImage;
import utility.DialogViewWarning;
import appschema.SharedPreferenceController;
import utility.Utility;
import constants.FontConstants;
import constants.MainConstants;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import appschema.ExistingVisitorDetails;

public class ActivityExistingVisitorDetails extends Activity {

    private RelativeLayout relativeLayoutMain, relativeLayoutEmployee;
    private Button buttonEdit, buttonConfirm;
    private ImageView imageviewBack, imageviewHome, imageviewEmpPhoto, image_view_next;
    private TextView textviewNameTitle, textviewNameValue, textviewContactTitle, textviewContactValue,
            textviewEmpTitle, textviewEmpName, textviewEmpPhone, textviewEmpDept, textviewPurposeTitle, textviewPurposeValue, textviewInfo, textviewCompanyTitle, textviewCompanyName;
    private Typeface typefaceButton, typefaceTitle, typefaceValue;
    private DialogViewWarning dialog;
    private Handler noActionHandler;
    private DbHelper database;
    private Bitmap drawableEmployeeImage, bitmap;
    private SharedPreferenceController spController;
    private int applicationMode =  NumberConstants.kConstantZero;
    private ExistingVisitorDetails existingVisitorDetails;
    private EmployeeVisitorPreApproval employeeVisitorPreApproval;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_existing_visitor_details);

        assignObject();

        setOldValue();
    }

    /**
     * Assign objects Set fonts style and listeners
     *
     * @Created on 20170509
     * @author Karthick
     */
    private void assignObject() {
        textviewNameTitle = (TextView) findViewById(R.id.textview_name_title);
        textviewNameValue = (TextView) findViewById(R.id.textview_name_value);
        textviewContactTitle = (TextView) findViewById(R.id.textview_contact_title);
        textviewContactValue = (TextView) findViewById(R.id.textview_contact_value);
        textviewEmpTitle = (TextView) findViewById(R.id.textview_emp_title);
        textviewEmpName = (TextView) findViewById(R.id.text_view_employee_name);
        textviewCompanyTitle = (TextView) findViewById(R.id.textview_company_title);
        textviewCompanyName = (TextView) findViewById(R.id.textview_company_value);
        textviewEmpPhone = (TextView) findViewById(R.id.text_view_employee_Phone);
        textviewEmpDept = (TextView) findViewById(R.id.text_view_employee_Dept);
        textviewPurposeTitle = (TextView) findViewById(R.id.textview_purpose_title);
        textviewPurposeValue = (TextView) findViewById(R.id.textview_purpose_value);
        textviewInfo = (TextView) findViewById(R.id.textview_visitor_checkin_info);

        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewEmpPhoto = (ImageView) findViewById(R.id.image_view_employee);
        buttonEdit = (Button) findViewById(R.id.button_visit_detail_edit);
        buttonConfirm = (Button) findViewById(R.id.button_visit_detail_confirm);

        relativeLayoutMain = (RelativeLayout) findViewById(R.id.relative_layout_main);
        relativeLayoutEmployee = (RelativeLayout) findViewById(R.id.relativelayout_employee);

        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

        if (spController == null) {
            spController = new SharedPreferenceController();
        }
        if (spController != null) {
            applicationMode = spController.getApplicationMode(this);
        }

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        assignFontConstants();

        setAppBackground();

        assignListeners();
    }

    /**
     * Assign Font Constants
     *
     * @author Karthikeyan D S
     * @created 27JUL2018
     */
    private void assignFontConstants() {
        typefaceButton = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskSemiBold);
        typefaceValue = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);

        if (textviewNameTitle != null && typefaceTitle != null) {
            textviewNameTitle.setTypeface(typefaceTitle);
        }
        if (textviewNameValue != null && typefaceValue != null) {
            textviewNameValue.setTypeface(typefaceValue);
        }
        if (textviewContactTitle != null && typefaceTitle != null) {
            textviewContactTitle.setTypeface(typefaceTitle);
        }
        if (textviewContactValue != null && typefaceValue != null) {
            textviewContactValue.setTypeface(typefaceValue);
        }
        if (textviewCompanyTitle != null && typefaceTitle != null) {
            textviewCompanyTitle.setTypeface(typefaceTitle);
        }
        if (textviewCompanyName != null && typefaceValue != null) {
            textviewCompanyName.setTypeface(typefaceValue);
        }
        if (textviewEmpTitle != null && typefaceTitle != null) {
            textviewEmpTitle.setTypeface(typefaceTitle);
        }
        if (textviewEmpName != null && typefaceValue != null) {
            textviewEmpName.setTypeface(typefaceValue);
        }
        if (textviewEmpPhone != null && typefaceValue != null) {
            textviewEmpPhone.setTypeface(typefaceValue);
        }
        if (textviewEmpDept != null && typefaceValue != null) {
            textviewEmpDept.setTypeface(typefaceValue);
        }
        if (textviewPurposeTitle != null && typefaceTitle != null) {
            textviewPurposeTitle.setTypeface(typefaceTitle);
        }
        if (textviewPurposeValue != null && typefaceValue != null) {
            textviewPurposeValue.setTypeface(typefaceValue);
        }
        if (textviewInfo != null && typefaceValue != null) {
            textviewInfo.setTypeface(typefaceValue);
        }
        if (buttonEdit != null && typefaceButton != null) {
            buttonEdit.setTypeface(typefaceButton);
        }
        if (buttonConfirm != null && typefaceButton != null) {
            buttonConfirm.setTypeface(typefaceButton);
        }
    }

    /**
     * Assign Listeners.
     *
     * @author Karthikeyan D S
     * @created 27JUL2018
     */
    private void assignListeners() {
        if (image_view_next != null && listenerButtonConfirm != null) {
            image_view_next.setOnClickListener(listenerButtonConfirm);
        }
        if (imageviewBack != null && listenerButtonBack != null) {
            imageviewBack.setOnClickListener(listenerButtonBack);
        }
        if (imageviewHome != null && listenerButtonHome != null) {
            imageviewHome.setOnClickListener(listenerButtonHome);
        }
        if (buttonConfirm != null && listenerButtonConfirm != null) {
            buttonConfirm.setOnClickListener(listenerButtonConfirm);
        }
        if (buttonEdit != null && listenerButtonEdit != null) {
            buttonEdit.setOnClickListener(listenerButtonEdit);
        }
    }

    /**
     * Goto edit mode Next Activity based on application Mode.
     *
     * @author Karthikeyan D S
     * @created 27JUL2018
     */
    private OnClickListener listenerButtonEdit = new OnClickListener() {
        public void onClick(View v) {
            SingletonVisitorProfile.getInstance().setVisitorType(MainConstants.editVisitor);
            if (applicationMode ==NumberConstants.kConstantNormalMode) {
                launchNamePage();
            } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
//                if edit button Pressed redirect to all Pages after OTP entered
                launchOTPDetailsPage("editMode");
            }
        }
    };

    /**
     * Goto Next Activity based on application Mode.
     *
     * @author Karthikeyan D S
     * @created 27JUL2018
     */
    private OnClickListener listenerButtonConfirm = new OnClickListener() {
        public void onClick(View v) {
            SingletonVisitorProfile.getInstance().setVisitorType(MainConstants.existVisitor);
            if (applicationMode == NumberConstants.kConstantNormalMode) {
                launchPhotoPage();
            } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
//                if confirm button is pressed redirect to Camera Page after OTP entered.
                launchOTPDetailsPage("confirmMode");
            }
        }
    };

    /**
     * Goto contact page
     *
     * @created 20170509
     */
    private OnClickListener listenerButtonBack = new OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };

    /**
     * Handle tab click for goto settings page
     *
     * @author Karthick
     * @created on 20170509
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            launchHomePage();
        }
    };

    /**
     * Goto home page
     *
     * @created 20170509
     */
    private OnClickListener listenerButtonHome = new OnClickListener() {
        public void onClick(View v) {
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);
            }
            showWarningDialog();
            if (ApplicationController.getInstance() != null) {
                ApplicationController.getInstance().textSpeach(MainConstants.speachAreYouSureCode);
            }
        }
    };

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170509
     */
    private void launchPreviousPage() {
        Intent intent = new Intent(this, ExistingVisitorActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch photo activity
     *
     * @author Karthick
     * @created on 20170509
     */
    private void launchPhotoPage() {
        float density = getResources().getDisplayMetrics().density;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
//Log.d("DSk ","Activity Camera Two api");
            Intent intent = new Intent(this,
                    ActivityCameraTwoApi.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(getApplicationContext(),
                    GetImageActivity.class);
            startActivity(intent);
            finish();
        }

    }

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170509
     */
    private void launchNamePage() {
        Intent intent = new Intent(this, ActivityVisitorName.class);
        intent.putExtra(MainConstants.putExtraFromPage, MainConstants.fromSummary);
        startActivity(intent);
        finish();
    }

    /**
     * Launch OTPDetails activity with Intent Data to set
     * for Edit or Confirm Mode
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void launchOTPDetailsPage(String mode) {
        Intent intent = new Intent(this, ActivityOTPDetails.class);
        if (mode.equalsIgnoreCase("confirmMode")) {
            intent.putExtra("Mode", 2);
        }
        if (mode.equalsIgnoreCase("editMode")) {
            intent.putExtra("Mode", 1);
        } else {
            intent.putExtra(mode, 0);
        }
        startActivity(intent);
        finish();
    }

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170509
     */
    private void launchHomePage() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Set old value to textview
     *
     * @author Karthick
     * @created on 20170509
     */
    private void setOldValue() {
        if (database == null) {
            database = DbHelper.getInstance(this);
        }
        String contactNumber = MainConstants.kConstantEmpty;

        if (database != null && SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getContact() != null
                && !SingletonVisitorProfile.getInstance().getContact().isEmpty()) {
            existingVisitorDetails = new ExistingVisitorDetails();
            employeeVisitorPreApproval = new EmployeeVisitorPreApproval();
//			existingVisitorDetails = database.searchExistVisitor(SingletonVisitorProfile.getInstance().getContact());
            String visitPurpose = MainConstants.purposeDataCenter;
            contactNumber = SingletonVisitorProfile.getInstance().getContact();

            if (applicationMode == NumberConstants.kConstantNormalMode
                    && contactNumber != null && !contactNumber.isEmpty()) {
                existingVisitorDetails = database.searchExistVisitor(contactNumber, visitPurpose);
                existingVisitorDetails.setVisitorPreApproved(false);
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setVisitorPreApproved(false);
                }
                if (existingVisitorDetails != null && existingVisitorDetails.getEmployeeEmail() != null && !existingVisitorDetails.getEmployeeEmail().isEmpty()) {
                   boolean havingEmployeeDetails =  assignEmployeeDetailsCard(existingVisitorDetails.getEmployeeEmail());
                   if(havingEmployeeDetails)
                   {
                       if (textviewEmpTitle != null) {
                           textviewEmpTitle.setVisibility(View.VISIBLE);
                       }
                   }
                   else
                   {
                       if (textviewEmpTitle != null) {
                           textviewEmpTitle.setVisibility(View.GONE);
                       }
                   }
                } else {
                    if (relativeLayoutEmployee != null) {
                        relativeLayoutEmployee.setVisibility(View.GONE);
                    }
                    if(textviewEmpTitle!=null) {
                        textviewEmpTitle.setVisibility(View.GONE);
                    }
                    if (textviewEmpDept != null) {
                        textviewEmpDept.setVisibility(View.GONE);
                    }
                    if (textviewEmpName != null) {
                        textviewEmpName.setVisibility(View.GONE);
                    }
                        imageviewEmpPhoto.setVisibility(View.GONE);
                }
            }
            if (textviewPurposeValue != null && existingVisitorDetails != null && existingVisitorDetails.getPurpose() != null) {
                textviewPurposeValue.setText(existingVisitorDetails.getPurpose().toUpperCase());
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(existingVisitorDetails.getPurpose().toLowerCase());
                }

            } else if (applicationMode == NumberConstants.kConstantDataCentreMode && contactNumber != null && !contactNumber.isEmpty()) {
                existingVisitorDetails = new ExistingVisitorDetails();
                employeeVisitorPreApproval = new EmployeeVisitorPreApproval();

                if(contactNumber.contains("+91"))
                {
                    contactNumber =contactNumber.replaceFirst(Pattern.quote("+91"),"");
                }
//                Log.d("DSK ","EDIT MODE"+contactNumber);
                long mDateOfVisitToday = getTodayLongValue();

                existingVisitorDetails.setVisitorPreApproved(false);
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setVisitorPreApproved(false);
                }
                employeeVisitorPreApproval = database.searchEmployeeVisitorPreApprovalRecords(contactNumber, mDateOfVisitToday);
                if (employeeVisitorPreApproval == null) {
                    existingVisitorDetails = database.searchExistVisitor(contactNumber, visitPurpose);
                } else {
                    if (employeeVisitorPreApproval != null) {
                        if (employeeVisitorPreApproval.getmPhoneNumber() != null && !employeeVisitorPreApproval.getmPhoneNumber().isEmpty()) {
                            existingVisitorDetails.setContact(employeeVisitorPreApproval.getmPhoneNumber().replaceFirst("/+91",""));
                        }
                        if (employeeVisitorPreApproval.getmVisitorName() != null && !employeeVisitorPreApproval.getmVisitorName().isEmpty()) {
                            existingVisitorDetails.setName(employeeVisitorPreApproval.getmVisitorName());
                        }
                        existingVisitorDetails.setPurpose(MainConstants.purposeDataCenter);
                        existingVisitorDetails.setVisitorPreApproved(true);
                        if (employeeVisitorPreApproval.getmVisitorCompanyName() != null && !employeeVisitorPreApproval.getmVisitorCompanyName().isEmpty()) {
                            existingVisitorDetails.setCompanyName(employeeVisitorPreApproval.getmVisitorCompanyName());
                            if (textviewCompanyName != null) {
                                textviewCompanyName.setVisibility(View.VISIBLE);
                            }
                            if (textviewCompanyTitle != null) {
                                textviewCompanyTitle.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (textviewCompanyName != null) {
                                textviewCompanyName.setVisibility(View.GONE);
                            }
                            if (textviewCompanyTitle != null) {
                                textviewCompanyTitle.setVisibility(View.GONE);
                            }
                        }

                        if (employeeVisitorPreApproval.getmEmployeeEmailId() != null && !employeeVisitorPreApproval.getmEmployeeEmailId().isEmpty()) {
                            ZohoEmployeeRecord zohoEmployeeRecord = database.searchEmployee(employeeVisitorPreApproval.getmEmployeeEmailId());
                            if (zohoEmployeeRecord != null && zohoEmployeeRecord.getEmployeeEmailId() != null) {
                                existingVisitorDetails.setEmployeeEmail(zohoEmployeeRecord.getEmployeeEmailId());
                                if (SingletonEmployeeProfile.getInstance() != null) {
                                    SingletonEmployeeProfile.getInstance().setEmployeeMailId(zohoEmployeeRecord.getEmployeeEmailId());
                                    SingletonEmployeeProfile.getInstance().setEmployeeName(zohoEmployeeRecord.getEmployeeName());
                                    SingletonEmployeeProfile.getInstance().setEmployeeDepartment(zohoEmployeeRecord.getEmployeeDept());
                                    SingletonEmployeeProfile.getInstance().setEmployeeId(zohoEmployeeRecord.getEmployeeId());
                                    SingletonEmployeeProfile.getInstance().setEmployeeZuid(zohoEmployeeRecord.getEmployeeZuid());
                                    SingletonEmployeeProfile.getInstance().setEmployeeSeatingLocation(zohoEmployeeRecord.getEmployeeSeatingLocation());
                                    SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(zohoEmployeeRecord.getEmployeeMobileNumber());
                                    SingletonEmployeeProfile.getInstance().setEmployeeExtentionNumber(zohoEmployeeRecord.getEmployeeExtentionNumber());
                                }
                            }
                        }
                        if (SingletonVisitorProfile.getInstance() != null) {
                            SingletonVisitorProfile.getInstance().setVisitorPreApproved(true);
                        }
                    } else {
//                        if details Not Present.
                        launchPreviousPage();
                    }
                }

                if (textviewCompanyName != null && existingVisitorDetails != null && existingVisitorDetails.getCompanyName() != null
                        && !existingVisitorDetails.getCompanyName().isEmpty()) {
                    textviewCompanyTitle.setVisibility(View.VISIBLE);
                    textviewCompanyName.setVisibility(View.VISIBLE);
                    textviewCompanyName.setText(existingVisitorDetails.getCompanyName());
                    if (SingletonVisitorProfile.getInstance() != null) {
                        SingletonVisitorProfile.getInstance().setCompany(existingVisitorDetails.getCompanyName());
                    }
                }
                if (employeeVisitorPreApproval != null && employeeVisitorPreApproval.getmEmployeeEmailId() != null
                        && !employeeVisitorPreApproval.getmEmployeeEmailId().isEmpty()) {
                    boolean havingEmployeeDetails =  assignEmployeeDetailsCard(employeeVisitorPreApproval.getmEmployeeEmailId());
                    if(havingEmployeeDetails)
                    {
                        showEmployeeDetails();
                    }
                    else
                    {
                        hideEmployeeDetails();
                    }
                } else {
                  hideEmployeeDetails();
                }
            }
            if (textviewNameValue != null && existingVisitorDetails != null && existingVisitorDetails.getName() != null) {
                textviewNameValue.setText(existingVisitorDetails.getName().toUpperCase());
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setVisitorName(existingVisitorDetails.getName());
                }
                textviewNameTitle.setVisibility(View.VISIBLE);
                textviewNameValue.setVisibility(View.VISIBLE);
            } else {
                textviewNameTitle.setVisibility(View.GONE);
                textviewNameValue.setVisibility(View.GONE);
            }
            if (textviewContactValue != null && existingVisitorDetails != null && existingVisitorDetails.getContact() != null) {
                textviewContactValue.setText(existingVisitorDetails.getContact());
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setContact(existingVisitorDetails.getContact());
                }
                if (textviewContactTitle != null) {
                    textviewContactTitle.setVisibility(View.VISIBLE);
                }
                if (textviewContactValue != null) {
                    textviewContactValue.setVisibility(View.VISIBLE);
                }
            } else {
                if (textviewContactTitle != null) {
                    textviewContactTitle.setVisibility(View.GONE);
                }
                if (textviewContactValue != null) {
                    textviewContactValue.setVisibility(View.GONE);
                }
            }
            if (textviewPurposeValue != null && existingVisitorDetails != null && existingVisitorDetails.getPurpose() != null) {
                textviewPurposeValue.setText(existingVisitorDetails.getPurpose());
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(existingVisitorDetails.getPurpose());
                }
                if (textviewPurposeTitle != null) {
                    textviewPurposeTitle.setVisibility(View.VISIBLE);
                }
                if (textviewPurposeValue != null) {
                    textviewPurposeValue.setVisibility(View.VISIBLE);
                }
            } else {
                if (textviewPurposeTitle != null) {
                    textviewPurposeTitle.setVisibility(View.GONE);
                }
                if (textviewPurposeValue != null) {
                    textviewPurposeValue.setVisibility(View.GONE);
                }
            }
        } else {
            //  if details Not Present.
            launchPreviousPage();
        }
    }

    /**
     * hide EmployeeDetails
     *
     * @author Karthikeyan D S
     * @created on 22Aug2018
     */
    private void hideEmployeeDetails()
    {
        if(relativeLayoutEmployee!=null)
        {
            relativeLayoutEmployee.setVisibility(View.GONE);
        }
        if (textviewEmpTitle != null) {
            textviewEmpTitle.setVisibility(View.GONE);
        }
        if (imageviewEmpPhoto != null) {
            imageviewEmpPhoto.setVisibility(View.GONE);
        }
        if (textviewEmpDept != null) {
            textviewEmpDept.setVisibility(View.GONE);
        }
        if (textviewEmpName != null) {
            textviewEmpName.setVisibility(View.GONE);
        }
        if(textviewEmpPhone!=null)
        {
            textviewEmpPhone.setVisibility(View.GONE);
        }
    }

    /**
     * show EmployeeDetails
     *
     * @author Karthikeyan D S
     * @created on 22Aug2018
     */
    private void showEmployeeDetails()
    {
        if(relativeLayoutEmployee!=null)
        {
            relativeLayoutEmployee.setVisibility(View.VISIBLE);
        }
        if (textviewEmpTitle != null) {
            textviewEmpTitle.setVisibility(View.VISIBLE);
        }
        if (imageviewEmpPhoto != null) {
            imageviewEmpPhoto.setVisibility(View.VISIBLE);
        }
        if (textviewEmpDept != null) {
            textviewEmpDept.setVisibility(View.VISIBLE);
        }
        if (textviewEmpName != null) {
            textviewEmpName.setVisibility(View.VISIBLE);
        }
        if(textviewEmpPhone!=null)
        {
            textviewEmpPhone.setVisibility(View.VISIBLE);
        }
    }
    /**
     * Assign Employee Details View
     *
     * @param employeeEmail
     * @author Karthikeyan D S
     * @created on 12AUG2018
     */
    private boolean assignEmployeeDetailsCard(String employeeEmail) {
        boolean isHavingEmployeeDetails = false;
        if (database != null) {
            ZohoEmployeeRecord zohoEmployeeRecord = database.searchEmployee(employeeEmail);
            Utility utility = new Utility();
            if (utility != null && zohoEmployeeRecord != null) {
                utility.storeEmployeeIntoSingleton(zohoEmployeeRecord);

                if (textviewEmpName != null && zohoEmployeeRecord != null
                        && zohoEmployeeRecord.getEmployeeName() != null&&!zohoEmployeeRecord.getEmployeeName().isEmpty()) {
                    textviewEmpName.setText(zohoEmployeeRecord.getEmployeeName().toUpperCase());
                    isHavingEmployeeDetails =true;
                }
                else {
                    if(textviewEmpName!=null) {
                        textviewEmpName.setVisibility(View.GONE);
                    }
                }
                if (textviewEmpPhone != null && zohoEmployeeRecord != null
                        && zohoEmployeeRecord.getEmployeeMobileNumber() != null&& !zohoEmployeeRecord.getEmployeeMobileNumber().isEmpty()) {
                    textviewEmpPhone.setText(zohoEmployeeRecord.getEmployeeMobileNumber());
                }
                else {
                    if(textviewEmpPhone!=null) {
                        textviewEmpPhone.setVisibility(View.GONE);
                    }
                }
                if (textviewEmpDept != null && zohoEmployeeRecord != null
                        && zohoEmployeeRecord.getEmployeeDept() != null && !zohoEmployeeRecord.getEmployeeDept().isEmpty()) {
                    textviewEmpDept.setText(zohoEmployeeRecord.getEmployeeDept());
                }
                else {
                    if(textviewEmpDept!=null) {
                        textviewEmpDept.setVisibility(View.GONE);
                    }
                }
                if (ApplicationController.getInstance() != null && ApplicationController.getInstance().checkExternalPermission()) {
                    if (zohoEmployeeRecord != null
                            && zohoEmployeeRecord.getEmployeeZuid() != null
                            && !zohoEmployeeRecord.getEmployeeZuid().isEmpty()) {
                        String employeePhoto = MainConstants.kConstantFileDirectory
                                .concat(MainConstants.kConstantFileEmployeeImageFolder)
                                .concat(zohoEmployeeRecord.getEmployeeZuid())
                                .concat(MainConstants.kConstantImageFileFormat);
                        if (employeePhoto != null && !employeePhoto.isEmpty() && new File(employeePhoto).exists()) {
                            drawableEmployeeImage = CompressImage
                                    .decodeSampleBitmapFromSdCard(
                                            getResources(), employeePhoto,
                                            100, 100);
                            if (drawableEmployeeImage != null) {
                                bitmap = BitmapFactory.decodeFile(employeePhoto);
                                if (imageviewEmpPhoto != null && bitmap != null) {
                                    imageviewEmpPhoto.setImageBitmap(bitmap);
                                }
                            }
                        }
                        else {
                            if(imageviewEmpPhoto!=null) {
                                imageviewEmpPhoto.setVisibility(View.GONE);
                            }
                        }
                    }

                }
            }
        }
        return isHavingEmployeeDetails;
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthick
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            launchHomePage();
        }
    };

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthick
     * @created on 20170509
     */
    private void showWarningDialog() {
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        if (dialog != null) {
            dialog.setDialogCode(MainConstants.dialogDecision);
            dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
            dialog.setActionListener(listenerDialogAction);
            dialog.showDialog();
        } else {
            launchHomePage();
        }
    }

    /**
     * Set background color
     *
     * @author Karthick
     * @created on 20170509
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutMain.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFfa654b, 0xFFfc5d55, 0xFFfd575d, 0xFFff5066});
                relativeLayoutMain.setBackground(gd);
            }
        }
    }

    /**
     * @return today's Date Long Value
     * @author Karthikeyan D S
     * @Created on 04JUL2018
     */
    private long getTodayLongValue() {
        long getVisitorDateLong = NumberConstants.kConstantMinusOne;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = Calendar.getInstance().getTime();

        long joinDate = NumberConstants.kConstantMinusOne;

        String strDate = simpleDateFormat.format(date);
        try {
            date = simpleDateFormat.parse(strDate);
            joinDate = date.getTime();
//			Log.d("DSK",""+joinDate);
            return joinDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getVisitorDateLong;
    }

    /**
     * Goto previous page on backpress
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onBackPressed() {
        launchPreviousPage();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void deallocateObjects() {
        if (database != null) {
            database.close();
            database = null;
        }
        imageviewBack = null;
        image_view_next = null;
        imageviewHome = null;
        imageviewEmpPhoto = null;
        textviewContactTitle = null;
        textviewContactValue = null;
        textviewEmpTitle = null;
        textviewEmpName = null;
        textviewEmpPhone = null;
        textviewEmpDept = null;
        textviewInfo = null;
        textviewNameTitle = null;
        textviewNameValue = null;
        textviewPurposeTitle = null;
        textviewPurposeValue = null;
        buttonConfirm = null;
        buttonEdit = null;
        relativeLayoutMain
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutMain = null;
        relativeLayoutEmployee = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (drawableEmployeeImage != null) {
            drawableEmployeeImage.recycle();
            drawableEmployeeImage = null;
        }
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        existingVisitorDetails = null;
        employeeVisitorPreApproval = null;
        applicationMode = NumberConstants.kConstantZero;
    }
}
