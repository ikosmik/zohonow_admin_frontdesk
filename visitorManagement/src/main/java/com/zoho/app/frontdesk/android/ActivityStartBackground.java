package com.zoho.app.frontdesk.android;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import appschema.SharedPreferenceController;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import backgroundprocess.GetAllRecords;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import logs.ActivityInternalLogs;
import utility.SpinnerViewAdapter;
import utility.TimeZoneConventer;
import utility.UsedAlarmManager;
import utility.UsedCustomToast;
import zohocreator.VisitorZohoCreator;

public class ActivityStartBackground extends Activity implements CompoundButton.OnCheckedChangeListener {

    private Button buttonStartBackground, buttonSave, buttonBack, buttonLogs,
            buttonExportDb, buttonConfirm, buttonCancel;
    private Typeface typefaceButton;
    private TextView textviewMemory, deviceLocationTitle, officeLocation,textViewAppPassword,textViewForgotPassword;
    private EditText editTextPassword;
    private Spinner spinnerLocation,spinnerOfficeLocation;
    private SpinnerViewAdapter adapterLocation;
    private SpinnerViewAdapter adapterOfficeLocation;
    private List<String> listLocation = new ArrayList<String>();
    private List<String> listOfficeLocation = new ArrayList<String>();
    private ImageView imageViewBlackTheme, imageViewMixedTheme;
    private Switch applicationModeSwitch;
    private int applicationModeCode = NumberConstants.kConstantZero;
    private SharedPreferenceController spController;
    private RelativeLayout relativeLayoutChoosePurpose, relativeLayoutPassword;
    private String appSettingsStoredPassword = MainConstants.kConstantEmpty;
    private VisitorZohoCreator visitorZohoCreator;
    private boolean requestForgotPassword ;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_start_background);

        assignObject();

//        Log.d("DSK ",""+applicationModeCode);
        getMemoryUsage();

        try {
            File dir = getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {

        }
//      Set Device Location data in Spinner.
        setSpinnerLocation();
        setOfficeSpinnerLocation();
    }

    /**
     * Assign objects Set fonts style and listeners
     * <p>
     * Created on 20150820
     * Created by karthick
     * <p>
     * Modified by Karthikeyan D S
     */
    private void assignObject() {

        spinnerLocation = (Spinner) findViewById(R.id.spinner_location);
        spinnerOfficeLocation = (Spinner) findViewById(R.id.spinner_office_location);
        buttonStartBackground = (Button) findViewById(R.id.button_start_background);
        buttonExportDb = (Button) findViewById(R.id.button_export_db);
        buttonBack = (Button) findViewById(R.id.button_back);
        buttonLogs = (Button) findViewById(R.id.button_logs);
        buttonSave = (Button) findViewById(R.id.button_save);

        textviewMemory = (TextView) findViewById(R.id.textview_memory);
        textViewAppPassword = (TextView) findViewById(R.id.text_view_app_password_header);
        textViewForgotPassword = (TextView) findViewById(R.id.text_view_forgot_password);
        deviceLocationTitle = (TextView) findViewById(R.id.textview_device_loc_title);
        officeLocation = (TextView) findViewById(R.id.textview_office_loc_title);


        applicationModeSwitch = (Switch) findViewById(R.id.switch_application_mode);

        imageViewBlackTheme = (ImageView) findViewById(R.id.imageView_theme_black);
        imageViewMixedTheme = (ImageView) findViewById(R.id.imageView_theme_mixed_color);

        //here the the theme selection buttons are set to hide, to make them visible remove the line before comment
        imageViewBlackTheme.setVisibility(View.GONE);
        imageViewMixedTheme.setVisibility(View.GONE);

//
        relativeLayoutChoosePurpose = (RelativeLayout) findViewById(R.id.relativeLayoutChoosePurpose);
        relativeLayoutPassword = (RelativeLayout) findViewById(R.id.relativeLayoutPassword);
        editTextPassword = (EditText) findViewById(R.id.edit_text_password);

        buttonConfirm = (Button) findViewById(R.id.button_confirm);
        buttonCancel = (Button) findViewById(R.id.button_cancel);

        setAppModeandPassword();
        //      set Font Constants
        setFontConstants();
//      assign Listeners
        assignListeners();
//      Set Theme Chooser
        setThemeChooser();
    }

    private void setAppModeandPassword() {
        if(spController==null) {
            spController = new SharedPreferenceController();
        }
        if (spController != null) {
            applicationModeCode = spController.getApplicationMode(ActivityStartBackground.this);
            if (applicationModeCode == NumberConstants.kConstantDataCentreMode) {
                appSettingsStoredPassword = spController.getAppSettingsPassword(getApplicationContext());
                if (appSettingsStoredPassword == null || appSettingsStoredPassword.isEmpty()) {
                    textViewForgotPassword.setVisibility(View.INVISIBLE);
                    showConfirmPassword();
                } else {
                    textViewForgotPassword.setVisibility(View.VISIBLE);
                    showSettingsPasswordPage();
                }
            } else {
                relativeLayoutChoosePurpose.setVisibility(View.VISIBLE);
                relativeLayoutPassword.setVisibility(View.GONE);
                applicationModeSwitch.setVisibility(View.GONE);
//                spinnerLocation.setVisibility(View.VISIBLE);
//                buttonSave.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Hide keyboard
     *
     * @author Karthikeyan D S
     * @created on 25Jul2018
     */
    private void hideKeyboard() {
//        Log.d("DSK ","Hide Keyboard");
        View view = this.getCurrentFocus();
        if (view != null) {
//            Log.d("DSK ","Hide Keyboard2");
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if(imm!=null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    /**
     * show Confirm Password Mode
     *
     * @author Karthikeyan D S
     * @created on 25OCT2018
     */
    private void showConfirmPassword() {
        showSettingsPasswordPage();
        textViewAppPassword.setVisibility(View.VISIBLE);
    }

    /**
     * Set Font Style to the family
     *
     * @author Karthikeyan D S
     * @created on 28AUG2018
     */
    private void setFontConstants() {
        typefaceButton = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProSemibold);
        buttonStartBackground.setTypeface(typefaceButton);
        buttonExportDb.setTypeface(typefaceButton);
        buttonBack.setTypeface(typefaceButton);
        buttonLogs.setTypeface(typefaceButton);
        buttonCancel.setTypeface(typefaceButton);
        buttonConfirm.setTypeface(typefaceButton);
        textviewMemory.setTypeface(typefaceButton);
        textViewAppPassword.setTypeface(typefaceButton);
        editTextPassword.setTypeface(typefaceButton);
        buttonSave.setTypeface(typefaceButton);
        deviceLocationTitle.setTypeface(typefaceButton);
        officeLocation.setTypeface(typefaceButton);
    }

    /**
     * Assign Listeners to the view
     *
     * @author Karthikeyan D S
     * @created on 28AUG2018
     */
    private void assignListeners() {
        if (buttonBack != null && listenerBack != null) {
            buttonBack.setOnClickListener(listenerBack);
        }
        if (buttonStartBackground != null && listenerStartBackground != null) {
            buttonStartBackground.setOnClickListener(listenerStartBackground);
        }
        if (buttonExportDb != null && listenerExportDb != null) {
            buttonExportDb.setOnClickListener(listenerExportDb);
        }
        if (buttonLogs != null && listenerTappedLogs != null) {
            buttonLogs.setOnClickListener(listenerTappedLogs);
        }
        if (buttonConfirm != null && listenerSubmitPassword != null) {
            buttonConfirm.setOnClickListener(listenerSubmitPassword);
        }
        if (buttonCancel != null && listenerCancelPassword != null) {
            buttonCancel.setOnClickListener(listenerCancelPassword);
        }

        if (imageViewMixedTheme != null && listenerMixedTheme != null) {
            imageViewMixedTheme.setOnClickListener(listenerMixedTheme);
        }
        if (imageViewBlackTheme != null && listenerBlackTheme != null) {
            imageViewBlackTheme.setOnClickListener(listenerBlackTheme);
        }
        if (buttonSave != null && listenerSaveLocation != null) {
            buttonSave.setOnClickListener(listenerSaveLocation);
        }


//		if (spinnerLocation != null && listenerAgencySelection != null) {
//            spinnerLocation.setOnItemSelectedListener(listenerAgencySelection);
//        }

//        applicationModeSwitch.setOnCheckedChangeListener(switchApplicationModeListener);

        if (applicationModeSwitch != null) {
            applicationModeSwitch.setOnCheckedChangeListener(this);
        }

        if(textViewForgotPassword!=null && forgotPasswordListener!=null)
        {
            textViewForgotPassword.setOnClickListener(forgotPasswordListener);
        }

        spinnerOfficeLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String selected_location = spinnerOfficeLocation.getSelectedItem().toString();
                if(selected_location.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia))
                {
                    adapterLocation.clear();
                    adapterLocation.add(MainConstants.selectLocation);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationOne);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationTwo);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationSeven);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationThree);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationFour);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationFive);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationEight);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationNine);
                }
                else if(selected_location.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaTenkasi))
                {
                    adapterLocation.clear();
                    adapterLocation.add(MainConstants.selectLocation);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationSix);
                }
                else if(selected_location.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta))
                {
                    adapterLocation.clear();
                    adapterLocation.add(MainConstants.selectLocation);
                    adapterLocation.add(MainConstants.kConstantDeviceLocationRenigunta);
                }
                else if(selected_location.equalsIgnoreCase(MainConstants.selectLocation))
                {
                    adapterLocation.clear();
                    adapterLocation.add(MainConstants.selectLocation);
                }
                else
                {
                    adapterLocation.clear();
                    adapterLocation.add(MainConstants.selectLocation);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationOne);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationTwo);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationSeven);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationThree);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationFour);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationFive);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationEight);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationNine);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationSix);
//                            adapterLocation.add(MainConstants.kConstantDeviceLocationRenigunta);

                    UsedCustomToast.makeCustomToast(ActivityStartBackground.this,"There is no device locations under "+selected_location,
                            Toast.LENGTH_LONG, Gravity.BOTTOM).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });



    }

    /**
     * Set Application Theme in Main Settings.
     *
     * @author Karthikeyan D S
     * @Created on 20180526
     */
    private void setThemeChooser() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                if (imageViewBlackTheme != null) {
                    imageViewBlackTheme.setImageResource(R.drawable.icon_circle_black_filled_tick);
                }
                if (imageViewMixedTheme != null) {
                    imageViewMixedTheme.setImageResource(R.drawable.icon_circle_blue_filled_empty);
                }
            }
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                if (imageViewBlackTheme != null) {
                    imageViewBlackTheme.setImageResource(R.drawable.icon_circle_black_filled_empty);
                }
                if (imageViewMixedTheme != null) {
                    imageViewMixedTheme.setImageResource(R.drawable.icon_circle_blue_filled_tick);
                }
            }
        }
    }

    /**
     * ApplicationMode Switch onCheckChanged Listener.
     *
     * @author Karthikeyan D S
     * @Created on 20180521
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (spController == null) {
            spController = new SharedPreferenceController();
        }
        if (isChecked) {
            deviceLocationTitle.setVisibility(View.GONE);
            spinnerLocation.setVisibility(View.GONE);
            officeLocation.setVisibility(View.GONE);
            spinnerOfficeLocation.setVisibility(View.GONE);
            if (applicationModeCode != NumberConstants.kConstantDataCentreMode) {
                buttonSave.setVisibility(View.VISIBLE);
            }
//            set application mode code as 2 for DataCenter.
//            applicationModeCode=2;
            spController.saveApplicationMode(ActivityStartBackground.this, NumberConstants.kConstantDataCentreMode);
//            String deviceLocation = "Datacenter";
//            spController.saveDeviceLocation(ActivityStartBackground.this,deviceLocation );
//            setSpinnerLocation();
//            setOfficeSpinnerLocation();
        } else {
            deviceLocationTitle.setVisibility(View.VISIBLE);
            spinnerLocation.setVisibility(View.VISIBLE);
            officeLocation.setVisibility(View.VISIBLE);
            spinnerOfficeLocation.setVisibility(View.VISIBLE);
            if (applicationModeCode != NumberConstants.kConstantDataCentreMode) {
                buttonSave.setVisibility(View.VISIBLE);
            }
//          set application mode code as 1 for Normal Mode.
//            applicationModeCode=1;
            spController.saveApplicationMode(ActivityStartBackground.this, NumberConstants.kConstantNormalMode);
//            spController.saveDeviceLocation(this,deviceLocation );
//            setSpinnerLocation();
//            setOfficeSpinnerLocation();
        }
    }

    // private Context context;
    /*
     * Set Mixed Theme Color.
     *
     * @author Karthikeyan D S
     *
     */
    private OnClickListener listenerMixedTheme = new OnClickListener() {
        public void onClick(View v) {
            if (spController == null) {
                spController = new SharedPreferenceController();
            }
            if (spController != null) {
                spController.setAppThemeColor(ActivityStartBackground.this, 2);
            }
            if (SingletonMetaData.getInstance() != null) {
                SingletonMetaData.getInstance().setThemeCode(2);
            }
//			Log.d("ThemeCode",""+spController.getAppThemeColor(ActivityStartBackground.this));
            if (imageViewBlackTheme != null) {
                imageViewBlackTheme.setImageResource(R.drawable.icon_circle_black_filled_empty);
            }
            if (imageViewMixedTheme != null) {
                imageViewMixedTheme.setImageResource(R.drawable.icon_circle_blue_filled_tick);
            }
        }
    };
    /*
     * Set Black Theme
     *
     * @author Karthikeyan D S
     */
    private OnClickListener listenerBlackTheme = new OnClickListener() {
        public void onClick(View v) {
            if (spController == null) {
                spController = new SharedPreferenceController();
            }
//			if(SingletonMetaData.getInstance()!=null)
//			{
//				SingletonMetaData.getInstance().setThemeCode(1);
//			}
            if (spController != null) {
                spController.setAppThemeColor(ActivityStartBackground.this, 1);
            }
//			Log.d("ThemeCode",""+spController.getAppThemeColor(ActivityStartBackground.this));
            if (imageViewBlackTheme != null) {
                imageViewBlackTheme.setImageResource(R.drawable.icon_circle_black_filled_tick);
            }
            if (imageViewMixedTheme != null) {
                imageViewMixedTheme.setImageResource(R.drawable.icon_circle_blue_filled_empty);
            }
        }
    };
    /*
     * Save selected device location
     *
     * @author Karthick
     * @Created on 20170803
     */
    private OnClickListener listenerSaveLocation = new OnClickListener() {
        public void onClick(View v) {
            if (saveSelectedDeviceLocation() && saveSelectedOfficeLocation()) {
//                Log.d("DSK ","location in ");
                Intent logIntent = new Intent(getApplicationContext(),
                        WelcomeActivity.class);
                startActivity(logIntent);
                finish();
            }
        }
    };

    private OnClickListener forgotPasswordListener = new OnClickListener()
    {
        public void onClick(View v)
        {
            if(isNetworkAvailable())
            {
                if(!requestForgotPassword)
                {
                    requestForgotPassword = MainConstants.boolTrue;
                    BackgroundProcessSendOTPRequest backgroundProcessSendOTPRequest = new BackgroundProcessSendOTPRequest();
                    backgroundProcessSendOTPRequest.execute();
                }
                else
                {
                    UsedCustomToast.makeCustomToast(ActivityStartBackground.this, StringConstants.kConstantRequestSent,
                            Toast.LENGTH_SHORT, Gravity.TOP).show();
                }
            }
            else
            {
                UsedCustomToast.makeCustomToast(ActivityStartBackground.this, StringConstants.kConstantMessageNoNetworkAvailable,
                        Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        }
    };

    private OnClickListener listenerSubmitPassword = new OnClickListener() {
        public void onClick(View v) {
            if (appSettingsStoredPassword == null || appSettingsStoredPassword.isEmpty())
            {
                if (editTextPassword != null && editTextPassword.getText().toString().trim() != null
                        && !editTextPassword.getText().toString().trim().isEmpty()) {
                    String appDataCentrePassword = editTextPassword.getText().toString().trim();
                    if (appDataCentrePassword != null && !appDataCentrePassword.isEmpty()
                            && appDataCentrePassword.matches(MainConstants.kConstantValidAllCharacterswithoutSmiley) ) {
                        spController.setAppSettingsPassword(getApplicationContext(), appDataCentrePassword);
                        UsedCustomToast.makeCustomToast(ActivityStartBackground.this, StringConstants.kConstantAppPasswordSaved,
                                Toast.LENGTH_LONG, Gravity.TOP).show();
                        hideSettingsPasswordPage();
                        callWelcomeActivity();
                    } else {
                        if (editTextPassword != null) {
                            editTextPassword.setFocusable(true);
                            editTextPassword.setError(StringConstants.kConstantInvalidPasswordFormat);
                        }
                    }
                } else {
                    if (editTextPassword != null) {
                        editTextPassword.setFocusable(true);
                        editTextPassword.setError(StringConstants.kConstantEnterPassword);
                    }
                }
            } else {
                if (editTextPassword != null && editTextPassword.getText().toString().trim() != null
                        && !editTextPassword.getText().toString().trim().isEmpty())
                {
                    String appDataCentrePassword = editTextPassword.getText().toString().trim();
                    if (appDataCentrePassword != null && !appDataCentrePassword.isEmpty()) {
                        if (appSettingsStoredPassword != null && !appSettingsStoredPassword.isEmpty()
                                && appSettingsStoredPassword.matches(MainConstants.kConstantValidAllCharacterswithoutSmiley)) {
                            if (appSettingsStoredPassword.equalsIgnoreCase(appDataCentrePassword)
                                    || appDataCentrePassword.equalsIgnoreCase(StringConstants.kConstantappPasswordAdminLevel)) {
                                hideSettingsPasswordPage();
                                hideKeyboard();
                            } else {
                                if (editTextPassword != null) {
                                    editTextPassword.setFocusable(true);
                                    editTextPassword.setError(StringConstants.kConstantInvalidPassword);
                                }
                            }
                        }
                        else
                        {
                            if (editTextPassword != null) {
                                editTextPassword.setFocusable(true);
                                editTextPassword.setError(StringConstants.kConstantInvalidPasswordFormat);
                            }
                        }

                    }
                    else
                    {
                        if (editTextPassword != null) {
                            editTextPassword.setFocusable(true);
                            editTextPassword.setError(StringConstants.kConstantInvalidPasswordFormat);
                        }
                    }
                } else {
                    if (editTextPassword != null) {
                        editTextPassword.setFocusable(true);
                        editTextPassword.setError(StringConstants.kConstantEnterPassword);
                    }
                }
            }
        }
    };
    private OnClickListener listenerCancelPassword = new OnClickListener() {
        public void onClick(View v) {
            if(appSettingsStoredPassword != null && !appSettingsStoredPassword.isEmpty()) {
                callWelcomeActivity();
            }
            else
            {
                UsedCustomToast.makeCustomToast(ActivityStartBackground.this, StringConstants.kConstantEnterPassword,Toast.LENGTH_SHORT,Gravity.TOP).show();
            }
        }
    };
    /*
     * Tapped Logs page to show internal Logs.
     *
     * Created on 2015
     */
    private OnClickListener listenerTappedLogs = new OnClickListener() {

        public void onClick(View v) {
            Intent logIntent = new Intent(getApplicationContext(),
                    ActivityInternalLogs.class);
            startActivity(logIntent);
        }
    };
    /*
     * Start background process
     *
     * Created on 20150820 Created by karthick
     */
    private OnClickListener listenerStartBackground = new OnClickListener() {

        public void onClick(View v) {
            if (ApplicationController.getInstance() != null && ApplicationController.getInstance().checkExternalPermission()) {
                initiateBackgroundProcess();
            } else {
                checkPermission(NumberConstants.requestExternalPermissionForBG);
            }
        }
    };
    /**
     * Export sqlite database to sdcard
     *
     * @author Karthick
     * @created on 20161104
     */
    private OnClickListener listenerExportDb = new OnClickListener() {
        public void onClick(View v) {
//			System.runFinalization();
//		    Runtime.getRuntime().gc();
//		    System.gc();
            if (ApplicationController.getInstance() != null && ApplicationController.getInstance().checkExternalPermission()) {
                exportDB();
            } else {
                checkPermission(NumberConstants.requestExternalPermissionForExportDB);
            }
        }
    };
    /*
     * Finish the activity
     *
     * Created on 20150820 Created by karthick
     */
    private OnClickListener listenerBack = new OnClickListener() {

        public void onClick(View v) {
            callWelcomeActivity();
        }
    };

    /**
     * Clear cache
     * <p>
     * Created on 20170601
     *
     * @auther Karthick
     */
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    /**
     * Get memory usage and display
     * <p>
     * Created on 20170601
     *
     * @auther Karthick
     */
    private void getMemoryUsage() {
        MemoryInfo mi = new MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem / 1048576L;

        final Runtime runtime = Runtime.getRuntime();
        final long usedMemInMB = (runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
        final long maxHeapSizeInMB = runtime.maxMemory() / 1048576L;
        final long availHeapSizeInMB = maxHeapSizeInMB - usedMemInMB;

        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();

        if (textviewMemory != null) {
            textviewMemory.setText("App Used Memory:" + usedMemInMB + " (MB)\nMax Memory:"
                    + runtime.maxMemory() / 1024 / 1024 + " (MB)\nAvailable Memory:" + availHeapSizeInMB +
                    " (MB)\nAvailable RAM:" + availableMegs + " (MB)\nAvailable Internal memory:"
                    + (availableBlocks * blockSize) / 1024 / 1024 + " (MB)");
        }
    }

    private void showSettingsPasswordPage() {
        relativeLayoutChoosePurpose.setVisibility(View.GONE);
        relativeLayoutPassword.setVisibility(View.VISIBLE);
        buttonBack.setVisibility(View.GONE);
        buttonLogs.setVisibility(View.GONE);
        textviewMemory.setVisibility(View.GONE);
        imageViewBlackTheme.setVisibility(View.GONE);
        imageViewMixedTheme.setVisibility(View.GONE);
    }

    private void hideSettingsPasswordPage() {
        textViewForgotPassword.setVisibility(View.VISIBLE);
        relativeLayoutChoosePurpose.setVisibility(View.VISIBLE);
        relativeLayoutPassword.setVisibility(View.GONE);
        buttonBack.setVisibility(View.VISIBLE);
        buttonLogs.setVisibility(View.VISIBLE);
        textviewMemory.setVisibility(View.VISIBLE);
        imageViewBlackTheme.setVisibility(View.VISIBLE);
        imageViewMixedTheme.setVisibility(View.VISIBLE);
    }

    /**
     * Check network and BG already started then start background process start
     *
     * @author Karthick
     * @created on 20161104
     */
    private void initiateBackgroundProcess() {
        if (!isNetworkAvailable()) {
            // showNoNetworkDialog();
            // setGridView();
            TimeZoneConventer connection = new TimeZoneConventer();
            if (!connection.isWiFiEnabled(getApplicationContext())) {
                connection.wifiReconnect(getApplicationContext());
            }

            utility.UsedCustomToast.makeCustomToast(
                    getApplicationContext(),
                    StringConstants.kConstantErrorMsgInNetworkError,
                    Toast.LENGTH_LONG).show();

        } else {
            // Log.d("service","start "+MainConstants.alreadyStartedBackgroundProcess);
            if (MainConstants.alreadyStartedBackgroundProcess <= NumberConstants.kConstantBackgroundStopped) {
                startBackgroundProcess();
            } else {
                utility.UsedCustomToast.makeCustomToast(
                        getApplicationContext(),
                        StringConstants.kConstantMessageBackgroundRunning, Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    /**
     * Export local sqlite to sdcard
     *
     * @author Karthick
     * @created on 20161104
     */
    private void exportDB() {
        // File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source = null;
        FileChannel destination = null;
        String currentDBPath = MainConstants.kConstantDBCurrentPath;
        // String backupDBPath = "insurer.db";
        File currentDB = new File(data, currentDBPath);
        // File backupDB = new File(sd, backupDBPath);

        File backupDirectory = new File(MainConstants.kConstantFileDirectory);
        File backupDB = new File(MainConstants.kConstantDBExportedPath);
        try {
            if (!backupDirectory.exists()) {
                backupDirectory.mkdirs();
            }
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "DB Export!" + e, Toast.LENGTH_LONG).show();
        }
    }

    /*
     * Finish the activity
     *
     * Created on 20150820 Created by karthick
     */
    private void startBackgroundProcess() {
        Intent eventService = new Intent(getApplicationContext(),
                GetAllRecords.class);
        // Log.d("onstoped", "eventService:"+isServiceRunning());
        UsedAlarmManager.deactivateAlarmManager(this);
        UsedAlarmManager.activateAlarmManager(this);
        if (!isServiceRunning())// ||
        // (MainConstants.alreadyStartedBackgroundProcess
        // <= 0))
        {
            MainConstants.alreadyStartedBackgroundProcess = NumberConstants.kConstantBackgroundStarted;
            //getApplicationContext().startService(eventService);
            // buttonStartBackground.setEnabled(false);
            utility.UsedCustomToast
                    .makeCustomToast(getApplicationContext(),
                            StringConstants.kConstantBackgroundStarted,
                            Toast.LENGTH_LONG).show();
        } else {
            // buttonStartBackground.setEnabled(true);
        }

    }

    private boolean isMyServiceRunning(Context context) {
        // Log.d("start","service");
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (GetAllRecords.class.getName().equals(
                    service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }

    /*
     * Find background service is running
     *
     * Created on 20150820 Created by karthick
     */
    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().contains("GetAllRecords")) {
                return true;
            }
        }
        return false;
    }

    /*
     * Find network is connected or not
     *
     * @created on 20150510
     *
     * @author karthick
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setSpinnerLocation() {
        if(spController==null)
        {
            spController = new SharedPreferenceController();
        }
        String location = spController.getLocations(this);
        applicationModeCode = spController.getApplicationMode(ActivityStartBackground.this);
        if (applicationModeCode == 1) {
//            Log.d("DSK","applicationModeCode1 "+applicationModeCode);
//            Log.d("DSK","location1 "+location);
//            Log.d("DSK","location1 "+spController.getDeviceLocation(this));
            listLocation.clear();
            String[] arrayList = null;
            if (location != null && !location.isEmpty()) {
                arrayList = location.split(MainConstants.locationSplit);
            }
            if (spController == null) {
                spController = new SharedPreferenceController();
            }
            String deviceLocation = MainConstants.kConstantEmpty;
            if (spController != null) {
                deviceLocation = spController.getDeviceLocation(this);
            }
            if (listLocation != null && arrayList != null && arrayList.length > MainConstants.kConstantZero) {
                for (int i = 0; i < Arrays.asList(arrayList).size(); i++) {
                    if (arrayList != null && arrayList[i] != null && !arrayList[i].isEmpty()) {
                        String data = arrayList[i];
//                    String data = utility.convertCamelCase(arrayList[i]);
                        if (data != null && !data.isEmpty()) {
                            listLocation.add(data.trim());
                        }
                    }
                }
                Collections.sort(listLocation);
                listLocation.add(0, MainConstants.selectLocation);
            } else if (listLocation != null) {
                listLocation.add(MainConstants.selectLocation);
                listLocation.add(MainConstants.kConstantDeviceLocationOne);
                listLocation.add(MainConstants.kConstantDeviceLocationTwo);
                listLocation.add(MainConstants.kConstantDeviceLocationSeven);
                listLocation.add(MainConstants.kConstantDeviceLocationThree);
                listLocation.add(MainConstants.kConstantDeviceLocationFour);
                listLocation.add(MainConstants.kConstantDeviceLocationFive);
                listLocation.add(MainConstants.kConstantDeviceLocationEight);
                listLocation.add(MainConstants.kConstantDeviceLocationNine);
                listLocation.add(MainConstants.kConstantDeviceLocationSix);
                listLocation.add(MainConstants.kConstantDeviceLocationRenigunta);
            }

            if (adapterLocation == null) {
                adapterLocation = new SpinnerViewAdapter(this, R.layout.custom_spinner_white, listLocation);
                adapterLocation
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerLocation.setAdapter(adapterLocation);
            } else {
                adapterLocation.notifyDataSetChanged();
            }

            //set old location selection
            if (spinnerLocation != null && deviceLocation != null && !deviceLocation.isEmpty()) {
                spinnerLocation.setSelection(listLocation.indexOf(deviceLocation));
            }
        } else if (applicationModeCode == NumberConstants.kConstantDataCentreMode) {
//            Log.d("DSK","applicationModeCode2 "+applicationModeCode);
//            Log.d("DSK","location2 "+location);
//            Log.d("DSK","location2 "+spController.getDeviceLocation(this));
            applicationModeSwitch.setChecked(true);
            deviceLocationTitle.setVisibility(View.GONE);
            spinnerLocation.setVisibility(View.GONE);
            officeLocation.setVisibility(View.GONE);
            spinnerOfficeLocation.setVisibility(View.GONE);
            buttonSave.setVisibility(View.GONE);
//            UsedCustomToast.makeCustomToast(ActivityStartBackground.this,"Switched to DataCentre Mode!",Toast.LENGTH_SHORT, Gravity.TOP).show();
        }
//        else
//        {
//            UsedCustomToast.makeCustomToast(this, MainConstants.kConstantSelectLocation, Toast.LENGTH_SHORT, Gravity.TOP).show();
//        }
    }

    private void setOfficeSpinnerLocation() {
        if(spController==null)
        {
            spController = new SharedPreferenceController();
        }
        String location = spController.getLocations(this);
        applicationModeCode = spController.getApplicationMode(ActivityStartBackground.this);
        if (applicationModeCode == 1) {
//            Log.d("DSK","applicationModeCode1 "+applicationModeCode);
//            Log.d("DSK","location1 "+location);
//            Log.d("DSK","location1 "+spController.getOfficeLocation(this));
            listOfficeLocation.clear();
            String[] arrayList = null;
            if (location != null && !location.isEmpty()) {
                arrayList = location.split(MainConstants.locationSplit);
            }
            if (spController == null) {
                spController = new SharedPreferenceController();
            }
            String officeLocation = MainConstants.kConstantEmpty;
            if (spController != null) {
                officeLocation = spController.getOfficeLocation(this);
            }
            if (listOfficeLocation != null && arrayList != null && arrayList.length > MainConstants.kConstantZero) {
                for (int i = 0; i < Arrays.asList(arrayList).size(); i++) {
                    if (arrayList != null && arrayList[i] != null && !arrayList[i].isEmpty()) {
                        String data = arrayList[i];
//                    String data = utility.convertCamelCase(arrayList[i]);
                        if (data != null && !data.isEmpty()) {
                            listOfficeLocation.add(data.trim());
                        }
                    }
                }
                Collections.sort(listOfficeLocation);
                listOfficeLocation.add(0, MainConstants.selectLocation);
            } else if (listOfficeLocation != null) {
                listOfficeLocation.add(MainConstants.selectLocation);
                listOfficeLocation.add(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia);
                listOfficeLocation.add(MainConstants.kConstantOfficeLocationIndiaTenkasi);
                listOfficeLocation.add(MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta);
            }

            if (adapterOfficeLocation == null) {
                adapterOfficeLocation = new SpinnerViewAdapter(this, R.layout.custom_spinner_white, listOfficeLocation);
                adapterOfficeLocation
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerOfficeLocation.setAdapter(adapterOfficeLocation);
            } else {
                adapterOfficeLocation.notifyDataSetChanged();
            }

            //set old location selection
            if (spinnerOfficeLocation != null && officeLocation != null && !officeLocation.isEmpty()) {
                spinnerOfficeLocation.setSelection(listOfficeLocation.indexOf(officeLocation));
            }
        } else if (applicationModeCode == NumberConstants.kConstantDataCentreMode) {
//            Log.d("DSK","applicationModeCode2 "+applicationModeCode);
//            Log.d("DSK","location2 "+location);
//            Log.d("DSK","location2 "+spController.getOfficeLocation(this));
            applicationModeSwitch.setChecked(true);
            officeLocation.setVisibility(View.GONE);
            spinnerOfficeLocation.setVisibility(View.GONE);
            buttonSave.setVisibility(View.GONE);
//            UsedCustomToast.makeCustomToast(ActivityStartBackground.this,"Switched to DataCentre Mode!",Toast.LENGTH_SHORT, Gravity.TOP).show();
        }
//        else
//        {
//            UsedCustomToast.makeCustomToast(this, MainConstants.kConstantSelectLocation, Toast.LENGTH_SHORT, Gravity.TOP).show();
//        }
    }

    /*
     * Save selected device location
     *
     * @created on 20170803
     * @author Karthick
     */
    private boolean saveSelectedDeviceLocation() {
        if (spController == null) {
            spController = new SharedPreferenceController();
        }
//        Log.d("DSK ","applicationModeCode "+applicationModeCode);
        if (applicationModeCode == NumberConstants.kConstantNormalMode)
        {
            if (spController != null && spinnerLocation != null && spinnerLocation.getSelectedItem() != null
                    && !spinnerLocation.getSelectedItem().toString().trim().isEmpty()
                    && !spinnerLocation.getSelectedItem().toString().trim().equalsIgnoreCase(MainConstants.selectLocation))
            {
                String location = spinnerLocation.getSelectedItem().toString().trim();
//                Log.d("DSK ","location "+location);
                spController.saveDeviceLocation(this, location);
                return true;
            }
            else
            {
                UsedCustomToast.makeCustomToast(this, StringConstants.kConstantSelectLocation, Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        } else if (applicationModeCode == NumberConstants.kConstantDataCentreMode)
        {
            String location = MainConstants.purposeDataCenter;
//            Log.d("DSK ","location "+location);
            spController.saveDeviceLocation(this, location);
            return true;
        }
        return false;
    }

    private boolean saveSelectedOfficeLocation() {
        if (spController == null) {
            spController = new SharedPreferenceController();
        }
//        Log.d("DSK ","applicationModeCode "+applicationModeCode);
        if (applicationModeCode == NumberConstants.kConstantNormalMode)
        {
            if (spController != null && spinnerOfficeLocation != null && spinnerOfficeLocation.getSelectedItem() != null
                    && !spinnerOfficeLocation.getSelectedItem().toString().trim().isEmpty()
                    && !spinnerOfficeLocation.getSelectedItem().toString().trim().equalsIgnoreCase(MainConstants.selectLocation))
            {
                String location = spinnerOfficeLocation.getSelectedItem().toString().trim();
//                Log.d("DSK ","location "+location);
                spController.saveOfficeLocation(this, location);
                spController.saveOfficeLocationCode(this,saveOfficeLocation_Code(location));
                return true;
            }
            else
            {
                UsedCustomToast.makeCustomToast(this, StringConstants.kConstantSelectOfficeLocation, Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        } else if (applicationModeCode == NumberConstants.kConstantDataCentreMode)
        {
            String location = MainConstants.purposeDataCenter;
//            Log.d("DSK ","location "+location);
            spController.saveOfficeLocation(this, location);
            spController.saveOfficeLocationCode(this,saveOfficeLocation_Code(location));
            return true;
        }
        return false;
    }

    private int saveOfficeLocation_Code(String officeLocation)
    {
        int officeLocation_code = 0;
        if(!officeLocation.isEmpty())
        {
            if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaChennaiDLF))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationIndiaChennaiDLF);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationIndiaChennaiEstancia);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaTenkasi))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeIndiaTenkasi);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationIndiaTenkasi);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeIndiaTenkasi;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationUSAustin))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeUSAustin);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationUSAustin);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeUSAustin;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationUSPleasanton))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeUSPleasanton);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationUSPleasanton);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeUSPleasanton;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationJapanYokohama))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeJapanYokohama);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationJapanYokohama);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeJapanYokohama;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationChinaBeijing))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeChinaBeijing);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationChinaBeijing);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeChinaBeijing;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta))
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantOfficeLocationCodeIndiaRenigunta);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantOfficeLocationIndiaAndhraRenigunta);
                officeLocation_code= MainConstants.kConstantOfficeLocationCodeIndiaRenigunta;
            }
            else if(officeLocation.equalsIgnoreCase(MainConstants.purposeDataCenter))
            {
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.purposeDataCenter);
                return officeLocation_code;
            }
            else
            {
                SingletonVisitorProfile.getInstance().setLocationCode(
                        MainConstants.kConstantDeveloperOfficeLocationCode);
                SingletonVisitorProfile.getInstance().setOfficeLocation(
                        MainConstants.kConstantDeveloperOfficeLocation);
                officeLocation_code= MainConstants.kConstantDeveloperOfficeLocationCode;
            }
        }
        return officeLocation_code;
    }

    /**
     * Check the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private void checkPermission(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission(requestCode);
            }
        }
    }

    /**
     * Request the app permission
     *
     * @author Karthick
     * @created 20170208
     */
    private void requestCameraPermission(int requestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                requestCode);
    }

    /**
     * Handle app permission response
     *
     * @author Karthick
     * @created 20170208
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == NumberConstants.requestExternalPermissionForBG) {
                initiateBackgroundProcess();
            } else if (requestCode == NumberConstants.requestExternalPermissionForExportDB) {
                exportDB();
            }
        } else {
            if (requestCode == NumberConstants.requestExternalPermissionForBG) {
                utility.UsedCustomToast
                        .makeCustomToast(getApplicationContext(),
                                StringConstants.kConstantBackgroundPermissionError,
                                Toast.LENGTH_LONG).show();
            } else if (requestCode == NumberConstants.requestExternalPermissionForExportDB) {
                utility.UsedCustomToast
                        .makeCustomToast(getApplicationContext(),
                                StringConstants.kConstantExportPermissionError,
                                Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthikeyan D S
     * @created on 20180424
     */
    public void onBackPressed() {
        super.onBackPressed();
        callWelcomeActivity();
    }

    /**
     * onDestroy
     *
     * @author Karthikeyan D S
     * @created on 20180424
     */
    public void onDestroy()
    {
        super.onDestroy();
        deallocateObjects();
    }

    private void deallocateObjects()
    {
//        if(buttonStartBackground!=null) {
//            buttonStartBackground = null;
//        }
//        if(buttonSave!=null) {
//        buttonSave=null;
//        }
//        if(buttonBack!=null) {
//        buttonBack=null;
//        }
//        if(buttonLogs!=null) {
//        buttonLogs=null;
//        }
//        if(buttonExportDb!=null) {
//        buttonExportDb=null;
//        }
//        if(buttonConfirm!=null) {
//        buttonConfirm=null;
//        }
//        if(buttonCancel!=null) {
//        buttonCancel=null;
//        }
//        if(typefaceButton!=null) {
//        typefaceButton=null;
//        }
//        if(textviewMemory!=null) {
//        textviewMemory=null;
//        }
//        if(deviceLocationTitle!=null) {
//        deviceLocationTitle=null;
//        }
//        if(textViewAppPassword!=null) {
//        textViewAppPassword=null;
//        }
//        if(editTextPassword != null) {
//        editTextPassword=null;
//        }
//        if(spinnerLocation!=null) {
//        spinnerLocation=null;
//        }
//        if(adapterLocation!=null) {
//        adapterLocation=null;
//        }
//        if(listLocation!=null && !listLocation.isEmpty()) {
//        listLocation=null;
//        }
//        if(imageViewBlackTheme!=null) {
//        imageViewBlackTheme=null;
//        }
//        if(imageViewMixedTheme!=null) {
//        imageViewMixedTheme=null;
//        }
//        if(applicationModeSwitch!=null) {
//        applicationModeSwitch=null;
//        }
//        if(applicationModeCode> NumberConstants.kConstantMinusOne) {
//        applicationModeCode = NumberConstants.kConstantMinusOne;
//        }
//        if(spController!=null) {
//        spController=null;
//        }
//        if(relativeLayoutChoosePurpose!=null) {
//        relativeLayoutChoosePurpose=null;
//        }
//        if(relativeLayoutPassword!=null) {
//        relativeLayoutPassword=null;
//        }
        if(appSettingsStoredPassword!=null && !appSettingsStoredPassword.isEmpty()) {
            appSettingsStoredPassword = MainConstants.kConstantEmpty;
        }
        requestForgotPassword = false;
        applicationModeCode = NumberConstants.kConstantZero;
    }
    /**
     * call WelcomePage Activity
     *
     * @author Karthikeyan D S
     * @created on 25OCT2018
     */
    private void callWelcomeActivity() {
        Intent backPress = new Intent(ActivityStartBackground.this, WelcomeActivity.class);
        startActivity(backPress);
        finish();
    }

    /**
     * request App Password
     *
     * @author Karthikeyan D S
     * @created on 29OCT2018
     */
    private int requestAppPassword()
    {
        String creatorResponseId = MainConstants.kConstantEmpty;
        int responecode = NumberConstants.kConstantMinusOne;
//        sendAppPasswordtoCreator
        if(spController==null)
        {
            spController = new SharedPreferenceController();
        }
        if(spController!=null)
        {
            String appSettingsPassword = spController.getAppSettingsPassword(ActivityStartBackground.this);
            if(appSettingsPassword!=null && !appSettingsPassword.isEmpty())
            {
                visitorZohoCreator= new VisitorZohoCreator(ActivityStartBackground.this);
                if(visitorZohoCreator!=null)
                {
                    creatorResponseId = visitorZohoCreator.sendAppPasswordtoCreator(ActivityStartBackground.this,appSettingsPassword);
//                    Log.d("DSK "," "+creatorResponseId);
                    if(creatorResponseId!=null && !creatorResponseId.isEmpty())
                    {
                        responecode = 1;
                    }
                    else {
                        responecode = 0;
                    }
                }
            }
            else
            {
                UsedCustomToast.makeCustomToast(ActivityStartBackground.this,"App Settings Password is Empty",Toast.LENGTH_SHORT,Gravity.TOP).show();
            }
        }
        return responecode;
    }



    /*
     * background process for upload app settings details
     */
    public class BackgroundProcessSendOTPRequest extends AsyncTask<String, String, String> {
        Context context;
        String contact;
        private int isOTPRequestSend = 0;

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... f_url) {
            if ( requestAppPassword()==1) {
//               OTP Request Sent.
                isOTPRequestSend = 1;
            } else {
//               Error OTP Request Not Sent to Creator.
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String responseParam) {
//            progressBarLoading.setVisibility(View.GONE);
            if (isOTPRequestSend == 1) {
                UsedCustomToast.makeCustomToast(ActivityStartBackground.this, StringConstants.kConstantRequestSentToAdmin,
                        Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
            else
            {
                UsedCustomToast.makeCustomToast(ActivityStartBackground.this, StringConstants.kConstantRequestNotSentCreatorIssue,
                        Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        }
    }

}
