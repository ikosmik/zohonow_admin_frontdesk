package com.zoho.app.frontdesk.android;

import java.util.Locale;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import constants.StringConstants;
import utility.BackgroundUploadVisitorData;
import utility.DialogViewWarning;
import appschema.SharedPreferenceController;
import utility.UsedCustomToast;
import utility.Validation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;

public class ActivityVisitorName extends Activity {

    private DialogViewWarning dialog;
    private EditText editTextVisitorName;
    private RelativeLayout relativeLayoutExisitingVisitor;
    private ImageView imageviewBack, imageviewHome, imageviewPrevious,
            imageviewNext, image_view_next;
    private TextView textViewErrorInName, textviewVisitorNameTitle;
    //private DbHelper dbHelper;
    private Typeface typeFaceTitle, typefaceVisitorName, typefaceErrorMsg;
    private int actionCode = MainConstants.backPress;
    private Handler noActionHandler;
    private TextToSpeech textToSpeech;
    private Validation validation = new Validation();
    private int fromPage = MainConstants.fromContact;
    private BackgroundUploadVisitorData backgroundProcess;
    int applicationMode =  NumberConstants.kConstantZero;
    private SharedPreferenceController sharedPreferenceController;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_visitor_name);

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        assignObjects();

        backgroundProcess = new BackgroundUploadVisitorData(this,
                MainConstants.jsonCreateUploadPhoto);
        if (backgroundProcess != null) {
            backgroundProcess.execute();
        }
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStart() {
        super.onStart();
        setOldValue();
    }

    /**
     * This function is called, when the activity is stopped.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStop() {
        super.onStop();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Assign Objects to the variable
     *
     * @author Karthick
     * @created on 20170508
     */
    private void assignObjects() {

        editTextVisitorName = (EditText) findViewById(R.id.editTextVisitorName);
        textViewErrorInName = (TextView) findViewById(R.id.textViewErrorInName);
        textviewVisitorNameTitle = (TextView) findViewById(R.id.textview_visitor_name_title);

        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

        relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        if (getIntent() != null) {
            fromPage = getIntent().getIntExtra(MainConstants.putExtraFromPage, MainConstants.fromContact);
        }

        sharedPreferenceController = new SharedPreferenceController();
        if (sharedPreferenceController != null) {
            applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
        }

        editTextVisitorName
                .setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                        | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        assignFontConstants();
        assignListreners();

        setAppBackground();
    }

    /**
     * Assign Font Constants
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void assignFontConstants() {
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceErrorMsg = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskItalic);

        typefaceVisitorName = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);

        editTextVisitorName.setTypeface(typefaceVisitorName);
        textViewErrorInName.setTypeface(typefaceErrorMsg);
        textviewVisitorNameTitle.setTypeface(typeFaceTitle);
    }

    /**
     * Assign Listeners
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void assignListreners() {
        // Onclick listeners
        if (imageviewPrevious!=null && listenerPrevious!=null) {
            imageviewPrevious.setOnClickListener(listenerPrevious);
        } if (imageviewNext!=null && listenerConfirm!=null) {
            imageviewNext.setOnClickListener(listenerConfirm);
        } if (imageviewHome!=null && listenerHomePage!=null) {
            imageviewHome.setOnClickListener(listenerHomePage);
        } if (imageviewBack!=null && listenerBack!=null) {
            imageviewBack.setOnClickListener(listenerBack);
        } if (image_view_next!=null && listenerConfirm!=null) {
            image_view_next.setOnClickListener(listenerConfirm);
        }
        editTextVisitorName.setOnEditorActionListener(listenerDonePressed);
        editTextVisitorName.addTextChangedListener(textWatcherNameChanged);
    }

    /**
     * Set background color
     *
     * @author Karthick
     * @created on 20170508
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP, new int[]{
                        0xFF4bbcef, 0xFF46c0e5, 0xFF3ec5d4, 0xFF35ccc1});
                relativeLayoutExisitingVisitor.setBackground(gd);
            }
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerBack = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            //showDetailDialog();
        }
    };

    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerHomePage = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.homePress;
            hideKeyboard();
            showDetailDialog();
        }
    };

    /**
     * when the done pressed then process start.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                if (validateName()) {
                    launchNextPage();
                }
                return true;
            }
            return false;
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerConfirm = new OnClickListener() {
        public void onClick(View v) {
            if (validateName()) {
                launchNextPage();
            }
            else
            {
                UsedCustomToast.makeCustomToast(ActivityVisitorName.this,"Not Valid", Toast.LENGTH_SHORT, Gravity.TOP).show();
            }
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerPrevious = new OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchHomePage() {
        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, HomePage.class);
            startActivity(intent);
        }
        finish();
    }

    /**
     * Launch next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchVisitorDetailPage() {
        Intent intent = new Intent(getApplicationContext(),
                ActivityExistingVisitorDetails.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch previous activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchPreviousPage() {
        if (SingletonVisitorProfile.getInstance().getVisitorType() == MainConstants.editVisitor) {
            Intent intent = new Intent(getApplicationContext(),
                    ActivityExistingVisitorDetails.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(),
                    ExistingVisitorActivity.class);
            startActivity(intent);
        }
        finish();
    }

    /**
     * Launch next page activity based on the Device Location.
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    private void launchNextPage() {
        if (applicationMode == NumberConstants.kConstantDataCentreMode || applicationMode == NumberConstants.kConstantNormalMode) {
            Intent intentNextPage = new Intent(this, ActivityCompanyDetails.class);
            startActivity(intentNextPage);
            finish();
        } /*else if (applicationMode == NumberConstants.kConstantNormalMode) {
            String deviceLocation = MainConstants.kConstantEmpty;
            if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getDeviceLocation() != null
                    && !SingletonMetaData.getInstance().getDeviceLocation().isEmpty()) {
                deviceLocation = SingletonMetaData.getInstance().getDeviceLocation();
            }
//			Log.d("DSKActivityCode1 ", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//			Log.d("DSKEmployeeId1 ", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
//			Log.d("DSKdeviceLocation ", "" + deviceLocation);
            if (deviceLocation != null && !deviceLocation.isEmpty() &&
                    deviceLocation.equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
//			Log.d("DSKActivityCode1 ", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//			Log.d("DSKEmployeeId1 ", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
//			Log.d("DSKdeviceLocation ", "" + deviceLocation);
                if (SingletonVisitorProfile.getInstance() != null &&
                        SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 1) {
//				Log.d("EmployeeActivityCod2", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//				Log.d("EmployeeId2", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    callNextPageActivity(1);
                } else if (SingletonVisitorProfile.getInstance() != null &&
                        SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 2) {
//				Log.d("EmployeeActivityCode3", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//				Log.d("EmployeeId3", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                    if (SingletonEmployeeProfile.getInstance() != null &&
                            !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty() &&
                            SingletonEmployeeProfile.getInstance().getEmployeeId().startsWith("ZT-")) {
//					Log.d("EmployeeActivityCode4", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId4", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                        callNextPageActivity(2);
                    } else if (SingletonEmployeeProfile.getInstance() != null &&
                            !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty() &&
                            SingletonEmployeeProfile.getInstance().getEmployeeId() != null) {
//					Log.d("EmployeeActivityCode5", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId5", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                        callNextPageActivity(1);
                    } else {
//					Log.d("EmployeeActivityCode6", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId6", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                        callNextPageActivity(2);
                    }
                } else if (SingletonVisitorProfile.getInstance() != null &&
                        SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 3) {
                    callNextPageActivity(1);
                } else {
                    if (SingletonEmployeeProfile.getInstance() != null &&
                            SingletonEmployeeProfile.getInstance().getEmployeeId() != null
                            && !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()
                            && SingletonEmployeeProfile.getInstance().getEmployeeId().startsWith("ZT-")) {
//					Log.d("EmployeeActivityCode7", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId7", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                        callNextPageActivity(2);
                    } else if (SingletonEmployeeProfile.getInstance() != null &&
                            SingletonEmployeeProfile.getInstance().getEmployeeId() != null &&
                            !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()) {
//					Log.d("EmployeeActivityCode8", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId8", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                        callNextPageActivity(1);
                    } else {
//					Log.d("EmployeeActivityCode9", "" + SingletonVisitorProfile.getInstance().getEmployeeActivityCode());
//					Log.d("EmployeeId9", "" + SingletonEmployeeProfile.getInstance().getEmployeeId());
                        callNextPageActivity(2);
                    }
                }

            } else {
                if (SingletonVisitorProfile.getInstance() != null &&
                        SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 1) {
                    callNextPageActivity(1);

                } else if (SingletonVisitorProfile.getInstance() != null &&
                        SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 2) {
                    callNextPageActivity(2);
                } else {
                    callNextPageActivity(1);
                }
            }
        }*/
    }


    /**
     * Launch next page activity.
     *
     * @author Karthikeyan D S
     * @created on 20180403
     */
    private void callNextPageActivity(int nextPageActivityCode) {
        if (nextPageActivityCode == 1) {
            Intent intent = new Intent(getApplicationContext(),
                    ActivityVisitorEmployee.class);
            startActivity(intent);
            finish();
        } else if (nextPageActivityCode == 2) {
            Intent intent = new Intent(getApplicationContext(),
                    ActivityVisitorEmployeeTenkasi.class);
            startActivity(intent);
            finish();
        }
    }


    /**
     * Goto home page when no action.
     *
     * @author Karthick
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };

    /**
     * This function is called when the back pressed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * textWatcher to validate the VisitorMobileNo
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextWatcher textWatcherNameChanged = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            validateNameTyped();
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
        }

        public void afterTextChanged(Editable s) {
        }

    };

    /**
     * Check is existing user or not
     *
     * @author Karthick
     * @created 20170508
     */
    private void validateNameTyped() {
        if ((editTextVisitorName != null)
                && (!editTextVisitorName.getText().toString().trim().isEmpty())
                && (editTextVisitorName.getText().toString().trim().length() > MainConstants.kConstantZero)) {
            validateName();
        } else {
            textViewErrorInName.setVisibility(View.INVISIBLE);
        }
        if (imageviewNext != null) {
            if (editTextVisitorName != null
                    && editTextVisitorName.getText().toString().trim().length() >= MainConstants.kConstantThree) {
                if (imageviewNext != null) {
                    imageviewNext.setVisibility(View.VISIBLE);
                }
                if (image_view_next != null) {
                    image_view_next.setVisibility(View.VISIBLE);
                }
            } else {
                if (imageviewNext != null) {
                    imageviewNext.setVisibility(View.GONE);
                }
                if (image_view_next != null) {
                    image_view_next.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * Check is existing user or not
     *
     * @author Karthick
     * @created 20170508
     */
    private boolean validateName() {
        String editTextVisitor = editTextVisitorName.getText().toString().trim();
        boolean isValidVisitorName = false;
        if (editTextVisitor != null && !editTextVisitor.isEmpty()
                && (editTextVisitor.length() > MainConstants.kConstantZero)) {
            if (!editTextVisitor.matches(MainConstants.kConstantValidText)) {
                textViewErrorInName.setVisibility(View.VISIBLE);
                isValidVisitorName =false;
            } else {
                textViewErrorInName.setVisibility(View.GONE);
                editTextVisitorName.setTextColor(Color
                        .parseColor(MainConstants.color_code_efefef));
                if (SingletonVisitorProfile.getInstance() != null) {
                    SingletonVisitorProfile.getInstance().setVisitorName(editTextVisitor);
                }
                isValidVisitorName =true;
            }
        } else {
            isValidVisitorName =false;
        }
        return isValidVisitorName;
    }

    /**
     * Hide keyboard
     *
     * @author Karthick
     * @created on 20170508
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showDetailDialog() {
        showWarningDialog();
        textSpeach(MainConstants.speachAreYouSureCode);
    }

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showWarningDialog() {
        hideKeyboard();
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    /**
     * Handle tab click for goto settings page
     *
     * @author Karthick
     * @created on 20170508
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            launchHomePage();
        }
    };

    /**
     * Set singleton name value to edittext
     *
     * @author Karthick
     * @created on 20170508
     */
    private void setOldValue() {
        if (editTextVisitorName != null
                && SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getVisitorName() != null
                && !SingletonVisitorProfile.getInstance().getVisitorName()
                .trim().isEmpty()) {
            editTextVisitorName
                    .removeTextChangedListener(textWatcherNameChanged);
            editTextVisitorName.setText(SingletonVisitorProfile.getInstance()
                    .getVisitorName().trim());
            editTextVisitorName.setSelection(SingletonVisitorProfile
                    .getInstance().getVisitorName().trim().length());
            editTextVisitorName.addTextChangedListener(textWatcherNameChanged);
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.VISIBLE);
            }
            if (image_view_next != null) {
                image_view_next.setVisibility(View.VISIBLE);
            }
        } else {
            if (imageviewNext != null) {
                imageviewNext.setVisibility(View.GONE);
            }
            if (image_view_next != null) {
                image_view_next.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {

        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (editTextVisitorName != null
                    && editTextVisitorName.getText() != null && editTextVisitorName
                    .getText().toString().length() <= 0)) {
                textToSpeech.speak(TextToSpeachConstants.speachName,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void deallocateObjects() {
        editTextVisitorName = null;
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        image_view_next = null;
        textViewErrorInName = null;
        textviewVisitorNameTitle = null;
//		dbHelper = null;
        typeFaceTitle = null;
        typefaceVisitorName = null;
        relativeLayoutExisitingVisitor
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutExisitingVisitor = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (backgroundProcess != null) {
            backgroundProcess.cancel(true);
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        applicationMode= NumberConstants.kConstantZero;
    }
}
