package com.zoho.app.frontdesk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import utility.DialogViewWarning;
import appschema.SharedPreferenceController;
import utility.UsedCustomToast;

/**
 * Created by Karthikeyan D S on 27July2018
 */
public class ActivityDataCentreDeviceDetails extends Activity implements CompoundButton.OnCheckedChangeListener {

    private DialogViewWarning dialog;
    private RelativeLayout relativeLayoutDeviceDetails, relativeLayoutImageSerailNumber;
    private TextView textViewDeclarationHeader, textViewLaptopDetailsHeader, textViewHaveOtherDevicesHeader,
            textViewFindSerialNum;
    private EditText editTextMake, editTextSerialNumber, editTextOtherDetails, editTextOtherLaptopDetails;
    private ImageView imageviewBack, imageviewHome, imageviewPrevious, image_view_next, imageViewSerailNumbeImage,
            imageviewNext;
    private Typeface typeFaceTitle, typeFaceAccept, typeFaceWarning;
    private Handler noActionHandler;
    private TextToSpeech textToSpeech;
    private int applicationMode = NumberConstants.kConstantZero;
    private int actionCode = MainConstants.backPress;
    private SharedPreferenceController sharedPreferenceController;
    private Switch anyOtherDevicesSwitch;
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    private ImageView mImageView;
    //The "x" and "y" position of the "TextView" on screen.
    private Point point;
    private Button buttonCloseSerialImage;
    private RadioGroup lapDetailsRadioGroup,otherDevicesRadioGroup;
    private RadioButton radioButtonAssus, radioButtonAcer, radioButtonDell, radioButtonHp, radioButtonLenovo,
            radioButtonMac, radioButtonSelectedDevice, radioButtonOthersLaptopBrands;

    protected void onCreate(Bundle SavedInstanceState) {
        super.onCreate(SavedInstanceState);
//       remove title bar.
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_datacentre_device_details);

        assignObjects();
        setOldValue();
    }

    /**
     * assignObjects
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void assignObjects() {
        relativeLayoutImageSerailNumber = (RelativeLayout) findViewById(R.id.relative_layout_serial_image_info);
        textViewDeclarationHeader = (TextView) findViewById(R.id.textview_device_title);
        textViewLaptopDetailsHeader = (TextView) findViewById(R.id.text_view_laptop_details_header);
        textViewHaveOtherDevicesHeader = (TextView) findViewById(R.id.text_view_have_other_devices);

        textViewFindSerialNum = (TextView) findViewById(R.id.text_view_find_serial_number);
        relativeLayoutDeviceDetails = (RelativeLayout) findViewById(R.id.relativeLayout_device_details);

        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

        editTextSerialNumber = (EditText) findViewById(R.id.edit_text_serial_number);
//        editTextMake = (EditText) findViewById(R.id.edit_text_laptop_name);
        editTextOtherDetails = (EditText) findViewById(R.id.edit_text_other_devices);
        editTextOtherLaptopDetails = (EditText) findViewById(R.id.edit_text_other_laptop_data);

        lapDetailsRadioGroup = (RadioGroup) findViewById(R.id.radioGroupDeviceDetails);
        radioButtonAcer = (RadioButton) findViewById(R.id.radioButtonAcer);
        radioButtonAssus = (RadioButton) findViewById(R.id.radioButtonAssus);
        radioButtonDell = (RadioButton) findViewById(R.id.radioButtonDell);
        radioButtonHp = (RadioButton) findViewById(R.id.radioButtonHp);
        radioButtonLenovo = (RadioButton) findViewById(R.id.radioButtonLenovo);
        radioButtonMac = (RadioButton) findViewById(R.id.radioButtonMac);
        radioButtonOthersLaptopBrands = (RadioButton) findViewById(R.id.radioButtonOtherLaptopBrands);

        otherDevicesRadioGroup = (RadioGroup) findViewById(R.id.radioGroupOtherDeviceDetails);
        lapDetailsRadioGroup = (RadioGroup) findViewById(R.id.radioGroupDeviceDetails);

        buttonCloseSerialImage = (Button) findViewById(R.id.button_close);
        imageViewSerailNumbeImage = (ImageView) findViewById(R.id.image_view_serial_num_suggestion);

        anyOtherDevicesSwitch = (Switch) findViewById(R.id.switch_any_device);

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        sharedPreferenceController = new SharedPreferenceController();
        if (sharedPreferenceController != null) {
            applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
        }

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        if (anyOtherDevicesSwitch != null) {
            anyOtherDevicesSwitch.setOnCheckedChangeListener(this);
        }

//        Assign FontConstants
        assignFontConstants();

//       assign Listenrs
        assignListeners();

//        set AppBackground
        setAppBackground();

    }

    /**
     * assignFontConstants
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void assignFontConstants() {
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typeFaceAccept = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskSemiBold);
        typeFaceWarning = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskItalic);

        if (textViewDeclarationHeader != null && typeFaceTitle != null) {
            textViewDeclarationHeader.setTypeface(typeFaceTitle);
        }
        if (textViewLaptopDetailsHeader != null && typeFaceTitle != null) {
            textViewLaptopDetailsHeader.setTypeface(typeFaceTitle);
        }
        if (textViewHaveOtherDevicesHeader != null && typeFaceTitle != null) {
            textViewHaveOtherDevicesHeader.setTypeface(typeFaceTitle);
        }
        if (radioButtonMac != null && typeFaceTitle != null) {
            radioButtonMac.setTypeface(typeFaceTitle);
        }
        if (radioButtonLenovo != null && typeFaceTitle != null) {
            radioButtonLenovo.setTypeface(typeFaceTitle);
        }
        if (radioButtonHp != null && typeFaceTitle != null) {
            radioButtonHp.setTypeface(typeFaceTitle);
        }
        if (radioButtonDell != null && typeFaceTitle != null) {
            radioButtonDell.setTypeface(typeFaceTitle);
        }
        if (radioButtonAssus != null && typeFaceTitle != null) {
            radioButtonAssus.setTypeface(typeFaceTitle);
        }
        if (radioButtonAcer != null && typeFaceTitle != null) {
            radioButtonAcer.setTypeface(typeFaceTitle);
        }
        if (radioButtonOthersLaptopBrands != null && typeFaceTitle != null) {
            radioButtonOthersLaptopBrands.setTypeface(typeFaceTitle);
        }
//        editTextMake.setTypeface(typeFaceTitle);
        if (editTextOtherDetails != null && typeFaceTitle != null) {
            editTextOtherDetails.setTypeface(typeFaceTitle);
        }
        if (editTextSerialNumber != null && typeFaceTitle != null) {
            editTextSerialNumber.setTypeface(typeFaceTitle);
        }
        if (editTextOtherLaptopDetails != null && typeFaceTitle != null) {
            editTextOtherLaptopDetails.setTypeface(typeFaceTitle);
        }
        if (anyOtherDevicesSwitch != null && typeFaceTitle != null) {
            anyOtherDevicesSwitch.setTypeface(typeFaceTitle);
        }
    }

    /**
     * assignListeners
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void assignListeners() {
        // Onclick listeners
        if (imageviewPrevious != null && listenerPrevious != null) {
            imageviewPrevious.setOnClickListener(listenerPrevious);
        }
        if (imageviewNext != null && listenerConfirm != null) {
            imageviewNext.setOnClickListener(listenerConfirm);
        }
        if (image_view_next != null && listenerConfirm != null) {
            image_view_next.setOnClickListener(listenerConfirm);
        }
        if (imageviewHome != null && listenerHomePage != null) {
            imageviewHome.setOnClickListener(listenerHomePage);
        }
        if (imageviewBack != null && listenerBack != null) {
            imageviewBack.setOnClickListener(listenerBack);
        }

        if (textViewFindSerialNum != null && listenerFindSerailNumber != null) {
            textViewFindSerialNum.setOnClickListener(listenerFindSerailNumber);
        }

        if (buttonCloseSerialImage != null && listenerCloseSerailNumber != null) {
            buttonCloseSerialImage.setOnClickListener(listenerCloseSerailNumber);
        }
        if (editTextSerialNumber != null && textWatcherLaptopSerialNo != null) {
            editTextSerialNumber.addTextChangedListener(textWatcherLaptopSerialNo);
        }
//        if (editTextMake != null && textWatcherLaptopMake != null) {
//            editTextMake.addTextChangedListener(textWatcherLaptopMake);
//        }
        if (editTextOtherDetails != null && textWatcherothers != null) {
            editTextOtherDetails.addTextChangedListener(textWatcherothers);
        }

        lapDetailsRadioGroup.clearCheck(); // this is so we can start fresh, with no selection on both RadioGroups
        otherDevicesRadioGroup.clearCheck();
        otherDevicesRadioGroup.setOnCheckedChangeListener(otherDevicesRadioGroupListener);
        lapDetailsRadioGroup.setOnCheckedChangeListener(lapDetailsRadioGroupListener);
    }

    RadioGroup.OnCheckedChangeListener otherDevicesRadioGroupListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                lapDetailsRadioGroup.setOnCheckedChangeListener(null); // remove the listener before clearing so we don't throw that stackoverflow exception(like Vladimir Volodin pointed out)
                lapDetailsRadioGroup.clearCheck(); // clear the second RadioGroup!
                lapDetailsRadioGroup.setOnCheckedChangeListener(lapDetailsRadioGroupListener); //reset the listener
                editTextOtherLaptopDetails.setVisibility(View.VISIBLE);
                editTextOtherLaptopDetails.requestFocus();
            }
        }
    };

    RadioGroup.OnCheckedChangeListener lapDetailsRadioGroupListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                otherDevicesRadioGroup.setOnCheckedChangeListener(null);
                otherDevicesRadioGroup.clearCheck();
                otherDevicesRadioGroup.setOnCheckedChangeListener(otherDevicesRadioGroupListener);
                editTextOtherLaptopDetails.setVisibility(View.INVISIBLE);
            }
        }
    };

    /**
     * Set background color
     *
     * @author Karthikeyan D S
     * @created on 02JUl2018
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutDeviceDetails.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFba6fb6, 0xFF8e76c3, 0xFF8478c6, 0xFF8278c7});
                relativeLayoutDeviceDetails.setBackground(gd);
            }
        }
    }
    /**
     * intent to launch previous page
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            // showDetailDialog();
        }
    };
    private View.OnClickListener listenerFindSerailNumber = new View.OnClickListener() {
        public void onClick(View v) {
            relativeLayoutImageSerailNumber.setVisibility(View.VISIBLE);
            imageViewSerailNumbeImage.setVisibility(View.VISIBLE);
            buttonCloseSerialImage.setVisibility(View.VISIBLE);
//            showPopup(ActivityDataCentreDeviceDetails.this);
            hideKeyboard();
            hideViewContent();
        }
    };

    private View.OnClickListener listenerCloseSerailNumber = new View.OnClickListener() {
        public void onClick(View v) {
            relativeLayoutImageSerailNumber.setVisibility(View.GONE);
            imageViewSerailNumbeImage.setVisibility(View.GONE);
            buttonCloseSerialImage.setVisibility(View.GONE);
//            showPopup(ActivityDataCentreDeviceDetails.this);
            showViewContent();
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private View.OnClickListener listenerConfirm = new View.OnClickListener() {
        public void onClick(View v) {
            if (validateLaptopDetails()) {
                if (storeValues()) {
                    launchNextPage();
                }
            }
        }
    };

    /**
     * Handle tab click for goto settings page
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            launchHomePage();
        }
    };
    /**
     * intent to launch previous page
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private View.OnClickListener listenerHomePage = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.homePress;
            hideKeyboard();
            showDetailDialog();
        }
    };
    /**
     * Launch previous activity.
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private View.OnClickListener listenerPrevious = new View.OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };
    /**
     * Play text to voice
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * hide View
     *
     * @author Karthikeyan D S
     * @Created on 29JUL2018
     */
    private void hideViewContent() {
        textViewFindSerialNum.setVisibility(View.GONE);
        textViewLaptopDetailsHeader.setVisibility(View.GONE);
        textViewHaveOtherDevicesHeader.setVisibility(View.GONE);
        textViewFindSerialNum.setVisibility(View.GONE);
        textViewDeclarationHeader.setVisibility(View.GONE);
        editTextOtherDetails.setVisibility(View.INVISIBLE);
        editTextSerialNumber.setVisibility(View.GONE);
//        editTextMake.setVisibility(View.GONE);
        lapDetailsRadioGroup.setVisibility(View.GONE);

        radioButtonOthersLaptopBrands.setVisibility(View.GONE);
        editTextOtherLaptopDetails.setVisibility(View.INVISIBLE);
        image_view_next.setVisibility(View.INVISIBLE);
        imageviewPrevious.setVisibility(View.INVISIBLE);
        imageviewBack.setVisibility(View.GONE);
        imageviewNext.setVisibility(View.GONE);

        anyOtherDevicesSwitch.setVisibility(View.GONE);
    }

    /**
     * Show hide View
     *
     * @author Karthikeyan D S
     * @Created on 29JUL2018
     */
    private void showViewContent() {
        textViewFindSerialNum.setVisibility(View.VISIBLE);
        textViewLaptopDetailsHeader.setVisibility(View.VISIBLE);
        textViewHaveOtherDevicesHeader.setVisibility(View.VISIBLE);
        textViewFindSerialNum.setVisibility(View.VISIBLE);
        textViewDeclarationHeader.setVisibility(View.VISIBLE);

        if (anyOtherDevicesSwitch.isChecked()) {
            editTextOtherDetails.setVisibility(View.VISIBLE);
        }
        radioButtonOthersLaptopBrands.setVisibility(View.VISIBLE);
        if (radioButtonOthersLaptopBrands.isChecked()) {
            editTextOtherLaptopDetails.setVisibility(View.VISIBLE);
        }
        textViewFindSerialNum.setVisibility(View.VISIBLE);

        editTextSerialNumber.setVisibility(View.VISIBLE);
//        editTextMake.setVisibility(View.VISIBLE);

        lapDetailsRadioGroup.setVisibility(View.VISIBLE);

        image_view_next.setVisibility(View.VISIBLE);
        imageviewPrevious.setVisibility(View.VISIBLE);
        imageviewBack.setVisibility(View.VISIBLE);
        imageviewNext.setVisibility(View.VISIBLE);

        anyOtherDevicesSwitch.setVisibility(View.VISIBLE);
    }

    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };
    /**
     * textWatcher to validate the Laptop SerialNo
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    public TextWatcher textWatcherLaptopSerialNo = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            }
//            setAutoCheckinDeviceHandler();
        }

        public void afterTextChanged(Editable s) {
        }
    };
    /**
     * textWatcher to validate the others Field Value
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    public TextWatcher textWatcherothers = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            }
//            setAutoCheckinDeviceHandler();
        }

        public void afterTextChanged(Editable s) {
        }
    };
    /**
     * textWatcher to validate the Laptop make
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    public TextWatcher textWatcherLaptopMake = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            setAutoCheckinDeviceHandler();
        }

        public void afterTextChanged(Editable s) {
        }
    };

    /**
     * ApplicationMode Switch onCheckChanged Listener.
     *
     * @author Karthikeyan D S
     * @Created on 20180521
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            }

            editTextOtherDetails.setVisibility(View.VISIBLE);
//            editTextOtherDetails.setFocusable(true);
            editTextOtherDetails.requestFocus();
        } else {
            editTextOtherDetails.setVisibility(View.INVISIBLE);
            if (editTextOtherDetails != null && editTextOtherDetails.getText().toString().trim() != null
                    && !editTextOtherDetails.getText().toString().trim().isEmpty()) {
                editTextOtherDetails.setText(MainConstants.kConstantEmpty);
            }
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void showDetailDialog() {
        showWarningDialog();
        textSpeach(MainConstants.speachAreYouSureCode);
    }

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void showWarningDialog() {
        hideKeyboard();
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    /**
     * Launch previous activity
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void launchHomePage() {
        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        } else {
            launchPreviousPage();
        }
        finish();
    }

    /**
     * Launch previous activity.
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void launchPreviousPage() {
        Intent intent = new Intent(getApplicationContext(),
                ActivityHaveAnyDevices.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch next activity.
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void launchNextPage() {
        Intent intent = new Intent(getApplicationContext(),
                ActivityVisitorTempID.class);
        startActivity(intent);
        finish();
    }

    /**
     * remove Handlers
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void setAutoCheckinDeviceHandler() {
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
        }
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * Hide keyboard
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void textSpeach(int textSpeachCode) {
        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (SingletonVisitorProfile.getInstance() != null
                    && SingletonVisitorProfile.getInstance()
                    .getPurposeOfVisitCode() != null && SingletonVisitorProfile
                    .getInstance().getPurposeOfVisitCode().isEmpty())) {
//				textToSpeech.speak(TextToSpeachConstants.speachPurpose,
//						TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * validate the Laptop details
     *
     * @author Karthikeyan D S
     * @created on 20AUG2018
     */
    private boolean validateLaptopDetails() {
        boolean isValid = true;
        boolean isHaveSerialNumber = true;
        // get selected radio button from radioGroup
        int selectedIdLaptopModel = lapDetailsRadioGroup.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        radioButtonSelectedDevice = (RadioButton) findViewById(selectedIdLaptopModel);

        String selectedDeviceModel = MainConstants.kConstantEmpty;

        if (radioButtonSelectedDevice != null && radioButtonSelectedDevice.getText() != null) {
            selectedDeviceModel = radioButtonSelectedDevice.getText().toString().trim();
            if (selectedDeviceModel != null && !selectedDeviceModel.isEmpty()) {
                isHaveSerialNumber = true;
            } else {
                isHaveSerialNumber = false;
                UsedCustomToast.makeCustomToast(getApplicationContext(), StringConstants.kConstantDeviceModel, Toast.LENGTH_SHORT, Gravity.TOP).show();
            }

        } else if (radioButtonOthersLaptopBrands != null && radioButtonOthersLaptopBrands.isChecked()) {
            String otherLapDetails = radioButtonOthersLaptopBrands.getText().toString().trim();
//            Log.d("DSK ",""+selectedDeviceModel);
            if (otherLapDetails != null && !otherLapDetails.isEmpty()) {
//                Log.d("DSK ",""+selectedDeviceModel);
                selectedDeviceModel = editTextOtherLaptopDetails.getText().toString().trim();
                if (selectedDeviceModel != null && !selectedDeviceModel.isEmpty()) {
                    isHaveSerialNumber = true;
                } else {
                    isHaveSerialNumber = false;
                    isValid = false;
                    UsedCustomToast.makeCustomToast(getApplicationContext(), StringConstants.kConstantOtherDeviceModel, Toast.LENGTH_SHORT, Gravity.TOP).show();
                }
            } else {
                isHaveSerialNumber = false;
            }
        } else {
            isHaveSerialNumber = false;
        }
        if (isHaveSerialNumber) {
            if (selectedDeviceModel != null && !selectedDeviceModel.isEmpty()) {
                SingletonVisitorProfile.getInstance().setDeviceMake(selectedDeviceModel);
                isValid = true;
            }
            if (editTextSerialNumber != null
                    && editTextSerialNumber.getText().toString().trim() != null && isValid)
            {
                if (!editTextSerialNumber.getText().toString().trim().isEmpty()) {
                    if (editTextSerialNumber.getText().toString().trim().matches(MainConstants.kConstantValidCompanyName)) {
                        SingletonVisitorProfile.getInstance().setDeviceSerialNumber(
                                editTextSerialNumber.getText().toString().trim());
                    } else {
                        isValid = false;
                        UsedCustomToast.makeCustomToast(getApplicationContext(), StringConstants.kConstantValidSerialNumber, Toast.LENGTH_SHORT, Gravity.TOP).show();
                    }
                }
                else {
                    isValid = false;
                    UsedCustomToast.makeCustomToast(getApplicationContext(), StringConstants.kConstantSerialNumber, Toast.LENGTH_SHORT, Gravity.TOP).show();
                }
            }
        }
        return isValid;
    }

    /**
     * validate the Laptop details
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private boolean validate() {

        boolean isValid = false;
        if (editTextMake != null
                && editTextMake.getText() != null
                && !editTextMake.getText().toString().trim().isEmpty()
                && editTextSerialNumber != null
                && editTextSerialNumber.getText() != null
                && !editTextSerialNumber.getText().toString().trim()
                .isEmpty()) {
            isValid = true;
        } else if ((editTextMake != null
                && editTextMake.getText() != null
                && editTextMake.getText().toString().trim().isEmpty())
                && (editTextSerialNumber != null
                && editTextSerialNumber.getText() != null
                && editTextSerialNumber.getText().toString().trim()
                .isEmpty())) {
            isValid = true;
        } else if ((editTextMake != null
                && editTextMake.getText() != null
                && !editTextMake.getText().toString().trim().isEmpty())
                && (editTextSerialNumber != null
                && editTextSerialNumber.getText() != null
                && editTextSerialNumber.getText().toString().trim()
                .isEmpty())) {
            isValid = true;
        }

        // assign value to singleton object
        if (editTextMake != null && editTextMake.getText() != null
                && isValid) {
            SingletonVisitorProfile.getInstance().setDeviceMake(
                    editTextMake.getText().toString().trim());

        }
        if (editTextSerialNumber != null
                && editTextSerialNumber.getText() != null && isValid) {
            SingletonVisitorProfile.getInstance().setDeviceSerialNumber(
                    editTextSerialNumber.getText().toString().trim());
        }

//        if (editTextMake != null && editTextMake.getText() != null
//                && !editTextMake.getText().toString().trim().isEmpty()) {
//            editTextMake
//                    .setBackgroundResource(R.drawable.cell_underline_white);
//        } else if (isValid == false) {
//            UsedCustomToast.makeCustomToast(this, MainConstants.enterLaptopMake, Toast.LENGTH_SHORT, Gravity.TOP).show();
//        }

//		if (editTextSerialNumber != null
//				&& editTextSerialNumber.getText() != null
//				&& !editTextSerialNumber.getText().toString().trim()
//						.isEmpty()) {
//		} else if(isValid==false){
//			UsedCustomToast.makeCustomToast(this, MainConstants.enterLaptopSerialNumber, Toast.LENGTH_SHORT, Gravity.TOP).show();
//		}
        return isValid;
    }

    /**
     * Store value to singleton
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private boolean storeValues() {
        boolean isValid = false;
        String editTextOtherDetailsValue = MainConstants.kConstantEmpty;
        editTextOtherDetailsValue = editTextOtherDetails.getText().toString().trim();
        if (SingletonVisitorProfile.getInstance() != null) {
            if (editTextOtherDetailsValue != null && !editTextOtherDetailsValue.isEmpty()
                    && editTextOtherDetailsValue.length() > 0) {
                if (editTextOtherDetailsValue.matches(MainConstants.kConstantValidAllCharacterswithoutSmiley)) {
                    editTextOtherDetailsValue = editTextOtherDetailsValue.replaceAll("\\n", ",");
                    SingletonVisitorProfile.getInstance().setOtherDeviceDetails(editTextOtherDetailsValue);
                    isValid = true;
                } else {
                    isValid = false;
                    UsedCustomToast.makeCustomToast(getApplicationContext(), "Enter Valid Other Devices", Toast.LENGTH_LONG, Gravity.TOP).show();
                }
            } else {
                SingletonVisitorProfile.getInstance().setOtherDeviceDetails(editTextOtherDetailsValue);
                isValid = true;
            }
        }
        return isValid;
    }

    /**
     * Hide keyboard
     *
     * @author Karthikeyan D S
     * @created on 27JUL2018
     */
    private void setOldValue() {
        if (SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getDeviceMake() != null) {
            String selectedDeviceDetails = SingletonVisitorProfile.getInstance().getDeviceMake();
//            Log.d("DSK ",""+selectedDeviceDetails);
            if (selectedDeviceDetails != null && !selectedDeviceDetails.isEmpty()) {
//                Log.d("DSK ",""+selectedDeviceDetails);
                if (selectedDeviceDetails.equalsIgnoreCase("Asus")) {
                    radioButtonAssus.setChecked(true);
                } else if (selectedDeviceDetails.equalsIgnoreCase("Acer")) {
                    radioButtonAcer.setChecked(true);
                } else if (selectedDeviceDetails.equalsIgnoreCase("Dell")) {
                    radioButtonDell.setChecked(true);
                } else if (selectedDeviceDetails.equalsIgnoreCase("HP")) {
                    radioButtonHp.setChecked(true);
                } else if (selectedDeviceDetails.equalsIgnoreCase("Lenovo")) {
                    radioButtonLenovo.setChecked(true);
                } else if (selectedDeviceDetails.equalsIgnoreCase("Apple")) {
                    radioButtonMac.setChecked(true);
                } else {
                    radioButtonOthersLaptopBrands.setChecked(true);
                    editTextOtherLaptopDetails.setText(selectedDeviceDetails);
                }
            }

        }
        if (editTextSerialNumber != null && SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getDeviceSerialNumber() != null) {
            editTextSerialNumber.setText(SingletonVisitorProfile.getInstance().getDeviceSerialNumber());
            editTextSerialNumber.setSelection(SingletonVisitorProfile.getInstance().getDeviceSerialNumber().length());
        }

        if (editTextOtherDetails != null && SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getOtherDeviceDetails() != null &&
                !SingletonVisitorProfile.getInstance().getOtherDeviceDetails().isEmpty()) {
            anyOtherDevicesSwitch.setChecked(true);
            editTextOtherDetails.setText(SingletonVisitorProfile.getInstance().getOtherDeviceDetails().replaceAll(",", "\n"));
//            editTextOtherDetails.setSelection(SingletonVisitorProfile.getInstance().getOtherDeviceDetails().length());
        }
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthikeyan D S
     * @created on 28JUL2018
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthikeyan D S
     * @created on 28JUL2018
     */
    private void deallocateObjects() {
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        image_view_next = null;
        imageviewPrevious = null;
        textViewDeclarationHeader = null;
        textViewFindSerialNum = null;
        textViewHaveOtherDevicesHeader = null;
        textViewLaptopDetailsHeader = null;
        typeFaceTitle = null;
        relativeLayoutDeviceDetails = null;
        editTextOtherLaptopDetails = null;
        editTextOtherDetails = null;
        editTextSerialNumber = null;
        editTextMake = null;
        lapDetailsRadioGroup = null;
        radioButtonMac = null;
        radioButtonLenovo = null;
        radioButtonHp = null;
        radioButtonDell = null;
        radioButtonAcer = null;
        radioButtonAssus = null;
        radioButtonSelectedDevice = null;
        radioButtonOthersLaptopBrands = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        if (dialog != null) {
            dialog = null;
        }
        applicationMode = NumberConstants.kConstantZero;
    }
}