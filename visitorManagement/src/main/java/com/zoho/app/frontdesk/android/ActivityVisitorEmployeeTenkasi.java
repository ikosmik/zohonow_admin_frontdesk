package com.zoho.app.frontdesk.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.LruCache;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.FontConstants;
import constants.MainConstants;
import constants.StringConstants;
import constants.TextToSpeachConstants;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;
import utility.CompressImage;
import utility.DialogViewWarning;
import utility.Utility;

public class ActivityVisitorEmployeeTenkasi extends Activity{

    private DialogViewWarning dialog;
    private CustomAutoCompleteView textGetEmployeeName;
    private ZohoEmployeeRecord employee;
    private ArrayList<ZohoEmployeeRecord> employeeList;
    private RelativeLayout relativeLayoutExisitingVisitor;
    private ImageView imageviewBack, imageviewHome, imageviewPrevious,
            imageviewNext,image_view_next;
    private Button buttonSkip;
    private DbHelper database;
    private ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter myAdapter;
    private TextView textEmployeeErrorMsg, textviewVisitorNameTitle;
    private LruCache<String, Bitmap> mMemoryCache;
    private Typeface typeFaceTitle, typefaceVisitorName, typefaceErrorMsg, typefaceButton;
    private int actionCode = MainConstants.backPress;
    private Handler noActionHandler, handlerPurpose;
    private TextToSpeech textToSpeech;
    private String employeeFilterId = "ZT-";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_employee);
        assignObjects();
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStart() {
        super.onStart();
        setOldValue();
    }

    /**
     * This function is called, when the activity is stopped.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStop() {
        super.onStop();
    }

     /**
     * onResume()
     */
    public void onResume() {
        super.onResume();
    }
    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Assign Objects to the variable
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private void assignObjects() {

        textGetEmployeeName = (CustomAutoCompleteView) findViewById(R.id.textGetEmployeeName);
        textEmployeeErrorMsg = (TextView) findViewById(R.id.textViewEmployeeDetailErrorMsg);
        textviewVisitorNameTitle = (TextView) findViewById(R.id.textview_visitor_employee_title);
        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);

        buttonSkip = (Button) findViewById(R.id.button_skip);
        buttonSkip.setText("-Others-");
        handlerPurpose = new Handler();
        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        if (textToSpeechListener != null) {
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }

        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);
        image_view_next.setOnClickListener(listenerConfirm);

        textGetEmployeeName
                .setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                        | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceErrorMsg = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskItalic);

        typefaceVisitorName = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceVisitorName = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceButton= Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskSemiBold);

        textGetEmployeeName.setTypeface(typefaceVisitorName);

        textEmployeeErrorMsg.setTypeface(typefaceErrorMsg);
        textviewVisitorNameTitle.setTypeface(typeFaceTitle);

        buttonSkip.setTypeface(typefaceButton);

        // Load Employee Details
        ZohoEmployeeRecord[] employeeArray = new ZohoEmployeeRecord[0];

        // set the custom ArrayAdapter
        myAdapter = new ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter(this,
                R.layout.custom_spinner, employeeArray);
        textGetEmployeeName.setAdapter(myAdapter);

        // TextChangedListener
        // add the listener so it will tries to suggest while the user types
        textGetEmployeeName
                .addTextChangedListener(textWatcherEmployeeNameChanged);
        // Onclick listeners
        buttonSkip.setOnClickListener(listenerButtonSkip);
        imageviewPrevious.setOnClickListener(listenerPrevious);
        imageviewNext.setOnClickListener(listenerConfirm);
        imageviewHome.setOnClickListener(listenerHomePage);
        imageviewBack.setOnClickListener(listenerBack);
        textGetEmployeeName.setOnItemClickListener(listenerChooseEmployee);
        textGetEmployeeName
                .setOnFocusChangeListener(listenerSetFocusOnEmployee);
        textGetEmployeeName.setOnEditorActionListener(listenerNextPressed);

        setAppBackground();

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };

    }

    /**
     * Set background color
     *
     * @author Karthick
     * @created on 20170508
     */
    private void setAppBackground() {
        if( SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getThemeCode()>MainConstants.kConstantZero)
        {
            if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantOne) {
                relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
            }
            else if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantTwo)
            {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[] {0xFFfa654b,0xFFfc5d55,0xFFfd575d,0xFFff5066});
                relativeLayoutExisitingVisitor.setBackground(gd);
            }
        }
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private TextToSpeech.OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };

    /**
     * intent to launch previous page
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            // showDetailDialog();
        }
    };

    /**
     * intent to launch previous page
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private View.OnClickListener listenerHomePage = new View.OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.homePress;
            hideKeyboard();
            showDetailDialog();
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private View.OnClickListener listenerConfirm = new View.OnClickListener() {
        public void onClick(View v) {
            if(handlerPurpose!=null && purposeSeletionTask!=null) {
                handlerPurpose.removeCallbacks(purposeSeletionTask);
            }
            processEmployeeName();
        }
    };

    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private View.OnClickListener listenerPrevious = new View.OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };

    /**
     * Choose the purpose of visit
     *
     * @author jayapriya
     *
     * @created 27/02/2015
     */
    private AdapterView.OnItemClickListener listenerChooseEmployee = new AdapterView.OnItemClickListener() {

        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            employee = (ZohoEmployeeRecord) parent.getAdapter().getItem(
                    position);
            buttonSkip.setVisibility(View.INVISIBLE);
            imageviewNext.setVisibility(View.VISIBLE);
            image_view_next.setVisibility(View.VISIBLE);
            textGetEmployeeName
                    .setBackgroundResource(R.drawable.cell_underline_white);

            String name = ((ZohoEmployeeRecord) parent.getAdapter().getItem(
                    position)).getEmployeeName();
            textGetEmployeeName
                    .removeTextChangedListener(textWatcherEmployeeNameChanged);
            textGetEmployeeName.setText(name.toUpperCase());

            textGetEmployeeName.setSelection(textGetEmployeeName.getText()
                    .length());
            textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
            textGetEmployeeName
                    .addTextChangedListener(textWatcherEmployeeNameChanged);
            // show dialog box to get the visitor purpose
            // when visitor visits want to visit the HR person
            if (employee.getEmployeeDept().equals(MainConstants.HRDepartment)
                    || employee.getEmployeeDept().equals(
                    MainConstants.HRDepartmentAnotherNamePlural)
                    || employee.getEmployeeDept().equals(
                    MainConstants.HRDepartmentAnotherNameSingular)) {
            }
            else {
                SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
                        getResources().getString(
                                R.string.radio_string_purpose_personal));
            }
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            }
            handlerPurpose.removeCallbacks(purposeSeletionTask);
            handlerPurpose.postDelayed(purposeSeletionTask, 1000);
        }
    };

    private View.OnFocusChangeListener listenerSetFocusOnEmployee = new View.OnFocusChangeListener() {

        public void onFocusChange(View v, boolean hasFocus) {
            if (textGetEmployeeName.hasFocus() == false) {

                if ((textGetEmployeeName != null)
                        && (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
                        && (textGetEmployeeName.getText().toString().trim()
                        .length() > MainConstants.kConstantZero)
                        && ((employee == null)
                        || (employee.getEmployeeName() == null) || (employee
                        .getEmployeeName() == MainConstants.kConstantEmpty))) {

                    textEmployeeErrorMsg.setVisibility(View.VISIBLE);
                } else {
                    textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
                }
            }

        }
    };

    /*
     * when done pressed then process started
     *
     * @author jayapriya
     *
     * @created02/12/2014
     */
    private TextView.OnEditorActionListener listenerNextPressed = new EditText.OnEditorActionListener() {

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                processEmployeeName();
                return true;
            }
            return false;
        }

    };

    /**
     * Launch previous activity
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private void actionHandle() {

        if (actionCode == MainConstants.homePress) {
            launchHomePage();
        } else {
            launchNextPage();
        }

        finish();
    }

    /**
     * Launch next activity.
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private void launchHomePage() {
        Intent intent = new Intent(getApplicationContext(),
                WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch previous activity.
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private void launchPreviousPage() {
        Intent intent = new Intent(getApplicationContext(),
                ActivityCompanyDetails.class);
        startActivity(intent);
        finish();
    }

    /**
     * Launch next activity.
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private void launchNextPage() {
        if(SingletonVisitorProfile.getInstance()!=null ) {
            SingletonVisitorProfile.getInstance().setEmployeeActivityCode(2);
        }
            Intent intent = new Intent(getApplicationContext(), ActivityVisitorPurpose.class);
            startActivity(intent);
            finishAffinity();
    }

    /**
     * Check valid employee then goto next page
     * otherwise show error message
     *
     * @author Karthick
     *
     * @created on 20170518
     */
    private void processEmployeeName() {
        if(validate()) {
            Utility utility = new Utility();
            utility.storeEmployeeIntoSingleton(employee);
            launchNextPage();
        }
    }

    /**
     * Skip employee name
     *
     * @author Karthick
     *
     * @created 20160913
     */
    private View.OnClickListener listenerButtonSkip = new View.OnClickListener() {
        public void onClick(View v) {
//            hideKeyboard();
//            actionCode = MainConstants.skipPress;
//            showWarningDialog(MainConstants.kConstantSkipEmployeeMessage);
            if( SingletonVisitorProfile.getInstance()!=null) {
                SingletonVisitorProfile.getInstance().setEmployeeActivityCode(0);
            }
            if(SingletonEmployeeProfile.getInstance()!=null && SingletonEmployeeProfile.getInstance().getEmployeeName()!=null)
            {
                SingletonEmployeeProfile.getInstance().setEmployeeName(null);
            }
            Intent nextPage = new Intent(ActivityVisitorEmployeeTenkasi.this,ActivityVisitorEmployee.class);
            startActivity(nextPage);
            finish();
        }
    };

    /**
     * Set employee selection
     *
     * @author Karthick
     *
     * @created on 20170518
     */
    private void setOldValue() {
        if ((SingletonEmployeeProfile.getInstance() != null)
                && (SingletonEmployeeProfile.getInstance().getEmployeeName() != null)
                && (SingletonEmployeeProfile.getInstance().getEmployeeName() != MainConstants.kConstantEmpty) && textGetEmployeeName!=null) {

            textGetEmployeeName.removeTextChangedListener(textWatcherEmployeeNameChanged);
            textGetEmployeeName.setText(SingletonEmployeeProfile.getInstance()
                    .getEmployeeName().toUpperCase());
            textGetEmployeeName.setSelection(textGetEmployeeName.getText()
                    .toString().trim().length());
            textGetEmployeeName.addTextChangedListener(textWatcherEmployeeNameChanged);
            if(database==null) {
                database = new DbHelper(this);
            }
            if(database!=null) {
                employeeList = database.getEmployeeListWithNameBasedID(textGetEmployeeName
                        .getText().toString().trim().replaceAll("'", "''"),employeeFilterId);
//                Log.d("Employee",""+employeeList + ""+employeeFilterId);
                if ((employeeList != null)
                        && (employeeList.size() == MainConstants.kConstantOne)) {
                    employee = employeeList.get(MainConstants.kConstantZero);

                    buttonSkip.setVisibility(View.INVISIBLE);
                    imageviewNext.setVisibility(View.VISIBLE);
                    image_view_next.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /**
     * Validate visitor select valid employee
     *
     * @author Karthick
     *
     * @created on 20170518
     */
    private boolean validate() {
        if (((textGetEmployeeName.getText() != null)
                && (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
                && (textGetEmployeeName.getText().toString().trim()
                .length() > MainConstants.kConstantZero) && (employee != null))) {
            textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
            return true;
        } else {
            textEmployeeErrorMsg.setVisibility(View.VISIBLE);
            return false;
        }
    }

    /**
     * Next process
     *
     * @author Karthick
     * @created on 20161114
     */
    Runnable purposeSeletionTask = new Runnable() {
        public void run() {
            processEmployeeName();
        }
    };

    /**
     * Goto home page when no action.
     *
     * @author Karthick
     *
     * @created on 20170508
     *
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };


    private TextWatcher textWatcherEmployeeNameChanged = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            setCustomAdaptor(count);
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTimeInVisitorDetailActivity);
            buttonSkip.setVisibility(View.VISIBLE);
            imageviewNext.setVisibility(View.GONE);
            image_view_next.setVisibility(View.GONE);
        }
        public void afterTextChanged(Editable s) {

        }
    };

    private void setCustomAdaptor(int count) {
        if (count == MainConstants.kConstantZero) {
            SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
                    MainConstants.kConstantEmpty);
            textGetEmployeeName
                    .setBackgroundResource(R.drawable.cell_underline_white);
        }
        employee = null;
        if ((textGetEmployeeName != null)
                && (textGetEmployeeName.getText().toString().trim() != MainConstants.kConstantEmpty)
                && (textGetEmployeeName.getText().toString().trim().length() >= MainConstants.kConstantThree)) {

            textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
            // get suggestions from the database
            if (database == null) {
                database = new DbHelper(this);
            }
            if (database != null) {
                employeeList = database.getEmployeeListWithNameBasedID(textGetEmployeeName
                        .getText().toString().trim().replaceAll("'", "''"), employeeFilterId);
//            Log.d("Employee",""+employeeList);
                if ((employeeList != null)
                        && (employeeList.size() > MainConstants.kConstantZero)) {
                    setEmployeeMaxLength(MainConstants.maxLengthTracedEmployee);
                    ZohoEmployeeRecord[] employeeArray = employeeList
                            .toArray(new ZohoEmployeeRecord[employeeList.size()]);
                    if ((employeeArray != null)
                            && (employeeArray.length > MainConstants.kConstantZero)) {
                        // update the adapter
                        myAdapter = new ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter(this,
                                R.layout.custom_spinner, employeeArray);
                        textGetEmployeeName.setAdapter(myAdapter);
                        // update the adapater
                        myAdapter.notifyDataSetChanged();
                    }
                } else {
                    try {
                        myAdapter.clear();
                    } catch (Exception e) {
                    }
                    myAdapter.notifyDataSetChanged();
                    textEmployeeErrorMsg.setVisibility(View.VISIBLE);

                    setEmployeeMaxLength(MainConstants.maxLengthUnTracedEmployee);
                }
            } else {
                if ((SingletonEmployeeProfile.getInstance() != null)
                        || (employee == null)) {
                    SingletonEmployeeProfile.getInstance().clearInstance();
                }
                if (myAdapter != null) {
                    try {
                        myAdapter.clear();
                    } catch (Exception e) {
                    }
                    myAdapter.notifyDataSetChanged();
                }
                textEmployeeErrorMsg.setVisibility(View.INVISIBLE);
            }
        }
    }
    /**
     * Set max length for employee field
     *
     * @author Karthick
     * @created 20170424
     */
    private void setEmployeeMaxLength(int length) {
        InputFilter[] filterArray = new InputFilter[1];
        if(filterArray!=null && filterArray.length>0) {
            filterArray[0] = new InputFilter.LengthFilter(length);
        }
        if(textGetEmployeeName!=null && filterArray!=null && filterArray[0]!=null) {
            textGetEmployeeName.setFilters(filterArray);
        }
    }

    /**
     * Hide keyboard
     *
     * @author Karthick
     * @created on 20170508
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showDetailDialog() {
        showWarningDialog(StringConstants.kConstantHomePageErrorMessage);
        textSpeach(MainConstants.speachAreYouSureCode);
    }

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthikeyan D S
     * @created on 2018/04/08
     */
    private void showWarningDialog(String errorMessage) {
        hideKeyboard();
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(errorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    /**
     * Handle tab click for goto settings page
     *
     * @author Karthick
     * @created on 20170508
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            actionHandle();
        }
    };

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {

        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (textGetEmployeeName != null
                    && textGetEmployeeName.getText() != null && textGetEmployeeName
                    .getText().toString().length() <= 0)) {
                textToSpeech.speak(TextToSpeachConstants.speachEmployeeName,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    public class AutocompleteCustomArrayAdapter extends
            ArrayAdapter<ZohoEmployeeRecord> {

        Context mContext;
        int layoutResourceId;
        ZohoEmployeeRecord employee[] = null;
        long minFrequency, maxFrequency, avarageFrequency;

        public AutocompleteCustomArrayAdapter(Context mContext,
                                              int layoutResourceId, ZohoEmployeeRecord[] employee) {

            super(mContext, layoutResourceId, employee);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.employee = employee;
            if (employee.length > MainConstants.kConstantZero) {
                this.minFrequency = employee[employee.length - 1]
                        .getEmployeeVisitFrequency();
                this.maxFrequency = employee[0].getEmployeeVisitFrequency();
                this.avarageFrequency = (maxFrequency - minFrequency) / 3;
            }
        }

        public class ViewHolder {
            public RelativeLayout relativeLayoutCustomSpinner;
            public TextView textEmployeeName;
            public TextView textEmployeePhoneNo;
            public TextView textEmployeeDept;
            public ImageView employeeImage;

            public ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter.ViewHolder getTag() {
                return null;
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            try {

                // object item based on the position
                ZohoEmployeeRecord employees = employee[position];
                if (employees != null) {
                    final ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter.ViewHolder view;
                    if (convertView == null) {
                        view = new ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter.ViewHolder();
                        // inflate the layout
                        LayoutInflater inflater = ((ActivityVisitorEmployeeTenkasi) mContext)
                                .getLayoutInflater();
                        convertView = inflater.inflate(layoutResourceId,
                                parent, false);
                        view.relativeLayoutCustomSpinner = (RelativeLayout) convertView
                                .findViewById(R.id.relativeLayout_custom_spinner);
                        view.textEmployeeName = (TextView) convertView
                                .findViewById(R.id.text_view_employee_name);
                        view.textEmployeePhoneNo = (TextView) convertView
                                .findViewById(R.id.text_view_employee_Phone);
                        view.textEmployeeDept = (TextView) convertView
                                .findViewById(R.id.text_view_employee_Dept);
                        view.employeeImage = (ImageView) convertView
                                .findViewById(R.id.image_view_employee);
                        convertView.setTag(view);
                    } else {
                        view = (ActivityVisitorEmployeeTenkasi.AutocompleteCustomArrayAdapter.ViewHolder) convertView.getTag();
                    }

                    view.textEmployeeName.setText(employees.getEmployeeName()
                            .toUpperCase());
                    view.textEmployeeName.setTypeface(typeFaceTitle);
                    view.textEmployeePhoneNo.setText(employees
                            .getEmployeeMobileNumber());
                    view.textEmployeePhoneNo.setTypeface(typeFaceTitle);
                    view.textEmployeeDept.setText(employees.getEmployeeDept());
                    view.textEmployeeDept.setTypeface(typeFaceTitle);

                    if ((this.maxFrequency >= employees
                            .getEmployeeVisitFrequency())
                            && ((this.maxFrequency - this.avarageFrequency) <= employees
                            .getEmployeeVisitFrequency())
                            && this.maxFrequency > 0) {

                        view.employeeImage.getLayoutParams().height = 150;
                        view.employeeImage.getLayoutParams().width = 150;

                        int myInteger = getResources().getInteger(R.integer.int_emp_freq_high);
                        if(myInteger>0) {
                            view.employeeImage.getLayoutParams().height = myInteger+100;
                            view.employeeImage.getLayoutParams().width = myInteger;
                        }
                        view.textEmployeeName.setTextSize(30);
                        view.textEmployeeDept.setTextSize(26);
                        view.textEmployeePhoneNo.setTextSize(26);

                        if (view.textEmployeeName != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_name_freq_high);
                            view.textEmployeeName.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        if (view.textEmployeeDept != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_dept_freq_high);
                            view.textEmployeeDept.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        if (view.textEmployeePhoneNo != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_phone_freq_high);
                            view.textEmployeePhoneNo.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        view.textEmployeeName
                                .setTypeface(typefaceVisitorName);
                        view.relativeLayoutCustomSpinner
                                .setBackgroundColor(Color.rgb(248, 125, 35));
                    } else if (((this.maxFrequency - this.avarageFrequency) > employees
                            .getEmployeeVisitFrequency())
                            && ((this.maxFrequency - (MainConstants.kConstantTwo * this.avarageFrequency)) <= employees
                            .getEmployeeVisitFrequency())
                            && this.maxFrequency > 0) {

                        view.employeeImage.getLayoutParams().height = 250;
                        view.employeeImage.getLayoutParams().width = 250;
                        int myInteger = getResources().getInteger(R.integer.int_emp_freq_medium);
                        if(myInteger>0) {
                            view.employeeImage.getLayoutParams().height = myInteger;
                            view.employeeImage.getLayoutParams().width = myInteger;
                        }
                        view.textEmployeeName.setTextSize(26);
                        view.textEmployeeDept.setTextSize(23);
                        view.textEmployeePhoneNo.setTextSize(23);

                        if (view.textEmployeeName != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_name_freq_medium);
                            view.textEmployeeName.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        if (view.textEmployeeDept != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_dept_freq_medium);
                            view.textEmployeeDept.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        if (view.textEmployeePhoneNo != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_phone_freq_medium);
                            view.textEmployeePhoneNo.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }

                        view.textEmployeeName
                                .setTypeface(typefaceVisitorName);
                        view.relativeLayoutCustomSpinner
                                .setBackgroundColor(Color.rgb(186, 166, 150));

                    } else {

                        view.employeeImage.getLayoutParams().height = 200;
                        view.employeeImage.getLayoutParams().width = 200;
                        int myInteger = getResources().getInteger(R.integer.int_emp_freq_low);
                        if(myInteger>0) {
                            view.employeeImage.getLayoutParams().height = myInteger;
                            view.employeeImage.getLayoutParams().width = myInteger;
                        }
                        view.textEmployeeName.setTextSize(20);
                        view.textEmployeeDept.setTextSize(18);
                        view.textEmployeePhoneNo.setTextSize(20);

                        if (view.textEmployeeName != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_name_freq_low);
                            view.textEmployeeName.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        if (view.textEmployeeDept != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_dept_freq_low);
                            view.textEmployeeDept.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }
                        if (view.textEmployeePhoneNo != null) {
                            int dropdownFontSize = (int) getResources().getDimension(
                                    R.dimen.emp_list_phone_freq_low);
                            view.textEmployeePhoneNo.setTextSize(TypedValue.COMPLEX_UNIT_PX,dropdownFontSize);
                        }

                        view.textEmployeeName.setTypeface(typefaceVisitorName);
                        view.relativeLayoutCustomSpinner
                                .setBackgroundColor(Color.rgb(184, 183, 183));
                    }

                    String employeePhoto = MainConstants.kConstantFileDirectory
                            .concat(MainConstants.kConstantFileEmployeeImageFolder)
                            .concat(employees.getEmployeeZuid())
                            .concat(MainConstants.kConstantImageFileFormat);
                    if(ApplicationController.getInstance()!=null && ApplicationController.getInstance().checkExternalPermission() && (employees.getEmployeePhoto() != null)
                            && (employees.getEmployeePhoto() != MainConstants.kConstantEmpty)
                            && (new File(employeePhoto).exists())) {
                        if (getBitmapFromMemCache(employees.getEmployeeZuid()
                                .toString()) != null) {
                            view.employeeImage
                                    .setImageBitmap(getBitmapFromMemCache(employees
                                            .getEmployeeZuid().toString()));
                        } else {
                            Bitmap drawableEmployeeImage= CompressImage
                                    .decodeSampleBitmapFromSdCard(
                                            getResources(), employeePhoto,
                                            100, 100);
                            if (drawableEmployeeImage != null) {
                                Bitmap bitmap = BitmapFactory.decodeFile(employeePhoto);
                                view.employeeImage.setImageBitmap(bitmap);

                                ActivityVisitorEmployeeTenkasi.BitmapWorkerTask task = new ActivityVisitorEmployeeTenkasi.BitmapWorkerTask(
                                        employees.getEmployeeZuid()
                                                .toString(),
                                        drawableEmployeeImage);
                                task.execute(employees.getEmployeeZuid()
                                        .toString());
                            } else {
                                if (view.employeeImage != null) {
                                    view.employeeImage.setImageResource( R.drawable.photo);
                                }
                            }
                        }
                    } else {
                        if (view.employeeImage != null) {
                            view.employeeImage.setImageResource( R.drawable.photo);
                        }
                    }
                }
            } catch (NullPointerException e) {
            } catch (Exception e) {
            }
            return convertView;
        }
    }

    /**
     * Get bitmap from memory cache
     *
     * @author karthick
     *
     * @created 27/05/2015
     */
    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    /**
     * Add bitmap to memory cache
     *
     * @author karthick
     * @created 27/05/2015
     */
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    public void onBackPressed() {
        launchPreviousPage();
    }

    /*
     * Add bitmap to memory cache
     *
     *
     * @author karthick
     * @created 27/05/2015
     */
    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

        String zuid;
        Bitmap empPhotoInBitmap;

        BitmapWorkerTask(String zuid, Bitmap empPhoto) {
            this.zuid = zuid;
            empPhotoInBitmap = empPhoto;
        }

        // Decode image in background.
        protected Bitmap doInBackground(String... params) {
            if(empPhotoInBitmap!=null) {
                addBitmapToMemoryCache(zuid, empPhotoInBitmap);
            }
            return empPhotoInBitmap;
        }
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     *
     * @created on 20170508
     */
    private void deallocateObjects() {
        mMemoryCache.evictAll();
        textGetEmployeeName = null;
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        textEmployeeErrorMsg = null;
        textviewVisitorNameTitle = null;
        typeFaceTitle = null;
        typefaceVisitorName = null;
        relativeLayoutExisitingVisitor
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutExisitingVisitor = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (handlerPurpose != null) {
            handlerPurpose.removeCallbacks(purposeSeletionTask);
            handlerPurpose = null;
        }
        if (dialog != null) {
            dialog.hideDialog();
            dialog=null;
        }
        if(textToSpeech!=null)
        {
            textToSpeech.stop();
        }
    }
}
