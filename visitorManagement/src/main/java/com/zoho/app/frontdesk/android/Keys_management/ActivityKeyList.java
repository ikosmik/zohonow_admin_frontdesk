package com.zoho.app.frontdesk.android.Keys_management;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.DataBaseRepository;
import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.KeyBackgroundProcess;
import com.zoho.app.frontdesk.android.Keys_management.roomDataBase.keyEntity;
import com.zoho.app.frontdesk.android.R;

import java.util.ArrayList;
import java.util.List;

import utility.CustomKeyListAdapter;

public class ActivityKeyList extends Activity {

    public ListView list_view_key_list;
    public CustomKeyListAdapter adapter_key_list;
    public DataBaseRepository dataBaseRepository;
    public List<keyEntity> list_keys;
    public ArrayList<String> key_list_value = new ArrayList<String>();
    public ArrayList<String> key_list_description = new ArrayList<String>();
    public ArrayList<String> KeyID = new ArrayList<String>();
    public ArrayList<Integer> item_image = new ArrayList<Integer>();
    public ArrayList<String> spare_key = new ArrayList<String>();
    public ArrayList<Integer> selected_image_list = new ArrayList<Integer>();
    public ArrayList<String> bg_color_list = new ArrayList<String>();
    Context context ;

    //here declarations done
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.key_selection_layout);
        list_view_key_list = findViewById(R.id.list_view_keys);
        //TextView start_process = findViewById(R.id.textView_item_title);
        dataBaseRepository = new DataBaseRepository(this);
        context = this;
        //here creating the list
        try{
            fetchKeys();
            setKeysToList(list_keys);
        }catch (Exception e){
            Log.e("Failed","******************* Try Again "+e.getMessage());
        }

        //here setting the on click function
        list_view_key_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ActivityKeyList.this,key_list_value.get(position), Toast.LENGTH_SHORT).show();
            }
        });

//        start_process.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try{
//                    KeyBackgroundProcess keyBackgroundProcess = new KeyBackgroundProcess(context);
//                    Toast.makeText(ActivityKeyList.this,"Background process Started", Toast.LENGTH_LONG).show();
//                    finish();
//                    startActivity(getIntent());
//                }catch (Exception e){
//                    Log.e("Failed","******************* Try Again :-) : "+e.getMessage());
//                }
//            }
//        });
    }

public void setKeysToList(List<keyEntity> key_list)
{
    adapter_key_list = new CustomKeyListAdapter(this,key_list_value,item_image,key_list_description,KeyID,spare_key
    ,selected_image_list,bg_color_list);
    list_view_key_list.setAdapter(adapter_key_list);
}
public void fetchKeys()
{
    //appDatabase.appDatabaseObject().deleteAllKeys();
    list_keys = dataBaseRepository.fetchAllKeys();
    //Log.e("Output", "******************* Result :-) "+list_keys.size()+" | "+list_keys);
    for (int i = 0; i < list_keys.size(); i++)
    {
        key_list_value.add(list_keys.get(i).getKey_title());
        key_list_description.add(list_keys.get(i).getDescription());
        KeyID.add(list_keys.get(i).getKey_id());
        item_image.add(list_keys.get(i).getKey_image());
        spare_key.add(list_keys.get(i).getSpare());
    }
}
}