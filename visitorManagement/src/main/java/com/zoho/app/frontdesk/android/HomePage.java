package com.zoho.app.frontdesk.android;

import java.util.HashSet;
import java.util.Set;

import appschema.SingletonCode;
import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import backgroundprocess.SchedulerEventReceiver;
import constants.NumberConstants;
import constants.StringConstants;
import utility.CurrentLocationCode;
import utility.SetLocationToSingleton;
import appschema.SharedPreferenceController;
import utility.UsedAlarmManager;
import utility.UsedCustomToast;
import utility.Validation;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import constants.FontConstants;
import constants.MainConstants;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;

public class HomePage extends Activity implements OnClickListener {

    private RelativeLayout relativeLayoutVisitor, relativeLayoutInterview,
            relativeLayoutForgotID, relativeLayoutVisitorHead, relativeLayoutInterviewHead,
            relativeLayoutForgotIDHead, relativeLayoutHomePage;
    private LinearLayout linearLayoutHomePage;
    private TextView textViewEmployee, textViewVisitor, textViewInterview;
    private Typeface textViewFont, fontSourceSansPro;
    private ImageView imageviewTexture;
    private Bitmap bitmap;
    private boolean alarmUp;
    private Handler noActionHandler;
    private Dialog getEmployeeIdDialog;
    private Button buttonConfirm, buttonCancel;
    private EditText getEmployeeId;
    private TextView getEmployeeIdTitle;
    private DbHelper database;
    private int validateIdCount =  NumberConstants.kConstantZero;
    private Boolean chooseOption = false;
    //private BitmapDrawable drawableBitMap;
    private TextToSpeech textToSpeech;
    private utility.CurrentLocationCode locationCode;
    private String deviceLocation;
    private int applicationMode= NumberConstants.kConstantZero;
    private SharedPreferenceController spController;

    protected void onCreate(Bundle savedInstanceState) {
        // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > MainConstants.kConstantNine) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Set the orientation as Portrait
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            // this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        }

        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_homepage);

        assignObjects();
        setFont();
//        if(SingletonVisitorProfile.getInstance().getDeviceLocation().equalsIgnoreCase(MainConstants.kConstantDeviceLocationOne))
//        {
//            visitorOnClick();
//        }
        if(SingletonVisitorProfile.getInstance().getOfficeLocation().equalsIgnoreCase(MainConstants.kConstantOfficeLocationIndiaChennaiEstancia))
        {
            visitorOnClick();
        }
        //kConstantOfficeLocationIndiaChennaiEstancia
        SingletonEmployeeProfile.getInstance().clearInstance();
        SingletonVisitorProfile.getInstance().clearInstance();
        initializeAlarmManager();
        // setLocation();
        // Toast.makeText(getApplicationContext(),
        // SingletonVisitorProfile.getInstance().getLocationCode()+"",
        // Toast.LENGTH_LONG).show();
        // Log.d("Location Code",
        // SingletonVisitorProfile.getInstance().getLocationCode()+"");
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        // Log.d("ONUserIntraction","Active");
    }

    /*
     * Assign the values to the variable
     *
     * @author karthick
     *
     * @created 15/04/2015
     */
    private void assignObjects()
    {
        relativeLayoutHomePage = (RelativeLayout) findViewById(R.id.relativeLayoutHomePage);
        relativeLayoutVisitor = (RelativeLayout) findViewById(R.id.relativeLayout_visitor);
        relativeLayoutForgotID = (RelativeLayout) findViewById(R.id.relativeLayout_employee);
        relativeLayoutInterview = (RelativeLayout) findViewById(R.id.relativeLayout_interview);

        relativeLayoutVisitorHead = (RelativeLayout) findViewById(R.id.relativeLayout_visitor_head);
        relativeLayoutForgotIDHead = (RelativeLayout) findViewById(R.id.relativeLayout_employee_head);
        relativeLayoutInterviewHead = (RelativeLayout) findViewById(R.id.relativeLayout_interview_head);

        linearLayoutHomePage = (LinearLayout) findViewById(R.id.linear_layout_main);

        imageviewTexture = (ImageView) findViewById(R.id.imageview_texture);

        textViewEmployee = (TextView) findViewById(R.id.textView_employee);
        textViewVisitor = (TextView) findViewById(R.id.textView_visitor);
        textViewInterview = (TextView) findViewById(R.id.textView_interview);

        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noFaceTime);

        setDeviceScreenColor();
        setForgetID();

        if(database==null)
        {
            database = new DbHelper(this);
        }

        //set Texture for background
//		UsedBitmap usedBitmap = new UsedBitmap();
//		bitmap = Bitmap.createBitmap(300, 480, Bitmap.Config.ARGB_8888);
//		bitmap = usedBitmap.applyNoiceEffect(bitmap);
//		if (imageviewTexture != null && ApplicationController.getInstance()!=null && ApplicationController.getInstance().getNoiseBitmap()!=null) {
//			imageviewTexture
//					.setImageBitmap( ApplicationController.getInstance().getNoiseBitmap());
//			imageviewTexture.setAlpha(.08f);
//		}

        // BitmapDrawable drawableBitMap_Overlay = new
        // BitmapDrawable(getResources(),
        // CompressImage.decodeSampledBitmapFromResource(getResources(),
        // R.drawable.bgnd_blue_pattern, 500, 500));
        // drawableBitMap_Overlay.setAlpha(150);
        // relativeLayoutHomePage.setBackground(drawableBitMap_Overlay);

        onclickListeners();

        SingletonCode.setSingletonSelectionCode(MainConstants.kConstantZero);
        SingletonCode
                .setSingletonVisitorUploadErrorCode(MainConstants.kConstantZero);

        locationCode = new CurrentLocationCode(getApplicationContext());
    }

    private void onclickListeners()
    {
        relativeLayoutVisitor.setOnClickListener(this);
        relativeLayoutForgotID.setOnClickListener(this);
        relativeLayoutInterview.setOnClickListener(this);
    }

    /**
     * set FontStyle
     *
     * @author Karthikeyan D S
     * @createdon 21Sep2018
     */
    private void setFont()
    {
        textViewFont = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        fontSourceSansPro = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantSourceSansProBlack);

        textViewEmployee.setTypeface(textViewFont);
        textViewVisitor.setTypeface(textViewFont);
        textViewInterview.setTypeface(textViewFont);
    }
    /**
     * show DeviceScreenColor
     *
     * @author Karthikeyan D S
     * @createdon 21Sep2018
     */
    private void setDeviceScreenColor()
    {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutHomePage.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                relativeLayoutHomePage.setBackgroundColor(Color.rgb(166, 54, 134));
                relativeLayoutHomePage.setBackgroundColor(Color.rgb(10, 204, 205));
            }
        }
    }
    /**
     * show Forget ID Circle
     *
     * @author Karthikeyan D S
     * @createdon 21Sep2018
     */
    private void setForgetID()
    {
//        To Show Forget Id Based on the device Location
        if (SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getDeviceLocation()!=null
                && !SingletonMetaData.getInstance().getDeviceLocation().isEmpty())
        {
            deviceLocation = SingletonMetaData.getInstance().getDeviceLocation();
            applicationMode = SingletonMetaData.getInstance().getApplicationMode();
        }
//		Application Mode is Based on data center Mode Selection in StartBackground Activity Page.
        if (applicationMode ==NumberConstants.kConstantDataCentreMode)
        {
            linearLayoutHomePage.setWeightSum(MainConstants.kConstantOne);
            relativeLayoutInterviewHead.setVisibility(View.GONE);
            relativeLayoutForgotIDHead.setVisibility(View.GONE);
        }
        else if(applicationMode == NumberConstants.kConstantNormalMode){
            if (deviceLocation != null && !deviceLocation.isEmpty()
                    && deviceLocation.equalsIgnoreCase( MainConstants.kConstantDeviceLocationSix))
            {
                if (linearLayoutHomePage != null) {
                    linearLayoutHomePage.setWeightSum(MainConstants.kConstantTwo);
                }
                relativeLayoutInterviewHead.setVisibility(View.GONE);
                textViewEmployee.setText(getResources().getString(R.string.employee));
            } else {
                if (linearLayoutHomePage != null) {
                    linearLayoutHomePage.setWeightSum(MainConstants.kConstantTwo);
                }
                relativeLayoutInterviewHead.setVisibility(View.GONE);
            }
        }
    }
    /*
     * Created by karthick On 28/04/2015
     *
     * intent to launch the home page.
     */
    private void getHomePage() {

        if ((getEmployeeIdDialog != null) && (getEmployeeIdDialog.isShowing())) {
            getEmployeeIdDialog.dismiss();
            // Log.d("showing","dialog");
        }
        // else
        // {
        noActionHandler.removeCallbacks(noActionThread);
        callWelcomeActivity();

        // }
        // this.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        // this.overridePendingTransition(R.anim.animation_leave,
        // R.anim.animation_enter);
    }

    /**
     * Goto welcomePage Activity
     *
     * @author Karthikeyan D S
     * @created 22Sep2018
     */
    private void callWelcomeActivity()
    {
        Intent intent = new Intent(getApplicationContext(),
                WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Goto home page when no action.
     *
     * @author karthick
     * @created 16/06/2015
     */
    private Runnable noActionThread = new Runnable() {
        public void run() {
            // Log.d("visitor", "noActionThread");
            getHomePage();
        }
    };

    private void deallocateObjects() {
        // Log.d("Android", "deallocate");
        relativeLayoutVisitor = null;
        relativeLayoutForgotID = null;
        if (relativeLayoutHomePage != null) {
            relativeLayoutHomePage
                    .setBackgroundResource(MainConstants.kConstantZero);
            relativeLayoutHomePage = null;
        }
        if (imageviewTexture != null) {
            imageviewTexture.setBackground(null);
            imageviewTexture = null;
        }
        relativeLayoutInterview = null;
        textViewEmployee = null;
        textViewVisitor = null;
        textViewInterview = null;
        textViewFont = null;
        applicationMode =  NumberConstants.kConstantZero;
        //drawableBitMap = null;
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        if(textToSpeech!=null)
        {
            textToSpeech.stop();
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.relativeLayout_visitor) {
            visitorOnClick();
        } else if (v.getId() == R.id.relativeLayout_employee) {
            employeeOnClick();
        } else if (v.getId() == R.id.relativeLayout_interview) {
            interviewOnClick();
        }
    }

    private void visitorOnClick() {
        if (!chooseOption) {
            chooseOption = true;
            SingletonCode
                    .setSingletonSelectionCode(MainConstants.visitorSingletonCode);
            Intent existVisitorIntent = new Intent(getApplicationContext(),
                    ExistingVisitorActivity.class);
            // Intent existVisitorIntent = new Intent(getApplicationContext(),
            // VisitorDetailActivity.class);
            startActivity(existVisitorIntent);
            noActionHandler.removeCallbacks(noActionThread);
            finish();
        }

    }

    private void interviewOnClick() {
        if (!chooseOption) {
            chooseOption = true;
            SingletonCode.setSingletonSelectionCode(MainConstants.interviewSingletonCode);
            gotoEmployeePage();
        }
    }
    /**
     * assign SingletonCode based on DeviceLocation and
     * goto NextPage
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void employeeOnClick() {
        if (!chooseOption) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTime);
            chooseOption = true;
            setSingletonCode();
            gotoEmployeePage();
        }
    }
    /**
     * assign SingletonCode to Process Based on DeviceLocation
     *
     *  @return deviceLocation
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void setSingletonCode()
    {
//      get DeviceLocation and set singleton based on DeviceLocation.
        String deviceLocation = getDeviceLocation();
        if(deviceLocation!=null && !deviceLocation.isEmpty() && !deviceLocation.equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
            SingletonCode.setSingletonSelectionCode(MainConstants.employeeSingletonCode);
        }
        else
        {
            SingletonCode.setSingletonSelectionCode(MainConstants.employeeSingletonCodeTenkasi);
        }
    }

    /**
     * getDeviceLocation from SharedPreferenceController
     * @return deviceLocation
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private String getDeviceLocation()
    {
        String deviceLocation = MainConstants.kConstantEmpty;
        if(spController==null)
        {
            spController = new SharedPreferenceController();
        }
         deviceLocation = spController.getDeviceLocation(this);
        return deviceLocation;
    }

    /**
     * gotoEmployeePage
     *
     * @author Karthikeyan D S
     * @created on 21Sep2018
     */
    private void gotoEmployeePage() {
        Intent nextActivityIntent = new Intent(this, ActivityGetEmployeeIDDetails.class);
        startActivity(nextActivityIntent);
//        noActionHandler.removeCallbacks(noActionThread);
        finish();
    }

    /*
     * Dialog dismiss Listener for reinitialize grids
     *
     * @created on 20150618
     *
     * @author karthick
     */
    DialogInterface.OnDismissListener setOnDismissListener = new DialogInterface.OnDismissListener() {
        public void onDismiss(DialogInterface dialog) {
            chooseOption = false;
            if (noActionHandler != null) {
                noActionHandler.removeCallbacks(noActionThread);
                noActionHandler.postDelayed(noActionThread,
                        MainConstants.noFaceTime);
            }
            /*
             * if (employeeID.length() <= NumberConstants.kConstantZero) {
             * checkboxNotifyMe.setChecked(false); } if (isNotifyDialogShow) {
             * isNotifyDialogShow = false; } else {
             * initializeGridAdapter(false); }
             */
        }
    };

    /*
     * TextWatcher to set timer
     *
     * @created on 20150617
     *
     * @author karthick
     */
    private TextWatcher textWatcherOtherIssue = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // Log.d("text1","changed");
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // Log.d("text2","changed");
        }

        public void afterTextChanged(Editable s) {
            // Log.d("text3","changed");
            // Log.d("visitor", "afterTextChanged");
            // removeHadlerCallbacks();
            // noActionHandler.postDelayed(noActionTask,
            // NumberConstants.noActionTime);
        }
    };

    /*
     * Start upload employee mail id for notify to employee
     *
     * @created on 20150620
     *
     * @author karthick
     */
    private OnClickListener listenerNotifyMe = new OnClickListener() {
        public void onClick(View v) {
        }
    };

    private void setLocation() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        Criteria locationCritera = new Criteria();
        locationCritera.setAccuracy(Criteria.ACCURACY_FINE);
        locationCritera.setAltitudeRequired(false);
        locationCritera.setBearingRequired(false);
        locationCritera.setCostAllowed(true);
        locationCritera.setPowerRequirement(Criteria.NO_REQUIREMENT);

        String providerName = locationManager.getBestProvider(locationCritera,
                true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //TODO : Consider calling
            //ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(providerName);
        if (location != null) {
            // Log.d("Android", "" + location.getLatitude());
            // Log.d("Android", "" + location.getLongitude());
            SetLocationToSingleton.setSingletonValue(location.getLatitude(),
                    location.getLongitude());
        }
        locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location
                // provider.
                /*
                 * Toast.makeText( getApplicationContext(),
                 * "vm lat and long"+location
                 * .getLatitude()+":"+location.getLongitude(),
                 * Toast.LENGTH_LONG).show();
                 */
                SetLocationToSingleton.setSingletonValue(
                        location.getLatitude(), location.getLongitude());
            }

            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

    }

    /*
     * Created by Jayapriya on 11/10/2014 Initialize the alarmManager.
     */
    private void initializeAlarmManager() {
        alarmUp = (PendingIntent.getBroadcast(this,
                MainConstants.kConstantZero, new Intent(this,
                        SchedulerEventReceiver.class),
                PendingIntent.FLAG_NO_CREATE) != null);
        // Log.d("welcomeactivity", "Alarm is already active" + alarmUp);
        if (alarmUp) {
            // Log.d("welcomeactivity", "Alarm is already active");

        } else {
            // Log.d("welcomeactivity", "Alarm is not actived");
            UsedAlarmManager.activateAlarmManager(this);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        // Log.d("homeActivity", "onDestroy");
        // call function to remove location update
        locationCode.removeLocationCode();

//		Log.d("Android", "Android");
        noActionHandler.removeCallbacks(noActionThread);
        deallocateObjects();
    }

    /*
     * Show dialog to get employee id if user typed id and tapped confirm then
     * validate the employee Id as a valid or not if it as invalid and allows
     * type again, in two chances they typed invalid id then go to home page.
     *
     * @author jayapriya
     *
     * @created on 21/03/16
     */
    private void getEmployeeIdDialog() {
        final Context context = this;
        getEmployeeIdDialog = new Dialog(this,
                android.R.style.Theme_InputMethod);
        getEmployeeIdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getEmployeeIdDialog.setContentView(R.layout.dialog_activity);
        getEmployeeIdDialog.show();
        getEmployeeIdDialog.setCanceledOnTouchOutside(true);
        getEmployeeIdDialog.setOnDismissListener(setOnDismissListener);
        getEmployeeId = (EditText) getEmployeeIdDialog
                .findViewById(R.id.dialog_message);
        getEmployeeId.setSingleLine(false);
        getEmployeeIdTitle = (TextView) getEmployeeIdDialog
                .findViewById(R.id.dialog_title);
        buttonConfirm = (Button) getEmployeeIdDialog
                .findViewById(R.id.dialog_yes_button);
        buttonCancel = (Button) getEmployeeIdDialog
                .findViewById(R.id.dialog_no_button);
        getEmployeeIdTitle
                .setText(StringConstants.kConstantDialogTitleGetEmployeeId);
        getEmployeeId
                .setHint(StringConstants.kConstantDialogHintMsgGetEmployeeId);
        getEmployeeId.setTypeface(fontSourceSansPro);
        getEmployeeIdTitle.setVisibility(View.GONE);
        // getEmployeeId.setInputType(InputType.TYPE_CLASS_NUMBER);
        getEmployeeId.setTextColor(Color.rgb(102, 102, 102));

        getEmployeeId.setGravity(Gravity.CENTER);
        getEmployeeId.setFocusableInTouchMode(true);
        buttonConfirm.setTypeface(fontSourceSansPro);
        buttonCancel.setTypeface(fontSourceSansPro);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        // params.width = 300;

        float density = getResources().getDisplayMetrics().density;
        if (density < 2) {
            params.height = 150;
            params.setMargins(30, 50, 30, 50);
            getEmployeeId.setTextSize(24);
        } else {
            params.height = 200;
            params.setMargins(50, 100, 50, 100);
            getEmployeeId.setTextSize(30);
        }

        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        getEmployeeId.setLayoutParams(params);
        // RelativeLayout.LayoutParams textViewParams = new
        // RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
        // LayoutParams.MATCH_PARENT);
        // textViewParams.height = 150;
        // getEmployeeIdTitle.setLayoutParams(textViewParams);
        getEmployeeId.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // Log.d("onText1","changed");
                if (noActionHandler != null) {
                    noActionHandler.removeCallbacks(noActionThread);
                    noActionHandler.postDelayed(noActionThread,
                            MainConstants.afterTypingNoFaceTime);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                // Log.d("onText2","changed");
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // Log.d("onText","changed");
                if (count > 0) {
                    if (noActionHandler != null) {
                        noActionHandler.removeCallbacks(noActionThread);
                        noActionHandler.postDelayed(noActionThread,
                                MainConstants.afterTypingNoFaceTime);
                    }
                }

            }

        });
        buttonCancel.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                getEmployeeIdDialog.dismiss();

            }
        });

        buttonConfirm.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                if ((getEmployeeId != null)
                        && (getEmployeeId.getText().toString().trim() != "")
                        && (getEmployeeId.getText().toString().trim().length() > 0)) {
                    validateIdCount = validateIdCount + 1;
                    String employeeId = getEmployeeId.getText().toString()
                            .trim();

                    ZohoEmployeeRecord employee = database
                            .getEmployee(employeeId);
                    if ((employee != null) && (employee.getEmployeeName() != null)
                            && (employee.getEmployeeName() != "")) {
                        // set employee details into singleton variable
                        setEmployeeDetails(employee);
                        getEmployeeIdDialog.dismiss();
                        Intent nextActivityIntent = new Intent(
                                getApplicationContext(),
                                StationeryListActivity.class);
                        startActivity(nextActivityIntent);
                        finish();

                    } else {
                        if (validateIdCount >= 2) {
                            validateIdCount = 0;

                            UsedCustomToast
                                    .makeCustomToast(
                                            getApplicationContext(),
                                            StringConstants.toastInvalidEmployeeIdInSecondTime,
                                            Toast.LENGTH_LONG, Gravity.TOP)
                                    .show();

                            getHomePage();
                        } else {
                            UsedCustomToast
                                    .makeCustomToast(
                                            getApplicationContext(),
                                            StringConstants.toastInvalidEmployeeIdInFirstTime,
                                            Toast.LENGTH_LONG, Gravity.TOP)
                                    .show();

                        }
                    }
                } else {
                    UsedCustomToast.makeCustomToast(getApplicationContext(),
                            StringConstants.toastEmployeeIdIsEmpty,
                            Toast.LENGTH_LONG, Gravity.TOP).show();
                }

            }
        });

    }

    /**
     * @param employee
     * @author jayapriya
     * @created on 22/03/16
     */
    protected void setEmployeeDetails(ZohoEmployeeRecord employee) {
        Validation validation = new Validation();

        SingletonEmployeeProfile.getInstance().setEmployeeName(
                employee.getEmployeeName());
        SingletonEmployeeProfile.getInstance().setEmployeeId(
                employee.getEmployeeId());
        if (((SingletonVisitorProfile.getInstance().getLocationCode() != null))
                // && (SingletonVisitorProfile.getInstance()
                // .getLocationCode() != MainConstants.kConstantMinusOne)
                && ((SingletonVisitorProfile.getInstance().getLocationCode() == MainConstants.kConstantMinusOne)
                || (SingletonVisitorProfile.getInstance()
                .getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiDLF)
                || (SingletonVisitorProfile.getInstance()
                .getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaChennaiEstancia) || (SingletonVisitorProfile
                .getInstance().getLocationCode() == MainConstants.kConstantOfficeLocationCodeIndiaTenkasi))) {
            validation.SetMobileNumberWithLocation(employee
                    .getEmployeeMobileNumber().replace(
                            MainConstants.kConstantRemoveSpecialCharacter, ""));
        } else {
            SingletonEmployeeProfile.getInstance().setEmployeeMobileNo(
                    employee.getEmployeeMobileNumber());
            // Log.d("phone","else no"
            // +SingletonEmployeeProfile.getInstance().getEmployeeMobileNo());

        }
        SingletonEmployeeProfile.getInstance().setEmployeeSeatingLocation(
                employee.getEmployeeSeatingLocation());
        SingletonEmployeeProfile.getInstance().setEmployeePhoto(
                employee.getEmployeePhoto());
        SingletonEmployeeProfile.getInstance().setEmployeeZuid(
                employee.getEmployeeZuid());
        SingletonEmployeeProfile.getInstance().setEmployeeExtentionNumber(
                employee.getEmployeeExtentionNumber());
        SingletonEmployeeProfile.getInstance().setEmployeeMailId(
                employee.getEmployeeEmailId());
        SingletonEmployeeProfile.getInstance().setEmployeeRecordId(
                employee.getEmployeeRecordId());
    }

    /**
     * check sharedpreferences of stationeryDetails
     */
    private void getStationeryDetails() {
        Set<String> set = new HashSet<String>();
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                MainConstants.sharedPreferences, Context.MODE_PRIVATE);
        set = prefs.getStringSet("StationeryItemEntry", null);
    }

    /**
     * @author jayapriya
     */
    public void onPause() {
        super.onPause();
        // Log.d("onPause","onPause");
        noActionHandler.removeCallbacks(noActionThread);
    }

    /**
     * @author jayapriya
     */
    public void onResume() {
        super.onResume();
        // Log.d("onResume","resume");
        //Zoom in/out animation
//		Animation hyperspaceJump = AnimationUtils.loadAnimation(this, R.drawable.zoom_in_out_anim);
//		relativeLayoutVisitor.startAnimation(hyperspaceJump);
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread,
                    MainConstants.afterTypingNoFaceTime);
        }
    }

    /**
     * Goto welcome page
     *
     * @author Karthick
     * @created on 20161220
     */
    public void onBackPressed() {
        getHomePage();
    }
}
