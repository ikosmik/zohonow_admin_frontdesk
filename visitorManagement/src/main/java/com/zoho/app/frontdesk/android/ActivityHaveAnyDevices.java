package com.zoho.app.frontdesk.android;

import java.util.Locale;

import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import constants.StringConstants;
import utility.DialogViewWarning;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;
import appschema.SharedPreferenceController;

public class ActivityHaveAnyDevices extends Activity {

	private DialogViewWarning dialog;
	private RelativeLayout relativeLayoutExisitingVisitor;
	private ImageView imageviewBack, imageviewHome, imageviewPrevious,
			imageviewNext,image_view_next;
	private TextView textviewExtDeviceTitle, textviewHaveExtDeviceAccept;
	private Button buttonHaveDeviceYes,buttonHaveDeviceNo;
	private Typeface typeFaceTitle, typeFaceAccept;
	private int actionCode = MainConstants.backPress;
	private Handler noActionHandler;
	private TextToSpeech textToSpeech;
	private boolean isHaveDevice=false;
	private SharedPreferenceController sharedPreferenceController;
	private int applicationMode= NumberConstants.kConstantZero;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove the title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// hide the status bar.
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_have_any_devices);
//		Log.d("DSK "," VisitorImagelength "+ SingletonVisitorProfile.getInstance()
//				.getVisitorImage().length);
		assignObjects();
	}

	/**
	 * This function is called, when the activity is started.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onStart() {
		super.onStart();
		
	}

	/**
	 * This function is called, when the activity is stopped.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onStop() {
		super.onStop();
	}

	/**
	 * This function is called, when the activity is destroyed.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onDestroy() {
		super.onDestroy();
		deallocateObjects();
	}

	/**
	 * Assign Objects to the variable
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void assignObjects() {

		textviewHaveExtDeviceAccept = (TextView) findViewById(R.id.textview_have_external_accept);
		textviewExtDeviceTitle = (TextView) findViewById(R.id.textview_have_device_title);

		imageviewBack = (ImageView) findViewById(R.id.imageview_back);
		imageviewHome = (ImageView) findViewById(R.id.imageview_home);
		imageviewNext = (ImageView) findViewById(R.id.imageview_next);
		imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
		image_view_next = (ImageView) findViewById(R.id.imageview_next_page);

		relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);
		buttonHaveDeviceYes = (Button) findViewById(R.id.button_have_device_yes);
		buttonHaveDeviceNo = (Button) findViewById(R.id.button_have_device_no);
		
		noActionHandler = new Handler();
		noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

		if (textToSpeechListener != null) {
			textToSpeech = new TextToSpeech(getApplicationContext(),
					textToSpeechListener);
		}
		
		// set font
		typeFaceTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantHKGroteskBold);
		typeFaceAccept = Typeface.createFromAsset(
				getAssets(), FontConstants.fontConstantHKGroteskSemiBold);
		textviewHaveExtDeviceAccept.setTypeface(typeFaceTitle);
		textviewExtDeviceTitle.setTypeface(typeFaceAccept);
		buttonHaveDeviceNo.setTypeface(typeFaceTitle);
		buttonHaveDeviceYes.setTypeface(typeFaceTitle);

		assignListeners();

		sharedPreferenceController = new SharedPreferenceController();
		if(sharedPreferenceController!=null) {
			applicationMode = sharedPreferenceController.getApplicationMode(getApplicationContext());
		}

		setAppBackground();
	}

	private void assignListeners()
	{
		// Onclick listeners
		image_view_next.setOnClickListener(listenerConfirm);
		imageviewPrevious.setOnClickListener(listenerPrevious);
		imageviewNext.setOnClickListener(listenerConfirm);
		imageviewHome.setOnClickListener(listenerHomePage);
		imageviewBack.setOnClickListener(listenerBack);
		buttonHaveDeviceNo.setOnClickListener(listenerHaveDeviceNo);
		buttonHaveDeviceYes.setOnClickListener(listenerHaveDeviceYes);
	}
	/**
	 * Set background color
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void setAppBackground() {
		if( SingletonMetaData.getInstance()!=null && SingletonMetaData.getInstance().getThemeCode()>MainConstants.kConstantZero)
		{
			if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantOne) {
				relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
			}
			else if(SingletonMetaData.getInstance().getThemeCode()==MainConstants.kConstantTwo)
			{
				GradientDrawable gd = new GradientDrawable(
					GradientDrawable.Orientation.BOTTOM_TOP, new int[] {
					0xFF4bbcef, 0xFF46c0e5, 0xFF3ec5d4, 0xFF35ccc1 });
				relativeLayoutExisitingVisitor.setBackground(gd);
			}
		}
	}

	/**
	 * Skip employee name 
	 * 
	 * @author Karthick
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerHaveDeviceNo = new OnClickListener() {
		public void onClick(View v) {
			isHaveDevice=false;
			removeSingletonValues();
			launchNextPage();
		}
	};

	/**
	 * remove Single ton Values
	 *
	 * @Aurthor Karthikeyan D S
	 * @Created on 06JUL2018
	 */
	private void removeSingletonValues()
	{
		if(SingletonVisitorProfile.getInstance()!=null)
		{
			if(SingletonVisitorProfile.getInstance().getOtherDeviceDetails()!=null)
			{
				SingletonVisitorProfile.getInstance().setOtherDeviceDetails(null);
			}
			if(SingletonVisitorProfile.getInstance().getDeviceMake()!=null)
			{
				SingletonVisitorProfile.getInstance().setDeviceMake(null);
			}
			if(SingletonVisitorProfile.getInstance().getDeviceSerialNumber()!=null)
			{
				SingletonVisitorProfile.getInstance().setDeviceSerialNumber(null);
			}
			if(SingletonVisitorProfile.getInstance().isExtDeviceFive())
			{
				SingletonVisitorProfile.getInstance().setExtDeviceFive(false);
			}
			if(SingletonVisitorProfile.getInstance().isExtDeviceFour())
			{
				SingletonVisitorProfile.getInstance().setExtDeviceFour(false);
			}
			if(SingletonVisitorProfile.getInstance().isExtDeviceThree())
			{
				SingletonVisitorProfile.getInstance().setExtDeviceThree(false);
			}
			if(SingletonVisitorProfile.getInstance().isExtDeviceTwo())
			{
				SingletonVisitorProfile.getInstance().setExtDeviceTwo(false);
			}
			if(SingletonVisitorProfile.getInstance().isExtDeviceOne())
			{
				SingletonVisitorProfile.getInstance().setExtDeviceOne(false);
			}

		}
	}
	/**
	 * Skip employee name 
	 * 
	 * @author Karthick
	 * 
	 * @created 20160913
	 */
	private OnClickListener listenerHaveDeviceYes = new OnClickListener() {
		public void onClick(View v) {
			isHaveDevice=true;
			launchNextPage();
		}
	};
	
	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
		@Override
		public void onInit(int status) {
			if (textToSpeech != null && status != TextToSpeech.ERROR) {
				textToSpeech.setLanguage(Locale.ENGLISH);
			}
			textSpeach(MainConstants.kConstantOne);
		}
	};

	/**
	 * intent to launch previous page
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerBack = new OnClickListener() {

		public void onClick(View v) {
			actionCode = MainConstants.backPress;
			launchPreviousPage();
			// showDetailDialog();
		}
	};

	/**
	 * intent to launch previous page
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerHomePage = new OnClickListener() {

		public void onClick(View v) {
			actionCode = MainConstants.homePress;
			hideKeyboard();
			showDetailDialog();
		}
	};

	/**
	 * Verify Existing visitor in creator and launched next activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerConfirm = new OnClickListener() {
		public void onClick(View v) {
			launchNextPage();
		}
	};

	/**
	 * Verify Existing visitor in creator and launched next activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private OnClickListener listenerPrevious = new OnClickListener() {
		public void onClick(View v) {
			launchPreviousPage();
		}
	};

	/**
	 * Launch previous activity
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void launchHomePage() {
		if (actionCode == MainConstants.homePress) {
			Intent intent = new Intent(this, WelcomeActivity.class);
			startActivity(intent);
		} else {
			launchPreviousPage();
		}

		finish();
	}

	/**
	 * Launch previous activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void launchPreviousPage() {
		float density = getResources().getDisplayMetrics().density;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && density< 2) {
			Intent intent = new Intent(getApplicationContext(),
					ActivityCameraTwoApi.class);
			startActivity(intent);
		} else {
			Intent intent = new Intent(getApplicationContext(),
					GetImageActivity.class);
			startActivity(intent);
		}
		finish();
	}

	/**
	 * Launch next activity.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void launchNextPage() {
		SingletonVisitorProfile.getInstance().setisHaveDevice(isHaveDevice);
		if(isHaveDevice) {
			if(applicationMode==NumberConstants.kConstantNormalMode) {
				Intent intent = new Intent(getApplicationContext(),
						ActivityDeviceDetails.class);
				startActivity(intent);
			}
			else if(applicationMode==NumberConstants.kConstantDataCentreMode)
			{
				Intent intent = new Intent(getApplicationContext(),
						ActivityDataCentreDeviceDetails.class);
				startActivity(intent);
			}
		} else {
			Intent intent = new Intent(getApplicationContext(),
					ActivityVisitorTempID.class);
			startActivity(intent);
		}
		finish();
	}

	/**
	 * Goto home page when no action.
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 * 
	 */
	public Runnable noActionThread = new Runnable() {
		public void run() {
			actionCode = MainConstants.homePress;
			launchHomePage();
		}
	};

	/**
	 * Next process
	 * 
	 * @author Karthick
	 * @created on 20161114
	 */
	Runnable purposeSeletionTask = new Runnable() {
		public void run() {
			launchNextPage();
		}
	};
	
	/**
	 * This function is called when the back pressed.
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	public void onBackPressed() {
		actionCode = MainConstants.backPress;
		launchPreviousPage();
	}

	/**
	 * Hide keyboard
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void hideKeyboard() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	/**
	 * Show dialog when have user data Otherwise goto welcome page
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void showDetailDialog() {
		showWarningDialog();
		textSpeach(MainConstants.speachAreYouSureCode);
	}

	/**
	 * Check if have value on edittext Then show warning dialog Otherwise goto
	 * back or home
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void showWarningDialog() {
		hideKeyboard();
		if (dialog == null) {
			dialog = new DialogViewWarning(this);
		}
		dialog.setDialogCode(MainConstants.dialogDecision);
		dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
		dialog.setActionListener(listenerDialogAction);
		dialog.showDialog();
	}

	/**
	 * Handle tab click for goto settings page
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
		public void onClick(View v) {
			launchHomePage();
		}
	};

	/**
	 * Play text to voice
	 * 
	 * @author Karthick
	 * @created on 20170508
	 */
	private void textSpeach(int textSpeachCode) {

		if (textToSpeech != null) {
			textToSpeech.stop();
			if (textSpeachCode == MainConstants.kConstantOne
					&& (SingletonVisitorProfile.getInstance() != null
							&& SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null && 
									SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty())) {
				textToSpeech.speak(TextToSpeachConstants.speachHaveLaptop,
						TextToSpeech.QUEUE_FLUSH, null, null);
			} else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
				textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
						TextToSpeech.QUEUE_FLUSH, null, null);
			}
		}
	}

	/**
	 * Deallocate Objects in the activity
	 * 
	 * @author Karthick
	 * 
	 * @created on 20170508
	 */
	private void deallocateObjects() {
		imageviewBack = null;
		imageviewHome = null;
		imageviewNext = null;
		textviewExtDeviceTitle = null;
		typeFaceTitle = null;
		relativeLayoutExisitingVisitor
				.setBackgroundResource(MainConstants.kConstantZero);
		relativeLayoutExisitingVisitor = null;
		if (noActionHandler != null) {
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler = null;
		}
		if(textToSpeech!=null)
		{
			textToSpeech.stop();
		}
		applicationMode=  NumberConstants.kConstantZero;
	}
}
