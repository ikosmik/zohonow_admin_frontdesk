package com.zoho.app.frontdesk.android;

import java.util.ArrayList;
import java.util.Locale;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import constants.NumberConstants;
import constants.StringConstants;
import utility.DialogViewWarning;
import utility.CustomArrayAdapter;
import appschema.SharedPreferenceController;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import constants.FontConstants;
import constants.MainConstants;
import constants.TextToSpeachConstants;

public class ActivityVisitorPurpose extends Activity {

    private static final int requestCode = 100;
    private DialogViewWarning dialog;
    private Spinner spinnerPurpose;
    private RelativeLayout relativeLayoutExisitingVisitor;
    private ImageView imageviewBack, imageviewHome, imageviewPrevious,
            imageviewNext, image_view_next;
    private Button buttonTakePic;
    private TextView textviewVisitorPurposeTitle;
    private ArrayList<String> purposeList = new ArrayList<String>();
    private CustomArrayAdapter adapterPurpose;
    //private Handler handlerPurpose;
    private Typeface typeFaceTitle;
    private int actionCode = MainConstants.backPress;
    private int applicationMode =  NumberConstants.kConstantZero;
    private SharedPreferenceController spController = new SharedPreferenceController();
    /**
     * Goto home page when no action.
     *
     * @author Karthick
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };
    private boolean isReadyPurposeSelect = false;
    private Handler noActionHandler;
    private TextToSpeech textToSpeech;
    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnInitListener textToSpeechListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (textToSpeech != null && status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.ENGLISH);
            }
            textSpeach(MainConstants.kConstantOne);
        }
    };
    /**
     * Set incharge email depends selected location
     *
     * @author Karthick
     * @created 20160526
     */
    private OnItemSelectedListener listenerPurposeSelection = new OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parentView,
                                   View selectedItemView, int position, long id) {
            if (isReadyPurposeSelect) {
                if (SingletonVisitorProfile.getInstance() != null
                        && spinnerPurpose != null
                        && spinnerPurpose.getSelectedItem() != null
                        && !spinnerPurpose.getSelectedItem().toString()
                        .isEmpty()) {
                    SingletonVisitorProfile.getInstance()
                            .setPurposeOfVisitCode(
                                    spinnerPurpose.getSelectedItem().toString());
                }
//				handlerPurpose.postDelayed(purposeSeletionTask, 1000);
            } else {
                isReadyPurposeSelect = true;
            }
        }

        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerBack = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            // showDetailDialog();
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerConfirm = new OnClickListener() {
        public void onClick(View v) {
            if (validatePurpose()) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(ActivityVisitorPurpose.this, new String[]{Manifest.permission.CAMERA}, requestCode);
                } else {
                    launchNextPage();
                }

            }
        }
    };
    /**
     * Verify Existing visitor in creator and launched next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerPrevious = new OnClickListener() {
        public void onClick(View v) {
            launchPreviousPage();
        }
    };
    /**
     * Handle tab click for goto settings page
     *
     * @author Karthick
     * @created on 20170508
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            launchHomePage();
        }
    };
    /**
     * intent to launch previous page
     *
     * @author Karthick
     * @created on 20170508
     */
    private OnClickListener listenerHomePage = new OnClickListener() {

        public void onClick(View v) {
            actionCode = MainConstants.homePress;
            hideKeyboard();
            showDetailDialog();
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_visitor_purpose);

        assignObjects();
        setPurposeList();
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStart() {
        super.onStart();

    }

    /**
     * This function is called, when the activity is stopped.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onStop() {
        super.onStop();

    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * Assign Objects to the variable
     *
     * @author Karthick
     * @created on 20170508
     */
    private void assignObjects() {

        spinnerPurpose = (Spinner) findViewById(R.id.spinner_purpose);
        textviewVisitorPurposeTitle = (TextView) findViewById(R.id.textview_purpose_title);
        imageviewBack = (ImageView) findViewById(R.id.imageview_back);
        imageviewHome = (ImageView) findViewById(R.id.imageview_home);
        imageviewNext = (ImageView) findViewById(R.id.imageview_next);
        imageviewPrevious = (ImageView) findViewById(R.id.imageview_previous);
        relativeLayoutExisitingVisitor = (RelativeLayout) findViewById(R.id.relativelayout_exisiting);
        buttonTakePic = (Button) findViewById(R.id.button_take_pic);
//		handlerPurpose = new Handler();
        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);

        if (textToSpeechListener != null) {
//			textToSpeech.stop();
            textToSpeech = new TextToSpeech(getApplicationContext(),
                    textToSpeechListener);
        }
        applicationMode = spController.getApplicationMode(this);
        // set font
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        buttonTakePic.setTypeface(typeFaceTitle);
        textviewVisitorPurposeTitle.setTypeface(typeFaceTitle);

        image_view_next = (ImageView) findViewById(R.id.imageview_next_page);


        // Onclick listeners
        if (image_view_next != null && listenerConfirm != null) {
            image_view_next.setOnClickListener(listenerConfirm);
        }
        if (imageviewPrevious != null && listenerPrevious != null) {
            imageviewPrevious.setOnClickListener(listenerPrevious);
        }
        if (imageviewNext != null && listenerConfirm != null) {
            imageviewNext.setOnClickListener(listenerConfirm);
        }
        if (buttonTakePic != null && listenerConfirm != null) {
            buttonTakePic.setOnClickListener(listenerConfirm);
        }
        if (imageviewHome != null && listenerHomePage != null) {
            imageviewHome.setOnClickListener(listenerHomePage);
        }
        if (imageviewBack != null && listenerBack != null) {
            imageviewBack.setOnClickListener(listenerBack);
        }
        if (spinnerPurpose != null && listenerPurposeSelection != null) {
            spinnerPurpose.setOnItemSelectedListener(listenerPurposeSelection);
        }

        setAppBackground();
    }

    /**
     * Set background color
     *
     * @author Karthick
     * @created on 20170508
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutExisitingVisitor.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFba6fb6, 0xFF8e76c3, 0xFF8478c6, 0xFF8278c7});
                relativeLayoutExisitingVisitor.setBackground(gd);
            }
        }
    }

    /**
     * Set list of purpose to spinner
     *
     * @author Karthick
     * @created 20170420
     */
    private void setPurposeList() {
        if(applicationMode==NumberConstants.kConstantNormalMode) {
            if (purposeList != null) {
                purposeList.clear();
                if (SingletonEmployeeProfile.getInstance() != null
                        && SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                        && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment().contains(
                        MainConstants.HRDepartment)
                        || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                        .equals(MainConstants.HRDepartmentAnotherNamePlural)
                        || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                        .equals(MainConstants.HRDepartmentAnotherNameSingular))) {
                    purposeList.add(MainConstants.purposeInterview);
                }
                purposeList.add(MainConstants.purposeOfficial);
                purposeList.add(MainConstants.purposePersonal);
                purposeList.add(MainConstants.purposeGadgetSpecialist);
                purposeList.add(MainConstants.purposeBankingExecutive);
                purposeList.add(MainConstants.purposeAuditor);
                purposeList.add(MainConstants.purposeHealthCareExpert);
                purposeList.add(MainConstants.purposeDoctor);
                purposeList.add(MainConstants.purposePress);
                purposeList.add(MainConstants.purposePartner);
                //Add Government or official
//			if(SingletonVisitorProfile.getInstance().getDeviceLocation().equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
                purposeList.add(MainConstants.purposeGovernment);
//			}
            }

            if (adapterPurpose == null) {

                adapterPurpose = new CustomArrayAdapter(this,
                        android.R.layout.simple_spinner_item, purposeList);
                adapterPurpose
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerPurpose.setAdapter(adapterPurpose);
            } else {
                adapterPurpose.notifyDataSetChanged();
            }

            if (SingletonVisitorProfile.getInstance() != null
                    && SingletonVisitorProfile.getInstance()
                    .getPurposeOfVisitCode() != null
                    && !SingletonVisitorProfile.getInstance()
                    .getPurposeOfVisitCode().isEmpty()
                    && spinnerPurpose != null
                    && purposeList != null
                    && purposeList.size() > 0
                    && purposeList.indexOf(SingletonVisitorProfile
                    .getInstance().getPurposeOfVisitCode()) >= 0) {
                spinnerPurpose.setSelection(purposeList
                        .indexOf(SingletonVisitorProfile.getInstance()
                                .getPurposeOfVisitCode()), false);
            } else if (SingletonVisitorProfile.getInstance() != null
                    && SingletonVisitorProfile.getInstance()
                    .getPurposeOfVisitCode() != null
                    && SingletonVisitorProfile.getInstance()
                    .getPurposeOfVisitCode().isEmpty()
                    && spinnerPurpose != null && purposeList != null
                    && purposeList.size() > 0) {
                spinnerPurpose.setSelection(0, false);
            }
        }
        else if(applicationMode==NumberConstants.kConstantDataCentreMode)
        {
            spinnerPurpose.setVisibility(View.GONE);
            textviewVisitorPurposeTitle.setVisibility(View.GONE);
        }
    }

    /**
     * Launch previous activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchHomePage() {
        if (actionCode == MainConstants.homePress) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
        } else {
            launchPreviousPage();
        }
        finish();
    }

    /**
     * Launch previous activity.
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    private void launchPreviousPage() {
//		To Launch ActivityEmployee
        if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 1) {
//			call Activity activityEmployee by using launch code 1.
            launchCodeforActivity(1);
        }
//		To Launch ActivityEmployeeTenkasi
        else if (SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 2) {
//			call Activity activityEmployeeTenkasi by using launch code 2.
            launchCodeforActivity(2);
        }
        else if(SingletonVisitorProfile.getInstance() != null && SingletonVisitorProfile.getInstance().getEmployeeActivityCode() == 3)
        {
            launchCodeforActivity(1);
        }
        else {
            if (SingletonVisitorProfile.getInstance()!=null && !SingletonVisitorProfile.getInstance().getDeviceLocation().isEmpty()
                   && !SingletonVisitorProfile.getInstance().getDeviceLocation().equalsIgnoreCase(MainConstants.kConstantDeviceLocationSix)) {
//				call Activity activity all Employee by using launch code 1.
                launchCodeforActivity(1);
            }
            else {
//				call Activity activityEmployeeTenkasi by using launch code 2.
                launchCodeforActivity(2);
            }

        }
    }

    /**
     * Launch code activity for nextpage.
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    private void launchCodeforActivity(int launchCode) {
//		call Activity activityEmployeeTenkasi by using launch code 1.
        if (launchCode == 2) {
            Intent intent = new Intent(getApplicationContext(), ActivityVisitorEmployeeTenkasi.class);
            startActivity(intent);
            finish();
        }
//		call Activity activityEmployee by using launch code 2.
        else if (launchCode == 1) {
            Intent intent = new Intent(getApplicationContext(), ActivityVisitorEmployee.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Next process
     *
     * @author Karthick
     * @created on 20161114
     */
//	Runnable purposeSeletionTask = new Runnable() {
//		public void run() {
//			launchNextPage();
//		}
//	};

    /**
     * Launch next activity.
     *
     * @author Karthick
     * @created on 20170508
     */
    private void launchNextPage() {
        if(applicationMode==NumberConstants.kConstantDataCentreMode)
        {
            SingletonVisitorProfile.getInstance().setEmployeeActivityCode(3);
        }
        float density = getResources().getDisplayMetrics().density;
//        Log.d("DSK Density",""+density);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && density < 2) {
//            Log.d("DSK Density","in 1 "+density);
            Intent intent = new Intent(getApplicationContext(),
                    ActivityCameraTwoApi.class);
            startActivity(intent);
        } else {
//            Log.d("DSK Density","in 2 "+density);
            Intent intent = new Intent(getApplicationContext(),
                    GetImageActivity.class);
            startActivity(intent);
        }
        finish();
    }

    /**
     * This function is called when the back pressed.
     *
     * @author Karthick
     * @created on 20170508
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * Check is existing user or not
     *
     * @author Karthick
     * @created 20170508
     */
    private boolean validatePurpose() {
//        Log.d("DSKApplicationMode",""+spController.getApplicationMode(this));

        if (applicationMode == NumberConstants.kConstantNormalMode) {
            if (spinnerPurpose != null && spinnerPurpose.getSelectedItem() != null
                    && !spinnerPurpose.getSelectedItem().toString().isEmpty()) {
                SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
                        spinnerPurpose.getSelectedItem().toString());
            } else {
                SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
                        MainConstants.purposeOfficial);
            }
            return true;
        } else if(applicationMode == NumberConstants.kConstantDataCentreMode){
            SingletonVisitorProfile.getInstance().setPurposeOfVisitCode(
                    MainConstants.purposeDataCenter);
            return true;
        }
        return false;
    }

    /**
     * Hide keyboard
     *
     * @author Karthick
     * @created on 20170508
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show dialog when have user data Otherwise goto welcome page
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showDetailDialog() {
        showWarningDialog();
        textSpeach(MainConstants.speachAreYouSureCode);
    }

    /**
     * Check if have value on edittext Then show warning dialog Otherwise goto
     * back or home
     *
     * @author Karthick
     * @created on 20170508
     */
    private void showWarningDialog() {
        hideKeyboard();
        if (dialog == null) {
            dialog = new DialogViewWarning(this);
        }
        dialog.setDialogCode(MainConstants.dialogDecision);
        dialog.setDialogMessage(StringConstants.kConstantHomePageErrorMessage);
        dialog.setActionListener(listenerDialogAction);
        dialog.showDialog();
    }

    /**
     * Play text to voice
     *
     * @author Karthick
     * @created on 20170508
     */
    private void textSpeach(int textSpeachCode) {

        if (textToSpeech != null) {
            textToSpeech.stop();
            if (textSpeachCode == MainConstants.kConstantOne
                    && (SingletonVisitorProfile.getInstance() != null
                    && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null &&
                    SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty())) {
                textToSpeech.speak(TextToSpeachConstants.speachPurpose,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            } else if (textSpeachCode == MainConstants.speachAreYouSureCode) {
                textToSpeech.speak(TextToSpeachConstants.speachAreYouSure,
                        TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }

    /**
     * Deallocate Objects in the activity
     *
     * @author Karthick
     * @created on 20170508
     */
    private void deallocateObjects() {
        spinnerPurpose = null;
        imageviewBack = null;
        imageviewHome = null;
        imageviewNext = null;
        textviewVisitorPurposeTitle = null;
        typeFaceTitle = null;
        relativeLayoutExisitingVisitor
                .setBackgroundResource(MainConstants.kConstantZero);
        relativeLayoutExisitingVisitor = null;
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
        }
        applicationMode = NumberConstants.kConstantZero;
    }
}
