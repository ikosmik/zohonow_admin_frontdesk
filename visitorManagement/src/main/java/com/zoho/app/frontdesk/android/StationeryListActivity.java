package com.zoho.app.frontdesk.android;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonVisitorProfile;
import constants.StringConstants;
import utility.CustomScrollView;
import utility.CustomScrollViewInterface;
import utility.DialogView;
import utility.JSONParser;
import utility.NetworkConnection;
import utility.TimeZoneConventer;
import utility.stationeryListGridViewAdaptor;
import zohocreator.VisitorZohoCreator;
import utility.stationeryListGridViewAdaptor.ViewHolder;
import logs.InternalAppLogs;

import constants.DataBaseConstants;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import database.dbschema.Stationery;
import database.dbschema.StationeryEntry;
import appschema.StationeryRequest;
import database.dbhelper.DbHelper;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView.OnEditorActionListener;

/**
 * show stationery item list in grid view and employee choose your stationery
 * item with quantity, send request to the security,
 * 
 * @author jayapriya
 * @created on 19/03/16
 * 
 */
public class StationeryListActivity extends Activity implements
		CustomScrollViewInterface {
	VisitorZohoCreator creator;
	private GridView gridViewstationery;
	private List<Stationery> listOfStationery = new ArrayList<Stationery>();
	DbHelper dbHelper;
	private Typeface fontButton, fontWorkSans, fontBlackJerk, fontJournal,
			fontAnuDaw;
	Button buttonSubmit, buttonCancel, tellUs;
	// private TextView textViewTitle;
	Handler handlerHideDialog;
	stationeryListGridViewAdaptor adapter;
	private TextView textViewEmployeeName, textViewNoStationaryItem;
	private Boolean IsallowedToClickStationaryItems = true;
	Context context;
	long currentTime = 0L;
	int stationery_quantity;
	// private LruCache<String, Bitmap> mMemoryCache;
	private ImageView imageViewBack, reportImage;
	private int chooseOption = 0;
	public HashMap<Integer, StationeryEntry> map;
	private ProgressBar progressBar;
	private View seperator;
	private RelativeLayout relativeLayout;
	Dialog dialogGetHomeMessage, reportDialog;
	TextView textViewDialogHomeMessage, textViewHomeMessageConfirm,
			textViewHomeMessageCancel, textViewReportMsg,
			textViewReportFirstMsg, textViewReportErrorCode;

	private Handler noActionHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		// Set the orientation as Portrait
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			// this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
		}
		setContentView(R.layout.activity_stationery_list);
		// Log.d("onCreate", "create list");
		assignObjects();
	}

	/*
	 * Assign objects to the variable.
	 */
	private void assignObjects() {

		gridViewstationery = (GridView) findViewById(R.id.gridview_stationery);
		buttonSubmit = (Button) findViewById(R.id.button_confirm);
		buttonCancel = (Button) findViewById(R.id.button_cancel);
		imageViewBack = (ImageView) findViewById(R.id.image_view_stationery_entry_back);
		// textViewTitle = (TextView) findViewById(R.id.text_stationery_entry);
		textViewEmployeeName = (TextView) findViewById(R.id.text_employee_name);
		textViewNoStationaryItem = (TextView) findViewById(R.id.text_view_no_item);

		fontButton = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantSourceSansProbold);
		fontWorkSans = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantWorkSans);
		fontBlackJerk = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantBlackJerk);
		fontJournal = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantJournal);
		fontAnuDaw = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantNoteThisReg);
		// fontTamil=Typeface.createFromAsset(getAssets(),
		// FontConstants.fontAkshar);
		buttonCancel.setTypeface(fontWorkSans);
		buttonSubmit.setTypeface(fontWorkSans);
		textViewNoStationaryItem.setTypeface(fontButton);
		// textViewTitle.setTypeface(fontButton);
		textViewEmployeeName.setTypeface(fontBlackJerk);
		dbHelper = new DbHelper(this);
		creator = new VisitorZohoCreator(this);
		handlerHideDialog = new Handler();
		context = this;
		map = new HashMap<Integer, StationeryEntry>();
		noActionHandler = new Handler();
		noActionHandler.postDelayed(noActionThread, MainConstants.noFaceTime);

		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;
		// //Log.d("cache","size"+cacheSize);
		/*
		 * mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
		 * 
		 * @Override protected int sizeOf(String key, Bitmap bitmap) { // The
		 * cache size will be measured in kilobytes rather than // number of
		 * items. // ////Log.d("lru","size"+bitmap.getByteCount() / //
		 * 1024+"key "+key); //
		 * ////Log.d("lru","size"+mMemoryCache.trimToSize(maxSize));
		 * 
		 * return bitmap.getByteCount() / 1024; } };
		 */
		// initializeGridAdapter(NumberConstants.isNewGrid);
		// inputMethodManager = (InputMethodManager)
		// getSystemService(Context.INPUT_METHOD_SERVICE);

		if ((SingletonEmployeeProfile.getInstance().getEmployeeName() != null)
				&& (SingletonEmployeeProfile.getInstance().getEmployeeName() != "")) {
			String[] name = SingletonEmployeeProfile.getInstance()
					.getEmployeeName().split(" ");
			if (TimeZoneConventer.showHappyGreeting() != "") {
				textViewEmployeeName.setText(TimeZoneConventer
						.showHappyGreeting()
						+ name[0]
						+ StringConstants.kConstantSelectStationeryMsg);
			} else {
				textViewEmployeeName
						.setText(StringConstants.kConstantGreetingMorning
								+ name[0]
								+ StringConstants.kConstantSelectStationeryMsg);
			}
		} else {
			getHomePage();
		}

		listOfStationery.clear();
		listOfStationery.addAll(dbHelper.getAllStationeryBasedOnCode(1));
		if (listOfStationery.size() > MainConstants.kConstantZero) {
			gridViewstationery.setVisibility(View.VISIBLE);
			adapter = new stationeryListGridViewAdaptor(this, listOfStationery,
					fontJournal, fontAnuDaw,listenerStationeryItemSelection);
			gridViewstationery.setAdapter(adapter);
			textViewNoStationaryItem.setVisibility(View.INVISIBLE);
		} else {
			gridViewstationery.setVisibility(View.INVISIBLE);
			textViewNoStationaryItem.setVisibility(View.VISIBLE);
		}

		buttonSubmit.setOnClickListener(listenerTappedSubmit);
		buttonCancel.setOnClickListener(listenerTappedCancel);
		imageViewBack.setOnClickListener(listenerTappedCancel);

	}

	/**
	 * Goto home page when no action.
	 * 
	 * @author karthick
	 * @created 16/06/2015
	 * 
	 */
	public Runnable noActionThread = new Runnable() {
		public void run() {
			// Log.d("list", "noActionThread");
			getHomePage();

		}
	};

	/*
	 * Created by karthick On 28/04/2015 intent to launch the home page.
	 */
	private void getHomePage() {

		Intent intent = new Intent(getApplicationContext(),
				WelcomeActivity.class);
		startActivity(intent);

		noActionHandler.removeCallbacks(noActionThread);
		DialogView.hideDialog();
		finish();

		// this.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
		// this.overridePendingTransition(R.anim.animation_leave,
		// R.anim.animation_enter);
	}

	/**
	 * deallocate objects.
	 * 
	 * @author jayapriya Created On 27/1/2016
	 * 
	 */
	private void deallocateObjects() {
		// creator =null;
		// gridViewstationery = null;
		listOfStationery.clear();
		listOfStationery = null;
		// dbHelper=null;
		fontButton = null;
		fontWorkSans = null;
		// buttonSubmit=null;
		// buttonCancel=null;
		// textViewTitle = null;
		handlerHideDialog = null;
		context = null;
		stationery_quantity = 0;
		// imageViewBack=null;

	}

	/**
	 * Created On 31/10/2014 (non-Javadoc)
	 * 
	 * @author jayapriya
	 * @see android.app.Activity#onResume()
	 * 
	 */
	public void onResume() {
		super.onResume();
		if (noActionHandler != null) {
			noActionHandler.removeCallbacks(noActionThread);
			noActionHandler.postDelayed(noActionThread,
					MainConstants.afterTypingNoFaceTime);
		}

	}

	private void setGridView() {
		// ////////////Log.d("selectedStationeryListFinal","size"+selectedStationeryListFinal.size());
		map.clear();
		listOfStationery.clear();
		listOfStationery.addAll(dbHelper.getAllStationery());
		if (listOfStationery.size() > MainConstants.kConstantZero) {
			gridViewstationery.setVisibility(View.VISIBLE);
			gridViewstationery.setAdapter(new stationeryListGridViewAdaptor(
					this, listOfStationery, fontJournal, fontAnuDaw,listenerStationeryItemSelection));
		} else {
			gridViewstationery.setVisibility(View.INVISIBLE);
		}

	}

	/*
	 * Add bitmap to memory cache
	 * 
	 * @author karthick
	 * 
	 * @created 27/05/2015
	 */
	/*
	 * public void addBitmapToMemoryCache(String key, Bitmap bitmap) { //
	 * ////Log.d("add cache",""+bitmap ); if (getBitmapFromMemCache(key) ==
	 * null) { // ////Log.d("add","cache"+key); mMemoryCache.put(key, bitmap);
	 * // ////Log.d("add","cache"+getBitmapFromMemCache(key)); } }
	 * 
	 * /* Clear memory catch and deallocate the objects.
	 * 
	 * @author jayapriya
	 * 
	 * @created on 19/03/16
	 */
	protected void onDestroy() {
		super.onDestroy();
		// //Log.d("on","destroy");
		// mMemoryCache.evictAll();
		deallocateObjects();
		noActionHandler.removeCallbacks(noActionThread);
		DialogView.hideDialog();
		DialogView.setNullDialog();

	}

	/*
	 * Get bitmap from memory cache
	 * 
	 * @author karthick
	 * 
	 * @created 27/05/2015
	 */
	/*
	 * public Bitmap getBitmapFromMemCache(String key) { //
	 * //Log.d("get","cache"+key); return mMemoryCache.get(key); }
	 * 
	 * /* Add bitmap to memory cache
	 * 
	 * 
	 * @author karthick
	 * 
	 * @created 27/05/2015
	 */
	/*
	 * class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
	 * 
	 * ImageView stationeryImage;
	 * 
	 * public BitmapWorkerTask(ImageView image) { this.stationeryImage = image;
	 * }
	 * 
	 * // Decode image in background.
	 * 
	 * @Override protected Bitmap doInBackground(String... params) { final
	 * Bitmap bitmap = UsedBitmap.decodeBitmap(params[0], 100, 100);
	 * 
	 * // Bitmap circleBitmap=UsedBitmap.setCircleBitmap(bitmap); //
	 * //Log.d("add","cache back" +bitmap +"param "+params[0]);
	 * addBitmapToMemoryCache(String.valueOf(params[0]), bitmap); return bitmap;
	 * }
	 * 
	 * @Override protected void onPostExecute(Bitmap bitmap) {
	 * adapter.notifyDataSetChanged(); //
	 * this.stationeryImage.setImageBitmap(bitmap); // might want to change
	 * "executed" for the returned string passed // into onPostExecute() but
	 * that is upto you }
	 * 
	 * @Override protected void onPreExecute() { }
	 * 
	 * @Override protected void onProgressUpdate(Void... values) { }
	 * 
	 * }
	 * 
	 * 
	 * // onClickListeners /**
	 * 
	 * @author jayapriya
	 * 
	 * @created on mar 2016
	 */
	private OnClickListener listenerTappedSubmit = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String msg = "";
			if (chooseOption == 0) {
				chooseOption = 1;
				if (!isNetworkAvailable()) {
					noActionHandler.removeCallbacks(noActionThread);
					// showNoNetworkDialog();
					// setGridView();
					TimeZoneConventer connection = new TimeZoneConventer();
					if (!connection.isWiFiEnabled(context)) {
						connection.wifiReconnect(context);
					}
					chooseOption = 0;
					utility.UsedCustomToast.makeCustomToast(
							getApplicationContext(),
							StringConstants.kConstantErrorMsgInNetworkError,
							Toast.LENGTH_LONG, Gravity.TOP).show();
					utility.UsedCustomToast.makeCustomToast(
							getApplicationContext(),
							StringConstants.kConstantErrorMsgInNetworkError,
							Toast.LENGTH_LONG, Gravity.TOP).show();

				} else {

					currentTime = System.currentTimeMillis();
					if ((map != null) && (map.size() > 0)) {
						disableProcess();
						StationeryRequest request = new StationeryRequest();
						request.setEmployeeId(SingletonEmployeeProfile
								.getInstance().getEmployeeId());
						request.setRequestTime(currentTime);
						msg = dbHelper.insertStationeryRequest(request);
//						Log.d("keyyyyy", "stationery request"+request);

						if (msg != "") {
							sendFailureRequest(msg, ErrorConstants.SqliteError);
							uploadStationeryItemEntryFailure(ErrorConstants.SqliteError);
						} else {
							Set<Integer> keys = map.keySet();
							for (Integer key : keys) {
//								System.out.println("keyyyyy"+key);
//								Log.d("keyyyyy",""+key);
								StationeryEntry entry = (StationeryEntry) map
										.get(key);
								msg = dbHelper.insertStationeryEntry(entry);
//								Log.d("keyyyyy", "stationery"+entry.getRequestId());
//								Log.d("keyyyyy", "stationery"+entry.getCreatorId());
//								Log.d("keyyyyy", "stationery"+entry.getQuantity());
//								Log.d("keyyyyy", "stationery"+entry.getStationeryItemCode());
								if (msg != "") {
									break;
								}
							}
							if (msg != "") {
								sendFailureRequest(msg,
										ErrorConstants.SqliteError);
								uploadStationeryItemEntryFailure(ErrorConstants.SqliteError);
							} else {
								uploadStationeryItemEntry(keys);
								// disable backbutton, cancel and confirm button
								// for not allowed to changes and waiting for
								// response
								setEnableViews(false);
								IsallowedToClickStationaryItems = false;
							}
						}
					}

					else {
						initializeErrorDialog();
						showErrorDialogBox();
						// dialog.hideDialog();
						// dialog.showReportDialog(StationeryListActivity.this,MainConstants.selectAtleastOneItem,MainConstants.reportNoItemSelection);
						chooseOption = 0;
						noActionHandler.postDelayed(noActionThread,
								MainConstants.afterTypingNoFaceTime);

					}

				}
			}
		}
	};

	// method to Enable , disable the view - means it is not allowed to call the
	// listener
	private void setEnableViews(Boolean isEnable) {
		if (isEnable != null) {
			buttonSubmit.setEnabled(isEnable);
			buttonCancel.setEnabled(isEnable);
			imageViewBack.setEnabled(isEnable);
		}
	}

	private void uploadStationeryItemEntry(Set<Integer> keys) {
		JSONArray array = new JSONArray();
		String entryItem = "";
		String deviceId  = Settings.Secure.getString(getApplicationContext().getContentResolver(),
				Settings.Secure.ANDROID_ID);
		String deviceLocation = SingletonVisitorProfile.getInstance().getDeviceLocation();
		String appVersionCode = BuildConfig.VERSION_NAME;
		for (Integer key : keys) {
//			System.out.println(key);
			StationeryEntry entry = (StationeryEntry) map.get(key);
			JSONObject object = new JSONObject();
			try {
				object.put("code", entry.getStationeryItemCode());
				object.put("quantity", entry.getQuantity());

			} catch (JSONException e) {

			}
			array.put(object);
			object = null;
			// if(entryItem !=""){
			// entryItem =entryItem+ "_";
			// }
			// entryItem = entryItem + entry.getStationeryItemCode() + ":"+
			// entry.getQuantity();

		}

		JSONObject stationeryEntryObject = new JSONObject();
		try {
			stationeryEntryObject.put("StationeryItemEntry", array);
			stationeryEntryObject.put("device_id",deviceId);
			stationeryEntryObject.put("device_location",deviceLocation);
			stationeryEntryObject.put("app_version",appVersionCode);
		} catch (JSONException e) {

		}
		entryItem = stationeryEntryObject.toString();
		// Log.d("item", "entry" + entryItem);
		String requestParameter = MainConstants.kConstantCreatorAuthTokenAndScope
				// +DataBaseConstants.addStationeryRequestTime
				// +TimeZoneConventer.getDate(currentTime)
				+ DataBaseConstants.addStationeryEmployeeId
				+ SingletonEmployeeProfile.getInstance().getEmployeeId()
				+ DataBaseConstants.addStationeryEmployeeName
				+ SingletonEmployeeProfile.getInstance().getEmployeeName()
				+ DataBaseConstants.addStationeryEmployeeEmailId
				+ SingletonEmployeeProfile.getInstance().getEmployeeMailId()
				+ DataBaseConstants.addStationeryEmployeePhoneNo
				+ SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
				+ DataBaseConstants.addStationeryEmployeeZuid
				+ SingletonEmployeeProfile.getInstance().getEmployeeZuid()
				+ DataBaseConstants.addStationeryEmployeeLocation
				+ SingletonEmployeeProfile.getInstance()
						.getEmployeeSeatingLocation()
				+ DataBaseConstants.addStationeryRequestId
				+ SingletonEmployeeProfile.getInstance().getEmployeeId() + "_"
				+ TimeZoneConventer.getDate(currentTime)
				+ DataBaseConstants.addStationeryEntry
				+ stationeryEntryObject.toString();
		// + DataBaseConstants.addStationeryEntry + "StationeryItemEntry:[1]";
		utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
				StringConstants.kConstantMsgInStationeryItemSendRequest,
				Toast.LENGTH_LONG, Gravity.TOP).show();
		// sendVolleyStringRequest(
		// (MainConstants.applicationStationeryApp
		// + MainConstants.kConstantAddStationeryRequestRecord +
		// requestParameter)
		// .replace(" ", "%20"), "StationeryRequest");
		backgroundProcess backgroundProcess = new backgroundProcess(
				requestParameter);
		backgroundProcess.execute();
	}

	private OnClickListener listenerTappedCancel = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if (chooseOption == 0) {
				chooseOption = 1;
				if (map.size() > 0) {
					intializeDialogBoxForHomePage();
					showHomeDialogBox();
				} else {
					getHomePage();
				}
			}

		}
	};

	private OnClickListener listenerClear = new OnClickListener() {

		@Override
		public void onClick(View v) {

			finish();
			overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_out);

		}
	};

	/*
	 * Created by jayapriya On 01/12/2014 show dialogbox for home page
	 */
	private void showHomeDialogBox() {

		if ((dialogGetHomeMessage != null)
				&& (!dialogGetHomeMessage.isShowing())) {

			dialogGetHomeMessage.show();
		}
	}

	/*
	 * Created by jayapriya On 01/12/2014 show dialogbox for home page
	 */
	private void showErrorDialogBox() {

		if ((reportDialog != null) && (!reportDialog.isShowing())) {

			reportDialog.show();
		}
	}

	/*
	 * when the done pressed then process start.
	 * 
	 * @author jayapriya
	 * 
	 * @created 02/12/2014
	 */
	private OnEditorActionListener listenerDonePressed = new EditText.OnEditorActionListener() {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

			if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
					|| (actionId == EditorInfo.IME_ACTION_DONE)) {

				return true;
			}
			return false;
		}
	};

//	private void sendVolleyStringRequest(String url, String requestCode) {
//		// Log.d("response", "string url" + url);
//		StringRequest stringRequest = new StringRequest(Request.Method.POST,
//				url, new Response.Listener<String>() {
//
//					@Override
//					public void onResponse(String response, String reponseCode) {
//						long id = 0L;
//						JSONObject jsonObject = null;
//						setEnableViews(true);
//						IsallowedToClickStationaryItems = true;
//						if ((response != null) && (response != "")) {
//							// Log.d("request", "response" + response);
//
//							if (reponseCode.equals("StationeryRequest")) {
//								// Log.d("request", "response" + response);
//								try {
//									jsonObject = new JSONObject(response);
//								} catch (JSONException e) {
//
//								}
//								id = creator.getStationeryRequestId(jsonObject);
//								if ((id > 0L)) {
//									enableProcess();
//									utility.UsedCustomToast
//											.makeCustomToast(
//													getApplicationContext(),
//													MainConstants.kConstantMsgInStationeryItemUploadSuccess,
//													Toast.LENGTH_LONG,
//													Gravity.TOP).show();
//									Intent intent = new Intent(
//											getApplicationContext(),
//											WelcomeActivity.class);
//									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//									startActivity(intent);
//									// noActionHandler.removeCallbacks(noActionThread);
//									finish();
//								} else {
//									sendFailureRequest(
//											response,
//											ErrorConstants.SendStationeryRequestVolleyException);
//
//									uploadStationeryItemEntryFailure();
//								}
//
//							} else if (reponseCode.equals("RequestFailure")) {
//
//							} else {
//								sendFailureRequest(
//										response,
//										ErrorConstants.SendStationeryRequestVolleyException);
//
//								uploadStationeryItemEntryFailure();
//							}
//						} else {
//							// else
//							// Log.d("error", "creator" + response);
//							uploadStationeryItemEntryFailure();
//						}
//
//					}
//				}, new Response.ErrorListener() {
//
//					@Override
//					public void onErrorResponse(VolleyError error,
//							String reponseCode) {
//
//						// Log.d("creator", "error:" + error);
//						sendFailureRequest(
//								error.getMessage(),
//								ErrorConstants.SendStationeryRequestVolleyException);
//						setEnableViews(true);
//						IsallowedToClickStationaryItems = true;
//						uploadStationeryItemEntryFailure();
//						// SingletonCode.setSingletonVolleyExceptionError(MainConstants.kConstantVolleyError);
//					}
//				}, requestCode);
//
//		ApplicationController.getInstance().getRequestQueue()
//				.add(stringRequest);
//
//	}

	private void uploadStationeryItemEntryFailure(int code) {
		chooseOption = 0;
		enableProcess();
		noActionHandler.postDelayed(noActionThread,
				MainConstants.afterTypingNoFaceTime);
		/*
		 * setInternalLogs(getApplicationContext(),
		 * ErrorCode.creatorUploadStationeryItemEntryUploadFailureException,
		 * MainConstants.uploadStationeryItemEntryVolleyException);
		 * DialogView.hideDialog(); DialogView.showReportDialog(context,
		 * NumberConstants.reportStatusUploadFailure);
		 */
		// if((SingletonCode.getSingletonVolleyExceptionError()
		// !=null)&&(SingletonCode.getSingletonVolleyExceptionError() != "")){
		DialogView.hideDialog();
		DialogView.setNullDialog();
		DialogView.showReportDialog(this,
				MainConstants.uploadStationeryFailure,
				code);

		// }
		// setGridView();

	}

	private void enableProcess() {
		buttonCancel.setEnabled(true);
		buttonSubmit.setEnabled(true);
		imageViewBack.setEnabled(true);
		if (gridViewstationery != null) {
			gridViewstationery.setEnabled(true);
		}

	}

	private void disableProcess() {
		buttonCancel.setEnabled(false);
		buttonSubmit.setEnabled(false);
		imageViewBack.setEnabled(false);
		if (gridViewstationery != null) {
			gridViewstationery.setEnabled(false);
		}
	}

	/*
	 * Find network is connected or not
	 * 
	 * @created on 20150510
	 * 
	 * @author karthick
	 */
	private boolean isNetworkAvailable() {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	/*
	 * Show no network dialog
	 * 
	 * @created on 20150510 @author karthick
	 */
	private void showNoNetworkDialog() {

		// setInternalLogs(getApplicationContext(),ErrorCode.creatorUploadStationeryItemEntryNoNetworkException,MainConstants.noNetworkConnMsg);
		// IKDialogView.hideDialog();
		// IKDialogView.showReportDialog(context,
		// 1);
		handlerHideDialog.postDelayed(runnableHideDialog, 3000);
	}

	/*
	 * Hide dialog
	 * 
	 * @created on 20150725
	 * 
	 * @author karthick
	 */
	Runnable runnableHideDialog = new Runnable() {
		public void run() {
			// DialogView.hideDialog();
		}
	};

	/*
	 * 
	 * remove selected item in hashmap
	 */
	private void removeSelectedItemInHashMap(int position) {
		// for (int i = selectedStationeryListFinal.size() - 1; i >= 0; i--) {
		if (map.containsKey(position)) {
			map.remove(position);
		}
		// }
	}

	/*
	 * add selected item in hashmap
	 */
	private void addSelectedItemInHashMap(int position, int stationeryCode,
			int count) {

		if (count > 0) {
			// Log.d("stationery", " selectedStationeryListFinal " + stationeryCode);
			StationeryEntry entry = new StationeryEntry();
			entry.setQuantity(count);
			entry.setStationeryItemCode(stationeryCode);
			entry.setEntryTime(// TimeZoneConventer.getDate(
			(System.currentTimeMillis() / 1000) * 1000);
			if (map != null) {
				map.put(position, entry);
			}
		}

	}

//	/*
//	 * Set grid adaptor
//	 *
//	 * @created on 20150510 @author karthick
//	 */
//	public class stationeryListGridViewAdaptor extends BaseAdapter {
//		private Context context;
//		private List<Stationery> listOfStationery;
//		private Typeface fontJournal, fontAnuDaw;
//
//		public stationeryListGridViewAdaptor(Context context,
//				List<Stationery> items, Typeface fontJournal,
//				Typeface fontAnuDaw) {
//			// super(context,items);
//
//			this.context = context;
//			this.listOfStationery = items;
//			this.fontJournal = fontJournal;
//			this.fontAnuDaw = fontAnuDaw;
//
//			// ////////////Log.d("adapter", "gridview" +
//			// this.listOfStationery.size());
//		}
//
//		public class ViewHolder {
//			public EditText editTextQuantity;
//			public RelativeLayout relativeLayoutIncreaseCount,
//					relativeLayoutDecreaseCount;
//			public RelativeLayout stationeryLayout;
//			public TextView textStationeryName;
//			public int isSelected;
//			public int stationery_id;
//
//			public ViewHolder getTag() {
//
//				return null;
//			}
//
//			public EditText findViewById(int stationeryQuantity) {
//
//				return null;
//			}
//
//			// public int selected;
//		}
//
//		public View getView(final int position, View convertView,
//				ViewGroup parent) {
//
//			// ////////Log.d("position","get"+position);
//			Stationery stationeryItem = listOfStationery.get(position);
//			if (listOfStationery.get(position) != null) {
//				final ViewHolder view;
//				LayoutInflater inflater = ((Activity) context)
//						.getLayoutInflater();
//				// View gridViewstationery;
//				if (convertView == null) {
//					view = new ViewHolder();
//					convertView = inflater.inflate(
//							R.layout.grid_view_single_stationery_item, null);
//					view.stationeryLayout = (RelativeLayout) convertView
//							.findViewById(R.id.stationery_layout);
//					view.textStationeryName = (TextView) convertView
//							.findViewById(R.id.text_stationery_name);
//					view.relativeLayoutDecreaseCount = (RelativeLayout) convertView
//							.findViewById(R.id.relativeLayout_stationery_decrease_count);
//					view.relativeLayoutIncreaseCount = (RelativeLayout) convertView
//							.findViewById(R.id.relativeLayout_stationery_increase_count);
//					view.editTextQuantity = (EditText) convertView
//							.findViewById(R.id.stationery_quantity);
//					view.editTextQuantity
//							.setInputType(InputType.TYPE_CLASS_NUMBER);
//					view.editTextQuantity.setTypeface(fontAnuDaw);
//					view.editTextQuantity.setTextColor(Color.GRAY);
//					view.textStationeryName.setTypeface(fontJournal);
//					convertView.setTag(view);
//				} else {
//					view = (ViewHolder) convertView.getTag();
//				}
//				// view.editTextQuantity.setText("0");
//				// ////////Log.d("position","view"+view.editTextQuantity.getText());
//				view.isSelected = position;
//				view.stationery_id = stationeryItem.getStationeryCode();
//				if (selectedStationeryListFinal.containsKey(position)) {
//					view.editTextQuantity.setText(""
//							+ selectedStationeryListFinal.get(position).getQuantity());
//				} else {
//					view.editTextQuantity.setText("");
//				}
//
//				if ((stationeryItem != null)
//						&& (stationeryItem.getStationeryName() != "")) {
//					view.textStationeryName.setText(stationeryItem
//							.getStationeryName());
//					if ((view.editTextQuantity != null)
//							&& (view.editTextQuantity.getText().toString() != "")
//							&& (view.editTextQuantity.getText().toString()
//									.length() > 0)
//							&& (Integer.parseInt(view.editTextQuantity
//									.getText().toString()) > 0)) {
//						view.textStationeryName.setTextColor(Color.WHITE);
//						view.editTextQuantity.setTextColor(Color.WHITE);
//						view.stationeryLayout.setBackgroundColor(Color.rgb(25,
//								138, 206));
//					} else {
//						view.textStationeryName.setTextColor(Color.rgb(86, 86,
//								86));
//						view.editTextQuantity.setTextColor(Color
//								.rgb(86, 86, 86));
//						view.stationeryLayout.setBackgroundColor(Color.WHITE);
//
//					}
//				}
//				/*
//				 * if ((stationeryItem.getStationeryImage() != null) && (new
//				 * File(stationeryItem.getStationeryImage()) .exists())) {
//				 * Bitmap bitmap = getBitmapFromMemCache(stationeryItem
//				 * .getStationeryImage());
//				 *
//				 * if (bitmap != null) {
//				 * view.stationeryImage.setImageBitmap(bitmap);
//				 *
//				 *
//				 * } else { view.stationeryImage.setImageDrawable(getResources()
//				 * .getDrawable(R.drawable.photo));
//				 *
//				 * BitmapWorkerTask task = new BitmapWorkerTask(
//				 * view.stationeryImage);
//				 * task.execute(stationeryItem.getStationeryImage());
//				 *
//				 *
//				 * } } else {
//				 * view.stationeryImage.setImageDrawable(getResources()
//				 * .getDrawable(R.drawable.photo));
//				 *
//				 *
//				 * }
//				 */
//
//				view.editTextQuantity.setTag(view);
//				view.editTextQuantity.setOnTouchListener(new OnTouchListener() {
//
//					@Override
//					public boolean onTouch(View v, MotionEvent event) {
//
//						// Log.d("ontouch","text");
//						ViewHolder viewHolder = (ViewHolder) v.getTag();
//
//						noActionHandler.removeCallbacks(noActionThread);
//						if (viewHolder.editTextQuantity != null) {
//							if (gridViewstationery.isEnabled()) {
//								viewHolder.editTextQuantity.setFocusable(true);
//								viewHolder.editTextQuantity
//										.setFocusableInTouchMode(true);
//							} else {
//								viewHolder.editTextQuantity.setFocusable(false);
//								viewHolder.editTextQuantity
//										.setFocusableInTouchMode(false);
//
//								// utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
//								// MainConstants.kConstantMsgInStationeryItemSendRequest,
//								// Toast.LENGTH_LONG, Gravity.TOP).show();
//							}
//
//						}
//
//						return false;
//
//					}
//
//				});
//				/**
//				 * Modified by gokul based on selection and deselection of the
//				 * stationary item the count is increased and color gets changed
//				 *
//				 *
//				 * **/
//				view.stationeryLayout.setTag(view);
//				view.stationeryLayout
//						.setOnClickListener(listenerStationeryItemSelection);
//				//
//				view.relativeLayoutDecreaseCount.setTag(view);
//
//				view.relativeLayoutDecreaseCount
//						.setOnClickListener(new View.OnClickListener() {
//							@Override
//							public void onClick(View view) {
//								// Log.d("button","enable"+gridViewstationery.isEnabled());
//								if (gridViewstationery.isEnabled()) {
//									noActionHandler
//											.removeCallbacks(noActionThread);
//									hideKeyboard(view);
//									ViewHolder viewHolder = (ViewHolder) view
//											.getTag();
//									if ((viewHolder.editTextQuantity != null)
//											&& (viewHolder.editTextQuantity
//													.getText().toString() != "")
//											&& (viewHolder.editTextQuantity
//													.getText().toString()
//													.length() > 0)
//											&& (Integer
//													.parseInt(viewHolder.editTextQuantity
//															.getText()
//															.toString()) > 0)) {
//
//										int count = Integer
//												.parseInt(viewHolder.editTextQuantity
//														.getText().toString());
//
//										if ((count >= 1) && (selectedStationeryListFinal.size() > 0)) {
//											removeSelectedItemInHashMap(viewHolder.isSelected);
//											count = count - 1;
//										}
//										if (count >= 1) {
//											viewHolder.textStationeryName
//													.setTextColor(Color.WHITE);
//											viewHolder.editTextQuantity
//													.setTextColor(Color.WHITE);
//											viewHolder.stationeryLayout
//													.setBackgroundColor(Color
//															.rgb(25, 138, 206));
//
//										} else {
//											viewHolder.textStationeryName
//													.setTextColor(Color.rgb(86,
//															86, 86));
//											viewHolder.editTextQuantity
//													.setTextColor(Color.rgb(86,
//															86, 86));
//											viewHolder.stationeryLayout
//													.setBackgroundColor(Color.WHITE);
//
//										}
//										// Log.d("count1", "value" + count);
//										addSelectedItemInHashMap(
//												viewHolder.isSelected,
//												viewHolder.stationery_id, count);
//										viewHolder.editTextQuantity.setText(""
//												+ count);
//										viewHolder.editTextQuantity
//												.setFocusable(true);
//										viewHolder.editTextQuantity
//												.requestFocus();
//
//										viewHolder.editTextQuantity
//												.setSelection(viewHolder.editTextQuantity
//														.getText().toString()
//														.length());
//
//									} else {
//										viewHolder.textStationeryName
//												.setTextColor(Color.rgb(86, 86,
//														86));
//										viewHolder.editTextQuantity
//												.setTextColor(Color.rgb(86, 86,
//														86));
//										viewHolder.stationeryLayout
//												.setBackgroundColor(Color.WHITE);
//
//									}
//								} else {
//									// utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
//									// MainConstants.kConstantMsgInStationeryItemSendRequest,
//									// Toast.LENGTH_LONG, Gravity.TOP).show();
//								}
//							}
//						});
//
//				// attach the TextWatcher listener to the EditText
//				view.editTextQuantity.addTextChangedListener(new MyTextWatcher(
//						view));
//				view.editTextQuantity.setTag(view);
//
//				view.relativeLayoutIncreaseCount.setTag(view);
//				view.relativeLayoutIncreaseCount
//						.setOnClickListener(new View.OnClickListener() {
//							@Override
//							public void onClick(View view) {
//								if (gridViewstationery.isEnabled()) {
//									noActionHandler
//											.removeCallbacks(noActionThread);
//									hideKeyboard(view);
//									ViewHolder viewHolder = (ViewHolder) view
//											.getTag();
//									if ((viewHolder.editTextQuantity != null)
//											&& (viewHolder.editTextQuantity
//													.getText().toString() != "")
//											&& (viewHolder.editTextQuantity
//													.getText().toString()
//													.length() > 0)
//											&& (Integer
//													.parseInt(viewHolder.editTextQuantity
//															.getText()
//															.toString()) >= 0)) {
//
//										int count = Integer
//												.parseInt(viewHolder.editTextQuantity
//														.getText().toString());
//
//										if ((count >= 1) && (selectedStationeryListFinal.size() > 0)) {
//											removeSelectedItemInHashMap(viewHolder.isSelected);
//										}
//										if (count < MainConstants.stationeryItemMaximumCount) {
//											count = count + 1;
//										} else {
//
//											// utility.UsedCustomToast
//											// .makeCustomToast(
//											// getApplicationContext(),
//											// MainConstants.toastMsgForMaximumCount,
//											// Toast.LENGTH_LONG,
//											// Gravity.TOP).show();
//											// utility.UsedCustomToast
//											// .makeCustomToast(
//											// getApplicationContext(),
//											// MainConstants.toastMsgForMaximumCount,
//											// Toast.LENGTH_LONG,
//											// Gravity.TOP).show();
//										}
//										// Log.d("count2", "value" + count);
//										addSelectedItemInHashMap(
//												viewHolder.isSelected,
//												viewHolder.stationery_id, count);
//										viewHolder.editTextQuantity.setText(""
//												+ count);
//										viewHolder.editTextQuantity
//												.setFocusable(true);
//										viewHolder.editTextQuantity
//												.requestFocus();
//
//										viewHolder.editTextQuantity
//												.setSelection(viewHolder.editTextQuantity
//														.getText().toString()
//														.length());
//										viewHolder.textStationeryName
//												.setTextColor(Color.WHITE);
//										viewHolder.editTextQuantity
//												.setTextColor(Color.WHITE);
//										viewHolder.stationeryLayout
//												.setBackgroundColor(Color.rgb(
//														25, 138, 206));
//
//									} else {
//
//										viewHolder.editTextQuantity
//												.setFocusable(true);
//										viewHolder.editTextQuantity
//												.requestFocus();
//										viewHolder.editTextQuantity
//												.setText("1");
//										viewHolder.editTextQuantity
//												.setSelection(viewHolder.editTextQuantity
//														.getText().toString()
//														.length());
//										viewHolder.textStationeryName
//												.setTextColor(Color.WHITE);
//										viewHolder.editTextQuantity
//												.setTextColor(Color.WHITE);
//										viewHolder.stationeryLayout
//												.setBackgroundColor(Color.rgb(
//														25, 138, 206));
//
//									}
//								} else {
//									// utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
//									// MainConstants.kConstantMsgInStationeryItemSendRequest,
//									// Toast.LENGTH_LONG, Gravity.TOP).show();
//								}
//							}
//						});
//			}
//			return convertView;
//		}
//
//		@Override
//		public int getCount() {
//
//			// ////////////Log.d("getcount", "count" +
//			// this.listOfStationery.size());
//			return this.listOfStationery.size();
//		}
//
//		@Override
//		public Object getItem(int position) {
//
//			return position;
//		}
//
//		@Override
//		public long getItemId(int position) {
//
//			return 0;
//		}
//
//		@Override
//		public boolean areAllItemsEnabled() {
//			return false;
//		}
//
//		@Override
//		public boolean isEnabled(int position) {
//			// Return true for clickable, false for not
//			return false;
//		}
//
//	}

	/*
	 * get quantity and store it into hashmap
	 */
	private class MyTextWatcher implements TextWatcher {
		String previousQuantity = "";
		private ViewHolder view;

		private MyTextWatcher(ViewHolder view) {
			this.view = view;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			if ((view.editTextQuantity != null)
					&& (view.editTextQuantity.getText().toString() != "")
					&& (view.editTextQuantity.getText().toString().length() > 0)) {
				previousQuantity = view.editTextQuantity.getText().toString();
			} else {
				previousQuantity = "";
			}

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			// ////////////Log.d("view on text", "position" + view.isSelected);
			if ((view.editTextQuantity != null)) {

				if ((view.editTextQuantity.getText().toString() != "")
						&& (view.editTextQuantity.getText().toString().length() > 0)) {
					int quantity = Integer.parseInt(view.editTextQuantity
							.getText().toString());
					if (quantity == 0) {
						view.editTextQuantity.setText("");
					}
					if (noActionHandler != null) {
						noActionHandler.removeCallbacks(noActionThread);
						noActionHandler.postDelayed(noActionThread,
								MainConstants.afterTypingNoFaceTime);
					}
				}
			}

		}

		@Override
		public void afterTextChanged(Editable s) {

			if ((view.editTextQuantity != null)) {

				if ((view.editTextQuantity.getText().toString() != "")
						&& (view.editTextQuantity.getText().toString().length() > 0)) {
					int count = Integer.parseInt(view.editTextQuantity
							.getText().toString());

					view.textStationeryName.setTextColor(Color.WHITE);
					view.editTextQuantity.setTextColor(Color.WHITE);
					view.stationeryLayout.setBackgroundColor(Color.rgb(25, 138,
							206));

					if (count > MainConstants.stationeryItemMaximumCount) {

						// utility.UsedCustomToast.makeCustomToast(
						// getApplicationContext(),
						// MainConstants.toastMsgForMaximumCount,
						// Toast.LENGTH_LONG, Gravity.TOP).show();
						// utility.UsedCustomToast.makeCustomToast(
						// getApplicationContext(),
						// MainConstants.toastMsgForMaximumCount,
						// Toast.LENGTH_LONG, Gravity.TOP).show();
						view.editTextQuantity.setText(previousQuantity);
						view.editTextQuantity
								.setSelection(view.editTextQuantity.getText()
										.toString().length());

					} else {

						// if ((count >= 1) && (selectedStationeryListFinal.size() > 0)) {
						removeSelectedItemInHashMap(view.isSelected);

						// }
						// Log.d("count3", "value" + count);
						addSelectedItemInHashMap(view.isSelected,
								view.stationery_id, count);
						view.editTextQuantity
								.setSelection(view.editTextQuantity.getText()
										.toString().length());
					}
				} else {
					view.textStationeryName.setTextColor(Color.rgb(86, 86, 86));
					view.editTextQuantity.setTextColor(Color.rgb(86, 86, 86));
					view.stationeryLayout.setBackgroundColor(Color.WHITE);

					removeSelectedItemInHashMap(view.isSelected);

				}
			}
		}
	}

	/*
	 * 
	 * 
	 * Created by karthick Created on 09/03/2015
	 */
	@Override
	public void onBackPressed() {
		// super.onBackPressed();
		if (map.size() > 0) {
			intializeDialogBoxForHomePage();
			showHomeDialogBox();
		} else {
			getHomePage();
		}
	}

	/*
	 * private void getHomePage(Context context) { if (selectedStationeryListFinal.size() > 0) {
	 * customDialog = new CustomDialog();
	 * customDialog.viewClearStationerySelectionDialog(context, listenerClear);
	 * } else { finish(); overridePendingTransition(android.R.anim.fade_in,
	 * android.R.anim.fade_out);
	 * 
	 * }
	 * 
	 * }
	 */

	/*
	 * set internal log and insert to local sqlite
	 * 
	 * 
	 * @created On 20150910
	 * 
	 * @author karthick
	 */
	/*
	 * public void setInternalLogs(Context context, int errorCode, String
	 * errorMessage) {
	 * //SingletonErrorMessage.getInstance().setErrorMessage(errorMessage);
	 * NetworkConnection networkConnection = new NetworkConnection();
	 * InternalAppLogs internalAppLogs = new InternalAppLogs();
	 * internalAppLogs.setErrorCode(errorCode);
	 * internalAppLogs.setErrorMessage(errorMessage);
	 * internalAppLogs.setLogDate(new Date().getTime());
	 * internalAppLogs.setWifiSignalStrength(networkConnection
	 * .getCurrentWifiSignalStrength(context));
	 * DbHelper.getInstance(context).insertLogs(internalAppLogs); }
	 */

	@Override
	protected void onStop() {
		super.onStop();

	}

	private void hideKeyboard(View view) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	@Override
	public void onScrollChanged(CustomScrollView scrollView, int x, int y,
			int oldx, int oldy) {
		// Log.d("scroll", "changed");

	}

	/*
	 * Created by jayapriya On 01/12/2014 Intialize the dialogbox for home page
	 */
	private void intializeDialogBoxForHomePage() {
		dialogGetHomeMessage = new Dialog(this);
		dialogGetHomeMessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogGetHomeMessage
				.setContentView(R.layout.dialog_box_confirm_message);
		// dialog.(MessageConstant.dialogTitle);
		dialogGetHomeMessage.setCanceledOnTouchOutside(true);
		dialogGetHomeMessage.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialogGetHomeMessage.setOnDismissListener(setOnDismissListener);
		textViewDialogHomeMessage = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message);
		textViewHomeMessageConfirm = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_confirm);
		textViewHomeMessageCancel = (TextView) dialogGetHomeMessage
				.findViewById(R.id.text_view_error_message_cancel);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		params.height = 200;
		textViewDialogHomeMessage.setLayoutParams(params);
		textViewDialogHomeMessage.setTextColor(Color.rgb(102, 102, 102));
		textViewDialogHomeMessage.setText(StringConstants.kConstantLostData);
		textViewDialogHomeMessage.setTypeface(fontButton);
		textViewHomeMessageCancel.setTypeface(fontButton);
		textViewHomeMessageConfirm.setTypeface(fontButton);
		textViewHomeMessageConfirm
				.setText(StringConstants.kConstantDialogConfirm);
		textViewHomeMessageCancel.setText(StringConstants.kConstantDialogCancel);
		// textViewHomeMessageConfirm.setText(MainConstants.kConstantOka);
		textViewHomeMessageConfirm.setOnClickListener(discardConfirm);
		textViewHomeMessageCancel.setOnClickListener(discardCancel);

	}

	/*
	 * Created by jayapriya On 01/12/2014 Intialize the dialogbox for home page
	 */
	private void initializeErrorDialog() {
		reportDialog = new Dialog(context);
		reportDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		reportDialog.setContentView(R.layout.dialog_report);
		// dialog.(MessageConstant.dialogTitle);
		textViewReportMsg = (TextView) reportDialog
				.findViewById(R.id.textView_report_msg);
		textViewReportFirstMsg = (TextView) reportDialog
				.findViewById(R.id.textView_report_first_msg);
		textViewReportErrorCode = (TextView) reportDialog
				.findViewById(R.id.textView_report_error_code);
		progressBar = (ProgressBar) reportDialog
				.findViewById(R.id.progressBar_report);
		reportImage = (ImageView) reportDialog
				.findViewById(R.id.imageView_report);
		tellUs = (Button) reportDialog.findViewById(R.id.button_tellus);
		seperator = (View) reportDialog.findViewById(R.id.firstSeparator);
		relativeLayout = (RelativeLayout) reportDialog
				.findViewById(R.id.relativeLayout_dialog);
		relativeLayout.getLayoutParams().width = (int) (400 * context
				.getApplicationContext().getResources().getDisplayMetrics().density);
		reportImage.setVisibility(View.GONE);
		textViewReportFirstMsg.setVisibility(View.VISIBLE);
		// reportImage.setBackgroundDrawable(new
		// ColorDrawable(android.graphics.Color.TRANSPARENT));
		progressBar.setVisibility(View.GONE);
		reportDialog.setCanceledOnTouchOutside(true);
		reportDialog.setOnKeyListener(null);
		textViewReportErrorCode.setVisibility(View.GONE);
		seperator.setVisibility(View.VISIBLE);
		tellUs.setVisibility(View.VISIBLE);
		// tellUs.setText(MainConstants.tellUs);
		textViewReportFirstMsg.setVisibility(View.INVISIBLE);// .setText(MainConstants.kConstantAtleastOneItemSelected);
		textViewReportMsg
				.setText(StringConstants.kConstantAtleastOneItemSelected);
		textViewReportErrorCode.setVisibility(View.INVISIBLE);// .setText(MainConstants.visitorUploadErrorCodeText+reportStatus);
		textViewReportMsg.setTextColor(Color.rgb(102, 102, 102));
		relativeLayout.setBackgroundColor(Color.rgb(204, 204, 204));
		reportDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		tellUs.setTextColor(Color.rgb(102, 102, 102));
		tellUs.setTextSize(20);
		seperator.setBackgroundColor(Color.rgb(171, 171, 171));
		textViewReportMsg.setTextSize(23);
		textViewReportFirstMsg.setMinHeight(80);
		textViewReportFirstMsg.setGravity(Gravity.TOP);
		tellUs.setTypeface(fontButton);
		textViewReportMsg.setTypeface(fontButton);
		textViewReportFirstMsg.setTypeface(fontButton);
		textViewReportErrorCode.setTypeface(fontButton);
		tellUs.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				reportDialog.dismiss();
			}

		});
	}

	/*
	 * Created by Jayapriya On 02/12/2014 Click the errorMessageCancel to
	 * dismiss the dialogBox
	 */
	private OnClickListener discardCancel = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if ((dialogGetHomeMessage != null)
					&& (dialogGetHomeMessage.isShowing())) {
				dialogGetHomeMessage.dismiss();
			}
		}
	};
	/*
	 * @created On 20160804
	 * 
	 * @author Gokul If the stationary item is selected or deselected in the
	 * layout, based on the selection the color changes of the single item view
	 * and add into the Hashmap will be done
	 */

	/**
	 * 
	 */
	private OnClickListener listenerStationeryItemSelection = new OnClickListener() {
		@Override
		public void onClick(View view) {
			if (IsallowedToClickStationaryItems == true) {
				ViewHolder viewHolder = (ViewHolder) view.getTag();
				if (viewHolder != null) {
					if (noActionHandler != null && noActionThread != null
							&& MainConstants.afterTypingNoFaceTime >0) {
						noActionHandler.removeCallbacks(noActionThread);
						noActionHandler.postDelayed(noActionThread,
								MainConstants.afterTypingNoFaceTime);
					}
					// If not null the remove the item selection from hasahmap
					if (map != null && !map.isEmpty()
							&& map.get(viewHolder.isSelected) != null) {
						removeSelectedItemInHashMap(viewHolder.isSelected);
						if (viewHolder.textStationeryName != null) {
							viewHolder.textStationeryName.setTextColor(Color
									.rgb(86, 86, 86));
						}
						if (viewHolder.stationeryLayout != null) {
							viewHolder.stationeryLayout
									.setBackgroundColor(Color.WHITE);
						}
					}
					// If null add the item into hash selectedStationeryListFinal
					else {
						addSelectedItemInHashMap(viewHolder.isSelected,
								viewHolder.stationery_id, 1);
						if (viewHolder.textStationeryName != null) {

							viewHolder.textStationeryName
									.setTextColor(Color.WHITE);
						}
						if (viewHolder.stationeryLayout != null) {
							viewHolder.stationeryLayout
									.setBackgroundColor(Color.rgb(25, 138, 206));
						}
					}
				}
			}
		}
	};

	/*
	 * Created by Jayapriya On 26/09/2014 Click the errorMessageConfirm to
	 * activate the next processing.
	 */
	private OnClickListener discardConfirm = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if ((dialogGetHomeMessage != null)) {
				dialogGetHomeMessage.dismiss();
			}
			Intent intent = new Intent(getApplicationContext(),
					WelcomeActivity.class);
			startActivity(intent);

			noActionHandler.removeCallbacks(noActionThread);
			finish();
		}
	};

	/*
	 * store selection values into shared preferences
	 */
	/*
	 * private void storeStationeryDetailIntoSharedPreferences(HashMap<Integer,
	 * StationeryEntry> selectedStationeryListFinal){ Set<String> set = new HashSet<String>();
	 * SharedPreferences prefs = getApplicationContext().getSharedPreferences(
	 * MainConstants.sharedPreferences, Context.MODE_PRIVATE);
	 * SharedPreferences.Editor prefsEditor = prefs.edit();
	 * prefsEditor.putStringSet("myStringSet", set); prefsEditor.commit(); }
	 */

	/**
	 * @author jayapriya
	 * 
	 */
	public void onPause() {
		super.onPause();
		// Log.d("onPause", "onPause");
		noActionHandler.removeCallbacks(noActionThread);
	}

	/*
	 * Dialog dismiss Listener for reinitialize grids
	 * 
	 * @created on 20150618
	 * 
	 * @author karthick
	 */
	DialogInterface.OnDismissListener setOnDismissListener = new DialogInterface.OnDismissListener() {
		public void onDismiss(DialogInterface dialog) {
			chooseOption = 0;
			if (noActionHandler != null) {
				noActionHandler.removeCallbacks(noActionThread);
				noActionHandler.postDelayed(noActionThread,
						MainConstants.afterTypingNoFaceTime);
			}
			/*
			 * if (employeeID.length() <= NumberConstants.kConstantZero) {
			 * checkboxNotifyMe.setChecked(false); } if (isNotifyDialogShow) {
			 * isNotifyDialogShow = false; } else {
			 * initializeGridAdapter(false); }
			 */
		}
	};

	private void sendFailureRequest(String msg, int code) {
		String requestParameter = MainConstants.kConstantCreatorAuthTokenAndScope
				// +DataBaseConstants.addStationeryRequestTime
				// +TimeZoneConventer.getDate(currentTime)
				+ DataBaseConstants.addLogCode + code
				+ DataBaseConstants.addLogMsg + msg;
//		sendVolleyStringRequest(
//				(MainConstants.applicationStationeryApp
//						+ MainConstants.kConstantAddStationeryRequestFailureDetail + requestParameter)
//						.replace(" ", "%20"), "RequestFailure");

	}

	/*
	 * background process for upload visitor details
	 */
	public class backgroundProcess extends AsyncTask<String, String, String> {

		String response = MainConstants.kConstantEmpty;
		String requestParameter = MainConstants.kConstantEmpty;
		int responseCode = MainConstants.kConstantMinusOne;

		public backgroundProcess(String requestParameter) {
			this.requestParameter = requestParameter;
		}

		protected void onPreExecute() {
			super.onPreExecute();

		}

		protected String doInBackground(String... f_url) {

			JSONParser jsonParser = new JSONParser();

			URL url;
			try {
				url = new URL(MainConstants.applicationStationeryApp
						+ MainConstants.kConstantAddStationeryRequestRecord);

				JSONObject jsonObject = jsonParser.getJsonArrayInPostMethod(
						url, requestParameter.replace(" ", "%20"));

				if ((jsonObject != null)) {
					response=jsonObject.toString();
					long id = creator.getStationeryRequestId(jsonObject);

					if ((id > 0L)) {
						responseCode=ErrorConstants.stationeryRequestSuccess;
					} else {
						sendFailureRequest(jsonObject.toString(),
								ErrorConstants.stationeryRequestJsonDataError);
						responseCode = ErrorConstants.stationeryRequestJsonDataError;
					}

				}

			} catch (MalformedURLException e) {
				responseCode = ErrorConstants.stationeryRequestURLError;
			} catch (IOException e) {
				responseCode = ErrorConstants.stationeryRequestIOError;
			} catch (JSONException e) {
				responseCode = ErrorConstants.stationeryRequestJSONError;
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String responseParam) {

			if(responseCode==ErrorConstants.stationeryRequestSuccess) {
			utility.UsedCustomToast.makeCustomToast(getApplicationContext(),
					StringConstants.kConstantMsgInStationeryItemUploadSuccess,
					Toast.LENGTH_LONG, Gravity.TOP).show();
			Intent intent = new Intent(getApplicationContext(),
					WelcomeActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			// noActionHandler.removeCallbacks(noActionThread);
			finish();
			} else {
			enableProcess();
			setEnableViews(true);
			IsallowedToClickStationaryItems = true;
			uploadStationeryItemEntryFailure(responseCode);
			setInternalLogs(responseCode,response);
			}
		}
	}

	
	/*
	 * set internal log and insert to local sqlite
	 * 
	 * 
	 * @created On 20150910
	 * 
	 * @author karthick
	 */
	private void setInternalLogs(int errorCode,
			String errorMessage) {

		NetworkConnection networkConnection = new NetworkConnection();
		InternalAppLogs internalAppLogs = new InternalAppLogs();
		internalAppLogs.setErrorCode(errorCode);
		internalAppLogs.setErrorMessage(errorMessage);
		internalAppLogs.setLogDate(new Date().getTime());
		internalAppLogs.setWifiSignalStrength(networkConnection
				.getCurrentWifiSignalStrength(this));
		DbHelper.getInstance(this).insertLogs(internalAppLogs);
	}
}
