package com.zoho.app.frontdesk.android;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageReq;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import appschema.SingletonEmployeeProfile;
import appschema.SingletonMetaData;
import appschema.SingletonVisitorProfile;
import backgroundprocess.GetAllRecords;
import constants.ErrorConstants;
import constants.FontConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import database.dbhelper.DbHelper;
import database.dbschema.EmployeeCheckInOut;
import logs.InternalAppLogs;
import utility.DialogView;
import utility.DialogViewWarning;
import utility.JSONParser;
import utility.NetworkConnection;
import utility.TimeZoneConventer;
import utility.UsedBitmap;
import zohocreator.ForgotIDZohoCreator;


public class EmployeeTemporaryIdActivity extends Activity {

    private ImageView imageViewBack, imageViewEmployeePhoto;
    private TextView textViewUserWelcome,textViewCheckIndifferenceData, textViewCheckInTimeText, textViewCheckInTimeData, textViewCheckIndiffText,
            textViewCheckIndiffData;
    private Button buttonCheckInOut;
    private Typeface typeFaceTitle, typefaceVisitorTempID, fontBlackJerk;
    private DbHelper database;
    private int checkInOutCode = MainConstants.kConstantZero;
    private ArrayList<EmployeeCheckInOut> CheckInCheckOutcode = new ArrayList<EmployeeCheckInOut>();
    private DialogViewWarning dialog;
    private DialogView dialogView;
    private int actionCode = MainConstants.backPress, retryCount = 0;
    private JSONParser jsonParser ;
    private ForgotIDZohoCreator forgotIDZohoCreator;
    private Handler noActionHandler, signValidateHandler, handlerHideDialog, handlerAutoCheckIn;
    private RelativeLayout relativeLayoutEmployeeForgetID;
    private MediaPlayer mPlayer;
    private Bitmap bitmap=null,resized=null;
    private NetworkConnection networkConnection;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remove the title bar.
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // hide the status bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_employee_forget_id_tenkasi);

        getEmployeeImageFromPeople();
//        Log.d("DSK ",""+SingletonEmployeeProfile.getInstance().getEmployeePhoto());
        //assign Objects.
        assignObjects();

    }

    /**
     * Assign Objects.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    public void assignObjects() {
        imageViewEmployeePhoto = (ImageView) findViewById(R.id.image_view_employee_photo);
        imageViewBack = (ImageView) findViewById(R.id.imageview_back);
        buttonCheckInOut = (Button) findViewById(R.id.check_in);
        textViewUserWelcome = (TextView) findViewById(R.id.textview_visitor_contact_title);
//        textViewCheckInTimeText = (TextView) findViewById(R.id.text_view_checked_in_time_text);
//        textViewCheckInTimeData = (TextView) findViewById(R.id.text_view_checked_in_time_data);
//        textViewCheckIndiffText = (TextView) findViewById(R.id.text_view_checked_in_diff_text);
        textViewCheckIndiffData = (TextView) findViewById(R.id.text_view_checked_in_diff_data);
        textViewCheckIndifferenceData = (TextView) findViewById(R.id.text_view_checked_in_difference_data);
        //        relativeLayoutEmployeeForgetID = (RelativeLayout) findViewById(R.id.relativelayout_forget_id);

        database = new DbHelper(this);

//        if (textToSpeechListener != null) {
//            textToSpeech = new TextToSpeech(getApplicationContext(),
//                    textToSpeechListener);
//        }

//        setFont Constants
        setFont();

        handlerHideDialog = new Handler();
        noActionHandler = new Handler();
        noActionHandler.postDelayed(noActionThread, MainConstants.noFaceDragDropTime);

        // OnClick listeners
        if (imageViewBack != null && listenerBack != null) {
            imageViewBack.setOnClickListener(listenerBack);
        }
        if (buttonCheckInOut != null && listenerCheckInOut != null) {
            buttonCheckInOut.setOnClickListener(listenerCheckInOut);
        }

//        setAppBackground();
        loadEmployeeName();
    }

    /**
     * load Employee Name
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void loadEmployeeName()
    {
        //         Assign Employee CheckIn/CheckOut View Based on the Previous Login of Employee.
        if (SingletonEmployeeProfile.getInstance() != null && SingletonEmployeeProfile.getInstance().getEmployeeId() != null
                && !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()) {
            loadCheckInCheckOutView();
            String employeePhoto = MainConstants.kConstantFileDirectory
                    .concat(MainConstants.kConstantFileEmployeeImageFolder)
                    .concat(SingletonEmployeeProfile.getInstance().getEmployeeZuid())
                    .concat(MainConstants.kConstantImageFileFormat);
            if (ApplicationController.getInstance() != null && ApplicationController.getInstance().checkExternalPermission() && (SingletonEmployeeProfile.getInstance().getEmployeePhoto() != null)
                    && (SingletonEmployeeProfile.getInstance().getEmployeePhoto() != MainConstants.kConstantEmpty)
                    && (new File(employeePhoto).exists())) {
                setEmployeeImage(employeePhoto);
            }
            else {
                textViewCheckIndiffData.setVisibility(View.GONE);
                textViewCheckIndifferenceData.setVisibility(View.VISIBLE);
            }
//            Log.d("DSK", "" + employeePhoto);
        }
    }
    /**
     * set Employee Image.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void setEmployeeImage(String path) {
//        Log.d("DSK", "" + path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (path != null && !path.isEmpty()) {
            bitmap = BitmapFactory.decodeFile(path, options);
            resized = UsedBitmap.scaleBitmap(bitmap, 1080, 1120);
//                 Log.d("DSK bitmap","size"+bitmap.getByteCount());
            if(imageViewEmployeePhoto!=null) {
                imageViewEmployeePhoto.setImageBitmap(resized);
            }
        }
    }

    /**
     * set Font Style
     *
     * @author Karthikeyan D S
     * @created on 24Sep2018
     */
    private void setFont() {
        // set font.
        typeFaceTitle = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        typefaceVisitorTempID = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantHKGroteskBold);
        fontBlackJerk = Typeface.createFromAsset(getAssets(),
                FontConstants.fontConstantPoppinsSemiBold);

        textViewUserWelcome.setTypeface(fontBlackJerk);
//        textViewCheckInTimeText.setTypeface(typeFaceTitle);
//        textViewCheckInTimeData.setTypeface(typeFaceTitle);
        textViewCheckIndiffData.setTypeface(typeFaceTitle);
        buttonCheckInOut.setTypeface(typeFaceTitle);
    }

    /**
     * Modified Set background color
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    private void setAppBackground() {
        if (SingletonMetaData.getInstance() != null && SingletonMetaData.getInstance().getThemeCode() > MainConstants.kConstantZero) {
            if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantOne) {
                relativeLayoutEmployeeForgetID.setBackgroundColor(Color.BLACK);
            } else if (SingletonMetaData.getInstance().getThemeCode() == MainConstants.kConstantTwo) {
                GradientDrawable gd = new GradientDrawable(
                        GradientDrawable.Orientation.BOTTOM_TOP,
                        new int[]{0xFFba6fb6, 0xFF8e76c3, 0xFF8478c6, 0xFF8278c7});
                relativeLayoutEmployeeForgetID.setBackground(gd);
            }
        }
    }

    /**
     * Load CheckIn/CheckOut Action
     *
     * @author Karthikeyan D S
     * @created on 20180508
     */
    private void loadCheckInCheckOutView() {
        if (database != null) {
            CheckInCheckOutcode = database.getEmployeeCheckInCheckoutDetails(SingletonEmployeeProfile.getInstance().getEmployeeId());
//            Log.d("DbData", "" + CheckInCheckOutcode.toString() + "" + CheckInCheckOutcode.isEmpty() + "" + CheckInCheckOutcode.size());
            if (CheckInCheckOutcode.size() == MainConstants.kConstantZero || CheckInCheckOutcode.toString().isEmpty() || CheckInCheckOutcode.toString() == null ||
                    (CheckInCheckOutcode.get(MainConstants.kConstantZero).getCheckInTime() > MainConstants.invalidNumber && CheckInCheckOutcode.get(MainConstants.kConstantZero).getCheckOutTime() > MainConstants.invalidNumber)) {
                buttonCheckInOut.setText(StringConstants.kConstantCheckIn);
//                textViewCheckInTimeText.setVisibility(View.GONE);
//                textViewCheckInTimeData.setVisibility(View.GONE);
//                textViewCheckIndiffText.setVisibility(View.GONE);
                textViewCheckIndiffData.setVisibility(View.GONE);
                getEmployeeDetails();
                checkInOutCode = MainConstants.kConstantOne;
            } else if (CheckInCheckOutcode.size() > MainConstants.kConstantZero && CheckInCheckOutcode.get(MainConstants.kConstantZero).getCheckOutTime() == MainConstants.invalidNumber) {
                buttonCheckInOut.setText(StringConstants.kConstantCheckOut);
                if (CheckInCheckOutcode.get(MainConstants.kConstantZero).getCheckInTime() > MainConstants.kConstantZero) {
//                    textViewCheckInTimeText.setVisibility(View.VISIBLE);
//                    textViewCheckInTimeData.setVisibility(View.VISIBLE);
//                    textViewCheckIndiffText.setVisibility(View.GONE);
                    textViewCheckIndiffData.setVisibility(View.VISIBLE);

                    Calendar calendar = Calendar.getInstance();
//
                    String checkedInTimeDiff = TimeZoneConventer.getTimeForCheckedInTime(CheckInCheckOutcode.get(MainConstants.kConstantZero).getCheckInTime(), MainConstants.creatorDateFormatForgetID);
                    String CheckedOutCurrentTimeDiff = TimeZoneConventer.getTimeForCheckedInTime(calendar.getTimeInMillis(), MainConstants.creatorDateFormatForgetID);

//                  Checked In Time and Date
                    String checkedInTime = TimeZoneConventer.getDate(CheckInCheckOutcode.get(MainConstants.kConstantZero).getCheckInTime(), MainConstants.peopleCheckInDateFormat);
//                  Total Time Difference
                    String diff = TimeZoneConventer.TimerFunction(checkedInTimeDiff, CheckedOutCurrentTimeDiff);

//                  textViewCheckInTimeData.setText(checkedInTime);
                    textViewCheckIndiffData.setText(diff);
                    textViewCheckIndifferenceData.setText(diff);
                }
                getEmployeeDetails();
                checkInOutCode = MainConstants.kConstantTwo;
            }
        }
    }

    /**
     * get Employee Details Function.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void getEmployeeDetails() {
        if ((SingletonEmployeeProfile.getInstance().getEmployeeName() != null)
                && (SingletonEmployeeProfile.getInstance().getEmployeeName() != MainConstants.kConstantEmpty)) {
            String[] name = SingletonEmployeeProfile.getInstance()
                    .getEmployeeName().split(" ");
            if (TimeZoneConventer.showHappyGreeting() != MainConstants.kConstantEmpty) {
                textViewUserWelcome.setText(TimeZoneConventer.showHappyGreeting() + name[MainConstants.kConstantZero]
                        + StringConstants.kConstantSelectStationeryMsg);
            } else {
                textViewUserWelcome
                        .setText(StringConstants.kConstantGreetingMorning + name[MainConstants.kConstantZero]
                                + StringConstants.kConstantSelectStationeryMsg);
            }
        } else {
            launchPreviousPage();
        }
    }

    /**
     * Check Device Camera Permission on the Page Call.
     *
     * @author Karthikeyan D S
     * @created on 19/12/17.
     */
    private boolean check_device_permission() {
        if (ContextCompat.checkSelfPermission(EmployeeTemporaryIdActivity.this,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(EmployeeTemporaryIdActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(EmployeeTemporaryIdActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }
    /**
     * Hide dialog
     *
     * @author Karthikeyan D S
     * @created 20170508
     */
    Runnable hideDialogTask = new Runnable() {
        public void run() {
            DialogView.hideDialog();
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };
    /**
     * Goto home page when no action.
     *
     * @author Karthikeyan D S
     * @created on 20170508
     */
    public Runnable noActionThread = new Runnable() {
        public void run() {
            DialogView.hideDialog();
            actionCode = MainConstants.homePress;
            launchHomePage();
        }
    };

    /**
     * Listener to launch previous page.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private View.OnClickListener listenerBack = new View.OnClickListener() {
        public void onClick(View v) {
            actionCode = MainConstants.backPress;
            launchPreviousPage();
            //showDetailDialog();
        }
    };
    /**
     * Listener to launch previous page.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private View.OnClickListener listenerCheckInOut = new View.OnClickListener() {
        public void onClick(View v) {
            if (!check_device_permission()) {
                EnableRuntimePermission();
            } else {
                onCallBackgroundDatabaseSyncActivity();
            }
        }
    };
    /**
     * Handle tab click for goto settings page
     *
     * @author Karthick
     * @created on 20170508
     */
    private View.OnClickListener listenerDialogAction = new View.OnClickListener() {
        public void onClick(View v) {
            launchHomePage();
        }
    };

    /**
     * Runtime Permission for Read or Write External Storage.
     *
     * @author Karthikeyan D S
     * @created on 19/12/17.
     */
    public void EnableRuntimePermission() {
        if (check_device_permission() == false) {
            ActivityCompat.requestPermissions(EmployeeTemporaryIdActivity.this, new String[]{
                    Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, NumberConstants.requestExternalPermissionForStorage);

        } else {
        }
    }

    /**
     * Background Sync Activity to the Data in the Local DB whereas on Click of Confirm Button.
     *
     * @author Karthikeyan D S
     * @created on 10/01/18.
     */
    private void onCallBackgroundDatabaseSyncActivity() {
        InsertDataToCreator insertDataToCreator = new InsertDataToCreator();
        insertDataToCreator.execute();
    }

    /**
     * Intent to launch previous page.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void launchPreviousPage() {
        Intent launchPreviousPage = new Intent(EmployeeTemporaryIdActivity.this, ActivityGetEmployeeIDDetails.class);
        startActivity(launchPreviousPage);
        finish();
    }

    /**
     * Intent to launch home page.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void launchHomePage() {
        Intent launchPreviousPage = new Intent(EmployeeTemporaryIdActivity.this, WelcomeActivity.class);
        startActivity(launchPreviousPage);
        finish();
    }

    /**
     * This function is called, when the activity is started.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    public void onStart() {
        super.onStart();
//        setOldValue();
    }

    /**
     * This function is called, when the activity is stopped.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    public void onStop() {
        super.onStop();
    }

    /**
     * This function is called, when the activity is destroyed.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    public void onDestroy() {
        super.onDestroy();
        deallocateObjects();
    }

    /**
     * This Function is called when back button is pressed.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    public void onBackPressed() {
        actionCode = MainConstants.backPress;
        launchPreviousPage();
    }

    /**
     * This Function is called when check In Function Handled.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private long checkInCode() {
        EmployeeCheckInOut employeeCheckInOut = new EmployeeCheckInOut();
        long sucessCode = MainConstants.kConstantZero;
        long currentCheckInTime = MainConstants.kConstantMinusOne;
        if (SingletonEmployeeProfile.getInstance() != null &&
                SingletonEmployeeProfile.getInstance().getEmployeeId() != null &&
                !SingletonEmployeeProfile.getInstance().getEmployeeId().isEmpty()) {
            if (database != null) {
                employeeCheckInOut.setEmployeeID(SingletonEmployeeProfile.getInstance().getEmployeeId());
                employeeCheckInOut.setEmployeeName(SingletonEmployeeProfile.getInstance().getEmployeeName());
                employeeCheckInOut.setEmployeeEmailID(SingletonEmployeeProfile.getInstance().getEmployeeMailId());

                currentCheckInTime = getCurrentDateTime();
                String dateFormat =  TimeZoneConventer.getDate(currentCheckInTime,
                        MainConstants.creatorDateFormatForgetID);
                long checkInTime = TimeZoneConventer.getDateTimeInMilliSeconds(dateFormat,
                        MainConstants.creatorDateFormatForgetID);

                employeeCheckInOut.setCheckInTimeDate(dateFormat);
                employeeCheckInOut.setCheckInTime(checkInTime);
                employeeCheckInOut.setCheckOutTime(MainConstants.kConstantMinusOne);

                if (SingletonVisitorProfile.getInstance() != null) {
                    String officeLocation = SingletonVisitorProfile.getInstance().getDeviceLocation();
                    if (officeLocation != null && !officeLocation.isEmpty()) {
                        employeeCheckInOut.setOfficeLocation(SingletonVisitorProfile.getInstance().getDeviceLocation());
                    }
                }
                employeeCheckInOut.setIsSynced(MainConstants.kConstantEmpty);

                int getRecordCount = database.getEmployeeCheckInCheckOut();
//                 Log.d("RecordCount",""+getRecordCount);
                employeeCheckInOut.setTemporaryID(getRecordCount + MainConstants.kConstantEmpty);

//                Sync Employee CheckIn/CheckOut Details to Creator.
                if(forgotIDZohoCreator==null) {
                    forgotIDZohoCreator = new ForgotIDZohoCreator();
                }
                String code = forgotIDZohoCreator.SyncCheckInCheckOutDetailsCreator(EmployeeTemporaryIdActivity.this, employeeCheckInOut);
//                 Log.d("DSK Message","MessagesucessCode"+sucessCode);
                if (code != null && !code.isEmpty()) {
                    if(jsonParser==null) {
                        jsonParser = new JSONParser();
                    }
                    ArrayList<EmployeeCheckInOut> employeeCheckInOutsDetailsList = jsonParser.parseEmployeeCheckINDetails(code);
//                      Log.d("Message", "employeeCheckInOutsDetailsList " + employeeCheckInOutsDetailsList.size());
                    if (database != null && employeeCheckInOutsDetailsList != null && !employeeCheckInOutsDetailsList.isEmpty()
                            && employeeCheckInOutsDetailsList.size() > 0) {
//                            Log.d("Message", "employeeCheckInOutsDetailsList " + employeeCheckInOutsDetailsList.size());
                        sucessCode = database.insertCheckInCheckOutDetails(employeeCheckInOut);
                        checkInOutCode = MainConstants.kConstantOne;
                    } else {
                        SingletonEmployeeProfile.getInstance().setEmployeeForgetIdErrorCode(ErrorConstants.checkInCheckOutRequestJSONError);
                        sucessCode = NumberConstants.kConstantMinusOne;
                    }

                } else {
                    sucessCode = MainConstants.kConstantZero;
                    return sucessCode;
                }
            }
        }
        return sucessCode;
    }

    /**
     * This Function is called when check Out Function Handled.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private int checkOutCode() {
        int updateDataCode = MainConstants.kConstantZero;
        JSONParser jsonParser = new JSONParser();
        long currentCheckOutTime = MainConstants.kConstantMinusOne;
        if (CheckInCheckOutcode.size() > MainConstants.kConstantZero) {
            currentCheckOutTime = getCurrentDateTime();
            ArrayList<EmployeeCheckInOut> checkInCheckOut = new ArrayList<EmployeeCheckInOut>();
            if (CheckInCheckOutcode.get(MainConstants.kConstantZero).getRecordId() > MainConstants.kConstantZero) {
                if (database != null) {
                    checkInCheckOut = database.getEmployeeCheckInCheckOutDetails(CheckInCheckOutcode.get(MainConstants.kConstantZero).getRecordId());
                    if (checkInCheckOut.size() > 0 && checkInCheckOut != null && !checkInCheckOut.isEmpty()) {

                        String dateFormat =  TimeZoneConventer.getDate(currentCheckOutTime,
                                MainConstants.creatorDateFormatForgetID);
                        long checkOutTime = TimeZoneConventer.getDateTimeInMilliSeconds(dateFormat,
                                MainConstants.creatorDateFormatForgetID);

                        checkInCheckOut.get(0).setCheckOutTime(checkOutTime);
                        checkInCheckOut.get(0).setCheckOutTimeDate(dateFormat);
//                        Log.d("DSK Message", "checkOutTime " + checkOutTime +"checkOutTime " +dateFormat);
                        if (forgotIDZohoCreator == null) {
                            forgotIDZohoCreator = new ForgotIDZohoCreator();
                        }
                        String code = forgotIDZohoCreator.SyncCheckInCheckOutDetailsCreator(EmployeeTemporaryIdActivity.this, checkInCheckOut.get(0));
//                    Log.d("Message", "code " + code);
                        if (code != null && !code.isEmpty()) {
                            ArrayList<EmployeeCheckInOut> employeeCheckInOutsDetailsList = jsonParser.parseEmployeeCheckINDetails(code);
//                            Log.d("Message", "employeeCheckInOutsDetailsList " + employeeCheckInOutsDetailsList.size());
                            if (database != null && employeeCheckInOutsDetailsList != null && !employeeCheckInOutsDetailsList.isEmpty() &&
                                    employeeCheckInOutsDetailsList.get(0).getIsSynced() != null &&
                                    !employeeCheckInOutsDetailsList.get(0).getIsSynced().isEmpty() &&
                                    employeeCheckInOutsDetailsList.size() > 0) {
                                updateDataCode = database.updateEmployeeDetails(CheckInCheckOutcode.get(MainConstants.kConstantZero).getRecordId(), currentCheckOutTime, checkInCheckOut.get(0).getCheckOutTimeDate());
//                                Log.d("DSK ", " updateDataCode " + updateDataCode);
                                checkInOutCode = MainConstants.kConstantTwo;
                            } else {
                                SingletonEmployeeProfile.getInstance().setEmployeeForgetIdErrorCode(ErrorConstants.checkInCheckOutRequestJSONError);
                                updateDataCode = -1;
                            }
                        }
                    } else {
                        updateDataCode = -1;
                        return updateDataCode;
                    }
                }
            }
        }
        return updateDataCode;
    }

    /**
     * Get Current Time and Date
     *
     * @author Karthikeyan D S
     * @created on 07052018
     */
    private long getCurrentDateTime() {
        long currentDateTime = MainConstants.kConstantMinusOne;
        Calendar calendar = Calendar.getInstance();
        currentDateTime = calendar.getTimeInMillis();
        return currentDateTime;
    }

    /**
     * Play error music
     *
     * @author Karthick
     * @created on 20170522
     */
    private void errorMusicPlay(int rawId) {
        if (mPlayer != null) {
            mPlayer.release();
        }
        mPlayer = MediaPlayer.create(this, rawId);
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
            }
        });
    }

    /**
     * Handle Upload Success Response
     *
     * @author Karthikeyan D S
     * @created on 20180416
     */
    private void handleSuccessResponse(int checkedInOutCode) {
        if (checkedInOutCode == 1) {
            DialogView.showReportDialog(EmployeeTemporaryIdActivity.this,
                    MainConstants.badgeDialog,
                    ErrorConstants.ForgetIdAllCheckInSuccessCode);
        } else if (checkedInOutCode == 2) {
            DialogView.showReportDialog(EmployeeTemporaryIdActivity.this,
                    MainConstants.badgeDialog,
                    ErrorConstants.ForgetIdAllCheckOutSuccessCode);
        }
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            handlerHideDialog.postDelayed(hideDialogTask, 2000);
        }
    }

    /**
     * Handle Upload Error Response
     *
     * @author Karthikeyan D S
     * @created on 20180416
     */
    private void handleErrorResponse() {
        DialogView.showReportDialog(EmployeeTemporaryIdActivity.this, MainConstants.uploadCheckInOutFailure,
                MainConstants.reportCheckInCheckOutFailureError);
        errorMusicPlay(R.raw.bonk);
        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler.postDelayed(noActionThread, MainConstants.noActionTime);
        }
    }

    /**
     * Deallocate Objects in the activity.
     *
     * @author Karthikeyan D S
     * @created on 20180405
     */
    private void deallocateObjects() {
        typeFaceTitle = null;
        typefaceVisitorTempID = null;
        dialog = null;
        imageViewBack = null;
        buttonCheckInOut = null;
        textViewUserWelcome = null;
        textViewCheckInTimeText = null;
        textViewCheckInTimeData = null;
        textViewCheckIndiffText = null;
        textViewCheckIndiffData = null;
        textViewCheckIndifferenceData=null;
        relativeLayoutEmployeeForgetID = null;
        imageViewEmployeePhoto = null;
        checkInOutCode = -1;

        if (noActionHandler != null) {
            noActionHandler.removeCallbacks(noActionThread);
            noActionHandler = null;
        }
        if (handlerHideDialog != null) {
            handlerHideDialog.removeCallbacks(hideDialogTask);
            handlerHideDialog = null;
        }
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        if(bitmap!=null) {
            bitmap.recycle();
        }
        if(resized!=null)
        {
            resized.recycle();
        }
        if(networkConnection!=null)
        {
            networkConnection = null;
        }
        DialogView.setNullDialog();
    }

    /**
     * Insert the Forget ID Details to creator
     *
     * @author Karthikeyan D S
     * @created on 20180416
     */
    public class InsertDataToCreator extends AsyncTask<String, String, String> {

        int checkInCode = MainConstants.kConstantZero;

        protected void onPreExecute() {
            super.onPreExecute();
//            setEnableFields(false);
//            isBackPressEnabled = false;
            if (dialog == null) {
                if (checkInOutCode == MainConstants.kConstantOne) {
                    DialogView.showReportDialog(EmployeeTemporaryIdActivity.this, MainConstants.checkInProcessing,
                            MainConstants.forgotIDUploadCode);
//                    DialogView.setReportImage(EmployeeTemporaryIdActivity.this, MainConstants.printBadgeDialog ,
//                            MainConstants.checkInProcessing);
                } else if (checkInOutCode == MainConstants.kConstantTwo) {
                    DialogView.showReportDialog(EmployeeTemporaryIdActivity.this, MainConstants.checkOutProcessing, MainConstants.forgotIDUploadCode);
//                    DialogView.setReportImage(EmployeeTemporaryIdActivity.this,MainConstants.printBadgeDialog ,
//                            MainConstants.checkOutProcessing);
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            if (checkInOutCode == MainConstants.kConstantOne) {
                if (checkInCode() > MainConstants.kConstantZero) {
                    checkInCode = MainConstants.kConstantOne;
                } else {
                    checkInCode = MainConstants.kConstantMinusOne;
                }
            } else if (checkInOutCode == MainConstants.kConstantTwo) {
                if (checkOutCode() > MainConstants.kConstantZero) {
                    checkInCode = MainConstants.kConstantTwo;
                } else {
                    checkInCode = MainConstants.kConstantMinusOne;
                }
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {

        }

        protected void onPostExecute(String message) {
            if (checkInCode == 1 || checkInCode == 2) {
                handleSuccessResponse(checkInCode);
            } else if (checkInCode == -1) {
                handleErrorResponse();
            }
        }

    }

    /**
     * Download employee image through volley
     *
     * @author Karthikeyan D S
     * @created 03DEC2018
     */
    private void sendVolleyRequestImage(String employeeZUID, String photoUrl) {
        ImageReq request = new ImageReq(photoUrl,
                new Response.Listener<Bitmap>() {
                    public void onResponse(final Bitmap bitmap, String id) {
//                        Log.d("DSK ","bitmap "+bitmap.getByteCount());
                        if (bitmap!=null)
                        {
                            if(imageViewEmployeePhoto!=null) {
                                resized = UsedBitmap.scaleBitmap(bitmap, 1080, 1120);
                                bitmap.recycle();
                                if(resized!=null) {
                                    imageViewEmployeePhoto.setImageBitmap(resized);
                                }
                            }
                        }
                    }
                }, 0, 0, null, new Response.ErrorListener() {
            public void onErrorResponse(final VolleyError error,
                                        String id) {
               setInternalLogs(
                        getApplicationContext(),
                        ErrorConstants.backgroundProcessDownloadImageVolleyError,
                        error.getLocalizedMessage() + " "+ error.networkResponse.toString() );
            }
        }, employeeZUID);

        ApplicationController.getInstance().getRequestQueue().add(request);

    }
    /**
     * get Employee Image From People.
     *
     * @author Karthikeyan D S
     * @created 04DEC2018
     */
    private void getEmployeeImageFromPeople()
    {
        if (networkConnection == null) {
            networkConnection = new NetworkConnection();
        }
        if (networkConnection.wifiConnected(getApplicationContext())) {
            if (SingletonEmployeeProfile.getInstance() != null &&
                    SingletonEmployeeProfile.getInstance().getEmployeeZuid() != null &&
                    !SingletonEmployeeProfile.getInstance().getEmployeeZuid().isEmpty() &&
                    SingletonEmployeeProfile.getInstance().getEmployeePhoto() != null &&
                    !SingletonEmployeeProfile.getInstance().getEmployeePhoto().isEmpty())
            {
                sendVolleyRequestImage(SingletonEmployeeProfile.getInstance().getEmployeeZuid(),
                        SingletonEmployeeProfile.getInstance().getEmployeePhoto()
                                + MainConstants.kConstantPeopleAuthToken);
            }
        }
    }

    /*
     * set internal log and insert to local sqlite
     *
     *
     * @created On 20150910
     *
     * @author karthick
     */
    public void setInternalLogs(Context context, int errorCode,
                                String errorMessage) {
        // SingletonErrorMessage.getInstance().setErrorMessage(errorMessage);
        MainConstants.alreadyStartedBackgroundProcess =NumberConstants.kConstantBackgroundStopped;
        NetworkConnection networkConnection = new NetworkConnection();
        InternalAppLogs internalAppLogs = new InternalAppLogs();
        internalAppLogs.setErrorCode(errorCode);
        internalAppLogs.setErrorMessage(errorMessage);
        internalAppLogs.setLogDate(new Date().getTime());
        internalAppLogs.setWifiSignalStrength(networkConnection
                .getCurrentWifiSignalStrength(context));
        DbHelper.getInstance(context).insertLogs(internalAppLogs);
    }
}
