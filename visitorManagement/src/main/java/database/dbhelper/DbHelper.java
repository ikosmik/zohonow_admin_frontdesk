/*
 * created by jayapriya on 19/08/14
 */
package database.dbhelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import appschema.SingletonEmployeeProfile;

import appschema.SingletonVisitorProfile;
import appschema.VisitorDetailsDatabase;

import constants.ErrorConstants;
import constants.NumberConstants;
import database.dbschema.EmployeeCheckInOut;
import database.dbschema.EmployeeFrequency;
import database.dbschema.EmployeeVisitorPreApproval;
import database.dbschema.PurposeOfVisitRecord;
import database.dbschema.VisitRecord;
import database.dbschema.VisitorRecord;
import database.dbschema.VisitorVisitLinkRecord;
import database.dbschema.ZohoEmployeeRecord;
import logs.InternalAppLogs;

import constants.DataBaseConstants;
import constants.MainConstants;
import database.dbschema.Stationery;
import database.dbschema.StationeryEntry;
import appschema.StationeryRequest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteReadOnlyDatabaseException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import appschema.ExistingVisitorDetails;
import utility.NetworkConnection;

public class DbHelper extends SQLiteOpenHelper {

    Context context;
    //   @Created Karthikeyan D S on 29JUN2018
    // Create Visitor PreApproval Data table
    public static final String dbVisitorPreApprovalData = DataBaseConstants.tableNotCreate
            + DataBaseConstants.dbtableNamePreApproval
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbEmployeeRecordId
            + DataBaseConstants.kConstantIdAutoIncrement
            + DataBaseConstants.dbStationeryCreatorId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnNamePreApprovalVisitorName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnNamePreApprovalVisitorCompanyName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnNamePreApprovalDateofVisit
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnNamePreApprovalDateofVisitLong
            + DataBaseConstants.kConstantsPrimaryKeyLong
            + DataBaseConstants.dbColumnNamePreApprovalVisitorPhoneNumber
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnNamePreApprovalisVisitorActive
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbColumnNamePreApprovalEmailId
            + DataBaseConstants.kConstantsTextWithCloseBrace;
    // Create table visitorVisitLink table
    public static final String dbVisitorVisitLink = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableVisitorVisitLink
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.visitorId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.visitId
            + DataBaseConstants.kConstantsInteger
            + " FOREIGN KEY ("
            + DataBaseConstants.visitorId
            + ") REFERENCES "
            + DataBaseConstants.tableNameVisitorRecordInOffMode
            + " ("
            + DataBaseConstants.visitorId
            + DataBaseConstants.kConstantsCloseBrace
            + ", FOREIGN KEY ("
            + DataBaseConstants.visitId
            + ") REFERENCES "
            + DataBaseConstants.tableNameVisitRecordInOffMode
            + " ("
            + DataBaseConstants.visitId + "))";
    public static final String dbStationeryList = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableStationeryList
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbStationeryItemCode
            + DataBaseConstants.kConstantIntegerPrimaryKey
            + DataBaseConstants.dbStationeryItemName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryItemLimitPerMonth
            + DataBaseConstants.kConstantsInteger
            // + DataBaseConstants.dbStationeryItemImage
            // + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryCreatorId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryLocationCode
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbDirtyBit
            + DataBaseConstants.kConstantsIntegerWithCloseBrace;
    public static final String dbNewStationeryList = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameNewStationeryList
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.visitorRecordId
            + DataBaseConstants.kConstantIdAutoIncre
            + DataBaseConstants.dbStationeryItemCode
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryItemName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryItemLimitPerMonth
            + DataBaseConstants.kConstantsInteger
            // + DataBaseConstants.dbStationeryItemImage
            // + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryOfficeDeviceLocation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryOfficeParentLocation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryCreatorId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryLocationCode
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbDirtyBit
            + DataBaseConstants.kConstantsIntegerWithCloseBrace;
    public static final String dbStationeryRequest = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableStationeryRequest
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbStationeryRequestId
            + DataBaseConstants.kConstantTextPrimaryKey
            + DataBaseConstants.dbStationeryRequestTime
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbEmployeeId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryCreatorId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbDirtyBit
            + DataBaseConstants.kConstantsIntegerWithCloseBrace;
    public static final String dbStationeryItemEntry = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableStationeryItemEntry
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbStationeryEntryTime
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryRequestId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbStationeryItemCode
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryItemQuantity
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbStationeryCreatorId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbDirtyBit
            + DataBaseConstants.kConstantsInteger
            + " PRIMARY KEY ("
            + DataBaseConstants.dbStationeryEntryTime
            + " , "
            + DataBaseConstants.dbStationeryItemCode
            + " ), "
            + " FOREIGN KEY ("
            + DataBaseConstants.dbStationeryRequestId
            + ") REFERENCES "
            + DataBaseConstants.tableStationeryRequest
            + " ("
            + DataBaseConstants.dbStationeryRequestId
            + DataBaseConstants.kConstantsCloseBrace
            + ", FOREIGN KEY ("
            + DataBaseConstants.dbStationeryItemCode
            + ") REFERENCES "
            + DataBaseConstants.tableStationeryList
            + " ("
            + DataBaseConstants.dbStationeryItemCode
            + DataBaseConstants.kConstantsCloseBrace
            + DataBaseConstants.kConstantsCloseBrace;
    private static final int dbVersion = 14;
    // Create table VisitorRecord
    private static final String dbVisitorRecordCreate = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameVisitorDetail
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.visitorRecordId
            + DataBaseConstants.kConstantTextPrimaryKey
            + DataBaseConstants.visitorName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorAddress
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorMobileNo
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorMailId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorDesignation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorCompany
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorCompanyWebsite
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorFax
            + DataBaseConstants.kConstantsTextWithCloseBrace;
    // Create table VisitorRecordInOffMode
    private static final String dbVisitorRecordCreateInOffMode = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameVisitorRecordInOffMode
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.visitorId
            + DataBaseConstants.kConstantIdAutoIncrement
            + DataBaseConstants.visitorName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorAddress
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorMobileNo
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorMailId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorDesignation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorCompany
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorCompanyWebsite
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorFax
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorBusinessCard
            + DataBaseConstants.kConstantsTextWithCloseBrace;
    //    	Create Employee CheckIn/CheckOut Table.
    private static final String dbEmployeeForgetIDCreate = DataBaseConstants.tableNotCreate +
            DataBaseConstants.tableNameEmployeeForgetID + DataBaseConstants.kConstantsOpenBrace +
            DataBaseConstants.dbTableRowId + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DataBaseConstants.dbEmployeeId + DataBaseConstants.kConstantsText + DataBaseConstants.dbEmployeeName +
            DataBaseConstants.kConstantsText + DataBaseConstants.dbEmployeeEmailId + DataBaseConstants.kConstantsText +
            DataBaseConstants.dbEmployeeCheckInTime + DataBaseConstants.kConstantsInteger +
            DataBaseConstants.dbEmployeeCheckOutTime + DataBaseConstants.kConstantsInteger +
            DataBaseConstants.dbEmployeeCheckInTimeDate + DataBaseConstants.kConstantsText +
            DataBaseConstants.dbEmployeeCheckOutTimeDate + DataBaseConstants.kConstantsText +
            DataBaseConstants.dbEmployeeOfficeLocation + DataBaseConstants.kConstantsText +
            DataBaseConstants.dbEmployeeTemporaryID + DataBaseConstants.kConstantsText +
            DataBaseConstants.dbIsSyncWithCreator + DataBaseConstants.kConstantsIntegerWithCloseBrace;

    /*
    @modified       :   arumugalingam on : 19-9-2019
    #issue VM-14    :   Some employees are missing from the list in Visitor App
    @modfication    :   ZUID is changed from primary key to normal key, and employee id is changed to primary key
    @reason         :   some of the employees in zoho people have -1 as zuid ,so these records are not stored when zuid
                        is set to primary key and it stores only one record with zuid -1 so i've changed it to normal key
    * */
    //    Create Employee table
    private static final String dbEmployeeCreate = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameEmployee
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbEmployeeZuid
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeId
            + DataBaseConstants.kConstantIntegerPrimaryKey
            + DataBaseConstants.dbEmployeeRecordId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeePhoneNumber
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeEmailId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeStatus
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeSeatingLocation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeExtension
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeePhoto
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeVisitFrequency
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbEmployeeDept
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbIsDataCenterEmployee
            + DataBaseConstants.kConstantsIntegerWithoutComma
            + DataBaseConstants.kConstantsNotNull
            + DataBaseConstants.kConstantsDefault
            + MainConstants.kConstantZero
            + DataBaseConstants.kConstantsCloseBrace;

    // Create Employee table
    private static final String dbEmployeeCreateNew = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameEmployeeNewTable
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbEmployeeZuid
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbEmployeeRecordId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeePhoneNumber
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeEmailId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeStatus
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeSeatingLocation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeExtension
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeePhoto
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbEmployeeVisitFrequency
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbIsDataCenterEmployee
            + DataBaseConstants.kConstantsIntegerWithoutComma
            + DataBaseConstants.kConstantsNotNull
            + DataBaseConstants.kConstantsDefault
            + MainConstants.kConstantZero
            + DataBaseConstants.kConstantsCamma
            + DataBaseConstants.dbEmployeeDept
            + DataBaseConstants.kConstantsTextWithCloseBrace;
    // Create purpose of Visit table
    private static final String dbPurposeOfVisitCreate = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNamePurposeOfVisit
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.purpose
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitFrequency
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.location
            + DataBaseConstants.kConstantsIntegerWithCloseBrace;
    // Create visitor details for existing
    private static final String dbExistingVisitorCreate = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableExistingVisitor
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.colnExistLastVisitDate
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.colnExistVisitorName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.colnExistCreatorCompanyName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.colnExistVisitorContact
            + DataBaseConstants.kConstantTextPrimaryKey
            + DataBaseConstants.colnExistVisitorEmployee
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.colnExistVisitorPurpose
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.colnExistCreatorVisitID
            + DataBaseConstants.kConstantsTextWithCloseBrace;
    // Create table VisitRecordInOffMode
    private static final String dbVisitRecordInOffMode = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameVisitRecordInOffMode
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.visitId
            + DataBaseConstants.kConstantIdAutoIncrement
            + DataBaseConstants.visitPurpose
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dateOfVisit
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.officeLocation
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitEmployeeEmailId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.codeNo
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.visitorPhoto
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorQrCode
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitorSign
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitEmployeeName
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitEmployeeId
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.officeLocationNumber
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.GMTDiffMinutes
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.visitEmployeePhoneNo
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.visitIdInCreator
            + DataBaseConstants.kConstantsTextWithCloseBrace;
    private static DbHelper localeDb;
    // ZohoEmployeeRecord employeeRecord;
    public String[] allColumns = {DataBaseConstants.name,
            DataBaseConstants.country, DataBaseConstants.frequency,
            DataBaseConstants.position, DataBaseConstants.name_country_position};
    // create logs table
    String dbLogs = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameLogs
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbLogDate + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbLogErrorMessage
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbLogErrorCode
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbLogWifiSingnalStrength
            + DataBaseConstants.kConstantsIntegerWithCloseBrace;
    String dbVisitorDetails = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameVisitor
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbVisitorVisitDate
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbVisitorJsonId
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbVisitorJsonData
            + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbVisitorPhotoUrl
            + DataBaseConstants.kConstantsBlob
            + DataBaseConstants.dbVisitorSignatureUrl
            + DataBaseConstants.kConstantsBlob
            + DataBaseConstants.dbVisitorPhotoUploaded
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbVisitorSignatureUploaded
            + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbVisitorPurpose
            + DataBaseConstants.kConstantsTextWithCloseBrace;

    //	String dbUnComplete = DataBaseConstants.tableNotCreate
//			+ DataBaseConstants.tableNameUnComplete
//			+ DataBaseConstants.kConstantsOpenBrace
//			+ DataBaseConstants.dbJsonDataDate
//			+ DataBaseConstants.kConstantsInteger
//			+ DataBaseConstants.dbJsonDataId
//			+ DataBaseConstants.kConstantsText
//			+ DataBaseConstants.dbVisitorPurpose
//			+ DataBaseConstants.kConstantsTextWithCloseBrace;

    private static final String dbKeyList = DataBaseConstants.tableNotCreate
            + DataBaseConstants.tableNameKeyList
            + DataBaseConstants.kConstantsOpenBrace
            + DataBaseConstants.dbColumnKeyID + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnKeyType + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnKeyPhoto + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnSecurityDetails + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnVehicleNumber + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnRoomDetails + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnFurnitureType + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnOfficeLocation + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnLocationInsideOffice + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnStatus + DataBaseConstants.kConstantsText
            + DataBaseConstants.dbColumnSpareKeyCount + DataBaseConstants.kConstantsInteger
            + DataBaseConstants.dbColumnIsActive + " BOOLEAN,"
            + DataBaseConstants.dbColumnDescription + "Text)";

//    public static final String dbKeyAllocation = DataBaseConstants.tableNotCreate
//            + DataBaseConstants.tableNameKeyAllocation
//            + DataBaseConstants.kConstantsOpenBrace
//            + DataBaseConstants.dbColumnAllKeyID + DataBaseConstants.kConstantsText
////            + " FOREIGN KEY ("
////            + DataBaseConstants.dbColumnAllKeyID
////            + ") REFERENCES "
////            + DataBaseConstants.tableNameKeyList
////            + " (" + DataBaseConstants.dbColumnKeyID + ") "
//            + DataBaseConstants.dbColumnAllOfficeLocation + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllLocationInsideOffice + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllAllocatedTime + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllDeAllocatedTime + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllReturnedBy + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllPersonThumbPhoto + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllPersonName + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllPersonPhoto + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllEmail + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllMobileNo + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllPersonSignature + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllReturnedPersonName + DataBaseConstants.kConstantsText
//            + DataBaseConstants.dbColumnAllReturnedPersonContact + "Text)";

    private SQLiteDatabase database;

    /**
     * Create constructor
     *
     * @param context get the context object
     * @author jayapriya
     * @created 19/08/2014
     */
    public DbHelper(Context context) {
        // super(context,MainConstants.kConstantFileDirectory+DataBaseConstants.dbName,
        // null, dbVersion);
        super(context, DataBaseConstants.dbName, null, dbVersion);

        localeDb = this;
    }

    /**
     * Create constructor
     *
     * @param context   get the context object
     * @param dbName    get the database name
     * @param factory   get cursorfactory
     * @param dbVersion speficy the database verision
     * @author jayapriya
     * @created 19/08/2014
     */
    public DbHelper(Context context, String dbName, CursorFactory factory,
                    int dbVersion) {

        super(context, dbName, factory, dbVersion);

    }

    /**
     * Create constructor with
     *
     * @param context
     * @return localeDb
     * @author jayapriya
     * @created 19/08/2014
     */
    public static DbHelper getInstance(Context context) {
        if (localeDb == null) {
            localeDb = new DbHelper(context, DataBaseConstants.dbName, null,
                    dbVersion);
        }
        return localeDb;
    }

    /**
     * create the table to store visitor details in local database
     *
     * @author jayapriya
     * @created 19/08/2014
     * @Modified Karthikeyan D S on 29JUN2018
     */
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(dbEmployeeCreate);
        database.execSQL(dbPurposeOfVisitCreate);
//		database.execSQL(dbVisitorRecordCreate);
//		database.execSQL(dbVisitorRecordCreateInOffMode);
//		database.execSQL(dbVisitRecordInOffMode);
//		database.execSQL(dbVisitorVisitLink);
        database.execSQL(dbStationeryList);

//        Removed by Karthikeyan D S for adding new Stationery List.
//        database.execSQL(dbStationeryRequest);

        database.execSQL(dbStationeryItemEntry);
        database.execSQL(dbLogs);
//		database.execSQL(dbUnComplete);
        database.execSQL(dbVisitorDetails);
        database.execSQL(dbExistingVisitorCreate);

//        Added By Karthikeyan D S
        database.execSQL(dbVisitorPreApprovalData);

//        Forget Id for "Tenkasi" New Table Created with defined Fields.
        database.execSQL(dbEmployeeForgetIDCreate);
//        New Stationery List Table Created with define fields.
        database.execSQL(dbNewStationeryList);

        database.execSQL(dbStationeryRequest);
//        database.execSQL(dbKeyList);
//        database.execSQL("create table test (NAME TEXT,AGE INTEGER)");

    }

    /**
     * Upgrade the new version
     *
     * @author jayapriya
     * @created 19/08/2014
     * @Modified Karthikeyan D S
     */
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {

        // database.execSQL(DataBaseConstants.tableIsCreate
        // + DataBaseConstants.tableNameVisitor);
        /*
         * database.execSQL(DataBaseConstants.tableIsCreate +
         * DataBaseConstants.tableNameEmployee);
         * database.execSQL(DataBaseConstants.tableIsCreate +
         * DataBaseConstants.tableNamePurposeOfVisit);
         * database.execSQL(DataBaseConstants.tableIsCreate +
         * DataBaseConstants.tableNameVisitorDetail);
         */
        if (oldVersion == 6) {
            database.execSQL(DataBaseConstants.dropTable + DataBaseConstants.tableExistingVisitor);
        }
        if (oldVersion < 9) {
            database.execSQL("ALTER TABLE " + DataBaseConstants.tableStationeryList + " ADD COLUMN " + DataBaseConstants.dbStationeryLocationCode + " INTEGER DEFAULT 0;");
        }
        if (oldVersion < 10) {
            database.execSQL("ALTER TABLE " + DataBaseConstants.tableNameEmployee + " ADD COLUMN " + DataBaseConstants.dbIsDataCenterEmployee + " INTEGER DEFAULT 0;");
        }
//        For Stationery Quantity Below Changes Made
        if (oldVersion < 11) {
//            database.execSQL("ALTER TABLE " + DataBaseConstants.tableNameNewStationeryList + " ADD COLUMN " + DataBaseConstants.visitorRecordId + " INTEGER PRIMARY KEY;");
            database.execSQL(" Drop Table NewStationeryList ");
            database.execSQL(" CREATE TABLE if not exists NewStationeryList (ID Integer PRIMARY KEY AUTOINCREMENT , Code INTEGER , Name Text,LimitPerMonth INTEGER,DeviceLocation Text," +
                    "ParentLocation Text, CreatorId Integer,LocationCode Integer,DirtyBit Integer);");
        }
//        for Data Center below changes made in table
        if (oldVersion < 12) {
            database.execSQL("ALTER TABLE " + DataBaseConstants.tableExistingVisitor + " ADD COLUMN " + DataBaseConstants.colnExistCreatorCompanyName + " Text;");
        }
//        drop PreApproval Flow old table which is not and create new Table as per schema discussed.
        if (oldVersion < 13) {
            database.execSQL("Drop Table " + DataBaseConstants.dbtableNamePreApprovalMultiPersonDetail);
            database.execSQL("Drop Table " + DataBaseConstants.dbtableNamePreApproval);
            database.execSQL(DataBaseConstants.tableNotCreate
                    + DataBaseConstants.dbtableNamePreApproval
                    + DataBaseConstants.kConstantsOpenBrace
                    + DataBaseConstants.dbEmployeeRecordId
                    + DataBaseConstants.kConstantIdAutoIncrement
                    + DataBaseConstants.dbStationeryCreatorId
                    + DataBaseConstants.kConstantsText
                    + DataBaseConstants.dbColumnNamePreApprovalVisitorName
                    + DataBaseConstants.kConstantsText
                    + DataBaseConstants.dbColumnNamePreApprovalVisitorCompanyName
                    + DataBaseConstants.kConstantsText
                    + DataBaseConstants.dbColumnNamePreApprovalDateofVisit
                    + DataBaseConstants.kConstantsText
                    + DataBaseConstants.dbColumnNamePreApprovalDateofVisitLong
                    + DataBaseConstants.kConstantsPrimaryKeyLong
                    + DataBaseConstants.dbColumnNamePreApprovalVisitorPhoneNumber
                    + DataBaseConstants.kConstantsText
                    + DataBaseConstants.dbColumnNamePreApprovalisVisitorActive
                    + DataBaseConstants.kConstantsInteger
                    + DataBaseConstants.dbColumnNamePreApprovalEmailId
                    + DataBaseConstants.kConstantsTextWithCloseBrace);
        }
        if (oldVersion < 14) {
            database.execSQL("Alter Table " + DataBaseConstants.tableNameEmployeeForgetID + " ADD COLUMN " + DataBaseConstants.dbEmployeeCheckInTimeDate + " TEXT;");
            database.execSQL("Alter Table " + DataBaseConstants.tableNameEmployeeForgetID + " ADD COLUMN " + DataBaseConstants.dbEmployeeCheckOutTimeDate + " TEXT;");
        }
        onCreate(database);
    }

    /**
     * Open the SQlite database
     *
     * @author jayapriya
     * @created 19/08/2014
     */

    public void open() throws SQLException {
        //database.execSQL(dbVisitorRecordCreate);
        database.execSQL(dbEmployeeCreate);
        database.execSQL(dbPurposeOfVisitCreate);
        // Log.d("dBHelper", "open()tocreatetable");
        database = this.getWritableDatabase();
    }

    /**
     * create employee new table
     *
     * @author jayapriya
     * @created on 15/6/16
     */
    public void createNewEmployeeTable() {
        database = this.getWritableDatabase();
        database.execSQL(dbEmployeeCreateNew);
    }

    /**
     * drop employee new table
     *
     * @author jayapriya
     * @created on 15/6/16
     */
    public void dropNewEmployeeTable() {
        // Log.d("response","drop new table");
        database = this.getWritableDatabase();
        database.execSQL(DataBaseConstants.dropTable
                + DataBaseConstants.tableNameEmployeeNewTable);
    }

    /**
     * drop employee old table
     *
     * @author jayapriya
     * @created on 15/6/16
     */
    public void dropOldEmployeeTable() {
        // Log.d("response","drop new table");
        database = this.getWritableDatabase();
        database.execSQL(DataBaseConstants.dropTable
                + DataBaseConstants.tableNameEmployee);
    }

    /**
     *
     *
     */
    public void renameNewEmployeeTable() {
        // Log.d("rename ","name ");
        database = this.getWritableDatabase();
        database.execSQL("ALTER TABLE "
                + DataBaseConstants.tableNameEmployeeNewTable + " RENAME TO "
                + DataBaseConstants.tableNameEmployee);
        getEmployeeList();
    }

    /*
     * rename table
     */

    /**
     * get the employeeDetailWithMailId
     *
     * @return ZohoEmployeeRecord
     * @author jayapriya
     * @created 08/10/2014
     */
    /*
     * public ZohoEmployeeRecord getEmployeeDetailsWithMailId(String name) {
     *
     * if (name != null) {
     *
     * String selectQuery = DataBaseConstants.kConstantsSelectQuery +
     * DataBaseConstants.tableNameEmployee + DataBaseConstants.kConstantsWhere +
     * DataBaseConstants.dbEmployeeName +
     * DataBaseConstants.kConstantsEqualWithSingleQuote + name +
     * DataBaseConstants.kConstantsSingleQuote; //+
     * DataBaseConstants.kConstantsLimit //+ MainConstants.kConstantOne;
     *
     * this.database = this.getReadableDatabase();
     *
     * if (database != null) { Cursor cursor = database.rawQuery(selectQuery,
     * null); if (cursor != null) { cursor.moveToFirst(); while
     * (!cursor.isAfterLast()) { employeeRecord = cursorToEmployee(cursor);
     * cursor.moveToNext(); } cursor.close(); }
     *
     * } } return employeeRecord;
     *
     * }
     *
     *
     * /* Delete existing table
     *
     * @author jayapriya
     *
     * @created 19/08/2014
     */
    public void deleteVisitor() {

        this.database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameVisitorDetail;
        database.execSQL(query);

    }

    /*
     * Delete existing table
     *
     * @author jayapriya
     *
     * @created 22/03/2016
     */
    public void deleteAllStationery() {

        this.database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableStationeryList;
        database.execSQL(query);

    }

    /**
     * Delete existing table NewStationeryList
     *
     * @author Karthikeyan D S
     * @created 25/05/2018
     */
    public void deleteAllNewStationery() {

        this.database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameNewStationeryList;
        database.execSQL(query);

    }

    /*
     * Delete existing table
     *
     * @author jayapriya
     *
     * @created 22/03/2016
     */
    public void deleteAllStationeryEntryItemList() {

        this.database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableStationeryItemEntry;
        database.execSQL(query);

    }

    /*
     * Delete existing table
     *
     * @author jayapriya
     *
     * @created 22/03/2016
     */
    public void deleteAllStationeryRequests() {

        database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableStationeryRequest;
        database.execSQL(query);

    }

    /*
     * Delete all existing visitor
     *
     * @author Karthick
     *
     * @created 20170705
     */
    public void deleteAllExistVisitor() {

        database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableExistingVisitor;
        database.execSQL(query);

    }

    public void deleteAllKey() {

        database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameKeyList;
        database.execSQL(query);

    }

    public void deleteAllKeyAllocation() {

        database = this.getWritableDatabase();
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameKeyAllocation;
        database.execSQL(query);

    }

    /**
     * Insert employeeDetails into the local database.
     *
     * @author jayapriya
     * @created 29/09/2014
     */
    public void insertEmployeeDetails(ArrayList<ZohoEmployeeRecord> employee) {
        database = this.getWritableDatabase();
        // Log.d("insert", "employee" + employee.size());
        for (int i = MainConstants.kConstantZero; i < employee.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.dbEmployeeZuid,
                    employee.get(i).getEmployeeZuid());
            values.put(DataBaseConstants.dbEmployeeId,
                    employee.get(i).getEmployeeId());
            values.put(DataBaseConstants.dbEmployeeRecordId,
                    employee.get(i).getEmployeeRecordId());
            values.put(DataBaseConstants.dbEmployeeName,
                    employee.get(i).getEmployeeName());
            values.put(DataBaseConstants.dbEmployeePhoneNumber,
                    employee.get(i).getEmployeeMobileNumber());
            values.put(DataBaseConstants.dbEmployeeEmailId,
                    employee.get(i).getEmployeeEmailId());
            values.put(DataBaseConstants.dbEmployeeExtension,
                    employee.get(i).getEmployeeExtentionNumber());
            values.put(DataBaseConstants.dbEmployeePhoto,
                    employee.get(i).getEmployeePhoto());
            values.put(DataBaseConstants.dbEmployeeSeatingLocation,
                    employee.get(i).getEmployeeSeatingLocation());
            values.put(DataBaseConstants.dbEmployeeStatus,
                    employee.get(i).getEmployeeStatus());
            values.put(DataBaseConstants.dbEmployeeVisitFrequency,
                    MainConstants.kConstantZero);
            values.put(DataBaseConstants.dbEmployeeDept, employee.get(i)
                    .getEmployeeDept());
            // //Log.d("insert", "employee" + values);
            try {
                database.insertWithOnConflict(
                        DataBaseConstants.tableNameEmployeeNewTable, null, values,
                        SQLiteDatabase.CONFLICT_REPLACE);
            } catch (SQLiteDatabaseLockedException dbLockedException) {
                setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Insert Employee Table : "+dbLockedException.getLocalizedMessage());
            }

        }

//		database.close();

    }

    /**
     * Get the employee mailID depends on employee ID
     *
     * @return ZohoEmployeeRecord object
     * Created karthick Created 20150627
     */
    public ZohoEmployeeRecord getEmployeeDetails(String employeeZUID) {

        ZohoEmployeeRecord ZohoEmployeeRecord = null;
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee + " WHERE "
                    + DataBaseConstants.dbEmployeeZuid + " LIKE '"
                    + employeeZUID + "'";
            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "EmployeeDatabase  " + selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                ZohoEmployeeRecord = cursorToEmployee(cursor);
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLException e) {
            // Log.d("window", "SQLiteException");
            ZohoEmployeeRecord = null;
        }
        return ZohoEmployeeRecord;
    }

    /**
     * Get the employee mailID depends on employee ID
     *
     * @return ZohoEmployeeRecord object Created karthick Created 20150627
     */
    public ZohoEmployeeRecord getEmployee(String employeeID) {

        ZohoEmployeeRecord ZohoEmployeeRecord = null;
        try {

            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee + " WHERE "
                    + DataBaseConstants.dbEmployeeId + " LIKE '" + employeeID
                    + "'";
            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "EmployeeDatabase  " + selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                ZohoEmployeeRecord = cursorToEmployee(cursor);
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException");
            ZohoEmployeeRecord = null;
        }
        return ZohoEmployeeRecord;
    }

    /**
     * Get the employee mailID depends on employee ID
     *
     * @return ZohoEmployeeRecord object Created karthick Created 20150627
     */
    public ZohoEmployeeRecord searchEmployee(String contact) {

        ZohoEmployeeRecord ZohoEmployeeRecord = null;
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee + " WHERE "
                    + DataBaseConstants.dbEmployeeEmailId + " LIKE '" + contact
                    + "' limit 1";
            Cursor cursor = database.rawQuery(selectQuery, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                ZohoEmployeeRecord = cursorToEmployee(cursor);
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLiteException e) {
            ZohoEmployeeRecord = null;
        }
        return ZohoEmployeeRecord;
    }

    /*
     * Delete employee details into the local database
     *
     * @author jayapriya
     *
     * @created 29/09/2014
     */
    public void deleteEmployee() {

        database = this.getWritableDatabase();
        // //Log.d("dbhelper", "deleteemployee");
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameEmployee;
        database.execSQL(query);
//		database.close();

    }

    /**
     * Get employeeList
     *
     * @return ArrayList of zohoemployeerecord.
     * @author jayapriya
     * @created 29/09/2014
     */
    public ArrayList<ZohoEmployeeRecord> getEmployeeList() {
        ArrayList<ZohoEmployeeRecord> listOfEmployees = new ArrayList<ZohoEmployeeRecord>();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee;
            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "EmployeeDatabase  " + selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ZohoEmployeeRecord employeeRecord = cursorToEmployee(cursor);

                listOfEmployees.add(employeeRecord);
                cursor.moveToNext();
            }
            cursor.close();
            // database.close();
        } catch (SQLiteException e1) {
//            Log.d("DSK ",""+e1.getLocalizedMessage());
        }
        return listOfEmployees;
    }

    /**
     * Get employeeList
     *
     * @return ArrayList of zohoemployeerecord.
     * @author jayapriya
     * @created 29/09/2014
     */
    public ArrayList<ZohoEmployeeRecord> getNewTableEmployeeList() {
        ArrayList<ZohoEmployeeRecord> listOfEmployees = new ArrayList<ZohoEmployeeRecord>();
        database = this.getReadableDatabase();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameEmployeeNewTable;
        Cursor cursor = database.rawQuery(selectQuery, null);

        // Log.d("window", " new EmployeeDatabase  " + selectQuery);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ZohoEmployeeRecord employeeRecord = cursorToEmployee(cursor);

            listOfEmployees.add(employeeRecord);
            cursor.moveToNext();
        }
        cursor.close();
        // database.close();
        return listOfEmployees;
    }

    /**
     * Get the employee details depends on employee name Based on Employee ID(Tenkasi) .
     *
     * @return array list of employeeRecords
     * @author Karthikeyan D S
     * @created 04/04/2018
     */

    public ArrayList<ZohoEmployeeRecord> getEmployeeListWithNameBasedID(String name, String employeeFilterID) {
        ArrayList<ZohoEmployeeRecord> listOfEmployees = new ArrayList<ZohoEmployeeRecord>();

        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee
                    + " WHERE ( Name LIKE '%" + name + "%' OR PhoneNo LIKE '%" + name + "%'" + " ) AND EmployeeID LIKE 'ZT-%' ORDER BY "
                    + DataBaseConstants.dbEmployeeVisitFrequency + " DESC";
//			Log.d("Employee",""+selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ZohoEmployeeRecord employeeRecord = cursorToEmployee(cursor);
                // employeeRecord.getEmployeeVisitFrequency());
                listOfEmployees.add(employeeRecord);
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLiteException e) {
//			Log.d("window", "SQLiteException");
        }
        return listOfEmployees;
    }

    /**
     * Get the employee details depends on employee name Based on EmployeeEmailID  .
     *
     * @return array list of employeeRecords
     * @author Karthikeyan D S
     * @created 27/05/2018
     */
    public ArrayList<ZohoEmployeeRecord> getEmployeeListWithNameBasedEmailID(String name, ArrayList<String> employeeFilterEmailID) {
        ArrayList<ZohoEmployeeRecord> listOfEmployees = new ArrayList<ZohoEmployeeRecord>();
        String employeeFilterEmailIDs = employeeFilterEmailID.toString();
//        Log.d("DSK ",""+employeeFilterEmailID.toString());
        employeeFilterEmailIDs = employeeFilterEmailIDs.replace("[", "(");
        employeeFilterEmailIDs = employeeFilterEmailIDs.replace("]", ")");
//        Log.d("DSK Employee ",""+employeeFilterEmailID);
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee
                    + " WHERE ( Name LIKE '%" + name + "%' OR PhoneNo LIKE '%" + name + "%'" + " ) AND EmailId in " + employeeFilterEmailIDs;
//			Log.d("DSKselectQuery ",""+selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
//            Log.d("DSKCursorCount ",""+cursor.getCount());
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ZohoEmployeeRecord employeeRecord = cursorToEmployee(cursor);
                // employeeRecord.getEmployeeVisitFrequency());
                listOfEmployees.add(employeeRecord);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (SQLiteException e) {
//			Log.d("DSK", "SQLiteException" + e.getLocalizedMessage());
        }

        return listOfEmployees;
    }

    /**
     *
     * @author Karthikeyan D S
     * @Created on 23OCT2018
     * @return employee Record Count
     */
    public int getEmployeeRecordCount()
    {
        int employeeCount = NumberConstants.kConstantZero;
        try
        {
            database= this.getReadableDatabase();
            String selectQuery = "Select Count(*) from Employee";
            Cursor cursor = database.rawQuery(selectQuery, null);
            employeeCount = cursor.getCount();
            cursor.close();
        }
        catch(SQLiteDatabaseLockedException dbLockedException)
        {
        }
        return employeeCount;
    }
    /**
     * Get the employee details depends on employee name
     *
     * @return array list of employeeRecords
     * @author jayapriya
     * @created 29/09/2014
     */
    public ArrayList<ZohoEmployeeRecord> getEmployeeListWithName(String name) {
        ArrayList<ZohoEmployeeRecord> listOfEmployees = new ArrayList<ZohoEmployeeRecord>();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee
                    + " WHERE Name LIKE '%" + name + "%' OR PhoneNo LIKE '%" + name + "%'" + " ORDER BY "
                    + DataBaseConstants.dbEmployeeVisitFrequency + " DESC";
            Cursor cursor = database.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ZohoEmployeeRecord employeeRecord = cursorToEmployee(cursor);
                // employeeRecord.getEmployeeVisitFrequency());
                /*
                @author : arumugalingam    date: 13/08/2019
                @subject : here filtered test name from zoho people employee records*/
                if(!employeeRecord.getEmployeeId().toUpperCase().contains("SS") && !employeeRecord.getEmployeeId().toUpperCase().contains("SECURITY"))
                {
                    //here adding the filtered employees to array list that will bedisplayed to app user

                    listOfEmployees.add(employeeRecord);

//                    Log.e("record"," : ********************* "+employeeRecord.getEmployeeName());
//                    Log.e("record"," : ********************* "+employeeRecord.getEmployeeId());
                }
                else
                    {
//                        Log.e("others"," : ********************* "+employeeRecord.getEmployeeName());
//                        Log.e("others"," : ********************* "+employeeRecord.getEmployeeId());

                    }
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException");
        }

        return listOfEmployees;
    }

    /**
     * Get the employee details depends on employee name
     *
     * @return array list of employeeRecords
     * @author jayapriya
     * @created 29/09/2014
     */
    public ArrayList<ZohoEmployeeRecord> getEmployeeListWithNameWithoutFrequency(
            String name) {
        ArrayList<ZohoEmployeeRecord> listOfEmployees = new ArrayList<ZohoEmployeeRecord>();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameEmployee
                    + " WHERE Name LIKE '%" + name + "%'";// +" ORDER BY "+DataBaseConstants.dbEmployeeVisitFrequency
            // + " DESC";
            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "EmployeeDatabase  " + selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ZohoEmployeeRecord employeeRecord = cursorToEmployee(cursor);
                // Log.d("window", "freq:  " +
                // employeeRecord.getEmployeeVisitFrequency());
                listOfEmployees.add(employeeRecord);
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException");
        }
        return listOfEmployees;
    }

    /**
     * Get the employee details depends on range
     *
     * @author karthick
     * @created 28/05/2015
     */
    public ArrayList<String> getEmployeeListRange(long offset, int limit,
                                                  boolean isHRDepartment) {

        // Log.d("window", offset+"Name:"+limit);
        ArrayList<String> listOfEmployeesID = new ArrayList<String>();

        try {
            String selectQuery;
            if (isHRDepartment) {
                selectQuery = "SELECT " + DataBaseConstants.dbEmployeeId
                        + " FROM " + DataBaseConstants.tableNameEmployee
                        + "WHERE " + DataBaseConstants.dbEmployeeDept
                        + " = 'HR' ORDER BY "
                        + DataBaseConstants.dbEmployeeName + " ASC limit "
                        + offset + "," + limit;
            } else {
                selectQuery = "SELECT " + DataBaseConstants.dbEmployeeId
                        + " FROM " + DataBaseConstants.tableNameEmployee
                        + " ORDER BY " + DataBaseConstants.dbEmployeeName
                        + " ASC limit " + offset + "," + limit;
            }
            database = this.getReadableDatabase();

            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "EmployeeDatabase  " + cursor.getCount());
            if (cursor.moveToFirst()) {
                do {

                    listOfEmployeesID.add(cursor
                            .getString(MainConstants.kConstantZero));
                    // Log.d("window", "EmployeeDatabase  " +
                    // cursor.getString(MainConstants.kConstantZero));
                } while (cursor.moveToNext());

                // database.close();
            }
            cursor.close();
        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException");
        }
        return listOfEmployeesID;
    }

    /**
     * Update employee frequency with employee ID
     *
     * @author karthick
     * @created april 2015
     */
    public void updateEmployeeVisitFrequency(EmployeeFrequency employeeFrequency) {

        try {
//			 Log.d("employee frequency ",
//			 ""+employeeFrequency.getEmployeeID());
            database = this.getWritableDatabase();
            // for(int i=0;i<employeeFrequencyList.size();i++) {
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.dbEmployeeVisitFrequency,
                    employeeFrequency.getVisitFrequency());
            // database.update(DataBaseConstants.tableNameEmployee, values,
            // DataBaseConstants.dbEmployeeId+" =?",new
            // String[]{employeeFrequency.getEmployeeID()});
            database.execSQL("UPDATE " + DataBaseConstants.tableNameEmployee
                    + " SET " + DataBaseConstants.dbEmployeeVisitFrequency
                    + "=" + DataBaseConstants.dbEmployeeVisitFrequency + "+" + employeeFrequency.getVisitFrequency() + " WHERE "
                    + DataBaseConstants.dbEmployeeId + " =' "
                    + employeeFrequency.getEmployeeID() + " ' ");

            // }
//			database.close();
        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException"+e);
        }
    }

    /**
     * filter the employee list with employee name.
     *
     * @return arrayList of employee records
     * @author jayapriya
     * @created 29/09/2014
     */
    /*
     * public ArrayList<ZohoEmployeeRecord> FilterEmployeeList(String name) {
     * ArrayList<ZohoEmployeeRecord> listOfEmployees = new
     * ArrayList<ZohoEmployeeRecord>(); database = this.getReadableDatabase();
     * String selectQuery = DataBaseConstants.kConstantsSelectQuery +
     * DataBaseConstants.tableNameEmployee + DataBaseConstants.kConstantsWhere +
     * DataBaseConstants.dbEmployeeName +
     * DataBaseConstants.kConstantsLikeStatementWithSingleQuote + name +
     * DataBaseConstants.kConstantsPercentageWithSingleQuote; Cursor cursor =
     * database.rawQuery(selectQuery, null);
     *
     * //Log.d("window", "EmployeeDatabase  " + cursor); cursor.moveToFirst();
     * while (!cursor.isAfterLast()) { ZohoEmployeeRecord employeeRecord =
     * cursorToEmployee(cursor); listOfEmployees.add(employeeRecord);
     * cursor.moveToNext(); } cursor.close(); return listOfEmployees; }
     *
     * /* cursor to filter the employee details
     *
     * @author jayapriya
     *
     * @created 25/02/2015
     */
    /*
     * private ZohoEmployeeRecord cursorToEmployeeFilter(Cursor cursor) {
     * ZohoEmployeeRecord employee = new ZohoEmployeeRecord();
     *
     * employee.setEmployeeName(cursor .getString(MainConstants.kConstantZero));
     * employee.setEmployeeMobileNumber(cursor
     * .getString(MainConstants.kConstantOne)); employee.setEmployeePhoto(cursor
     * .getString(MainConstants.kConstantTwo));
     *
     * return employee; }
     *
     * /* cursor to get the employee details
     *
     * @author jayapriya
     *
     * @created 08/10/2014
     */
    private ZohoEmployeeRecord cursorToEmployee(Cursor cursor) {
        ZohoEmployeeRecord employee = new ZohoEmployeeRecord();

        employee.setEmployeeZuid(cursor.getString(MainConstants.kConstantZero));
        employee.setEmployeeId(cursor.getString(MainConstants.kConstantOne));
        employee.setEmployeeRecordId(cursor
                .getString(MainConstants.kConstantTwo));
        employee.setEmployeeName(cursor.getString(MainConstants.kConstantThree));
        employee.setEmployeeMobileNumber(cursor
                .getString(MainConstants.kConstantFour));
        employee.setEmployeeEmailId(cursor
                .getString(MainConstants.kConstantFive));
        employee.setEmployeeStatus(cursor.getString(MainConstants.kConstantSix));
        employee.setEmployeeSeatingLocation(cursor
                .getString(MainConstants.kConstantSeven));
        employee.setEmployeeExtentionNumber(cursor
                .getString(MainConstants.kConstantEight));
        employee.setEmployeePhoto(cursor.getString(MainConstants.kConstantNine));
        employee.setEmployeeVisitFrequency(cursor
                .getLong(MainConstants.kConstantTen));
        employee.setEmployeeDept(cursor
                .getString(MainConstants.kConstantTwelve));
        if (cursor.getString(MainConstants.kConstantEleven) == "1") {
            employee.setDataCenterEmployee(true);
        } else if (cursor.getString(MainConstants.kConstantEleven) == "0") {
            employee.setDataCenterEmployee(false);
        }
        // Log.d("record", "get" +
        // cursor.getString(MainConstants.kConstantOne));

//
//         Log.d("record", "get" + cursor.getString(MainConstants.kConstantOne)); Log.d("record", "get"
//          + cursor.getString(MainConstants.kConstantTwo)); Log.d("record",
//          "get" + cursor.getString(MainConstants.kConstantThree));
//          Log.d("record", "get" +
//          cursor.getString(MainConstants.kConstantFour)); Log.d("record", "get"
//          + cursor.getString(MainConstants.kConstantFive)); Log.d("record",
//          "get" + cursor.getString(MainConstants.kConstantSix));
//          Log.d("record", "get" +
//          cursor.getString(MainConstants.kConstantSeven)); Log.d("record",
//          "get" + cursor.getString(MainConstants.kConstantTen));

        return employee;
    }

    /**
     * Created by Jayapriya On 29/10/14
     *
     * @param purpose
     */
    public void insertPurposeOfVisit(ArrayList<PurposeOfVisitRecord> purpose) {

        database = this.getWritableDatabase();

        for (int i = MainConstants.kConstantZero; i < purpose.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.purpose, purpose.get(i).getPurpose());
            values.put(DataBaseConstants.visitFrequency,
                    Integer.valueOf(purpose.get(i).getVisitFrequency()));
            values.put(DataBaseConstants.location,
                    Integer.valueOf(purpose.get(i).getLocation()));

            database.insert(DataBaseConstants.tableNamePurposeOfVisit, null,
                    values);

        }
    }

    /**
     * insert the visitorRecord into local database.
     *
     * @param visitorRecordList pass the visitor detail
     * @author jayapriya
     * @created n 02/12/2014
     */
    public void insertVisitorRecord(ArrayList<VisitorRecord> visitorRecordList) {
        database = this.getWritableDatabase();

        for (int i = MainConstants.kConstantZero; i < visitorRecordList.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.visitorRecordId,
                    Long.valueOf(visitorRecordList.get(i).getRecordId()));
            values.put(DataBaseConstants.visitorName, visitorRecordList.get(i)
                    .getName());
            values.put(DataBaseConstants.visitorAddress,
                    visitorRecordList.get(i).getAddress());
            values.put(DataBaseConstants.visitorMobileNo, visitorRecordList
                    .get(i).getMobileNo());
            values.put(DataBaseConstants.visitorMailId, visitorRecordList
                    .get(i).getMailId());
            values.put(DataBaseConstants.visitorDesignation, visitorRecordList
                    .get(i).getDesignation());
            values.put(DataBaseConstants.visitorCompany,
                    visitorRecordList.get(i).getCompany());
            values.put(DataBaseConstants.visitorCompanyWebsite,
                    visitorRecordList.get(i).getCompanyWebsite());
            values.put(DataBaseConstants.visitorFax, visitorRecordList.get(i)
                    .getFax());

            database.insertWithOnConflict(
                    DataBaseConstants.tableNameVisitorDetail, null, values,
                    SQLiteDatabase.CONFLICT_REPLACE);
        }

//		database.close();

    }

    /**
     * Insert un complete Record
     *
     * @param date
     * @param JsonDataId
     *
     * @author Karthick
     * @created on 20161111
     */
//	public void insertUnCompleteRecord(long date, String JsonDataId,String purpose) {
//		database = this.getWritableDatabase();
//		ContentValues values = new ContentValues();
//		values.put(DataBaseConstants.dbJsonDataDate, date);
//		values.put(DataBaseConstants.dbJsonDataId, JsonDataId);
//		v

    /**
     * Insert un complete Record
     *
     * @param visitorDetailsDb
     * @author Karthick
     * @created on 20161114
     */
    public void insertVisitorRecord(VisitorDetailsDatabase visitorDetailsDb) {
        database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.dbVisitorVisitDate,
                visitorDetailsDb.getVisitDate());
        values.put(DataBaseConstants.dbVisitorJsonId,
                visitorDetailsDb.getJsonDataId());
        values.put(DataBaseConstants.dbVisitorPhotoUrl,
                visitorDetailsDb.getPhotoUrl());
        values.put(DataBaseConstants.dbVisitorJsonData,
                visitorDetailsDb.getJsonData());

        values.put(DataBaseConstants.dbVisitorSignatureUrl,
                visitorDetailsDb.getSignatureUrl());
        values.put(DataBaseConstants.dbVisitorPhotoUploaded,
                visitorDetailsDb.getPhotoUploaded());
        values.put(DataBaseConstants.dbVisitorSignatureUploaded,
                visitorDetailsDb.getSignatureUploaded());

        values.put(DataBaseConstants.dbVisitorPurpose,
                visitorDetailsDb.getVisitPurpose());

        database.insert(DataBaseConstants.tableNameVisitor, null, values);
//		database.close();

    }

    /**
     * Update photo sync success update
     *
     * @param visitorDetailsDb
     * @author Karthick
     * @created on 20161114
     */
    public void UpdateVisitorPhotoUploaded(
            VisitorDetailsDatabase visitorDetailsDb) {
        try {

            database = this.getWritableDatabase();
            database.execSQL(DataBaseConstants.kConstantsUpdate
                    + DataBaseConstants.tableNameVisitor
                    + DataBaseConstants.kConstantsSet
                    + DataBaseConstants.dbVisitorPhotoUploaded
                    + DataBaseConstants.kConstantEqual
                    + MainConstants.kConstantPhotoUploaded
                    + DataBaseConstants.kConstantsWhere
                    + DataBaseConstants.dbVisitorJsonId
                    + DataBaseConstants.kConstantsLikeStatementWithSingleQuote
                    + visitorDetailsDb.getJsonDataId()
                    + DataBaseConstants.kConstantsSingleQuote);

        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException"+e);
        }
    }

    /**
     * Update photo sync success update
     *
     * @param visitorDetailsDb
     * @author Karthick
     * @created on 20161114
     */
    public void UpdateVisitorSignUploaded(
            VisitorDetailsDatabase visitorDetailsDb) {

        try {

            database = this.getWritableDatabase();
            database.execSQL(DataBaseConstants.kConstantsUpdate
                    + DataBaseConstants.tableNameVisitor
                    + DataBaseConstants.kConstantsSet
                    + DataBaseConstants.dbVisitorSignatureUploaded
                    + DataBaseConstants.kConstantEqual
                    + MainConstants.kConstantSignatureUploaded
                    + DataBaseConstants.kConstantsWhere
                    + DataBaseConstants.dbVisitorJsonId
                    + DataBaseConstants.kConstantsLikeStatementWithSingleQuote
                    + visitorDetailsDb.getJsonDataId()
                    + DataBaseConstants.kConstantsSingleQuote);

        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException"+e);
        }
    }


    /**
     * Get visitor Json Data
     *
     * @return ArrayList<String>
     * @author Karthick
     * @created on 20161111
     */
    public ArrayList<VisitorDetailsDatabase> getVisitorJsonData() {
        ArrayList<VisitorDetailsDatabase> visitorDetailsDbList = new ArrayList<VisitorDetailsDatabase>();

        database = this.getReadableDatabase();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitor;
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            VisitorDetailsDatabase visitorDetailsDb = readCursorValueVisitor(cursor);

            visitorDetailsDbList.add(visitorDetailsDb);
            cursor.moveToNext();
        }
        cursor.close();
        return visitorDetailsDbList;
    }

    /**
     * Get Un Complete Json Data
     *
     * @return ArrayList<String>
     *
     * @author Karthick
     * @created on 20161111
     */
//	public ArrayList<String> getUnCompletedVisitor() {
//		ArrayList<String> unCompleteJsonDataList = new ArrayList<String>();
//		unCompleteJsonDataList.clear();
//		database = this.getReadableDatabase();
//		String selectQuery = DataBaseConstants.kConstantsSelectQuery
//				+ DataBaseConstants.tableNameUnComplete;
//		Cursor cursor = database.rawQuery(selectQuery, null);
//		cursor.moveToFirst();
//		while (!cursor.isAfterLast()) {
//			unCompleteJsonDataList.add(cursor.getString(1));
//			cursor.moveToNext();
//		}
//		cursor.close();
//		return unCompleteJsonDataList;
//	}

    /**
     * Get visitor Photo Not Sync
     *
     * @return ArrayList<String>
     * @author Karthick
     * @created on 20161114
     */
    public ArrayList<VisitorDetailsDatabase> getVisitorPhotoNotSync() {
        ArrayList<VisitorDetailsDatabase> visitorDetailsDbList = new ArrayList<VisitorDetailsDatabase>();

        database = this.getReadableDatabase();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitor
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.dbVisitorPhotoUploaded
                + DataBaseConstants.kConstantEqual
                + MainConstants.kConstantPhotoNotUploaded;
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            VisitorDetailsDatabase visitorDetailsDb = readCursorValueVisitor(cursor);

            visitorDetailsDbList.add(visitorDetailsDb);
            cursor.moveToNext();
        }
        cursor.close();
        return visitorDetailsDbList;
    }

    /**
     * Get visitor Sign Not Sync
     *
     * @return ArrayList<String>
     * @author Karthick
     * @created on 20161114
     */
    public ArrayList<VisitorDetailsDatabase> getVisitorSignNotSync() {
        ArrayList<VisitorDetailsDatabase> visitorDetailsDbList = new ArrayList<VisitorDetailsDatabase>();

        database = this.getReadableDatabase();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitor
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.dbVisitorSignatureUploaded
                + DataBaseConstants.kConstantEqual
                + MainConstants.kConstantSignatureNotUploaded;
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            VisitorDetailsDatabase visitorDetailsDb = readCursorValueVisitor(cursor);

            visitorDetailsDbList.add(visitorDetailsDb);
            cursor.moveToNext();
        }
        cursor.close();
        return visitorDetailsDbList;
    }

    /**
     * delete Un Complete Json Data
     *
     * @param jsonDataId
     *
     * @author Karthick
     * @created on 20161111
     */
//	public void deleteUnCompleteJsonData(String jsonDataId) {
//		database = this.getWritableDatabase();
//		String query = constants.DataBaseConstants.ConstantsDeleteQuery
//				+ DataBaseConstants.tableNameUnComplete
//				+ DataBaseConstants.kConstantsWhere
//				+ DataBaseConstants.dbJsonDataId
//				+ DataBaseConstants.kConstantsLikeStatementWithSingleQuote
//				+ jsonDataId + DataBaseConstants.kConstantsSingleQuote;
//		database.execSQL(query);
////		database.close();
//	}

    /**
     * delete Un Complete Json Data
     *
     * @param jsonDataId
     * @author Karthick
     * @created on 20161114
     */
    public void deleteVisitorData(String jsonDataId) {
        database = this.getWritableDatabase();
        String query = constants.DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameVisitor
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.dbVisitorJsonId
                + DataBaseConstants.kConstantsLikeStatementWithSingleQuote
                + jsonDataId + DataBaseConstants.kConstantsSingleQuote;
        database.execSQL(query);
//		database.close();
    }

    /**
     * delete Un Complete Json Data
     *
     * @author Karthick
     * @created on 20161114
     */
    public void deleteVisitorOldData() {
        try {
            Calendar date = Calendar.getInstance();
            date.add(Calendar.DATE, MainConstants.oldVisitDataDeleteDay);

            database = this.getWritableDatabase();
            String query = constants.DataBaseConstants.ConstantsDeleteQuery
                    + DataBaseConstants.tableNameVisitor
                    + DataBaseConstants.kConstantsWhere
                    + DataBaseConstants.dbVisitorVisitDate
                    + DataBaseConstants.kConstantLessthan
                    + date.getTimeInMillis();
            database.execSQL(query);
        } catch (SQLiteException e) {

        }
        //database.close();
    }

    /*
     * If wifi is not connected then insert the visitorRecord in the local
     * database.
     *
     * @author jayapriya
     *
     * @created 02/12/2014
     */
    public void insertVisitorRecordInOffMode() {

        database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.visitorName, SingletonVisitorProfile
                .getInstance().getVisitorName());
        values.put(DataBaseConstants.visitorAddress, SingletonVisitorProfile
                .getInstance().getVisitorLocation());
        values.put(DataBaseConstants.visitorMobileNo, SingletonVisitorProfile
                .getInstance().getPhoneNo());
        values.put(DataBaseConstants.visitorMailId, SingletonVisitorProfile
                .getInstance().getEmail());
        values.put(DataBaseConstants.visitorDesignation,
                SingletonVisitorProfile.getInstance().getDesignation());
        values.put(DataBaseConstants.visitorCompany, SingletonVisitorProfile
                .getInstance().getCompany());
        values.put(DataBaseConstants.visitorCompanyWebsite,
                SingletonVisitorProfile.getInstance().getCompanyWebsite());
        values.put(DataBaseConstants.visitorFax, SingletonVisitorProfile
                .getInstance().getFax());
        values.put(DataBaseConstants.visitorBusinessCard,
                SingletonVisitorProfile.getInstance().getVisitorBusinessCard());
        database.insert(DataBaseConstants.tableNameVisitorRecordInOffMode,
                null, values);
    }

    /**
     * Created by jayapriya On 3/12/14 insert the visitRecord
     */
    public void insertVisitRecordInOffMode() {

        // deleteVisitorRecord();
        database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.visitPurpose, SingletonVisitorProfile
                .getInstance().getPurposeOfVisit());
        values.put(DataBaseConstants.dateOfVisit, SingletonVisitorProfile
                .getInstance().getDateOfVisit());
        values.put(DataBaseConstants.officeLocation, SingletonVisitorProfile
                .getInstance().getOfficeLocation());
        values.put(DataBaseConstants.visitEmployeeEmailId,
                SingletonEmployeeProfile.getInstance().getEmployeeMailId());
        values.put(DataBaseConstants.codeNo, SingletonVisitorProfile
                .getInstance().getCodeNo());
        values.put(DataBaseConstants.visitorPhoto, SingletonVisitorProfile
                .getInstance().getVisitorImage());
        values.put(DataBaseConstants.visitorQrCode, SingletonVisitorProfile
                .getInstance().getQrcode());
        values.put(DataBaseConstants.visitorSign, SingletonVisitorProfile
                .getInstance().getVisitorSign());
        values.put(DataBaseConstants.visitEmployeeName,
                SingletonEmployeeProfile.getInstance().getEmployeeName());
        values.put(DataBaseConstants.visitEmployeeId, SingletonEmployeeProfile
                .getInstance().getEmployeeId());
        values.put(DataBaseConstants.officeLocationNumber,
                SingletonVisitorProfile.getInstance().getLocationCode());
        values.put(DataBaseConstants.GMTDiffMinutes, SingletonVisitorProfile
                .getInstance().getGmtdiff());
        values.put(DataBaseConstants.visitEmployeePhoneNo,
                SingletonEmployeeProfile.getInstance().getEmployeeMobileNo());
        values.put(DataBaseConstants.visitIdInCreator, SingletonVisitorProfile
                .getInstance().getVisitId());

        database.insert(DataBaseConstants.tableNameVisitRecordInOffMode, null,
                values);

        // }

    }

    /**
     *
     */
    public void insertVisitorAndVisitLink(Integer visitorId, Integer visitId) {
        database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.visitorId, visitorId);
        values.put(DataBaseConstants.visitId, visitId);

        // Log.d("window", "link data Inserted values" + values);
        database.insert(DataBaseConstants.tableVisitorVisitLink, null, values);

        // }

    }

    public void insertStationery(Stationery item) {
        // ////Log.d("dbhelper", "insertSnack" + " code " + item.size());
        database = this.getWritableDatabase();
        // for (int i = MainConstants.kConstantZero; i < item.size(); i++) {

        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.dbStationeryItemCode,
                item.getStationeryCode());
        values.put(DataBaseConstants.dbStationeryItemName,
                item.getStationeryName());
        values.put(DataBaseConstants.dbStationeryItemLimitPerMonth,
                item.getLimitPerMonth());
        // //////Log.d("insert", "photo" + item.get(i).photo +
        // " tamil "+item.get(i).snackNameInTamil);
        // values.put(DataBaseConstants.dbStationeryItemImage,
        // item.getStationeryImage());
        values.put(DataBaseConstants.dbDirtyBit, 0);

        database.insertWithOnConflict(DataBaseConstants.tableStationeryList,
                null, values, SQLiteDatabase.CONFLICT_REPLACE);
        // }
//		database.close();
    }

    /**
     * Update StationeryList ID
     *
     * @author jayapriya
     * @created 22/03/16
     */
    public void updateStationeryList(Stationery stationery) {

        try {
            database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.dbStationeryCreatorId,
                    stationery.getCreatorId());
            values.put(DataBaseConstants.dbDirtyBit, 1);
            database.execSQL("UPDATE " + DataBaseConstants.tableStationeryList
                    + " SET " + DataBaseConstants.dbStationeryItemCode + "="
                    + stationery.getStationeryCode());

        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException"+e);
        }
    }

    /**
     * Update StationeryList ID
     *
     * @author jayapriya
     * @created 22/03/16
     */
    public void updateStationeryEntry(StationeryEntry stationery) {

        try {

            database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.dbStationeryCreatorId,
                    stationery.getCreatorId());
            values.put(DataBaseConstants.dbDirtyBit, 1);
            database.execSQL("UPDATE "
                    + DataBaseConstants.tableStationeryItemEntry + " SET "
                    + DataBaseConstants.dbStationeryCreatorId + " = "
                    + stationery.getCreatorId() + " , "
                    + DataBaseConstants.dbDirtyBit + " = 1 WHERE "
                    + DataBaseConstants.dbStationeryItemCode + "="
                    + stationery.getStationeryItemCode() + " AND "
                    + DataBaseConstants.dbStationeryEntryTime + " = "
                    + stationery.getEntryTime());

        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException"+e);
        }
    }

    /**
     * Update StationeryList ID
     *
     * @author jayapriya
     * @created 22/03/16
     */
    public void updateStationeryRequest(StationeryRequest request) {

        try {

            database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.dbStationeryCreatorId,
                    request.getCreatorId());
            values.put(DataBaseConstants.dbDirtyBit, 1);
            database.execSQL("UPDATE " + DataBaseConstants.tableStationeryList
                    + " SET " + DataBaseConstants.dbStationeryCreatorId + "="
                    + request.getCreatorId() + ","
                    + DataBaseConstants.dbDirtyBit + "=1 WHERE "
                    + DataBaseConstants.dbStationeryRequestTime + "="
                    + request.getRequestTime());

        } catch (SQLiteException e) {
            // Log.d("window", "SQLiteException"+e);
        }
    }

    /**
     * insert StationeryList ID
     *
     * @author jayapriya
     * @created 22/03/16
     */
    public String insertStationeryEntry(StationeryEntry item) {
        // Log.d("dbhelper", "insertentey" + " code " + item.getEntryTime());
        database = this.getWritableDatabase();
        // for (int i = MainConstants.kConstantZero; i < item.size(); i++) {

        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.dbStationeryItemCode,
                item.getStationeryItemCode());
        values.put(DataBaseConstants.dbStationeryEntryTime, item.getEntryTime());
        values.put(DataBaseConstants.dbStationeryRequestId, item.getRequestId());
        // //////Log.d("insert", "photo" + item.get(i).photo +
        // " tamil "+item.get(i).snackNameInTamil);
        values.put(DataBaseConstants.dbStationeryItemQuantity,
                item.getQuantity());
        values.put(DataBaseConstants.dbDirtyBit, 1);
        try {
            database.insertWithOnConflict(
                    DataBaseConstants.tableStationeryItemEntry, null, values,
                    SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLiteException e) {
            return e.getMessage();
        } catch (IllegalStateException e) {
            // //Log.d("window", "IllegalStateException i:" + e);
            return e.getMessage();
        }
        return "";
        // }
        // database.close();
    }

    /**
     * insert StationeryList ID
     *
     * @author jayapriya
     * @created 22/03/16
     */
    public String insertStationeryRequest(StationeryRequest item) {
        // ////Log.d("dbhelper", "insertSnack" + " code " + item.size());
        database = this.getWritableDatabase();
        // for (int i = MainConstants.kConstantZero; i < item.size(); i++) {

        ContentValues values = new ContentValues();
        values.put(DataBaseConstants.dbStationeryRequestId, item.getRequestId());
        values.put(DataBaseConstants.dbStationeryRequestTime,
                item.getRequestTime());
        values.put(DataBaseConstants.dbEmployeeId, item.getEmployeeId());
        // //////Log.d("insert", "photo" + item.get(i).photo +
        // " tamil "+item.get(i).snackNameInTamil);
        values.put(DataBaseConstants.dbDirtyBit, 1);

        try {
            database.insertWithOnConflict(
                    DataBaseConstants.tableStationeryRequest, null, values,
                    SQLiteDatabase.CONFLICT_REPLACE);

        } catch (SQLiteException e) {
            return e.getMessage();
        } catch (IllegalStateException e) {
            // //Log.d("window", "IllegalStateException i:" + e);
            return e.getMessage();
        }
        return "";
        // }
        // database.close();
    }

    /**
     * insert StationeryList ID
     *
     * @author jayapriya
     * @created 22/03/16
     */
    public void insertBulkStationeryRecords(ArrayList<Stationery> list) {
        // Log.d("insert","stationery");
        if (list != null) {
            String sql = "INSERT INTO "
                    + DataBaseConstants.tableStationeryList
                    + " ("
                    // + DatabaseConstants.dbConstantStockEntryID + ","
                    + DataBaseConstants.dbStationeryItemCode + ","
                    + DataBaseConstants.dbStationeryItemName + ","
                    + DataBaseConstants.dbStationeryItemLimitPerMonth + ","
                    // + DataBaseConstants.dbStationeryItemImage + ","
                    + DataBaseConstants.dbStationeryCreatorId + ","
                    + DataBaseConstants.dbStationeryLocationCode + ","
                    + DataBaseConstants.dbDirtyBit + ") VALUES (?,?,?,?,?,?)";
            database = this.getWritableDatabase();
            database.beginTransactionNonExclusive();

            SQLiteStatement stmt = database.compileStatement(sql);

            for (int i = 0; i < list.size(); i++) {
//                Log.d("dsk"+i,""+list.get(i).getStationeryCode());
//                Log.d("dsk"+i,""+list.get(i).getStationeryLocationCode());
//                Log.d("dsk"+i,""+list.get(i).getStationeryName());
//                Log.d("dsk"+i,""+list.get(i).getCreatorId());

                /*
                 * if (stock.get(i).getStockEntryId() != null) { stmt.bindLong(1,
                 * stock.get(i).getStockEntryId()); } else { stmt.bindLong(1, 0L); }
                 */
                if (list.get(i).getStationeryCode() > 0) {

                    stmt.bindLong(1, list.get(i).getStationeryCode());
                } else {
                    stmt.bindLong(1, 0);
                }

                if (list.get(i).getStationeryName() != null) {
                    stmt.bindString(2, list.get(i).getStationeryName());
                } else {
                    stmt.bindString(2, "");
                }
                if (list.get(i).getLimitPerMonth() > 0) {
                    stmt.bindLong(3, list.get(i).getLimitPerMonth());
                } else {
                    stmt.bindLong(3, 0);
                }

                if (list.get(i).getCreatorId() > 0) {
                    stmt.bindLong(4, list.get(i).getCreatorId());
                }
                if (list.get(i).getStationeryLocationCode() > 0) {
                    stmt.bindLong(5, list.get(i).getStationeryLocationCode());
                }
                stmt.bindLong(6, 1);
                // ////Log.d("bult", "insert snack " +
                // list.get(i).getSnackNameInTamil());

                try {
                    stmt.execute();
                } catch (SQLiteConstraintException e) {
                    database.setTransactionSuccessful();
                    database.endTransaction();
                    return;
                }
                stmt.clearBindings();
            }

            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }


    public void insertBulkstationeryEntryRecords(ArrayList<StationeryEntry> list) {
        // Log.d("insert","stationery");
        if (list != null) {
            String sql = "INSERT INTO "

                    + DataBaseConstants.tableStationeryItemEntry
                    + " ("
                    // + DatabaseConstants.dbConstantStockEntryID + ","
                    + DataBaseConstants.dbStationeryEntryTime + ","
                    + DataBaseConstants.dbStationeryItemCode + ","
                    + DataBaseConstants.dbStationeryRequestId + ","
                    + DataBaseConstants.dbStationeryItemQuantity + ","
                    + DataBaseConstants.dbStationeryCreatorId + ","
                    + DataBaseConstants.dbDirtyBit + ") VALUES (?,?,?,?,?,?)";
            database = this.getWritableDatabase();
            database.beginTransactionNonExclusive();

            SQLiteStatement stmt = database.compileStatement(sql);
            for (int i = 0; i < list.size(); i++) {

                /*
                 * if (stock.get(i).getStockEntryId() != null) { stmt.bindLong(1,
                 * stock.get(i).getStockEntryId()); } else { stmt.bindLong(1, 0L); }
                 */
                if (list.get(i).getEntryTime() > 0) {

                    stmt.bindLong(1, list.get(i).getEntryTime());
                } else {
                    stmt.bindLong(1, 0);
                }

                if (list.get(i).getStationeryItemCode() > 0) {
                    stmt.bindLong(2, list.get(i).getStationeryItemCode());
                } else {
                    stmt.bindLong(2, 0);
                }
                if (list.get(i).getRequestId() != null) {
                    stmt.bindString(3, list.get(i).getRequestId());
                } else {
                    stmt.bindString(3, "");
                }
                if ((list.get(i).getQuantity() > 0)// &&(new
                    // File(list.get(i).getPhoto()).exists())
                        ) {
                    // Log.d("list","photo"+list.get(i).getPhoto());
                    stmt.bindLong(4, list.get(i).getQuantity());
                    // stmt.bindLong(5, 2);
                } else {
                    stmt.bindLong(4, 0);
                    // stmt.bindLong(5, 1);
                }
                if (list.get(i).getCreatorId() > 0) {
                    stmt.bindLong(5, list.get(i).getCreatorId());
                }
                stmt.bindLong(6, 1);
                // ////Log.d("bult", "insert snack " +
                // list.get(i).getSnackNameInTamil());

                try {
                    stmt.execute();
                } catch (SQLiteConstraintException e) {
                    database.setTransactionSuccessful();
                    database.endTransaction();
                    return;
                }
                stmt.clearBindings();
            }

            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    public void insertBulkStationeryRequestRecords(
            ArrayList<StationeryRequest> list) {
        // Log.d("insert","stationery");
        if (list != null) {
            String sql = "INSERT INTO "
                    + DataBaseConstants.tableStationeryRequest
                    + " ("
                    // + DatabaseConstants.dbConstantStockEntryID + ","
                    + DataBaseConstants.dbStationeryRequestTime + ","
                    + DataBaseConstants.dbStationeryRequestId + ","
                    + DataBaseConstants.dbEmployeeId + ","
                    + DataBaseConstants.dbStationeryCreatorId + ","
                    + DataBaseConstants.dbDirtyBit + ") VALUES (?,?,?,?,?)";
            database = this.getWritableDatabase();
            database.beginTransactionNonExclusive();

            SQLiteStatement stmt = database.compileStatement(sql);
            for (int i = 0; i < list.size(); i++) {
                /*
                 * if (stock.get(i).getStockEntryId() != null) { stmt.bindLong(1,
                 * stock.get(i).getStockEntryId()); } else { stmt.bindLong(1, 0L); }
                 */

                if (list.get(i).getRequestTime() > 0) {

                    stmt.bindLong(1, list.get(i).getRequestTime());
                } else {
                    stmt.bindLong(1, 0l);
                }


                if (list.get(i).getRequestId() != null) {
                    stmt.bindString(2, list.get(i).getRequestId());
                } else {
                    stmt.bindString(2, "");
                }


                if (list.get(i).getEmployeeId() != null) {
                    stmt.bindString(3, list.get(i).getEmployeeId());
                } else {
                    stmt.bindString(3, "");
                }
                if ((list.get(i).getCreatorId() > 0)// &&(new
                    // File(list.get(i).getPhoto()).exists())
                        ) {
                    // Log.d("list","photo"+list.get(i).getPhoto());
                    stmt.bindLong(4, list.get(i).getCreatorId());
                    // stmt.bindLong(5, 2);
                } else {
                    stmt.bindString(4, "");
                    // stmt.bindLong(5, 1);
                }

                stmt.bindLong(5, 1);
                // ////Log.d("bult", "insert snack " +
                // list.get(i).getSnackNameInTamil());

                try {
                    stmt.execute();
                } catch (SQLiteConstraintException e) {
                    database.setTransactionSuccessful();
                    database.endTransaction();
                    return;
                }
                stmt.clearBindings();
            }

            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    /**
     * Insert visitor details
     *
     * @author Karthick
     * @created on 20170515
     */
    public void insertExistVsitor(ExistingVisitorDetails existingVisitorDetails) {

        if (existingVisitorDetails != null) {
            database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            if (existingVisitorDetails != null && existingVisitorDetails.getVisitedDate() > 0) {
                values.put(DataBaseConstants.colnExistLastVisitDate,
                        existingVisitorDetails.getVisitedDate());
            }
            if (existingVisitorDetails != null && existingVisitorDetails.getName() != null) {
                values.put(DataBaseConstants.colnExistVisitorName,
                        existingVisitorDetails.getName());
            }
            if (existingVisitorDetails != null && existingVisitorDetails.getCompanyName() != null) {
                values.put(DataBaseConstants.colnExistCreatorCompanyName,
                        existingVisitorDetails.getCompanyName());
            }
            if (existingVisitorDetails != null && existingVisitorDetails.getContact() != null) {
                values.put(DataBaseConstants.colnExistVisitorContact,
                        existingVisitorDetails.getContact());
            }
            if (existingVisitorDetails != null && existingVisitorDetails.getEmployeeEmail() != null) {
                values.put(DataBaseConstants.colnExistVisitorEmployee,
                        existingVisitorDetails.getEmployeeEmail());
            }
            if (existingVisitorDetails != null && existingVisitorDetails.getPurpose() != null) {
                values.put(DataBaseConstants.colnExistVisitorPurpose,
                        existingVisitorDetails.getPurpose());
            }
            if (existingVisitorDetails != null && existingVisitorDetails.getCreatorVisitID() != null) {
                values.put(DataBaseConstants.colnExistCreatorVisitID,
                        existingVisitorDetails.getCreatorVisitID());
            }
            database.insertWithOnConflict(DataBaseConstants.tableExistingVisitor,
                    null, values, SQLiteDatabase.CONFLICT_REPLACE);
        }
    }

    /**
     * insert all StationeryList based on Location Mapping.
     *
     * @author Karthikeyan D S
     * @created 25/05/18
     */
    public void insertBulkNewStationeryRecords(ArrayList<Stationery> newStationeryList) {
        // Log.d("insert","stationery");
        if (newStationeryList != null) {
            String sql = "INSERT INTO "
                    + DataBaseConstants.tableNameNewStationeryList
                    + " ("
                    // + DatabaseConstants.dbConstantStockEntryID + ","
                    + DataBaseConstants.dbStationeryItemCode + ","
                    + DataBaseConstants.dbStationeryItemName + ","
                    + DataBaseConstants.dbStationeryItemLimitPerMonth + ","
                    // + DataBaseConstants.dbStationeryItemImage + ","
                    + DataBaseConstants.dbStationeryCreatorId + ","
                    + DataBaseConstants.dbStationeryLocationCode + ","
                    + DataBaseConstants.dbStationeryOfficeDeviceLocation + ","
                    + DataBaseConstants.dbStationeryOfficeParentLocation + ","
                    + DataBaseConstants.dbDirtyBit + ") VALUES (?,?,?,?,?,?,?,?)";
            database = this.getWritableDatabase();
            database.beginTransactionNonExclusive();

            SQLiteStatement stmt = database.compileStatement(sql);

//            Log.d( "insertion "," "+newStationeryList.size());

//            ArrayList<Stationery>newFetchedStationeryList = newStationeryList;
            for (int i = 0; i < newStationeryList.size(); i++) {
//                Log.d("insertion "+i,"StationeryCode "+newStationeryList.get(i).getStationeryCode());
//                Log.d("insertion "+i,"StationeryLocationCode "+newStationeryList.get(i).getStationeryLocationCode());
//                Log.d("insertion "+i,"StationeryNam "+newStationeryList.get(i).getStationeryName());
//                Log.d("insertion "+i,"CreatorId "+newStationeryList.get(i).getCreatorId());
//                Log.d("insertion "+i,"OfficeParentLocation "+newStationeryList.get(i).getOfficeParentLocation());
//                Log.d("insertion "+i,"OfficeDeviceLocation "+newStationeryList.get(i).getOfficeDeviceLocation());

                /*
                 * if (stock.get(i).getStockEntryId() != null) { stmt.bindLong(1,
                 * stock.get(i).getStockEntryId()); } else { stmt.bindLong(1, 0L); }
                 */

                if (newStationeryList.get(i).getStationeryCode() > 0) {
                    stmt.bindLong(1, newStationeryList.get(i).getStationeryCode());
                } else {
                    stmt.bindLong(1, 0);
                }
                if (newStationeryList.get(i).getStationeryName() != null) {
                    stmt.bindString(2, newStationeryList.get(i).getStationeryName());
                } else {
                    stmt.bindString(2, MainConstants.kConstantEmpty);
                }
                if (newStationeryList.get(i).getLimitPerMonth() > 0) {
                    stmt.bindLong(3, newStationeryList.get(i).getLimitPerMonth());
                } else {
                    stmt.bindLong(3, 0);
                }
                if (newStationeryList.get(i).getCreatorId() > 0) {
                    stmt.bindLong(4, newStationeryList.get(i).getCreatorId());
                }
                if (newStationeryList.get(i).getStationeryLocationCode() > 0) {
                    stmt.bindLong(5, newStationeryList.get(i).getStationeryLocationCode());
                }
                if (newStationeryList.get(i).getOfficeDeviceLocation() != null) {
                    stmt.bindString(6, newStationeryList.get(i).getOfficeDeviceLocation());
                } else {
                    stmt.bindString(6, MainConstants.kConstantEmpty);
                }
                if (newStationeryList.get(i).getOfficeParentLocation() != null) {
                    stmt.bindString(7, newStationeryList.get(i).getOfficeParentLocation());
                } else {
                    stmt.bindString(7, MainConstants.kConstantEmpty);
                }
                stmt.bindLong(8, 1);
                // ////Log.d("bult", "insert snack " +
                // list.get(i).getSnackNameInTamil());

                try {
                    stmt.execute();
                } catch (SQLiteConstraintException e) {
                    database.setTransactionSuccessful();
                    database.endTransaction();
//                    Log.d("insertion","Database Values Returned");
                    return;
                }
                stmt.clearBindings();
            }

            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    /**
     * Bulk insert visitors details
     *
     * @author Karthick
     * @created on 20170515
     */
    public void insertBulkVisitorRecords(
            ArrayList<ExistingVisitorDetails> list) {
        if (list != null) {
            String sql = DataBaseConstants.insertInto
                    + DataBaseConstants.tableExistingVisitor
                    + DataBaseConstants.kConstantsOpenBrace
                    + DataBaseConstants.colnExistLastVisitDate + DataBaseConstants.kConstantsCamma
                    + DataBaseConstants.colnExistVisitorName + DataBaseConstants.kConstantsCamma
                    + DataBaseConstants.colnExistVisitorContact + DataBaseConstants.kConstantsCamma
                    + DataBaseConstants.colnExistVisitorEmployee + DataBaseConstants.kConstantsCamma
                    + DataBaseConstants.colnExistVisitorPurpose + DataBaseConstants.kConstantsCamma
                    + DataBaseConstants.colnExistCreatorVisitID + DataBaseConstants.kConstantsCloseBrace
                    + DataBaseConstants.valuesForExistVisitor;
            database = this.getWritableDatabase();
            database.beginTransactionNonExclusive();

            SQLiteStatement stmt = database.compileStatement(sql);
            for (int i = 0; i < list.size(); i++) {

                if (list.get(i) != null && list.get(i).getVisitedDate() > 0) {
                    stmt.bindLong(1, list.get(i).getVisitedDate());
                } else {
                    stmt.bindLong(1, -1);
                }
                if (list.get(i) != null && list.get(i).getName() != null) {
                    stmt.bindString(2, list.get(i).getName());
                } else {
                    stmt.bindString(2, MainConstants.kConstantEmpty);
                }
                if (list.get(i) != null && list.get(i).getContact() != null) {
                    stmt.bindString(3, list.get(i).getContact());
                } else {
                    stmt.bindString(3, MainConstants.kConstantEmpty);
                }
                if (list.get(i) != null && list.get(i).getEmployeeEmail() != null) {
                    stmt.bindString(4, list.get(i).getEmployeeEmail());
                } else {
                    stmt.bindString(4, MainConstants.kConstantEmpty);
                }
                if (list.get(i) != null && list.get(i).getPurpose() != null) {
                    stmt.bindString(5, list.get(i).getPurpose());
                } else {
                    stmt.bindString(5, MainConstants.kConstantEmpty);
                }
                if (list.get(i) != null && list.get(i).getCreatorVisitID() != null) {
                    stmt.bindString(6, list.get(i).getCreatorVisitID());
                } else {
                    stmt.bindString(6, MainConstants.kConstantEmpty);
                }

                try {
                    stmt.execute();
                } catch (SQLiteConstraintException e) {
                    database.setTransactionSuccessful();
                    database.endTransaction();
                    return;
                }
                stmt.clearBindings();
            }
            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    /**
     * Search visitor details using contact
     *
     * @author Karthick
     * @created on 20170515
     */
    public ExistingVisitorDetails searchExistVisitor(String contact, String purpose) {
        ExistingVisitorDetails existingVisitorDetails = null;
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableExistingVisitor
                    + DataBaseConstants.kConstantsWhere
                    + DataBaseConstants.colnExistVisitorContact
                    + DataBaseConstants.kConstantsLikeStatementWithSingleQuote
                    + "+91"
                    + contact
                    + DataBaseConstants.kConstantsSingleQuote + " AND " + DataBaseConstants.colnExistVisitorPurpose
                    + " COLLATE NOCASE != '" + purpose + "'";
            Cursor cursor = database.rawQuery(selectQuery, null);
//            Log.d("DSKselectQuery",""+selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                existingVisitorDetails = cursorToExistVisitor(cursor);
                cursor.moveToNext();
            }
            cursor.close();
//            Log.d("DSK",""+cursor.getCount());
        } catch (SQLiteException e) {
//            Log.d("DSK",""+e.getLocalizedMessage());
        }
//        Log.d("DSK",""+existingVisitorDetails);
        return existingVisitorDetails;
    }


//    /**
//     * Search visitor details using contact
//     *
//     * @author Karthick
//     * @created on 20170515
//     */
//    public ExistingVisitorDetails searchExistVisitorByPurpose(String contact, String purpose) {
//        ExistingVisitorDetails existingVisitorDetails = null;
//        try {
//            database = this.getReadableDatabase();
//            String selectQuery = DataBaseConstants.kConstantsSelectQuery
//                    + DataBaseConstants.tableExistingVisitor
//                    + DataBaseConstants.kConstantsWhere
//                    + DataBaseConstants.colnExistVisitorContact
//                    + DataBaseConstants.kConstantsLikeStatementWithSingleQuote
//                    + contact +
//                    DataBaseConstants.kConstantsSingleQuote + " AND " + DataBaseConstants.colnExistVisitorPurpose
//                    + " COLLATE NOCASE = '" + purpose + "'";
////            Log.d("DSKSelectQuery",""+selectQuery);
//            Cursor cursor = database.rawQuery(selectQuery, null);
////         Log.d("DSK",""+cursor.getCount());
//            cursor.moveToFirst();
//            while (!cursor.isAfterLast()) {
//                existingVisitorDetails = cursorToExistVisitor(cursor);
//                cursor.moveToNext();
//            }
//            cursor.close();
//        } catch (SQLiteException e) {
////            Log.d("DSK",""+e.getLocalizedMessage());
//        }
////        Log.d("DSK",""+existingVisitorDetails);
//        return existingVisitorDetails;
//    }

    /**
     * Read cursor value and set to ExistingVisitorDetails variables
     *
     * @author Karthick
     * @created on 20170515
     */
    private ExistingVisitorDetails cursorToExistVisitor(Cursor cursor) {

        ExistingVisitorDetails existingVisitorDetails = new ExistingVisitorDetails();
        if (existingVisitorDetails != null && cursor.getLong(MainConstants.kConstantZero) > 0) {
            existingVisitorDetails.setVisitedDate(cursor
                    .getLong(MainConstants.kConstantZero));
//            Log.d("DSK "," existingVisitorDetails setVisitedDate "+existingVisitorDetails.getVisitedDate());
        }
        if (existingVisitorDetails != null && cursor.getString(MainConstants.kConstantOne) != null) {
            existingVisitorDetails.setName(cursor
                    .getString(MainConstants.kConstantOne));
//            Log.d("DSK "," existingVisitorDetails setName "+existingVisitorDetails.getName());
        }
        if (existingVisitorDetails != null && cursor.getString(MainConstants.kConstantTwo) != null) {
            existingVisitorDetails.setCompanyName(cursor
                    .getString(MainConstants.kConstantTwo));
//            Log.d("DSK "," existingVisitorDetails setContact "+existingVisitorDetails.getCompanyName());
        }
        if (existingVisitorDetails != null && cursor.getString(MainConstants.kConstantThree) != null) {
            existingVisitorDetails.setContact(cursor
                    .getString(MainConstants.kConstantThree));
//            Log.d("DSK "," existingVisitorDetails setContact "+existingVisitorDetails.getContact());
        }
        if (existingVisitorDetails != null && cursor.getString(MainConstants.kConstantFour) != null) {
            existingVisitorDetails.setEmployeeEmail(cursor.getString(MainConstants.kConstantFour));
//            Log.d("DSK "," existingVisitorDetails setEmployeeEmail "+existingVisitorDetails.getEmployeeEmail());
        }
        if (existingVisitorDetails != null && cursor.getString(MainConstants.kConstantFive) != null) {
            existingVisitorDetails.setPurpose(cursor
                    .getString(MainConstants.kConstantFive));
//            Log.d("DSK "," existingVisitorDetails setPurpose "+existingVisitorDetails.getPurpose());
        }
        if (existingVisitorDetails != null && cursor.getString(MainConstants.kConstantSix) != null) {
            existingVisitorDetails.setCreatorVisitID(cursor.getString(MainConstants.kConstantSix));
//            Log.d("DSK "," existingVisitorDetails setCreatorVisitID "+existingVisitorDetails.getCreatorVisitID());
        }
        return existingVisitorDetails;
    }


    /**
     * get all snack details
     *
     * @author jayapriya
     * @created on 201501030
     */
    public ArrayList<Stationery> getAllStationery() {
        ArrayList<Stationery> Stationery = new ArrayList<Stationery>();
        database = this.getReadableDatabase();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableStationeryList + " ORDER BY Name";
        Cursor cursor = database.rawQuery(selectQuery, null);

        // Log.d("window", "stationery  " + selectQuery);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Stationery item = cursorToStationery(cursor);

            Stationery.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return Stationery;
    }

    /**
     * get all Stationery details based on location code
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    public ArrayList<Stationery> getAllStationeryBasedOnCode(int locationCode) {
        ArrayList<Stationery> stationeryDetails = new ArrayList<Stationery>();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableStationeryList + " where LocationCode <= " + locationCode + " ORDER BY Name";
            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "stationery  " + selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Stationery item = cursorToStationery(cursor);

                stationeryDetails.add(item);
                cursor.moveToNext();
            }
            cursor.close();
            return stationeryDetails;
        } catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Stationery Based on LocationCode : "+dbLockedException.getLocalizedMessage());
        }
        return stationeryDetails;
    }

    /**
     * get all Stationery details based on location code
     *
     * @author karthikeyan D S
     * @created on 20180521
     */
    public ArrayList<Stationery> getAllStationeryBasedOnDeviceLocation(String deviceLocationName) {
        ArrayList<Stationery> Stationery = new ArrayList<Stationery>();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameNewStationeryList + " where DeviceLocation like '" + deviceLocationName + "' ORDER BY Name";
//        Log.d("DSK", "stationery  " + selectQuery);

            Cursor cursor = database.rawQuery(selectQuery, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Stationery item = cursorToStationery(cursor);
                Stationery.add(item);
                cursor.moveToNext();
            }
            cursor.close();
            return Stationery;
        } catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Stationery Based on Deice Location : "+dbLockedException.getLocalizedMessage());
        }
        return Stationery;
    }

    /**
     * Cursor For Stationery Details to allocate fetched Stationery
     *
     * @author karthikeyan D S
     * @created on 20180521
     */
    private Stationery cursorToStationery(Cursor cursor) {
        Stationery stationeryItem = new Stationery();
        stationeryItem.setRecordId(cursor
                .getInt(MainConstants.kConstantZero));
        stationeryItem.setStationeryCode(cursor
                .getInt(MainConstants.kConstantOne));
        stationeryItem.setStationeryName(cursor
                .getString(MainConstants.kConstantTwo));
        stationeryItem.setLimitPerMonth(cursor
                .getInt(MainConstants.kConstantThree));
        // stationeryItem.setStationeryImage(cursor.getString(MainConstants.kConstantThree));
        stationeryItem.setCreatorId(cursor
                .getLong(MainConstants.kConstantFour));
        stationeryItem.setDirtyBit(cursor.getInt(MainConstants.kConstantFive));

        // Log.d("snack", "name" + stationeryItem.getStationeryName());
        return stationeryItem;
    }

    /**
     * get all Stationery details based on location code
     * <p>
     * new
     *
     * @author Karthikeyan D S
     * @created on 20180521
     */
    public Stationery getAllStationeryBasedOnStationeryCode(int stationeryCode, String mDeviceLocation) {
        Stationery stationeryDetails = new Stationery();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsSelectQuery
                    + DataBaseConstants.tableNameNewStationeryList + " where Code =" + stationeryCode + " AND DeviceLocation ='" + mDeviceLocation + "' ORDER BY Name";
            Cursor cursor = database.rawQuery(selectQuery, null);

            // Log.d("window", "stationery  " + selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Stationery item = cursorToStationery(cursor);
                stationeryDetails = item;
                cursor.moveToNext();
            }
            cursor.close();
            return stationeryDetails;
        } catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Stationery Based on Code : "+dbLockedException.getLocalizedMessage());
        }
        return stationeryDetails;
    }

    /**
     * @return
     */
    public Integer getVisitorVisitLinkRecord(Integer visitorId) {
        Integer visitId = 0;
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableVisitorVisitLink
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.visitorId
                + DataBaseConstants.kConstantEqual + visitorId;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            VisitorVisitLinkRecord linkRecord = cursorToVisitorVisitLink(cursor);
            visitId = linkRecord.getVisitLinkId();
            // listOfRecord.add(linkRecord);
            cursor.moveToNext();
        }
        cursor.close();

        return visitId;

    }

    public ArrayList<VisitorVisitLinkRecord> getAllVisitorVisitLinkRecord() {
        ArrayList<VisitorVisitLinkRecord> listOfRecord = new ArrayList<VisitorVisitLinkRecord>();
        // Integer visitId = 0;
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableVisitorVisitLink;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            VisitorVisitLinkRecord linkRecord = cursorToVisitorVisitLink(cursor);
            // visitId = linkRecord.getVisitLinkId();
            listOfRecord.add(linkRecord);
            cursor.moveToNext();
        }
        cursor.close();

        return listOfRecord;

    }

    private VisitorVisitLinkRecord cursorToVisitorVisitLink(Cursor cursor) {
        VisitorVisitLinkRecord linkRecord = new VisitorVisitLinkRecord();
        linkRecord.setVisitorLinkId(cursor.getInt(MainConstants.kConstantZero));
        linkRecord.setVisitLinkId(cursor.getInt(MainConstants.kConstantOne));
        return linkRecord;
    }

    /**
     * Created by jayapriya On 3/12/14 get the visitRecord.
     */
    public VisitRecord getVisitRecordListInOffMode(Integer visitId) {

        // ArrayList<VisitRecord> listOfVisit = new ArrayList<VisitRecord>();
        VisitRecord visitRecord = new VisitRecord();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitRecordInOffMode
                + DataBaseConstants.kConstantsWhere + DataBaseConstants.visitId
                + DataBaseConstants.kConstantEqual + visitId;
        try {
            database = this.getReadableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);

            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {

                visitRecord = cursorToVisitInOffMode(cursor);

                // listOfVisit.add(visitRecord);
                cursor.moveToNext();
            }
            cursor.close();
            return visitRecord;
        } catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Visitor Record OFF Mode : "+dbLockedException.getLocalizedMessage());
        }
        return visitRecord;
    }

    public ArrayList<VisitRecord> getAllVisitRecordListInOffMode() {

        ArrayList<VisitRecord> listOfVisit = new ArrayList<VisitRecord>();

        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitRecordInOffMode;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            VisitRecord visitRecord = cursorToVisitInOffMode(cursor);

            listOfVisit.add(visitRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return listOfVisit;
    }

    private VisitRecord cursorToVisitInOffMode(Cursor cursor) {
        VisitRecord visitRecord = new VisitRecord();
        visitRecord.setVisitIdInOffMode(cursor
                .getInt(MainConstants.kConstantZero));
        visitRecord.setPurpose(cursor.getString(MainConstants.kConstantOne));
        visitRecord
                .setDateOfVisit(cursor.getString(MainConstants.kConstantTwo));
        visitRecord.setOfficeLocation(cursor
                .getString(MainConstants.kConstantThree));
        visitRecord.setEmployeeEmailId(cursor
                .getString(MainConstants.kConstantFour));
        visitRecord.setCodeNumber(cursor.getInt(MainConstants.kConstantFive));
        visitRecord.setVisitorPhoto(cursor
                .getString(MainConstants.kConstantSix));
        visitRecord.setVisitorQrCode(cursor
                .getString(MainConstants.kConstantSeven));
        visitRecord.setVisitorSign(cursor
                .getString(MainConstants.kConstantEight));
        visitRecord.setEmployeeName(cursor
                .getString(MainConstants.kConstantNine));
        visitRecord.setEmployeeId(cursor.getInt(MainConstants.kConstantTen));
        visitRecord.setOfficeLocationNumber(cursor
                .getInt(MainConstants.kConstantEleven));
        visitRecord.setGMTDiffMinutes(cursor
                .getInt(MainConstants.kConstantTwelve));
        visitRecord.setEmployeePhoneNo(cursor
                .getString(MainConstants.kConstantThirteen));
        visitRecord.setVisitIdInCreator(cursor
                .getString(MainConstants.kConstantFourteen));

        return visitRecord;
    }

    /**
     * get the visitorRecord list
     *
     * @return Array list of visitor record when the wifi on.
     * @author jayapriya
     * @created 02/12/2014
     */
    public ArrayList<VisitorRecord> getVisitorRecordListInOffMode() {

        ArrayList<VisitorRecord> listOfVisitor = new ArrayList<VisitorRecord>();

        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitorRecordInOffMode;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            VisitorRecord visitorRecord = cursorToVisitorInOffMode(cursor);

            listOfVisitor.add(visitorRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return listOfVisitor;
    }

    /*
     * cursor to get the visitor detail when wifi off mode
     */
    private VisitorRecord cursorToVisitorInOffMode(Cursor cursor) {
        VisitorRecord visitorRecord = new VisitorRecord();
        visitorRecord.setVisitorIdInOffMode(cursor
                .getInt(MainConstants.kConstantZero));
        visitorRecord.setName(cursor.getString(MainConstants.kConstantOne));
        visitorRecord.setAddress(cursor.getString(MainConstants.kConstantTwo));
        visitorRecord.setMobileNo(cursor
                .getString(MainConstants.kConstantThree));
        visitorRecord.setMailId(cursor.getString(MainConstants.kConstantFour));
        visitorRecord.setDesignation(cursor
                .getString(MainConstants.kConstantFive));
        visitorRecord.setCompany(cursor.getString(MainConstants.kConstantSix));
        visitorRecord.setCompanyWebsite(cursor
                .getString(MainConstants.kConstantSeven));
        visitorRecord.setFax(cursor.getString(MainConstants.kConstantEight));
        visitorRecord.setBusinessCard(cursor
                .getString(MainConstants.kConstantNine));

        return visitorRecord;
    }

    /*
     * get the visitor records in the local database
     *
     * @author jayapriya
     *
     * @created 02/12/2014
     */
    public ArrayList<VisitorRecord> getVisitorRecordList() {

        ArrayList<VisitorRecord> listOfVisitor = new ArrayList<VisitorRecord>();

        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitorDetail;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            VisitorRecord visitorRecord = cursorToVisitor(cursor);

            listOfVisitor.add(visitorRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return listOfVisitor;
    }

    /**
     * search visitor list in phone number
     *
     * @param contact
     * @return visitor record.
     * @author jayapriya
     * @created 02/12/2014
     */
    public VisitorRecord searchVisitorListInPhoneNo(String contact) {
        // Log.d("search", "visitor" + contact);
        String selectQuery;
        VisitorRecord visitorRecord = new VisitorRecord();
        selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitorDetail
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.visitorMobileNo
                + DataBaseConstants.kConstantEqual + "\"" + contact + "\"";
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            visitorRecord = cursorToVisitor(cursor);

            // listOfVisit.add(visitorRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return visitorRecord;
    }

    /**
     * search the visitor record in visitor mailId
     *
     * @param contact
     * @return visitor record
     * @author jayapriya
     * @created 02/12/2014
     */
    public VisitorRecord searchVisitorListInMailId(String contact) {
        String selectQuery;
        VisitorRecord visitorRecord = new VisitorRecord();
        selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameVisitorDetail
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.visitorMailId
                + DataBaseConstants.kConstantEqual + "\"" + contact + "\"";
        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            visitorRecord = cursorToVisitor(cursor);

            // listOfVisit.add(visitorRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return visitorRecord;
    }

    /*
     * cursor to get the visitor record
     *
     * @param cursor
     *
     * @return visitor record
     *
     * @author jayapriya
     *
     * @created 02/12/2014
     */
    private VisitorRecord cursorToVisitor(Cursor cursor) {
        VisitorRecord visitorRecord = new VisitorRecord();
        visitorRecord.setRecordId(cursor.getLong(MainConstants.kConstantZero));
        visitorRecord.setName(cursor.getString(MainConstants.kConstantOne));
        visitorRecord.setAddress(cursor.getString(MainConstants.kConstantTwo));
        visitorRecord.setMobileNo(cursor
                .getString(MainConstants.kConstantThree));
        visitorRecord.setMailId(cursor.getString(MainConstants.kConstantFour));
        visitorRecord.setDesignation(cursor
                .getString(MainConstants.kConstantFive));
        visitorRecord.setCompany(cursor.getString(MainConstants.kConstantSix));
        visitorRecord.setCompanyWebsite(cursor
                .getString(MainConstants.kConstantSeven));
        visitorRecord.setFax(cursor.getString(MainConstants.kConstantEight));

        return visitorRecord;
    }

    /**
     * read cursor value
     *
     * @param cursor
     * @author Karthick
     * @created on 20161114
     */
    public VisitorDetailsDatabase readCursorValueVisitor(Cursor cursor) {

        VisitorDetailsDatabase visitorDetailsDb = new VisitorDetailsDatabase();
        visitorDetailsDb.setVisitDate(cursor.getLong(0));
        visitorDetailsDb.setJsonDataId(cursor.getString(1));
        visitorDetailsDb.setJsonData(cursor.getString(2));
        visitorDetailsDb.setPhotoUrl(cursor.getBlob(3));
        visitorDetailsDb.setSignatureUrl(cursor.getBlob(4));
        visitorDetailsDb.setPhotoUploaded(cursor.getInt(5));
        visitorDetailsDb.setSignatureUploaded(cursor.getInt(6));
        visitorDetailsDb.setVisitPurpose(cursor.getString(7));

        return visitorDetailsDb;
    }

    /*
     * delete the visitorRecord table in local database.
     *
     * @author jayapriya
     *
     * @created 02/12/2014
     */
    public void deleteVisitorRecord() {
        // //Log.d("deleteVisitorRecord", "deleteVisitorRecord");
        database = this.getWritableDatabase();
        String query = constants.DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameVisitorRecordInOffMode;
        int values = 0;

        // //Log.d("delete", "value" + values);
        database.execSQL(query);
//		database.close();
    }

    /**
     * search the purpose record in visitor detail.
     *
     * @return
     * @author jayapriya
     * @created 04/11/2014
     */
    public ArrayList<PurposeOfVisitRecord> searchPurposeOfVisitList(
            String purpose) {

        ArrayList<PurposeOfVisitRecord> listOfVisit = new ArrayList<PurposeOfVisitRecord>();

        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNamePurposeOfVisit
                + DataBaseConstants.kConstantsWhere + DataBaseConstants.purpose
                + DataBaseConstants.kConstantEqual + purpose;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            PurposeOfVisitRecord visitRecord = cursorToVisit(cursor);

            listOfVisit.add(visitRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return listOfVisit;
    }

    /**
     * get purpose record
     *
     * @return
     * @author jayapriya
     * @created 04/11/2014
     */
    public ArrayList<PurposeOfVisitRecord> getPurposeOfVisitList() {
        ArrayList<PurposeOfVisitRecord> listOfVisit = new ArrayList<PurposeOfVisitRecord>();

        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNamePurposeOfVisit
                + DataBaseConstants.kConstantsOrderBy
                + DataBaseConstants.visitFrequency
                + DataBaseConstants.kConstantsDescWithLimit
                + MainConstants.kConstantFontSplitCount;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            PurposeOfVisitRecord visitRecord = cursorToVisit(cursor);

            listOfVisit.add(visitRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return listOfVisit;
    }

    /*
     * cursor to get the purpose of visit
     *
     * @author jayapriya
     *
     * @created 04/11/2014
     */
    private PurposeOfVisitRecord cursorToVisit(Cursor cursor) {
        PurposeOfVisitRecord visitRecord = new PurposeOfVisitRecord();

        visitRecord.setPurpose(cursor.getString(MainConstants.kConstantZero));
        // Log.d("purpose", "" + cursor.getString(MainConstants.kConstantZero));
        visitRecord
                .setVisitFrequency(cursor.getInt(MainConstants.kConstantOne));

        visitRecord.setLocation(cursor.getInt(MainConstants.kConstantTwo));

        return visitRecord;
    }

    /*
     * get the purpose of visit depends on purpose
     *
     * @author jayapriya
     *
     * @created 04/11/2014
     */
    public ArrayList<PurposeOfVisitRecord> getVisitPurposeList(String purpose) {

        ArrayList<PurposeOfVisitRecord> listOfVisit = new ArrayList<PurposeOfVisitRecord>();

        String selectQuery = DataBaseConstants.kConstantsSelectQueryForVisitPurpose
                + DataBaseConstants.tableNamePurposeOfVisit
                + DataBaseConstants.kConstantsWhereConditionInVisitPurpose
                + purpose
                + DataBaseConstants.kConstantsPercentageWithSingleQuote;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            PurposeOfVisitRecord visitRecord = cursorToVisitPurpose(cursor);
            listOfVisit.add(visitRecord);
            cursor.moveToNext();
        }
        cursor.close();
        return listOfVisit;
    }

    /*
     * cursor to get the visit purpose
     *
     * @author jayapriya
     *
     * @created 04/11/2014
     */
    private PurposeOfVisitRecord cursorToVisitPurpose(Cursor cursor) {
        PurposeOfVisitRecord visitRecord = new PurposeOfVisitRecord();

        visitRecord.setPurpose(cursor.getString(MainConstants.kConstantZero));
        // Log.d("purpose", "" + cursor.getString(MainConstants.kConstantZero));
        return visitRecord;
    }

    /**
     * delete the purpose of visit
     *
     * @author jayapriya
     * @created 04/11/2014
     */
    public void deletePurposeOfVisit() {
        database = this.getWritableDatabase();
        String query = constants.DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNamePurposeOfVisit;
        database.execSQL(query);

    }

    /**
     * delete visit record in local with the visitidInlocal
     *
     * @param visitIdInLocal
     * @author jayapriya
     * @created 04/11/2014
     */
    public void deleteVisitRecordInWifiOn(Integer visitIdInLocal) {
        // Log.d("deleteVisitRecord", "deleteVisitRecord");
        database = this.getWritableDatabase();
        String query = constants.DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameVisitRecordInOffMode
                + DataBaseConstants.kConstantsWhere + DataBaseConstants.visitId
                + DataBaseConstants.kConstantEqual + visitIdInLocal;
        database.execSQL(query);
    }

    /**
     * delete visitor record in local with the visitoridInlocal
     *
     * @param visitorId
     * @author jayapriya
     * @created 04/11/2014
     */
    public void deleteVisitorRecordInWifiOn(Integer visitorId) {
        // Log.d("deleteVisitorRecord", "deleteVisitorRecord");
        database = this.getWritableDatabase();
        String query = constants.DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameVisitorRecordInOffMode
                + DataBaseConstants.kConstantsWhere
                + DataBaseConstants.visitorId
                + DataBaseConstants.kConstantEqual + visitorId;
        database.execSQL(query);
    }

    /**
     * delete visitorvisit record in local with the visitidInlocal
     *
     * @param visitIdInLocal
     * @author jayapriya
     * @created 04/11/2014
     */

    public void deleteVisitorVisitRecordInWifiOn(Integer visitIdInLocal) {
        database = this.getWritableDatabase();
        String query = constants.DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableVisitorVisitLink
                + DataBaseConstants.kConstantsWhere + DataBaseConstants.visitId
                + DataBaseConstants.kConstantEqual + visitIdInLocal;
        database.execSQL(query);
    }

    /**
     * Insert internal app logs
     *
     * @author karthick
     * @created on 20150921
     */
    public void insertLogs(InternalAppLogs internalAppLogs) {
        deleteOldLogs();
        try {
            database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DataBaseConstants.dbLogDate, internalAppLogs.getLogDate());
            values.put(DataBaseConstants.dbLogErrorMessage,
                    internalAppLogs.getErrorMessage());
            values.put(DataBaseConstants.dbLogErrorCode,
                    internalAppLogs.getErrorCode());
            values.put(DataBaseConstants.dbLogWifiSingnalStrength,
                    internalAppLogs.getWifiSignalStrength());
            try {
                database.insert(DataBaseConstants.tableNameLogs, null, values);
            }catch (SQLiteDatabaseLockedException dbLockedException) {
                setInternalLogs(context, ErrorConstants.dataBaseLockedException,"While Insert Logs : "+dbLockedException.getLocalizedMessage());
            }

        } catch (SQLiteException e) {

        }

    }

    /**
     * Get all logs
     *
     * @author karthick
     * @created on 20150921
     */
    public ArrayList<InternalAppLogs> getLogs() {
        // ////Log.d("dbhelper", "getLogs");
        ArrayList<InternalAppLogs> internalAppLogList = new ArrayList<InternalAppLogs>();
        internalAppLogList.clear();
        database = this.getReadableDatabase();
        String selectQuery = DataBaseConstants.kConstantsSelectQuery
                + DataBaseConstants.tableNameLogs
                + DataBaseConstants.kConstantOrderBy
                + DataBaseConstants.dbLogDate + DataBaseConstants.kConstantDesc;
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            InternalAppLogs internalAppLogs = new InternalAppLogs();
            internalAppLogs.setLogDate(cursor.getLong(0));
            internalAppLogs.setErrorMessage(cursor.getString(1));
            internalAppLogs.setErrorCode(cursor.getInt(2));
            internalAppLogs.setWifiSignalStrength(cursor.getInt(3));
            internalAppLogList.add(internalAppLogs);
            cursor.moveToNext();
        }
        cursor.close();
        // db.close();
        return internalAppLogList;
    }

    /*
     * Delete All logs
     *
     * @author karthick
     *
     * @created on 20150921
     */
    public void deleteAllLogs() {
        database = this.getWritableDatabase();
        // ////////Log.d("dbhelper", "deleteemployee");
        String query = DataBaseConstants.ConstantsDeleteQuery
                + DataBaseConstants.tableNameLogs;
        try {
            database.execSQL(query);
        } catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"While Delete Logs : "+dbLockedException.getLocalizedMessage());
        }
        // database.close();
    }

    /**
     * Delete Employee ForgetID Table Data before 10days
     *
     * @author Karthikeyan D S
     * @created on 28Sep2018
     */
    public void deleteEmployeeForgetIDTable(long checkInTimeTenDaysBeforeValue)
    {
        database = this.getWritableDatabase();
        String deleteQuery = DataBaseConstants.ConstantsDeleteQuery + DataBaseConstants.tableNameEmployeeForgetID
                +DataBaseConstants.kConstantsWhere+DataBaseConstants.dbEmployeeCheckInTime
                +" <= " + checkInTimeTenDaysBeforeValue;
        try {
            database.execSQL(deleteQuery);
        }catch (SQLiteDatabaseLockedException dbLockedException){

        }
    }
    /**
     * Delete old logs
     *
     * @author Karthick
     * @created on 20161115
     */
    public void deleteOldLogs() {
        try {
            database = this.getWritableDatabase();
            String query = DataBaseConstants.kConstantsDeleteLogQuery;
            database.execSQL(query);
        } catch (SQLiteException e) {

        }
    }

    /**
     * Delete old logs
     *
     * @author Karthick
     * @created on 20161115
     */
    public void deleteOldVisitor() {
        try {
            database = this.getWritableDatabase();
            String query = DataBaseConstants.kConstantsDeleteVisitorQuery;
            database.execSQL(query);
        } catch (SQLiteException e) {

        }

    }

    /**
     * Insert internal logs Details into the local database.
     *
     * @author Karthick
     * @created 20160531
     */
    public long insertCheckInCheckOutDetails(EmployeeCheckInOut employeeTemporaryDetails) {
//        Log.d("DSK "," CheckInTimeDate "+ employeeTemporaryDetails.getCheckInTimeDate());
        long sucesscode = -1;
        try {
            database = this.getWritableDatabase();
            if (employeeTemporaryDetails != null) {
                ContentValues values = new ContentValues();
                values.put("EmployeeID", employeeTemporaryDetails.getEmployeeID());
                values.put("Name", employeeTemporaryDetails.getEmployeeName());
                values.put("EmailID", employeeTemporaryDetails.getEmployeeEmailID());
                values.put("CheckInTime", employeeTemporaryDetails.getCheckInTime());
                values.put("CheckOutTime", employeeTemporaryDetails.getCheckOutTime());
                values.put("CheckInTimeDate", employeeTemporaryDetails.getCheckInTimeDate());
                values.put("CheckOutTimeDate", employeeTemporaryDetails.getCheckOutTimeDate());
                values.put("OfficeLocation", employeeTemporaryDetails.getOfficeLocation());
                values.put("TemporaryID", employeeTemporaryDetails.getTemporaryID());
                values.put("SyncedWithCreator", employeeTemporaryDetails.getIsSynced());
//                values.put("CheckOutTime",0);
                sucesscode = database.insert("employee_forget_id", null, values);
//              sucesscode =1;
            }
            //database.close();
        } catch (SQLiteException e) {
//            Log.d("DbData", "Sql lite Exception");
            sucesscode = -1;
        }
        return sucesscode;
    }

    /**
     * fetch Employee CheckIn CheckoutDetails from the local database.
     *
     * @author Karthikeyan D S
     * @created 20180406
     */
    public ArrayList<EmployeeCheckInOut> getEmployeeCheckInCheckoutDetails(String employeeID) {
//         Log.d("RecordCount","dsk "+employeeID);
        ArrayList<EmployeeCheckInOut> CheckInCheckOutDetails = new ArrayList<EmployeeCheckInOut>();
        String selectQuery = "Select * from employee_forget_id where EmployeeID is '" + employeeID + "' ORDER BY CheckInTime DESC Limit 1";
        try {
            database = this.getReadableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {

                EmployeeCheckInOut checkInCheckOut = cursorToEmployeeCheckInDetails(cursor);
                CheckInCheckOutDetails.add(checkInCheckOut);
                cursor.moveToNext();
            }
            cursor.close();
            return CheckInCheckOutDetails;
        } catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Employee Check-In Check-Out Details : "+dbLockedException.getLocalizedMessage());
        }
        return CheckInCheckOutDetails;
    }

    private EmployeeCheckInOut cursorToEmployeeCheckInDetails(Cursor cursor) {

        EmployeeCheckInOut checkInCheckOutRecord = new EmployeeCheckInOut();

        checkInCheckOutRecord.setRecordId(cursor.getInt(0));
        checkInCheckOutRecord.setEmployeeID(cursor.getString(1));
        checkInCheckOutRecord.setEmployeeName(cursor.getString(2));
        checkInCheckOutRecord.setEmployeeEmailID(cursor.getString(3));
        checkInCheckOutRecord.setCheckInTime(cursor.getLong(4));
        checkInCheckOutRecord.setCheckOutTime(cursor.getLong(5));
        checkInCheckOutRecord.setOfficeLocation(cursor.getString(6));
        checkInCheckOutRecord.setTemporaryID(cursor.getString(7));
//        checkInCheckOutRecord.setIsSynced(cursor.getInt(8));

        // Log.d("purpose", "" + cursor.getString(MainConstants.kConstantZero));
        return checkInCheckOutRecord;
    }

    /**
     * Update Employee CheckIn CheckoutDetails from the local database.
     *
     * @author Karthikeyan D S
     * @created 20180406
     */
    public int updateEmployeeDetails(int recordID, long CheckOutTimeLocal,String CheckOutTimeLocalDate) {
        int updateCheckInCheckOutCode = 0;
        EmployeeCheckInOut EmployeeForgetIDDetails = null;
//        Log.d("DSK "," CheckOutTimeLocal "+ CheckOutTimeLocal+" CheckOutTimeLocalDate "+CheckOutTimeLocalDate);
        try {
            database = this.getWritableDatabase();
            String updateQuery = "Update employee_forget_id set CheckOutTime = " + CheckOutTimeLocal + " , CheckOutTimeDate = '"+ CheckOutTimeLocalDate +"' where ID is " + recordID;
//            Log.d("DSK "," updateQuery "+ updateQuery);
            database.execSQL(updateQuery);
            updateCheckInCheckOutCode = recordID;
        } catch (SQLiteDatabaseLockedException e) {
            updateCheckInCheckOutCode = 0;
        }
        return updateCheckInCheckOutCode;
    }

    /**
     * Get Record Count From the Table.
     *
     * @author Karthikeyan D S
     * @created 20180406
     */
    public int getEmployeeCheckInCheckOut() {
        int employeeCheckInCheckOut = 0;
        String selectQuery = "Select * from employee_forget_id where EmployeeID is Not Null and ID is Not Null";

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        employeeCheckInCheckOut = cursor.getCount() + 1;
        cursor.moveToFirst();
//        while (cursor.moveToNext()) {
//            employeeCheckInCheckOut += cursor.getCount();
////            cursor.moveToNext();
//        }
//        Log.d("RecordCount",""+employeeCheckInCheckOut);
        cursor.close();

        return employeeCheckInCheckOut;
    }

    /**
     * Get Record Count From the Table.
     *
     * @author Karthikeyan D S
     * @created 20180406
     */
    public ArrayList<EmployeeCheckInOut> getEmployeeCheckInCheckOutDetails(long recordID) {
        ArrayList<EmployeeCheckInOut> employeeCheckInCheckOut = new ArrayList<EmployeeCheckInOut>();
        String selectQuery = "Select * from employee_forget_id where EmployeeID is Not Null and ID is " + recordID;

        database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            EmployeeCheckInOut checkInCheckOut = cursorToEmployeeCheckInCheckOutDetails(cursor);
            employeeCheckInCheckOut.add(checkInCheckOut);
            cursor.moveToNext();
        }
        cursor.close();
        return employeeCheckInCheckOut;
    }

    private EmployeeCheckInOut cursorToEmployeeCheckInCheckOutDetails(Cursor cursor) {

        EmployeeCheckInOut checkInCheckOutRecord = new EmployeeCheckInOut();

        checkInCheckOutRecord.setRecordId(cursor.getInt(0));
        checkInCheckOutRecord.setEmployeeID(cursor.getString(1));
        checkInCheckOutRecord.setEmployeeName(cursor.getString(2));
        checkInCheckOutRecord.setEmployeeEmailID(cursor.getString(3));
        checkInCheckOutRecord.setCheckInTime(cursor.getLong(4));
        checkInCheckOutRecord.setCheckOutTime(cursor.getLong(5));
        checkInCheckOutRecord.setCheckInTimeDate(cursor.getString(6));
        checkInCheckOutRecord.setCheckOutTimeDate(cursor.getString(7));
        checkInCheckOutRecord.setOfficeLocation(cursor.getString(8));
        checkInCheckOutRecord.setTemporaryID(cursor.getString(9));
//        checkInCheckOutRecord.setIsSynced(cursor.getInt(8));
        // Log.d("purpose", "" + cursor.getString(MainConstants.kConstantZero));
        return checkInCheckOutRecord;
    }


    /**
     * Update Employee CheckIn CheckoutDetails from the local database.
     *
     * @author Karthikeyan D S
     * @created 20180406
     */
    public int updateDataCenterEmployeeDetails(ArrayList<String> dataCenterEmployeeList) {
//        Log.d("DSKdataCenterEmployee",""+dataCenterEmployeeList.size());
        int updateCheckInCheckOutCode = 0;
        String employeeFilterEmailIDs = dataCenterEmployeeList.toString();
//        Log.d("DSK ",""+employeeFilterEmailIDs.toString());
        employeeFilterEmailIDs = employeeFilterEmailIDs.replace("[", "(");
        employeeFilterEmailIDs = employeeFilterEmailIDs.replace("]", ")");
        EmployeeCheckInOut EmployeeForgetIDDetails = null;
        try {
            database = this.getWritableDatabase();
            String updateQuery = " Update " + DataBaseConstants.tableNameEmployee + " set isDataCenterEmployee = '1' " + " where EmailID in " + employeeFilterEmailIDs;
            database.execSQL(updateQuery);
//            Log.d("DSKdataCenterEmpquery",""+updateQuery);
            updateCheckInCheckOutCode = MainConstants.kConstantOne;
        } catch (Exception e) {
//            Log.d("DSKdataCenterEmperror",""+e.getLocalizedMessage());
            updateCheckInCheckOutCode = 0;
        }
        return updateCheckInCheckOutCode;
    }

    /**
     * Get the DataCenter employee details depends on Is DataCenter Employee as 1
     *
     * @return array list of employeeRecords
     * @author Karthikeyan D S
     * @created 06/06/2018
     */
    public ArrayList<String> getDataCenterEmployeeEmail() {
        ArrayList<String> listOfEmployeesEmail = new ArrayList<String>();
        try {
            database = this.getReadableDatabase();
            String selectQuery = DataBaseConstants.kConstantsQuerySelect
                    + DataBaseConstants.dbEmployeeEmailId
                    + DataBaseConstants.kConstantsQueryFrom
                    + DataBaseConstants.tableNameEmployee
                    + " WHERE "
                    + DataBaseConstants.dbIsDataCenterEmployee
                    + DataBaseConstants.kConstantEqual
                    + MainConstants.kConstantOne;
            Cursor cursor = database.rawQuery(selectQuery, null);
//            Log.d("DSKDataCenterquery",""+selectQuery);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String employeeRecord = cursorToEmployeeEmail(cursor);
                // employeeRecord.getEmployeeVisitFrequency());
                if (employeeRecord != null && !employeeRecord.isEmpty()) {
                    listOfEmployeesEmail.add(employeeRecord);
                }
                cursor.moveToNext();
            }
            cursor.close();
//			database.close();
        } catch (SQLiteException e) {
//             Log.d("DSKDataCenterquery", "SQLiteException");
        }
//        Log.d("DSKDataCenterquery",""+listOfEmployeesEmail.size());
        return listOfEmployeesEmail;
    }

    private String cursorToEmployeeEmail(Cursor cursor) {
        String employee = "";
        if (cursor != null && cursor.getCount() >= 0) {
            employee = cursor.getString(MainConstants.kConstantZero);
        }
        return employee;
    }

    /**
     * Get the DataCenter Employee Visitor PreApproval Data.
     *
     * @return recordId for inserted data to assign the
     * @author Karthikeyan D S
     * @created 30JUN2018
     */
    public long insertEmployeeVisitorPreApprovalRequest(EmployeeVisitorPreApproval employeeVisitorPreApproval) {
        EmployeeVisitorPreApproval employeeVisitorPreApprovalData = new EmployeeVisitorPreApproval();
        long mrecordID = NumberConstants.kConstantMinusOne;
        try {
            database = this.getWritableDatabase();
            if (employeeVisitorPreApproval != null) {
                ContentValues values = new ContentValues();
                values.put(DataBaseConstants.dbColumnNamePreApprovalDetailsCreatorId, employeeVisitorPreApproval.getmCreatorId());
                values.put(DataBaseConstants.dbColumnNamePreApprovalVisitorName, employeeVisitorPreApproval.getmVisitorName());
                values.put(DataBaseConstants.dbColumnNamePreApprovalVisitorCompanyName, employeeVisitorPreApproval.getmVisitorCompanyName());
                values.put(DataBaseConstants.dbColumnNamePreApprovalDateofVisit, employeeVisitorPreApproval.getmDateofVisit());
                values.put(DataBaseConstants.dbColumnNamePreApprovalDateofVisitLong, employeeVisitorPreApproval.getmDateofVisitlong());
                values.put(DataBaseConstants.dbColumnNamePreApprovalVisitorPhoneNumber, employeeVisitorPreApproval.getmPhoneNumber());
                values.put(DataBaseConstants.dbColumnNamePreApprovalEmailId, employeeVisitorPreApproval.getmEmployeeEmailId());
                values.put(DataBaseConstants.dbColumnNamePreApprovalisVisitorActive, employeeVisitorPreApproval.isVisitorActive());

                mrecordID = database.insert(DataBaseConstants.dbtableNamePreApproval, null, values);

                return mrecordID;
            }
        }catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Insert Visitor PreApproval Request : "+dbLockedException.getLocalizedMessage());
        }
        return -1;
    }

    /**
     * Update DataCenter Employee Visitor PreApproval records as false/true based on input.
     *
     * @author Karthikeyan D S
     * @created 02JUl2018
     */
    public void updateEmployeeVisitorPreApprovalRequestIsActive(boolean isVisitorActive) {
//        long updateCode = NumberConstants.kConstantMinusOne;
        try {
            database = this.getWritableDatabase();
            String updateQuery = " Update " + DataBaseConstants.dbtableNamePreApproval + " set isVisitorActive = " + isVisitorActive + " where CreatorId is NOT NULL";
            database.execSQL(updateQuery);
//            updateCode =MainConstants.kConstantOne;
        } catch (Exception e) {
//            updateCode=0;
        }

    }

    /**
     * Update DataCenter Employee Multiple Visitor PreApproval records as false/true based on input.
     *
     * @author Karthikeyan D S
     * @created 02JUl2018
     */
    public void updateEmployeeMultiVisitorPreApprovalRequestIsActive(boolean isVisitorActive) {
//        long updateCode = NumberConstants.kConstantMinusOne;
        try {
            database = this.getWritableDatabase();
            String updateQuery = " Update " + DataBaseConstants.dbtableNamePreApprovalMultiPersonDetail + " set isVisitorActive = " + isVisitorActive + " where CreatorId is NOT NULL";
            database.execSQL(updateQuery);
//            updateCode =MainConstants.kConstantOne;
        } catch (Exception e) {
//            updateCode=0;
        }
    }

    /**
     * Update DataCenter Employee Visitor PreApproval records with given input's based on the Local Table record id.
     *
     * @return updated Table record id.
     * @author Karthikeyan D S
     * @created 02JUl2018
     */
    public long updateEmployeeVisitorPreApprovalRequest(EmployeeVisitorPreApproval employeeVisitorPreApproval, long mPreApprovalDetailsRecordId) {
        long mRecordId = NumberConstants.kConstantMinusOne;
        try {
            database = this.getWritableDatabase();
            String updateQuery = " Update " + DataBaseConstants.dbtableNamePreApproval + " set isVisitorActive = " + employeeVisitorPreApproval.isVisitorActive() +
                    " AND VisitorName = " + employeeVisitorPreApproval.getmVisitorName() +
                    " AND PhoneNumber = " + employeeVisitorPreApproval.getmPhoneNumber() + " WHERE RecordId =" + mPreApprovalDetailsRecordId;
            database.execSQL(updateQuery);
//            updateCode =MainConstants.kConstantOne;
        } catch (Exception e) {
//            updateCode=0;
        }
        return mRecordId;
    }

    /**
     * get preApproval record id for given matched conditions
     *
     * @return Table record id.
     * @author Karthikeyan D S
     * @created 02JUl2018
     */
    public long mPreApprovalDetailsRecordId(EmployeeVisitorPreApproval employeeVisitorPreApproval) {
        long mpreApprovalDetailsRecordId = NumberConstants.kConstantMinusOne;
        if (employeeVisitorPreApproval != null) {
            String selectQuery = "SELECT RecordId FROM PreApprovalData WHERE RecordId IS NOT NULL AND PhoneNumber is " + employeeVisitorPreApproval.getmPhoneNumber() + " AND VisitorName LIKE '" + employeeVisitorPreApproval.getmVisitorName()
                    + "' AND  DateOfVisitlong = " + employeeVisitorPreApproval.getmDateofVisitlong();
            try {
                if (database != null) {
                    database = this.getReadableDatabase();
                    Cursor cursor = database.rawQuery(selectQuery, null);
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        mpreApprovalDetailsRecordId = cursorToEmployeePreApprovalDetails(cursor);
                        cursor.moveToNext();
                    }
                    cursor.close();
                }
            } catch (SQLiteDatabaseLockedException dbLockedException) {
                setInternalLogs(context, ErrorConstants.dataBaseLockedException,"PreApproval Details : "+dbLockedException.getLocalizedMessage());
            }
        }
        return mpreApprovalDetailsRecordId;
    }

    /**
     * cursor for selected table record to get record id
     *
     * @return Table record id.
     * @author Karthikeyan D S
     * @created 02JUl2018
     */
    private long cursorToEmployeePreApprovalDetails(Cursor cursor) {
        long mEmployeePreApprovalDetailsRecord = NumberConstants.kConstantMinusOne;
        mEmployeePreApprovalDetailsRecord = cursor.getInt(0);
//        Log.d("mRecordId", "mEmployeePreApprovalDetailsRecord " + cursor.getInt(0));
        return mEmployeePreApprovalDetailsRecord;
    }

    /**
     * Search DataCenter PreApproval Multiple visitor details using contact and today Date as long value
     *
     * @author Karthikeyan D S
     * @created on 02Jul2018
     */
    public EmployeeVisitorPreApproval searchEmployeeVisitorPreApprovalRecords(String contact, long mDateOfVisitToday) {
        EmployeeVisitorPreApproval employeeVisitorPreApproval = null;
        if (contact != null && !contact.isEmpty() && mDateOfVisitToday > 0) {
            try {
                database = this.getReadableDatabase();
                String selectQuery = DataBaseConstants.kConstantsSelectQuery
                        + DataBaseConstants.dbtableNamePreApproval
                        + DataBaseConstants.kConstantsWhere
                        + DataBaseConstants.dbColumnNamePreApprovalVisitorPhoneNumber
                        + DataBaseConstants.kConstantsLikeStatementWithSingleQuote
                        + "+91"
                        + contact
                        + DataBaseConstants.kConstantsSingleQuote + " AND " + DataBaseConstants.dbColumnNamePreApprovalDateofVisitLong
                        + " ='" + mDateOfVisitToday + "'";
                Cursor cursor = database.rawQuery(selectQuery, null);
//            Log.d("DSKselectQuery",""+selectQuery);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    employeeVisitorPreApproval = cursorToEmployeeVisitorPreApproval(cursor);
                    cursor.moveToNext();
                }
                cursor.close();
//            Log.d("DSK",""+cursor.getCount());
            } catch (SQLiteReadOnlyDatabaseException e) {
                setInternalLogs(context, ErrorConstants.dataBaseReadOnlyException,e.getLocalizedMessage());

//            Log.d("DSK",""+e.getLocalizedMessage());
            } catch (SQLiteDatabaseLockedException dbLockedException) {
                setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Employee PreApproved Visitors: "+dbLockedException.getLocalizedMessage());
            }
//        Log.d("DSK",""+existingVisitorDetails);
        }
        return employeeVisitorPreApproval;
    }

    /**
     * Read cursor value and set to ExistingVisitorDetails variables
     *
     * @author Karthikeyan D S
     * @created on 02Jul2018
     */
    private EmployeeVisitorPreApproval cursorToEmployeeVisitorPreApproval(Cursor cursor) {

        EmployeeVisitorPreApproval employeeVisitorPreApproval = new EmployeeVisitorPreApproval();
        if (cursor != null) {
            employeeVisitorPreApproval.setmRecordId(cursor.getLong(MainConstants.kConstantZero));
            employeeVisitorPreApproval.setmCreatorId(cursor.getString(MainConstants.kConstantOne));
            employeeVisitorPreApproval.setmVisitorName(cursor.getString(MainConstants.kConstantTwo));
            employeeVisitorPreApproval.setmVisitorCompanyName(cursor.getString(MainConstants.kConstantThree));
            employeeVisitorPreApproval.setmDateofVisit(cursor.getString(MainConstants.kConstantFour));
            employeeVisitorPreApproval.setmDateofVisitlong(cursor.getLong(MainConstants.kConstantFive));
            employeeVisitorPreApproval.setmPhoneNumber(cursor.getString(MainConstants.kConstantSix));
            if (cursor.getInt(MainConstants.kConstantSeven) == 1) {
                employeeVisitorPreApproval.setVisitorActive(true);
            } else {
                employeeVisitorPreApproval.setVisitorActive(false);
            }
            employeeVisitorPreApproval.setmEmployeeEmailId(cursor.getString(MainConstants.kConstantEight));
        }
        return employeeVisitorPreApproval;
    }

    /**
     * Fetch employee NotCheckDetails For Given Email ID
     *
     * @param employeeEmailID
     * @return
     *
     * @author Karthikeyan D S
     * @createdon 26Sep2018
     */
    public long getEmployeeForgetIDNotCheckOut(String employeeEmailID,long employeeCheckInTimeLong)
    {
        long recordId = NumberConstants.kConstantMinusOne;
        try {
            database = this.getReadableDatabase();
            String selectQuery = "Select ID From " + DataBaseConstants.tableNameEmployeeForgetID + " WHERE "
                    + DataBaseConstants.dbEmployeeEmailId + " COLLATE NOCASE = '" + employeeEmailID+"' AND "+DataBaseConstants.dbEmployeeCheckInTime +" = '"
                     +employeeCheckInTimeLong+"' AND "+ DataBaseConstants.dbEmployeeCheckOutTime +" = '-1' ";

//            Log.d("DSK "," "+selectQuery);
            Cursor cursor = database.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                recordId = cursorToEmployeeForgetIDNotCheckOut(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }catch (SQLiteDatabaseLockedException dbLockedException) {
            setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Get Employee Forget ID Not Check-Out Details : "+dbLockedException.getLocalizedMessage());
        }

        return recordId;
    }

    private long cursorToEmployeeForgetIDNotCheckOut(Cursor cursor)
    {
      long recordID = NumberConstants.kConstantMinusOne;
      if(cursor!=null) {
          recordID = cursor.getLong(NumberConstants.kConstantZero);
      }
      return recordID;
    }

    /**
     * update Employee Forget ID Details to local Table DB sqlite
     *
     * @created On 26Sep2018
     * @author Karthikeyan D S
     */
    public void updateEmployeeForgetIDCheckOutDetails(long recordID,EmployeeCheckInOut employeeCheckInOut)
    {
        if(employeeCheckInOut!=null && employeeCheckInOut.getCheckInTimeDate()!=null&& employeeCheckInOut.getCheckInTimeDate()!="") {
            try {
                database = this.getWritableDatabase();
                String updateQuery = " UPDATE " + DataBaseConstants.tableNameEmployeeForgetID + " SET " + DataBaseConstants.dbEmployeeCheckInTimeDate
                        + " = '" + employeeCheckInOut.getCheckInTimeDate() + "' , "
                        +DataBaseConstants.dbEmployeeCheckInTime  + " = '" + employeeCheckInOut.getCheckInTime()
                        +"' , " + DataBaseConstants.dbEmployeeCheckOutTimeDate
                        + " = '" + employeeCheckInOut.getCheckOutTimeDate() + "' , "
                        +DataBaseConstants.dbEmployeeCheckOutTime  + " = '" + employeeCheckInOut.getCheckOutTime() +"' WHERE ID = " + recordID;
//               Log.d("DSK "," updateQuery "+updateQuery);
                database.execSQL(updateQuery);
            }catch (SQLiteDatabaseLockedException dbLockedException) {
                setInternalLogs(context, ErrorConstants.dataBaseLockedException,"Update Employee Forget ID Not Check-Out Details : "+dbLockedException.getLocalizedMessage());
            }
        }
    }

    /**
     * insert Employee Forget ID Details to local Table DB sqlite
     *
     * @created On 26Sep2018
     * @author Karthikeyan D S
     */
    public void insertEmployeeForgetIDCheckOutDetails(EmployeeCheckInOut employeeCheckInOut)
    {
        if(employeeCheckInOut!=null && employeeCheckInOut.getCheckOutTimeDate()!=null
                && employeeCheckInOut.getCheckOutTimeDate()!="") {
            ContentValues values = new ContentValues();

            values.put(DataBaseConstants.dbEmployeeId,
                    employeeCheckInOut.getEmployeeID());
            values.put(DataBaseConstants.dbEmployeeName,
                    employeeCheckInOut.getEmployeeName());
            values.put(DataBaseConstants.dbEmployeeEmailId,
                    employeeCheckInOut.getEmployeeEmailID());
            values.put(DataBaseConstants.dbEmployeeCheckInTime,
                    employeeCheckInOut.getCheckInTime());
            values.put(DataBaseConstants.dbEmployeeCheckInTimeDate,
                    employeeCheckInOut.getCheckInTimeDate());
            values.put(DataBaseConstants.dbEmployeeCheckOutTime,
                    employeeCheckInOut.getCheckOutTime());
            values.put(DataBaseConstants.dbEmployeeCheckOutTimeDate,
                    employeeCheckInOut.getCheckOutTimeDate());
            values.put(DataBaseConstants.dbEmployeeOfficeLocation,
                    employeeCheckInOut.getOfficeLocation());
            values.put(DataBaseConstants.dbEmployeeTemporaryID,
                    employeeCheckInOut.getTemporaryID());
            try {
                database = this.getWritableDatabase();
                database.insert(DataBaseConstants.tableNameEmployeeForgetID, null, values);
            } catch (SQLiteDatabaseLockedException dbLockedException) {
                setInternalLogs(context, ErrorConstants.dataBaseLockedException, "Insert Employee Forget ID Not Check-Out Details through update: " + dbLockedException.getLocalizedMessage());
            }
        }
    }
    /**
     * set internal log and insert to local sqlite
     *
     * @created On 26Sep2018
     * @author Karthikeyan D S
     */
    public void setInternalLogs(Context context, int errorCode, String errorMessage) {
        if (context != null && errorMessage != null && !errorMessage.isEmpty()) {
            // SingletonErrorMessage.getInstance().setErrorMessage(errorMessage);
            NetworkConnection networkConnection = new NetworkConnection();
            InternalAppLogs internalAppLogs = new InternalAppLogs();
            internalAppLogs.setErrorCode(errorCode);
            internalAppLogs.setErrorMessage(errorMessage);
            internalAppLogs.setLogDate(new Date().getTime());
            internalAppLogs.setWifiSignalStrength(networkConnection.getCurrentWifiSignalStrength(context));
            try {
                DbHelper.getInstance(context).insertLogs(internalAppLogs);
            } catch (SQLiteDatabaseLockedException e1) {

            }
        }
    }
}
