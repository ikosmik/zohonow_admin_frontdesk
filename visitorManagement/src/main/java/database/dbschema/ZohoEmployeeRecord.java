/*
 * Created By Jayapriya on 29/09/14
 */
package database.dbschema;


/**
 *@Modified Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 */
public class ZohoEmployeeRecord {
	
	private String employeeZuid;
	private String employeeRecordId;
	private String employeeStatus;
	private String employeeSeatingLocation;
	private String employeeExtentionNumber;
	private String employeePhoto;
	private String employeeId;
	private String employeeName;
	private String employeeFirstName;
	private String employeeLastName;
	private String employeeMobileNumber;
	private String employeeEmailId;
	private String employeeDept;
	private long employeeVisitFrequency;


	/**
	 * for DataCenter Employee Details added column is Datacenter Employee True/False.
	 *  Modified by Karthikeyan D S
	 *  on 06062018
	 */
	private boolean isDataCenterEmployee;


	public boolean isDataCenterEmployee() {
		return isDataCenterEmployee;
	}

	public void setDataCenterEmployee(boolean dataCenterEmployee) {
		isDataCenterEmployee = dataCenterEmployee;
	}
	
    public String getEmployeeEmailId(){
    	return employeeEmailId;
    }
    public void setEmployeeEmailId(String employeeEmailId){
    	this.employeeEmailId =employeeEmailId;
    }
    public  String getEmployeeMobileNumber(){
		return employeeMobileNumber;
    	
    }
    public void setEmployeeMobileNumber(String employeeMobileNumber){
    	this.employeeMobileNumber = employeeMobileNumber;
    }
    public String getEmployeeId(){
		return employeeId;
    	
    }
    public void setEmployeeId(String employeeId){
    	this.employeeId = employeeId;
    }
	public String getEmployeeZuid() {
		return employeeZuid;
	}
	public void setEmployeeZuid(String employeeZuid) {
		this.employeeZuid = employeeZuid;
	}
	public String getEmployeeRecordId() {
		return employeeRecordId;
	}
	public void setEmployeeRecordId(String employeeRecordId) {
		this.employeeRecordId = employeeRecordId;
	}
	public String getEmployeeStatus() {
		return employeeStatus;
	}
	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}
	public String getEmployeeSeatingLocation() {
		return employeeSeatingLocation;
	}
	public void setEmployeeSeatingLocation(String employeeSeatingLocation) {
		this.employeeSeatingLocation = employeeSeatingLocation;
	}
	public String getEmployeeExtentionNumber() {
		return employeeExtentionNumber;
	}
	public void setEmployeeExtentionNumber(String employeeExtentionNumber) {
		this.employeeExtentionNumber = employeeExtentionNumber;
	}
	public String getEmployeePhoto() {
		return employeePhoto;
	}
	public void setEmployeePhoto(String employeePhoto) {
		this.employeePhoto = employeePhoto;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName.replaceAll("[^\\x1F-\\x7F]+","").trim();
	}
	public String getEmployeeLastName() {
		return employeeLastName;
	}
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName.replaceAll("[^\\x1F-\\x7F]+","").trim();
	}
	public String getEmployeeName() {

		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
	//here filtering the test names that are created in people
		this.employeeName = employeeName.replaceAll("[^\\x1F-\\x7F]+","").trim();
	}
	public long getEmployeeVisitFrequency() {
		return employeeVisitFrequency;
	}
	public void setEmployeeVisitFrequency(long employeeVisitFrequency) {
		this.employeeVisitFrequency = employeeVisitFrequency;
	}
	public String getEmployeeDept() {
		return employeeDept;
	}
	public void setEmployeeDept(String employeeDept) {
		this.employeeDept = employeeDept;
	}
}
