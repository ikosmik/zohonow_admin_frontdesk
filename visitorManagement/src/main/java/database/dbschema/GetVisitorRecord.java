package database.dbschema;
/**
 *  Created by Jayapriya 
 *  Date 19/04/14
 *
 *@Modified Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 */
public class GetVisitorRecord {
	public String recordId;
    public String name;
    public String country;
    public int frequency;
    public int position;
    public String name_country_position;
    public String getRecordId(){
		return recordId;
    	
    }
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getName_country_position() {
		return name_country_position;
	}
	public void setName_country_position(String name_country_position) {
		this.name_country_position = name_country_position;
	}
    
    
	
}
