package database.dbschema;


/**
 *
 * @author jayapriya
 * @created
 *
 * @Modified Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 */
public class PurposeOfVisitRecord {
	
	public String purpose;
    public Integer visitFrequency;
    public Integer location;
    
   
    public  String getPurpose(){
		return purpose;
    	
    }
    public void setPurpose(String data){
    	  purpose = data;   
    }
    public Integer getVisitFrequency(){
    	  return visitFrequency;
    }
    public void setVisitFrequency(Integer count){
    	  visitFrequency = count;
    }
    public Integer getLocation(){
    	  return location;

    }
    public void setLocation(Integer value){
    	     location = value;
    }
    
}
