package database.dbschema;

/**
 * @Created by Karthikeyan D S on 30JUN2018
 */
public class EmployeeVisitorPreApproval {

    private long mRecordId;
    private String mCreatorId;
    private String mVisitorName;
    private String mVisitorCompanyName;
    private String mDateofVisit;
    private long mDateofVisitlong;
    private String mPhoneNumber;
    private String mEmployeeEmailId;
    private boolean isVisitorActive;

    private boolean isVisitorPreApproved;

    public boolean isVisitorPreApproved() {
        return isVisitorPreApproved;
    }

    public void setVisitorPreApproved(boolean visitorPreApproved) {
        isVisitorPreApproved = visitorPreApproved;
    }

    public long getmRecordId() {
        return mRecordId;
    }

    public void setmRecordId(long mRecordId) {
        this.mRecordId = mRecordId;
    }

    public String getmCreatorId() {
        return mCreatorId;
    }

    public void setmCreatorId(String mCreatorId) {
        this.mCreatorId = mCreatorId;
    }

    public String getmVisitorName() {
        return mVisitorName;
    }

    public void setmVisitorName(String mVisitorName) {
        this.mVisitorName = mVisitorName;
    }

    public String getmVisitorCompanyName() {
        return mVisitorCompanyName;
    }

    public void setmVisitorCompanyName(String mVisitorCompanyName) {
        this.mVisitorCompanyName = mVisitorCompanyName;
    }

    public String getmDateofVisit() {
        return mDateofVisit;
    }

    public void setmDateofVisit(String mDateofVisit) {
        this.mDateofVisit = mDateofVisit;
    }

    public long getmDateofVisitlong() {
        return mDateofVisitlong;
    }

    public void setmDateofVisitlong(long mDateofVisitlong) {
        this.mDateofVisitlong = mDateofVisitlong;
    }

    public String getmPhoneNumber() {
        return mPhoneNumber;
    }

    public void setmPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public String getmEmployeeEmailId() {
        return mEmployeeEmailId;
    }

    public void setmEmployeeEmailId(String mEmployeeEmailId) {
        this.mEmployeeEmailId = mEmployeeEmailId;
    }

    public boolean isVisitorActive() {
        return isVisitorActive;
    }

    public void setVisitorActive(boolean visitorActive) {
        isVisitorActive = visitorActive;
    }
}
