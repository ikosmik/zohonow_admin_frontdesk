package database.dbschema;

/**
 *
 * @Modified by Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 */
public class Stationery {
	
	private long stationeryAddedTime;
	private String stationeryName;
	private int stationeryCode;
	private int limitPerMonth;
	//private String stationeryImage;
	private int stationeryLocationCode;
	private int dirtyBit;
	private int quantity;
	private long creatorId;

	private long recordId;

//	Added by Karthikeyan D S
	private String officeDeviceLocation;
	private String officeParentLocation;

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getStationeryName() {
		return stationeryName;
	}
	public void setStationeryName(String stationeryName) {
		this.stationeryName = stationeryName;
	}
	public int getStationeryCode() {
		return stationeryCode;
	}
	public void setStationeryCode(int stationeryCode) {
		this.stationeryCode = stationeryCode;
	}
	public int getLimitPerMonth() {
		return limitPerMonth;
	}
	public void setLimitPerMonth(int limitPerMonth) {
		this.limitPerMonth = limitPerMonth;
	}
	public long getStationeryAddedTime() {
		return stationeryAddedTime;
	}
	public void setStationeryAddedTime(long stationeryAddedTime) {
		this.stationeryAddedTime = stationeryAddedTime;
	}
	public int getDirtyBit() {
		return dirtyBit;
	}
	public void setDirtyBit(int dirtyBit) {
		this.dirtyBit = dirtyBit;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}
	public int getStationeryLocationCode() {
		return stationeryLocationCode;
	}

	public void setStationeryLocationCode(int stationeryLocationCode) {
		this.stationeryLocationCode = stationeryLocationCode;
	}

	public String getOfficeDeviceLocation() {
		return officeDeviceLocation;
	}

	public void setOfficeDeviceLocation(String officeDeviceLocation) {
		this.officeDeviceLocation = officeDeviceLocation;
	}

	public String getOfficeParentLocation() {
		return officeParentLocation;
	}

	public void setOfficeParentLocation(String officeParentLocation) {
		this.officeParentLocation = officeParentLocation;
	}
}
