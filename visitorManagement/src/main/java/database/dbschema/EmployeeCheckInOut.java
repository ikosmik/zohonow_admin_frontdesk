package database.dbschema;

/**
 * @Modified Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 *
 */
public class EmployeeCheckInOut {

    public String employeeID;
    public String employeeZUID;
    public String EmployeeName;
    public String EmployeeEmailID;
    public String OfficeLocation;
    public String deviceId;
    public String isSynced;
    public String TemporaryID;
    public String checkInTimeDate;
    public String checkOutTimeDate;
    public long checkInTime;
    public long checkOutTime;
    public int recordId;

    public String getCheckInTimeDate() {
        return checkInTimeDate;
    }

    public void setCheckInTimeDate(String checkInTimeDate) {
        this.checkInTimeDate = checkInTimeDate;
    }

    public String getCheckOutTimeDate() {
        return checkOutTimeDate;
    }

    public void setCheckOutTimeDate(String checkOutTimeDate) {
        this.checkOutTimeDate = checkOutTimeDate;
    }

    public String getEmployeeZUID() {
        return employeeZUID;
    }

    public void setEmployeeZUID(String employeeZUID) {
        this.employeeZUID = employeeZUID;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public long getCheckOutTime() {
        return checkOutTime;
    }
    public void setCheckOutTime(long checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getIsSynced() {
        return isSynced;
    }

    public void setIsSynced(String isSynced) {
        this.isSynced = isSynced;
    }
    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public String getEmployeeEmailID() {
        return EmployeeEmailID;
    }

    public void setEmployeeEmailID(String employeeEmailID) {
        EmployeeEmailID = employeeEmailID;
    }

    public String getOfficeLocation() {
        return OfficeLocation;
    }

    public void setOfficeLocation(String officeLocation) {
        OfficeLocation = officeLocation;
    }

    public String getTemporaryID() {
        return TemporaryID;
    }

    public void setTemporaryID(String temporaryID) {
        TemporaryID = temporaryID;
    }

}
