package database.dbschema;

/**
 * @Modified Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 */
public class VisitorVisitLinkRecord {
	public Integer visitorLinkId;
	public Integer visitLinkId;
	
	public Integer getVisitLinkId(){
		return visitLinkId;
	}
	public void setVisitLinkId(Integer visitLinkId){
		this.visitLinkId = visitLinkId;
	}
	public Integer getVisitorLinkId(){
		return visitorLinkId;
	}
	public void setVisitorLinkId(Integer visitorLinkId){
		this.visitorLinkId = visitorLinkId;
	}
}
