package database.dbschema;

/**
 * @Modified by Karthikeyan D S to Location DBSCHEMA on 30JUN2018
 */
public class StationeryEntry {
	
	private long entryTime;
	private int quantity;
	private int stationeryStationeryItemCode;
	private String requestId;
	private long creatorId;
	private int dirtyBit;
	public long getEntryTime() {
		return entryTime;
	}
	public void setEntryTime(long entryTime) {
		this.entryTime = entryTime;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getStationeryItemCode() {
		return stationeryStationeryItemCode;
	}
	public void setStationeryItemCode(int stationeryStationeryItemCode) {
		this.stationeryStationeryItemCode = stationeryStationeryItemCode;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}
	public int getDirtyBit() {
		return dirtyBit;
	}
	public void setDirtyBit(int dirtyBit) {
		this.dirtyBit = dirtyBit;
	}

}
