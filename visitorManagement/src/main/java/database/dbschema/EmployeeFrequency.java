package database.dbschema;

/**
*
* @author karthick
* @created on 30/04/2015
 *
 * @Modified Karthikeyan D S to Location DBSCHEMA on 30JUN2018
*/
public class EmployeeFrequency {
	
		private String EmployeeID;
		private long visitFrequency;
		public String getEmployeeID() {
			return EmployeeID;
		}
		public void setEmployeeID(String employeeID) {
			EmployeeID = employeeID;
		}
		public long getVisitFrequency() {
			return visitFrequency;
		}
		public void setVisitFrequency(long visitFrequency) {
			this.visitFrequency = visitFrequency;
		}
}
