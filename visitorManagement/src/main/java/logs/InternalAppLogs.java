package logs;

public class InternalAppLogs {
	
	private String errorMessage;
	private int errorCode;
	private int wifiSignalStrength;
	private long logDate;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public int getWifiSignalStrength() {
		return wifiSignalStrength;
	}
	public void setWifiSignalStrength(int wifiSignalStrength) {
		this.wifiSignalStrength = wifiSignalStrength;
	}
	public long getLogDate() {
		return logDate;
	}
	public void setLogDate(long logDate) {
		this.logDate = logDate;
	}

}
