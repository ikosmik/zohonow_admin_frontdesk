package logs;

/**
 * Assign value to ActivityInternalLogs activity
 * 
 * @created on 20150921
 * @author karthick
 */
import java.util.List;
import com.zoho.app.frontdesk.android.R;
import constants.FontConstants;
import constants.MainConstants;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LogListAdapter extends ArrayAdapter<InternalAppLogs> {
	
	private Context context;
	public LogListAdapter(Context context, int resource,List<InternalAppLogs> items) {
		super(context, resource,items);
		this.context=context;
	}
	
	/*private view holder class*/
	public class ViewHolderNew{
        TextView wifiSignalStrength,Date,logErrorMsg,logErrorNumber;
        RelativeLayout LogDetails;
    }
	
	//set adaptor view properties
	int Position;
    public View getView(int position, View convertView, ViewGroup parent) {
    	Position=position;
    	ViewHolderNew holder;
        final InternalAppLogs internalAppLog = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.log_data_view, null);
            holder = new ViewHolderNew();
           
            holder.wifiSignalStrength = (TextView) convertView.findViewById(R.id.log_host_number);
            holder.Date=(TextView)convertView.findViewById(R.id.log_date);
            holder.logErrorNumber=(TextView)convertView.findViewById(R.id.log_errorNumber);
            holder.logErrorMsg=(TextView)convertView.findViewById(R.id.log_errorMessage);
            holder.LogDetails=(RelativeLayout)convertView.findViewById(R.id.logDataLayout);
	        //set font style
            Typeface fontLogMsg = Typeface.createFromAsset(
					context.getAssets(),
					FontConstants.fontConstantSourceSansProbold);
            holder.logErrorMsg.setTypeface(fontLogMsg);
			Typeface fontDialogMsg = Typeface.createFromAsset(
					context.getAssets(),
					FontConstants.fontConstantSourceSansProLight);
			holder.wifiSignalStrength.setTypeface(fontDialogMsg);
			holder.Date.setTypeface(fontDialogMsg);
			holder.logErrorNumber.setTypeface(fontDialogMsg);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderNew) convertView.getTag();
        }
        
        //Date date=new Date(internalAppLog.getLogDate());
        // SimpleDateFormat sdf = new SimpleDateFormat(MainConstants.kConstantLogPageDateFormat);
        //set wifi signal strength
        holder.wifiSignalStrength.setText(MainConstants.kConstantLogPageWifiSignalStrength + String.valueOf(internalAppLog.getWifiSignalStrength()) + MainConstants.kConstantLogPageWifidBm );
        holder.Date.setText(DateUtils.getRelativeDateTimeString(context, internalAppLog.getLogDate(), DateUtils.MINUTE_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, 0));
        
        //this if condition for visibility of error code textview
        if((internalAppLog.getErrorCode()>=MainConstants.kConstantZero)&&(internalAppLog.getErrorCode()<2000)) {
        holder.logErrorNumber.setText(MainConstants.kConstantLogPageErrorCode + internalAppLog.getErrorCode());
        holder.logErrorNumber.setVisibility(View.VISIBLE);
        }
        else if(internalAppLog.getErrorCode()>=2000) {
            holder.logErrorNumber.setText(MainConstants.kConstantLogPageCode + internalAppLog.getErrorCode());
            holder.logErrorNumber.setVisibility(View.VISIBLE);
            }
        else {
        	holder.logErrorNumber.setVisibility(View.GONE);
        }
        
        holder.logErrorMsg.setText(internalAppLog.getErrorMessage());
        ////Log.d("window", "position:"+position);
        //Change alternate row colors
        if(position%MainConstants.kConstantTwo==MainConstants.kConstantZero) {
        	holder.LogDetails.setBackgroundColor(Color.rgb(217,208,190));
        } else {
        	holder.LogDetails.setBackgroundColor(Color.rgb(224,218,207));
        }
        return convertView;
    }    
}
