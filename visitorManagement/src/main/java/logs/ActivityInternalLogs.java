package logs;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import utility.CustomDialog;
import com.zoho.app.frontdesk.android.R;
import constants.FontConstants;
import constants.MainConstants;
import database.dbhelper.DbHelper;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityInternalLogs extends Activity {

	public LogListAdapter adapter;
	private TextView noLogdata,textViewTitle;
	private DbHelper sqliteHelper;
	private ListView listviewLogs;
	private ImageView imageViewBack, imageViewClear,imageViewScreenShot;
	private CustomDialog customDialog;
	private Typeface fontTitle, fontButton, fontTextview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove the title bar
				this.requestWindowFeature(Window.FEATURE_NO_TITLE);
				// hide the status bar.
				getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
						WindowManager.LayoutParams.FLAG_FULLSCREEN);
				//getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				// set Portrait mode
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
//							setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
							getWindow()
									.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
						}
				setContentView(R.layout.activity_internal_logs);
				assignObject();
	}
	
	public void onResume() {
		super.onResume();

		ArrayList<InternalAppLogs> internalAppLogList = new ArrayList<InternalAppLogs>();
		sqliteHelper = DbHelper.getInstance(this);
		internalAppLogList.clear();
		internalAppLogList.addAll(sqliteHelper.getLogs());
		
		if (internalAppLogList.size() > 0) {
			noLogdata.setVisibility(View.INVISIBLE);
		} else {
			noLogdata.setVisibility(View.VISIBLE);
		}
		adapter = new LogListAdapter(this, R.layout.log_data_view,
				internalAppLogList);
		listviewLogs.setAdapter(adapter);
	}

	/*
	 * Assign objects Set fonts style and listeners
	 * 
	 * Created on 20150722
	 * Created by karthick
	 */
	private void assignObject() {
		fontTitle = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantCaptureIt);
		fontButton = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantSourceSansProSemibold);
		fontTextview = Typeface.createFromAsset(getAssets(),
				FontConstants.fontConstantSourceSansProSemibold);
		textViewTitle = (TextView) findViewById(R.id.textView_logs_title);
		listviewLogs = (ListView) findViewById(R.id.Log_Details_list);
		noLogdata = (TextView) findViewById(R.id.no_Log_data);
		imageViewBack = (ImageView) findViewById(R.id.imageview_back_in_logs);
		imageViewClear = (ImageView) findViewById(R.id.imageview_logs_clear);
		//imageViewScreenShot=(ImageView) findViewById(R.id.);
		textViewTitle.setTypeface(fontTitle);
		noLogdata.setTypeface(fontTextview);
		imageViewBack.setOnClickListener(listenerBack);
		imageViewClear.setOnClickListener(listenerClear);
		//imageViewScreenShot.setOnClickListener(listenerTappedImageScreenShot);
	}

	/*
	 * Show custom dialog for clear the internal logs data
	 * 
	 * Created on 20150722
	 * Created by karthick
	 */
	private OnClickListener listenerClear = new OnClickListener() {
		public void onClick(View v) {
			customDialog=new CustomDialog();
			customDialog.viewClearDialog(v.getContext(), listenerDialogClear);
			//takeScreenshot();
		}
	};
	
	/*
	 * Clear the internal logs data
	 * 
	 * Created on 20150722 
	 * Created by karthick
	 */
	private OnClickListener listenerDialogClear = new OnClickListener() {

		@Override
		public void onClick(View v) {

			sqliteHelper = DbHelper.getInstance(getApplicationContext());	
			sqliteHelper.deleteAllLogs();
			customDialog.hideClearDialog();
			onResume();		
		}
	};

	/*
	 * Finish the activity
	 * 
	 * Created on 20150722
	 * Created by karthick
	 */
	private OnClickListener listenerBack = new OnClickListener() {
		public void onClick(View v) {
			finish();
			overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			
		}
	};
	
	/*
	 * take screenshot
	 * 
	 * Created on 20150922 
	 * Created by karthick
	 */
	public void takeScreenshot() {
	   // Date now = new Date();
	   // android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
	    try {
	        // image naming and path  to include sd card  appending name you choose for file
	        String mPath = MainConstants.kConstantFileDirectory + MainConstants.kConstantFileName;
	       File fileDirectory= new File( MainConstants.kConstantFileDirectory);
	        if(!fileDirectory.exists()) {
	        	fileDirectory.mkdir();
	        }
	        // create bitmap screen capture
	        View view = getWindow().getDecorView().getRootView();
	        view.setDrawingCacheEnabled(true);
	        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
	        view.setDrawingCacheEnabled(false);
	        File imageFile = new File(mPath);
	        FileOutputStream outputStream = new FileOutputStream(imageFile);
	        int quality = 100;
	        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
	        outputStream.flush();
	        outputStream.close();
	       // sendMail(imageFile);
	    } catch (Throwable e) {
	        //e.printStackTrace();
	    }
	}
	
}
