package zohocreator;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.zoho.app.frontdesk.android.BuildConfig;
import appschema.SingletonVisitorProfile;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import constants.ErrorConstants;
import constants.MainConstants;
import constants.NumberConstants;
import constants.StringConstants;
import database.dbschema.EmployeeCheckInOut;
import database.dbschema.ZohoEmployeeRecord;

public class ForgotIDZohoCreator {

    private JSONArray jsonArray;
    private JSONObject jsonObject;

    /**
     * created by karthick On 15/04/2015
     * <p>
     * read the response in post method
     *
     * @param employee
     * @param forgotIDNumber
     * @return forgotIDRecordID
     */
    public String uploadForgotID(ZohoEmployeeRecord employee, String forgotIDNumber) {
        String forgotIDRecordID = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(MainConstants.kConstantDateTimeFormat);

            SimpleDateFormat GMTFormatter = new SimpleDateFormat(MainConstants.kConstantDateTimeFormat);
            GMTFormatter.setTimeZone(TimeZone.getTimeZone(MainConstants.creatorUTCDateFormat));
            Date date = new Date();
            URL url;
            url = new URL(MainConstants.ForgotIDApplication
                    + MainConstants.kConstantAddForgotID);
            //Log.d("singleton","officeLocation"+SingletonVisitorProfile.getInstance().getLocationCode());

            //Log.d("VisitorZohoCreator", "Creator" + GMTFormatter.format(date));
            String setParameter = MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.forgotIDEmployeeID + employee.getEmployeeId()
                    + MainConstants.forgotIDEmployeeName + employee.getEmployeeName()
                    + MainConstants.forgotIDEmployeePhone + employee.getEmployeeMobileNumber()
                    + MainConstants.forgotIDEmployeeEmailID + employee.getEmployeeEmailId()
                    //+ MainConstants.forgotIDEmployeeExtension + employee.employeeExtentionNumber
                    //SingletonVisitorProfile.getInstance().getLocationCode()
                    + MainConstants.forgotIDEmployeeLocationCode + SingletonVisitorProfile.getInstance().getLocationCode()
                    //+ MainConstants.forgotIDEmployeeDateOfRequest + formatter.format(date)
                    //+ MainConstants.forgotIDEmployeeDateOfRequestGMT +GMTFormatter.format(date)
                    + MainConstants.forgotIDEmployeeDateOfIssued + formatter.format(date)
                    + MainConstants.forgotIDEmployeeTemporaryIDCardNumber + forgotIDNumber
                    //+ MainConstants.forgotIDEmployeeReturned + MainConstants.forgotIDReturnedNo
                    + MainConstants.forgotIDEmployeeLocation + employee.getEmployeeSeatingLocation();
            //+ MainConstants.forgotIDEmployeeUploadCompleted+MainConstants.kConstantTrue;

            //Log.d("singleton","officeLocation"+SingletonVisitorProfile.getInstance().getLocationCode());
            jsonObject = getJsonArrayInPostMethod(url, setParameter);
            if (jsonObject != null) {
                //Log.d("jsonObject", "jsonObject" + jsonObject);
                jsonArray = jsonObject
                        .optJSONArray(MainConstants.kConstantFormName);
                //Log.d("jsonArray", "jsonArray" + jsonArray);
                JSONObject jsonSubObject = (JSONObject) jsonArray
                        .getJSONObject(MainConstants.kConstantOne);
                JSONArray jsonSubArray = jsonSubObject
                        .getJSONArray(MainConstants.kConstantOperation);
                //Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
                JSONObject jsonSubOfSubObject = (JSONObject) jsonSubArray
                        .getJSONObject(MainConstants.kConstantOne);
                //Log.d("jsonArray", "jsonObjectst" + jsonSubOfSubObject);

                JSONObject jsonObjectOfValues = jsonSubOfSubObject
                        .getJSONObject(MainConstants.kConstantValues);
                //Log.d("jsonArray",
                //	"recordId"
                //+ jsonObjectOfValues
                //	.getString(MainConstants.kConstantIncidentsId));

                if ((jsonObjectOfValues != null)
                        && (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
                        && (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

                    // String visitorName =
                    // jsonArray.getJSONObject(i).getString(
                    // MainConstants.ConstantVisitorName);
                    forgotIDRecordID = jsonObjectOfValues
                            .getString(MainConstants.forgotIDId);

                    //	Log.d("id", "recordId" + forgotIDRecordID);
                } else {
                    //Log.d("uploadVisitorDetail", "jsonArray null");
                }
                //Log.d("VisitorZohoCreator", "Creator" + jsonObject.toString());
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            //Log.d("window", "MalformedURLException" + e);
            forgotIDRecordID = null;
            //ErrorCode.errorCode=ErrorCode.uploadIncidentUrlException;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //Log.d("window", "JSONException" + e);
            forgotIDRecordID = "";
            //ErrorCode.errorCode=ErrorCode.uploadIncidentJsonException;
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            //e1.printStackTrace();
            //Log.d("window", "IOException" + e1);
            //ErrorCode.errorCode=ErrorCode.uploadIncidentNoNetwork;
            forgotIDRecordID = null;
        }
        return forgotIDRecordID;
    }

    /**
     * created by jayapriya On 25/10/2014
     * <p>
     * read the response in post method
     *
     * @param url
     * @param setParameter
     * @return JSONObject
     * @throws IOException
     */
    public JSONObject getJsonArrayInPostMethod(URL url, String setParameter) throws IOException {

        //Log.d("get", "update"+url.toString()+":"+setParameter);
        HttpURLConnection con = null;
        JSONObject jsonObject = null;
        String json = "";


        con = (HttpURLConnection) url.openConnection();
        //Log.d("get", "update2");
        con.setDoOutput(true);
        //Log.d("get", "update3");
        con.setRequestMethod(MainConstants.kConstantPostMethod);
        //Log.d("get", "update4");
        con.setRequestProperty(MainConstants.kConstantUserAgent,
                MainConstants.user_Agent);
        //Log.d("get", "update5");
        con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                MainConstants.acceptLanguage);
        con.setConnectTimeout(60 * 1000);
        //Log.d("get", "update6");
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        //Log.d("get", "update7");
        wr.writeBytes(setParameter);
        wr.flush();
        wr.close();
        int status = con.getResponseCode();
        if (status == MainConstants.kConstantSuccessCode) {
            // read the response
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));

            StringBuffer response = new StringBuffer();
            String line = null;

            while ((line = in.readLine()) != null) {

                response.append(line + MainConstants.kConstantNewLine);
                json = response.toString();
                //Log.d("get", "data:"+response.toString());
                try {
                    jsonObject = new JSONObject(json);
                } catch (JSONException e) {
                    jsonObject = null;
                    //Log.d("get", "JSONException:"+e);
                }
            }
        } else {
            //ErrorCode.errorCode=ErrorCode.uploadIncidentNotSuccess;
        }


        return jsonObject;
    }


    public void updateEmployeeCheckInErrorCode(String forgotIDRecordID, int checkInErrorCode) {

        String data = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteriaIncidentNumber
                + forgotIDRecordID + MainConstants.forgotIDCheckInErrorCode + checkInErrorCode;
        //Log.d("Android", "forgotIDRecordID:"+data);
        try {
            URL url;
            url = new URL(MainConstants.ForgotIDApplication + MainConstants.kConstantUpdateForgotID);
            HttpURLConnection con = (HttpURLConnection) url
                    .openConnection();
            // con.setUseCaches(false);
            con.setDoOutput(true);
            con.setRequestMethod(MainConstants.kConstantPostMethod);
            con.setRequestProperty(MainConstants.kConstantUserAgent,
                    MainConstants.user_Agent);
            con.setRequestProperty(
                    MainConstants.kConstantAcceptLanguage,
                    MainConstants.acceptLanguage);
            con.setConnectTimeout(60 * 1000);
            DataOutputStream wr = new DataOutputStream(
                    con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            int status = con.getResponseCode();
            //Log.d("Android", "status:"+status);
            if (status == 200) {
                // throw new IOException("Post failed with error code " +
                // status);
            }


            // read the response
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                // Log.d("Android", "response:"+line);
                if (line.startsWith("Auth=")) {
                }
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            //Log.d("Android","url"+e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            //Log.d("Android","IOE:"+e);
        }

    }


    public void updateEmployeeTempIdUploadCompleted(String forgotIDRecordID) {

        String data = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteriaIncidentNumber
                + forgotIDRecordID + MainConstants.forgotIDEmployeeUploadCompleted + MainConstants.kConstantTrue;
        //Log.d("Android", "forgotIDRecordID:"+data);
        try {
            URL url;
            url = new URL(MainConstants.ForgotIDApplication + MainConstants.kConstantUpdateForgotID);
            HttpURLConnection con = (HttpURLConnection) url
                    .openConnection();
            // con.setUseCaches(false);
            con.setDoOutput(true);
            con.setRequestMethod(MainConstants.kConstantPostMethod);
            con.setRequestProperty(MainConstants.kConstantUserAgent,
                    MainConstants.user_Agent);
            con.setRequestProperty(
                    MainConstants.kConstantAcceptLanguage,
                    MainConstants.acceptLanguage);
            con.setConnectTimeout(60 * 1000);
            DataOutputStream wr = new DataOutputStream(
                    con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            int status = con.getResponseCode();
            //Log.d("Android", "status:"+status);
            if (status == 200) {
                // throw new IOException("Post failed with error code " +
                // status);
            }


            // read the response
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                // Log.d("Android", "response:"+line);
                if (line.startsWith("Auth=")) {
                }
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            //Log.d("Android","url"+e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            //Log.d("Android","IOE:"+e);
        }

    }


    public int employeeCheckIn(String employeeEmailId) {
//	//Log.d("people","employeeId"+employeeEmailId);
        int PeopleAPICode = 0;
////https://people.zoho.com/people/api/attendance/bulkImport
//
//	HttpClient httpclient = new DefaultHttpClient();
//    HttpPost httppost = new HttpPost(MainConstants.peopleCheckInURL);
//
//
//    try {
//    	SimpleDateFormat formatter = new SimpleDateFormat(MainConstants.peopleCheckInDateFormat);
//    	Date checkInDate=new Date();
//        // Add your data
//        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
//        //nameValuePairs.add(new BasicNameValuePair("empId","1265"));
//        nameValuePairs.add(new BasicNameValuePair(MainConstants.peopleCheckInAuthToken,MainConstants.peopleCheckInAuthTocken));
//        nameValuePairs.add(new BasicNameValuePair(MainConstants.peopleCheckInDateFormatString,MainConstants.peopleCheckInDateFormat));
//
//        nameValuePairs.add(new BasicNameValuePair(MainConstants.peopleCheckInCheckIn,formatter.format(checkInDate)));
//        //nameValuePairs.add(new BasicNameValuePair("checkOut","17/04/2015 20:30:45"));
//
//        nameValuePairs.add(new BasicNameValuePair(MainConstants.peopleCheckInEmailId,employeeEmailId));
//        //nameValuePairs.add(new BasicNameValuePair("dateFormat","yyyy-MM-dd HH:mm:ss"));
//        //nameValuePairs.add(new BasicNameValuePair("data","[{'empId':'CON1','checkIn':'2015-04-17 18:35:00'}, {'empId':'CON1','checkOut':'2015-04-17 18:35:00'}]"));
//
//        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//        // Execute HTTP Post Request
//        HttpResponse response = httpclient.execute(httppost);
//        //Log.d("Android", "people status:"+EntityUtils.toString(response.getEntity()));
//       // PeopleAPICode=parseCheckInResponse(EntityUtils.toString(response.getEntity()).toString());
//        int responseCode = response.getStatusLine().getStatusCode();
//        if(responseCode ==200){
//        	PeopleAPICode=MainConstants.peopleResponseSuccessCode;
//        }
//        else
//        {
//        	PeopleAPICode=MainConstants.peopleResponseOtherError;
//        }
//
//    } catch (ClientProtocolException e) {
//    	//Log.d("Android", "people status:"+e);
//    PeopleAPICode=MainConstants.peopleResponseNetworkError;
//    } catch (IOException e) {
//    	//Log.d("Android", "people status:"+e);
//    	 PeopleAPICode=MainConstants.peopleResponseNetworkError;
//    }
        return PeopleAPICode;
    }

    //public int parseCheckInResponse(String response)
//{    //Log.d("Android", "people parse status:");
//	int PeopleAPICode=0;
//	//Log.d("Android", "people parse status:"+response);
//	try {
//		JSONArray jsonArray = new JSONArray(response);
//		JSONObject jsonSubObject = (JSONObject) jsonArray
//				.getJSONObject(0);
//	 	if(jsonSubObject.getString(MainConstants.peopleResponse).equals(MainConstants.peopleResponseSuccess)&&jsonSubObject.getString(MainConstants.peoplePunchIn).trim().length()>0)
//	 	{
//	 	//Log.d("Android", "people parse status:"+jsonSubObject.getString(MainConstants.peoplePunchIn));
//	 	PeopleAPICode=MainConstants.peopleResponseSuccessCode;
//	 	}
//	} catch (ParseException e) {
//		// TODO Auto-generated catch block
//		//e.printStackTrace();
//		PeopleAPICode=parseDateFormatResponse(response);
//	} catch (JSONException e) {
//		//e.printStackTrace();
//		PeopleAPICode=parseDateFormatResponse(response);
//	}
//	return PeopleAPICode;
//}
    private int parseDateFormatResponse(String response) {
        int PeopleAPICode = 0;
        try {
            JSONArray jsonArray = new JSONArray(response);
            JSONObject jsonSubObject = (JSONObject) jsonArray
                    .getJSONObject(0);
            //Log.d("Android", "status:"+jsonSubObject.getString("error"));
            if (jsonSubObject.getString("error").equals("Unparseable date")) {
                PeopleAPICode = MainConstants.peopleResponseDateFormatError;
                //Log.d("Android", "status:"+jsonSubObject.getString("error"));
            }
        } catch (JSONException e) {
            //e.printStackTrace();
            PeopleAPICode = MainConstants.peopleResponseOtherError;
            if (response.indexOf(StringConstants.peopleAuthTockenInvalid) > 0) {
                PeopleAPICode = MainConstants.peopleResponseAuthTockenError;
            }
            //Log.d("Android", "JSONException:");
        }
        return PeopleAPICode;
    }


    public int uploadFile(Context context, String uploadServerUri, String sourceFileUri) {

        //Log.d("window", "url" + uploadServerUri);
        int uploadCode = MainConstants.kConstantMinusOne;
        Date date = new Date();
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = MainConstants.kConstantLineEnd;
        String twoHyphens = MainConstants.kConstantDoubleHyphen;
        String boundary = date.getTime() + MainConstants.kConstantEmpty;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = MainConstants.kConstantOne
                * MainConstants.kConstantBufferSize
                * MainConstants.kConstantBufferSize;
        File sourceFile = new File(sourceFileUri);

        if (sourceFile.isFile()) {

            if (uploadServerUri != null) {
                int serverResponseCode = MainConstants.kConstantZero;
                try {

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(
                            sourceFile);
                    URL url = new URL(uploadServerUri.replaceAll(
                            MainConstants.kConstantWhiteSpace,
                            MainConstants.kConstantreplaceUrl));
                    //Log.d("window", "url" + sourceFileUri);
                    // Open a HTTP connection to the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod(MainConstants.kConstantPostMethod);
                    //Log.d("window", "setConnectTimeout:" + conn.getConnectTimeout());
                    conn.setConnectTimeout(MainConstants.connectionTimeout);
                    // Log.d("window", "setConnectTimeout:" + conn.getConnectTimeout());
                    conn.setReadTimeout(MainConstants.connectionTimeout);
                    // conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty(MainConstants.kConstantEncrype,
                            MainConstants.kConstantMultiPart);
                    conn.setRequestProperty(MainConstants.kConstantContentType,
                            MainConstants.kConstantMultiPart
                                    + MainConstants.kConstantBoundary + boundary);
                    conn.setRequestProperty(MainConstants.kConstantUploadFile,
                            sourceFileUri);
                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes(MainConstants.kConstantContentDisposition
                            + sourceFileUri
                            + MainConstants.kConstantDoubleQuotes + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    //Log.d("window", "url" + sourceFileUri);
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer,
                            MainConstants.kConstantZero, bufferSize);

                    while (bytesRead > MainConstants.kConstantZero) {

                        dos.write(buffer, MainConstants.kConstantZero,
                                bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer,
                                MainConstants.kConstantZero, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();

                    String serverResponseMessage = conn.getResponseMessage();
                    // Log.d("window", "serverResponseMessage" + serverResponseMessage);
                    uploadCode = serverResponseCode;
                    if (serverResponseCode == MainConstants.kConstantSuccessCode) {
                        uploadCode = serverResponseCode;
                    }
                    //Log.d("window", "serverResponseMessage" + uploadCode);
                    // close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                } catch (MalformedURLException ex) {

                    uploadCode = ErrorConstants.visitorUploadPhotoURLError;

                    // Log.d("window", "MalformedURLException u: " + ex.getMessage(),
                    //       ex);
                } catch (FileNotFoundException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoNotFoundError;
                    // Log.d("window", "FileNotFoundException: " + e.getMessage());
                } catch (ProtocolException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoProtocalError;
                    // Log.d("window", "ProtocolException: u " + e.getMessage());
                } catch (IOException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoIOError;
                    // Log.d("window", "IOException:  u1" + e.getMessage());


                    if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                        uploadCode = ErrorConstants.visitorUploadPhotoTimeOutError;
                    }
                    //Log.d("window", "IOException:  u3" + e.getMessage());
                    if (!NetworkAnalytics.isNetworkAvailable(context)) {
                        uploadCode = ErrorConstants.visitorUploadPhotoNetworkError;
                    }
                    // Log.d("window", "IOException:  u2" + e.getMessage());
                }
            }

        } // End else block
        return uploadCode;
    }

    /**
     * Add device details to cloud
     *
     * @param checkInOutDetails
     * @return response String
     * @author Karthikeyan D S
     * @created 2018/04/09
     */
    public String SyncCheckInCheckOutDetailsCreator(Context context,EmployeeCheckInOut checkInOutDetails) {
        String response = MainConstants.kConstantEmpty;
        if (checkInOutDetails != null) {
            URL url;
            try {
                url = new URL(MainConstants.creatorLink
                        + MainConstants.creatorOwnerName
                        + MainConstants.creatorJsonFormat
                        + MainConstants.creatorTemporaryIDAppName
                        + MainConstants.creatorEmployeeForgetIdJsonDetails
                );

                //Function to return Formed Values.
                String urlDataValues = mapValues(context, checkInOutDetails);

                String params = MainConstants.creatorAuthtokenText
                        + MainConstants.creatorAuthtokenValue
                        + MainConstants.creatorScope
                        + MainConstants.creatorScopeValue
                        + MainConstants.employeeForgetIdJson
                        + urlDataValues;

                response = restAPICall(MainConstants.kConstantPostMethod, url, params);
//                Log.d("Message", "CheckedoutFinalDbResponse" + response);
            } catch (MalformedURLException urlExcep) {

            } catch (IOException ioExcep) {

            }
        }

        return response;
    }


    private String mapValues(Context context,EmployeeCheckInOut checkInOutDetails) {
        String mapValues = MainConstants.kConstantEmpty;
        if(checkInOutDetails!=null) {
            String dateText = MainConstants.kConstantEmpty;
            SimpleDateFormat dateFormat = new SimpleDateFormat(MainConstants.kConstantDateTimeFormat);
            JSONObject json;
//          Get Android Device Id
            String mAppVersion = BuildConfig.VERSION_NAME;

            String deviceName = Build.MANUFACTURER + " " + Build.MODEL;
            //get Device details.
            String androidDeviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

            Map<String, String> EmployeeCheckInCheckOut = new HashMap<String, String>();
            if (checkInOutDetails.getEmployeeID() != null && !checkInOutDetails.getEmployeeID().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeIdkey, checkInOutDetails.getEmployeeID());
            }
            if (checkInOutDetails.getEmployeeZUID() != null && !checkInOutDetails.getEmployeeZUID().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.jsonEmployeeZuid, checkInOutDetails.getEmployeeZUID());
            }
            if (checkInOutDetails.getEmployeeEmailID() != null && !checkInOutDetails.getEmployeeEmailID().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeEmailIdkey, checkInOutDetails.getEmployeeEmailID());
            }
            if (checkInOutDetails.getEmployeeName() != null && !checkInOutDetails.getEmployeeName().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeNamekey, checkInOutDetails.getEmployeeName());
            }
            if (checkInOutDetails.getCheckInTime() > MainConstants.kConstantZero) {
                if(checkInOutDetails.getCheckInTimeDate()!=null && !checkInOutDetails.getCheckInTimeDate().isEmpty()) {
                    EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeCheckInTimekey, checkInOutDetails.getCheckInTimeDate());
                }
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeCheckInTimekeyLong, checkInOutDetails.getCheckInTime() + "");
            }
            if (checkInOutDetails.getCheckOutTime() > MainConstants.kConstantZero) {
                if(checkInOutDetails.getCheckInTimeDate()!=null && !checkInOutDetails.getCheckInTimeDate().isEmpty()) {
                    EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeCheckOutTimekey, checkInOutDetails.getCheckOutTimeDate());
                }
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeCheckOutTimekeyLong, checkInOutDetails.getCheckOutTime() + "");
            }
            if (checkInOutDetails.getOfficeLocation() != null && !checkInOutDetails.getOfficeLocation().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeOfficeLocationkey, checkInOutDetails.getOfficeLocation());
            }
            if (checkInOutDetails.getTemporaryID() != null && !checkInOutDetails.getTemporaryID().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.kConstantEmployeeTempIdkey, checkInOutDetails.getTemporaryID());
            }
            EmployeeCheckInCheckOut.put(MainConstants.kConstantAppVersion, mAppVersion);
            EmployeeCheckInCheckOut.put(MainConstants.kConstantDeviceID, androidDeviceID.toString().trim());
            EmployeeCheckInCheckOut.put(MainConstants.kConstantDeviceName, deviceName);

            if (SingletonVisitorProfile.getInstance().getDeviceLocation() != null
                    && !SingletonVisitorProfile.getInstance()
                    .getDeviceLocation().isEmpty()) {
                EmployeeCheckInCheckOut.put(MainConstants.spKeyDeviceLocation,SingletonVisitorProfile.getInstance().getDeviceLocation());
            }

            json = new JSONObject(EmployeeCheckInCheckOut);

//          Log.d("Message", "mapData" + json);

            mapValues = json.toString();
        }
        else
        {
            mapValues=MainConstants.kConstantEmpty;
        }
        return mapValues;
    }

    /**
     * Call GET or POST method Rest API call
     *
     * @param restApiMethod String
     * @param url           URL
     * @param setParameter  String
     * @return responseString String
     * @throws IOException
     * @author Karthikeyan D S
     * @created 2018/04/09
     */
    public String restAPICall(String restApiMethod, URL url, String setParameter) throws IOException {
        String responseString = MainConstants.kConstantEmpty;

        if (restApiMethod != null && !restApiMethod.isEmpty() && url != null && !url.toString().isEmpty()) {
            HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();
            if (con != null) {
                con.setDoOutput(true);
                con.setRequestMethod(restApiMethod);

                con.setConnectTimeout(NumberConstants.connectionTimeout);
                con.setReadTimeout(NumberConstants.connectionTimeout);

                if (restApiMethod == MainConstants.kConstantPostMethod && setParameter != null && !setParameter.isEmpty()) {
                    con.setRequestProperty(MainConstants.kConstantUserAgent,
                            MainConstants.user_Agent);
                    con.setRequestProperty(MainConstants.kConstantAcceptLanguage,
                            MainConstants.acceptLanguage);
                    DataOutputStream dataOutputStream = new DataOutputStream(
                            con.getOutputStream());
                    if (dataOutputStream != null) {
                        dataOutputStream.writeBytes(setParameter);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                    }
                } else {
                    con.connect();
                }

                int status = con.getResponseCode();
                // Log.d("URL and STATUS",url+"---"+status);
                if (status == NumberConstants.kConstantSuccessCode) {
                    // read the response
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

                    StringBuffer response = new StringBuffer();
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        response.append(line + MainConstants.kConstantNewLine);
                    }
                    responseString = response.toString();
//                    Log.d("post", "response:" + responseString);

                }
            }
        }
        return responseString;
    }

}


