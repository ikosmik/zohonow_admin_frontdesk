package zohocreator;

import org.json.JSONObject;

import java.io.InputStream;

/**
 * Created by jayapriya on 11/03/15.
 */
public class TaskResult {

    public static int CODE_ERROR = 0;
    public static int CODE_SUCCESS = 1;

    public int code = CODE_ERROR;
    public String message = null;
    private JSONObject data = null;

    public Object getResultData() {
        return data;
    }

    public void setResultData(JSONObject data) {
        this.data = data;
    }
}

