package zohocreator;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utility.JSONParser;
import appschema.SingletonVisitorProfile;

import constants.MainConstants;

public class InterviewerZohoCreator {
	
	
	
	private JSONArray jsonArray;
    private JSONObject jsonObject;
	
    /**
	 * created by karthick On 20/04/2015
	 * 
	 * Get Data from zoho creator
	 */
	public void uploadForgotID() {
		
		try {
			String urlForInterviewCandidate=MainConstants.kConstantViewRecord
                    + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantInterviewCandidateReport
                    +MainConstants.kConstantRawAndOwnerName
                    +MainConstants.kConstantViewInterviewRecordCriteria
                    + SingletonVisitorProfile.getInstance().getLocationCode();
			//Log.d("jsonObject", "url:" + urlForInterviewCandidate);
			JSONParser jsonParser = new JSONParser();
			jsonObject =jsonParser.getJsonArrayInGetMethod(urlForInterviewCandidate);
				if (jsonObject != null) {
					//Log.d("jsonObject", "jsonObject" + jsonObject);
					jsonArray = jsonObject
							.optJSONArray(MainConstants.kConstantFormName);
					//Log.d("jsonArray", "jsonArray" + jsonArray);
					JSONObject jsonSubObject = (JSONObject) jsonArray
							.getJSONObject(MainConstants.kConstantOne);
					JSONArray jsonSubArray = jsonSubObject
							.getJSONArray(MainConstants.kConstantOperation);
					//Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
					JSONObject jsonSubOfSubObject = (JSONObject) jsonSubArray
							.getJSONObject(MainConstants.kConstantOne);
					//Log.d("jsonArray", "jsonObjectst" + jsonSubOfSubObject);

					JSONObject jsonObjectOfValues = jsonSubOfSubObject
							.getJSONObject(MainConstants.kConstantValues);
					//Log.d("jsonArray",
					//	"recordId"
					//+ jsonObjectOfValues
					//	.getString(MainConstants.kConstantIncidentsId));

					if ((jsonObjectOfValues != null)
						&& (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
						&& (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

					// String visitorName =
					// jsonArray.getJSONObject(i).getString(
					// MainConstants.ConstantVisitorName);
					 jsonObjectOfValues
							.getString(MainConstants.forgotIDId);

				//	Log.d("id", "recordId" + jsonObjectOfValues
				//			.getString(MainConstants.forgotIDId));
				} else {
					//Log.d("uploadVisitorDetail", "jsonArray null");
				}
				//Log.d("VisitorZohoCreator", "Creator" + jsonObject.toString());
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			//Log.d("window", "JSONException" + e);
			//ErrorCode.errorCode=ErrorCode.uploadIncidentJsonException;
		 } catch(IOException e) {
         	
         }
		catch(TimeoutException e) {
    	 	//uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
     }
	}

	

}
