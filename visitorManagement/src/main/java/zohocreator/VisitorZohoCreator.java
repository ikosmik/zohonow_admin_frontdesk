package zohocreator;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import appschema.VisitorOTPDetails;
import constants.NumberConstants;
import constants.StringConstants;
import database.dbschema.EmployeeCheckInOut;
import database.dbschema.EmployeeVisitorPreApproval;
import database.dbschema.ZohoEmployeeRecord;
import utility.NetworkConnection;
import appschema.SharedPreferenceController;
import utility.TimeZoneConventer;
import utility.UsedBitmap;
import utility.Validation;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageReq;
import com.android.volley.toolbox.StringRequest;
import com.zoho.app.frontdesk.android.ApplicationController;

import utility.JSONParser;

import com.zoho.app.frontdesk.android.BuildConfig;
import com.zoho.app.frontdesk.android.R;

import appschema.SingletonEmployeeProfile;

import appschema.SingletonVisitorProfile;

import logs.InternalAppLogs;
import constants.DataBaseConstants;
import constants.ErrorConstants;
import constants.MainConstants;
import database.dbschema.Stationery;
import database.dbschema.StationeryEntry;
import appschema.StationeryRequest;
import database.dbhelper.DbHelper;
import database.dbschema.EmployeeFrequency;
import database.dbschema.PurposeOfVisitRecord;
import database.dbschema.VisitorRecord;

import android.content.Context;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;


/**
 * Get the visitor in app and store the name into ZohoCreator.
 *
 * @author jayapriya
 * @created 16/10/14
 */
public class VisitorZohoCreator {

    DbHelper sqliteHelper;
    Integer code;
    RequestQueue mRequestQueue;
    Context context;
    private SharedPreferenceController sharedPreferenceController;

    public VisitorZohoCreator(Context context) {
        this.context = context;
        sqliteHelper = new DbHelper(context);
        mRequestQueue = ApplicationController.getInstance().getRequestQueue();
    }

    private String upLoadBussinessCardUri = null;

    private String upLoadVisitorImageUri = null;

    private String upLoadVisitorSignUri = null;

    private Integer visitFrequency;
//	private String recordId;
//	private String visitorName;
//	private String mobileNo;
//	private String mailId;
//	private String location;

    private JSONArray jsonArray;
    private JSONObject jsonObject;
    private String json = MainConstants.kConstantEmpty;
    private static URL url;
    private static JSONParser jsonParser = new JSONParser();

    // ResponseVisitorCallBack<JSONObject> response;

    /**
     * upload visitor record into the zoho creator, when the wifi on.
     *
     * @return
     * @author jayapriya
     * @created 16/10/14
     */

    // Store visitor records into the creator
    public int uploadVisitorRecord() {

        int uploadCode = MainConstants.kConstantMinusOne;
        String setParameter = "";

        try {
            URL url = new URL("");
            if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment().equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartmentAnotherNamePlural))
                    && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                    .getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {

                url = new URL(MainConstants.interviewApplication + MainConstants.kConstantAddCandidateRecord);

                setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCreatorCandidateName
                        + SingletonVisitorProfile.getInstance().getVisitorName()
                        + MainConstants.kConstantCreatorCandidateLocation
                        + SingletonVisitorProfile.getInstance().getAddress()
                        + MainConstants.kConstantCreatorCandidateMobileNo
                        + SingletonVisitorProfile.getInstance().getPhoneNo()
                        + MainConstants.kConstantCreatorCandidateEmail
                        + SingletonVisitorProfile.getInstance().getEmail();

            } else {
                if (SingletonVisitorProfile.getInstance() != null && !SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty()
                        && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equalsIgnoreCase(MainConstants.purposeDataCenter)) {
//                        Log.d("DSK ","uploadVisitorRecord");
                } else {
                    //Log.d("upload", "uploadVisitorRecord else");
                    url = new URL(MainConstants.application + MainConstants.kConstantAddVisitorDetails);

                    setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCreatorVisitorName
                            + SingletonVisitorProfile.getInstance().getVisitorName()
                            + MainConstants.kConstantCreatorLocation + SingletonVisitorProfile.getInstance().getAddress()
                            + MainConstants.kConstantCreatorMobileNo + SingletonVisitorProfile.getInstance().getPhoneNo()
                            + MainConstants.kConstantCreatorEmail + SingletonVisitorProfile.getInstance().getEmail()
                            + MainConstants.kConstantCreatorDesignation
                            + SingletonVisitorProfile.getInstance().getDesignation() + MainConstants.kConstantCreatorCompany
                            + SingletonVisitorProfile.getInstance().getCompany()
                            + MainConstants.kConstantCreatorCompanyWebsite
                            + SingletonVisitorProfile.getInstance().getCompanyWebsite() + MainConstants.kConstantCreatorFax
                            + SingletonVisitorProfile.getInstance().getFax();
                }
            }

            //Log.d("VisitorZohoCreator", "Creator" + url);

            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
            //Log.d("jsonObject", "jsonObject" + jsonObject);
            jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
            JSONObject jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
            JSONArray jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
            JSONObject jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);

            JSONObject jsonObjectOfValues = jsonSubOfSubObject.getJSONObject(MainConstants.kConstantValues);

            if ((jsonObjectOfValues != null) && (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
                    && (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

                // String visitorName = jsonArray.getJSONObject(i).getString(
                // MainConstants.ConstantVisitorName);
                String id = jsonObjectOfValues.getString(MainConstants.kConstantVisitorId);
                SingletonVisitorProfile.getInstance().setVisitorCreatorId(id);
            } else {
            }

            if ((SingletonVisitorProfile.getInstance().getVisitorCreatorId() != null)
                    && (SingletonVisitorProfile.getInstance().getVisitorCreatorId() != MainConstants.kConstantEmpty)) {

                if ((SingletonVisitorProfile.getInstance().getVisitorBusinessCard() != null) && (SingletonVisitorProfile
                        .getInstance().getVisitorBusinessCard() != MainConstants.kConstantEmpty)) {
                    upLoadBussinessCardUri = MainConstants.kConstantFileUploadUrl
                            + MainConstants.kConstantCreatorAuthTokenAndScope
                            + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitorDetail
                            + MainConstants.kConstantFileUploadFieldNameInBussinessCard
                            + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                            + MainConstants.kConstantFileAccessTypeAndName
                            + SingletonVisitorProfile.getInstance().getVisitorName()
                            + MainConstants.kConstantFileSharedBy;
                    // uploadFile(upLoadBussinessCardUri,
                    // SingletonVisitorProfile.getInstance()
                    // .getVisitorBusinessCard());
                    // Log.d("upload", "businesscard");

                }
                uploadCode = uploadVisitRecord();
                //Log.d("window: ", "uploadVisitRecord:" + uploadCode);
                // UploadBusinessCardAndVisitRecord upload = new
                // UploadBusinessCardAndVisitRecord();
                // upload.execute();

            }

        } catch (MalformedURLException e) {

            uploadCode = ErrorConstants.visitorUploadVisitorURLError;
            // Log.d("window","url"+e);
        } catch (JSONException e) {
            uploadCode = ErrorConstants.visitorUploadVisitorJSONError;
            jsonObject = null;
        } catch (IOException e) {
            uploadCode = ErrorConstants.visitorUploadVisitorIOError;

            if (!NetworkAnalytics.isNetworkAvailable(context)) {
                uploadCode = ErrorConstants.visitorUploadVisitorNetworkError;
            }
            if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                uploadCode = ErrorConstants.visitorUploadVisitorTimeOutError;
            }
            jsonObject = null;

        }
        return uploadCode;
    }

    // Singleton.getInstance().setVisitorCreatorId(recordId);
    // Log.d("recordId", "recordId" + recordId);

    // }

    public int uploadFile(String uploadServerUri, String sourceFileUri) {

        int uploadCode = MainConstants.kConstantMinusOne;
        Date date = new Date();
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = MainConstants.kConstantLineEnd;
        String twoHyphens = MainConstants.kConstantDoubleHyphen;
        String boundary = date.getTime() + MainConstants.kConstantEmpty;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = MainConstants.kConstantOne * MainConstants.kConstantBufferSize
                * MainConstants.kConstantBufferSize;
        File sourceFile = new File(sourceFileUri);

        if (sourceFile.isFile()) {

            if (uploadServerUri != null) {
                int serverResponseCode = MainConstants.kConstantZero;
                try {

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(uploadServerUri.replaceAll(MainConstants.kConstantWhiteSpace,
                            MainConstants.kConstantreplaceUrl));
                    //Log.d("window", "url" + sourceFileUri);
                    // Open a HTTP connection to the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod(MainConstants.kConstantPostMethod);
                    // Log.d("window", "setConnectTimeout:" +
                    // conn.getConnectTimeout());
                    conn.setConnectTimeout(MainConstants.connectionPhotoTimeout);
                    // Log.d("window", "setConnectTimeout:" +
                    // conn.getConnectTimeout());
                    conn.setReadTimeout(MainConstants.readPhotoTimeout);
                    // conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty(MainConstants.kConstantEncrype, MainConstants.kConstantMultiPart);
                    conn.setRequestProperty(MainConstants.kConstantContentType,
                            MainConstants.kConstantMultiPart + MainConstants.kConstantBoundary + boundary);
                    conn.setRequestProperty(MainConstants.kConstantUploadFile, sourceFileUri);
                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes(MainConstants.kConstantContentDisposition + sourceFileUri
                            + MainConstants.kConstantDoubleQuotes + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // Log.d("window", "url" + sourceFileUri);
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, MainConstants.kConstantZero, bufferSize);

                    while (bytesRead > MainConstants.kConstantZero) {

                        dos.write(buffer, MainConstants.kConstantZero, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, MainConstants.kConstantZero, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();

                    //String serverResponseMessage = conn.getResponseMessage();
                    //Log.d("window", "serverResponseMessage" + serverResponseMessage);
                    uploadCode = serverResponseCode;
                    if (serverResponseCode == MainConstants.kConstantSuccessCode) {
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(conn.getInputStream()));

                        StringBuffer response = new StringBuffer();
                        String line = null;

                        while ((line = in.readLine()) != null) {

                            response.append(line + MainConstants.kConstantNewLine);
                            json = response.toString();

                        }
                        if ((json != null) && (json.contains(StringConstants.creatorResponseSuccessFirst) || json.contains(StringConstants.creatorResponseSuccessSecond))) {
                            uploadCode = MainConstants.kConstantSuccessCode;
                        } else {

                            setInternalLogs(context, ErrorConstants.visitorUploadPhotoResponseError, json);
                            uploadCode = ErrorConstants.visitorUploadPhotoResponseError;
                        }
                    }
                    //Log.d("window", "serverResponseMessage" + uploadCode);
                    // close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                } catch (MalformedURLException ex) {

                    uploadCode = ErrorConstants.visitorUploadPhotoURLError;
                    setInternalLogs(context, uploadCode, ex.getLocalizedMessage());
                    //Log.d("window", "MalformedURLException u: " + ex.getMessage(), ex);
                } catch (FileNotFoundException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoNotFoundError;
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                    //Log.d("window", "FileNotFoundException: " + e.getMessage());
                } catch (ProtocolException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoProtocalError;
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                    //Log.d("window", "ProtocolException: u " + e.getMessage());
                } catch (IOException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoIOError;
                    //Log.d("window", "IOException:  u1" + e.getMessage());

                    if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                        uploadCode = ErrorConstants.visitorUploadPhotoTimeOutError;
                    }
                    //Log.d("window", "IOException:  u3" + e.getMessage());
                    if (!NetworkAnalytics.isNetworkAvailable(context)) {
                        uploadCode = ErrorConstants.visitorUploadPhotoNetworkError;
                    }
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                    //Log.d("window", "IOException:  u2" + e.getMessage());
                }
            }

        } // End else block
        return uploadCode;
    }

    public int uploadFile(String uploadServerUri, byte[] sourceFileByte) {

        int uploadCode = MainConstants.kConstantMinusOne;
        Date date = new Date();
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = MainConstants.kConstantLineEnd;
        String twoHyphens = MainConstants.kConstantDoubleHyphen;
        String boundary = date.getTime() + MainConstants.kConstantEmpty;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = MainConstants.kConstantOne * MainConstants.kConstantBufferSize
                * MainConstants.kConstantBufferSize;

        String sourceFileUri = context.getFilesDir()
                + MainConstants.kConstantImageUploadFile;
        UsedBitmap usedBitmap = new UsedBitmap();
        boolean isCreated = usedBitmap.writeFile(sourceFileByte, sourceFileUri);

        File sourceFile = new File(sourceFileUri);

        if (isCreated && sourceFile.isFile()) {

            if (uploadServerUri != null) {
                int serverResponseCode = MainConstants.kConstantZero;
                try {

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(uploadServerUri.replaceAll(MainConstants.kConstantWhiteSpace,
                            MainConstants.kConstantreplaceUrl));
                    //Log.d("window", "url" + sourceFileUri);
                    // Open a HTTP connection to the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod(MainConstants.kConstantPostMethod);
                    // Log.d("window", "setConnectTimeout:" +
                    // conn.getConnectTimeout());
                    conn.setConnectTimeout(MainConstants.connectionPhotoTimeout);
                    // Log.d("window", "setConnectTimeout:" +
                    // conn.getConnectTimeout());
                    conn.setReadTimeout(MainConstants.readPhotoTimeout);
                    // conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty(MainConstants.kConstantEncrype, MainConstants.kConstantMultiPart);
                    conn.setRequestProperty(MainConstants.kConstantContentType,
                            MainConstants.kConstantMultiPart + MainConstants.kConstantBoundary + boundary);
                    conn.setRequestProperty(MainConstants.kConstantUploadFile, sourceFileUri);
                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes(MainConstants.kConstantContentDisposition + sourceFileUri
                            + MainConstants.kConstantDoubleQuotes + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // Log.d("window", "url" + sourceFileUri);
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, MainConstants.kConstantZero, bufferSize);

                    while (bytesRead > MainConstants.kConstantZero) {

                        dos.write(buffer, MainConstants.kConstantZero, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, MainConstants.kConstantZero, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();

                    //String serverResponseMessage = conn.getResponseMessage();
                    //Log.d("window", "serverResponseMessage" + serverResponseMessage);
                    uploadCode = serverResponseCode;
                    if (serverResponseCode == MainConstants.kConstantSuccessCode) {
                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(conn.getInputStream()));

                        StringBuffer response = new StringBuffer();
                        String line = null;

                        while ((line = in.readLine()) != null) {

                            response.append(line + MainConstants.kConstantNewLine);
                            json = response.toString();

                        }
                        if ((json != null) && (json.contains(StringConstants.creatorResponseSuccessFirst) || json.contains(StringConstants.creatorResponseSuccessSecond))) {
                            uploadCode = MainConstants.kConstantSuccessCode;
                        } else {

                            setInternalLogs(context, ErrorConstants.visitorUploadPhotoResponseError, json);
                            uploadCode = ErrorConstants.visitorUploadPhotoResponseError;
                        }
                    }
                    //Log.d("window", "serverResponseMessage" + uploadCode);
                    // close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                } catch (MalformedURLException ex) {

                    uploadCode = ErrorConstants.visitorUploadPhotoURLError;
                    setInternalLogs(context, uploadCode, ex.getLocalizedMessage());
                    //Log.d("window", "MalformedURLException u: " + ex.getMessage(), ex);
                } catch (FileNotFoundException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoNotFoundError;
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                    //Log.d("window", "FileNotFoundException: " + e.getMessage());
                } catch (ProtocolException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoProtocalError;
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                    //Log.d("window", "ProtocolException: u " + e.getMessage());
                } catch (IOException e) {
                    uploadCode = ErrorConstants.visitorUploadPhotoIOError;
                    //Log.d("window", "IOException:  u1" + e.getMessage());

                    if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                        uploadCode = ErrorConstants.visitorUploadPhotoTimeOutError;
                    }
                    //Log.d("window", "IOException:  u3" + e.getMessage());
                    if (!NetworkAnalytics.isNetworkAvailable(context)) {
                        uploadCode = ErrorConstants.visitorUploadPhotoNetworkError;
                    }
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                    //Log.d("window", "IOException:  u2" + e.getMessage());
                }
            }

        } // End else block
        return uploadCode;
    }

    /**
     * Upload json data to cloud
     *
     * @author Karthick
     * @created on 20161110
     */
    public int uploadThupFile(String uploadServerUri, String sourceFileUri) {

//         Log.d("DSK window", "url" + uploadServerUri);
        int uploadCode = MainConstants.kConstantMinusOne;
        Date date = new Date();
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = MainConstants.kConstantLineEnd;
        String twoHyphens = MainConstants.kConstantDoubleHyphen;
        String boundary = date.getTime() + MainConstants.kConstantEmpty;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = MainConstants.kConstantOne * MainConstants.kConstantBufferSize
                * MainConstants.kConstantBufferSize;
        File sourceFile = new File(sourceFileUri);
        if (sourceFile.isFile()) {
            UsedBitmap usedBitmap = new UsedBitmap();
            Bitmap bitmap = usedBitmap.decodeBitmap(sourceFileUri, 100, 178);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.PNG, 70, stream);
            if (uploadServerUri != null) {
                int serverResponseCode = MainConstants.kConstantZero;
                try {
                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(uploadServerUri.replaceAll(MainConstants.kConstantWhiteSpace,
                            MainConstants.kConstantreplaceUrl));
//                    Log.d("DSK window", "url" + url);
                    // Open a HTTP connection to the URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod(MainConstants.kConstantPostMethod);
//                     Log.d("DSK window", "setConnectTimeout:" +
//                     conn.getConnectTimeout());
                    conn.setConnectTimeout(MainConstants.connectionPhotoTimeout);
//                     Log.d("DSK window", "setConnectTimeout:" +
//                     conn.getConnectTimeout());
                    conn.setReadTimeout(MainConstants.readPhotoTimeout);
                    // conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty(MainConstants.kConstantEncrype, MainConstants.kConstantMultiPart);
                    conn.setRequestProperty(MainConstants.kConstantContentType,
                            MainConstants.kConstantMultiPart + MainConstants.kConstantBoundary + boundary);
                    conn.setRequestProperty(MainConstants.kConstantUploadFile, sourceFileUri);
                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes(MainConstants.kConstantContentDisposition + sourceFileUri
                            + MainConstants.kConstantDoubleQuotes + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, MainConstants.kConstantZero, bufferSize);
                    //buffer=stream.toByteArray();
                    while (bytesRead > MainConstants.kConstantZero) {

                        dos.write(buffer, MainConstants.kConstantZero, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, MainConstants.kConstantZero, bufferSize);

                    }
                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    // Responses from the server (code and message)
                    serverResponseCode = conn.getResponseCode();
//                    Log.d("DSK Post Method ","ResponseMessage "+conn.getResponseMessage());
//                    Log.d("DSK Post Method ","ErrorStream "+conn.getErrorStream());
                    //String serverResponseMessage = conn.getResponseMessage();
                    uploadCode = serverResponseCode;
                    if (serverResponseCode == MainConstants.kConstantSuccessCode) {

                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(conn.getInputStream()));

                        StringBuffer response = new StringBuffer();
                        String line = null;

                        while ((line = in.readLine()) != null) {

                            response.append(line + MainConstants.kConstantNewLine);
                            json = response.toString();

                        }
                        if ((json != null) && (json.contains(StringConstants.creatorResponseSuccessFirst) || json.contains(StringConstants.creatorResponseSuccessSecond))) {
                            uploadCode = MainConstants.kConstantSuccessCode;
                        } else {

                            setInternalLogs(context, ErrorConstants.visitorUploadThumbPhotoResponseError, json);
                            uploadCode = ErrorConstants.visitorUploadThumbPhotoResponseError;
                        }
                    }
                    // close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                } catch (MalformedURLException ex) {

                    uploadCode = ErrorConstants.visitorUploadThumbPhotoURLError;
                } catch (FileNotFoundException e) {
                    uploadCode = ErrorConstants.visitorUploadThumbPhotoNotFoundError;

                } catch (ProtocolException e) {
                    uploadCode = ErrorConstants.visitorUploadThumbPhotoProtocalError;

                } catch (IOException e) {
                    uploadCode = ErrorConstants.visitorUploadThumbPhotoIOError;

                    if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                        uploadCode = ErrorConstants.visitorUploadThumbPhotoTimeOutError;
                    }
                    //Log.d("window", "IOException:  u3" + e.getMessage());
                    if (!NetworkAnalytics.isNetworkAvailable(context)) {
                        uploadCode = ErrorConstants.visitorUploadThumbPhotoNetworkError;
                    }
                    if (sourceFile.exists()) {
                        sourceFile.delete();
                    }
                    //Log.d("window", "IOException:  u2" + e.getMessage());
                }
            }

        } // End else block
        return uploadCode;
    }

    /**
     * Created BY Jayapriya
     *
     * @return
     */
    public ArrayList<PurposeOfVisitRecord> getPurposeOfVisitorRecord() {
        //Log.d("window", "getpurpose");
        ArrayList<PurposeOfVisitRecord> visitorPurpose = new ArrayList<PurposeOfVisitRecord>();
        // s="authtoken=8a45d8964eddMainConstants.kConstantOne047e086973957ae5500&scope=creatorapi";
        // if (Singleton.getInstance().getLocationCode() !=
        // MainConstants.ConstantMinusOne) {
        try {
            //URL url;
            // Log.d("zohocreator", "purposeofvisitlocation"
            // + SingletonVisitorProfile.getInstance().getLocationCode());
            // url = new URL(MainConstants.kConstantViewRecord
            // + MainConstants.kConstantViewPurposeRecord
            // + MainConstants.kConstantViewPurposeRecordCriteria
            // + Singleton.getInstance().getLocationCode());
            // + Singleton.getInstance().getLocationCode());
            // Log.d("zohocreator", "purposeofvisitUrl" + url);
            jsonObject = jsonParser.getJsonArrayInGetMethod(MainConstants.kConstantViewRecord
                    + MainConstants.kConstantViewPurposeRecord + MainConstants.kConstantViewPurposeRecordCriteria
                    + SingletonVisitorProfile.getInstance().getLocationCode());
            // Log.d("creator", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.getJSONArray(MainConstants.kConstantPurposeRecord);
                // Log.d("creator", "jsonArray" + jsonArray);

                // jsonArray = jsonParser.getJsonArrayInGetMethod(url);
                // Log.d("check", "jsonArraylength" + jsonArray.length());
                if ((jsonArray == null) || (jsonArray.length() == MainConstants.kConstantZero)
                        || (jsonArray.toString() == MainConstants.kConstantEmpty)) {
                    // Log.d("purposeOfvisit", "jsonArraynull");
                } else {
                    for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
                        PurposeOfVisitRecord purposeOfVisitList = new PurposeOfVisitRecord();
                        purposeOfVisitList.purpose = jsonArray.getJSONObject(i)
                                .getString(MainConstants.kConstantPurposeRecord);

                        // Log.d("creator", "purposeOfVisit"
                        // + purposeOfVisitList.purpose);
                        purposeOfVisitList.visitFrequency = jsonArray.getJSONObject(i)
                                .getInt(MainConstants.kConstantFrequency);
                        // Log.d("creator", "purposeOfVisit"
                        // + purposeOfVisitList.visitFrequency);
                        purposeOfVisitList.location = jsonArray.getJSONObject(i)
                                .getInt(MainConstants.kConstantPurposeLocation);
                        // Log.d("creator", "purposeOfVisit"
                        // + purposeOfVisitList.location);
                        visitorPurpose.add(purposeOfVisitList);

                    }
                }
            }
            // Log.d("Android", "response:"+inputLine);

        }
        // Log.d("window", "response:"+response);

        // in.close();

        // print result

        catch (JSONException e) {

            //e.printStackTrace();
        } catch (IOException e) {

        } catch (TimeoutException e) {
            // uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
        }
        // }
        // Log.d("window", "size:"+visitorRecord.size());
        return visitorPurpose;
    }

    /**
     * Created BY karthick
     *
     * @return
     */
    public ArrayList<EmployeeFrequency> getEmployeeVisitFrequeny() {

        //Log.d("window", "freq:");
        ArrayList<EmployeeFrequency> employeeFrequencyList = new ArrayList<EmployeeFrequency>();

        try {

            jsonObject = jsonParser.getJsonArrayInGetMethod(
                    MainConstants.kConstantUtilityViewRecord + MainConstants.kConstantEmployeeFrequencyReport
                            + MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantRawAndOwnerName);
            //Log.d("creator", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.getJSONArray(MainConstants.kConstantEmployeeVisitFrequency);
                // Log.d("creator", "jsonArray" + jsonArray);

                // jsonArray = jsonParser.getJsonArrayInGetMethod(url);
                // Log.d("check", "jsonArraylength" + jsonArray.length());
                if ((jsonArray == null) || (jsonArray.length() == MainConstants.kConstantZero)
                        || (jsonArray.toString() == MainConstants.kConstantEmpty)) {
                    // Log.d("purposeOfvisit", "jsonArraynull");
                } else {
                    for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
                        EmployeeFrequency employeeFrequency = new EmployeeFrequency();
                        employeeFrequency.setEmployeeID(
                                jsonArray.getJSONObject(i).getString(MainConstants.kConstantEmployeeIDFrequency));

                        employeeFrequency.setVisitFrequency(
                                jsonArray.getJSONObject(i).getInt(MainConstants.kConstantEmployeeFrequency));

                        employeeFrequencyList.add(employeeFrequency);

                    }
                }
            }
            // Log.d("Android", "response:"+inputLine);

        }
        // Log.d("window", "response:"+response);

        // in.close();

        // print result

        catch (JSONException e) {

            //e.printStackTrace();
        } catch (IOException e) {

        } catch (TimeoutException e) {
            // uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
        }
        // }
        // Log.d("window", "size:"+visitorRecord.size());
        return employeeFrequencyList;
    }

    /**
     * Created BY karthick
     *
     * @return
     */
    public EmployeeFrequency getEmployeeVisitFrequenyCount(String employeeID) {

        //	https: // creator.zoho.com/api/json/copy-of-visitormanagement/view/Visit_Report?authtoken=e05d9de0d0fd1466825ef6bb74215072&scope=creatorapi&EmployeeID=CON1

        //Log.d("window", "freq:" + MainConstants.kConstantViewRecord + MainConstants.kConstantVisitReport
        //		+ MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantRawAndOwnerName);
        EmployeeFrequency employeeFrequency = null;

        try {

            jsonObject = jsonParser.getJsonArrayInGetMethod(MainConstants.kConstantViewRecord
                    + MainConstants.kConstantVisitReport + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantRawAndOwnerName + MainConstants.kConstantCreatorEmployeeId + employeeID);
            //Log.d("creator", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.getJSONArray(MainConstants.kConstantVisit);
                //Log.d("creator", "jsonArray" + jsonArray.length());
                employeeFrequency = new EmployeeFrequency();
                employeeFrequency.setEmployeeID(employeeID);

                employeeFrequency.setVisitFrequency(jsonArray.length());
            }
            // Log.d("Android", "response:"+inputLine);

        }
        // Log.d("window", "response:"+response);

        // in.close();

        // print result

        catch (JSONException e) {

            //e.printStackTrace();
        } catch (IOException e) {

        } catch (TimeoutException e) {
            // uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
        }
        // }
        // Log.d("window", "size:"+visitorRecord.size());
        return employeeFrequency;
    }

    /**
     * created by jayapriya on 25/10/14
     *
     * @return
     */
    public int searchVisitorRecord(Context context) {

        this.context = context;
        int uploadCode = MainConstants.kConstantMinusOne;
        // s="authtoken=8a45d8964eddMainConstants.kConstantOne047e086973957ae5500&scope=creatorapi";
        String id = null;
        String url;

        String contact;
        if (SingletonVisitorProfile.getInstance().getContact() != null) {
            contact = SingletonVisitorProfile.getInstance().getContact().replace(MainConstants.kConstantWhiteSpace,
                    MainConstants.kConstantEmpty);
            // Log.d("window","contact" +contact);
            if ((contact.matches(MainConstants.kConstantValidPhoneNo)) == true
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartmentAnotherNamePlural))
                    && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()
                    .equals(ApplicationController.getInstance().getResources()
                            .getString(R.string.radio_string_purpose_interview)))) {

                // Log.d("window","contact0" +contact);
                url = MainConstants.kConstantViewInterviewRecord + MainConstants.kConstantViewInterviewDetail
                        + MainConstants.kConstantInterviewDetailCriteriaInMobileNo
                        + SingletonVisitorProfile.getInstance().getPhoneNo() + MainConstants.kConstantDoubleQuotes;

            } else if ((contact.matches(MainConstants.kConstantValidPhoneNo)) == true
                    && (!(SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                    .getInstance().getResources().getString(R.string.radio_string_purpose_interview))))) {

                // Log.d("window","contact1" +contact);
                url = MainConstants.kConstantViewRecord + MainConstants.kConstantViewVisitorDetail
                        + MainConstants.kConstantVisitorDetailCriteriaInMobileNo
                        + SingletonVisitorProfile.getInstance().getPhoneNo() + MainConstants.kConstantDoubleQuotes;

            } else if ((!contact.matches(MainConstants.kConstantValidPhoneNo)) == true
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartmentAnotherNamePlural))
                    && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()
                    .equals(ApplicationController.getInstance().getResources()
                            .getString(R.string.radio_string_purpose_interview)))) {

                // Log.d("window","contact2" +contact);
                url = MainConstants.kConstantViewInterviewRecord + MainConstants.kConstantViewInterviewDetail
                        + MainConstants.kConstantInterviewDetailCriteriaInMailId + contact
                        + MainConstants.kConstantDoubleQuotes;
            } else {

                // Log.d("window","contact3" +contact);
                url = MainConstants.kConstantViewRecord + MainConstants.kConstantViewVisitorDetail
                        + MainConstants.kConstantVisitorDetailCriteriaInMailId + contact
                        + MainConstants.kConstantDoubleQuotes;

            }

            try {
                jsonObject = jsonParser.getJsonArrayInGetMethod(url);
                // Log.d("creator", "jsonObject" + jsonObject.toString());
                if (jsonObject != null) {

                    if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                            && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                            .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                            .equals(MainConstants.HRDepartmentAnotherNamePlural))
                            && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()
                            .equals(ApplicationController.getInstance().getResources()
                                    .getString(R.string.radio_string_purpose_interview))) {

                        jsonArray = jsonObject.getJSONArray(MainConstants.kConstantCreatorCandidate);
                    } else {

                        jsonArray = jsonObject.getJSONArray(MainConstants.kConstantVisitorDetail);
                    }
                    // Log.d("creator", "jsonArray" + jsonArray);

                    // jsonArray = jsonParser.getJsonArrayInGetMethod(url);
                    // Log.d("check", "jsonArraylength" + jsonArray.length());
                    if ((jsonArray == null) || (jsonArray.length() == MainConstants.kConstantZero)
                            || (jsonArray.toString() == MainConstants.kConstantEmpty)) {
                        // Log.d("check", "jsonArraynullUploadRecord");
                        // Log.d("upload", "uploadVisitorRecord");
                        uploadCode = uploadVisitorRecord();
                    } else {
                        for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
                            id = jsonArray.getJSONObject(i).getString(MainConstants.kConstantVisitorId);

                        }
                        SingletonVisitorProfile.getInstance().setVisitorCreatorId(id);
                        if ((SingletonVisitorProfile.getInstance().getVisitorCreatorId() != null)
                                && (SingletonVisitorProfile.getInstance()
                                .getVisitorCreatorId() != MainConstants.kConstantEmpty)) {
                            checkVisitorRecord(jsonArray);
                            // Log.d("upload", "updateVisitorRecord");
                            uploadCode = updateVisitorRecord();
                            // Log.d("window: ",
                            // "updateVisitorRecord:"+uploadCode);
                            if (uploadCode == MainConstants.kConstantMinusOne) {
                                // Log.d("upload",
                                // "updateVisitorRecorda:"+uploadCode);
                                if ((SingletonVisitorProfile.getInstance().getVisitorBusinessCard() != null)
                                        && (SingletonVisitorProfile.getInstance()
                                        .getVisitorBusinessCard() != MainConstants.kConstantEmpty)) {
                                    upLoadBussinessCardUri = MainConstants.kConstantFileUploadUrl
                                            + MainConstants.kConstantCreatorAuthTokenAndScope
                                            + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitorDetail
                                            + MainConstants.kConstantFileUploadFieldNameInBussinessCard
                                            + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                                            + MainConstants.kConstantFileAccessTypeAndName
                                            + SingletonVisitorProfile.getInstance().getVisitorName()
                                            + MainConstants.kConstantFileSharedBy;
                                    // uploadFile(upLoadBussinessCardUri,
                                    // SingletonVisitorProfile.getInstance()
                                    // .getVisitorBusinessCard());
                                    // Log.d("upload", "businesscard");

                                }

                                // Log.d("upload", "uploadVisitRecord2");
                                uploadCode = uploadVisitRecord();
                            }
                        }
                    }

                }

            } catch (JSONException e) {

                uploadCode = ErrorConstants.visitorSearchVisitorJSONError;
            } catch (IOException e) {
                uploadCode = ErrorConstants.visitorSearchVisitorURLError;
                if (!NetworkAnalytics.isNetworkAvailable(context)) {
                    uploadCode = ErrorConstants.visitorSearchVisitorNetworkError;
                }
                if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                    uploadCode = ErrorConstants.visitorSearchVisitorTimeOutError;
                }
                //Log.d("upload", "businesscard" + e);

            } catch (TimeoutException e) {
                uploadCode = ErrorConstants.visitorSearchVisitorTimeOutError;
            }

        }
        return uploadCode;
    }

    /**
     * created by jayapriya on 12/11/14
     */
    public void checkExistingVisitorRecord() {

        // s="authtoken=8a45d8964eddMainConstants.kConstantOne047e086973957ae5500&scope=creatorapi";
        //String id = null;
        String url;
        try {

            String contact;
            if (SingletonVisitorProfile.getInstance().getContact() != null) {
                contact = SingletonVisitorProfile.getInstance().getContact().replace(MainConstants.kConstantWhiteSpace,
                        MainConstants.kConstantEmpty);
                if (contact.matches(MainConstants.kConstantValidPhoneNo) == true) {
                    // if (contact.length() >= 9) {
                    // contact = contact.substring(contact.length() - 9,
                    // contact.length());

                    // }
                    //Log.d("check", "Contact" + contact);
                    url = // new URL(
                            MainConstants.kConstantViewRecord + MainConstants.kConstantViewVisitorDetail

                                    + MainConstants.kConstantVisitorDetailCriteriaInMobileNo + contact
                                    + MainConstants.kConstantDoubleQuotes;
                    //Log.d("search", "url" + url);

                } else {
                    //Log.d("check", "Contact" + contact);
                    url = // new URL(
                            MainConstants.kConstantViewRecord + MainConstants.kConstantViewVisitorDetail
                                    + MainConstants.kConstantVisitorDetailCriteriaInMailId + contact
                                    + MainConstants.kConstantDoubleQuotes;
                    //Log.d("search", "url" + url);

                }
                jsonObject = jsonParser.getJsonArrayInGetMethod(url);
                //Log.d("creator", "jsonObject" + jsonObject);
                if (jsonObject != null) {
                    jsonArray = jsonObject.getJSONArray(MainConstants.kConstantVisitorDetail);
                    //Log.d("creator", "jsonArray" + jsonArray);

                    // jsonArray = jsonParser.getJsonArrayInGetMethod(url);
                    //Log.d("check", "jsonArraylength" + jsonArray.length());
                    if ((jsonArray == null) || (jsonArray.length() == MainConstants.kConstantZero)
                            || (jsonArray.toString() == MainConstants.kConstantEmpty)) {
                        //Log.d("check", "jsonArraynullUploadRecord");

                    } else {
                        // for (int i = MainConstants.kConstantZero; i <
                        // jsonArray.length(); i++) {
                        checkVisitorRecord(jsonArray);

                    }

                }
            }

        } catch (JSONException e) {

            //e.printStackTrace();
        } catch (IOException e) {

        } catch (TimeoutException e) {
            // uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
        }
    }

    private void checkVisitorRecord(JSONArray jsonArray) {
        if (jsonArray != null) {
            try {

                String visitorName;
                //Log.d("compare", "comparerecord");
                visitorName = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantVisitorName);

//				String id = jsonArray.getJSONObject(MainConstants.kConstantZero)
//						.getString(MainConstants.kConstantVisitorId);

                String location = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantLocation);

                String company = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantCompany);

                String companywebsite = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantCompanyWebsite);

                String designation = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantDesignation);

                String mobileNo = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantMobileNo);

                String mailId = jsonArray.getJSONObject(MainConstants.kConstantZero)
                        .getString(MainConstants.kConstantMailId);

                String fax = jsonArray.getJSONObject(MainConstants.kConstantZero).getString(MainConstants.kConstantFax);

                if (SingletonVisitorProfile.getInstance() != null) {
                    Validation validation = new Validation();
                    SingletonVisitorProfile.getInstance().setAddress(validation.CompareCreatorRecord(location,
                            SingletonVisitorProfile.getInstance().getAddress()));
                    SingletonVisitorProfile.getInstance().setCompany(validation.CompareCreatorRecord(company,
                            SingletonVisitorProfile.getInstance().getCompany()));
                    SingletonVisitorProfile.getInstance().setCompanyWebSite(validation.CompareCreatorRecord(
                            companywebsite, SingletonVisitorProfile.getInstance().getCompanyWebsite()));
                    SingletonVisitorProfile.getInstance().setVisitorName(validation.CompareCreatorRecord(visitorName,
                            SingletonVisitorProfile.getInstance().getVisitorName()));
                    SingletonVisitorProfile.getInstance().setEmail(
                            validation.CompareCreatorRecord(mailId, SingletonVisitorProfile.getInstance().getEmail()));
                    SingletonVisitorProfile.getInstance().setPhoneNo(validation.CompareCreatorRecord(mobileNo,
                            SingletonVisitorProfile.getInstance().getPhoneNo()));
                    SingletonVisitorProfile.getInstance().setFax(
                            validation.CompareCreatorRecord(fax, SingletonVisitorProfile.getInstance().getFax()));
                    SingletonVisitorProfile.getInstance().setDesignation(validation.CompareCreatorRecord(designation,
                            SingletonVisitorProfile.getInstance().getDesignation()));

                }

                //Log.d("updateRecord", "updateRecord" + id);

            } catch (JSONException e) {

                //e.printStackTrace();
            }
        }
    }

    public int updateVisitorRecord() {

        int uploadCode = MainConstants.kConstantMinusOne;

        String setParameter = "";

        try {
            if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment().equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartmentAnotherNamePlural))
                    && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                    .getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {

                url = new URL(MainConstants.interviewApplication + MainConstants.kConstantUpdateCandidateDetail);

                setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                        + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                        + MainConstants.kConstantCreatorCandidateName
                        + SingletonVisitorProfile.getInstance().getVisitorName()
                        + MainConstants.kConstantCreatorCandidateLocation
                        + SingletonVisitorProfile.getInstance().getAddress()
                        + MainConstants.kConstantCreatorCandidateMobileNo
                        + SingletonVisitorProfile.getInstance().getPhoneNo()
                        + MainConstants.kConstantCreatorCandidateEmail
                        + SingletonVisitorProfile.getInstance().getEmail();

            } else {

                if (SingletonVisitorProfile.getInstance() != null && !SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty()
                        && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equalsIgnoreCase(MainConstants.purposeDataCenter)) {
                    url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantUpdateFrontdeskDCVisitorDetail);

                    setParameter = MainConstants.kConstantFrontdeskDCAuthTokenAndScope + MainConstants.kConstantCreatorVisitorId
                            + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                            + "visitor_name"
                            + SingletonVisitorProfile.getInstance().getVisitorName()
                            + "phone_number" + SingletonVisitorProfile.getInstance().getPhoneNo()
                    ;
//
//                    Log.d("DSK ", "updateVisitorRecord");
                } else {
                    url = new URL(MainConstants.application + MainConstants.kConstantUpdateVisitorDetail);
                    setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                            + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                            + MainConstants.kConstantCreatorVisitorName
                            + SingletonVisitorProfile.getInstance().getVisitorName()
                            + MainConstants.kConstantCreatorLocation + SingletonVisitorProfile.getInstance().getAddress()
                            + MainConstants.kConstantCreatorMobileNo + SingletonVisitorProfile.getInstance().getPhoneNo()
                            + MainConstants.kConstantCreatorEmail + SingletonVisitorProfile.getInstance().getEmail()
                            + MainConstants.kConstantCreatorDesignation
                            + SingletonVisitorProfile.getInstance().getDesignation() + MainConstants.kConstantCreatorCompany
                            + SingletonVisitorProfile.getInstance().getCompany()
                            + MainConstants.kConstantCreatorCompanyWebsite
                            + SingletonVisitorProfile.getInstance().getCompanyWebsite() + MainConstants.kConstantCreatorFax
                            + SingletonVisitorProfile.getInstance().getFax();
                }
            }

            // Log.d("VisitorZohoCreator", "Creator" + url + setParameter);
            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
            // Log.d("jsonObject", "jsonObject" + jsonObject.toString());
            if (jsonObject != null) {
                //Log.d("jsonObject", "jsonObject" + jsonObject);

                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
                //Log.d("jsonArray", "jsonArray" + jsonArray);
                /*
                 * JSONObject jsonSubObject = (JSONObject) jsonArray
                 * .getJSONObject(MainConstants.kConstantOne); JSONArray
                 * jsonSubArray = jsonSubObject
                 * .getJSONArray(MainConstants.kConstantOperation);
                 * Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
                 *
                 * Log.d("jsonArray", "jsonArrayst" + jsonSubArray
                 * .getJSONObject( MainConstants.kConstantOne) .getString(
                 * MainConstants.kConstantUpdate)); JSONObject
                 * jsonSubOfSubObject = (JSONObject) jsonSubArray
                 * .getJSONObject(MainConstants.kConstantOne);
                 * Log.d("jsonArray", "jsonArrayst" + jsonSubOfSubObject); //
                 * .getJSONObject(MainConstants.kConstantValues); JSONObject
                 * jsonSubOfSubArray = jsonSubOfSubObject
                 * .getJSONObject(MainConstants.kConstantValues); String
                 * visitorId = jsonSubOfSubArray
                 * .getString(MainConstants.kConstantVisitorId);
                 * Log.d("jsonArray", "jsonArraystst" + jsonSubOfSubArray
                 * .getString(MainConstants.kConstantVisitorId) + visitorId); //
                 * SingletonVisitorProfile
                 * .getInstance().setVisitorCreatorId(visitorId);
                 */

            }
        } catch (MalformedURLException e) {
            uploadCode = ErrorConstants.visitorUpdateVisitorURLError;
            //Log.d("jsonObject", "MalformedURLException" + e);
        } catch (IOException e) {
            //Log.d("jsonObject", "IOException" + e);
            uploadCode = ErrorConstants.visitorUpdateVisitorIOError;

            if (!NetworkAnalytics.isNetworkAvailable(context)) {
                uploadCode = ErrorConstants.visitorUpdateVisitorNetworkError;
            }
            if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                uploadCode = ErrorConstants.visitorUpdateVisitorTimeOutError;
            }
        } catch (JSONException e) {
            //Log.d("jsonObject", "JSONException" + e);
            uploadCode = ErrorConstants.visitorUpdateVisitorJSONError;
        }
        return uploadCode;
    }

    private void updateVisitRecordInQrcode() {

        String setParameter;
        setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantCreatorDateOfVisit
                + SingletonVisitorProfile.getInstance().getDateOfVisit()
                + MainConstants.kConstantCreatorDateOfVisitLocal
                + SingletonVisitorProfile.getInstance().getDateOfVisitLocal() + MainConstants.kConstantCreatorCodeNo
                + SingletonVisitorProfile.getInstance().getCodeNo() + MainConstants.kConstantCreatorTempID
                + SingletonVisitorProfile.getInstance().getTempIDNumber() + MainConstants.kConstantCreatorExpectedDate
                + "visited" + MainConstants.kConstantIsUpload + MainConstants.kConstantTrue;

        //Log.d("window", "visitId" + SingletonVisitorProfile.getInstance().getVisitId());
        if (SingletonVisitorProfile.getInstance().getVisitId() != null) {
            try {
                URL url;
                url = new URL(MainConstants.application + MainConstants.kConstantUpdateVisitDetail);
                // Log.d("VisitorZohoCreator", "Creator" + url);

                jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
                if (jsonObject != null) {
                    //Log.d("jsonObject", "jsonObject" + jsonObject);

                    /*
                     * jsonArray = jsonObject
                     * .optJSONArray(MainConstants.kConstantFormName);
                     * Log.d("jsonArray", "jsonArray" + jsonArray); JSONObject
                     * jsonSubObject = (JSONObject) jsonArray
                     * .getJSONObject(MainConstants.kConstantOne); JSONArray
                     * jsonSubArray = jsonSubObject
                     * .getJSONArray(MainConstants.kConstantOperation);
                     * Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
                     *
                     * Log.d("jsonArray", "jsonArrayst" + jsonSubArray
                     * .getJSONObject( MainConstants.kConstantOne) .getString(
                     * MainConstants.kConstantUpdate)); JSONObject
                     * jsonSubOfSubObject = (JSONObject) jsonSubArray
                     * .getJSONObject(MainConstants.kConstantOne);
                     * Log.d("jsonArray", "jsonArrayst" + jsonSubOfSubObject);
                     * // .getJSONObject(MainConstants.kConstantValues);
                     * JSONObject jsonSubOfSubArray = jsonSubOfSubObject
                     * .getJSONObject(MainConstants.kConstantValues); String
                     * visitId = jsonSubOfSubArray
                     * .getString(MainConstants.kConstantVisitId);
                     * Log.d("jsonArray", "jsonArraystst" + jsonSubOfSubArray
                     * .getString(MainConstants.kConstantVisitId) + visitId);
                     * //SingletonVisitorProfile
                     * .getInstance().setVisitId(visitId);
                     *
                     * Log.d("upload", "uploadfilesign" +
                     * SingletonVisitorProfile .getInstance().getVisitorSign());
                     */
                }
                // }
            } catch (MalformedURLException e) {

                // e.printStackTrace();
                // Log.d("window","url"+e);
            } catch (IOException e) {
            } catch (JSONException e) {
            }
        }
    }

    public int updateVisitRecordIsUpload() {
        String setParameter;
        int uploadCode = MainConstants.kConstantMinusOne;

        try {
            if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                    && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment().equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                    .equals(MainConstants.HRDepartmentAnotherNamePlural))
                    && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                    .getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {

                url = new URL(MainConstants.interviewApplication + MainConstants.kConstantUpdateInterviewDetail);
                setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                        + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantInterviewIsUpload
                        + MainConstants.kConstantTrue;
            } else {
                if (SingletonVisitorProfile.getInstance().getPurposeOfVisitCode() != null && !SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().isEmpty() && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equalsIgnoreCase(MainConstants.purposeDataCenter)) {
                    url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantUpdateFrontdeskDCVisitDetail);
                    setParameter = MainConstants.kConstantFrontdeskDCAuthTokenAndScope + MainConstants.kConstantCriteria
                            + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantIsUpload
                            + MainConstants.kConstantTrue;
//			}
                } else {
                    url = new URL(MainConstants.application + MainConstants.kConstantUpdateVisitDetail);
                    setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                            + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantIsUpload
                            + MainConstants.kConstantTrue;
                }
            }
            //Log.d("window", "Creator" + url);
            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
            // Log.d("update", "jsonObject" + jsonObject);

        } catch (MalformedURLException e) {
            uploadCode = ErrorConstants.visitorUploadCompletedTrueURLError;
            //Log.d("jsonObject", "MalformedURLException" + e);
        } catch (IOException e) {
            //Log.d("jsonObject", "IOException" + e);
            uploadCode = ErrorConstants.visitorUploadCompletedTrueIOError;

            if (!NetworkAnalytics.isNetworkAvailable(context)) {
                uploadCode = ErrorConstants.visitorUploadCompletedTrueNetworkError;
            }
            if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                uploadCode = ErrorConstants.visitorUploadCompletedTrueTimeOutError;
            }
        } catch (JSONException e) {
            //Log.d("jsonObject", "JSONException" + e);
            uploadCode = ErrorConstants.visitorUploadCompletedTrueJSONError;
        }
        return uploadCode;
    }

    /**
     * @param fieldName
     * @return
     */
    public int updateVisitJsonDataIsUpload(String ID, String fieldName, String purpose) {
        String setParameter;
        int uploadCode = MainConstants.kConstantMinusOne;

        try {

//			if(purpose.equals(MainConstants.interViewPurpose)) {
//				url = new URL(MainConstants.interviewApplication + MainConstants.kConstantUpdateAppJsonDetails);
//				setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
//						+ ID + fieldName
//						+ MainConstants.kConstantTrue;
//			} else {
//            Log.d("DSK ","getPurposeOfVisitCode "+SingletonVisitorProfile.getInstance().getPurposeOfVisitCode());
            SharedPreferenceController sharedPreferenceController = new SharedPreferenceController();
            int applicationMode = sharedPreferenceController.getApplicationMode(context);
            if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantUpdateAppJsonDetails);
                setParameter = MainConstants.kConstantFrontdeskDCAuthTokenAndScope + MainConstants.kConstantCriteria
                        + ID + fieldName
                        + MainConstants.kConstantTrue;
//			}
            } else {
                url = new URL(MainConstants.application + MainConstants.kConstantUpdateAppJsonDetails);
                setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                        + ID + fieldName
                        + MainConstants.kConstantTrue;
            }

            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
            if ((jsonObject != null) && (jsonObject.toString().contains(StringConstants.creatorResponseSuccessFirst) || jsonObject.toString().contains(StringConstants.creatorResponseSuccessSecond))) {
                uploadCode = MainConstants.kConstantSuccessCode;
            } else {
                setInternalLogs(context, ErrorConstants.visitorUploadCompletedTrueJSONError, jsonObject.toString());

                uploadCode = ErrorConstants.visitorUploadCompletedTrueJSONError;
            }

        } catch (MalformedURLException e) {
            setInternalLogs(context, ErrorConstants.visitorUploadCompletedTrueURLError, e.toString());

            uploadCode = ErrorConstants.visitorUploadCompletedTrueURLError;

        } catch (IOException e) {
            setInternalLogs(context, ErrorConstants.visitorUploadCompletedTrueIOError, e.toString());

            uploadCode = ErrorConstants.visitorUploadCompletedTrueIOError;

            if (!NetworkAnalytics.isNetworkAvailable(context)) {
                setInternalLogs(context, ErrorConstants.visitorUploadCompletedTrueNetworkError, e.toString());

                uploadCode = ErrorConstants.visitorUploadCompletedTrueNetworkError;
            }
            if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                setInternalLogs(context, ErrorConstants.visitorUploadCompletedTrueTimeOutError, e.toString());

                uploadCode = ErrorConstants.visitorUploadCompletedTrueTimeOutError;
            }
        } catch (JSONException e) {
            setInternalLogs(context, ErrorConstants.visitorUploadCompletedTrueJSONError, e.toString());

            uploadCode = ErrorConstants.visitorUploadCompletedTrueJSONError;
        }
        return uploadCode;
    }

    /**
     * created by jayapriya On 6/11 /14
     */
    public void searchPurposeRecord() {

        // s="authtoken=8a45d8964eddMainConstants.kConstantOne047e086973957ae5500&scope=creatorapi";
        String id = null;

//		URL url;
        //Log.d("window", "PurposeOfVisit" + SingletonVisitorProfile.getInstance().getPurposeOfVisit());
        try {
            // Log.d("window", "PurposeOfVisit"
            // + SingletonVisitorProfile.getInstance().getPurposeOfVisit());
            String purpose;
            Integer frequency;
            if (SingletonVisitorProfile.getInstance().getPurposeOfVisit() != null) {
                purpose = SingletonVisitorProfile.getInstance().getPurposeOfVisit();
                // / Log.d("window", "PurposeOfVisit" + purpose);
                /*
                 * url = new URL("https://creator.zoho.com/api/json/visitormgmt"
                 * + MainConstants.ConstantViewPurposeRecord +
                 * "&criteria=purposeOfVisit==\MainConstants.kConstantEmpty +
                 * purpose + "\MainConstants.kConstantEmpty);
                 */
                url = new URL(MainConstants.kConstantViewRecord + MainConstants.kConstantViewPurposeRecord
                        // +
                        // "&criteria=purposeOfVisit==\MainConstants.kConstantEmpty
                        // +
                        // purpose +"\MainConstants.kConstantEmpty);
                        + MainConstants.kConstantPurposeCriteria + Uri.encode(purpose, MainConstants.kConstantEncode)
                        + MainConstants.kConstantDoubleQuotes);
                // (Name == "Jean" && DOB ==
                // "20-Jul-MainConstants.kConstantOne98MainConstants.kConstantOne")
                // Log.d("search", "url" + url);

                jsonObject = jsonParser.getJsonArrayInGetMethod(MainConstants.kConstantViewRecord
                        + MainConstants.kConstantViewPurposeRecord + MainConstants.kConstantPurposeCriteria
                        + Uri.encode(purpose, MainConstants.kConstantEncode) + MainConstants.kConstantDoubleQuotes);
                // Log.d("creator", "jsonObject" + jsonObject);
                if ((jsonObject != null) && (jsonObject.toString() != MainConstants.kConstantEmpty)) {
                    jsonArray = jsonObject.optJSONArray(MainConstants.kConstantPurposeRecord);
                    // Log.d("creator", "jsonArray" + jsonArray);

                    // jsonArray = jsonParser.getJsonArrayInGetMethod(url);
                    // Log.d("check", "jsonArraylength" +
                    // jsonArray.length());
                    if ((jsonArray == null) || (jsonArray.length() == MainConstants.kConstantZero)
                            || (jsonArray.toString() == MainConstants.kConstantEmpty)) {
                        // Log.d("check", "jsonArraynullUploadRecord");
                        uploadPurposeRecord();

                    } else {
                        for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
                            if ((jsonArray.getJSONObject(i)
                                    .getInt(MainConstants.kConstantLocationCode)) == SingletonVisitorProfile
                                    .getInstance().getLocationCode()) {

                                id = jsonArray.getJSONObject(i).getString(MainConstants.kConstantPurposeId);
                                frequency = jsonArray.getJSONObject(i).getInt(MainConstants.kConstantVisitFrequency);

                                // Log.d("id", "purposeId" + id);
                                // Log.d("id", "frequency" + frequency);
                                // if ((locationCode != null)
                                // && (locationCode.getMessage() !=
                                // MainConstants.kConstantEmpty)
                                // && (locationCode == 2)) {
                                // Singleton.getInstance().setPurposeOfVisit(purpose);
                                // uploadPurposeRecord();

                                // } //else {
                                SingletonVisitorProfile.getInstance()
                                        .setVisitfrequency(frequency + MainConstants.kConstantOne);
                                SingletonVisitorProfile.getInstance().setPurposeId(id);
                                // updatePurposeRecord(id);

                            } else {
                                // Log.d("creator",
                                // "locationnulluploadpurpose");
                                // uploadPurposeRecord();
                            }

                        }

                        if ((SingletonVisitorProfile.getInstance().getPurposeId() == null) || (SingletonVisitorProfile
                                .getInstance().getPurposeId().toString() == MainConstants.kConstantEmpty)) {
                            // Log.d("creator", "locationnulluploadpurpose");
                            uploadPurposeRecord();
                        } else {
                            updatePurposeRecord(SingletonVisitorProfile.getInstance().getPurposeId());
                        }

                    }

                } else {
                    // Log.d("check", "jsonObjectnullUploadRecord");
                    uploadPurposeRecord();
                }
            }

            // if ((Singleton.getInstance().getVisitorCreatorId() != null)
            // && (Singleton.getInstance().getVisitorCreatorId().toString() !=
            // MainConstants.kConstantEmpty)) {
            // uploadVisitRecord();
            // }

        } catch (MalformedURLException e) {

            //e.printStackTrace();
            //Log.d("window", "MalformedURLException" + e);
        } // Log.d("window", "size:"+visitorRecord.size());
        catch (JSONException e) {

            //e.printStackTrace();
            //Log.d("window", "JSONException" + e);
        } catch (IOException e) {

        } catch (TimeoutException e) {
            // uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
        }

    }

    /**
     * created By Jayapriya On 6/11/14
     */
    public void uploadPurposeRecord() throws JSONException {

        String setParameter;
        visitFrequency = MainConstants.kConstantOne;
        //Log.d("window", "purpose" + SingletonVisitorProfile.getInstance().getPurposeOfVisit()
        //	+ MainConstants.kConstantWhiteSpace);
        setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantUploadPurposeRawWithId
                + SingletonVisitorProfile.getInstance().getPurposeOfVisit() + MainConstants.kConstantCreatorFrequency
                + visitFrequency + MainConstants.kConstantCreatorLocationCode
                + SingletonVisitorProfile.getInstance().getLocationCode();

        try {
            URL url;
            url = new URL(MainConstants.application + MainConstants.kConstantAddPurposeRecord);
            // Log.d("VisitorZohoCreator", "Creator" + url);

            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);

        } catch (MalformedURLException e) {

            // e.printStackTrace();
            //Log.d("window", "MalformedURLException" + e);
        } catch (IOException e) {
        }
    }

    /**
     * @param recordId
     */
    public void updatePurposeRecord(String recordId) {

        String setParameter;
        setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteria
                + SingletonVisitorProfile.getInstance().getPurposeId() + MainConstants.kConstantCreatorPurposeOfVisit
                + SingletonVisitorProfile.getInstance().getPurposeOfVisit() + MainConstants.kConstantCreatorFrequency
                + SingletonVisitorProfile.getInstance().getVisitfrequency() + MainConstants.kConstantCreatorLocationCode
                + SingletonVisitorProfile.getInstance().getLocationCode();

        try {
            url = new URL(MainConstants.application + MainConstants.kConstantUpdatePurposeDetail);
            // Log.d("VisitorZohoCreator", "Creator" + url);
            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
            // Log.d("VisitorZohoCreator", "jsonObject" + jsonObject);
            if ((jsonObject != null) && (jsonObject.toString() != MainConstants.kConstantEmpty)
                    && (jsonObject.length() > MainConstants.kConstantZero)) {
                // Log.d("VisitorZohoCreator", " update purposerecord");
            }

        } catch (MalformedURLException e) {

            //Log.d("window", "MalformedURLException" + e);
        } catch (IOException e) {
        } catch (JSONException e) {
        }

    }

    public int uploadVisitRecord() {

        int uploadCode = MainConstants.kConstantMinusOne;
        String setParameter = "";
        SharedPreferenceController sharedPreferenceController = new SharedPreferenceController();
        int applicationMode = sharedPreferenceController.getApplicationMode(context);
        // Log.d("window",""
        // +SingletonVisitorProfile.getInstance().getPurposeOfVisit());

        /*
         * setParameter =
         * "authtoken=8a45d8964edd1047e086973957ae5500&scope=creatorapi&visitorId="
         * + Singleton.getInstance().getVisitorCreatorId() + "&purpose=" +
         * "industry visit" + "&dateOfVisit=" + "10 th November 2015 at10.00"
         * +"&expectedDateOfVisit=" +MainConstants.kConstantEmpty +
         * "&GMTDiffMinutes=" + "340" + "&officeLocation=" + "chennai" +
         * "&officeLocationNumber=" + "7" + "&employeeName=" + "jaya" +
         * "&employeeID=" + "34398898383434343" + "&employeeEmailID=" +
         * "jaya@gmail.com" + "&codeNumber=" + "5" + "&Approval=" + "no";
         */

        if (SingletonVisitorProfile.getInstance().getVisitorCreatorId() != null) {
            try {
                URL url = new URL("");
                if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                        && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                        .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                        .equals(MainConstants.HRDepartmentAnotherNamePlural))
                        && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                        .getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {

                    url = new URL(MainConstants.interviewApplication + MainConstants.kConstantAddInterviewRecord);

                    setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.creatorInterviewId
                            + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                            + MainConstants.creatorInterviewcheckInTimeGmt
                            + SingletonVisitorProfile.getInstance().getDateOfVisit()
                            + MainConstants.creatorInterviewcheckInTimeLocal
                            + SingletonVisitorProfile.getInstance().getDateOfVisitLocal()
                            + MainConstants.creatorInterviewLocationCode
                            + SingletonVisitorProfile.getInstance().getLocationCode()
                            + MainConstants.creatorInterviewEmployeeName
                            + SingletonEmployeeProfile.getInstance().getEmployeeName()
                            + MainConstants.creatorInterviewEmployeeId
                            + SingletonEmployeeProfile.getInstance().getEmployeeId()
                            + MainConstants.creatorInterviewEmployeeZuid
                            + SingletonEmployeeProfile.getInstance().getEmployeeZuid()
                            + MainConstants.creatorInterviewEmployeeEmailId
                            + SingletonEmployeeProfile.getInstance().getEmployeeMailId()
                            + MainConstants.creatorInterviewEmployeePhoneNo
                            + SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
                            + MainConstants.creatorInterviewSeatingLocation
                            + SingletonEmployeeProfile.getInstance().getEmployeeSeatingLocation()
                            + MainConstants.creatorInterviewEmployeeExtention
                            + SingletonEmployeeProfile.getInstance().getEmployeeExtentionNumber()
                            + MainConstants.creatorInterviewCodeNo + SingletonVisitorProfile.getInstance().getCodeNo()
                            + MainConstants.creatorInterviewTempID
                            + SingletonVisitorProfile.getInstance().getTempIDNumber()
                            + MainConstants.creatorInterviewApproval + MainConstants.approval;
                } else {

                    if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                        url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantAddFrontdeskDCVisitRecord);
                        setParameter = MainConstants.kConstantFrontdeskDCAuthTokenAndScope + MainConstants.kConstantCreatorVisitorId
                                + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                                + MainConstants.kConstantCreatorDateOfVisitLocal
                                + SingletonVisitorProfile.getInstance().getDateOfVisitLocal()
                                + MainConstants.kConstantCreatorOfficeLocation
                                + SingletonVisitorProfile.getInstance().getOfficeLocation()
                                + MainConstants.kConstantCreatorLocationNo
                                + SingletonVisitorProfile.getInstance().getLocationCode()
                                + "employee_email_id"
                                + SingletonEmployeeProfile.getInstance().getEmployeeMailId()
                                + "visitor_temp_id"
                                + "DAC-" + SingletonVisitorProfile.getInstance().getTempIDNumber()
                                + MainConstants.kConstantCreatorPurpose1
                                + SingletonVisitorProfile.getInstance().getPurposeOfVisitCode();
//                        Log.d("Dsk ","uploadVisitRecord");
                    } else {
                        url = new URL(MainConstants.application + MainConstants.kConstantAddVisitRecord);

                        setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCreatorVisitorId
                                + SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                                + MainConstants.kConstantCreatorPurpose
                                + SingletonVisitorProfile.getInstance().getPurposeOfVisit()
                                + MainConstants.kConstantCreatorDateOfVisit
                                + SingletonVisitorProfile.getInstance().getDateOfVisit()
                                + MainConstants.kConstantCreatorDateOfVisitLocal
                                + SingletonVisitorProfile.getInstance().getDateOfVisitLocal()
                                + MainConstants.kConstantCreatorGMT + SingletonVisitorProfile.getInstance().getGmtdiff()
                                + MainConstants.kConstantCreatorOfficeLocation
                                + SingletonVisitorProfile.getInstance().getOfficeLocation()
                                + MainConstants.kConstantCreatorLocationNo
                                + SingletonVisitorProfile.getInstance().getLocationCode()
                                + MainConstants.kConstantCreatorEmployeeName
                                + SingletonEmployeeProfile.getInstance().getEmployeeName()
                                + MainConstants.kConstantCreatorEmployeeId
                                + SingletonEmployeeProfile.getInstance().getEmployeeId()
                                + MainConstants.creatorInterviewEmployeeZuid
                                + SingletonEmployeeProfile.getInstance().getEmployeeZuid()
                                + MainConstants.kConstantCreatorEmployeeEmailId
                                + SingletonEmployeeProfile.getInstance().getEmployeeMailId()
                                + MainConstants.kConstantCreatorEmployeePhoneNo
                                + SingletonEmployeeProfile.getInstance().getEmployeeMobileNo()
                                + MainConstants.kConstantCreatorSeatingLocation
                                + SingletonEmployeeProfile.getInstance().getEmployeeSeatingLocation()
                                + MainConstants.kConstantCreatorEmployeeExtention
                                + SingletonEmployeeProfile.getInstance().getEmployeeExtentionNumber()
                                + MainConstants.kConstantCreatorCodeNo + SingletonVisitorProfile.getInstance().getCodeNo()
                                + MainConstants.kConstantCreatorTempID
                                + SingletonVisitorProfile.getInstance().getTempIDNumber()
                                + MainConstants.kConstantCreatorExpectedDate + "visited"
                                + MainConstants.kConstantCreatorApproval + MainConstants.approval
                                + MainConstants.kConstantCreatorPurpose1
                                + SingletonVisitorProfile.getInstance().getPurposeOfVisitCode();
                    }
                }

                //Log.d("VisitorZohoCreator", "Creator" + url);

                jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
                // Log.d("jsonObject", "jsonObject" + jsonObject.toString());
                if (jsonObject != null) {
                    //Log.d("jsonObject", "jsonObject" + jsonObject);

                    jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
                    // Log.d("jsonArray", "jsonArray" + jsonArray);
                    JSONObject jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
                    JSONArray jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
                    // Log.d("jsonArray", "jsonArrayst" + jsonSubArray);

                    JSONObject jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
                    // .getJSONObject(MainConstants.kConstantValues);
                    JSONObject jsonSubOfSubArray = jsonSubOfSubObject.getJSONObject(MainConstants.kConstantValues);
                    String visitId = jsonSubOfSubArray.getString(MainConstants.kConstantVisitId);

                    SingletonVisitorProfile.getInstance().setVisitId(visitId);
                    if ((SingletonVisitorProfile.getInstance().getVisitId() != null) && (SingletonVisitorProfile.getInstance().getVisitId().toString() != MainConstants.kConstantEmpty)) {

                        if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                                && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                                .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                                .equals(MainConstants.HRDepartmentAnotherNamePlural))
                                && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()
                                .equals(ApplicationController.getInstance().getResources()
                                        .getString(R.string.radio_string_purpose_interview))) {

                            upLoadVisitorImageUri = MainConstants.kConstantFileUploadUrl
                                    + MainConstants.kConstantCreatorAuthTokenAndScope
                                    + MainConstants.fileUploadAppNameAndFormNameInInterviewDetail
                                    + MainConstants.kConstantFileUploadFieldNameInInterviewImage
                                    + SingletonVisitorProfile.getInstance().getVisitId()
                                    + MainConstants.kConstantFileAccessTypeAndName
                                    + SingletonVisitorProfile.getInstance().getVisitId()
                                    + MainConstants.creatorPhotoName + MainConstants.kConstantImageFileFormat
                                    + MainConstants.kConstantFileSharedBy;
                        } else {
                            if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                                upLoadVisitorImageUri = MainConstants.kConstantFileUploadUrl
                                        + MainConstants.kConstantFrontdeskDCAuthTokenAndScope
                                        + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetailFrontDeskDc
                                        + MainConstants.kConstantFileUploadFieldNameInVisitorImage
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.kConstantFileAccessTypeAndName
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.creatorPhotoName + MainConstants.kConstantImageFileFormat
                                        + MainConstants.kConstantFileSharedByFrontdeskDC;
                            } else {
                                upLoadVisitorImageUri = MainConstants.kConstantFileUploadUrl
                                        + MainConstants.kConstantCreatorAuthTokenAndScope
                                        + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetail
                                        + MainConstants.kConstantFileUploadFieldNameInVisitorImage
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.kConstantFileAccessTypeAndName
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.creatorPhotoName + MainConstants.kConstantImageFileFormat
                                        + MainConstants.kConstantFileSharedBy;
                            }
                        }
                        //Log.d("window: ", "uploadedCode:" + uploadCode);
                        int photoUploadCode = uploadFile(upLoadVisitorImageUri,
                                SingletonVisitorProfile.getInstance().getVisitorImage());
                        if (photoUploadCode != MainConstants.kConstantSuccessCode) {
                            uploadCode = photoUploadCode;
                        }
                        //Log.d("window: ", "uploadedCode:" + uploadCode);

                        if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                                && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                                .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                                .equals(MainConstants.HRDepartmentAnotherNamePlural))
                                && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode()
                                .equals(ApplicationController.getInstance().getResources()
                                        .getString(R.string.radio_string_purpose_interview))) {

                            upLoadVisitorSignUri = MainConstants.kConstantFileUploadUrl
                                    + MainConstants.kConstantCreatorAuthTokenAndScope
                                    + MainConstants.fileUploadAppNameAndFormNameInInterviewDetail
                                    + MainConstants.kConstantFileUploadFieldNameInInterviewSign
                                    + SingletonVisitorProfile.getInstance().getVisitId()
                                    + MainConstants.kConstantFileAccessTypeAndName
                                    + SingletonVisitorProfile.getInstance().getVisitId()
                                    + MainConstants.creatorSignatureName + MainConstants.kConstantImageFileFormat
                                    + MainConstants.kConstantFileSharedBy;
                        } else {
                            if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                                upLoadVisitorImageUri = MainConstants.kConstantFileUploadUrl
                                        + MainConstants.kConstantFrontdeskDCAuthTokenAndScope
                                        + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetailFrontDeskDc
                                        + MainConstants.kConstantFileUploadFieldNameInVisitorSign
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.kConstantFileAccessTypeAndName
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.creatorSignatureName + MainConstants.kConstantImageFileFormat
                                        + MainConstants.kConstantFileSharedByFrontdeskDC;
                            } else {
                                upLoadVisitorSignUri = MainConstants.kConstantFileUploadUrl
                                        + MainConstants.kConstantCreatorAuthTokenAndScope
                                        + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetail
                                        + MainConstants.kConstantFileUploadFieldNameInVisitorSign
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.kConstantFileAccessTypeAndName
                                        + SingletonVisitorProfile.getInstance().getVisitId()
                                        + MainConstants.creatorSignatureName + MainConstants.kConstantImageFileFormat
                                        + MainConstants.kConstantFileSharedBy;
                            }
                        }
                        int uploadSignatureError = uploadFile(upLoadVisitorSignUri,
                                SingletonVisitorProfile.getInstance().getVisitorSign());
                        if (uploadSignatureError != MainConstants.kConstantSuccessCode) {
                            if (uploadCode <= 0) {
                                // uploadCode=ErrorConstants.visitorUploadSignatureError;
                            } else {
                                uploadCode += ErrorConstants.visitorUploadSignatureError;
                            }
                        }
                        // Log.d("window: ", "uploadedCode:"+uploadCode);

                        int uploadCompletedTrueCode = updateVisitRecordIsUpload();
                        if (uploadCompletedTrueCode != MainConstants.kConstantMinusOne) {
                            if (uploadCode <= 0) {
                                uploadCode = uploadCompletedTrueCode;
                            } else {
                                uploadCode += uploadCompletedTrueCode;
                            }
                        }
                        // Log.d("window: ", "uploadedCode:"+uploadCode);
                    }
                    //Log.d("window: ", "uploadedCode:" + uploadCode);
                }
            } catch (MalformedURLException e) {
                uploadCode = ErrorConstants.visitorUploadVisitURLError;
                //Log.d("jsonObject", "MalformedURLExceptiona" + e);
            } catch (IOException e) {
                //Log.d("jsonObject", "IOExceptiona" + e);
                uploadCode = ErrorConstants.visitorUploadVisitIOError;

                if (!NetworkAnalytics.isNetworkAvailable(context)) {
                    uploadCode = ErrorConstants.visitorUploadVisitNetworkError;
                }
                if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                    uploadCode = ErrorConstants.visitorUploadVisitTimeOutError;
                }
            } catch (JSONException e) {
                //Log.d("jsonObject", "JSONExceptiona" + e);
                uploadCode = ErrorConstants.visitorUploadVisitJSONError;
            }
        }
        //Log.d("window: ", "uploadedCode add:" + uploadCode);
        return uploadCode;
    }

    // Async Task Class

    class UploadFilesInVisitRecord extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            upLoadVisitorImageUri = MainConstants.kConstantFileUploadUrl + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetail
                    + MainConstants.kConstantFileUploadFieldNameInVisitorImage
                    + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantFileAccessTypeAndName
                    + MainConstants.kConstantFileSharedBy;
            if (upLoadVisitorImageUri != null) {
                uploadFile(upLoadVisitorImageUri, SingletonVisitorProfile.getInstance().getVisitorImage());
            }

            upLoadVisitorSignUri = MainConstants.kConstantFileUploadUrl + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetail
                    + MainConstants.kConstantFileUploadFieldNameInVisitorSign
                    + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantFileAccessTypeAndName
                    + MainConstants.kConstantFileSharedBy;
            if (upLoadVisitorSignUri != null) {
                uploadFile(upLoadVisitorSignUri, SingletonVisitorProfile.getInstance().getVisitorSign());
            }
            //Log.d("upload", "uploadfile");

            updateVisitRecordIsUpload();

            return null;
        }

    }

    public void uploadBusinessCard(String url, String visitorId, HashMap<String, String> visitorRecord) {

        //Log.d("Background", "UploadBusinessCardAndVisitRecordInWifiOn");
        // uploadVisitRecordIntoCreator(visitorRecord,
        // VisitorIdInCreator);
        if ((visitorRecord.get(DataBaseConstants.visitorBusinessCard) != null)
                && (visitorRecord.get(DataBaseConstants.visitorBusinessCard) != MainConstants.kConstantEmpty)) {

            upLoadBussinessCardUri = MainConstants.kConstantFileUploadUrl + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitorDetail
                    + MainConstants.kConstantFileUploadFieldNameInBussinessCard
                    + visitorRecord.get(DataBaseConstants.visitorBusinessCard)
                    + MainConstants.kConstantFileAccessTypeAndName + MainConstants.kConstantFileSharedBy;
            uploadFile(upLoadBussinessCardUri, visitorRecord.get(DataBaseConstants.visitorBusinessCard));
            //Log.d("upload", "uploadbusinesscard");

        }

    }

    public static ArrayList<VisitorRecord> getVisitorRecord() {
        JSONObject jsonObject;
        JSONArray jsonArray;
        ArrayList<VisitorRecord> visitorRecordList = new ArrayList<VisitorRecord>();
        try {
            jsonObject = jsonParser.getJsonArrayInGetMethod(
                    MainConstants.kConstantViewRecord + MainConstants.kConstantViewVisitorDetail);

            // Log.d("url","" +MainConstants.kConstantViewRecord
            // + MainConstants.kConstantViewVisitorDetail);
            // Log.d("new","https://creator.zoho.com/api/json/visitormanagement/view/Visitor_Report?authtoken=e05d9de0d0fd1466825ef6bb74215072&scope=creatorapi&raw=true&zc_ownername=frontdesk10");
            // jsonObject = jsonParser
            // .getJsonArrayInGetMethod("https://creator.zoho.com/api/json/visitormanagement/view/"+"Visitor_Report?authtoken=e05d9de0d0fd1466825ef6bb74215072&scope=creatorapi&raw=true&zc_ownername=frontdesk10");
            // jsonObject = jsonParser
            // .getJsonArrayInGetMethod("https://people.zoho.com/people/api/searchbyemailIds?authtoken=c369a17d7cab82a61a0f82748228e406&values=mail.priya@zoho.com");
            // Log.d("creator", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.getJSONArray(MainConstants.kConstantVisitorDetail);
                // Log.d("creator", "jsonArray" + jsonArray);

                // Log.d("check", "jsonArraylength" + jsonArray.length());

                if ((jsonArray != null) && (jsonArray.length() != MainConstants.kConstantZero)
                        && (jsonArray.toString() != MainConstants.kConstantEmpty)) {
                    // Log.d("jsonArray","array" +jsonArray);
                    for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
                        VisitorRecord visitorList = new VisitorRecord();
                        visitorList.setRecordId(jsonArray.getJSONObject(i).getLong(DataBaseConstants.visitorRecordId));
                        // Log.d("recordId","" +visitorList.getRecordId());
                        visitorList.setName(jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorName));
                        // Log.d("visitorList.name","" +visitorList.getName());
                        visitorList.setAddress(jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorAddress));
                        // Log.d("visitorList.Address",""
                        // +visitorList.getAddress());
                        visitorList
                                .setMobileNo(jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorMobileNo));
                        // Log.d("visitorList.mobileNo",""
                        // +visitorList.getMobileNo());
                        visitorList.setMailId(jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorMailId));
                        // Log.d("visitorList.mailId",""
                        // +visitorList.getMailId());
                        visitorList.setDesignation(
                                jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorDesignation));
                        // Log.d("visitorList.designation",""
                        // +visitorList.getDesignation());
                        visitorList.setCompany(jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorCompany));
                        // Log.d("visitorList.company",""
                        // +visitorList.getCompany());
                        visitorList.setCompanyWebsite(
                                jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorCompanyWebsite));
                        // Log.d("visitorList.companyWebsite",""
                        // +visitorList.getCompanyWebsite());
                        visitorList.setFax(jsonArray.getJSONObject(i).getString(DataBaseConstants.visitorFax));
                        // Log.d("visitorList.fax","" +visitorList.getFax());
                        visitorRecordList.add(visitorList);

                    }
                }
            }
        } catch (JSONException e) {
            jsonObject = null;
        } catch (IOException e) {

        } catch (TimeoutException e) {
            // uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError;
        }
        return visitorRecordList;

    }

    /**
     * created by jayapriya on 25/10/14
     *
     * @return
     * @throws JSONException
     * @throws IOException
     */
    public JSONObject searchVisitRecordWithVisitId(String visitId) throws IOException, JSONException, TimeoutException {

        // s="authtoken=8a45d8964eddMainConstants.kConstantOne047e086973957ae5500&scope=creatorapi";
        String id = null;
        // URL url;
        //Log.d("visitId", "" + visitId + " " + url);
//		String contact;
        if ((visitId != null) && (visitId != MainConstants.kConstantEmpty)
                && (visitId.length() > MainConstants.kConstantZero)) {

            jsonObject = jsonParser.getJsonArrayInGetMethod(MainConstants.kConstantViewRecord
                    + MainConstants.kConstantViewVisitDetail + MainConstants.kConstantCriteria + visitId);
            //Log.d("creator", "jsonObject" + jsonObject);
        }
        return jsonObject;

    }

    public void updateCreatorInQrcode() {
        if ((SingletonVisitorProfile.getInstance().getVisitId() != null)
                && (SingletonVisitorProfile.getInstance().getVisitId() != MainConstants.kConstantEmpty)
                && (SingletonVisitorProfile.getInstance().getVisitId().length() > MainConstants.kConstantZero)
                && (SingletonVisitorProfile.getInstance().getVisitorCreatorId() != null)
                && (SingletonVisitorProfile.getInstance().getVisitorCreatorId() != MainConstants.kConstantEmpty)
                && (SingletonVisitorProfile.getInstance().getVisitorCreatorId()
                .length() > MainConstants.kConstantZero)) {

            updateVisitorRecord();

            upLoadVisitorImageUri = MainConstants.kConstantFileUploadUrl + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetail
                    + MainConstants.kConstantFileUploadFieldNameInVisitorImage
                    + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantFileAccessTypeAndName
                    + MainConstants.kConstantFileSharedBy;
            uploadFile(upLoadVisitorImageUri, SingletonVisitorProfile.getInstance().getVisitorImage());

            upLoadVisitorSignUri = MainConstants.kConstantFileUploadUrl + MainConstants.kConstantCreatorAuthTokenAndScope
                    + MainConstants.kConstantFileUploadAppNameAndFormNameInVisitDetail
                    + MainConstants.kConstantFileUploadFieldNameInVisitorSign
                    + SingletonVisitorProfile.getInstance().getVisitId() + MainConstants.kConstantFileAccessTypeAndName
                    + SingletonVisitorProfile.getInstance().getVisitorName() + MainConstants.kConstantFileSharedBy;
            uploadFile(upLoadVisitorSignUri, SingletonVisitorProfile.getInstance().getVisitorSign());

            updateVisitRecordInQrcode();
        }
    }

    /**
     * created By Jayapriya On 6/11/14
     */
    public void uploadEmployeeVisitFrequency(String employeeID) throws JSONException {

        String setParameter;
        setParameter = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantRaw
                + MainConstants.kConstantEmployeeIDAdd + employeeID;

        try {
            URL url;
            url = new URL(MainConstants.kConstantUtilityApplication + MainConstants.kConstantAddEmployeeFrequency);
            // Log.d("VisitorZohoCreator", "Creator" + url);

            jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
            //Log.d("VisitorZohoCreator", "Creator" + jsonObject);
        } catch (MalformedURLException e) {

            // e.printStackTrace();
            //Log.d("window", "MalformedURLException" + e);
        } catch (IOException e) {
        }
    }

    public ArrayList<Stationery> processStationery(String response) {

        // Log.d("response", "stationeryItemList" + response);
        ArrayList<Stationery> stationeryItemList = null;

        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantStationeryFormName);
            } else {
                return null;
            }
            if (jsonArray != null) {
                stationeryItemList = new ArrayList<Stationery>();
                // Log.d("jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {

                    // Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    Stationery stationeryItem = new Stationery();

                    try {
                        stationeryItem.setStationeryCode(jsonArray.getJSONObject(i).getInt("code"));
                        stationeryItem.setCreatorId(jsonArray.getJSONObject(i).getLong("ID"));
                        // Log.d("inside", "for" + jsonArray.length() + "id "
                        // + jsonArray.getJSONObject(i).getString("ID"));

                    } catch (JSONException e) {

                    }

                    try {
                        stationeryItem.setStationeryName(jsonArray.getJSONObject(i).getString("name"));
                    } catch (JSONException e1) {
                        stationeryItem.setStationeryName(MainConstants.kConstantEmpty);

                    }

                    try {
                        stationeryItem.setLimitPerMonth(jsonArray.getJSONObject(i).getInt("limit_per_month"));

                    } catch (JSONException e) {
                        stationeryItem.setLimitPerMonth(MainConstants.kConstantZero);

                    }
                    try {
                        stationeryItem.setStationeryLocationCode(jsonArray.getJSONObject(i).getInt("location_code"));

                    } catch (JSONException e) {
                        stationeryItem.setStationeryLocationCode(MainConstants.kConstantZero);

                    }
                    /*
                     * try { Matcher matcher = Pattern.compile(
                     * MainConstants.kConstantImgPatternFormat) .matcher(
                     * jsonArray.getJSONObject(i).getString( "image")); while
                     * (matcher.find()) { String filename =
                     * matcher.group(1).substring(
                     * matcher.group(1).lastIndexOf("/") + 1); Log.d("text",
                     * "stationeryItemList image: "
                     * +filename+" "+MainConstants.kConstantFileDirectory
                     * +MainConstants
                     * .kConstantFileStationeryImageFolder+filename);
                     * stationeryItem .setStationeryImage(
                     * MainConstants.kConstantFileDirectory +
                     * MainConstants.kConstantFileStationeryImageFolder +
                     * filename //+MainConstants.kConstantImageFileFormat );
                     * downloadImage(filename); } } catch (JSONException e) {
                     * stationeryItem
                     * .setStationeryImage(MainConstants.kConstantEmpty);
                     *
                     * }
                     */

                    stationeryItemList.add(stationeryItem);

                }
            }

        }
        return stationeryItemList;

    }

    /**
     * New Stationery Request List Drag and Drop View.
     * <p>
     * created by Karthikeyan D S
     * On 25/05/2018
     */
    public ArrayList<Stationery> newProcessStationeryList(String response) {

//         Log.d("dsk response", "stationeryItemList" + response);
        ArrayList<Stationery> stationeryItemList = null;

        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantLocationStationeryMappingFormName);
            } else {
                return null;
            }
            if (jsonArray != null) {
                stationeryItemList = new ArrayList<Stationery>();
//                 Log.d("jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {

//                     Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    Stationery stationeryItem = new Stationery();
                    try {
                        stationeryItem.setStationeryCode(jsonArray.getJSONObject(i).getInt("stationery_name.code"));
                        stationeryItem.setCreatorId(jsonArray.getJSONObject(i).getLong("ID"));
                        // Log.d("inside", "for" + jsonArray.length() + "id "
                        // + jsonArray.getJSONObject(i).getString("ID"));

                    } catch (JSONException e) {

                    }
                    try {
                        stationeryItem.setStationeryName(jsonArray.getJSONObject(i).getString("stationery_name.name"));
                    } catch (JSONException e1) {
                        stationeryItem.setStationeryName(MainConstants.kConstantEmpty);

                    }
                    try {
                        stationeryItem.setLimitPerMonth(jsonArray.getJSONObject(i).getInt("stationery_name.limit_per_month"));

                    } catch (JSONException e) {
                        stationeryItem.setLimitPerMonth(MainConstants.kConstantZero);

                    }
                    try {
                        stationeryItem.setStationeryLocationCode(jsonArray.getJSONObject(i).getInt("stationery_name.location_code"));

                    } catch (JSONException e) {
                        stationeryItem.setStationeryLocationCode(MainConstants.kConstantZero);

                    }
                    try {
                        stationeryItem.setOfficeDeviceLocation(jsonArray.getJSONObject(i).getString("office_device_location.location_name"));

                    } catch (JSONException e) {
                        stationeryItem.setOfficeDeviceLocation(MainConstants.kConstantEmpty);
                    }

                    try {
                        stationeryItem.setOfficeParentLocation(jsonArray.getJSONObject(i).getString("office_device_location.office_location"));

                    } catch (JSONException e) {
                        stationeryItem.setOfficeParentLocation(MainConstants.kConstantEmpty);

                    }

                    stationeryItemList.add(stationeryItem);

                }
            }

        }
        return stationeryItemList;

    }

    /**
     * ForgetID Zoho Creator Not CheckOut
     * <p>
     * created by Karthikeyan D S
     * On 25/05/2018
     */
    public ArrayList<EmployeeCheckInOut> forgetIdZohoCreatorNotCheckOutParse(String response) {

//         Log.d("dsk response", "forgetIDNotCheckOutDetails" + response);
        ArrayList<EmployeeCheckInOut> forgetIDNotCheckOutDetails = null;

        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantEmployeeForgetID);
            } else {
                return null;
            }
            if (jsonArray != null) {
                forgetIDNotCheckOutDetails = new ArrayList<EmployeeCheckInOut>();
//                 Log.d("jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
//                     Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    long checkInOutTimeLong = NumberConstants.kConstantMinusOneLong;
                    EmployeeCheckInOut employeeCheckInOut = new EmployeeCheckInOut();
                    try {
                        employeeCheckInOut.setEmployeeEmailID(jsonArray.getJSONObject(i).getString("employee_email_id"));
                    } catch (JSONException e) {

                    }
                    try {
                        employeeCheckInOut.setCheckInTimeDate(jsonArray.getJSONObject(i).getString("check_in_time"));
                        checkInOutTimeLong = TimeZoneConventer.getDateTimeInMilliSeconds
                                (jsonArray.getJSONObject(i).getString("check_in_time"), MainConstants.creatorDateFormatForgetID);
                        employeeCheckInOut.setCheckInTime(checkInOutTimeLong);
//                        Log.d("DSK "," setCheckInTime "+checkInOutTimeLong);
//                        Log.d("DSK "," setCheckInTime "+jsonArray.getJSONObject(i).getString("check_in_time"));
                        checkInOutTimeLong = NumberConstants.kConstantMinusOneLong;
                    } catch (JSONException e1) {

                    }
                    try {
                        employeeCheckInOut.setCheckOutTimeDate(jsonArray.getJSONObject(i).getString("check_out_time"));
                        checkInOutTimeLong = TimeZoneConventer.getDateTimeInMilliSeconds
                                (jsonArray.getJSONObject(i).getString("check_out_time"), MainConstants.creatorDateFormatForgetID);
                        employeeCheckInOut.setCheckOutTime(checkInOutTimeLong);
//                        Log.d("DSK "," setCheckOutTime "+checkInOutTimeLong);
//                        Log.d("DSK "," setCheckOutTime "+jsonArray.getJSONObject(i).getString("check_out_time"));
                        checkInOutTimeLong = NumberConstants.kConstantMinusOneLong;
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeCheckInOut.setTemporaryID(jsonArray.getJSONObject(i).getString("Temp"));
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeCheckInOut.setOfficeLocation(jsonArray.getJSONObject(i).getString("office_location"));
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeCheckInOut.setEmployeeID(jsonArray.getJSONObject(i).getString("employee_email_id.employee_id"));
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeCheckInOut.setEmployeeName(jsonArray.getJSONObject(i).getString("employee_email_id.employee_name"));
                    } catch (JSONException e1) {
                    }
                    forgetIDNotCheckOutDetails.add(employeeCheckInOut);
                }
            }

        }
        return forgetIDNotCheckOutDetails;
    }

    /**
     * DataCenter Employee Filter List.
     * <p>
     * created by Karthikeyan D S
     * On 25/05/2018
     */
    public ArrayList<ZohoEmployeeRecord> dataCenterEmployeeList(String response) {

//         Log.d("dsk response", "stationeryItemList" + response);
        ArrayList<ZohoEmployeeRecord> dataCenterEmployeeList = null;

        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantDataCenterEmployeeFormName);
            } else {
                return null;
            }
            if (jsonArray != null) {
                dataCenterEmployeeList = new ArrayList<ZohoEmployeeRecord>();
//                 Log.d("jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {

//                     Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    ZohoEmployeeRecord employeeDetailsDataCenter = new ZohoEmployeeRecord();
                    try {
                        employeeDetailsDataCenter.setEmployeeEmailId(jsonArray.getJSONObject(i).getString("employee.email_id"));
                        employeeDetailsDataCenter.setEmployeeRecordId(jsonArray.getJSONObject(i).getString("ID"));
                        // Log.d("inside", "for" + jsonArray.length() + "id "
                        // + jsonArray.getJSONObject(i).getString("ID"));

                    } catch (JSONException e) {

                    }
                    try {
                        employeeDetailsDataCenter.setEmployeeId(jsonArray.getJSONObject(i).getString("employee.employee_id"));
                    } catch (JSONException e1) {

                    }
                    try {
                        if (jsonArray.getJSONObject(i).getBoolean("is_datacenter_employee")) {
                            employeeDetailsDataCenter.setDataCenterEmployee(true);
                        } else {
                            employeeDetailsDataCenter.setDataCenterEmployee(false);
                        }
                    } catch (JSONException e1) {

                    }
                    dataCenterEmployeeList.add(employeeDetailsDataCenter);
                }
            }

        }
        return dataCenterEmployeeList;
    }

    /**
     * DataCenter Employee Visitor PreApproval List.
     *
     * @created by Karthikeyan D S
     * On 30JUN2018
     */
    public Map<String, EmployeeVisitorPreApproval> dataCenterEmployeeVisitorPreApprovalList(String response) {
        if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
            Log.d("dsk response", "DataCenter PreApproval Response Parser" + response);
        }
        Map<String, EmployeeVisitorPreApproval> employeeVisitorDataCenterPreApprovalHashMap = new HashMap<String, EmployeeVisitorPreApproval>();
        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                Log.d("dsk jsonObject", "jsonObject" + jsonObject);
            }
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantDataCenterEmployeePreApprovalFormName);
            } else {
                return null;
            }
            if (jsonArray != null) {
//                 Log.d("dsk jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
//                     Log.d("dsk inside", "for" + jsonArray.length() + "i " + i);

                    EmployeeVisitorPreApproval employeeVisitorDataCenterPreApproval = new EmployeeVisitorPreApproval();
                    try {
                        employeeVisitorDataCenterPreApproval.setmCreatorId(jsonArray.getJSONObject(i).getString("visitor_pre_approval_id"));
                    } catch (JSONException e) {
                    }
                    try {
                        employeeVisitorDataCenterPreApproval.setmEmployeeEmailId(jsonArray.getJSONObject(i).getString("visitor_pre_approval_id.employee_email_id"));
                    } catch (JSONException e1) {

                    }
                    try {
                        employeeVisitorDataCenterPreApproval.setmVisitorCompanyName(jsonArray.getJSONObject(i).getString("visitor_pre_approval_id.company_name"));
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeVisitorDataCenterPreApproval.setmVisitorName(jsonArray.getJSONObject(i).getString("visitor_name"));
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeVisitorDataCenterPreApproval.setmPhoneNumber(jsonArray.getJSONObject(i).getString("visitor_phone_number"));
                    } catch (JSONException e1) {
                    }
                    try {
                        employeeVisitorDataCenterPreApproval.setmDateofVisit(jsonArray.getJSONObject(i).getString("visitor_pre_approval_id.date_of_visit"));
                        String mDateofVisit = jsonArray.getJSONObject(i).getString("visitor_pre_approval_id.date_of_visit");
                        if (mDateofVisit != null && !mDateofVisit.isEmpty()) {
//							Log.d("DSK ","mDateofVisitbefor "+mDateofVisit);
                            long mDateofVisitLong = assignEmployeeDateofJoin(mDateofVisit);
//							Log.d("DSK ","mDateofVisitafter "+mDateofVisitLong);
                            if (mDateofVisitLong > 0) {
                                employeeVisitorDataCenterPreApproval.setmDateofVisitlong(mDateofVisitLong);
                            } else {
                                employeeVisitorDataCenterPreApproval.setmDateofVisitlong(-1);
                            }
                        } else {
                            employeeVisitorDataCenterPreApproval.setmDateofVisitlong(-1);
                        }

                    } catch (JSONException e1) {
                    }
                    try {
                        employeeVisitorDataCenterPreApprovalHashMap.put(jsonArray.getJSONObject(i).getString("ID"), employeeVisitorDataCenterPreApproval);
                    } catch (JSONException e1) {
                    }
                }
            }
        }
        return employeeVisitorDataCenterPreApprovalHashMap;
    }

    /**
     * DataCenter Employee Visitor PreApproval List.
     *
     * @created by Karthikeyan D S
     * On 30JUN2018
     */
    public ArrayList<String> employeeIDExtensionParse(String response) {
//            Log.d("DSK ", "employeeForgetIDNotCheckOut " + response);
        ArrayList<String> employeeForgetIDNotCheckOut = new ArrayList<String>();
        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {
            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            if (NumberConstants.kConstantProduction == NumberConstants.kConstantZero) {
                Log.d("DSK jsonObject", "jsonObject" + jsonObject);
            }
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantEmployeeCommonExtension);
            } else {
                return null;
            }
            if (jsonArray != null) {
//                 Log.d("dsk jsonArray", "array" + jsonArray);
                ArrayList<String> employeeExtension = new ArrayList<String>();
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
//                     Log.d("dsk inside", "for" + jsonArray.length() + "i " + i);
                    try {
                        employeeExtension.add(jsonArray.getJSONObject(i).getString("extension"));
                    } catch (JSONException e) {
                    }
                }
                employeeForgetIDNotCheckOut.addAll(employeeExtension);
            }
        }
        return employeeForgetIDNotCheckOut;
    }


    private ArrayList<StationeryEntry> processStationeryEntryLists(String response) {

        // Log.d("response", "stationeryItemList" + response);
        ArrayList<StationeryEntry> stationeryItemList = null;

        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantStationeryFormName);
            } else {
                return null;
            }
            if (jsonArray != null) {
                stationeryItemList = new ArrayList<StationeryEntry>();
                // Log.d("jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {

                    // Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    StationeryEntry stationeryItem = new StationeryEntry();

                    try {
                        stationeryItem.setEntryTime(jsonArray.getJSONObject(i).getLong("Added_Time"));
                        stationeryItem.setCreatorId(jsonArray.getJSONObject(i).getLong("ID"));
                        // Log.d("inside", "for" + jsonArray.length() + "id "
                        // + jsonArray.getJSONObject(i).getString("ID"));

                    } catch (JSONException e) {
                        stationeryItem.setEntryTime(0);
                    }

                    try {
                        stationeryItem.setStationeryItemCode(jsonArray.getJSONObject(i).getInt("code"));
                    } catch (JSONException e1) {
                        stationeryItem.setStationeryItemCode(MainConstants.kConstantZero);

                    }
                    try {
                        stationeryItem.setRequestId(jsonArray.getJSONObject(i).getString("request_id"));
                    } catch (JSONException e1) {
                        stationeryItem.setRequestId(MainConstants.kConstantEmpty);

                    }

                    try {
                        stationeryItem.setQuantity(jsonArray.getJSONObject(i).getInt("quantity"));

                    } catch (JSONException e) {
                        stationeryItem.setQuantity(MainConstants.kConstantZero);

                    }

                    stationeryItem.setDirtyBit(1);

                    stationeryItemList.add(stationeryItem);

                }
            }

        }
        return stationeryItemList;

    }

    public ArrayList<StationeryRequest> processStationeryRequest(String response) {

        // Log.d("response", "stationeryItemList" + response);
        ArrayList<StationeryRequest> stationeryRequest = null;

        if (response != null && !response.isEmpty() && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

            }
            // Log.d("jsonObject", "jsonObject" + jsonObject);
            if (jsonObject != null) {
                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantStationeryRequestFormName);
            } else {
                return null;
            }
            if (jsonArray != null) {
                stationeryRequest = new ArrayList<StationeryRequest>();
                // Log.d("jsonArray", "array" + jsonArray);
                for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {

                    // Log.d("inside", "for" + jsonArray.length() + "i " + i);
                    StationeryRequest stationeryItem = new StationeryRequest();

                    try {
                        stationeryItem.setRequestId(jsonArray.getJSONObject(i).getString("request_id"));
                        stationeryItem.setCreatorId(jsonArray.getJSONObject(i).getLong("ID"));
                        // Log.d("inside", "for" + jsonArray.length() + "id "
                        // + jsonArray.getJSONObject(i).getString("ID"));

                    } catch (JSONException e) {

                    }

                    try {
                        stationeryItem.setRequestTime(TimeZoneConventer
                                .getLocalDateTimeInMilliSeconds(jsonArray.getJSONObject(i).getString("Added_Time")));
                    } catch (JSONException e1) {
                        stationeryItem.setRequestTime(MainConstants.kConstantZero);
                    }

                    try {
                        stationeryItem.setEmployeeId(jsonArray.getJSONObject(i).getString("employee_id"));

                    } catch (JSONException e) {
                        stationeryItem.setEmployeeId(MainConstants.kConstantEmpty);

                    }

                    stationeryRequest.add(stationeryItem);

                }
            }

        }
        return stationeryRequest;

    }

    public StationeryEntry processAddStationeryEntry(String response) {
        JSONObject jsonObjectOfValues = null;
        StationeryEntry StationeryEntry = new StationeryEntry();

        if ((response != null) && (!response.isEmpty()) && (response.length() > MainConstants.kConstantZero)) {

            try {
                jsonObject = new JSONObject(response);
            } catch (JSONException e) {

                jsonObject = null;
                setInternalLogs(context, ErrorConstants.backgroundProcessJsonException, response);
                return null;
            }
            if (jsonObject != null) {
                jsonObjectOfValues = getStationeryEntry(jsonObject);

            }
            if ((jsonObjectOfValues != null) && (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
                    && (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

                // String visitorName =
                // jsonArray.getJSONObject(i).getString(
                // MainConstants.ConstantVisitorName);
                Long id;
                try {
                    id = jsonObjectOfValues.getLong("ID");
                    // ////Log.d("id", "id" + id);
                    // ////Log.d("read", "barcode" +
                    // jsonObjectOfValues.getString("snack_id"));
                    // ////Log.d("time",
                    // "time"
                    // + jsonObjectOfValues
                    // .getString("entry_time"));

                } catch (JSONException e) {

                    setInternalLogs(context, ErrorConstants.backgroundProcessJsonException, response);
                    id = 0L;
                }
                if (id == null) {
                    return null;
                } else {
                    StationeryEntry.setCreatorId(id);
                    try {
                        StationeryEntry.setStationeryItemCode(jsonObjectOfValues.getInt("code"));
                    } catch (JSONException error) {

                        StationeryEntry.setStationeryItemCode(0);
                    }
                    try {
                        StationeryEntry.setQuantity(jsonObjectOfValues.getInt("quantity"));
                    } catch (JSONException error) {

                        StationeryEntry.setQuantity(0);
                    }
                    try {
                        StationeryEntry.setEntryTime(TimeZoneConventer
                                .getLocalDateTimeInMilliSeconds(jsonObjectOfValues.getString("Added_Time")));
                        // ////Log.d("entry",
                        // "time long "+StationeryEntry.getEntryTime()
                        // +" date "+jsonObjectOfValues
                        // .getString("entry_time"));
                    } catch (JSONException error) {

                        StationeryEntry.setEntryTime(0L);
                    }
                }
            }
        }
        // }
        return StationeryEntry;
    }

    private JSONObject getStationeryEntry(JSONObject JsonObject) {

        JSONObject jsonObjectOfValues;
        // Log.d("jsonObject", "jsonObject" + jsonObject);
        jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);

        // Log.d("jsonArray", "jsonArray" + jsonArray);
        JSONObject jsonSubObject;
        try {
            jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
        } catch (JSONException e) {

            jsonSubObject = null;

        }
        JSONArray jsonSubArray;
        try {
            jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
        } catch (JSONException e) {

            jsonSubArray = null;

        }
        // //////Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
        JSONObject jsonSubOfSubObject;
        try {
            jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
        } catch (JSONException e) {

            jsonSubOfSubObject = null;
        }

        try {
            jsonObjectOfValues = jsonSubOfSubObject.getJSONObject(MainConstants.kConstantValues);
        } catch (JSONException e) {

            jsonObjectOfValues = null;
        }
        return jsonObjectOfValues;
    }

    public long getStationeryRequestId(JSONObject response) {
        long id = 0L;
        if (response != null) {

            jsonArray = response.optJSONArray(MainConstants.kConstantFormName);
            if ((jsonArray != null)) {
                JSONObject jsonSubObject;
                try {
                    jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
                } catch (JSONException e) {

                    return 0L;
                }
                if (jsonSubObject == null) {
                    return 0L;
                } else {
                    JSONArray jsonSubArray;
                    try {
                        jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
                    } catch (JSONException e) {

                        return 0L;
                    }
                    if (jsonSubArray != null) {
                        JSONObject jsonSubOfSubObject;
                        try {
                            jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
                        } catch (JSONException e) {

                            return 0L;
                        }
                        if (jsonSubOfSubObject != null) {
                            JSONObject jsonObjectOfValues;
                            try {
                                jsonObjectOfValues = jsonSubOfSubObject.getJSONObject(MainConstants.kConstantValues);
                            } catch (JSONException e) {

                                return 0L;
                            }

                            if ((jsonObjectOfValues != null)
                                    && (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
                                    && (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

                                // String visitorName =
                                // jsonArray.getJSONObject(i).getString(
                                // MainConstants.ConstantVisitorName);
                                try {
                                    id = jsonObjectOfValues.getLong("ID");
                                } catch (JSONException e) {

                                    //e.printStackTrace();
                                }
                            } else {
                                return 0L;
                            }
                        } else {
                            return 0L;
                        }
                    } else {
                        return 0L;
                    }
                }
            } else {
                return 0L;
            }
        } else {
            return 0L;
        }
        return id;

    }

    /**
     * Download image background start
     *
     * @author karthick
     * @created 27/05/2015
     */
    private void downloadImage(String filename) {

        File fileDirectory = new File(
                MainConstants.kConstantFileDirectory + StringConstants.kConstantFileStationeryImageFolder);

        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }

        File stationeryItemPhoto = new File(MainConstants.kConstantFileDirectory
                + StringConstants.kConstantFileStationeryImageFolder + filename + MainConstants.kConstantImageFileFormat);
        if (!stationeryItemPhoto.exists()) {
//			DownloadImage image = new DownloadImage();
            // image.execute(filename);

            ImageReq request = new ImageReq(
                    MainConstants.downloadImageURL + MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.filePath + filename
                            + MainConstants.sharedBy + MainConstants.sharedByValue + MainConstants.creatorAppLinkName
                            + MainConstants.creatorStationeryAppName + MainConstants.viewLinkName + MainConstants.reportName,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(final Bitmap bitmap, final String filename) {
                            //Log.d("Android ", bitmap + "del res:" + filename);
                            // /my_file.setImageBitmap(bitmap);
                            UsedBitmap usedBitmap = new UsedBitmap();
                            usedBitmap.storeBitmap(bitmap, MainConstants.kConstantFileDirectory
                                            + StringConstants.kConstantFileStationeryImageFolder + filename
                                    // +MainConstants.kConstantImageFileFormat
                            );
                            // UsedBitmap.decodeBitmap(MainConstants.kConstantFileDirectory+MainConstants.kConstantFileTemporaryFolder+filename+MainConstants.kConstantImageFileFormat,10,10);
                        }

                    }, 0, 0, null, new Response.ErrorListener() {
                public void onErrorResponse(final VolleyError error, String id) {
                    //Log.d("Android ", "error:" + id);
                    // my_file.setImageResource(R.drawable.ic_launcher);
                }
            }, filename);

            // ////Log.d("Android ", "volley:" + mRequestQueue);
            mRequestQueue.add(request);
        }
    }

    /**
     * Download visitor image from the Zoho cretor.
     *
     * @author karthick
     * @created 27/05/2015
     */
    class DownloadImage extends AsyncTask<String, String, String> {

        int total;
        int count = 0;

        @Override
        protected String doInBackground(String... zuid) {

            URL url;
            try {

                url = new URL(MainConstants.downloadImageURL + MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.filePath
                        + zuid[0] + MainConstants.sharedBy + MainConstants.sharedByValue + MainConstants.creatorAppLinkName
                        + MainConstants.creatorStationeryAppName + MainConstants.viewLinkName + MainConstants.reportName);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                if (bmp != null) {
                    UsedBitmap usedBitmap = new UsedBitmap();

                    usedBitmap.storeBitmap(bmp,
                            MainConstants.kConstantFileDirectory + StringConstants.kConstantFileStationeryImageFolder
                                    + zuid[0] + MainConstants.kConstantImageFileFormat);
                }
                // ////Log.d("DownloadImage" ,"bitmap:");
            } catch (MalformedURLException e) {
                // //////Log.d("DownloadImage" ,"bitmap:"+e);
            } catch (IOException e) {
                // //////Log.d("DownloadImage" ,"bitmap:"+e);
            }

            return null;
        }
    }

    /**
     * Send get request
     *
     * @param: url
     * @created on 20150728
     * @author karthick
     * @Modified Karthikeyan D S
     */
    public void sendVolleyRequest(String url, String requestCode) {

        //Log.d("Android ", "volley url:" + url + "requestCode" + requestCode);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            public void onResponse(String response, String responseCode) {

                //Log.d("stationery", "response" + response + "code " + responseCode);
                JSONObject jsonObject;
                if (sqliteHelper == null) {
                    sqliteHelper = new DbHelper(context);
                }
                try {
//                    Removed by Karthikeyan D S due to newly Created Table NewStationeryList
//                    if (responseCode.matches("StationeryList")) {
//
//                        //Log.d("stationery", "response" + response);
//                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
//                            ArrayList<Stationery> itemList = processStationery(response);
//                            sqliteHelper.deleteAllStationery();
//                            sqliteHelper.insertBulkStationeryRecords(itemList);
//                        }
//                    }

                    if (responseCode.matches("NewStationeryList")) {
//					Log.d("newstationery", "response " + response);
                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
                            ArrayList<Stationery> newItemList = newProcessStationeryList(response);
                            sqliteHelper.deleteAllNewStationery();
//						Log.d("insertion","NewStationeryListSize "+newItemList.size());
                            sqliteHelper.insertBulkNewStationeryRecords(newItemList);
                        }
                    }
                    if (responseCode.matches("StationeryEntry")) {
                        sqliteHelper.deleteAllStationeryEntryItemList();
                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
//						MainConstants.responseCount = MainConstants.responseCount + 1;
                            ArrayList<StationeryEntry> stationeryItemList = processStationeryEntryLists(response);
                            if ((stationeryItemList != null) && (stationeryItemList.size() > 0)) {

                                sqliteHelper.insertBulkstationeryEntryRecords(stationeryItemList);
                            }

                        }
//                    } else if (responseCode.matches("StationeryRequest")) {
//                        sqliteHelper.deleteAllStationeryRequests();
//                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
////						MainConstants.responseCount = MainConstants.responseCount + 1;
//                            ArrayList<StationeryRequest> list = processStationeryRequest(response);
//                            if (list != null && (list.size() > 0)) {
//                                sqliteHelper.insertBulkStationeryRequestRecords(list);
//                            }
//                        }
                    }

                } catch (SQLiteDatabaseLockedException e) {

                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error, String responseCode) {

                // //Log.d("creator", "error" + error);
                if (error.toString().contains("NoConnectionError")) {
//					MainConstants.backgroundProcessVolleyError = "NoConnectionError";
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "No Connection Error");
                } else {
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "Get Stationery List Volley Error: " + error.getLocalizedMessage());
                }
            }
        }, requestCode);
        // //////Log.d("string","requestString"+stringRequest);
        ApplicationController.getInstance().getRequestQueue().add(stringRequest);
    }


    /**
     * Send get request
     *
     * @param: url
     * @created on 20180606
     * @author Karthikeyan D S
     */
    public void sendEmployeeDatacenterRequestVolleyRequest(String url, String requestCode) {
        //Log.d("Android ", "volley url:" + url + "requestCode" + requestCode);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            public void onResponse(String response, String responseCode) {
                //Log.d("stationery", "response" + response + "code " + responseCode);
                JSONObject jsonObject;
                try {
                    if (responseCode.matches("DataCenterEmployeeList")) {
                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
//						Log.d("DataCenterEmployee", "response " + response);
//						MainConstants.responseCount = MainConstants.responseCount + 1;
                            ArrayList<ZohoEmployeeRecord> DatacenterEmployeEmailList = dataCenterEmployeeList(response);
                            if (DatacenterEmployeEmailList != null && (DatacenterEmployeEmailList.size() > 0)) {
                                ArrayList<String> dataCenterEmployeeEmailID = new ArrayList<String>();
                                for (int count = 0; count < DatacenterEmployeEmailList.size(); count++) {
                                    dataCenterEmployeeEmailID.add("'" + DatacenterEmployeEmailList.get(count).getEmployeeEmailId() + "'");
                                }
                                if (dataCenterEmployeeEmailID != null && !dataCenterEmployeeEmailID.isEmpty() &&
                                        dataCenterEmployeeEmailID.size() > 0) {
//									Log.d("DSKdataCenterEmployee","IN");
                                    if (sqliteHelper == null) {
                                        sqliteHelper = new DbHelper(context);
                                    }
                                    sqliteHelper.updateDataCenterEmployeeDetails(dataCenterEmployeeEmailID);
                                } else {
//									Log.d("DSKdataCenterEmployee","ERROR");
                                    setInternalLogs(context, ErrorConstants.dataCenterEmployeeFetchedListNull, "DataCenter Employee List Empty");
                                }
                            }
                        }
                    }
                } catch (SQLiteDatabaseLockedException e) {

                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error, String responseCode) {

                // //Log.d("creator", "error" + error);
                if (error.toString().contains("NoConnectionError")) {
//					MainConstants.backgroundProcessVolleyError = "NoConnectionError";
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "No Connection Error");
                } else {
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "Get DataCentre List Volley Error: " + error.getLocalizedMessage());
                }
            }
        }, requestCode);
//		 Log.d("string","requestString"+stringRequest);
        ApplicationController.getInstance().getRequestQueue().add(stringRequest);
    }

    /**
     * Send get request to get Employee PreApproved Visitor Details
     *
     * @param: url
     * @created on 29JUN2018
     * @author Karthikeyan D S
     */
    public void sendEmployeeVisitorPreApprovalRequestVolleyRequest(String url, String requestCode) {
//		Log.d("DSKAndroid ", "volley url:" + url + "requestCode" + requestCode);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            public void onResponse(String response, String responseCode) {
//				Log.d("DSKPreApproval", "response" + response + "code " + responseCode);
                JSONObject jsonObject;
                try {
                    if (responseCode.matches("VisitorPreApprovalDetails")) {
                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
//                            Log.d("DSKDataCenterEmployee", "PreApprovalresponse " + response);
////						MainConstants.responseCount = MainConstants.responseCount + 1;
                            Map<String, EmployeeVisitorPreApproval> DatacenterEmployeeVisitorPreApproval = dataCenterEmployeeVisitorPreApprovalList(response);
//                            Log.d("DSK ", "Visitor PreApproval Parse Response Size "+DatacenterEmployeeVisitorPreApproval.size() );
                            if (DatacenterEmployeeVisitorPreApproval != null && (DatacenterEmployeeVisitorPreApproval.size() > 0)) {
                                for (Map.Entry mapDetails : DatacenterEmployeeVisitorPreApproval.entrySet()) {
//									Log.d("DSK ", "Key " + mapDetails.getKey() + " Value " + DatacenterEmployeeVisitorPreApproval.get(mapDetails.getKey()).getmVisitorName());
                                    if (sqliteHelper == null) {
                                        sqliteHelper = new DbHelper(context);
                                    }
                                    if (sqliteHelper != null) {
                                        sqliteHelper.updateEmployeeVisitorPreApprovalRequestIsActive(false);
                                        long mPreApprovalDetailsRecordId = getPreApprovalDetails(DatacenterEmployeeVisitorPreApproval.get(mapDetails.getKey()));
                                        long mEmployeeVisitorPreApprovalRecordId = NumberConstants.kConstantMinusOne;

                                        if (mPreApprovalDetailsRecordId > 0) {
//											Log.d("DSK", "visitorDetailUpdate ");
                                            DatacenterEmployeeVisitorPreApproval.get(mapDetails.getKey()).setVisitorActive(true);
                                            mEmployeeVisitorPreApprovalRecordId = sqliteHelper.updateEmployeeVisitorPreApprovalRequest(DatacenterEmployeeVisitorPreApproval.get(mapDetails.getKey()), mPreApprovalDetailsRecordId);
                                        } else {
//											Log.d("DSK", "visitorDetailInsert ");
                                            DatacenterEmployeeVisitorPreApproval.get(mapDetails.getKey()).setVisitorActive(true);
                                            mEmployeeVisitorPreApprovalRecordId = sqliteHelper.insertEmployeeVisitorPreApprovalRequest(DatacenterEmployeeVisitorPreApproval.get(mapDetails.getKey()));
                                        }

                                        if (mEmployeeVisitorPreApprovalRecordId > 0) {
                                            setInternalLogs(context, ErrorConstants.preApprovalRequestSuccess, "PreApproved Visitor Details Inserted or Updated Successfully to local DB");
                                        } else {
                                            setInternalLogs(context, ErrorConstants.preApprovalRequestSuccess, "PreApproved Visitor Details Inserted Successfully to local DB");
                                        }
//										Log.d("DSK", "visitorDetail" + mEmployeeVisitorPreApprovalRecordId);
                                    }
                                }
                            }
                        }
                    }
                } catch (SQLiteDatabaseLockedException e) {

                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error, String responseCode) {
                // //Log.d("creator", "error" + error);
                if (error.toString().contains("NoConnectionError")) {
//					MainConstants.backgroundProcessVolleyError = "NoConnectionError";
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "No Connection Error");
                } else {
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "Get Employee Visitor Pre Approval Volley Error: " + error.getLocalizedMessage());
                }
            }
        }, requestCode);
//		 Log.d("string","requestString"+stringRequest);
        ApplicationController.getInstance().getRequestQueue().add(stringRequest);
    }


    /**
     * set internal log and insert to local sqlite
     *
     * @created On 20150910
     * @author karthick
     */
    public void setInternalLogs(Context context, int errorCode, String errorMessage) {
        if (context != null && errorMessage != null && !errorMessage.isEmpty()) {
            // SingletonErrorMessage.getInstance().setErrorMessage(errorMessage);
            NetworkConnection networkConnection = new NetworkConnection();
            InternalAppLogs internalAppLogs = new InternalAppLogs();
            internalAppLogs.setErrorCode(errorCode);
            internalAppLogs.setErrorMessage(errorMessage);
            internalAppLogs.setLogDate(new Date().getTime());
            internalAppLogs.setWifiSignalStrength(networkConnection.getCurrentWifiSignalStrength(context));
            try {
                DbHelper.getInstance(context).insertLogs(internalAppLogs);
            } catch (SQLiteDatabaseLockedException e1) {

            }
        }
    }

    /*
     * Add device details records
     *
     *
     * @created On 20160806
     *
     * @author karthick
     */
    public int uploadDeviceDetails() {

        int uploadCode = MainConstants.kConstantMinusOne;
        String setParameter;

        if (SingletonVisitorProfile.getInstance() != null
                && SingletonVisitorProfile.getInstance().getDeviceSerialNumber() != null
                && !SingletonVisitorProfile.getInstance().getDeviceSerialNumber().isEmpty()
                && SingletonVisitorProfile.getInstance().getDeviceMake() != null
                && !SingletonVisitorProfile.getInstance().getDeviceMake().isEmpty()) {
            setParameter = MainConstants.kConstantCreatorAuthTokenAndScope;

            URL url = null;

            try {
                if (SingletonEmployeeProfile.getInstance().getEmployeeDepartment() != null
                        && (SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                        .equals(MainConstants.HRDepartment) || SingletonEmployeeProfile.getInstance().getEmployeeDepartment()
                        .equals(MainConstants.HRDepartmentAnotherNamePlural))
                        && SingletonVisitorProfile.getInstance().getPurposeOfVisitCode().equals(ApplicationController
                        .getInstance().getResources().getString(R.string.radio_string_purpose_interview))) {

                    url = new URL(MainConstants.interviewApplication + MainConstants.kConstantAddDeviceInterviewRecord);

                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getDeviceSerialNumber() != null
                            && !SingletonVisitorProfile.getInstance().getDeviceSerialNumber().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorInterviewDeviceSerialNo
                                + SingletonVisitorProfile.getInstance().getDeviceSerialNumber();
                    }
                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getDeviceMake() != null
                            && !SingletonVisitorProfile.getInstance().getDeviceMake().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorInterviewDeviceMake
                                + SingletonVisitorProfile.getInstance().getDeviceMake();
                    }

                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getVisitorCreatorId() != null
                            && !SingletonVisitorProfile.getInstance().getVisitorCreatorId().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorInterviewDeviceVisitorId
                                + SingletonVisitorProfile.getInstance().getVisitorCreatorId();
                    }
                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getVisitId() != null
                            && !SingletonVisitorProfile.getInstance().getVisitId().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorInterviewDeviceVisitId
                                + SingletonVisitorProfile.getInstance().getVisitId();
                    }

                } else {
                    url = new URL(MainConstants.application + MainConstants.kConstantAddDevideRecord);
                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getDeviceSerialNumber() != null
                            && !SingletonVisitorProfile.getInstance().getDeviceSerialNumber().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorDeviceSerialNo
                                + SingletonVisitorProfile.getInstance().getDeviceSerialNumber();
                    }
                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getDeviceMake() != null
                            && !SingletonVisitorProfile.getInstance().getDeviceMake().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorDeviceMake
                                + SingletonVisitorProfile.getInstance().getDeviceMake();
                    }

                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getVisitorCreatorId() != null
                            && !SingletonVisitorProfile.getInstance().getVisitorCreatorId().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorDeviceVisitorId
                                + SingletonVisitorProfile.getInstance().getVisitorCreatorId();
                    }
                    if (SingletonVisitorProfile.getInstance() != null
                            && SingletonVisitorProfile.getInstance().getVisitId() != null
                            && !SingletonVisitorProfile.getInstance().getVisitId().isEmpty()) {
                        setParameter = setParameter + MainConstants.kConstantCreatorDeviceVisitId
                                + SingletonVisitorProfile.getInstance().getVisitId();
                    }
                }
            } catch (MalformedURLException e) {
                uploadCode = ErrorConstants.visitorDeviceURLError;
                setInternalLogs(context, uploadCode, e.getLocalizedMessage());
            }

            if (url != null) {
                try {
                    jsonObject = jsonParser.getJsonArrayInPostMethod(url, setParameter);
                } catch (IOException e) {
                    uploadCode = ErrorConstants.visitorDeviceIOError;

                    if (!NetworkAnalytics.isNetworkAvailable(context)) {
                        uploadCode = ErrorConstants.visitorDeviceNetworkError;
                    }
                    if (e.getMessage().contains(MainConstants.socketTimeoutException)) {
                        uploadCode = ErrorConstants.visitorDeviceTimeOutError;
                    }
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                } catch (JSONException e) {
                    uploadCode = ErrorConstants.visitorDeviceJSONError;
                    setInternalLogs(context, uploadCode, e.getLocalizedMessage());
                }
                // Log.d("jsonObject", "jsonObject" + jsonObject.toString());
                if (jsonObject != null) {
                    String jsonObjectString = jsonObject.toString();
                    if (jsonObjectString.contains(StringConstants.creatorSuccessMsg)) {
                        uploadCode = MainConstants.kConstantMinusOne;

                    } else {
                        setInternalLogs(context, ErrorConstants.visitorDeviceCreatorError, jsonObject.toString());
                    }
                }
            }
        }
        return uploadCode;
    }

    /**
     * Upload json data to cloud
     *
     * @author Karthick
     * @created on 20161111
     */
    public int addJsonEmptyRecord(String purpose) {

        String param = MainConstants.kConstantCreatorAuthTokenAndScope;
        int responseCode = MainConstants.kConstantMinusOne;
        SharedPreferenceController sharedPreferenceController = new SharedPreferenceController();
        int applicationMode = sharedPreferenceController.getApplicationMode(context);
        try {
            if (purpose.equals(MainConstants.interViewPurpose)) {
                url = new URL(MainConstants.interviewApplication + MainConstants.kConstantAddAppJsonDetails);

            } else if (applicationMode ==NumberConstants.kConstantDataCentreMode) {
                url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantAddAppJsonDetails);
                param = MainConstants.kConstantFrontdeskDCAuthTokenAndScope;
            } else {
                url = new URL(MainConstants.application + MainConstants.kConstantAddAppJsonDetails);
            }
            JSONObject jsonObject;
            try {
                jsonObject = jsonParser.getJsonArrayInPostMethod(url, param);
            } catch (JSONException e) {
                setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, MainConstants.kConstantEmpty);
                return ErrorConstants.visitorJsonDataJSONError;
            }
            if (jsonObject != null) {

                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
                JSONObject jsonSubObject = null;

                try {
                    if (jsonArray != null) {
                        jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, MainConstants.kConstantEmpty);
                    return ErrorConstants.visitorJsonDataJSONError;
                }

                JSONArray jsonSubArray = null;
                try {
                    if (jsonSubObject != null) {
                        jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, MainConstants.kConstantEmpty);
                    return ErrorConstants.visitorJsonDataJSONError;
                }
                JSONObject jsonSubOfSubObject = null;
                try {
                    if (jsonSubArray != null) {
                        jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, MainConstants.kConstantEmpty);
                    return ErrorConstants.visitorJsonDataJSONError;
                }
                JSONObject jsonSubOfSubArray = null;
                try {
                    if (jsonSubOfSubObject != null) {
                        jsonSubOfSubArray = jsonSubOfSubObject.getJSONObject(MainConstants.kConstantValues);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, MainConstants.kConstantEmpty);
                    return ErrorConstants.visitorJsonDataJSONError;
                }
                String visitId = null;
                try {
                    if (jsonSubOfSubArray != null) {
                        visitId = jsonSubOfSubArray.getString(MainConstants.kConstantVisitId);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, MainConstants.kConstantEmpty);
                    return ErrorConstants.visitorJsonDataJSONError;
                }
//					 if (purpose.equals(MainConstants.interViewPurpose)) {
//						 SingletonVisitorProfile.getInstance().setCandidateId(visitId);
//					 } else {
                SingletonVisitorProfile.getInstance().setVisitId(visitId);
                //	 }
                if (visitId != null && visitId.length() > 0) {
                    responseCode = MainConstants.kConstantSuccessCode;
                } else {
                    if (jsonObject != null) {
                        setInternalLogs(context, ErrorConstants.visitorJsonDataJSONError, jsonObject.toString());
                    }
                    responseCode = ErrorConstants.visitorJsonDataJSONError;
                }
            }
            return responseCode;

        } catch (IOException e) {
            String message = MainConstants.kConstantEmpty;
            if (jsonObject != null) {
                message = jsonObject.toString();
            }
            setInternalLogs(context, ErrorConstants.visitorJsonDataIOError, message);
            return ErrorConstants.visitorJsonDataIOError;
        }
    }

    /**
     * Update json data to cloud
     *
     * @author Karthick
     * @created on 20161110
     */
    public int updateVisitorJsonRecord(String jsonData, String purpose) {
        if (jsonData != null) {

            SharedPreferenceController sharedPreferenceController = new SharedPreferenceController();
            int applicationMode = sharedPreferenceController.getApplicationMode(context);

            String param = MainConstants.kConstantEmpty;
            int responseCode = MainConstants.kConstantMinusOne;
            try {
//			 if (purpose.equals(MainConstants.interViewPurpose)) {
//				 url = new URL(MainConstants.interviewApplication + MainConstants.kConstantUpdateAppJsonDetails);
//				  param = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteriaJsonData+SingletonVisitorProfile.getInstance()
//							.getCandidateId()+MainConstants.kConstantVisitorJsonData+jsonData;
//			 } else {
                if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                    url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantUpdateAppJsonDetails);
                    param = MainConstants.kConstantFrontdeskDCAuthTokenAndScope + MainConstants.kConstantCriteriaJsonData + SingletonVisitorProfile.getInstance()
                            .getVisitId() + MainConstants.kConstantVisitorJsonData + jsonData;
                } else {
                    url = new URL(MainConstants.application + MainConstants.kConstantUpdateAppJsonDetails);
                    param = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteriaJsonData + SingletonVisitorProfile.getInstance()
                            .getVisitId() + MainConstants.kConstantVisitorJsonData + jsonData;
                }
                // }
                if (purpose.equals(MainConstants.interViewPurpose) || purpose.equals(MainConstants.interViewPurposeOnly)) {
                    param = param + MainConstants.kConstantInterviewOrVisitor + MainConstants.kConstantInterview;
                } else if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                    param = param + MainConstants.kConstantInterviewOrVisitor + MainConstants.kConstantVisitor;
                } else {
                    param = param + MainConstants.kConstantInterviewOrVisitor + MainConstants.kConstantVisitor;
                }

                JSONObject jsonObject;
                try {
                    jsonObject = jsonParser.getJsonArrayInPostMethod(url, param);


                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataJSONError, MainConstants.kConstantEmpty);
                    return (ErrorConstants.visitorUpdateJsonDataJSONError);
                }
                jsonArray = null;
                if (jsonObject != null) {

                    jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
                    JSONObject jsonSubObject = null;
                    try {
                        if (jsonArray != null) {
                            jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
                        }
                    } catch (JSONException e) {
                        setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataJSONError, jsonObject.toString());
                        return (ErrorConstants.visitorUpdateJsonDataJSONError);
                    }
                    JSONArray jsonSubArray = null;
                    try {
                        if (jsonSubObject != null) {
                            jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
                        }
                    } catch (JSONException e) {
                        setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataJSONError, jsonObject.toString());
                        return (ErrorConstants.visitorUpdateJsonDataJSONError);
                    }
                    JSONObject jsonSubOfSubObject = null;
                    try {
                        if (jsonSubArray != null) {
                            jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
                        }
                    } catch (JSONException e) {
                        setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataJSONError, jsonObject.toString());
                        return (ErrorConstants.visitorUpdateJsonDataJSONError);
                    }
                    String status = null;
                    try {
                        if (jsonSubOfSubObject != null) {
                            status = jsonSubOfSubObject.getString(MainConstants.kConstantStatus);
                        }
                        if (status != null && !status.isEmpty() && status.equalsIgnoreCase(StringConstants.creatorResponseSuccessFirst)) {

                        } else {
                            setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataCreatorError, jsonObject.toString());
                            return (ErrorConstants.visitorUpdateJsonDataCreatorError);
                        }
                    } catch (JSONException e) {
                        setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataJSONError, jsonObject.toString());
                        return (ErrorConstants.visitorUpdateJsonDataJSONError);
                    }
                    if (jsonObject.toString() != null
                            && (jsonObject.toString()
                            .contains(StringConstants.creatorResponseSuccessFirst) || jsonObject.toString()
                            .contains(StringConstants.creatorResponseSuccessSecond))) {
                        responseCode = MainConstants.kConstantSuccessCode;
                    } else {
                        if (jsonObject != null) {
                            setInternalLogs(context, ErrorConstants.visitorUploadThumbPhotoResponseError, jsonObject.toString());
                        }
                        responseCode = ErrorConstants.visitorUpdateJsonDataOtherError;
                    }
                }
                return responseCode;

            } catch (IOException e) {
                if (jsonObject != null) {
                    setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataIOError, jsonObject.toString());
                } else {
                    setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataIOError, e.getLocalizedMessage() + e.getCause());

                }
                return (ErrorConstants.visitorUpdateJsonDataIOError);
            }
        }
        return (ErrorConstants.visitorUpdateJsonDataOtherError);
    }

    /**
     * Update Without Mail field to cloud
     *
     * @author Karthick
     * @created on 20161110
     */
    public int updateWithoutMailField() {

        String param = MainConstants.kConstantEmpty;
        int responseCode = MainConstants.kConstantMinusOne;
        SharedPreferenceController sharedPreferenceController = new SharedPreferenceController();
        int applicationMode = sharedPreferenceController.getApplicationMode(context);

        try {
            if (applicationMode == NumberConstants.kConstantDataCentreMode) {
                url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantUpdateAppJsonDetails);
                param = MainConstants.kConstantFrontdeskDCAuthTokenAndScope + MainConstants.kConstantCriteriaJsonData + SingletonVisitorProfile.getInstance()
                        .getVisitId() + MainConstants.kConstantVisitorWithoutMail + MainConstants.kConstantTrue;
            } else {
                url = new URL(MainConstants.application + MainConstants.kConstantUpdateAppJsonDetails);
                param = MainConstants.kConstantCreatorAuthTokenAndScope + MainConstants.kConstantCriteriaJsonData + SingletonVisitorProfile.getInstance()
                        .getVisitId() + MainConstants.kConstantVisitorWithoutMail + MainConstants.kConstantTrue;
            }
            JSONObject jsonObject;
            try {
                jsonObject = jsonParser.getJsonArrayInPostMethod(url, param);

            } catch (JSONException e) {
                setInternalLogs(context, ErrorConstants.visitorUpdateJsonDataJSONError, MainConstants.kConstantEmpty);
                return (ErrorConstants.visitorUpdateWithOutMailJSONError);
            }
            jsonArray = null;
            if (jsonObject != null) {

                jsonArray = jsonObject.optJSONArray(MainConstants.kConstantFormName);
                JSONObject jsonSubObject = null;
                try {
                    if (jsonArray != null) {
                        jsonSubObject = (JSONObject) jsonArray.getJSONObject(MainConstants.kConstantOne);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailJSONError, jsonObject.toString());
                    return (ErrorConstants.visitorUpdateWithOutMailJSONError);
                }
                JSONArray jsonSubArray = null;
                try {
                    if (jsonSubObject != null) {
                        jsonSubArray = jsonSubObject.getJSONArray(MainConstants.kConstantOperation);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailJSONError, jsonObject.toString());
                    return (ErrorConstants.visitorUpdateWithOutMailJSONError);
                }
                JSONObject jsonSubOfSubObject = null;
                try {
                    if (jsonSubArray != null) {
                        jsonSubOfSubObject = (JSONObject) jsonSubArray.getJSONObject(MainConstants.kConstantOne);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailJSONError, jsonObject.toString());
                    return (ErrorConstants.visitorUpdateWithOutMailJSONError);
                }
                String status = null;
                try {
                    if (jsonSubOfSubObject != null) {
                        status = jsonSubOfSubObject.getString(MainConstants.kConstantStatus);
                    }
                    if (status != null && !status.isEmpty() && status.equalsIgnoreCase(StringConstants.creatorResponseSuccessFirst)) {

                    } else {
                        setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailCreatorError, jsonObject.toString());
                        return (ErrorConstants.visitorUpdateWithOutMailCreatorError);
                    }
                } catch (JSONException e) {
                    setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailJSONError, jsonObject.toString());
                    return (ErrorConstants.visitorUpdateWithOutMailJSONError);
                }
                if (jsonObject.toString() != null
                        && (jsonObject.toString()
                        .contains(StringConstants.creatorResponseSuccessFirst) || jsonObject.toString()
                        .contains(StringConstants.creatorResponseSuccessSecond))) {
                    responseCode = MainConstants.kConstantSuccessCode;
                } else {
                    if (jsonObject != null) {
                        setInternalLogs(context, ErrorConstants.visitorUploadThumbPhotoResponseError, jsonObject.toString());
                    }
                    responseCode = ErrorConstants.visitorUpdateWithOutMailOtherError;
                }
            }
            return responseCode;

        } catch (IOException e) {
            if (jsonObject != null) {
                setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailIOError, jsonObject.toString());
            } else {
                setInternalLogs(context, ErrorConstants.visitorUpdateWithOutMailIOError, e.getLocalizedMessage());

            }
            return (ErrorConstants.visitorUpdateWithOutMailIOError);
        }
    }

    /**
     * Get candidate Details from Zoho creator.
     *
     * @throws IOException
     * @throws ProtocolException
     * @throws MalformedURLException
     * @author Karthick
     * @created 20170119
     */
    public String getExistingVisitorDetails(int startIndex, int limit)
            throws MalformedURLException, ProtocolException, IOException {

        String url = MainConstants.kConstantViewRecord
                + MainConstants.kConstantExitingVisitorReport
                + MainConstants.kConstantCreatorAuthTokenAndScope
                + MainConstants.kConstantRawAndOwnerName
                + MainConstants.creatorStartingIndex
                + startIndex
                + MainConstants.creatorRecordsLimit
                + limit;
//		Log.d("url", ""+url);
        String response = restApiGetMethod(url);
//		Log.d("DSK Exist Visitor ", "response"+response);
        return response;
    }

    /**
     * Send get request to get Employee PreApproved Visitor Details
     *
     * @param: url
     * @created on 29JUN2018
     * @author Karthikeyan D S
     */
    public void getEmployeeExtensionFromCreatorCommon(String url, String requestCode) {
//        Log.d("DSK ","Employee Extensionurl "+url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            public void onResponse(String response, String responseCode) {
                try {
                    if (responseCode.matches("GetEmployeeExtensionDetails")) {
//                        Log.d("DSK ", " GetEmployeeExtensionDetails " + response);
                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
//                            Log.d("DSK ", " GetEmployeeExtensionDetails " + response);
////						MainConstants.responseCount = MainConstants.responseCount + 1;
                            ArrayList<String> employeeExtensionParse = employeeIDExtensionParse(response);
                            String employeeIDExtensions = MainConstants.kConstantEmpty;

                            if (employeeExtensionParse != null && employeeExtensionParse.size() > 0) {
                                if (sharedPreferenceController == null) {
                                    sharedPreferenceController = new SharedPreferenceController();
                                }
                                sharedPreferenceController.clearSharedPreferences(context, MainConstants.spKeyEmployeeExtension);
                                for (int i = 0; i < employeeExtensionParse.size() && employeeExtensionParse.get(i) != null; i++) {
                                    employeeIDExtensions = employeeIDExtensions + employeeExtensionParse.get(i).trim();
                                    if (i + 1 < employeeExtensionParse.size()) {
                                        employeeIDExtensions = employeeIDExtensions + MainConstants.kConstantComma;
                                    }
                                }
                                sharedPreferenceController.saveEmployeeExtension(context, employeeIDExtensions);
//                                Log.d("DSK ", "employeeIDExtension "+sharedPreferenceController.getEmployeeExtension(context));
                            }

                        }
//                         Log.d("DSK ", "employeeIDExtensionFull "+sharedPreferenceController.getEmployeeExtension(context));
                    }
                } catch (SQLiteDatabaseLockedException e) {
                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error, String responseCode) {
                // //Log.d("creator", "error" + error);
                if (error.toString().contains("NoConnectionError")) {
//					MainConstants.backgroundProcessVolleyError = "NoConnectionError";
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "No Connection Error");
                } else {
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "Get Employee Extensions Volley Error: " + error.getLocalizedMessage());
                }
            }
        }, requestCode);
//		 Log.d("string","requestString"+stringRequest);
        ApplicationController.getInstance().getRequestQueue().add(stringRequest);
    }

    /**
     * get employee Forget ID Not Check Out Details from Creator
     *
     * @param: url
     * @created on 29JUN2018
     * @author Karthikeyan D S
     */
    public void getEmployeeForgetIDNotCheckOutDetails(String url, String requestCode) {
//		Log.d("DSK ", "volley url:" + url + "requestCode" + requestCode);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            public void onResponse(String response, String responseCode) {
//				Log.d("DSK ", " response" + response );
                JSONObject jsonObject;
                try {
                    if (responseCode.matches("ForgetIdNotCheckOutDetails")) {
                        if ((response != null) && (response != MainConstants.kConstantEmpty)) {
//                            Log.d("DSK ", "ForgetIdNotCheckOutDetailsresponse " + response);
////						MainConstants.responseCount = MainConstants.responseCount + 1;
                            long recordID = NumberConstants.kConstantMinusOne;
                            ArrayList<EmployeeCheckInOut> employeeCheckInOutDetails = forgetIdZohoCreatorNotCheckOutParse(response);
                            if (sqliteHelper == null) {
                                sqliteHelper = new DbHelper(context);
                            }
                            if (sqliteHelper != null) {
//                                get Long value for 10days before date to delete Records from Local DB if Available.
                                long tenDaysBeforeMilliseconds = TimeZoneConventer.getRequiredDateinMilliSeconds(MainConstants.kConstantDateTimeHalfFormat,
                                        -10, MainConstants.creatorDateFormatForgetID);

                                if(tenDaysBeforeMilliseconds>-1)
                                {
                                   sqliteHelper.deleteEmployeeForgetIDTable(tenDaysBeforeMilliseconds);
//                                   Log.d("DSK ","Delete");
                                }
                                if (employeeCheckInOutDetails != null && (employeeCheckInOutDetails.size() > 0)) {
                                    for (int i = 0; i < employeeCheckInOutDetails.size(); i++) {
                                        if (employeeCheckInOutDetails.get(i) != null) {

                                            recordID = sqliteHelper.getEmployeeForgetIDNotCheckOut(employeeCheckInOutDetails.get(i).getEmployeeEmailID(),
                                                    employeeCheckInOutDetails.get(i).getCheckInTime());
                                            if (recordID > NumberConstants.kConstantMinusOne) {
                                                if (employeeCheckInOutDetails.get(i).getCheckOutTimeDate() != null && !employeeCheckInOutDetails.get(i).getCheckOutTimeDate().isEmpty()) {
                                                    sqliteHelper.updateEmployeeForgetIDCheckOutDetails(recordID, employeeCheckInOutDetails.get(i));
                                                } else {
//                                                    Log.d("DSK ", "No Update " );
//                                                    no need to update.
                                                }
                                            } else {
                                                if (employeeCheckInOutDetails.get(i).getCheckOutTimeDate() == null || employeeCheckInOutDetails.get(i).getCheckOutTimeDate().isEmpty()) {
                                                    sqliteHelper.insertEmployeeForgetIDCheckOutDetails(employeeCheckInOutDetails.get(i));
                                                } else {
//                                                    Log.d("DSK ", "No Insert");
                                                }
                                            }
                                        }
                                    }
                                    recordID = NumberConstants.kConstantMinusOne;
                                }
                            }
                        }
                    }
                } catch (SQLiteDatabaseLockedException e) {

                }
            }
        }, new Response.ErrorListener() {

            public void onErrorResponse(VolleyError error, String responseCode) {
                // //Log.d("creator", "error" + error);
                if (error.toString().contains("NoConnectionError")) {
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "No Connection Error");
                } else {
                    setInternalLogs(context, ErrorConstants.backgroundProcessVolleyError,
                            "Get Forget ID Not Checked-Out Volley Error: " + error.getLocalizedMessage());
                }
            }
        }, requestCode);
//		 Log.d("string","requestString"+stringRequest);
        ApplicationController.getInstance().getRequestQueue().add(stringRequest);
    }

    /**
     * Get data from request url and return response
     *
     * @param stringUrl
     * @return response
     * @throws JSONException
     * @throws TimeoutException
     * @throws IOException
     * @throws MalformedURLException
     * @throws ProtocolException
     * @author Karthick
     * @created 20170119
     */
    private String restApiGetMethod(String stringUrl)
            throws MalformedURLException, ProtocolException, IOException {

        String response = MainConstants.kConstantEmpty;
        HttpURLConnection con = null;
        URL url = new URL(stringUrl);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(MainConstants.kConstantGetMethod);
        con.setDoInput(true);
        con.setReadTimeout(MainConstants.connectionTimeout);
        con.setConnectTimeout(MainConstants.connectionTimeout);

        con.connect();
        int status = con.getResponseCode();
        if (status == MainConstants.kConstantSuccessCode) {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = in.readLine()) != null) {
                stringBuilder.append(line + MainConstants.kConstantNewLine);
            }
            response = stringBuilder.toString();
        }
        return response;
    }


    /**
     * return Given Date in ANDROID long value
     * <p>
     * Date Format ("dd-MMMM-yyyy")
     *
     * @author Karthikeyan D S
     * @created 30JUN2018
     */
    private long assignEmployeeDateofJoin(String mDateofVisit) {
        long joinDate = -1;
        try {
//            Log.d("DSK ","mDateofVisit "+mDateofVisit);
            SimpleDateFormat sdf = new SimpleDateFormat(MainConstants.kConstantDataCentreDateFormat);
            Date date = sdf.parse(mDateofVisit);
//            Log.d("DSK ","mDateofVisit "+mDateofVisit);
            joinDate = date.getTime();
//            Log.d("DSK ","joinDate "+joinDate);
            return joinDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return joinDate;
    }

    /**
     * insert PreApproval Details
     *
     * @author Karthikeyan D S
     * @created 30JUN2018
     */
    private long getPreApprovalDetails(EmployeeVisitorPreApproval employeeVisitorPreApproval) {
        long mPreApprovalDetailsRecordId = NumberConstants.kConstantMinusOne;
        if (employeeVisitorPreApproval != null && employeeVisitorPreApproval.getmVisitorName() != null &&
                employeeVisitorPreApproval.getmDateofVisitlong() > 0) {
            if (sqliteHelper == null) {
                sqliteHelper = new DbHelper(context);
            }
            mPreApprovalDetailsRecordId = sqliteHelper.mPreApprovalDetailsRecordId(employeeVisitorPreApproval);
//			Log.d("mRecordId", "mEmployeePreApprovalDetailsRecordID " + mPreApprovalDetailsRecordId);
            return mPreApprovalDetailsRecordId;
        }
        return mPreApprovalDetailsRecordId;
    }

    /**
     * Get Category Details from Zoho Creator cloud through REST API Call.
     *
     * @author Karthikeyan D S
     * @created 09July2018
     */
    public String sendOTPDetailsCreator(Context context, String mobileNumber) {
        jsonParser = new JSONParser();
        JSONObject jsonObject;
        String creatorID = MainConstants.kConstantEmpty;
        JSONObject parametersJson;
        URL url;
        try {
            url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantAddVisitorOTP);
            //Get method have no param. so given empty string(third param)
            parametersJson = createParametersForEmployeeOtpURL(context, mobileNumber);

            String mobileNumberDetails = MainConstants.kConstantFrontdeskDCAuthTokenAndScope +
                    MainConstants.kConstantRaw + MainConstants.creatorOTPUploadJsonFormat + encodeString(parametersJson + "", MainConstants.kConstantUTFEight);

            jsonObject = jsonParser.getJsonArrayInPostMethod(url, mobileNumberDetails);

            if (jsonObject != null) {
//                Log.d("DSK jsonObject", "jsonObject" + jsonObject);
                jsonArray = jsonObject
                        .optJSONArray(MainConstants.kConstantFormName);
//                Log.d("DSK jsonArray", "jsonArray" + jsonArray);
                if (jsonArray != null) {
                    JSONObject jsonSubObject = (JSONObject) jsonArray
                            .getJSONObject(MainConstants.kConstantOne);
                    JSONArray jsonSubArray = jsonSubObject
                            .getJSONArray(MainConstants.kConstantOperation);
                    //Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
                    JSONObject jsonSubOfSubObject = (JSONObject) jsonSubArray
                            .getJSONObject(MainConstants.kConstantOne);
                    //Log.d("jsonArray", "jsonObjectst" + jsonSubOfSubObject);

                    JSONObject jsonObjectOfValues = jsonSubOfSubObject
                            .getJSONObject(MainConstants.kConstantValues);

                    if ((jsonObjectOfValues != null)
                            && (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
                            && (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

                        // String visitorName =
                        // jsonArray.getJSONObject(i).getString(
                        // MainConstants.ConstantVisitorName);
                        creatorID = jsonObjectOfValues
                                .getString(MainConstants.forgotIDId);

//                    Log.d("DSKid ", "recordId" + jsonObjectOfValues.getString(MainConstants.forgotIDId));
                    }
                    else
                    {
                        setInternalLogs(context,ErrorConstants.dataCentreOTPRequestJsonDataError,"App Password Request Creator Empty Response");
                    }
                }
            }
        } catch (JSONException e) {
            //e.printStackTrace();
            setInternalLogs(context,ErrorConstants.dataCentreOTPRequestJSONError,e.getLocalizedMessage());
        } catch (IOException e) {
            setInternalLogs(context,ErrorConstants.dataCentreOTPRequestIOError,e.getLocalizedMessage());
        }
        return creatorID;
    }

    /**
     * send App Password Request to Creator Frontdesk-DC Account
     *
     * @author Karthikeyan D S
     * @created 29OCT2018
     */
    public String sendAppPasswordtoCreator(Context context, String appSettingsPassword) {
        jsonParser = new JSONParser();
        JSONObject jsonObject;
        String creatorID = MainConstants.kConstantEmpty;
        JSONObject parametersJson;
        URL url;
        try {
            url = new URL(MainConstants.applicationFrontdeskDC + MainConstants.kConstantAddFrontdeskDCDevicePasswordRequest);
            //Get method have no param. so given empty string(third param)
            parametersJson = createParametersForAppPasswordURL(context, appSettingsPassword);
//            Log.d("DSK ", "URL " + url);
            String appPasswordDetailsUrl = MainConstants.kConstantFrontdeskDCAuthTokenAndScope +
                    MainConstants.kConstantRaw + MainConstants.creatorAppPasswordUploadJsonFormat + encodeString(parametersJson + "", MainConstants.kConstantUTFEight);
//            Log.d("DSK ", "appPasswordDetailsUrl " + appPasswordDetailsUrl);
            jsonObject = jsonParser.getJsonArrayInPostMethod(url, appPasswordDetailsUrl);

            if (jsonObject != null) {
//                Log.d("DSK jsonObject", "jsonObject" + jsonObject);
                jsonArray = jsonObject
                        .optJSONArray(MainConstants.kConstantFormName);
//                Log.d("DSK jsonArray", "jsonArray" + jsonArray);
                if (jsonArray != null) {
                    JSONObject jsonSubObject = (JSONObject) jsonArray
                            .getJSONObject(MainConstants.kConstantOne);
                    JSONArray jsonSubArray = jsonSubObject
                            .getJSONArray(MainConstants.kConstantOperation);
                    //Log.d("jsonArray", "jsonArrayst" + jsonSubArray);
                    JSONObject jsonSubOfSubObject = (JSONObject) jsonSubArray
                            .getJSONObject(MainConstants.kConstantOne);
                    //Log.d("jsonArray", "jsonObjectst" + jsonSubOfSubObject);

                    JSONObject jsonObjectOfValues = jsonSubOfSubObject
                            .getJSONObject(MainConstants.kConstantValues);

                    if ((jsonObjectOfValues != null)
                            && (jsonObjectOfValues.toString().length() > MainConstants.kConstantZero)
                            && (jsonObjectOfValues.toString() != MainConstants.kConstantEmpty)) {

                        // String visitorName =
                        // jsonArray.getJSONObject(i).getString(
                        // MainConstants.ConstantVisitorName);
                        creatorID = jsonObjectOfValues
                                .getString(MainConstants.forgotIDId);

//                    Log.d("DSKid ", "recordId" + jsonObjectOfValues.getString(MainConstants.forgotIDId));
                    }
                    else
                    {
                        setInternalLogs(context,ErrorConstants.appDCPasswordRequestResponseJsonDataError,"App Password Request Creator Empty Response");
                    }
                }
            }
        } catch (JSONException e) {
            //e.printStackTrace();
            setInternalLogs(context,ErrorConstants.appDCPasswordRequestJSONError,e.getLocalizedMessage());

        } catch (IOException e) {
            setInternalLogs(context,ErrorConstants.appDCPasswordRequestIOError,e.getLocalizedMessage());
        }
        return creatorID;
    }

    /**
     * Create Parameters for Employee OTP Request
     *
     * @author Karthikeyan D S
     * @created on 14JUL2018
     */
    public JSONObject createParametersForEmployeeOtpURL(Context context, String emailID) {
        JSONObject parametersMap = new JSONObject();
        SimpleDateFormat employeeOTPRequestDate;
        //Get Android Device Id From Singleton Class
//        String androidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String appVersion = BuildConfig.VERSION_NAME;

        //ParametersMap
        if (emailID != null && !emailID.isEmpty()) {
            if (parametersMap != null) {
                try {
                    parametersMap.put(MainConstants.jsonVisitorOTPRequestPhone, emailID);
                } catch (JSONException e) {
//                    setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsJSONDataError, e.getLocalizedMessage());
                }
            }
            employeeOTPRequestDate = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
            if (employeeOTPRequestDate != null) {
                try {
                    parametersMap.put(MainConstants.mConstantOTPRequestDate, employeeOTPRequestDate.format(new Date()));
                } catch (JSONException e) {
//                    setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsJSONDataError, e.getLocalizedMessage());
                }
            }
            if (appVersion != null && !appVersion.isEmpty()) {
                try {
                    parametersMap.put(MainConstants.kConstantAppVersion, appVersion);
                } catch (JSONException e) {
//                    setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsJSONDataError, e.getLocalizedMessage());
                }
            }
            String androidDeviceFromApp = "fromApplication";
            if (androidDeviceFromApp != null && !androidDeviceFromApp.isEmpty()) {
                try {
                    parametersMap.put("from_app", androidDeviceFromApp);
                } catch (JSONException e) {
//                    setInternalLogs(context, ErrorConstants.employeeOTPRequestEntryDetailsJSONDataError, e.getLocalizedMessage());
                }
            }
        }
        return parametersMap;
    }

    /**
     * Create Parameters for App Password Request
     *
     * @author Karthikeyan D S
     * @created on 29OCT2018
     */
    public JSONObject createParametersForAppPasswordURL(Context context,String devicePassword) {
        JSONObject parametersMap = new JSONObject();
        if (devicePassword!=null && !devicePassword.isEmpty()) {
            SimpleDateFormat passwordRequestDate;
            //Get Android Device Id From Singleton Class
            String androidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (androidDeviceId != null && !androidDeviceId.isEmpty()) {
                try {
                    parametersMap.put("device_id", androidDeviceId);
                } catch (JSONException e) {
                }
            }
            if (devicePassword != null && !devicePassword.isEmpty()) {
                try {
                    parametersMap.put("device_password", devicePassword);
                } catch (JSONException e) {
                }
            }
                try {
                    parametersMap.put("device_name",android.os.Build.MANUFACTURER.toUpperCase() + android.os.Build.MODEL);
                } catch (JSONException e) {
                }
            String appVersion = BuildConfig.VERSION_NAME;
            Date date = Calendar.getInstance().getTime();
            passwordRequestDate = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
            String formattedDate = passwordRequestDate.format(date);
            if (formattedDate != null && !formattedDate.isEmpty()) {
                try {
                    parametersMap.put("request_date", formattedDate);
                } catch (JSONException e) {
                }
            }

            if (appVersion != null && !appVersion.isEmpty()) {
                try {
                    parametersMap.put(MainConstants.kConstantAppVersion, appVersion);
                } catch (JSONException e) {
                }
            }
            String androidDeviceFromApp = "fromApplication";
            if (androidDeviceFromApp != null && !androidDeviceFromApp.isEmpty()) {
                try {
                    parametersMap.put("request_from", androidDeviceFromApp);
                } catch (JSONException e) {
                }
            }
        }
            return parametersMap;
    }

    /**
     * Encode the given String
     *
     * @param encodingString
     */
    public String encodeString(String encodingString, String encodingFormat) {
        String stringEncoded = encodingString;
        if (encodingString != null && !encodingString.isEmpty() &&
                encodingFormat != null && !encodingFormat.isEmpty()) {
            try {
                stringEncoded = URLEncoder.encode(encodingString, encodingFormat);
            } catch (UnsupportedEncodingException e) {
                stringEncoded = encodingString;
            }
        }
        return stringEncoded;
    }

    /**
     * Get Category Details from Zoho Creator cloud through REST API Call.
     *
     * @author Karthikeyan D S
     * @created 27July2018
     */
    public VisitorOTPDetails checkOTPIsValidinCreartor(Context context, String creatorIDLocal) throws IOException {
        URL url;
        jsonParser = new JSONParser();
        String response = MainConstants.kConstantEmpty;
        VisitorOTPDetails visitorOTPDetails = new VisitorOTPDetails();
        if (creatorIDLocal != null && !creatorIDLocal.isEmpty()) {
            url = new URL(MainConstants.kConstantDataCenterViewRecord
                    + MainConstants.kConstantOTPRequestReport + MainConstants.kConstantFrontdeskDCAuthTokenAndScope
                    + MainConstants.kConstantFrontdeskDCRawAndOwnerName + MainConstants.creatorID + creatorIDLocal);
            if (NumberConstants.kConstantProduction == MainConstants.kConstantZero) {
                Log.d("DSK creator ", "otp Request url " + url);
            }
            //Get method have no param. so given empty string(third param)
            try {
                //Get method have no param. so given empty string(third param)
                response = jsonParser.restAPICall(MainConstants.kConstantGetMethod, url, MainConstants.kConstantEmpty);
                if (response != null && response != MainConstants.kConstantEmpty) {
                    visitorOTPDetails = jsonParser.parseVisitorOTPisValidResponse(context, response);
                }
                if (NumberConstants.kConstantProduction == MainConstants.kConstantZero) {
                    Log.d("Dsk", url + "otp response :" + response);
                }
            } catch (IOException e) {
            }
            if (NumberConstants.kConstantProduction == MainConstants.kConstantZero) {
                Log.d("Dsk ", url + "otp response :" + response);
            }
        }
        return visitorOTPDetails;
    }

}
