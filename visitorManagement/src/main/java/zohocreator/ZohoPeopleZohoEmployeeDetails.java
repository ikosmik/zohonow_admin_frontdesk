package zohocreator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import constants.NumberConstants;
import constants.StringConstants;
import database.dbhelper.DbHelper;
import database.dbschema.ZohoEmployeeRecord;
import utility.JSONParser;
import utility.NetworkConnection;
import utility.Validation;
import logs.InternalAppLogs;
import constants.ErrorConstants;
import constants.MainConstants;
import android.content.Context;
import android.database.sqlite.SQLiteDatabaseLockedException;

/**
 *
 * @author jayapriya
 * @created Nov 2014
 *
 */
public class ZohoPeopleZohoEmployeeDetails {
	String lastName, firstName, empName, empEmailId, mobileNo;
	Integer empId;
	String empPhoneNo;
	Long empRecordId;
	DbHelper sqliteHelper;
	ArrayList<ZohoEmployeeRecord> employeeRecord = new ArrayList<ZohoEmployeeRecord>();
	int count = 0;
	Boolean checkResponse = false;
	static JSONParser jsonParser = new JSONParser();
	Context context;

	public ZohoPeopleZohoEmployeeDetails(Context context) {
		sqliteHelper = new DbHelper(context);
		this.context=context;
	}
	/**
	 * Method used to get all the Employees and their details
	 * from Zoho people.
	 * The Details from people gets stored into Local Database (SQLite)
	 *
	 * @author jayapriya
	 * @created Nov 2014
	 *
	 */
	// modified by Gokul
	public ArrayList<ZohoEmployeeRecord> getAllEmployeeRecord() throws TimeoutException, IOException, JSONException {
		//Boolean error = false;
		ArrayList<ZohoEmployeeRecord> employeeRecordList = new ArrayList<ZohoEmployeeRecord>();
		JSONObject countOfEmployeeFromPeopleJsonObject = null;
		JSONObject jsonObject;
		int startingIndex;
		int countOfEmployeeFromPeople = MainConstants.kConstantMinusOne;
//		 Log.d("background", "24082015");
//		Log.d("index","" +index);
		try
		{
			countOfEmployeeFromPeopleJsonObject = new JSONObject();
			countOfEmployeeFromPeopleJsonObject = jsonParser .getJsonArrayInGetMethod(MainConstants.kConstantZohoPeoplecountApi +  MainConstants.kConstantPeopleAuthTokenForGetCount);
//			Log.i("countOfEmployeeFromPeopleJsonObject",""+countOfEmployeeFromPeopleJsonObject );
			// insert into logs

			if(countOfEmployeeFromPeopleJsonObject != null)
			{
				countOfEmployeeFromPeople = getCountEmployeeCountFromPeople(countOfEmployeeFromPeopleJsonObject);
//			Log.i("Count of Employee",""+countOfEmployeeFromPeople);
				if(countOfEmployeeFromPeople != MainConstants.kConstantMinusOne && countOfEmployeeFromPeople > 0)
				{
//				Log.i("Count of Employee",""+countOfEmployeeFromPeople);
					int countForLoop = (int)(double) Math.ceil((double)countOfEmployeeFromPeople / MainConstants.peopleApiRecordLimit);
//				Log.i("countForLoop",""+countForLoop);
					/*** based on count from people the loop gets executed ***/
					ArrayList<ZohoEmployeeRecord> employeeRecordOutSideLoop = new ArrayList<ZohoEmployeeRecord>();
					for(int iterateLoop = 0;iterateLoop < countForLoop; iterateLoop++)
					{
						// In People request API index is starting 1
						startingIndex = (iterateLoop * MainConstants.peopleApiRecordLimit) + MainConstants.kConstantOne;
						jsonObject = new JSONObject();
						try {
							jsonObject = jsonParser
									.getJsonArrayInGetMethod(MainConstants.peopleRecordsDownloadURL
											+ MainConstants.kConstantPeopleAuthToken
											+ MainConstants.peopleStartingIndex
											+ startingIndex
											+ MainConstants.peopleRecordsLimit
											+ MainConstants.peopleApiRecordLimit);
//						Log.i("JsonObject",""+jsonObject);
							if (jsonObject != null) {
								ArrayList<ZohoEmployeeRecord> employeeRecordInSideLoop = new ArrayList<ZohoEmployeeRecord>();
								employeeRecordInSideLoop = checkJsonObject(jsonObject);
//							Log.d("response", "total new data size per execution" + employeeRecordInSideLoop.size());
								//Toast.makeText(context, ""+employeeRecordInSideLoop, Toast.LENGTH_LONG).show();
								//Toast.makeText(context, ""+employeeRecordInSideLoop.size(), Toast.LENGTH_LONG).show();
								employeeRecordOutSideLoop.addAll(employeeRecordInSideLoop);
							}
						} catch (JSONException jsonException) {
							//error =true;
							//Log.d("window", "JSONException" + jsonException);
							setInternalLogs(context, ErrorConstants.backgroundProcessJsonException, jsonException.getMessage());
							//return null;
						} catch (TimeoutException timeoutException) {
							//error =true;
							//Log.d("window", "TimeoutException" + timeoutException);
							setInternalLogs(context, ErrorConstants.backgroundProcessTimeOutException, timeoutException.getMessage());

							//return null;
						}catch (IOException ioException) {

							setInternalLogs(context, ErrorConstants.backgroundProcessIOException, ioException.getMessage());
							//error =true;
							//Log.d("window", "IOException" + ioException);
							//return null;
						}
					}
//				Log.d("response", "total new data size" + employeeRecordOutSideLoop.size());
					employeeRecordList.addAll(employeeRecordOutSideLoop);
					if((employeeRecordOutSideLoop != null) && (countOfEmployeeFromPeople <= employeeRecordOutSideLoop.size()))
					{
						try
						{
//						  Log.d("response","drop old table ");
							sqliteHelper.dropOldEmployeeTable();
							ArrayList<ZohoEmployeeRecord> emp = sqliteHelper.getNewTableEmployeeList();
//									Log.d("response","data "+emp.size());
							sqliteHelper.renameNewEmployeeTable();
							emp = sqliteHelper.getEmployeeList();
//									Log.d("response","new data "+emp.size());
							int sizeInnerFromFetch = 0;
							int sizeInnerFromLocal = 0;
							if(employeeRecordOutSideLoop != null)
							{
								sizeInnerFromFetch = employeeRecordOutSideLoop.size();
							}
							if(emp != null)
							{
								sizeInnerFromLocal = emp.size();
							}
							setInternalLogs(context,ErrorConstants.peopleRecordCountWithSuccess,StringConstants.peopleRecordCountSuccess+countOfEmployeeFromPeople+MainConstants.kConstantNewLine+StringConstants.fetchRecordCount+sizeInnerFromFetch+MainConstants.kConstantNewLine+StringConstants.EmployeeCountInLocalMessage+sizeInnerFromLocal);
//									setInternalLogs(context,ErrorConstants.FetchEmployeeCountWithSuccess,MainConstants.fetchRecordCount+employeeRecordOutSideLoop.size());
//									setInternalLogs(context,ErrorConstants.EmployeeCountInLocal,MainConstants.EmployeeCountInLocalMessage+emp.size());
						}
						catch(Exception throwedException)
						{
							// insert the success into logs, to track for future use.
							setInternalLogs(context, ErrorConstants.backgroundProcessIOException,throwedException.getMessage());
							//return null;
						}
					}
					else
					{
						int sizeInner = 0;
						if(employeeRecordOutSideLoop != null)
						{
							sizeInner = employeeRecordOutSideLoop.size();
						}
						// insert the success into logs, to track for future use.
						setInternalLogs(context,ErrorConstants.peopleRecordCountWithFailure,StringConstants.peopleRecordCountFailure+countOfEmployeeFromPeople+MainConstants.kConstantNewLine+StringConstants.fetchRecordCount+sizeInner);
//					setInternalLogs(context,ErrorConstants.FetchEmployeeCountWithSuccess,MainConstants.fetchRecordCount+employeeRecordOutSideLoop.size());
						//return null;
					}
				}
//						Log.d("response", "total new data size" + employeeRecordOutSideLoop.size());
				else
				{
					// insert into logs
					setInternalLogs(context,ErrorConstants.RecordCountNotGetException,StringConstants.RecordCountNotGetExceptionMessage);
					//return null;
				}
			}
		}
		catch(JSONException jsonException)
		{
			// insert into logs
			//Log.d("count response", "json exception    " );
			setInternalLogs(context,ErrorConstants.RecordCountNotGetException,StringConstants.RecordCountNotGetExceptionMessage+jsonException.getMessage());
		}
		catch (TimeoutException e)
		{
			// insert into logs
			//Log.d("count response", "timeout exception    " );
			setInternalLogs(context,ErrorConstants.RecordCountNotGetException,StringConstants.RecordCountNotGetExceptionMessage+e.getMessage());
		} catch (IOException e)
		{
			// insert into logs
			//Log.d("count response", "input_output exception    " );
			setInternalLogs(context,ErrorConstants.RecordCountNotGetException,StringConstants.RecordCountNotGetExceptionMessage+e.getMessage());
		}
		MainConstants.alreadyStartedBackgroundProcess = NumberConstants.kConstantBackgroundStopped;
		return employeeRecordList;
	}

	/**
	 * Method used to get count of Employees
	 * from Zoho people.
	 *
	 * @author Gokul
	 * @created Aug 2016
	 *
	 */

	public int getCountEmployeeCountFromPeople(JSONObject employeeCountUrl)
	{
		int employeeCount = MainConstants.kConstantMinusOne;
		String response = null;
		String result = null;
		String count = null;
		JSONObject employeeCountUrlResponse = null;
		JSONObject employeeCountUrlResult = null;
		try {
			response = employeeCountUrl.getString(StringConstants.employeeCountUrlKeyResponse);
		} catch (JSONException e) {
			// insert into logs
		}
//		Log.d("responseCount ",""+response);
		if(response != null)
		{
			try {
				employeeCountUrlResponse = new JSONObject(response);
			} catch (JSONException e) {
				// insert into logs
			}
			if(employeeCountUrlResponse != null)
			{
				try {
					result = employeeCountUrlResponse.getString(StringConstants.employeeCountUrlKeyResult);
				} catch (JSONException e) {
					// insert into logs
				}
//		Log.d("resultCount ",""+result);
				if(result != null)
				{
					try {
						employeeCountUrlResult = new JSONObject(result);
					} catch (JSONException e) {
						// insert into logs
					}
					if(employeeCountUrlResult != null)
					{
						try
						{
							count = employeeCountUrlResult.getString(StringConstants.employeeCountUrlKeyRecordCount);
						} catch (JSONException e) {
							// insert into logs
						}
						if(count != null && isNumber(count))
						{
							employeeCount = Integer.parseInt(count);
						}
					}
				}
			}
		}
//		Log.d("employeeCount ",""+employeeCount);
		if(employeeCount == MainConstants.kConstantMinusOne)
		{
			setInternalLogs(context,ErrorConstants.RecordCountNotGetException,StringConstants.RecordCountNotGetExceptionMessage);
		}
		return employeeCount;
	}
	/**
	 * Method used to find the given string is number or not
	 *
	 * @author Gokul
	 * @created Aug 2016
	 *
	 */
	public Boolean isNumber(String number)
	{
		Boolean isNumber = false;
		if(number != null && number.matches("[0-9]+"))
		{
			isNumber = true;
		}
//		Log.d("isNumber ",""+isNumber);
		return isNumber;
	}


	/*
	 * Check the jsonObject
	 *
	 * @author jayapriya
	 *
	 * @created 25/02/2015
	 */
	private ArrayList<ZohoEmployeeRecord> splitJsonArray(JSONArray jsonArray) {

		Validation validation=new Validation();
		ArrayList<ZohoEmployeeRecord> employeeRecord = new ArrayList<ZohoEmployeeRecord>();
		ZohoEmployeeRecord employee;

		for (int i = MainConstants.kConstantZero; i < jsonArray.length(); i++) {
			JSONObject jsonProductObject = null;
			if (!jsonArray.isNull(i)) {
				try {
					jsonProductObject = jsonArray.getJSONObject(i);
				} catch (JSONException jsonError) {
					jsonProductObject = null;
				}
			} else {
				jsonProductObject = null;
			}


			if (jsonProductObject != null) {

				employee = new ZohoEmployeeRecord();

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeRecordId)) {
					try {
						employee.setEmployeeRecordId(jsonProductObject
								.getString(StringConstants.peopleEmployeeRecordId));
					} catch (JSONException jsonError) {
						employee.setEmployeeRecordId(MainConstants.kConstantEmpty);
					}
				} else {
					employee.setEmployeeRecordId(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeDeptName)) {

					try {

						employee.setEmployeeDept(jsonProductObject
								.getString(StringConstants.peopleEmployeeDeptName));
						//Log.d("employeedetails","if:"+ jsonProductObject.getString("departmentname"));
					} catch (JSONException jsonError) {
						//Log.d("employeedetails", "jsonError" + jsonError);
						employee.setEmployeeDept(MainConstants.kConstantEmpty);
					}
				} else {
					//Log.d("employeedetails", "else:");
					employee.setEmployeeDept(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject.isNull(StringConstants.peopleEmployeeID)) {

					try {
						employee.setEmployeeId(jsonProductObject
								.getString(StringConstants.peopleEmployeeID));
					} catch (JSONException jsonError) {
						employee.setEmployeeId(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeeId(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeFirstName)) {

					try {

						employee.setEmployeeFirstName(jsonProductObject
								.getString(StringConstants.peopleEmployeeFirstName));
						firstName = jsonProductObject
								.getString(StringConstants.peopleEmployeeFirstName);
					} catch (JSONException jsonError) {
						employee.setEmployeeFirstName(MainConstants.kConstantEmpty);
						firstName = MainConstants.kConstantEmpty;
					}
				} else {

					employee.setEmployeeFirstName(MainConstants.kConstantEmpty);
					firstName = MainConstants.kConstantEmpty;
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeLastName)) {

					try {

						employee.setEmployeeLastName(jsonProductObject
								.getString(StringConstants.peopleEmployeeLastName));
						lastName = jsonProductObject
								.getString(StringConstants.peopleEmployeeLastName);
					} catch (JSONException jsonError) {
						employee.setEmployeeLastName(MainConstants.kConstantEmpty);
						lastName = MainConstants.kConstantEmpty;
					}
				} else {

					employee.setEmployeeLastName(MainConstants.kConstantEmpty);
					lastName = MainConstants.kConstantEmpty;
				}

				employee.setEmployeeName(firstName.replaceAll("[^\\x1F-\\x7F]+","").trim()
						+ MainConstants.kConstantWhiteSpace + lastName.replaceAll("[^\\x1F-\\x7F]+","").trim());

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeStatusMsg)) {

					try {

						employee.setEmployeeStatus(jsonProductObject
								.getString(StringConstants.peopleEmployeeStatusMsg));
					} catch (JSONException jsonError) {
						employee.setEmployeeStatus(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeeStatus(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeWorkLocation)) {

					try {

						employee.setEmployeeSeatingLocation(jsonProductObject
								.getString(StringConstants.peopleEmployeeWorkLocation));

					} catch (JSONException jsonError) {
						employee.setEmployeeSeatingLocation(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeeSeatingLocation(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeMobileNo)) {

					try {

						employee.setEmployeeMobileNumber(validation.getValidMobileNumber(jsonProductObject
								.getString(StringConstants.peopleEmployeeMobileNo)));
					} catch (JSONException jsonError) {
						employee.setEmployeeMobileNumber(MainConstants.kConstantEmpty);
					}

				} else {

					employee.setEmployeeMobileNumber(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeEmailId)) {

					try {
						employee.setEmployeeEmailId(jsonProductObject
								.getString(StringConstants.peopleEmployeeEmailId));
					} catch (JSONException jsonError) {
						employee.setEmployeeEmailId(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeeEmailId(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject.isNull(StringConstants.peopleEmployeeZuid)) {

					try {
						employee.setEmployeeZuid(jsonProductObject
								.getString(StringConstants.peopleEmployeeZuid));
					} catch (JSONException jsonError) {
						employee.setEmployeeZuid(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeeZuid(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeePhoto)) {

					try {
						employee.setEmployeePhoto(jsonProductObject
								.getString(StringConstants.peopleEmployeePhoto));
					} catch (JSONException jsonError) {
						employee.setEmployeePhoto(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeePhoto(MainConstants.kConstantEmpty);
				}

				if (!jsonProductObject
						.isNull(StringConstants.peopleEmployeeExtension)) {

					try {
						employee.setEmployeeExtentionNumber(jsonProductObject
								.getString(StringConstants.peopleEmployeeExtension));
					} catch (JSONException jsonError) {
						employee.setEmployeeExtentionNumber(MainConstants.kConstantEmpty);
					}
				} else {

					employee.setEmployeeExtentionNumber(MainConstants.kConstantEmpty);
				}

				count = count + 1;
				employeeRecord.add(employee);
			}
		}
		return employeeRecord;
	}

	private ArrayList<ZohoEmployeeRecord> checkJsonObject(
			JSONObject jsonResponse) {

		ArrayList<ZohoEmployeeRecord> employeeDetailList = new ArrayList<ZohoEmployeeRecord>();

		ArrayList<ZohoEmployeeRecord> employeeDetail = new ArrayList<ZohoEmployeeRecord>();
		employeeDetail.clear();
		JSONArray jsonArray = null;

		if (!jsonResponse.isNull("#")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("#");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("a")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("a");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("b")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("b");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("c")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("c");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("d")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("d");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("e")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("e");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("f")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("f");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("g")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("g");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("h")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("h");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("i")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("i");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("j")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("j");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("k")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("k");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("l")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("l");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("m")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("m");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("n")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("n");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("o")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("o");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("p")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("p");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("q")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("q");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("r")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("r");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("s")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("s");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}

			}
		}
		if (!jsonResponse.isNull("t")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("t");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("u")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("u");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("v")) {
			jsonArray = jsonResponse.optJSONArray("v");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("w")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("w");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("x")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("x");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("y")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("y");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}
		if (!jsonResponse.isNull("z")) {
			jsonArray = new JSONArray();
			jsonArray = jsonResponse.optJSONArray("z");
			if (jsonArray != null) {
				employeeDetail = splitJsonArray(jsonArray);
				if(employeeDetail != null) {
					employeeDetailList.addAll(employeeDetail);
				}
			}
		}

		//Log.d("employeeDetails", "detailSize" + employeeDetailList.size());
		if ((employeeDetailList != null)
				&& (employeeDetailList.size() > MainConstants.kConstantZero)) {
			// sqliteHelper.deleteEmployee();
			try {
				sqliteHelper.insertEmployeeDetails(employeeDetailList);
			}catch (SQLiteDatabaseLockedException e1)
			{

			}
			//Log.d("employeeDetails", "detailSize" + sqliteHelper.getNewTableEmployeeList().size());
			// sqliteHelper.updateEmployeeVisitFrequency("CON1",5);
			// String[] employeesZuid = new String[employeeDetail.size()];
			// for (int i = 0; i < employeeDetail.size(); i++) {
			// Log.d("emp","zuid" +employeeDetail.get(i).getEmployeeName());
			// employeesZuid[i] = employeeDetail.get(i).getEmployeeZuid();
			// }
			// DownloadImage image= new DownloadImage();
			// image.execute(employeesZuid);

			// for (int i = 0; i < employeeRecord.size(); i++) {

			// employeeRecord.get(i).setEmployeePhoto(JSONParser
			// .downloadEmployeeImage(employeeRecord.get(i).getEmployeeZuid(),
			// employeeRecord.get(i).getEmployeeName()));
			// Log.d("photo", "" + employeeRecord.get(i).getEmployeePhoto());

			// }

			// Log.d("window", "created " );

			// Log.d("window",
			// / "Employee record size " + dispEmployee.size());
			// for (int i = 0; i < dispEmployee.size(); i++) {
			// Log.d("window", " visitor  "
			// + dispEmployee.get(i).employeeFirstName.toString());
			// }
		}
		else
		{
			//Log.d("return else","emp");
		}
		return employeeDetailList;
	}

	/*
	 * public void checkZohoPeople() { int c = 0; JSONObject json = null;
	 * JSONArray jsonarry, array; // int startingIndex = 0; //for(int index = 0;
	 * index < 30;index ++) {
	 *
	 * // startingIndex = index * 200; // //Log.d("startIndex","start"
	 * +startingIndex); try { json = jsonParser .getJsonArrayInGetMethod(
	 * "https://people.zoho.com/people/fetchPeopleContacts.do?authtoken=b2abba0d72c97328b3b8031aff79d5f4&sIndex="
	 * + 200 + "&limit=200"); } catch (IOException e) { catch block
	 * e.printStackTrace(); } catch (JSONException e) { //e.printStackTrace(); }
	 * catch(TimeoutException e) {
	 * //uploadCode=ErrorConstants.visitorSearchVisitorTimeOutError; } if (json
	 * != null) { //Log.d("json","response" +json); //Log.d("creator",
	 * "jsonObject" + json.length());
	 *
	 * if (!json.isNull("a")) { jsonarry = json.optJSONArray("a");
	 * //Log.d("creatora", "a" + jsonarry); //Log.d("creator", "jsonArray" +
	 * jsonarry.length()); } if (!json.isNull("b")) { array =
	 * json.optJSONArray("b"); //Log.d("creatorb", "b" + array);
	 * //Log.d("creator", "jsonArray" + array.length());
	 *
	 * }
	 *
	 * }
	 *
	 *
	 * }
	 */

	/*
	 * class GetEmployeeRecords extends
	 * AsyncTask<JSONObject,String,ArrayList<ZohoEmployeeRecord>>{
	 *
	 * @Override protected ArrayList<ZohoEmployeeRecord>
	 * doInBackground(JSONObject... jsonObject) { ZohoEmployeeRecord employee;
	 * // ArrayList<ZohoEmployeeRecord> employeeRecord = new
	 * ArrayList<ZohoEmployeeRecord>(); JSONArray jsonArray; if (jsonObject !=
	 * null) {
	 *
	 * for (int i = 0; i < jsonObject[i].length(); i++) { //Log.d("creator",
	 * "jsonObject" + jsonObject[i].length()); jsonArray =
	 * checkJsonObject(jsonObject[i]); //Log.d("creator", "jsonArray" +
	 * jsonArray.length());
	 *
	 * if (jsonArray != null) { //Log.d("creator", "jsonArray" + jsonArray); for
	 * (int j = MainConstants.kConstantZero; j < jsonArray .length(); j++) {
	 * JSONObject jsonProductObject; try {
	 *
	 * jsonProductObject = jsonArray.getJSONObject(j);
	 *
	 * //Log.d("json", "object" + jsonProductObject); //employeeRecord.clear();
	 *
	 * employee = new ZohoEmployeeRecord();
	 *
	 * employee.employeeRecordId = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeRecordId);
	 * //Log.d("employeedetails", "employeeDetailsinputline" +
	 * employee.employeeRecordId); employee.employeeId = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeID); //Log.d("employeedetails",
	 * "employeeDetailsinputline" + employee.employeeId); firstName =
	 * jsonProductObject .getString(MainConstants.peopleEmployeeFirstName);
	 * employee.employeeFirstName = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeFirstName); lastName =
	 * jsonProductObject .getString(MainConstants.peopleEmployeeLastName);
	 * employee.employeeLastName = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeLastName);
	 * employee.setEmployeeName(firstName + MainConstants.kConstantWhiteSpace +
	 * lastName); //Log.d("employeedetails", "employeeDetailsinputline" +
	 * employee.employeeName); // mobileNo = //
	 * jsonProductObject.getString("Mobile Phone"); //
	 * employee.employeePhoneNumber = // validatePhoneNo(mobileNo);
	 * employee.employeeMobileNumber = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeMobileNo);
	 * //Log.d("employeedetails", "employeeDetailsinputline" +
	 * employee.employeeMobileNumber); employee.employeeEmailId =
	 * jsonProductObject .getString(MainConstants.peopleEmployeeEmailId);
	 * //Log.d("employeedetails", "employeeDetailsinputline" +
	 * employee.employeeEmailId); employee.employeeZuid = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeZuid); if ((employee.employeeZuid
	 * != null) && (employee.employeeZuid.toString() !=
	 * MainConstants.kConstantEmpty) && (employee.employeeZuid.toString()
	 * .length() > MainConstants.kConstantZero)) { employee.employeePhoto =
	 * jsonProductObject .getString(MainConstants.peopleEmployeePhoto); if
	 * (employee.employeePhoto != null) { //Log.d("downloadbefore", "image" +
	 * employee.employeePhoto); employee.employeePhoto = JSONParser
	 * .downloadEmployeeImage(employee.employeeZuid, employee.employeeName);
	 * //Log.d("download", "image" + employee.employeePhoto); } }
	 * employee.employeeExtentionNumber = jsonProductObject
	 * .getString(MainConstants.peopleEmployeeExtension); //
	 * employee.employeePhoto
	 * =jsonProductObject.getString(MainConstants.peopleEmployeePhoto); //
	 * Testing //
	 * employee.employeeSeatingLocation=jsonProductObject.getString(MainConstants
	 * .peopleEmployeeSeatingLocation); //
	 * employee.employeeStatus=jsonProductObject
	 * .getString(MainConstants.peopleEmployeeStatus);
	 *
	 * count = count + 1; //Log.d("count", "count" + count);
	 * employeeRecord.add(employee); } catch (JSONException e) {
	 * e.printStackTrace(); } } } }
	 *
	 *
	 * } return employeeRecord; } }
	 */

//	class DownloadImage extends AsyncTask<String, String, String> {
//
//		int total;
//		int count = 0;
//
//		@Override
//		protected String doInBackground(String... zuid) {
//			//Log.d("async", "downloadImage");
//			//Log.d("size", "" + zuid.length);
//			for (int i = 0; i < zuid.length; i++) {
//				try {
//					HttpClient httpClient = new DefaultHttpClient();
//					HttpGet httpGet = new HttpGet(
//							"https://contacts.zoho.com/file/download?ID="
//									+ zuid[i]
//									+ "&fsSendthumb&authtoken=10eba051409724c34298638b2c86c3a9");
//					HttpResponse response = httpClient.execute(httpGet);
//					//Log.d("request", "" + response.getStatusLine());
//					if (response.getStatusLine().getStatusCode() == MainConstants.kConstantSuccessCode) {
//
//						HttpEntity entity = response.getEntity();
//						InputStream input = entity.getContent();
//						File fileDirectory = new File(
//								MainConstants.kConstantFileDirectory
//										+ MainConstants.kConstantFileEmployeeImageFolder);
//
//						if (!fileDirectory.exists()) {
//							fileDirectory.mkdirs();
//						}
//						final File file = new File(
//								fileDirectory.getAbsolutePath(),
//								zuid[i]
//										+ MainConstants.kConstantImageFileFormat);
//
//						if (file.exists()) {
//							file.delete();
//						}
//						file.createNewFile();
//						// Output stream to write file
//						OutputStream output = new FileOutputStream(file);
//						byte data[] = new byte[MainConstants.kConstantBufferSize];
//						total = MainConstants.kConstantZero;
//						while ((count = input.read(data)) != MainConstants.kConstantMinusOne) {
//							total += count;
//							// writing data to file
//							output.write(data, MainConstants.kConstantZero,
//									count);
//						}
//						// flushing output
//						output.flush();
//						// closing streams
//						output.close();
//						input.close();
//
//					}
//
//				} catch (MalformedURLException e) {
//
//				} catch (ClientProtocolException e) {
//
//				} catch (IOException e) {
//
//				}
//			}
//			return null;
//		}
//	}


	/*
	 * set internal log and insert to local sqlite
	 *
	 *
	 * @created On 20150910
	 *
	 * @author karthick
	 */
	public void setInternalLogs(Context context, int errorCode,
								String errorMessage) {
		if(context != null && errorMessage != null && !errorMessage.isEmpty())
		{
			MainConstants.alreadyStartedBackgroundProcess = NumberConstants.kConstantBackgroundStopped;
			//SingletonErrorMessage.getInstance().setErrorMessage(errorMessage);
			NetworkConnection networkConnection = new NetworkConnection();
			InternalAppLogs internalAppLogs = new InternalAppLogs();
			internalAppLogs.setErrorCode(errorCode);
			internalAppLogs.setErrorMessage(errorMessage);
			internalAppLogs.setLogDate(new Date().getTime());
			internalAppLogs.setWifiSignalStrength(networkConnection
					.getCurrentWifiSignalStrength(context));
			DbHelper.getInstance(context).insertLogs(internalAppLogs);
		}
	}
}
