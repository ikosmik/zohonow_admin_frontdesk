package appschema;

/**
 * 
 * @author jayapriya On 12/11/2014
 * 
 */
public class VisitorRecord {
	public Long recordId;
	public String name;
	public String Address;
	public String mailId;
	public String mobileNo;
	public String designation;
	public String company;
	public String companyWebsite;
	public String fax;
	public String businessCard;
	public Integer visitorIdInOffMode;

	public Integer getVisitorIdInOffMode() {
		return visitorIdInOffMode;
	}

	public void setVisitorIdInOffMode(Integer visitorIdInOffMode) {
		this.visitorIdInOffMode = visitorIdInOffMode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;

	}

	public String getMailId() {
		return mailId;

	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public String getMobileNo() {
		return mobileNo;

	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;

	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAddress() {
		return Address;

	}

	public void setAddress(String Address) {
		this.Address = Address;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getBusinessCard() {
		return businessCard;
	}

	public void setBusinessCard(String businessCard) {
		this.businessCard = businessCard;
	}

}
