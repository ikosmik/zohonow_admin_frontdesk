package appschema;

import constants.MainConstants;

/**
 * 
 * @author jayapriya On 22/08/2014 Store the singleton variable.
 * 
 */
public class SingletonVisitorProfile {
	private static SingletonVisitorProfile visitor = null;

	// private String mString;
	// private String recordId;
	private String deviceLocation;
	private String visitorCreatorId;
	private String visitorName;
	private String designation;
	private String company;
	private String companyWebsite;
	private String fax;
	private String address;
	private String phoneNo;
	private String email;
	private String otherText;
	private String securitySign;
	private byte[] visitorSign;
	private byte[] visitorSignWithBG;
	private byte[] visitorImage;
	private String dateOfVisit;
	private String visitorLocation;
	private String currentLocation;
	private String visitorBusinessCard;
	private String contact;
	private Integer codeNo;
	private Integer minimumFrequency;
	private Integer maximumFrequency;
	private String purposeOfVisit;
	private String purposeOfVisitCode;
	private String purposeId;
	private Integer visitfrequency;
	private Integer locationCode;
	private String officeLocation;
	private Integer Gmtdiff;
	private String visitId;
	//private String candidateId;
	private String qrcode;
	private String visitorBadge;
	private String expectedDateOfVisit;
	private String dateOfVisitLocal;
	private long tempIDNumber;
	private String deviceSerialNumber;
	private String deviceMake;
	private int visitorThumbPhotoUploaded;
	private int retryCount;
	private int uploadedCode=0;
	private int visitorType = MainConstants.newVisitor;
	private boolean isHaveDevice=false;
	private boolean isExtDeviceOne=false,isExtDeviceTwo=false,isExtDeviceThree=false,
			isExtDeviceFive=false,isExtDeviceFour=false;

	public long getVisitorBadgeFileName() {
		return visitorBadgeFileName;
	}

	public void setVisitorBadgeFileName(long visitorBadgeFileName) {
		this.visitorBadgeFileName = visitorBadgeFileName;
	}

	private long visitorBadgeFileName;

	private int EmployeeActivityCode = 0;

	//private int candidateThumbPhotoUploaded;

	private String otherDeviceDetails;

	private boolean isVisitorPreApproved;

	private String OTPRequestCreatorID;
	private String entredOTPNumber;

	public String getEntredOTPNumber() {
		return entredOTPNumber;
	}

	public void setEntredOTPNumber(String entredOTPNumber) {
		this.entredOTPNumber = entredOTPNumber;
	}

	public String getOTPRequestCreatorID() {
		return OTPRequestCreatorID;
	}

	public void setOTPRequestCreatorID(String OTPRequestCreatorID) {
		this.OTPRequestCreatorID = OTPRequestCreatorID;
	}

	public boolean isVisitorPreApproved() {
		return isVisitorPreApproved;
	}

	public void setVisitorPreApproved(boolean visitorPreApproved) {
		isVisitorPreApproved = visitorPreApproved;
	}

	public String getOtherDeviceDetails() {
		return otherDeviceDetails;
	}

	public void setOtherDeviceDetails(String otherDeviceDetails) {
		this.otherDeviceDetails = otherDeviceDetails;
	}


	/**
	 * Created by jayapriya On 22/08/2014 intialize the singleton variables.
	 */
	public SingletonVisitorProfile() {

		// Log.d("Android", "SingletonVisitorProfile");
		visitorName = MainConstants.kConstantEmpty;
		designation = MainConstants.kConstantEmpty;
		email = MainConstants.kConstantEmpty;
		fax = MainConstants.kConstantEmpty;
		address = MainConstants.kConstantEmpty;
		phoneNo = MainConstants.kConstantEmpty;
		company = MainConstants.kConstantEmpty;
		companyWebsite = MainConstants.kConstantEmpty;
		visitorImage = new byte[0];
		contact = MainConstants.kConstantEmpty;
		visitorBusinessCard = MainConstants.kConstantEmpty;
		visitorCreatorId = MainConstants.kConstantEmpty;
		otherText = MainConstants.kConstantEmpty;
		securitySign = MainConstants.kConstantEmpty;
		visitorSign = new byte[0];
		visitorSignWithBG = new byte[0];
		dateOfVisit = MainConstants.kConstantEmpty;
		visitorLocation = MainConstants.kConstantEmpty;
		currentLocation = MainConstants.kConstantEmpty;
		visitorBusinessCard = MainConstants.kConstantEmpty;
		contact = MainConstants.kConstantEmpty;
		codeNo = MainConstants.kConstantMinusOne;
		purposeOfVisit = MainConstants.kConstantEmpty;
		purposeOfVisitCode = MainConstants.kConstantEmpty;
		purposeId = MainConstants.kConstantEmpty;
		visitfrequency = MainConstants.kConstantMinusOne;
		locationCode = MainConstants.kConstantMinusOne;
		officeLocation = MainConstants.kConstantEmpty;
		Gmtdiff = MainConstants.kConstantMinusOne;
		visitId = MainConstants.kConstantEmpty;
	//	candidateId = MainConstants.kConstantEmpty;
		dateOfVisitLocal = MainConstants.kConstantEmpty;

		deviceSerialNumber = MainConstants.kConstantEmpty;
		deviceMake = MainConstants.kConstantEmpty;
		visitorThumbPhotoUploaded = MainConstants.kConstantMinusOne;
		setRetryCount(MainConstants.kConstantZero);
	//	candidateThumbPhotoUploaded = MainConstants.kConstantMinusOne;
		visitorType = MainConstants.newVisitor;
		isHaveDevice=false;
		isExtDeviceOne=false;
		isExtDeviceTwo=false;
		isExtDeviceThree=false;
		isExtDeviceFive=false;
		isExtDeviceFour=false;
		setUploadedCode(0);
		EmployeeActivityCode = MainConstants.kConstantZero;
		entredOTPNumber = MainConstants.kConstantEmpty;
		OTPRequestCreatorID= MainConstants.kConstantEmpty;

		otherDeviceDetails = MainConstants.kConstantEmpty;
		isVisitorPreApproved=false;
	}

	/**
	 * Created by jayapriya On 22/08/2014 the singleton variable
	 */
	public void clearInstance() {
		visitorName = MainConstants.kConstantEmpty;
		designation = MainConstants.kConstantEmpty;
		email = MainConstants.kConstantEmpty;
		fax = MainConstants.kConstantEmpty;
		address = MainConstants.kConstantEmpty;
		phoneNo = MainConstants.kConstantEmpty;
		company = MainConstants.kConstantEmpty;
		companyWebsite = MainConstants.kConstantEmpty;
		visitorImage = new byte[0];
		contact = MainConstants.kConstantEmpty;
		visitorBusinessCard = MainConstants.kConstantEmpty;
		visitorCreatorId = MainConstants.kConstantEmpty;
		otherText = MainConstants.kConstantEmpty;
		securitySign = MainConstants.kConstantEmpty;
		visitorSign = new byte[0];
		visitorSignWithBG = new byte[0];
		dateOfVisit = MainConstants.kConstantEmpty;
		visitorLocation = MainConstants.kConstantEmpty;
		currentLocation = MainConstants.kConstantEmpty;
		visitorBusinessCard = MainConstants.kConstantEmpty;
		contact = MainConstants.kConstantEmpty;
		codeNo = MainConstants.kConstantMinusOne;
		purposeOfVisit = MainConstants.kConstantEmpty;
		purposeOfVisitCode = MainConstants.kConstantEmpty;
		purposeId = MainConstants.kConstantEmpty;
		visitfrequency = MainConstants.kConstantMinusOne;
		Gmtdiff = MainConstants.kConstantMinusOne;
		visitId = MainConstants.kConstantEmpty;
	//	candidateId = MainConstants.kConstantEmpty;
		dateOfVisitLocal = MainConstants.kConstantEmpty;
		tempIDNumber = 0;
		deviceSerialNumber = MainConstants.kConstantEmpty;
		deviceMake = MainConstants.kConstantEmpty;
		visitorThumbPhotoUploaded = MainConstants.kConstantMinusOne;
		setRetryCount(MainConstants.kConstantZero);
		//candidateThumbPhotoUploaded = MainConstants.kConstantMinusOne;
		visitorType = MainConstants.newVisitor;
		isHaveDevice=false;
		isHaveDevice=false;
		isExtDeviceOne=false;
		isExtDeviceTwo=false;
		isExtDeviceThree=false;
		isExtDeviceFive=false;
		isExtDeviceFour=false;
		setUploadedCode(MainConstants.kConstantZero);
		EmployeeActivityCode = MainConstants.kConstantZero;

		otherDeviceDetails = MainConstants.kConstantEmpty;
		isVisitorPreApproved=false;
		entredOTPNumber = MainConstants.kConstantEmpty;
		OTPRequestCreatorID= MainConstants.kConstantEmpty;

	}

	/**
	 * Created by Jayapriya On 22/08/2014 create Object of Singleton variable.
	 * 
	 * @return
	 */
	public static SingletonVisitorProfile getInstance() {
		if (visitor == null) {
			visitor = new SingletonVisitorProfile();
		}
		// Log.d("window", "SingletonObject");
		return visitor;
	}

	// get method of visitor name
	public String getVisitorName() {
		return this.visitorName;
	}

	// set method of visitorName
	public void setVisitorName(String data) {
		visitorName = data;
	}

	// get method of Address
	public String getAddress() {
		return this.address;
	}

	// set method of Address
	public void setAddress(String data) {
		address = data;
	}

	// get Method of Fax
	public String getFax() {
		return this.fax;
	}

	// set method of Fax
	public void setFax(String data) {
		fax = data;
	}

	// get method of EMail
	public String getEmail() {
		return this.email;
	}

	// set method ofE Mail
	public void setEmail(String data) {
		email = data;
	}

	// get method of phoneNo
	public String getPhoneNo() {
		return phoneNo;

	}

	// set method of PhoneNo
	public void setPhoneNo(String value) {
		phoneNo = value;
	}

	// get method of Company
	public String getCompany() {
		return company;

	}

	// set method of company
	public void setCompany(String data) {
		company = data;
	}

	// get method of companyWebsite
	public String getCompanyWebsite() {
		return companyWebsite;
	}

	// set method of companyWebsite
	public void setCompanyWebSite(String data) {
		companyWebsite = data;
	}

	// get method of Designation
	public String getDesignation() {
		return designation;
	}

	// set method of Designation
	public void setDesignation(String data) {
		designation = data;
	}

	// get method of other text
	public String getOtherText() {
		return otherText;
	}

	// set method of other text
	public void setOtherText(String data) {
		otherText = data;
	}

	// get method of visitor location
	public String getVisitorLocation() {
		return visitorLocation;
	}

	// set method of visitorLocation
	public void setVisitorLocation(String location) {
		visitorLocation = location;
	}

	// get method of visitorImage
	public byte[] getVisitorImage() {
		return visitorImage;
	}

	// set method of visitorImage
	public void setVisitorImage(byte[] image) {
		visitorImage = image;
	}

	// get method of visitorSign
	public byte[] getVisitorSign() {
		return visitorSign;
	}

	// set method of visitorSign
	public void setVisitorSign(byte[] sign) {
		visitorSign = sign;
	}

	// get method of visitorContact
	public String getContact() {
		return contact;
	}

	// set method of visitorContact
	public void setContact(String data) {
		contact = data;
	}

	// get method of businessCard
	public String getVisitorBusinessCard() {
		return visitorBusinessCard;
	}

	// set method of businessCard
	public void setVisitorBusinessCard(String card) {
		visitorBusinessCard = card;
	}

	// get method of SecuritySign
	public String getSecuritySign() {
		return securitySign;
	}

	// set method of securitySign
	public void setSecuritySign(String securitySign) {
		this.securitySign = securitySign;
	}

	// get method of current Location
	public String getCurrentLocation() {
		return currentLocation;
	}

	// set method of currentLocation
	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	// get method of dateOfVisit
	public String getDateOfVisit() {
		return dateOfVisit;
	}

	// set method of DateOfVisit
	public void setDateOfVisit(String dateOfVisit) {
		this.dateOfVisit = dateOfVisit;
	}

	// get method of codeNo
	public Integer getCodeNo() {
		return codeNo;
	}

	// set method of CodeNo
	public void setCodeNo(Integer codeNo) {
		this.codeNo = codeNo;
	}

	// get method of MinimumFrequency
	public Integer getMinimumFrequency() {
		return minimumFrequency;
	}

	// set method of MinimumFrequency
	public void setMinimumFrequency(Integer minimumFrequency) {
		this.minimumFrequency = minimumFrequency;
	}

	// get method of maximumFrequency
	public Integer getMaximumFrequency() {
		return maximumFrequency;
	}

	// set method of maximumFrequency
	public void setMaximumFrequency(Integer maximumFrequency) {
		this.maximumFrequency = maximumFrequency;
	}

	// get method of purposeOfvisit
	public String getPurposeOfVisit() {
		return purposeOfVisit;
	}

	// set method of purposeOfVisit
	public void setPurposeOfVisit(String visit) {
		this.purposeOfVisit = visit;
	}

	// get method of visitFrequency
	public Integer getVisitfrequency() {
		return visitfrequency;
	}

	// set method of visitFrequency
	public void setVisitfrequency(Integer visitfrequency) {
		this.visitfrequency = visitfrequency;
	}

	// get method of purposeId
	public String getPurposeId() {
		return purposeId;
	}

	// set method of purposeId
	public void setPurposeId(String purposeId) {
		this.purposeId = purposeId;
	}

	// get method of locationCode
	public Integer getLocationCode() {
		return locationCode;
	}

	// set method of locationCode
	public void setLocationCode(Integer locationCode) {
		this.locationCode = locationCode;
	}

	// get method of officeLocation
	public String getOfficeLocation() {
		return officeLocation;
	}

	// set method of officeLocation
	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = officeLocation;
	}

	// get method of getGmtDifference
	public Integer getGmtdiff() {
		return Gmtdiff;
	}

	// set method of GmtDifference
	public void setGmtdiff(Integer gmtdiff) {
		Gmtdiff = gmtdiff;
	}

	// get method of VisitId
	public String getVisitId() {
		return visitId;
	}

	// set method of VisitId
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}

	// get method of getQrCode
	public String getQrcode() {
		return qrcode;
	}

	// set method of QrCode
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	// get method of VisitorBadge
	public String getVisitorBadge() {
		return visitorBadge;
	}

	// set method of VisitorBadge
	public void setVisitorBadge(String visitorBadge) {
		this.visitorBadge = visitorBadge;
	}

	public String getExpectedDateOfVisit() {
		return expectedDateOfVisit;
	}

	public void setExpectedDateOfVisit(String expectedDateOfVisit) {
		this.expectedDateOfVisit = expectedDateOfVisit;
	}

	public String getVisitorCreatorId() {
		return visitorCreatorId;
	}

	public void setVisitorCreatorId(String visitorCreatorId) {
		this.visitorCreatorId = visitorCreatorId;
	}

	public String getDateOfVisitLocal() {
		return dateOfVisitLocal;
	}

	public void setDateOfVisitLocal(String dateOfVisitLocal) {
		this.dateOfVisitLocal = dateOfVisitLocal;
	}

	public long getTempIDNumber() {
		return tempIDNumber;
	}

	public void setTempIDNumber(long tempIDNumber) {
		this.tempIDNumber = tempIDNumber;
	}

	public String getDeviceSerialNumber() {
		return deviceSerialNumber;
	}

	public void setDeviceSerialNumber(String deviceSerialNumber) {
		this.deviceSerialNumber = deviceSerialNumber;
	}

	public String getDeviceMake() {
		return deviceMake;
	}

	public void setDeviceMake(String deviceMake) {
		this.deviceMake = deviceMake;
	}

	public String getPurposeOfVisitCode() {
		return purposeOfVisitCode;
	}

	public void setPurposeOfVisitCode(String purposeOfVisitCode) {
		this.purposeOfVisitCode = purposeOfVisitCode;
	}

	public int getVisitorThumbPhotoUploaded() {
		return visitorThumbPhotoUploaded;
	}

	public void setVisitorThumbPhotoUploaded(int visitorThumbPhotoUploaded) {
		this.visitorThumbPhotoUploaded = visitorThumbPhotoUploaded;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public int getVisitorType() {
		return visitorType;
	}

	public void setVisitorType(int visitorType) {
		this.visitorType = visitorType;
	}

	public boolean isExtDeviceOne() {
		return isExtDeviceOne;
	}

	public void setExtDeviceOne(boolean isExtDeviceOne) {
		this.isExtDeviceOne = isExtDeviceOne;
	}

	public boolean isExtDeviceTwo() {
		return isExtDeviceTwo;
	}

	public void setExtDeviceTwo(boolean isExtDeviceTwo) {
		this.isExtDeviceTwo = isExtDeviceTwo;
	}

	public boolean isExtDeviceThree() {
		return isExtDeviceThree;
	}

	public void setExtDeviceThree(boolean isExtDeviceThree) {
		this.isExtDeviceThree = isExtDeviceThree;
	}

	public boolean isExtDeviceFive() {
		return isExtDeviceFive;
	}

	public void setExtDeviceFive(boolean isDeviceFive) {
		this.isExtDeviceFive = isDeviceFive;
	}

	public boolean isExtDeviceFour() {
		return isExtDeviceFour;
	}

	public void setExtDeviceFour(boolean isExtDeviceFour) {
		this.isExtDeviceFour = isExtDeviceFour;
	}

	public byte[] getVisitorSignWithBG() {
		return visitorSignWithBG;
	}

	public void setVisitorSignWithBG(byte[] visitorSignWithBG) {
		this.visitorSignWithBG = visitorSignWithBG;
	}

	public boolean isHaveDevice() {
		return isHaveDevice;
	}

	public void setisHaveDevice(boolean iHaveDevice) {
		this.isHaveDevice = iHaveDevice;
	}

	public int getUploadedCode() {
		return uploadedCode;
	}

	public void setUploadedCode(int uploadedCode) {
		this.uploadedCode = uploadedCode;
	}

	public String getDeviceLocation() {
		return deviceLocation;
	}

	public void setDeviceLocation(String deviceLocation) {
		this.deviceLocation = deviceLocation;
	}

	public int getEmployeeActivityCode() {
		return EmployeeActivityCode;
	}

	public void setEmployeeActivityCode(int employeeActivityCode) {
		EmployeeActivityCode = employeeActivityCode;
	}

//	public String getCandidateId() {
//		return candidateId;
//	}
//
//	public void setCandidateId(String candidateId) {
//		this.candidateId = candidateId;
//	}
//
//	public int getCandidateThumbPhotoUploaded() {
//		return candidateThumbPhotoUploaded;
//	}
//
//	public void setCandidateThumbPhotoUploaded(int candidateThumbPhotoUploaded) {
//		this.candidateThumbPhotoUploaded = candidateThumbPhotoUploaded;
//	}
}
