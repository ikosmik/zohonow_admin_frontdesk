package appschema;

import constants.MainConstants;
import constants.NumberConstants;

/**
 * Created by Karthikeyan D S on 24/03/18.
 */

public class SingletonMetaData {

    private static final SingletonMetaData ourInstance = new SingletonMetaData();
    private int themeCode = MainConstants.kConstantMinusOne;
    private String deviceLocation;
    private int applicationMode;
    private String deviceID;

    public String getDeviceID() {
        return deviceID;
    }
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }
    public int getApplicationMode() {
        return applicationMode;
    }
    public void setApplicationMode(int applicationMode) {
        this.applicationMode = applicationMode;
    }
    public String getDeviceLocation() {
        return deviceLocation;
    }
    public void setDeviceLocation(String deviceLocation) {
        this.deviceLocation = deviceLocation;
    }
    public int getThemeCode() {
        return themeCode;
    }
    public void setThemeCode(int themeCode) {
        this.themeCode = themeCode;
    }

    public static SingletonMetaData getInstance() {
        return ourInstance;
    }

    public void clearInstance()
    {
        themeCode=  NumberConstants.kConstantMinusOne;
        deviceLocation=null;
        applicationMode=  NumberConstants.kConstantMinusOne;
        deviceID=null;
    }
}
