package appschema;

import constants.MainConstants;
/*
 * Created by jayapriya on 31/12/14
 * Create singleton class to store employee records.
 */
public class SingletonEmployeeProfile {

	private static SingletonEmployeeProfile employee = null;

	
	private String employeeFirstName;
	private String employeeLastName;
	private String employeeName;
	private String employeeMailId;
	private String employeeId;
	private String employeeSeatingLocation;
	private String employeeExtentionNumber;
	private String employeePhoto;
	private String employeeRecordId;
	private String employeeZuid;
	private String employeeDepartment;
	private String employeeMobileNo;
    private String employeeStatus;
    private String employeeBadge;
	private long employeeCheckInTime;
	private long employeeCommonCheckInTime;
	private long employeeRecordID;

	private int EmployeeForgetIdErrorCode;




	/**
	 * Created by jayapriya On 10/12/14 intialize the singleton variables.
	 */
	public SingletonEmployeeProfile() {

		employeeFirstName = MainConstants.kConstantEmpty;
		employeeLastName = MainConstants.kConstantEmpty;
		employeeName = MainConstants.kConstantEmpty;
		employeePhoto = MainConstants.kConstantEmpty;
		employeeRecordId = MainConstants.kConstantEmpty;
		employeeZuid = MainConstants.kConstantEmpty;
		employeeMobileNo = MainConstants.kConstantEmpty;
		employeeMailId = MainConstants.kConstantEmpty;
		employeeId = MainConstants.kConstantEmpty;
		employeeSeatingLocation = MainConstants.kConstantEmpty;
		employeeExtentionNumber = MainConstants.kConstantEmpty;
        employeeDepartment=MainConstants.kConstantEmpty;
        employeeStatus=MainConstants.kConstantEmpty;
		employeeCheckInTime = MainConstants.kConstantMinusOne;
		employeeCommonCheckInTime = MainConstants.kConstantMinusOne;
		employeeRecordID = MainConstants.kConstantMinusOne;
		EmployeeForgetIdErrorCode= MainConstants.kConstantMinusOne;
	}

	/**
	 * Created by jayapriya On 10/12/14 clear the singleton variable
	 */
	public void clearInstance() {
		employeeFirstName = MainConstants.kConstantEmpty;
		employeeLastName = MainConstants.kConstantEmpty;
		employeeName = MainConstants.kConstantEmpty;
		employeePhoto = MainConstants.kConstantEmpty;
		employeeRecordId = MainConstants.kConstantEmpty;
		employeeZuid = MainConstants.kConstantEmpty;
		employeeMobileNo = MainConstants.kConstantEmpty;
		employeeMailId = MainConstants.kConstantEmpty;
		employeeId = MainConstants.kConstantEmpty;
		employeeSeatingLocation = MainConstants.kConstantEmpty;
		employeeExtentionNumber = MainConstants.kConstantEmpty;
        employeeDepartment =MainConstants.kConstantEmpty;
        employeeStatus=MainConstants.kConstantEmpty;
        employeeCheckInTime = MainConstants.kConstantMinusOne;
        employeeCommonCheckInTime = MainConstants.kConstantMinusOne;
		employeeRecordID = MainConstants.kConstantMinusOne;
		EmployeeForgetIdErrorCode= MainConstants.kConstantMinusOne;
	}

	/**
	 * Created by Jayapriya On 10/12/14 create Object of Singleton variable.
	 * 
	 * @return
	 */
	public static SingletonEmployeeProfile getInstance() {
		if (employee == null) {
			employee = new SingletonEmployeeProfile();
		}
		// Log.d("window", "SingletonObject");
		return employee;
	}

	// get method of employeeFirstName
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	// set method of employeeFirstName
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	public String getEmployeeMailId() {
		return employeeMailId;
	}

	public void setEmployeeMailId(String employeeMailId) {
		this.employeeMailId = employeeMailId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeSeatingLocation() {
		return employeeSeatingLocation;
	}

	public void setEmployeeSeatingLocation(String employeeSeatingLocation) {
		this.employeeSeatingLocation = employeeSeatingLocation;
	}

	public String getEmployeeExtentionNumber() {
		return employeeExtentionNumber;
	}

	public void setEmployeeExtentionNumber(String employeeExtentionNumber) {
		this.employeeExtentionNumber = employeeExtentionNumber;
	}

	public String getEmployeePhoto() {
		return employeePhoto;
	}

	public void setEmployeePhoto(String employeePhoto) {
		this.employeePhoto = employeePhoto;
	}

	public String getEmployeeRecordId() {
		return employeeRecordId;
	}

	public void setEmployeeRecordId(String employeeRecordId) {
		this.employeeRecordId = employeeRecordId;
	}

	public String getEmployeeLastName() {
		return employeeLastName;
	}

	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	public String getEmployeeName() {
		return employeeName.replaceAll("[^\\x1F-\\x7F]+","").trim();
	}
	public void setEmployeeName(String employeeName) {

		this.employeeName = employeeName.replaceAll("[^\\x1F-\\x7F]+","").trim();
	}

		public String getEmployeeDepartment() {
		return employeeDepartment;
	}

	public void setEmployeeDepartment(String employeeDepartment) {
		this.employeeDepartment = employeeDepartment;
	}


	
	public String getEmployeeZuid() {
		return employeeZuid;
	}

	public void setEmployeeZuid(String employeeZuid) {
		this.employeeZuid = employeeZuid;
	}

	
	public String getEmployeeMobileNo() {
		return employeeMobileNo;
	}

	public void setEmployeeMobileNo(String employeeMobileNo) {
		this.employeeMobileNo = employeeMobileNo;
	}


    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

	public String getEmployeeBadge() {
		return employeeBadge;
	}

	public void setEmployeeBadge(String employeeBadge) {
		this.employeeBadge = employeeBadge;
	}

	public long getEmployeeCheckInTime() {
		return employeeCheckInTime;
	}

	public void setEmployeeCheckInTime(long employeeCheckInTime) {
		this.employeeCheckInTime = employeeCheckInTime;
	}

	public long getEmployeeCommonCheckInTime() {
		return employeeCommonCheckInTime;
	}

	public void setEmployeeCommonCheckInTime(long employeeCommonCheckInTime) {
		this.employeeCommonCheckInTime = employeeCommonCheckInTime;
	}

	public long getEmployeeRecordID() {
		return employeeRecordID;
	}

	public void setEmployeeRecordID(long employeeRecordID) {
		this.employeeRecordID = employeeRecordID;
	}


	public int getEmployeeForgetIdErrorCode() {
		return EmployeeForgetIdErrorCode;
	}

	public void setEmployeeForgetIdErrorCode(int employeeForgetIdErrorCode) {
		EmployeeForgetIdErrorCode = employeeForgetIdErrorCode;
	}

}
