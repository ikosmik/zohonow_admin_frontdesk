package appschema;

/**
 * 
 * @author jayapriya
 *
 */
public class VisitRecord {
	public Integer visitIdInOffMode;
	public  String purpose;
	public  String dateOfVisit;
	public  String officeLocation;
	public  String employeeEmailId;
	public  Integer codeNumber;
	public  String Approval;
	public  String visitorPhoto;
	public  String visitorQrCode;
	public  String visitorSign;
	public  Boolean isItUpload;
	public  String employeeName;
	public  Integer employeeID;
	public String employeePhoneNo;
	//public  String expectedDateOfVisit;
	public  Integer officeLocationNumber;
	public  Integer GMTDiffMinutes;
	public Integer getVisitIdInOffMode()
	{
		return visitIdInOffMode;
	}
	public void setVisitIdInOffMode(Integer visitIdInOffMode){
		this.visitIdInOffMode = visitIdInOffMode;
	}
	public String getEmployeePhoneNo(){
		return employeePhoneNo;
	}
	public void setEmployeePhoneNo(String employeePhoneNo){
		this.employeePhoneNo = employeePhoneNo;
	}
	public String getPurpose()
	{
		return purpose;
		
	}
	public void setPurpose(String purpose){
		this.purpose = purpose;
	}
	public String getDateOfVisit()
	{
		return dateOfVisit;
	}
	public void setDateOfVisit(String dateOfVisit)
	{
		this.dateOfVisit = dateOfVisit;
	}
	public String getOfficeLocation(){
	     return officeLocation;
	}
	public void setOfficeLocation(String officeLocation)
	{
		this.officeLocation = officeLocation;
	}
	public String getEmployeeEmailId(){
		return employeeEmailId;
	}
	public void setEmployeeEmailId(String employeeEmailId){
		this.employeeEmailId = employeeEmailId;
	}
	public Integer getCodeNumber(){
		return codeNumber;
	}
	public void setCodeNumber(Integer codeNumber){
		this.codeNumber = codeNumber;
	}
	public String getApproval(){
		return Approval;
	}
	public void setApproval(String Approval){
		this.Approval = Approval;
	}
	public String getVisitorPhoto(){
		return visitorPhoto;
	}
	public void setVisitorPhoto(String visitorPhoto){
		this.visitorPhoto = visitorPhoto;
	}
	public String getVisitorQrCode(){
		return visitorQrCode;
	}
	public void setVisitorQrCode(String visitorQrCode){
		this.visitorQrCode = visitorQrCode;
	}
	public String getVisitorSign(){
		return visitorSign;
	}
	public void setVisitorSign(String visitorSign){
		this.visitorSign = visitorSign;
	}
	
	public boolean getIsItUpload(){
		return isItUpload;
	}
	public void setIsItUpload(boolean isItUpload){
		this.isItUpload = isItUpload;
	}
	public String getEmployeeName(){
		return employeeName;
	}
	public void setEmployeeName(String employeeName){
		this.employeeName = employeeName;
	}
	public Integer getEmployeeId(){
		return employeeID;
	}
	public void setEmployeeId(Integer employeeId){
		this.employeeID = employeeId;
	}
	public Integer getOfficeLocationNumber(){
		return officeLocationNumber;
	}
	public void setOfficeLocationNumber(Integer officeLocationNumber){
		this.officeLocationNumber = officeLocationNumber;
	}
	public Integer getGMTDiffMinutes(){
		return GMTDiffMinutes;
	}
	public void setGMTDiffMinutes(Integer GMTDiffMinutes){
		this.GMTDiffMinutes = GMTDiffMinutes;
	}
	
}
