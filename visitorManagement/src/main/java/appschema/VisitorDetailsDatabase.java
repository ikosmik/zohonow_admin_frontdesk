package appschema;

public class VisitorDetailsDatabase {

	private long visitDate ;
	private String jsonDataId ;
	private String jsonData ;
	private byte[] photoUrl ;
	private byte[] signatureUrl ;
	private int photoUploaded ;
	private int signatureUploaded ;
	private String visitPurpose ;
	
	public long getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(long visitDate) {
		this.visitDate = visitDate;
	}
	public String getJsonDataId() {
		return jsonDataId;
	}
	public void setJsonDataId(String jsonDataId) {
		this.jsonDataId = jsonDataId;
	}
	public String getJsonData() {
		return jsonData;
	}
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	public byte[] getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(byte[] photoUrl) {
		this.photoUrl = photoUrl;
	}
	public byte[] getSignatureUrl() {
		return signatureUrl;
	}
	public void setSignatureUrl(byte[] signatureUrl) {
		this.signatureUrl = signatureUrl;
	}
	public int getPhotoUploaded() {
		return photoUploaded;
	}
	public void setPhotoUploaded(int photoUploaded) {
		this.photoUploaded = photoUploaded;
	}
	public int getSignatureUploaded() {
		return signatureUploaded;
	}
	public void setSignatureUploaded(int signatureUploaded) {
		this.signatureUploaded = signatureUploaded;
	}
	public String getVisitPurpose() {
		return visitPurpose;
	}
	public void setVisitPurpose(String visitPurpose) {
		this.visitPurpose = visitPurpose;
	}
}
