package appschema;

import com.zoho.app.frontdesk.android.R;

import constants.MainConstants;
import constants.NumberConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;

/**
 * Created by Karthick on 09/02/17.
 */
public class SharedPreferenceController {

    /**
     * Save Locations to Shared Preference
     *
     * @author Karthick
     * @created 20170803
     */
    public void saveLocations(Context context, String locations) {

        if (context != null && locations != null && !locations.isEmpty()) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putString(MainConstants.spKeyLocation, locations);
            editor.commit();
        }
    }

    /**
     * Get Locations from Shared Preference
     *
     * @author Karthikeyan D S
     * @created 20170803
     */
    public String getLocations(Context context) {
        String locations = MainConstants.kConstantEmpty;
        if (context != null) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            locations = zcDatePreference.getString(
            		MainConstants.spKeyLocation,
                    MainConstants.kConstantEmpty);
        }
        return locations;
    }

    /**
     * Save selected Locations to Shared Preference
     *
     * @author KArthikeyan D S
     * @created 20170803
     */
    public void saveDeviceLocation(Context context, String locations) {

        if (context != null && locations != null && !locations.isEmpty()) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putString(MainConstants.spKeyDeviceLocation, locations);
            editor.commit();
        }
    }

    public void saveOfficeLocation(Context context, String locations) {

        if (context != null && locations != null && !locations.isEmpty()) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putString(MainConstants.spKeyOfficeLocation, locations);
            editor.commit();
        }
    }

    public void saveOfficeLocationCode(Context context, int locations) {

        if (context != null && locations > 0) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putInt(MainConstants.spKeyOfficeLocationCode, locations);
            editor.commit();
        }
    }

    /**
     * Save selected Application Mode to Shared Preference
     *
     * @author Karthick
     * @created 20170803
     */
    public void saveApplicationMode(Context context, int applicationModeCode) {

        if (context != null && applicationModeCode>0) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putInt(MainConstants.spKeyApplicationMode, applicationModeCode);
            editor.commit();
        }
    }

    /**
     * Get selected Application Mode from Shared Preference
     *
     * @author Karthick
     * @created 20170803
     */
    public int getApplicationMode(Context context) {
       int applicationModeCode = MainConstants.kConstantZero;
        if (context != null) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            applicationModeCode = zcDatePreference.getInt(
                    MainConstants.spKeyApplicationMode,
                    MainConstants.kConstantZero);
        }
        return applicationModeCode;
    }

    /**
     * Get selected Locations from Shared Preference
     *
     * @author Karthick
     * @created 20170803
     */
    public String getDeviceLocation(Context context) {
        String locations = MainConstants.kConstantEmpty;
        if (context != null) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            locations = zcDatePreference.getString(
                    MainConstants.spKeyDeviceLocation,
                    MainConstants.kConstantEmpty);
        }
        return locations;
    }

    public String getOfficeLocation(Context context) {
        String locations = MainConstants.kConstantEmpty;
        if (context != null) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            locations = zcDatePreference.getString(
                    MainConstants.spKeyOfficeLocation,
                    MainConstants.kConstantEmpty);
        }
        return locations;
    }

    public int getOfficeLocationCode(Context context) {
        int locations = NumberConstants.kConstantZero;
        if (context != null) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            locations = zcDatePreference.getInt(
                    MainConstants.spKeyOfficeLocationCode,
                    NumberConstants.kConstantZero);
        }
        return locations;
    }

    public int setAppThemeColor(Context context, int appColorCode )
    {
        int colorCode=-1;
        if(context != null && appColorCode>-1)
        {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.app_color_code), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putInt(MainConstants.appColorThemeCode, appColorCode);
            editor.commit();
        }
        return colorCode;
    }

    public int getAppThemeColor(Context context)
    {
        int colorCode=-1;
        if(context != null)
        {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.app_color_code), Context.MODE_PRIVATE);
            colorCode = zcDatePreference.getInt(MainConstants.appColorThemeCode,MainConstants.kConstantSharedPreferenceControllerAppTheme);
        }
        return colorCode;
    }


    /**
     * Save Employee Extension to Shared Preference
     *
     * @author Karthikeyan D S
     * @created 25Sep2018
     */
    public void saveEmployeeExtension(Context context, String employeeExtension) {

        if (context != null && employeeExtension != null && !employeeExtension.isEmpty()) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putString(MainConstants.spKeyEmployeeExtension,employeeExtension);
            editor.commit();
        }
    }

    /**
     * Get  Employee Extension from Shared Preference
     *
     * @author Karthikeyan D S
     * @created 25Sep2018
     */
    public String getEmployeeExtension(Context context) {
        String employeeExtension = MainConstants.kConstantEmpty;
        if (context != null) {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.shared_pref_name), Context.MODE_PRIVATE);
            employeeExtension = zcDatePreference.getString(
                    MainConstants.spKeyEmployeeExtension,
                    MainConstants.kConstantEmpty);
        }
        return employeeExtension;
    }

    /**
     *  Set Settings Password for App in DataCentre Mode
     *
     * @author Karthikeyan D S
     * @created 24OCT2018
     */
    public void setAppSettingsPassword(Context context, String appPaswords)
    {
        if(context != null && appPaswords!=null && !appPaswords.isEmpty())
        {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.app_color_code), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = zcDatePreference.edit();
            editor.putString(MainConstants.appSettingsPassword, appPaswords);
            editor.commit();
        }
    }

    /**
     *  get Settings Password for App in DataCentre Mode
     *
     * @author Karthikeyan D S
     * @created 24OCT2018
     */
    public String getAppSettingsPassword(Context context)
    {
        String appSettingsPassword=MainConstants.kConstantEmpty;
        if(context != null)
        {
            SharedPreferences zcDatePreference = context.getSharedPreferences(context.getString(R.string.app_color_code), Context.MODE_PRIVATE);
            appSettingsPassword = zcDatePreference.getString(MainConstants.appSettingsPassword,MainConstants.kConstantEmpty);
        }
        return appSettingsPassword;
    }


    /**
     *  clear Particular Shared Preference
     *
     * @author Karthikeyan D S
     * @created 28Sep2018
     */
    public void clearSharedPreferences(Context context,String sharedPrefrenceKey)
    {
        if(sharedPrefrenceKey!=null && !sharedPrefrenceKey.isEmpty()) {
            SharedPreferences spreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor spreferencesEditor = spreferences.edit();
            spreferencesEditor.remove(sharedPrefrenceKey);
            spreferencesEditor.commit();
        }
    }
}
