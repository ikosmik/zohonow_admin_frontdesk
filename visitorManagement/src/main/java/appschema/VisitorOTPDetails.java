package appschema;

/**
 * Created by Karthikeyan D S on 27July2018
 */
public class VisitorOTPDetails {

    private String mCreatorID;
    private String mVisitorPhoneNumber;
    private String mRequestedOTPNumber;
    private String mOTPValidity;
    private long mOTPValidityLong;
    private String mOTPRequestedTime;

    public long getmOTPValidityLong() {
        return mOTPValidityLong;
    }

    public void setmOTPValidityLong(long mOTPValidityLong) {
        this.mOTPValidityLong = mOTPValidityLong;
    }

    public String getmOTPRequestedTime() {
        return mOTPRequestedTime;
    }

    public void setmOTPRequestedTime(String mOTPRequestedTime) {
        this.mOTPRequestedTime = mOTPRequestedTime;
    }

    public String getmCreatorID() {
        return mCreatorID;
    }

    public void setmCreatorID(String mCreatorID) {
        this.mCreatorID = mCreatorID;
    }

    public String getmOTPValidity() {
        return mOTPValidity;
    }

    public void setmOTPValidity(String mOTPValidity) {
        this.mOTPValidity = mOTPValidity;
    }

    public String getmVisitorPhoneNumber() {
        return mVisitorPhoneNumber;
    }

    public void setmVisitorPhoneNumber(String mVisitorPhoneNumber) {
        this.mVisitorPhoneNumber = mVisitorPhoneNumber;
    }

    public String getmRequestedOTPNumber() {
        return mRequestedOTPNumber;
    }

    public void setmRequestedOTPNumber(String mRequestedOTPNumber) {
        this.mRequestedOTPNumber = mRequestedOTPNumber;
    }
}
