package appschema;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Karthikeyan D S on 03JUL2018
 */
public class SingletonStationeryDetails {

    private final static SingletonStationeryDetails stationeryInstance = new SingletonStationeryDetails();

    public Map<Long, Integer> getSelectedStationeryDetails() {
        return selectedStationeryDetails;
    }

    public void setSelectedStationeryDetails(Map<Long, Integer> selectedStationeryDetails) {
        this.selectedStationeryDetails = selectedStationeryDetails;
    }

    private Map<Long,Integer> selectedStationeryDetails = new HashMap<Long,Integer>();

    public Map<Integer, Integer> getSelectedStationeryQuantityDetails() {
        return selectedStationeryQuantityDetails;
    }

    public void setSelectedStationeryQuantityDetails(Map<Integer, Integer> selectedStationeryQuantityDetails) {
        this.selectedStationeryQuantityDetails = selectedStationeryQuantityDetails;
    }

    private Map<Integer,Integer> selectedStationeryQuantityDetails = new HashMap<Integer, Integer>();

    public static SingletonStationeryDetails getStationeryInstance()
    {
        return stationeryInstance;
    }


    public void clearInstance()
    {
        if(selectedStationeryDetails!=null&&selectedStationeryDetails.size()>0) {
            selectedStationeryDetails.clear();
        }
        if(selectedStationeryQuantityDetails!=null &&selectedStationeryQuantityDetails.size()>0)
        {
            selectedStationeryQuantityDetails.clear();
        }
    }
}
