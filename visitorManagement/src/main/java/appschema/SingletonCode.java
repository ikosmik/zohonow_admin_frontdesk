package appschema;

public class SingletonCode {

	private static Integer singletonSelectionCode;
	private static Integer singletonVisitorUploadErrorCode;
	private static Integer singletonTellUsCode;
	private static String singletonWifiPrinterIP;
	private static String singletonPrinterErrorCode;
	private static String singletonVolleyExceptionError;
	
	public static Integer getSingletonSelectionCode() {
		return singletonSelectionCode;
	}

	public static void setSingletonSelectionCode(Integer singletonSelectionCode) {
		SingletonCode.singletonSelectionCode = singletonSelectionCode;
	}

	public static Integer getSingletonVisitorUploadErrorCode() {
		return singletonVisitorUploadErrorCode;
	}

	public static void setSingletonVisitorUploadErrorCode(
			Integer singletonVisitorUploadErrorCode) {
		SingletonCode.singletonVisitorUploadErrorCode = singletonVisitorUploadErrorCode;
	}

	public static Integer getSingletonTellUsCode() {
		return singletonTellUsCode;
	}

	public static void setSingletonTellUsCode(Integer singletonTellUsCode) {
		SingletonCode.singletonTellUsCode = singletonTellUsCode;
	}

	public static String getSingletonWifiPrinterIP() {
		return singletonWifiPrinterIP;
	}

	public static void setSingletonWifiPrinterIP(String singletonWifiPrinterIP) {
		SingletonCode.singletonWifiPrinterIP = singletonWifiPrinterIP;
	}

	public static String getSingletonPrinterErrorCode() {
		return singletonPrinterErrorCode;
	}

	public static void setSingletonPrinterErrorCode(
			String singletonPrinterErrorCode) {
		SingletonCode.singletonPrinterErrorCode = singletonPrinterErrorCode;
	}

	public static String getSingletonVolleyExceptionError() {
		return singletonVolleyExceptionError;
	}

	public static void setSingletonVolleyExceptionError(
			String singletonVolleyExceptionError) {
		SingletonCode.singletonVolleyExceptionError = singletonVolleyExceptionError;
	}
}
