package appschema;


public class ExistingVisitorDetails {

	private String name;
	private String contact;
	private String employeeEmail;
	private String purpose;
	private String creatorVisitID;
	private long visitedDate;
	private boolean isVisitorPreApproved;

	public boolean isVisitorPreApproved() {
		return isVisitorPreApproved;
	}

	public void setVisitorPreApproved(boolean visitorPreApproved) {
		isVisitorPreApproved = visitorPreApproved;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getEmployeeEmail() {
		return employeeEmail;
	}
	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getCreatorVisitID() {
		return creatorVisitID;
	}
	public void setCreatorVisitID(String creatorVisitID) {
		this.creatorVisitID = creatorVisitID;
	}
	public long getVisitedDate() {
		return visitedDate;
	}
	public void setVisitedDate(long visitedDate) {
		this.visitedDate = visitedDate;
	}

//	Added by Karthikeyan D S on 02Jul2018 for dataCenter PreApproval Flow
	private String companyName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
